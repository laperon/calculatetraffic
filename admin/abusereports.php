<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

//Check for suspend link
if (is_numeric($_GET['suspend'])) {

	$getsiteid = mysql_query("Select siteid from ".$prefix."reports where id=".$_GET['suspend']." limit 1");
	$suspendsite = mysql_result($getsiteid, 0, "siteid");

	@mysql_query("Update ".$prefix."msites set state=3 WHERE id=".$suspendsite." limit 1");
	@mysql_query("Update ".$prefix."reports set action=2 where id=".$_GET['suspend']);
}

//Check for ignore link
if (is_numeric($_GET['ignore'])) {
	@mysql_query("Update ".$prefix."reports set action=1 where id=".$_GET['ignore']);
}

$showtype = "";
$timequery = "";

if ($_POST["searchtext"] != "") {
            $searchfield=trim($_POST["searchfield"]);
            $searchtext=trim($_POST["searchtext"]);
            
} elseif (($_GET['searchfield'] != "") && ($_GET['searchtext'] != "")) {
            $searchfield=trim($_GET["searchfield"]);
            $searchtext=trim($_GET["searchtext"]);
}

// Operations affecting report records

// Ignore reports according to check boxes
if($_POST["Submit"] == "Ignore Selected Reports")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$sqry="UPDATE ".$prefix."reports set action=1 WHERE id=".$val;
		@mysql_query($sqry) or die("Error: Unable to ignore multiple reports!");
		}
	}
}

// Suspend reported sites according to check boxes
if($_POST["Submit"] == "Suspend Selected Sites")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$getsiteid = mysql_query("Select siteid from ".$prefix."reports where id=".$val." limit 1");
		$bansite = mysql_result($getsiteid, 0, "siteid");
		@mysql_query("Update ".$prefix."msites set state=3 WHERE id=".$bansite." limit 1");
		$sqry="UPDATE ".$prefix."reports set action=2 WHERE id=".$val;
		@mysql_query($sqry) or die("Error: Unable to suspend multiple sites!");
		}
	}
}

// Delete multiple reports according to check boxes
if($_POST["MDelete"] == "Yes - Delete")
{
	if(isset($_POST["idarray"]))
	{
		while (list ($key,$val) = @each ($_POST["idarray"])) {
		$dqry="DELETE FROM ".$prefix."reports WHERE id=".$val;
		mysql_query($dqry) or die("Error: Unable to delete reports!");
		}
	}
}

// Ban sites according to check boxes
if($_POST["Submit"] == "Ban Selected URLs")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$getsiteid = mysql_query("Select siteid from ".$prefix."reports where id=".$val." limit 1");
		$bansite = mysql_result($getsiteid, 0, "siteid");
		$geturl = mysql_query("SELECT url from ".$prefix."msites WHERE id=".$bansite);
		$urltoban = mysql_result($geturl, 0, "url");
		$checkban = mysql_query("Select id from `banned_sites` where domain='$urltoban' limit 1");
		if (mysql_num_rows($checkban) == 0) {
		$sqry = "Insert into `banned_sites` (domain) values ('$urltoban')";
		@mysql_query($sqry) or die("Error: Unable to ban multiple sites!");
		@mysql_query("Update ".$prefix."msites set state=3 where url='$urltoban'");
		}
		@mysql_query("UPDATE ".$prefix."reports set action=2 WHERE id=".$val);
		}
	}
}

// Ban domains according to check boxes
if($_POST["Submit"] == "Ban Selected Domains")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$getsiteid = mysql_query("Select siteid from ".$prefix."reports where id=".$val." limit 1");
		$bansite = mysql_result($getsiteid, 0, "siteid");
		$geturl = mysql_query("SELECT url from ".$prefix."msites WHERE id=".$bansite);
		$urltoban = mysql_result($geturl, 0, "url");
		if ($urltoban != "") {
			$splittoban = explode("/", $urltoban);
			$urltoban = $splittoban[2];
			$urltoban = ereg_replace("www.", "", $urltoban);
		}
		$checkban = mysql_query("Select id from `banned_sites` where domain='$urltoban' limit 1");
		if (mysql_num_rows($checkban) == 0) {
		$sqry = "Insert into `banned_sites` (domain) values ('$urltoban')";
		@mysql_query($sqry) or die("Error: Unable to ban multiple domains!");
		@mysql_query("Update ".$prefix."msites set state=3 where url LIKE '%$urltoban%'");
		}
		@mysql_query("UPDATE ".$prefix."reports set action=2 WHERE id=".$val);
		}
	}
}

?>
<script src="../inc/jsfuncs.js" type="text/javascript"></script>
<?
	// Hide the search form when displaying mass delete confirmation
	if(!isset($_POST["Delchecked"]))
	{

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>

<?
if ($timequery == "") {
?>

<center>

<table width="600" border="1" align="center" cellpadding="4" bordercolor="#FFFFFF">
  <tr>
    <td bordercolor="#000000" bgcolor="#FFFF99"><p align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Abuse Reports</font></strong></p>
        <p align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The LFMTE will run an automated security check on a site when a member clicks Report Site in the surfbar, or if a surfing session is interrupted while a particular site is being displayed.  The script will automatically suspend the site if a problem is detected, or create an abuse report below if it's unable to detect the problem.  False reports can occasionally be generated if members refresh the surfbar or experience a problem with their connection, so each site should be checked manually before being suspended.</font></p>
	<p align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">It's also possible that the previous site displayed, or an upcoming site preloading in the background caused the problem.  If you cannot find a problem with the reported site, you can click "View Surf Logs" to check the sites that were viewed directly before or after the site being reported.</font></p>
        <p>&nbsp;</p></td>
  </tr>
</table>
<br>
<p><font size=3><b>Show:</b>

<a href="admin.php?f=areports&show=<?=$showtype?>&sf=browse"><? if ($searchfield!="action") { echo("<b>All</b>"); } else { echo("All"); } ?></a> | 

<a href="admin.php?f=areports&show=<?=$showtype?>&searchfield=action&searchtext=0&sf=browse"><? if ($searchfield=="action" && $searchtext==0) { echo("<b>Open</b>"); } else { echo("Open"); } ?></a> | 

<a href="admin.php?f=areports&show=<?=$showtype?>&searchfield=action&searchtext=1&sf=browse"><? if ($searchfield=="action" && $searchtext==1) { echo("<b>Ignored</b>"); } else { echo("Ignored"); } ?></a> | 

<a href="admin.php?f=areports&show=<?=$showtype?>&searchfield=action&searchtext=2&sf=browse"><? if ($searchfield=="action" && $searchtext==2) { echo("<b>Suspended</b>"); } else { echo("Suspended"); } ?></a> | 

</p>
</center>

<?
}
?>

<form name="searchfrm" method="post" action="admin.php?f=areports&show=<?=$showtype?>&sf=browse">
<table width="300" border="0" align="center" cellpadding="4" cellspacing="0" class="lfmtable">
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td><select name="searchfield">
      <option <? if($searchfield=="" || $searchfield=="action") { echo "selected=\"selected\""; } ?>>Search By...</option>
      <option <? if($searchfield=="siteid") { echo "selected=\"selected\""; } ?> value="siteid">Site ID</option>
      <option <? if($searchfield=="userfrom") { echo "selected=\"selected\""; } ?> value="userfrom">Reported By</option>
            <option <? if($searchfield=="date") { echo "selected=\"selected\""; } ?> value="date">Date Reported</option>
                </select></td>
    <td><input name="searchtext" type="text" id="searchtext" value="<? if ($searchfield != "action") { echo("$searchtext"); } ?>"/></td>
    <td><input type="submit" name="Submit" value="Search" /></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</form>
<?
}
	// Get the starting record for site browse
	if(!isset($_GET["limitStart"]))
	{
		$st=0;
	}
	else
	{
		$st=$_GET["limitStart"];
	}

	// Get the total site count for site browse
	$cqry="SELECT COUNT(*) as mcount FROM ".$prefix."reports";
	// Check if there is search criteria
	if($searchtext != "")
	{
		$cqry.=" WHERE ".$searchfield." = '".$searchtext."'";
	
	}

    $cres=@mysql_query($cqry);
    $crow=@mysql_fetch_array($cres);

    // Get the first/next 40 records
    $mqry="SELECT * FROM ".$prefix."reports WHERE id != 0";

    // Add search criteria if applicable
    if($searchtext != "") {
    
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' LIMIT $st,40";
		
		$searchtext = str_replace("%", "", $searchtext);

    }
    else
    {

	    $mqry.=" ORDER BY id LIMIT $st,40";
    }

    $mres=@mysql_query($mqry);


	//
	// Site browsing
	// This is where the search and browse records are diaplayed
	//
	if($_GET["sf"] == "browse" && !isset($_POST["Delchecked"]))
	{
?>
<form name="mbrowse" id="mbrowse" method="post" action="admin.php?f=areports&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sf=browse">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr>
        <td colspan="13" align="left" valign="bottom">
		<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($crow["mcount"],$_GET["limitStart"],40,"areports&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&sf=browse");
		?></div>
		
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:9px" value="Ignore Selected Reports" />
		<input name="Delchecked" type="submit" class="lfmtable" id="Delchecked" style="height: 8; font-size:9px" value="Delete Selected Reports" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:9px" value="Suspend Selected Sites" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:9px" value="Ban Selected URLs" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:9px" value="Ban Selected Domains" />
		
		</td>
        </tr>
        
      <tr class="admintd">
        <td width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">ID</font></strong></td>
        <td width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date Reported</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Reported By </font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL </font></strong></td>
		
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Comments </font></strong></td>
        
        <?
        echo("<td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong>
          <font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Action</font></strong></td>");
        ?>
          
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg">&nbsp;</td>
        <td width="16" align="center" nowrap="NOWRAP"><input type="checkbox" name="checkbox" value="checkbox" onClick="javascript:checkAll(document.getElementById('mbrowse'),'mcheck');" /></td>
      </tr>
<?
	while($mrow=@mysql_fetch_array($mres))
	{
	
		$getsiteid = mysql_query("Select siteid, text from ".$prefix."reports where id=".$mrow["id"]." limit 1");
		$siteid = mysql_result($getsiteid, 0, "siteid");
		$reportreason = mysql_result($getsiteid, 0, "text");
		$geturl = mysql_query("SELECT url from ".$prefix."msites WHERE id=".$siteid);
		if (mysql_num_rows($geturl) > 0) {
		$siteurl = mysql_result($geturl, 0, "url");
		} else {
		$siteurl = "SITE DELETED";
		}
	
		if ($mrow["action"] == 0) {
			$showaction = "<a target=\"_self\" href=\"admin.php?f=areports&show=".$showtype."&searchfield=".$searchfield."&searchtext=".$searchtext."&sf=browse&suspend=".$mrow["id"]."\">Suspend Reported Site</a><br><a target=\"_self\" href=\"admin.php?f=areports&show=".$showtype."&searchfield=".$searchfield."&searchtext=".$searchtext."&sf=browse&ignore=".$mrow["id"]."\">Ignore Report</a>";
		} elseif ($mrow["action"] == 1) {
			$showaction = "Ignored";
		} elseif ($mrow["action"] == 2) {
			$showaction = "Suspended";
		}

		if($bgcolor == "#FFFFFF")
		{
			$bgcolor="#DDDDDD";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}
		
		$reportdate = date('M j Y', $mrow["date"]);
		$reporttime = date('g:i A', $mrow["date"])."<br>(".date('H:i:s', $mrow["date"]).")";
		
?>
      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["id"];?>
        </font></td>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <? echo($reportdate."<br>".$reporttime);?>
        </font></td>
        
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a target="_blank" href="admin.php?f=mm&searchfield=Id&searchtext=<?=$mrow["userfrom"];?>&sf=browse">User ID <?=$mrow["userfrom"];?></a>
        </font></td>
        
        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
          <a target="_blank" href="/adminchecksite.php?siteid=<?=$siteid;?>"><?=$siteurl;?></a></font>
          <br><br>
        <?
        	$action = "";
        	if ((time() - $mrow["date"]) < 172800) { $action = "&action=48all"; }
        	if ((time() - $mrow["date"]) < 86400) { $action = "&action=24all"; }
        	if ((time() - $mrow["date"]) < 3600) { $action = "&action=1all"; }
        	
        	echo("&nbsp;<font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=sstats&userid=".$mrow["userfrom"]."&reportedsiteid=".$siteid."&reportedtime=".$mrow["date"]."&sort=1".$action."\">[View Surf Logs]</a></font>");
        ?>
        </td>

        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <textarea wrap="soft" rows="3" cols="30"><?=$mrow["text"];?></textarea>
        </font></td>
        
         <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$showaction;?>
        </font></td>

        <td width="24" align="center" bgcolor="<?=$bgcolor;?>"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:delReport(<?=$mrow["id"];?>)"><img src="../images/del.png" alt="Delete Report" width="16" height="16" border="0" /></a></font></td>

        <td align="center"><input type="checkbox" class="mcheck" name="memcheck[]" value="<?=$mrow["id"];?>" onclick="HighlightRowIfChecked(this);" />
        </td>
      </tr>
<?
}
?>
    </table>	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>
</form>
<?
}
//
// End of site browsing/searching section
//

// Delete Confirmation section
// This area is for display of details
// relating to multiple site deletion
if($_POST["Delchecked"] == "Delete Selected Reports")
{
	if(!isset($_POST["memcheck"]))
	{
?>
<center>
  <font face="Verdana, Arial, Helvetica, sans-serif">NO RECORDS SELECTED</font>
</center>
<?
		exit;
	}
?>
<br /><br />
<center><strong><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif">You Are About To Delete The Following Reports</font></strong></center>
<br /><br />
<form method="POST" action="admin.php?f=areports&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sf=browse">
<table align="center" cellpadding="2" cellspacing="0" class="lfmtable">
<tr class="admintd">
<td><strong>ID</strong></td>
</tr>
<?
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$dqry="SELECT * FROM ".$prefix."reports WHERE id=".$val;
		$dres=@mysql_query($dqry);
		$drow=@mysql_fetch_array($dres);
?>
<tr>
<td><?=$drow["id"];?></td><input type="hidden" name="idarray[]" value="<?=$drow["id"];?>" />
</tr>
<?
		}
	}

?>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr><td colspan="2" align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Are You Sure?</strong></font></td>
</tr>
<tr>
  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="No - Cancel" />
    <input name="MDelete" type="submit" id="MDelete" value="Yes - Delete" /></td>
</tr>
</table>
</form>
<?
}

?>