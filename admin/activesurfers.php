<?
////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

// v1.32
// Updated April 2010 to include 36 hour membership upgrades

// v1.4
// Updated July 2010 to include option of viewing all previous claims

// Cross Promo Addition
// �2011 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

require_once "../inc/filter.php";

	// Prevent anyone who isn't logged in from opening this page
	include "../inc/checkauth.php"; 
	if(!isset($_SESSION["adminid"])) { exit; };

// Generate Promo Key Function
function generatePromoKey ($length = 20) {
	  $password = "";
	  $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
	  $i = 0; 
	  while ($i < $length) { 
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
		  $password .= $char;
		  $i++;
	  	}
	  return $password;
}
// End Generate Promo Key Function

// XML Functions
function startingxml($parser, $name, $attrs)
{
	global $curname;
	$curname = "$name";
}

function endingxml($parser, $name)
{
	// IGNORE ENDING
}

function RESULTSXML($parser, $data)
{
	global $curname,$INVITEDATA;
    $INVITEDATA[$curname] .= htmlentities($data);
}
// End XML Functions

	echo '<center>';
    extract($_POST);

	$time = time();
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d",$time-86400); 
	$tomorrow = date("Y-m-d",$time+129600); // add 36 hours just in case it's nearly midnight...

	$limit=50;
	$offset=$_GET[offset];
	if (empty($offset)) { $offset=0; } 
	
	$siteurl = mysql_result(mysql_query("Select affurl from `".$prefix."settings` where id=1 limit 1"), 0);
	$spliturl = explode("/", $siteurl);
	$thisdomain = $spliturl[2];
	$thisdomain = strtolower(str_replace("www.", "", $thisdomain));


if($submit=="Update Email") {

	@mysql_query("Update `".$prefix."templates` set template_data='".$_POST['template_data']."' where template_name='Active Surfer Reward'");

} elseif($submit=="Create Promo") {

	$starttime = mktime(0,0,0,substr($_POST[sdate],5,2),substr($_POST[sdate],8),substr($_POST[sdate],0,4));
	$endtime = mktime(0,0,0,substr($_POST[edate],5,2),substr($_POST[edate],8),substr($_POST[edate],0,4))+86399;
	$promokey = generatePromoKey();
	
	$timezone = date("Z");
	$savingstime = date("I");
	
	$zquery = "INSERT INTO `".$prefix."active_surfer_promos` (starttime,endtime,createdby,timezone,savingstime,promokey,surf,upgrade,cash,credits,bannerimps,textimps,active) VALUES ('$starttime','$endtime','$thisdomain','$timezone','$savingstime','$promokey','$surf','$upgrade','$cash','$credits','$bannerimps','$textimps',2)";
	mysql_query($zquery) or die (mysql_error());
	
	$newpromoid = mysql_insert_id();
	
	for ($i = 0; $i < count($_POST['inexchanges']); $i++) {
		
		$exchangedomain = $_POST['inexchanges'][$i];
		$exchangedomain = strtolower(str_replace("www.", "", $exchangedomain));
		$exchangerefid = 0;
		if ($exchangedomain != "") {
			mysql_query("Insert into `".$prefix."active_surfer_sites` (promoid, domain, refid) VALUES ($newpromoid, '$exchangedomain', '$exchangerefid')");
		}
	}
	
	
} elseif($submit=="Upload") {

	if ($_FILES['invfile']['name'] != null and $_FILES['invfile']['error']<1) {
	
	$tempfilename = "xmltempfile".rand(0,9999);
	
	copy($_FILES['invfile']['tmp_name'],$tempfilename);
	
	$fileconnect = fopen($tempfilename,'r');
	$tempfiledata = fread($fileconnect, 1000000);
	fclose($fileconnect);
	
	@unlink($tempfilename);
	
	// INITIATE XML PARSING
	$xml_parser = xml_parser_create();
	
	xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
	xml_set_element_handler($xml_parser, "startingxml", "endingxml");
	xml_set_character_data_handler($xml_parser, "RESULTSXML");
	
	$parser = xml_parse($xml_parser, $tempfiledata, true);
	
	if (!$parser) {
		echo("Could not parse XML file. There may be an error with the invitation file.<br><br><a href=\"admin.php?f=active\">Go Back</a>");
		exit;
	}
	
	xml_parser_free($xml_parser);
	
	$starttime = $INVITEDATA['STARTTIME'];
	$endtime = $INVITEDATA['ENDTIME'];
	$createdby = $INVITEDATA['CREATEDBY'];
	$timezone = $INVITEDATA['TIMEZONE'];
	$savingstime = $INVITEDATA['SAVINGSTIME'];
	$promokey = $INVITEDATA['PROMOKEY'];
	$surf = $INVITEDATA['SURF'];
	$upgrade = $INVITEDATA['UPGRADE'];
	$cash = $INVITEDATA['CASH'];
	$credits = $INVITEDATA['CREDITS'];
	$bannerimps = $INVITEDATA['BANNERIMPS'];
	$textimps = $INVITEDATA['TEXTIMPS'];
	
	$invdomains = $INVITEDATA['INVDOMAINS'];
	
	if (!isset($starttime) || !is_numeric($starttime)) {
		echo("Could not read XML data. There may be an error with the invitation file.<br><br><a href=\"admin.php?f=active\">Go Back</a>");
		exit;
	}
	
	if (($createdby != $thisdomain) && strpos($invdomains, $thisdomain) === false) {
		echo("The domain ".$thisdomain." could not be found on the list of domains in this promotion.<br><br>The list of domains included in this invitation is: ".$invdomains."<br><br><a href=\"admin.php?f=active\">Go Back</a>");
		exit;
	}
	
	$invdomainsarr = explode(',', $invdomains);
	

		// Check Time On Inviting Exchange
		$checkurl = "http://".$createdby."/claim_checktime.php";
		
		$urlinfo = parse_url($checkurl);
		$hostname = $urlinfo['host'];
		if ($hostname == "" || gethostbyname($hostname) == $hostname) {
			echo("Could not connect to: ".$createdby."<br>Their server appears to be offline.<br><br><a href=\"admin.php?f=active\">Go Back</a>");
			exit;
		} else {
			$port = 80;
			$timeout = 5;
			$headers = "GET ".$checkurl." HTTP/1.1\r\n";
			$headers .= "Host: ".$hostname."\r\n";
			$headers .= "Referer: http://".$_SERVER["SERVER_NAME"]."\r\n";
			$headers .= "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
			$headers .= "Connection: close\r\n";
			$headers .= "Accept: */*\r\n";
			$headers .= "\r\n";
			$checkreturn = "";
			$attempts = 0;
			while((strpos($checkreturn, "200 OK") === false) && (strpos($checkreturn, "302 Found") === false) && ($attempts<2)) {
				$connection = fsockopen($hostname, $port, $errno, $errstr, $timeout);
				if ($connection) {
					stream_set_timeout($connection, $timeout);
					fwrite($connection,$headers);
					//Read the reply
					$checkreturn = "";
					$readruns = 0;
					while (!feof($connection) && !$checktimeout['timed_out'] && $readruns < 50) {
						$checkreturn .= fread($connection, 10000);
						$checktimeout = stream_get_meta_data($connection);
						$readruns++;
					}
					fclose($connection);
				}
				$attempts = $attempts+1;
			}
			
			if((strpos($checkreturn, "200 OK") === false) && (strpos($checkreturn, "302 Found") === false)) {
				echo("Could not connect to: ".$createdby."<br>Their server may be having technical problems, or their SURFER REWARDS mod could be installed incorrectly.<br><br><a href=\"admin.php?f=active\">Go Back</a>");
				exit;
			} elseif (strpos($checkreturn, "Time:") === false) {
				echo("Could not connect to: ".$createdby."<br>Their server may be having technical problems, or their SURFER REWARDS mod could be installed incorrectly.<br><br><a href=\"admin.php?f=active\">Go Back</a>");
				exit;
			} else {
				$colonpos = strpos($checkreturn, "Time:");
				$colonpos = $colonpos+5;
				$servertimecount = trim(substr($checkreturn, $colonpos));
				
				$servertimesplit = explode(':', $servertimecount);
				$servertimecount = trim($servertimesplit[0]);
				
				if (!is_numeric($servertimecount)) {
					echo("Could not connect to: ".$createdby."<br>Their server may be having technical problems, or their SURFER REWARDS mod could be installed incorrectly.<br><br><a href=\"admin.php?f=active\">Go Back</a>");
					exit;
				}
			}
		}
		
		// End Check Time On Inviting Exchange
		
		$timediff = $time - $servertimecount;
		
		$thistimezone = date("Z");
		$thissavingstime = date("I");
		
		if ($thissavingstime == 0 && $savingstime == 1) {
			// Add an hour
			$timediff = $timediff+3600;
		} elseif ($thissavingstime == 1 && $savingstime == 0) {
			// Subtract an hour
			$timediff = $timediff-3600;
		}
		
		if ($thistimezone != $timezone) {
			// Figure out the date in the other timezone
			
			$timezonediff = $timezone-$thistimezone;
			$timediff = $timediff+$timezonediff;
			
		}
		
		
		if (($timediff>0 && $timediff<5) || ($timediff<0 && $timediff>-5)) {
			// Any difference less than 5 seconds is probably just the connection time.
			$timediff = 0;
		}
		
		@mysql_query("Delete from `".$prefix."active_surfer_promos` where promokey='$promokey'");
		
		$zquery = "INSERT INTO `".$prefix."active_surfer_promos` (starttime,endtime,createdby,timediff,promokey,surf,upgrade,cash,credits,bannerimps,textimps,active) VALUES ('$starttime','$endtime','$createdby','$timediff','$promokey','$surf','$upgrade','$cash','$credits','$bannerimps','$textimps',2)";
		mysql_query($zquery) or die (mysql_error());
		
		$newpromoid = mysql_insert_id();
		
		for ($i = 0; $i < count($invdomainsarr); $i++) {
			$exchangedomain = $invdomainsarr[$i];
			$exchangerefid = 0;
			if ($exchangedomain != $thisdomain) {
				mysql_query("Insert into `".$prefix."active_surfer_sites` (promoid, domain, refid) VALUES ($newpromoid, '$exchangedomain', '$exchangerefid')");
			}
		}
		
	} else {
		echo("There was an error uploading the file.<br><br><a href=\"admin.php?f=active\">Go Back</a>");
		exit;
	}


} elseif($submit=="Edit Promo" && is_numeric($_POST['promoid'])) {
	
	$promoid = $_POST['promoid'];
	
	$upgrade = $_POST['upgrade'];
	$cash = $_POST['cash'];
	$credits = $_POST['credits'];
	$bannerimps = $_POST['bannerimps'];
	$textimps = $_POST['textimps'];
	$active = $_POST['active'];
	
	if ($active < 1) { $active=2; }
	
	$zquery = "UPDATE `{$prefix}active_surfer_promos` SET upgrade='$upgrade', cash='$cash', credits='$credits', bannerimps='$bannerimps', textimps='$textimps', active=$active where id=$promoid";
	mysql_query($zquery) or die (mysql_error());
	
	for ($i = 0; $i < count($_POST['inexchanges']); $i++) {
		$exchangedomain = $_POST['inexchanges'][$i];
		$exchangerefid = $_POST['inrefids'][$i];
		if (is_numeric($exchangerefid)) {
			mysql_query("Update `".$prefix."active_surfer_sites` set refid=".$exchangerefid." where promoid=".$promoid." and domain='".$exchangedomain."'");
		}
	}
	
} elseif($_GET['action']=="removepromo" && is_numeric($_GET['promoid'])) {

	$remquery = "Update `{$prefix}active_surfer_promos` set active=0 where id=".$_GET['promoid'];
	mysql_query($remquery);
	
} elseif($submit=="Reward!") {
	if($prize=="upgrade") {
	
		$getupgend = mysql_query("SELECT mtype, upgend FROM {$prefix}members WHERE Id=$usrid");
		$upgend = mysql_result($getupgend, 0, "upgend");
		$usermtype = mysql_result($getupgend, 0, "mtype");
		
		$upgend = date("Y-m-d",strtotime($upgend)+86400); // add 24 hours to existing expiry date
		if($upgend <= date("Y-m-d")) { $upgend = date("Y-m-d",time()+129600); } // add 36 hours to current PHP time
		if($usermtype > $amount) { $amount = $usermtype; } // don't downgrade existing members!
		
		mysql_query("UPDATE {$prefix}members SET mtype=$amount, upgend='$upgend' WHERE Id=$usrid LIMIT 1") or die(mysql_error());
		
	} elseif($prize=="cash") {
		mysql_query("INSERT INTO {$prefix}sales (affid,saledate,itemamount,itemname,commission,txn_id,prize) VALUES ($usrid,NOW(),'$amount','Active Surfer Reward','$amount','0',1)") or die(mysql_error());
	} else {
		mysql_query("UPDATE {$prefix}members SET $prize=$prize+$amount WHERE Id=$usrid LIMIT 1") or die(mysql_error());
	}
	mysql_query("UPDATE `{$prefix}active_surfers` SET awarded=$time WHERE id=$ffrm") or die(mysql_error());
	// Query settings table for sitename
	$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
	$row=@mysql_fetch_array($res);
	$sitename=$row["sitename"];
	$replyaddress=$row["replyaddress"];
	$subject="Your Active Surfer Reward";
	$headers = 'From: '.$sitename.' <'.$replyaddress.'>';
	$res = mysql_query("SELECT * FROM {$prefix}members WHERE Id=$usrid");
	$row = mysql_fetch_array($res);
	$emailbody = mysql_result(mysql_query("SELECT template_data FROM {$prefix}templates WHERE template_name='Active Surfer Reward'"), 0);
	$emailbody = trim($emailbody);
	
	if ($emailbody != "") {
	
		if($prize=="upgrade") {
			$accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$amount"),0);
			$reward = '1 day '.$accname.' Upgrade';
		}
		elseif($prize=="cash") { $reward = '$'.$amount.' USD cash'; }
		elseif($prize=="credits") { $reward = $amount.' Credits'; }
		elseif($prize=="bannerimps") { $reward = $amount.' Banner Impressions'; }
		elseif($prize=="textimps") { $reward = $amount.' Text Link Impressions'; }
		$emailbody = str_replace("#FIRSTNAME#",$row[firstname],$emailbody);
		$emailbody = str_replace("#CLICKS#",$row[clickstoday],$emailbody);
		$emailbody = str_replace("#REWARD#",$reward,$emailbody);
		$emailbody = str_replace("#SITENAME#",$sitename,$emailbody);
		mail($row[email], $subject, $emailbody, $headers);
		echo "<p><em>User ID $usrid has received his reward!<br>Confirmation message sent to</em> <b>$row[email]</b>";
	
	} else {
		echo "<p><em>User ID $usrid has received his reward!</em>";
	}
	
} elseif($submit=="Decline") {
	mysql_query("DELETE FROM `{$prefix}active_surfers` WHERE id=$ffrm") or die(mysql_error());
	
} elseif (is_numeric($ffrm)) {
	if ($ffrm == 0) {
		$zquery = "INSERT INTO `{$prefix}active_surfer_rewards` (surf,upgrade,cash,credits,bannerimps,textimps) VALUES ('$surf','$upgrade','$cash','$credits','$bannerimps','$textimps')";
		} elseif ($submit == 'Delete') {
		$zquery = "DELETE FROM `{$prefix}active_surfer_rewards` WHERE id=$ffrm";
		} else {
		$zquery = "UPDATE `{$prefix}active_surfer_rewards` SET surf='$surf', upgrade='$upgrade', cash='$cash', credits='$credits', bannerimps='$bannerimps', textimps='$textimps', active=$active where id=$ffrm";
		}
	mysql_query($zquery) or die (mysql_error());
}

echo '<p>
Here are the claims that are pending manual approval:
<p>
<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="#F0F0F0"><th rowspan=2>Award ID</th><th rowspan=2>User ID</th><th rowspan=2>Username</th><th rowspan=2>Requested</th><th rowspan=2>Surfed</th><th colspan=2>Total Surfed</th><th rowspan=2>Award?</th></tr>
<tr><th width=50 style="font-size:9px">Today</th><th width=50 style="font-size:9px">Yesterday</th></tr>
';

$res = mysql_query("SELECT * FROM `{$prefix}active_surfers` WHERE NOT awarded ORDER BY id");
if(mysql_num_rows($res)) {
   while( $row=mysql_fetch_array($res) ) {
    $usrid = $row[usrid];
	$username = '&nbsp;'.@mysql_result(mysql_query("SELECT username FROM {$prefix}members WHERE Id=$usrid"),0);
	echo '<tr align="center"><td>'.$row[id].'</td><td><a href="javascript:editMember('.$row[usrid].')">'.$usrid.'</a></td><td>'.$username.'</td><td>';
	if($row[upgrade]>0) {
		$accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$row[upgrade]"),0);
		echo '1 day '.$accname.' Upgrade'; $prize="upgrade";
		}
		elseif($row[cash]>0) { echo '$'.$row[cash].' USD cash'; $prize="cash"; }
		elseif($row[credits]>0) { echo $row[credits].' credits'; $prize="credits";}
		elseif($row[bannerimps]>0) { echo $row[bannerimps].' banner impressions'; $prize="bannerimps"; }
		elseif($row[textimps]>0) { echo $row[textimps].' text link impressions'; $prize="textimps"; }
		else { echo 'No prize claimed!'; }
	$stoday = $syesterday = 0;
	$res2 = mysql_query("SELECT clickstoday FROM {$prefix}members WHERE Id=$row[usrid]");
	if(mysql_num_rows($res2)) { $stoday = mysql_result($res2,0); }
	$res2 = mysql_query("SELECT clicks FROM {$prefix}surflogs WHERE userid=$row[usrid] AND `date`='$yesterday'");
	if(mysql_num_rows($res2)) { $syesterday = mysql_result($res2,0); }
	echo '</td><td>'.$row[views].'</td><td>'.$stoday.'</td><td>'.$syesterday.'</td><td>
<form action="admin.php?f=active" method="post">
<input type=hidden name=ffrm value="'.$row[id].'">
<input type=hidden name=usrid value="'.$usrid.'">
<input type=hidden name=views value="'.$row[views].'">
<input type=hidden name=prize value="'.$prize.'">
<input type=hidden name=amount value="'.$row["$prize"].'">
<input type=submit name=submit value="Reward!">
<br>
<input type=submit name=submit value="Decline">
</form>
</td></tr>';
	}
   } else {
   echo '<tr><td colspan=8 align="center">There are no claims pending!</td></tr>';
   }


$mailtemplate = mysql_result(mysql_query("SELECT template_data FROM {$prefix}templates WHERE template_name='Active Surfer Reward'"), 0);

echo '
</table>

<br>

<p><b>Manual Approval Confirmation E-mail</b><br><br>Note: Leave blank to disable confirmation e-mails.</p>

<form action="admin.php?f=active" method="post">
<textarea rows=5 cols=40 name="template_data">'.$mailtemplate.'</textarea><br>
<input type="submit" name="submit" value="Update Email"><br>
</form>

<hr>
<p><b>Daily Surf Rewards</b></p>
<br><br><br>Here you can add, edit and delete active surfer rewards.
<br>Each reward must be active for it to appear in the Members Control Panel.
<br>This allows you to turn rewards on or off as required.
<br>Prizes can be awarded automatically when claimed or manually by admin approval.
<p>
<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="skyblue"><th rowspan=2>Surf</th><th colspan=5>Reward Choices (Member can choose ONE item)</th><th rowspan=2>Status</th><th rowspan=2>Action</th></tr>
<tr bgcolor="#DDDDDD"><th>1 Day Upgrade</th><th>Cash<th>Credits</th><th>Banners</th><th>Text Links</th></tr>
<form action="admin.php?f=active" method="POST">
<input type=hidden name=ffrm value="0">
<tr bgcolor="#DDDDDD" align="center">
<td><input type=text name=surf value="25" size=2></td>
<td><SELECT name="upgrade">
<OPTION value=0>None</OPTION>
';
$res = mysql_query("SELECT mtid,accname FROM {$prefix}membertypes WHERE mtid>1 ORDER BY mtid");
while($row=mysql_fetch_assoc($res)) {
	echo '<OPTION value='.$row[mtid].'>'.$row[accname].'</OPTION>';
	}
echo '</SELECT></td>
<td width=50><input type=text name=cash value="0.00" size=2></td>
<td width=50><input type=text name=credits value="0" size=2></td>
<td width=50><input type=text name=bannerimps value="0" size=2></td>
<td width=50><input type=text name=textimps value="0" size=2></td>
<td>&nbsp;</td>
<td style="vertical-align:middle;"><input type="submit" value="Add"></td>
</tr>
</FORM>
<p>';

$res = mysql_query("SELECT * FROM `{$prefix}active_surfer_rewards` ORDER BY id DESC");
$rows = mysql_num_rows($res);
for ($i = 0; $i < $rows; $i++) {
    $row=mysql_fetch_array($res);
    extract($row);
    echo '
<form action="admin.php?f=active" method="post">
<input type=hidden name=ffrm value="'.$id.'">
<tr bgcolor="#DDDDDD" align="center">
<td><input type=text name=surf value="'.$surf.'" size=2 style="text-align:right"></td>
<td><SELECT name="upgrade">
<OPTION value=0>None</OPTION>
';
$res2 = mysql_query("SELECT mtid,accname FROM {$prefix}membertypes WHERE mtid>1 ORDER BY mtid");
while($row2=mysql_fetch_assoc($res2)) {
	echo '<OPTION value='.$row2[mtid];
	if($upgrade == $row2[mtid]) { echo ' SELECTED'; }
	echo '>'.$row2[accname].'</OPTION>';
	}
echo '</SELECT></td>
<td><input type=text name=cash value="'.$cash.'" size=2 style="text-align:right"></td>
<td><input type=text name=credits  value="'.$credits.'" size=2 style="text-align:right"></td>
<td><input type=text name=bannerimps  value="'.$bannerimps.'" size=2 style="text-align:right"></td>
<td><input type=text name=textimps  value="'.$textimps.'" size=2 style="text-align:right"></td>
<td><select name="active">
<option value=0>Inactive</option>
<option value=1';
if($active==1) { echo ' selected'; }
	echo '>Active (Manual)</option>
<option value=2';
if($active==2) { echo ' selected'; }
	echo '>Active (Automatic)</option>
</select></td>
<td style="vertical-align:middle;"><input type=submit value="Save"><br><input type=submit name=submit value="Delete" onclick="return deleteIt()"></td>
</tr>
</form>';
	}

echo '
</table>
<br><br>

<p><font size="3"><b><a target="_blank" href="activesurfers_rewards.php?promoid=0">View Reward Claims</a></b></font></p>

<hr>
';

// Join A Cross Promo
echo '<p><b>Join A Cross Promotion.</b></p>
<p>If you have been invited to join a cross promotion from another LFMTE exchange, you can upload the invitation file here.</p>
<p>

	<form name="frm" enctype="multipart/form-data" method="POST" action="admin.php?f=active">
	<table width="400" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr>
        <td style="border:solid thin #666666;"  bgcolor="skyblue" align="center"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Upload An Invitation </font></strong></td>
        </tr>
      <tr>
        <td align="right"><input name="invfile" type="file" id="invfile" size="32" /></td>
      </tr>
      <tr>
        <td style="border:solid thin #666666;" align="center"><input type="submit" name="submit" value="Upload" /></td>
        </tr>
    </table>
    </form>

<hr>
';
// End Join A Cross Promo


// Create A Cross Promo
echo '<p><b>Create A Cross Promotion.</b></p>

<script language="javascript">
function addExchange() {
var formdiv = document.getElementById("formlist"); 
var newformfield = document.createElement(\'div\'); 
newformfield.innerHTML = "Domain: <input size=\'35\' type=\'text\' value=\'\' name=\'inexchanges[]\' /><br>";
while (newformfield.firstChild) {
        formdiv.appendChild(newformfield.firstChild); 
    }
}
</script>


<table width="400" border="0" align="center" cellpadding="5" cellspacing="0">
<tr><td align="left">
<p align="left">You can create a new cross promotion below. After setting up a cross promotion, it will not function properly until all the
invited exchanges have uploaded the invitation file, which you will be able to download and send to them.  The invited exchanges <b>cannot be changed</b>
once a cross promotion is created, so be sure to verify they have this mod installed and wish to participate before creating the promotion.</p>
</td></tr>
</table>


<p>

<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="skyblue"><th rowspan=2>Surf (Per Exchange)</th><th colspan=5>Reward Choices (Member can choose ONE item)</th>
<tr bgcolor="DDDDDD"><th>1 Day Upgrade</th><th>Cash<th>Credits</th><th>Banners</th><th>Text Links</th></tr>
<form action="admin.php?f=active" method="POST">
<input type=hidden name=ffrm value="0">
<tr bgcolor="#DDDDDD" align="center">
<td><input type=text name=surf size=2 value="25"></td>
<td><SELECT name="upgrade">
<OPTION value=0>None</OPTION>
';
$res = mysql_query("SELECT mtid,accname FROM {$prefix}membertypes WHERE mtid>1 ORDER BY mtid");
while($row=mysql_fetch_assoc($res)) {
	echo '<OPTION value='.$row[mtid].'>'.$row[accname].'</OPTION>';
	}
echo '</SELECT></td>
<td width=50><input type=text name=cash value="0.00" size=2></td>
<td width=50><input type=text name=credits value="0" size=2></td>
<td width=50><input type=text name=bannerimps value="0" size=2></td>
<td width=50><input type=text name=textimps value="0" size=2></td>
</tr>

<tr><td colspan="2" align="center">Start Date: <input name="sdate" type id="DPC_sdate_YYYY-MM-DD" value="'.$from.'" width="80" text></td><td colspan="4" align="center">End Date: <input name="edate" type id="DPC_edate_YYYY-MM-DD" value="'.$to.'" width="80" text></td></tr>

<tr bgcolor="skyblue"><td colspan="6" align="center"><b>Invited Exchange Domains</b><br><i>Example: fivehits.com</i></td></tr>

<tr bgcolor="DDDDDD"><td colspan="6" align="center">
	<div id="formlist">
	Domain: <input size="35" type="text" value="" name="inexchanges[]" /><br>
	</div>
	<input type="button" onclick="addExchange()" name="add" value="Add Exchange" />
</td></tr>

<tr bgcolor="skyblue"><td colspan="6" align="center"><input type="submit" name="submit" value="Create Promo" /></td></tr>

</FORM>
</table>


<hr>
';
// End Create A Cross Promo


// Get Cross Promos

echo '<p><b>Cross Promotions.</b></p>

<table width="400" border="0" align="center" cellpadding="5" cellspacing="0">
<tr><td align="left">
<p align="left">Here you can edit existing cross promotions.  If you created the promotion, you can click the link to download
the invitation file, which the participating exchanges will need to upload in the Admin area to join the promotion.</p>
</td></tr>
</table>
';



$getpromos = mysql_query("Select * from `".$prefix."active_surfer_promos` where active>0");

if (mysql_num_rows($getpromos) > 0) {
for ($i = 0; $i < mysql_num_rows($getpromos); $i++) {

	$promoid = mysql_result($getpromos, $i, "id");
	$starttime = mysql_result($getpromos, $i, "starttime");
	$endtime = mysql_result($getpromos, $i, "endtime");
	$createdby = mysql_result($getpromos, $i, "createdby");
	
	$surf = mysql_result($getpromos, $i, "surf");
	$upgrade = mysql_result($getpromos, $i, "upgrade");
	$cash = mysql_result($getpromos, $i, "cash");
	$credits = mysql_result($getpromos, $i, "credits");
	$bannerimps = mysql_result($getpromos, $i, "bannerimps");
	$textimps = mysql_result($getpromos, $i, "textimps");
	$active = mysql_result($getpromos, $i, "active");
	
	$startdate = date("j M Y", $starttime);
	$enddate = date("j M Y", $endtime);


	echo '<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="skyblue"><th rowspan=2>Surf (Per Exchange)</th><th colspan=5>Reward Choices (Member can choose ONE item)</th><th rowspan=2>Approval</th>
<tr bgcolor="#DDDDDD"><th>1 Day Upgrade</th><th>Cash<th>Credits</th><th>Banners</th><th>Text Links</th></tr>
<form action="admin.php?f=active" method="POST">
<input type=hidden name=promoid value="'.$promoid.'">
<tr bgcolor="#DDDDDD" align="center">
<td><input type=text name=surf size=2 value='.$surf.' disabled="disabled"></td>
<td><SELECT name="upgrade">
<OPTION value=0>None</OPTION>
';
$res = mysql_query("SELECT mtid,accname FROM {$prefix}membertypes WHERE mtid>1 ORDER BY mtid");
while($row=mysql_fetch_assoc($res)) {
	echo '<OPTION value='.$row[mtid]; if($upgrade == $row[mtid]) { echo ' SELECTED'; } echo'>'.$row[accname].'</OPTION>';
	}
echo '</SELECT></td>
<td width=50><input type=text name=cash value="'.$cash.'" size=2></td>
<td width=50><input type=text name=credits value="'.$credits.'" size=2></td>
<td width=50><input type=text name=bannerimps value="'.$bannerimps.'" size=2></td>
<td width=50><input type=text name=textimps value="'.$textimps.'" size=2></td>

<td><select name="active">
<option value=1';
if($active==1) { echo ' selected'; }
	echo '>Manual</option>
<option value=2';
if($active==2) { echo ' selected'; }
	echo '>Automatic</option>
</select></td>

</tr>
';

if ($createdby == $thisdomain) { echo'<tr bgcolor="orange"><td colspan="7" align="center"><a target="_blank" href="activesurfers_dl.php?promoid='.$promoid.'"><b>Download Invitation File</b></a></td></tr>'; }

echo'
<tr><td colspan="3" align="right">Start Date: <b>'.$startdate.'</b></td><td colspan="4" align="right">End Date: <b>'.$enddate.'</b></td></tr>

<tr bgcolor="skyblue"><td colspan="7" align="center"><b>Invited Exchange Domains</b></td></tr>

<tr bgcolor="skyblue"><td colspan="4" align="center">Domain</td><td colspan="3" align="center">Your Referral ID</td></tr>

';

echo '<tr bgcolor="#DDDDDD"><td colspan="4" align="center">'.$thisdomain.'</td><td colspan="3" align="left"><input type="text" name="nadomain" value="N/A" disabled="disabled"></td></tr>';

$getexchanges = mysql_query("Select domain, refid from `".$prefix."active_surfer_sites` where promoid=".$promoid." and domain!='".$thisdomain."' order by domain asc");
for ($j = 0; $j < mysql_num_rows($getexchanges); $j++) {
	$domain = mysql_result($getexchanges, $j, "domain");
	$refid = mysql_result($getexchanges, $j, "refid");
	echo '<tr bgcolor="#DDDDDD"><td colspan="4" align="center">'.$domain.'</td><td colspan="3" align="left"><input type="hidden" name="inexchanges[]" value="'.$domain.'"><input type="text" name="inrefids[]" value="'.$refid.'"></td></tr>';
}

echo'
<tr bgcolor="skyblue"><td colspan="7" align="center"><input type="submit" name="submit" value="Edit Promo" /> &nbsp; <a onclick="return confirm(\'Are you sure you want to remove this promo?\')" href="admin.php?f=active&action=removepromo&promoid='.$promoid.'"><font size=2 color=red><b>Remove Promo</b></font></a></td></tr>

</FORM>
</table>

<br><br>

<p><font size="3"><b><a target="_blank" href="activesurfers_rewards.php?promoid='.$promoid.'">View Reward Claims</a></b></font></p>

<hr>

	';

}
}
// End Get Cross Promos



echo '
</center>';

?>