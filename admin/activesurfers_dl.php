<?
////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$siteurl = mysql_result(mysql_query("Select affurl from `".$prefix."settings` where id=1 limit 1"), 0);
$spliturl = explode("/", $siteurl);
$thisdomain = $spliturl[2];
$thisdomain = strtolower(str_replace("www.", "", $thisdomain));

if(!is_numeric($_GET['promoid'])) {
echo("<p><b>Invalid Promo ID</b></p>");
exit;
}

$getpromoinfo = mysql_query("Select * from `".$prefix."active_surfer_promos` where id=".$_GET['promoid']);

$output = "<?xml version=\"1.0\" ?>\n";

$output .= "<crosspromoinvite>";

$output .= "<starttime>".mysql_result($getpromoinfo, 0, "starttime")."</starttime>";

$output .= "<endtime>".mysql_result($getpromoinfo, 0, "endtime")."</endtime>";

$output .= "<createdby>".mysql_result($getpromoinfo, 0, "createdby")."</createdby>";

$output .= "<timezone>".mysql_result($getpromoinfo, 0, "timezone")."</timezone>";

$output .= "<savingstime>".mysql_result($getpromoinfo, 0, "savingstime")."</savingstime>";

$output .= "<promokey>".mysql_result($getpromoinfo, 0, "promokey")."</promokey>";

$output .= "<surf>".mysql_result($getpromoinfo, 0, "surf")."</surf>";

$output .= "<upgrade>".mysql_result($getpromoinfo, 0, "upgrade")."</upgrade>";

$output .= "<cash>".mysql_result($getpromoinfo, 0, "cash")."</cash>";

$output .= "<credits>".mysql_result($getpromoinfo, 0, "credits")."</credits>";

$output .= "<bannerimps>".mysql_result($getpromoinfo, 0, "bannerimps")."</bannerimps>";

$output .= "<textimps>".mysql_result($getpromoinfo, 0, "textimps")."</textimps>";

$domaindata = $thisdomain;

$getexchanges = mysql_query("Select domain from `".$prefix."active_surfer_sites` where promoid=".$_GET['promoid']." and domain!='".$thisdomain."' order by domain asc");
for ($i = 0; $i < mysql_num_rows($getexchanges); $i++) {
	$domaindata .= ",".mysql_result($getexchanges, $i, "domain");
}

$output .= "<invdomains>".$domaindata."</invdomains>";

$output .= "</crosspromoinvite>";

if ($_GET['debug'] == 1) {
	echo($output);
	exit;
} else {
	header("Content-type: text/xml");
	header("Content-Disposition: attachment;filename=invite.xml");
	header('Pragma: no-cache');
	header('Expires: 0');
	echo($output);
	exit;
}

?>