<?
////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

echo("<html>
<body>
<center>
");

$time = time();
$today = date("Y-m-d");
$yesterday = date("Y-m-d",$time-86400); 
$tomorrow = date("Y-m-d",$time+129600); // add 36 hours just in case it's nearly midnight...

$limit=50;
$offset=$_GET[offset];
if (empty($offset)) { $offset=0; } 

####################

//Begin main page

####################

echo("<h4><b>Active Surfer Rewards</b></h4>
");

if(!is_numeric($_GET['promoid'])) {
echo("<p><b>Invalid Promo ID</b></p>");
exit;
}

$r1 = mysql_result(mysql_query("SELECT COUNT(id) FROM `{$prefix}active_surfers` WHERE awarded>0 AND upgrade>0 and promoid=".$_GET['promoid']),0);
$r2 = mysql_result(mysql_query("SELECT COUNT(id) FROM `{$prefix}active_surfers` WHERE awarded>0 AND cash>0 and promoid=".$_GET['promoid']),0);
$r3 = mysql_result(mysql_query("SELECT COUNT(id) FROM `{$prefix}active_surfers` WHERE awarded>0 AND credits>0 and promoid=".$_GET['promoid']),0);
$r4 = mysql_result(mysql_query("SELECT COUNT(id) FROM `{$prefix}active_surfers` WHERE awarded>0 AND bannerimps>0 and promoid=".$_GET['promoid']),0);
$r5 = mysql_result(mysql_query("SELECT COUNT(id) FROM `{$prefix}active_surfers` WHERE awarded>0 AND textimps>0 and promoid=".$_GET['promoid']),0);
$res = mysql_query("SELECT SUM(cash), SUM(credits), SUM(bannerimps), SUM(textimps) FROM `{$prefix}active_surfers` WHERE awarded>0 and promoid=".$_GET['promoid']);
$row = mysql_fetch_row($res);

echo 'Here are the total Active Surfer Rewards to date:
<p>
<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="#F0F0F0"><th>&nbsp;</th><th>Upgrades</th><th width=80>Cash</th><th width=80>Credits</th><th width=80>Banners</th><th width=80>Text Links</th></tr>
<tr align="center"><th>Claims</th><td>'.$r1.'</td><td>'.$r2.'</td><td>'.$r3.'</td><td>'.$r4.'</td><td>'.$r5.'</td></tr>
<tr align="center"><th>Value</th><td>n/a</td><td>$'.($row[0]+0).'</td><td>'.($row[1]+0).'</td><td>'.($row[2]+0).'</td><td>'.($row[3]+0).'</td></tr>
</table>
<br><br>
';


$res = mysql_query("SELECT COUNT(*) FROM `{$prefix}active_surfers` WHERE awarded>0 and promoid=".$_GET['promoid']);
$number = @mysql_result($res,0);
$limit = 50;
$finish = $offset + $limit;
if($finish > $number) { $finish = $number; }

echo 'A total of '.$number.' claims have been awarded.
<br>Showing claims '.($offset+1).' to '.$finish.':<br><br>
<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="#F0F0F0"><th>Award ID</th><th>User ID</th><th>Upgrade</th><th>Cash</th><th>Credits</th><th>Banners</th><th>Text Links</th><th>Claimed</th><th>Awarded</th></tr>
';

$res = mysql_query("SELECT * FROM `{$prefix}active_surfers` WHERE awarded>0 and promoid=".$_GET['promoid']." ORDER BY id DESC LIMIT $offset,$limit");
if(mysql_num_rows($res)) {
   while( $row=mysql_fetch_array($res) ) {
   	$accname = "n/a";
   	if($row[upgrade]>0) { $accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$row[upgrade]"),0); }
	echo '<tr align="center"><td>'.$row[id].'</td><td><a href="admin.php?f=mm&searchfield=Id&searchtext='.$row[usrid].'&sf=browse">'.$row[usrid].'</a></td><td>'.$accname.'</td><td>'.$row[cash].'</td><td>'.$row[credits].'</td><td>'.$row[bannerimps].'</td><td>'.$row[textimps].'</td><td>'.$row[claimed].'</td><td>'.date("r",$row[awarded]).'</td></tr>';
	}
	
	echo '</table>
<p>';
	$pages=intval($number/$limit);
	if ($offset>1) { 
		$prevoffset=$offset-$limit; 
		echo "<a href=\"activesurfers_rewards.php?offset=$prevoffset&promoid=".$_GET['promoid']."\">Prev</a> \n"; 
		} 
	for ($i=1;$i<$pages+2;$i++) { 
		$newoffset=$limit*($i-1);
		if($newoffset==$offset) {
			echo "<b>$i</b> ";
			} else {
			echo "<a href=\"activesurfers_rewards.php?offset=$newoffset&promoid=".$_GET['promoid']."\">$i</a> \n";
			}
		} 
	if ($number>($offset+$limit)) { 
		$nextoffset=$offset+$limit; 
		echo "<a href=\"activesurfers_rewards.php?offset=$nextoffset&promoid=".$_GET['promoid']."\">Next</a><p>\n"; 
		} 

   } else {
   echo '<tr><td colspan=9 align="center">There are no awarded claims in the database</td></tr>
</table>';
   }

echo '
</center>
</body>
</html>';

?>