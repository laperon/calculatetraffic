<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

?>

<html>

<head>
<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<style>
body {
  margin: 0;
  background-color: #FFFFFF;
}
</style>

<body>
<center>

<?

if(isset($_POST["Submit"]) && $_POST["Submit"] == "Add Admin" && strlen($_POST["username"]) > 0 && strlen($_POST["newpassword"]) > 0) {
	
	$checkexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."admin WHERE login='".$_POST["username"]."'"), 0);
	if ($checkexists < 1) {
		$checkexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."admins WHERE username='".$_POST["username"]."'"), 0);
	}
	
	if ($checkexists > 0) {
		echo("<br><div class=\"lfm_title\">Could Not Add Admin</div><br>");
		echo("<br><div class=\"lfm_descr\">Another Admin account with that username already exists.</div><br>");
		echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
		exit;
	}
	
	if (!isset($_POST["ipaddresses"]) || strlen($_POST["ipaddresses"]) < 5) {
		$_POST["ipaddresses"] = "";
	}
	
	// Add Secondary Admin Account
	$passwordhash = lfmMakeAdminPassword($_POST["newpassword"]);
	lfmsql_query("INSERT INTO ".$prefix."admins (username, password, ipaddresses) VALUES ('".$_POST["username"]."', '".$passwordhash."', '".$_POST["ipaddresses"]."')") or die(lfmsql_error());
	$newadminid = lfmsql_insert_id();
	
	// Add Widgets To New Account
	$getwidgets = lfmsql_query("SELECT widgetid, row, col, enabled FROM `".$prefix."widgetpositions` WHERE adminid='".$_SESSION["adminidnum"]."'") or die(lfmsql_error());
	if (lfmsql_num_rows($getwidgets) > 0) {
		for ($i = 0; $i < lfmsql_num_rows($getwidgets); $i++) {
			$widgetid = lfmsql_result($getwidgets, $i, "widgetid");
			$widgetrow = lfmsql_result($getwidgets, $i, "row");
			$widgetcol = lfmsql_result($getwidgets, $i, "col");
			$widgetenabled = lfmsql_result($getwidgets, $i, "enabled");
			lfmsql_query("INSERT INTO ".$prefix."widgetpositions (widgetid, adminid, row, col, enabled) VALUES ('".$widgetid."', '".$newadminid."', '".$widgetrow."', '".$widgetcol."', '".$widgetenabled."')") or die(lfmsql_error());
		}
	}
	
	// Disable All Permissions By Default
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Abuse Reports')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Administrators')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Affiliate Toolbox')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Banned Sites/Emails/IP Addresses')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Commissions')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Custom Fields')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Downloads')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Edit Members')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Login To Member Accounts')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Media Library')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Member Level Settings')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Member Sites/Ads')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Offers and Upsells')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Payment Settings')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Purchases')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Sales Packages')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Surf Settings')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Surf Stats/Logs')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'SurfingGuard')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'System Settings')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Templates')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'Surfbar Icons and Site Theme')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'View Members')") or die(lfmsql_error());
	lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$newadminid."', 'View Payments')") or die(lfmsql_error());
	
	echo("<br><div class=\"lfm_title\">Admin Added Successfully</div><br>");
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;
	
}

?>

<br><div class="lfm_title">Add Admin Account</div>

<form action="addadmin.php" method="post">

<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Username</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="username" type="text" id="username" value="" size="16" /></td>
  </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Password</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="newpassword" type="text" size="16" value="" /></td>
  </tr>

  <tr valign="top">
    <td colspan="2" align="left">
    	<hr>
    	<center><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Authorized IP Addresses</font></strong></center>
    	<?
	echo('<span align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">If you know the IP Address of the Admin who will be using this account, you can enter it below to prevent anyone else from logging into the account.  Leave this box blank if you don\'t want to restrict this account by IP Address.</font></span>');
	echo('<center><br><textarea cols="25" rows="3" name="ipaddresses"></textarea></center>');
	echo('<span align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">You can enter multiple addresses by separating them with commas.</font></span>');
    	?>
    </td>
  </tr>
    
  <tr>
    <td colspan="2" align="center">
      <input type="submit" name="Submit" value="Add Admin" /></td>
    </tr>
</table>
</form>

</body>
</html>
