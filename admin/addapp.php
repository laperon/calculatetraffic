<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php";
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
$errmsg="";

// Update user record and refresh main admin page
if($_POST["Submit"] == "Add Approved Site")
{

	if ($_POST['url'] == "") {
		$errmsg="Please enter a site to pre approve.";
	}

	$checkapproved = mysql_query("Select id from approved_sites where domain='".$_POST['url']."' limit 1");
	if(mysql_num_rows($checkapproved) > 0) {
		$errmsg="This site is already pre approved.";
	}

	if($errmsg == "")
	{

      // Add the new Site
      $qry="INSERT INTO approved_sites (domain) VALUES ('".$_POST['url']."')";

		@mysql_query($qry) or die(mysql_error());
		$msg="<center><font color=\"red\">The site has been approved.</font></center>";

        echo "<script language=\"JavaScript\">";
        echo "window.opener.location.href = window.opener.location.href;";
        echo "</script>";
	}

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<style>
.form {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color=#000000;
}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="memfrm" method="post" action="addapp.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Approve A Site</font></strong></td>
  </tr>
  <tr><td align=left><p><b><br>You can enter a specific URL like http://launchformulamarketing.com/index.html or an entire domain like launchformulamarketing.com</b></p></td></tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL or Domain:</font></strong></td>
    <td align="left"><input name="url" type="text" class="form" id="url" value="<?=$_POST["url"];?>" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" class="form" id="Submit" onClick="javascript:self.close();" value="Cancel" />
    <input name="Submit" type="submit" class="form" value="Add Approved Site" /></td>
  </tr>
</table>
<center>
  <font size="2" color="#FF0000"" face="Arial, Helvetica, sans-serif">
  <?=$errmsg;?>
  <?=$msg;?>
  </font>
</center>
</form>
</body>
</html>
