<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

include "../inc/checkauth.php";

if(!isset($_SESSION["adminid"])) { exit; };

?>

<html>

<head>
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<body>
<center>

<?

if (isset($_POST['Submit']) && $_POST['Submit'] == "Add Field" && is_numeric($_POST['newfieldtype'])) {
	
	// Add New Field/Tab
	$checklastrow = lfmsql_query("Select rank from `".$prefix."customfields` order by rank desc");
	if (lfmsql_num_rows($checklastrow) > 0) {
		$lastrow = lfmsql_result($checklastrow, 0, "rank");
	} else {
		$lastrow = 0;
	}
	$newrank = $lastrow+1;
	
	if ($_POST['newfieldtype'] == "2" && isset($_POST['fieldoptions'])) {
		// Add Options
		$fieldoptions = preg_replace("/((\r(?!\n))|((?<!\r)\n)|(\r\n))/", ",", $_POST['fieldoptions']);
		if(stristr($fieldoptions, ',') === FALSE) {
			echo("<p><b>You must enter more than one option for a Dropdown Options field.</b></p>");
			exit;
		}
	} else {
		$fieldoptions = "None";
	}
	
	if ($_POST['required'] != "1") { $_POST['required'] = 0; }
	if ($_POST['onsignup'] != "1") { $_POST['onsignup'] = 0; }
	if ($_POST['onprofile'] != "1") { $_POST['onprofile'] = 0; }
	if ($_POST['searchable'] != "1") { $_POST['searchable'] = 0; }
	if ($_POST['onipn'] != "1") { $_POST['onipn'] = 0; }
	
	lfmsql_query("INSERT INTO `".$prefix."customfields` (rank, name, type, options, required, onsignup, onprofile, searchable, onipn) VALUES ('".$newrank."', '".$_POST['fieldname']."', '".$_POST['newfieldtype']."', '".$fieldoptions."', '".$_POST['required']."', '".$_POST['onsignup']."', '".$_POST['onprofile']."', '".$_POST['searchable']."', '".$_POST['onipn']."')") or die(lfmsql_error());
	
	echo("<h2><b>Field Added Successfully</b></h2>");
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;	
}

if (isset($_POST['Submit']) && $_POST['Submit'] == "Continue" && $_POST['newfieldtype'] == "1") {
	?>
	<h2><b>Add A Text Input Field</b></h2>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="4">
	<tr>
	<td>
	
	<form action="addfield.php" method="post">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	<input type="hidden" name="newfieldtype" value="<? echo($_POST['newfieldtype']); ?>">
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Field Name: </font> </strong></td>
		<td colspan="2" class="formfield"><input type="text" name="fieldname" value="" maxlength="25"></td>
		<td align="center"><a href="#" title="The name label shown on any forms containing this field."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Required: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="required"><option value="0" selected="selected">No</option><option value="1">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, the member will be required to enter a value for this field.  If no, it can be left blank."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show On Signup: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="onsignup"><option value="0">No</option><option value="1" selected="selected">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added to the signup page form."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show On Profile: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="onprofile"><option value="0">No</option><option value="1" selected="selected">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added to the profile page form."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Searchable: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="searchable"><option value="0" selected="selected">No</option><option value="1">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added as a search option on the View Members admin page."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">In Sales Packages: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="onipn"><option value="0" selected="selected">No</option><option value="1">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added as an option to add to your Sales Packages."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>

	
	<tr><td align="center" colspan="2"><INPUT type="submit" name="Submit" value="Add Field"></td></tr>

	</table>
	</form>
	
	</td>
	</tr>
	</table>
		
	<?php
	exit;
}

if (isset($_POST['Submit']) && $_POST['Submit'] == "Continue" && $_POST['newfieldtype'] == "2") {
	?>
	<h2><b>Add A Dropdown Options Field</b></h2>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="4">
	<tr>
	<td>
	
	<form action="addfield.php" method="post">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	<input type="hidden" name="newfieldtype" value="<? echo($_POST['newfieldtype']); ?>">
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Field Name: </font> </strong></td>
		<td colspan="2" class="formfield"><input type="text" name="fieldname" value="" maxlength="25"></td>
		<td align="center"><a href="#" title="The name label shown on any forms containing this field."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Selectable Options: </font> </strong></td>
		<td colspan="2" class="formfield"><textarea name="fieldoptions" cols=30 rows=4></textarea></td>
		<td align="center"><a href="#" title="Enter the options users can choose from. Separate them by commas or returns."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Required: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="required"><option value="0" selected="selected">No</option><option value="1">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, the member will be required to enter a value for this field.  If no, it can be left blank."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show On Signup: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="onsignup"><option value="0">No</option><option value="1" selected="selected">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added to the signup page form."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show On Profile: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="onprofile"><option value="0">No</option><option value="1" selected="selected">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added to the profile page form."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Searchable: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="searchable"><option value="0" selected="selected">No</option><option value="1">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added as a search option on the View Members admin page."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>
	
	<tr>
	        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">In Sales Packages: </font> </strong></td>
		<td colspan="2" class="formfield"><select name="onipn"><option value="0" selected="selected">No</option><option value="1">Yes</option></select></td>
		<td align="center"><a href="#" title="If yes, this field will be added as an option to add to your Sales Packages."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
	</tr>

	
	<tr><td align="center" colspan="2"><INPUT type="submit" name="Submit" value="Add Field"></td></tr>

	</table>
	</form>
	
	</td>
	</tr>
	</table>
		
	<?php
	exit;
}

####################

//Begin main page

####################

?>

	<h2><b>Add A Field</b></h2>
	
	<table width="500" cellpadding="5" cellspacing="0" border="0">
	<tr><td colspan="2">
	<form action="addfield.php" method="post">
	<p>Please select the type of field you want to create.</p>
	<INPUT type="radio" name="newfieldtype" value="1" CHECKED>Text Input - A standard field for users to enter text.<br><br>
	<INPUT type="radio" name="newfieldtype" value="2">Dropdown Options - A dropdown list with options for users to choose from.<br><br>
	<center>
	<INPUT type="submit" name="Submit" value="Continue">
	</center>
	</form>
	</td></tr>
	
	</table>

<br><br>

</center>
</body>
</html>

<?php
exit;
?>