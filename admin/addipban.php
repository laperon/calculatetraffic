<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php";
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
$errmsg="";

// Update user record and refresh main admin page
if($_POST["Submit"] == "Add Banned Ip Address")
{

	if ($_POST['bipadd'] == "") {
		$errmsg="Please enter an ip address to ban.";
	} else {
	
	
	$testip = str_replace("*", "", $_POST['bipadd']);
	$testip = str_replace(".", "", $testip);
	if (!is_numeric($testip)) {
		$errmsg="This ip address is not valid.";
	}
	
	if(strpos($_POST['bipadd'], ".") === false) {
		$errmsg="This ip address is not valid.";
	}

	$checkbanned = mysql_query("Select id from ".$prefix."banips where banned='".$_POST['bipadd']."' limit 1");
	if(mysql_num_rows($checkbanned) > 0) {
		$errmsg="This ip address is already banned.";
	}
	
	}

	if($errmsg == "")
	{

      // Add the new Ip Address
      $qry="INSERT INTO ".$prefix."banips (banned) VALUES ('".$_POST['bipadd']."')";

		@mysql_query($qry) or die(mysql_error());
		@mysql_query("Update ".$prefix."members set status='Suspended' where lastip LIKE '%".$_POST['bipadd']."%' OR signupip LIKE '%".$_POST['bipadd']."%'");
		$msg="<center><font color=\"red\">The ip address has been banned.</font></center>";

        echo "<script language=\"JavaScript\">";
        echo "window.opener.location.href = window.opener.location.href;";
        echo "</script>";
	}

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<style>
.form {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color=#000000;
}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="memfrm" method="post" action="addipban.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Ban An Ip Address</font></strong></td>
  </tr>
  <tr><td align=left><p><b><br>You can enter a specific IP address like 192.168.0.1 or a range like 192.168.*.*</b></p></td></tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ip Address:</font></strong></td>
    <td align="left"><input name="bipadd" type="text" class="form" id="bipadd" value="<?=$_POST["bipadd"];?>" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" class="form" id="Submit" onClick="javascript:self.close();" value="Cancel" />
    <input name="Submit" type="submit" class="form" value="Add Banned Ip Address" /></td>
  </tr>
</table>
<center>
  <font size="2" color="#FF0000"" face="Arial, Helvetica, sans-serif">
  <?=$errmsg;?>
  <?=$msg;?>
  </font>
</center>
</form>
</body>
</html>
