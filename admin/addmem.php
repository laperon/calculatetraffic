<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php";
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
$errmsg="";

// Update user record and refresh main admin page
if($_POST["Submit"] == "Add Member")
{
	// Validate Firstname
	if(!isset($_REQUEST["firstname"]) || strlen(trim($_REQUEST["firstname"])) < 2)
	{
		$errmsg="First Name must be at least 2 characters<br>";
	}
	// Validate Lastname
	if(!isset($_REQUEST["lastname"]) || strlen(trim($_REQUEST["lastname"])) < 2)
	{
		$errmsg.="Last Name must be at least 2 characters<br>";
	}
	// Validate Email
	if(!isset($_REQUEST["email"]) || strlen(trim($_REQUEST["email"])) < 2)
	{
		$errmsg.="Email address must be set<br>";
	}
	// Validate Username
	if(!isset($_REQUEST["username"]) || strlen(trim($_REQUEST["username"])) < 2)
	{
		$errmsg.="Username must be at least 2 characters<br>";
	}
	// Validate Password
	if(!isset($_REQUEST["password"]) || strlen(trim($_REQUEST["password"])) < 4)
	{
		$errmsg.="Password must be at least 4 characters<br>";
	}

	if($errmsg == "")
	{
		// Get ID of Everyone group
		$qry="SELECT groupid FROM ".$prefix."groups WHERE groupname = 'Everyone'";
		$grpres=@mysql_query($qry) or die(mysql_error());
		$grprow=@mysql_fetch_array($grpres);
		$eogroup=$grprow["groupid"];

      // Add the new member
      $enc_password=md5($_REQUEST["password"]);
      $qry="INSERT INTO ".$prefix."members(firstname,lastname,email,address,city,state,postcode,country,telephone,username,password,refid,geo,paypal_email,joindate,mtype,groupid,status)
      VALUES('".$_REQUEST["firstname"]."','".$_REQUEST["lastname"]."','".$_REQUEST["email"]."','".$_REQUEST["address"]."','".$_REQUEST["city"]."','".$_REQUEST["state"]."','".$_REQUEST["postcode"]."','".$_REQUEST["country"]."','".$_REQUEST["telephone"]."','".$_REQUEST["username"]."','".$enc_password."','".$_REQUEST["rid"]."','".$_REQUEST["geo"]."','".$_REQUEST["paypal_email"]."',NOW(),'".$_REQUEST["mtype"]."','$eogroup','Active')";

		@mysql_query($qry) or die(mysql_error());
		$msg="<center><font color=\"red\">New member added!</font></center>";

        echo "<script language=\"JavaScript\">";
        echo "window.opener.location.href = window.opener.location.href;";
        echo "</script>";
	}

}

	// Get current member details
	$res=@mysql_query("SELECT MAX(Id) AS maxid FROM ".$prefix."members");
	$mm=@mysql_fetch_object($res);

	$qry="SELECT * FROM ".$prefix."members WHERE Id=$mm->maxid";
	$mres=@mysql_query($qry) or die(mysql_error());
	$mrow=@mysql_fetch_array($mres);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<style>
.form {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color=#000000;
}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="memfrm" method="post" action="addmem.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Add New Member</font></strong></td>
  </tr>
  <tr>
    <td width="30" align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">First Name:</font></strong></td>
    <td align="left"><input name="firstname" type="text" class="form" id="firstname" value="<?=$_REQUEST["firstname"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Last Name:</font></strong></td>
    <td align="left"><input name="lastname" type="text" class="form" id="lastname" value="<?=$_REQUEST["lastname"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Email Address: </font></strong></td>
    <td align="left"><input name="email" type="text" class="form" id="email" value="<?=$_REQUEST["email"];?>" size="30" /></td>
  </tr>

  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Address:</font></strong></td>
    <td align="left"><input name="address" type="text" class="form" id="address" value="<?=$_REQUEST["address"];?>" size="30" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">City:</font></strong></td>
    <td align="left"><input name="city" type="text" class="form" id="city" value="<?=$_REQUEST["city"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">State/County:</font></strong></td>
    <td align="left"><input name="state" type="text" class="form" id="state" value="<?=$_REQUEST["state"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Zip/Postcode:</font></strong></td>
    <td align="left"><input name="postcode" type="text" class="form" id="postcode" value="<?=$_REQUEST["postcode"];?>" size="12" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Country:</font></strong></td>
    <td align="left"><select name="country" class="form"><? echo select_country($mrow["country"]); ?></select></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Telephone:</font></strong></td>
    <td align="left"><input name="telephone" type="text" class="form" id="telephone" value="<?=$_REQUEST["telephone"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Username:</font></strong></td>
    <td align="left"><input name="username" type="text" class="form" id="username" value="<?=$_REQUEST["username"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Password:</font></strong></td>
    <td align="left"><input name="password" type="text" class="form" id="password" value="<?=$_REQUEST["password"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Status:</font></strong></td>
    <td align="left">          <select name="status" class="form">
		  <? if($mrow["status"] == "Active") { ?>
		  <option value="Active">Active</option>
		  <option value="Suspended">Suspended</option>
		  <? } else { ?>
		  <option value="Active" selected="selected">Active</option>
		  <option value="Suspended">Suspended</option>
		  <? } ?>
            </select>    </td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Paypal Email: </font></strong></td>
    <td align="left"><input name="paypal_email" type="text" class="form" id="paypal_email" value="<?=$_REQUEST["paypal_email"];?>" size="30" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Type : </font></strong></td>
    <td align="left"><select name="mtype" class="form">
<? 		GetMtype($mrow["mtype"]);
		if($mrow["mtype"] == 0){ ?>
		<option value="0" selected=selected>Pending</option>
<?
	}
?>
	</select>    </td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Group: </font></strong></td>
    <td align="left"><select name="groupid" class="form">
    <? GetGroup($mrow["groupid"]); ?>
    </select>    </td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Upline:</font></strong></td>
    <td align="left"><input name="refid" type="text" class="form" id="refid" value="<?=$_REQUEST["refid"];?>" size="4" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">OTO View: </font></strong></td>
    <td align="left"><input name="otoview" type="text" class="form" id="otoview" value="<?=$_REQUEST["otoview"];?>" size="4" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Mail Bounce: </font></strong></td>
    <td align="left"><input name="mailbounce" type="text" class="form" value="<?=$_REQUEST["mailbounce"];?>" size="4" /></td>
  </tr>

  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" class="form" id="Submit" onClick="javascript:self.close();" value="Cancel" />
    <input name="Submit" type="submit" class="form" value="Add Member" /></td>
  </tr>
</table>
<center>
  <font size="2" color="#FF0000"" face="Arial, Helvetica, sans-serif">
  <?=$errmsg;?>
  <?=$msg;?>
  </font>
</center>
</form>
</body>
</html>
