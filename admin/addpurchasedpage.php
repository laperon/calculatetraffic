<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";


include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

// Update promo table
if($_POST["Submit"] == "Add Page")
{
	$userid = $_POST["userid"];
	$pageid = $_POST["pageid"];
	
	if (!is_numeric($userid)) {
		echo("User ID must be a number.");
		exit;
	}
	
	if (!is_numeric($pageid)) {
		$getpagenum = lfmsql_query("SELECT pageid FROM ".$prefix."memberpages WHERE pagetag='".$pageid."'") or die(lfmsql_error());
		if (lfmsql_num_rows($getpagenum) < 1) {
			echo("Page tag not found");
			exit;
		} else {
			$pageid = lfmsql_result($getpagenum, 0, "pageid");
		}
	}
	
	if ($pageid < 1) {
		echo("Invalid Page");
		exit;
	}
	
	$qry="INSERT INTO ".$prefix."purchasedpages (userid, pageid, ipntransid) VALUES('$userid', '$pageid', '0')";
	@lfmsql_query($qry) or die("Unable to add page: ".lfmsql_error());

	$msg="<center><strong>Page Added</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Add Page</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?
	if($_POST["Submit"] == "Add Page")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">Page Added</p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="salefrm" method="post" action="addpurchasedpage.php">
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Add Purchased Page</font> </strong></td>
    </tr>
  <tr>
    <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>User ID</strong></font></td>
    <td align="left" nowrap="nowrap"><input type="text" name="userid" id="userid" value="0"></td>
    </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Page</font></strong></td>
    <td align="left" nowrap="nowrap"><select name="pageid">
    <?
    $getpagelist = lfmsql_query("SELECT * FROM ".$prefix."memberpages ORDER BY pageindex") or die(lfmsql_error());
        echo("<option value=\"0\">--Select A Page--</option>");
	while($menu=@lfmsql_fetch_object($getpagelist)) {
		echo("<option value=\"".$menu->pageid."\">".$menu->pagename."</option>");
	}
    ?>
    </select></td>
    </tr>
  
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Add Page" /></td>
    </tr>
</table>
</form>
<? 
}
?>
</body>
</html>