<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php";
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
$errmsg="";

// Update user record and refresh main admin page
if($_POST["Submit"] == "Add Site")
{
	// Validate Memid
	$memid = $_REQUEST["memid"];
	if(!is_numeric($memid)) {
		
		$getuserid = mysql_query("Select Id from ".$prefix."members where username='".$memid."' or email='".$memid."' limit 1");
		
		if (mysql_num_rows($getuserid) > 0) {
			$memid = mysql_result($getuserid, 0, "Id");
		} else {
			$errmsg = "User Not Found";
		}
	}

	if($errmsg == "")
	{

      // Add the new Site
      $qry="INSERT INTO ".$prefix."msites (memid, url, credits, state) VALUES ($memid, '".$_POST['url']."', ".$_POST['credits'].", 1)";

		@mysql_query($qry) or die(mysql_error());
		@mysql_query("Update ".$prefix."members set actsite=1 where Id=$memid limit 1");
		$msg="<center><font color=\"red\">New site added!</font></center>";

        echo "<script language=\"JavaScript\">";
        echo "window.opener.location.href = window.opener.location.href;";
        echo "</script>";
	}

}

	// Get current site details
	$res=@mysql_query("SELECT MAX(id) AS maxid FROM ".$prefix."msites");
	$mm=@mysql_fetch_object($res);

	$qry="SELECT * FROM ".$prefix."msites WHERE id=$mm->maxid";
	$mres=@mysql_query($qry) or die(mysql_error());
	$mrow=@mysql_fetch_array($mres);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<style>
.form {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color=#000000;
}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="memfrm" method="post" action="addsite.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Add New Site</font></strong></td>
  </tr>
  <tr>
    <td width="30" align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member:</font></strong></td>
    <td align="left"><input name="memid" type="text" class="form" id="memid" value="<?=$_REQUEST["memid"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL:</font></strong></td>
    <td align="left"><input name="url" type="text" class="form" id="url" value="<?=$_REQUEST["url"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Hits Assigned: </font></strong></td>
    <td align="left"><input name="credits" type="text" class="form" id="credits" value="<?=$_REQUEST["credits"];?>" size="30" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" class="form" id="Submit" onClick="javascript:self.close();" value="Cancel" />
    <input name="Submit" type="submit" class="form" value="Add Site" /></td>
  </tr>
</table>
<center>
  <font size="2" color="#FF0000"" face="Arial, Helvetica, sans-serif">
  <?=$errmsg;?>
  <?=$msg;?>
  </font>
</center>
</form>
</body>
</html>
