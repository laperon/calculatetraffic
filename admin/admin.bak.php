<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.32
// Copyright ©2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/bandj/functions.php";

include "../inc/checkauth.php";

if(!isset($_SESSION["adminid"])) { exit; };

ini_set('session.gc_maxlifetime', 3600);

// Redirect to member stats
if($_POST["Submit"] == "Show Stats Of Selected" && $_GET["f"] == "mm") {
	$_SESSION["newgetid"] = $_POST["memcheck"];
	header("Location: admin.php?f=mstats&sf=browse&genstats=go&statstype=1");
}

// Get our settings
$res = lfmsql_query("SELECT sitename, affurl FROM ".$prefix."settings") or die("Unable to find settings!");
$row = lfmsql_fetch_array($res);
$sitename = $row["sitename"];
$affurl = $row["affurl"];

    if(isset($_POST["CreateFile"]))
	{
		// Get the max salesid and create the array of members to be paid
		$msalesid=$_POST["msalesid"];
		$msaledate=$_POST["msaledate"];
		$members_to_pay=$_POST["pay_id"];
		$min_comm=get_setting("min_comm");
		
		$rcnt=0;
		foreach ($members_to_pay as $key => $mid){
		
			if($mid >0)
			{
				// Build our list of members and their payments
				$dqry="SELECT Id FROM ".$prefix."members WHERE Id=".$mid;
				$dres=@lfmsql_query($dqry) or die(lfmsql_error());
				$drow=@lfmsql_fetch_array($dres) or die(lfmsql_error());
				$id=$drow["Id"];
				$sqry="SELECT SUM(".$prefix."sales.commission) AS payout,firstname,lastname,paypal_email FROM ".$prefix."sales LEFT JOIN ".$prefix."members ON ".$prefix."members.Id=".$prefix."sales.affid WHERE ".$prefix."sales.status IS NULL AND affid=$id AND ".$prefix."sales.commission != 0 AND (saledate <= '$msaledate 23:59:59' OR ".$prefix."sales.commission < 0) GROUP BY affid";
				$sres=@lfmsql_query($sqry);

				// Create data for the mass payment file
				while($srow=@lfmsql_fetch_array($sres))
				{
					if($srow["payout"] >= $min_comm)
					{
						$data.=$srow["paypal_email"]."\t".$srow["payout"]."\tUSD"."\r\n";
						$rcnt++;
						// Update status on sales for this person
						@lfmsql_query("UPDATE ".$prefix."sales SET status='P' WHERE affid = $id AND (saledate <= '$msaledate 23:59:59' OR commission < 0)") or die(lfmsql_error());
					}
				}
			}
		}

        // Data is created .. now write this to the file
        if($rcnt > 0)
        {
            $name="pay-".date("Y-m-d").".txt";
            header("Content-type: application/txt");
            header("Content-length: ".strlen($data)."");
            header("Content-Disposition: attachment; filename=$name");
            header("Content-Description: PHP Generated Data");
            echo $data;

            // Get the admin's email address
            $res=@lfmsql_query("SELECT email FROM ".$prefix."admin");
            $row=@lfmsql_fetch_array($res);
            $adminemail=$row["email"];

            // Send an email to admin with payment details
            $subject = $sitename.' Payment data - '.date("d-M-Y");
            $headers = "From: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Return-Path: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
            $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
            mail($adminemail,$subject,$data,$headers);
			exit;
        }

	}

    if(isset($_POST["CreateFileA"]))
    {
        // Get the max salesid and create the array of members to be paid
        $msalesid=$_POST["msalesid"];
		$msaledate=$_POST["msaledate"];
		$min_comm=get_setting("min_comm");


		//new, get array of checked members from the pay all file
		$members_to_pay = $_POST['pay_id'];
        $rcnt=0;

		foreach ($members_to_pay as $key => $mid){

        // Build our list of members and their payments
        $sqry="SELECT SUM(".$prefix."sales.commission) AS payout,Id,firstname,lastname,paypal_email FROM ".$prefix."sales LEFT JOIN ".$prefix."members ON ".$prefix."members.Id=".$prefix."sales.affid WHERE (".$prefix."sales.status IS NULL OR ".$prefix."sales.status = 'R' )  AND ".$prefix."sales.commission != 0 AND (saledate <= '$msaledate 23:59:59' OR ".$prefix."sales.commission < 0) AND Id = '".$mid."' GROUP BY affid";
        $sres=@lfmsql_query($sqry);

        // Create data for the mass payment file
        while($srow=@lfmsql_fetch_array($sres))
        {
			if($srow["payout"] >= $min_comm)
			{
	            $data.=$srow["paypal_email"]."\t".$srow["payout"]."\tUSD"."\r\n";
    	        $rcnt++;
				// Update status on sales for this person
				@lfmsql_query("UPDATE ".$prefix."sales SET status='P' WHERE affid=".$srow["Id"]." AND (saledate <= '$msaledate 23:59:59' OR commission < 0)") or die(lfmsql_error());
			}
        }
		}
        // Data is created .. now write this to the file
        if($rcnt > 0)
        {
            $name="pay-".date("Y-m-d").".txt";
            header("Content-type: application/txt");
            header("Content-length: ".strlen($data)."");
            header("Content-Disposition: attachment; filename=$name");
            header("Content-Description: PHP Generated Data");
            echo $data;

            // Get the admin's email address
            $res=@lfmsql_query("SELECT email FROM ".$prefix."admin");
            $row=@lfmsql_fetch_array($res);
            $adminemail=$row["email"];

            // Send an email to admin with payment details
            $subject = $sitename.' Payment data - '.date("d-M-Y");
            $headers = "From: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Return-Path: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
            $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
            mail($adminemail,$subject,$data,$headers);
			exit;
        }
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title><?=$sitename;?> Administration</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var featureList=["calendar"];
function init() {
	var returnedDate = function(date) { document.getElementById('publishdate').value=date[0] + "-" + date[1] + "-" + date[2]; }
	var calendar = new OAT.Calendar(true);

	var showCallback = function(event) {
		calendar.show(event.clientX, event.clientY, returnedDate);
}

	OAT.Dom.attach("mybutton","click",showCallback);
}
</script>
<script type="text/javascript" src="datepickercontrol/datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol/datepickercontrol.css">
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
<script type="text/javascript">
var timeout    = 500;
var closetimer = 0;
var ddmenuitem = 0;

function jsddm_open()
{  jsddm_canceltimer();
   jsddm_close();
   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}

function jsddm_close()
{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}

function jsddm_timer()
{  closetimer = window.setTimeout(jsddm_close, timeout);}

function jsddm_canceltimer()
{  if(closetimer)
   {  window.clearTimeout(closetimer);
      closetimer = null;}}

$(document).ready(function()
{  $('#jsddm > li').bind('mouseover', jsddm_open)
   $('#jsddm > li').bind('mouseout',  jsddm_timer)});

document.onclick = jsddm_close;

</script>
<style type="text/css">
#jsddm {margin: 0;
        padding: 0}
#jsddm li
{float: left;
list-style: none;
font: 11px Tahoma, arial}
#jsddm li a
{display: block;
background: #20548E;
padding: 5px 12px;
text-decoration: none;
border-right: 1px solid white;
border-bottom: 1px solid white;
width: 70px;
color: #EAFFED;
white-space: nowrap}
#jsddm li a:hover
{background: #1A4473}
#jsddm li ul
{margin: 0;
padding: 0;
position: absolute;
visibility: hidden;
border-top: 1px solid white}
#jsddm li ul li
{float: none;
display: inline}
#jsddm li ul li a
{width: auto;
background: #9F1B1B}
#jsddm li ul li a:hover
{background: #7F1616}
</style>
</head>
<body>

<script src="style_files/js/jquery.min.js"></script>
<script src="style_files/js/bootstrap.min.js"></script>

<!-- Begin Header -->
<div class="lfm_admin_header"><?=$sitename;?> Admin</div>
<!-- End Header -->

<!-- Begin Navigation Menu -->

                <div class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
			    </button>
                        </div>
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                            
<?
	// Generate the admin menu
	// Get the top level items and then query their child items
	$menures=@lfmsql_query("SELECT * FROM ".$prefix."adminmenu WHERE menu_parent=0 ORDER BY menu_order, id");
	while($amenu=@lfmsql_fetch_object($menures)) {
		// Display top level item
		
		// Now get any submenu items
		$smcountres=@lfmsql_query("SELECT COUNT(*) FROM ".$prefix."adminmenu WHERE menu_parent=".$amenu->id);
		$smcount=@lfmsql_result($smcountres,0);
		if($smcount > 0) {
			// This is a category
			echo('<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$amenu->menu_label.'<b class="caret"></b></a>');
				echo('<ul class="dropdown-menu">');
				// Get sub items
				$smenures=@lfmsql_query("SELECT * FROM ".$prefix."adminmenu WHERE menu_parent=".$amenu->id." ORDER BY menu_order");
				while($asubmenu=@lfmsql_fetch_object($smenures)) { 
					echo('<li><a href="'.$asubmenu->menu_url.'" target="'.$asubmenu->menu_target.'">'.$asubmenu->menu_label.'</a></li>');
				}
				echo('</ul>');
			echo('</li>');
		} else {
			// This is a menu link
			echo('<li><a href="'.$amenu->menu_url.'" target="'.$amenu->menu_target.'">'.$amenu->menu_label.'</a></li>');
		}
	}
?>
                            
                            </ul>
                        </div><!-- /.nav-collapse -->
                </div><!-- /.navbar -->


<!-- End Navigation Menu -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>

          <?

        if(!isset($_GET["f"]))
        {

    // Get some stats
    $mres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members");
    $mrow=@lfmsql_fetch_array($mres);
    $sres=@lfmsql_query("SELECT sum(itemamount) as tsales FROM ".$prefix."sales where salestier=1");
    $srow=@lfmsql_fetch_array($sres);
    $rres=@lfmsql_query("SELECT sum(itemamount) as trefunds FROM ".$prefix."sales where salestier=1 and status = 'R'");
    $rrow=@lfmsql_fetch_array($rres);
    $pres=@lfmsql_query("SELECT sum(commission) as tcommspaid FROM ".$prefix."sales WHERE status = 'P'");
    $prow=@lfmsql_fetch_array($pres);
    $cres=@lfmsql_query("SELECT sum(commission) as tcomms FROM ".$prefix."sales WHERE status IS NULL");
    $crow=@lfmsql_fetch_array($cres);
    
    // Count members who are verified and subscribed
    $mactres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Active' AND newsletter=1");
    $mactrow=@lfmsql_fetch_array($mactres);
    
    // Count members who are not yet verified
    $munvres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Unverified'");
    $munvrow=@lfmsql_fetch_array($munvres);
    
     // Count members who were verified but have unsubscribed
    $munsres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Active' AND newsletter=0");
    $munsrow=@lfmsql_fetch_array($munsres);
    
     // Count suspended members
    $msusres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Suspended'");
    $msusrow=@lfmsql_fetch_array($msusres);
    
    // Count members who have a bouncing email address
    $mbounceres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Active' AND bouncemode=3");
    $mbouncerow=@lfmsql_fetch_array($mbounceres);

    $profit = $srow["tsales"]-($rrow["trefunds"]+$prow["tcommspaid"]+$crow["tcomms"]);

?>

<center>

<table width="800" border=0 cellpadding=10>
<tr><td align=left valign=top>

<div align="center">

<!-- Start Members Dashboard -->
<div class="lfm_infobox" style="width: 400px;">
<table width="400" border="0" cellpadding="1">
  <tr>
    <td colspan="2" align="center"><div class="lfm_infobox_heading">Member Dashboard</div><br></td>
  </tr>

    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Total Members (<a target="_self" href="admin.php?f=mm&sf=browse">View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$mrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>

    <tr>
      <td align="right" class="lfm_infobox_item">Subscribed Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=subscribed">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$mactrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Unverified Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=unverified">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$munvrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Suspended Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=suspended">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$msusrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Unsubscribed Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=unsubscribed">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$munsrow["mcount"];?></td>
    </tr>
    
    <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
    
    <tr>
      <td align="right" class="lfm_infobox_item">Bounced Mail Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=bounced">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$mbouncerow["mcount"];?></td>
    </tr>

</table>
</div>
<!-- End Members Dashboard -->

<br><br>

<!-- Start Sales Dashboard -->
<div class="lfm_infobox" style="width: 400px;">
<table width="400" border="0" cellpadding="1">
  <tr>
    <td colspan="2" align="center"><div class="lfm_infobox_heading">Sales Dashboard</div><br></td>
  </tr>

    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Total Sales: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=number_format($srow["tsales"],2);?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Total Refunds: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($rrow["trefunds"],2);?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Comm. Paid: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($prow["tcommspaid"],2);?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Comm. Owing: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($crow["tcomms"],2);?></td>
    </tr>
    
    <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
    
    <tr>
      <td align="right" class="lfm_infobox_item">Profit: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($profit,2);?></td>
    </tr>

</table>
</div>
<!-- End Sales Dashboard -->

<br><br>

<!-- Start Sales Leaderboard -->
<div class="lfm_infobox" style="width: 400px;">
<form method="post" action="admin.php">
  <table border="0" cellpadding="0" cellspacing="0">
  
  <tr>
    <td colspan="4" align="center"><div class="lfm_infobox_heading">Affiliate Leaderboard</div><br><br></td>
  </tr>
  
    <tr>
      <td colspan="4" align="center" nowrap="nowrap"><table width="100%" border="0" class="lfmtable">
<?    if(isset($_POST["sdate"]) && isset($_POST["edate"]))
    {
?>
        <tr>
          <td nowrap="nowrap">From:
            <input name="sdate" type id="DPC_sdate_YYYY-MM-DD" value="<?=$_POST['sdate'];?>" width="80" text>            </td>
          <td nowrap="nowrap">To:
            <input name="edate" type id="DPC_edate_YYYY-MM-DD" value="<?=$_POST['edate'];?>" width="80" text>            </td>
        </tr>
<?
    }
    else
    {
?>
        <tr>
          <td nowrap="nowrap">From:
            <input name="sdate" type="text" id="DPC_sdate_YYYY-MM-DD" value="<?=date("Y-m-d",mktime(0,0,0,date("m")-1,date("d"),date("Y")));?>" width="80">            </td>
          <td nowrap="nowrap">To:
            <input name="edate" type="text" id="DPC_edate_YYYY-MM-DD" value="<?=date("Y-m-d");?>" width="80">            </td>
        </tr>

<?
    }
    
    if (is_numeric($_POST['showtop']) && $_POST['showtop'] > 0) {
    	$showtop = $_POST['showtop'];
    } else {
    	$showtop = 10;
    }
    
?>
        <tr>
          <td colspan="2" align="center">Show the top <input name="showtop" type="text" size="3" value="<?=$showtop?>"> sellers.
          </td>
        </tr>
        
      </table></td>
      </tr>
    <tr>
      <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" id="Submit" value="Update Leaderboard" /></td>
      </tr>
    <tr class="button">
      <td align="center" nowrap="nowrap"><strong>#</strong></td>
      <td nowrap="nowrap"><strong>Name</strong></td>
      <td align="center" nowrap="nowrap"><strong>Number of  Sales </strong></td>
      <td align="center" nowrap="nowrap"><strong> Commission </strong></td>
      </tr>
<?
// Get sales data for all affiliates
if(isset($_POST["sdate"]))
{
    $sdate = $_POST["sdate"]." 00:00:00";
    $edate = $_POST["edate"]." 23:59:59";
}
else
{
    $sdate = date("Y-m-d",mktime(0,0,0,date("m")-1,date("d"),date("Y")))." 00:00:00";
    $edate = date("Y-m-d",mktime(23,59,59,date("m"),date("d"),date("Y")))." 23:59:59";
}
$lbqry="SELECT firstname,lastname,affid,count(*) as salecount,sum(s.commission) as comm FROM ".$prefix."sales s LEFT JOIN ".$prefix."members m on m.id=s.affid WHERE saledate > '$sdate' AND saledate < '$edate' AND prize=0 GROUP BY affid ORDER BY comm DESC LIMIT ".$showtop;
$lbres=@lfmsql_query($lbqry);
$affpos=0;                    // Position in the leaderboard
$totalsales=0;                // Overall total sales ($)
$totalnumsales=0;            // Overall total number of sales

while($lbrow=@lfmsql_fetch_array($lbres))
{
    $totalsales=$totalsales+$lbrow["comm"];
    $totalnumsales=$totalnumsales+$lbrow["salecount"];
    $affpos=$affpos+1;
	$affid=$lbrow["affid"];
?>
    <tr>
      <td align="center"><strong><?=$affpos;?></strong></td>
      <td><?=$lbrow["firstname"];?>&nbsp;<?=$lbrow["lastname"];?></td>
      <td align="center"><?=$lbrow["salecount"];?></td>
      <td align="center"><?=$lbrow["comm"];?></td>
      </tr>
<?
}
?>
    <tr>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap"><strong>Total Sales: <?=$totalnumsales;?></strong></td>
      <td nowrap="nowrap"><strong>Total Commissions: $<?=$totalsales;?></strong></td>
      </tr>
  </table>
</form>
</div>
<!-- End Sales Leaderboard -->

<?

$sres=@lfmsql_query("SELECT splittest FROM ".$prefix."settings");
$srow=@lfmsql_fetch_array($sres);
$splittest = $srow["splittest"];

if ($splittest > 1) {

?>

<br><br>

<!-- Start Split Test Stats -->
<div class="lfm_infobox" style="width: 400px;">
<table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
	
	<tr>
		<td colspan="4" align="center"><div class="lfm_infobox_heading">Split Test Stats</div><br><br></td>
	</tr>
	
        <tr>
          <td align="left" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Sales Page </font></strong></td>
          <td align="center" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Hits</font></strong></td>
          <td align="center" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Joins</font></strong></td>
          <td align="center" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">C/R</font></strong></td>
        </tr>
<?
	$sph=@lfmsql_query("SELECT count(salespage) AS salescount, salespage FROM ".$prefix."splithits GROUP BY salespage ORDER BY salespage");
	while($splithits=@lfmsql_fetch_object($sph))
	{
		// Get joins for this page
		$spr=@lfmsql_query("SELECT count(salespage) as salescount FROM ".$prefix."splitresults WHERE salespage='$splithits->salespage'");

		$splitresult=@lfmsql_fetch_object($spr);
		$crate=($splitresult->salescount/$splithits->salescount)*100;
		$crpc=number_format($crate,2);
?>
	     <tr>
          <td align="left"><?=$splithits->salespage;?></td>
          <td align="center"><?=$splithits->salescount;?></td>
          <td align="center"><?=$splitresult->salescount;?></td>
          <td align="center"><?=$crpc;?>%</td>
        </tr>
<? } ?>
      </table>
</div>
<!-- End Split Test Stats -->

<?
}
?>

</td>

<td align=right valign=top>

<?

//Traffic Exchange Dashboard
include "tedash.php";

?>

</td></tr>
</table>
<br>
  <p>
    <input name="Submit" type="button" class="footer-text" onClick="javascript:viewLog()" value="View Log" />
    </p>
</p>

  <div class="lfm_descr"><? echo(get_script_type());?> v<? echo(get_script_vers());?></div>
  <div class="lfm_descr"><a target="_blank" href="http://thetrafficexchangescript.com/updatecheck.php?ver=<? echo(get_script_vers());?>">Check For Updates</a></div>
  </center>
  <?       // Check if install.php has been deleted
        if((file_exists("install/install_check.php")) && !isset($_GET["f"]))
        {
        ?>
  <br />
  <br />
</div>
<center><font color="#FF0000"><strong>NOTE: THE FILE INSTALL/INSTALL_CHECK.PHP SHOULD BE DELETED.</strong></font></center><br>
          <?
        }

}
        // Include the appropriate page depending on menu selection in admin area
		
		//check if its one of the files that overide the f variable
		if ( isset($_GET["f"]) & !isset($_POST["EmailSelected"]) & !isset($_POST["EmailMT"]) & !isset($_POST["EmailAll"]) & !isset($_POST["PayAll"]) & !isset($_POST["GivePrize"]) & !isset($_POST["DeductCommissions"]) & !isset($_POST["CreateFile"]) & !isset($_POST["PaySelected"])  & !isset($_POST["EmailGeo"]) & !isset($_POST["EmailGrp"]) ) {
			
		
			//do the includes that aren't in the menu	
			switch ($_GET["f"]){
				case "ps":
					include "payselected.php";
					break;
				case "pa":
					include "payall.php";
					break;
				case "upd":
					include "updater.php";
					break;
				case "sm":
					include "sendemail.php";
					break;
				case "smr":
					include "sendemailregion.php";
					break;
				case "smg":
					include "sendemailgroup.php";
					break;
				case "smmt":
					include "sendemailmtype.php";
					break;
				default:
                                    
				//check out the database table and include the file
					$menu_item_res = @lfmsql_query ("SELECT * FROM ".$prefix."adminmenu WHERE f = '".$_GET["f"]."'");
					if (lfmsql_num_rows ($menu_item_res) != 0) {
						$menu_item_row = lfmsql_fetch_array ($menu_item_res);
						include $menu_item_row[filename];
						}
					break;
				}
			}
		//include the files
		if($_POST["GivePrize"] == "Give Prize") { include "giveprize.php"; }
		if($_POST["DeductCommissions"] == "Deduct Commissions") { include "deductcommissions.php"; }
		if($_POST["EmailSelected"] == "Email Members") { include "emailmembers.php"; }
        if($_POST["EmailAll"] == "Email All") { include "emailmembers.php"; }
        if($_POST["EmailMT"] == "Email Member Level") { include "emailmt.php"; }
		if(isset($_POST["EmailGrp"])) { include "emailgroup.php"; }
		if($_POST["PayAll"] == "Pay All" ) { include "payall.php"; }
 		if(isset($_POST["CreateFile"]) || isset($_POST["PaySelected"])) { include "payselected.php"; }
        if(isset($_POST["EmailGeo"])) { include "emailgeo.php"; }

 		
       
?>
    </td>
  </tr>
</table>
</body>
</html>
