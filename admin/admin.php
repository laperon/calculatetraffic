<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright ©2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";

include "../inc/checkauth.php";

if(!isset($_SESSION["adminid"])) { exit; };

ini_set('session.gc_maxlifetime', 3600);

// Redirect to member stats
if($_POST["Submit"] == "Show Stats Of Selected" && $_GET["f"] == "mm") {
	$_SESSION["newgetid"] = $_POST["memcheck"];
	header("Location: admin.php?f=mstats&sf=browse&genstats=go&statstype=1");
}

// Get our settings
$res = lfmsql_query("SELECT sitename, affurl FROM ".$prefix."settings") or die("Unable to find settings!");
$row = lfmsql_fetch_array($res);
$sitename = $row["sitename"];
$affurl = $row["affurl"];

    if(isset($_POST["CreateFile"]))
	{
		// Get the max salesid and create the array of members to be paid
		$msalesid=$_POST["msalesid"];
		$msaledate=$_POST["msaledate"];
		$members_to_pay=$_POST["pay_id"];
		$min_comm=get_setting("min_comm");
		
		$rcnt=0;
		foreach ($members_to_pay as $key => $mid){
		
			if($mid >0)
			{
				// Build our list of members and their payments
				$dqry="SELECT Id FROM ".$prefix."members WHERE Id=".$mid;
				$dres=@lfmsql_query($dqry) or die(lfmsql_error());
				$drow=@lfmsql_fetch_array($dres) or die(lfmsql_error());
				$id=$drow["Id"];
				$sqry="SELECT SUM(".$prefix."sales.commission) AS payout,firstname,lastname,paypal_email FROM ".$prefix."sales LEFT JOIN ".$prefix."members ON ".$prefix."members.Id=".$prefix."sales.affid WHERE ".$prefix."sales.status IS NULL AND affid=$id AND ".$prefix."sales.commission != 0 AND (saledate <= '$msaledate 23:59:59' OR ".$prefix."sales.commission < 0) GROUP BY affid";
				$sres=@lfmsql_query($sqry);

				// Create data for the mass payment file
				while($srow=@lfmsql_fetch_array($sres))
				{
					if($srow["payout"] >= $min_comm)
					{
						$data.=$srow["paypal_email"]."\t".$srow["payout"]."\tUSD"."\r\n";
						$rcnt++;
						// Update status on sales for this person
						@lfmsql_query("UPDATE ".$prefix."sales SET status='P' WHERE affid = $id AND (saledate <= '$msaledate 23:59:59' OR commission < 0)") or die(lfmsql_error());
					}
				}
			}
		}

        // Data is created .. now write this to the file
        if($rcnt > 0)
        {
            $name="pay-".date("Y-m-d").".txt";
            header("Content-type: application/txt");
            header("Content-length: ".strlen($data)."");
            header("Content-Disposition: attachment; filename=$name");
            header("Content-Description: PHP Generated Data");
            echo $data;

            // Get the admin's email address
            $res=@lfmsql_query("SELECT email FROM ".$prefix."admin");
            $row=@lfmsql_fetch_array($res);
            $adminemail=$row["email"];

            // Send an email to admin with payment details
            $subject = $sitename.' Payment data - '.date("d-M-Y");
            $headers = "From: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Return-Path: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
            $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
            mail($adminemail,$subject,$data,$headers);
			exit;
        }

	}

    if(isset($_POST["CreateFileA"]))
    {
        // Get the max salesid and create the array of members to be paid
        $msalesid=$_POST["msalesid"];
		$msaledate=$_POST["msaledate"];
		$min_comm=get_setting("min_comm");


		//new, get array of checked members from the pay all file
		$members_to_pay = $_POST['pay_id'];
        $rcnt=0;

		foreach ($members_to_pay as $key => $mid){

        // Build our list of members and their payments
        $sqry="SELECT SUM(".$prefix."sales.commission) AS payout,Id,firstname,lastname,paypal_email FROM ".$prefix."sales LEFT JOIN ".$prefix."members ON ".$prefix."members.Id=".$prefix."sales.affid WHERE (".$prefix."sales.status IS NULL OR ".$prefix."sales.status = 'R' )  AND ".$prefix."sales.commission != 0 AND (saledate <= '$msaledate 23:59:59' OR ".$prefix."sales.commission < 0) AND Id = '".$mid."' GROUP BY affid";
        $sres=@lfmsql_query($sqry);

        // Create data for the mass payment file
        while($srow=@lfmsql_fetch_array($sres))
        {
			if($srow["payout"] >= $min_comm)
			{
	            $data.=$srow["paypal_email"]."\t".$srow["payout"]."\tUSD"."\r\n";
    	        $rcnt++;
				// Update status on sales for this person
				@lfmsql_query("UPDATE ".$prefix."sales SET status='P' WHERE affid=".$srow["Id"]." AND (saledate <= '$msaledate 23:59:59' OR commission < 0)") or die(lfmsql_error());
			}
        }
		}
        // Data is created .. now write this to the file
        if($rcnt > 0)
        {
            $name="pay-".date("Y-m-d").".txt";
            header("Content-type: application/txt");
            header("Content-length: ".strlen($data)."");
            header("Content-Disposition: attachment; filename=$name");
            header("Content-Description: PHP Generated Data");
            echo $data;

            // Get the admin's email address
            $res=@lfmsql_query("SELECT email FROM ".$prefix."admin");
            $row=@lfmsql_fetch_array($res);
            $adminemail=$row["email"];

            // Send an email to admin with payment details
            $subject = $sitename.' Payment data - '.date("d-M-Y");
            $headers = "From: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Return-Path: ".$sitename." <".$adminemail.">"."\r\n";
            $headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
            $headers .= "X-Mailer: PHP v".phpversion()."\r\n";
            mail($adminemail,$subject,$data,$headers);
			exit;
        }
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title><?=$sitename;?> Administration</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="timer/timeTo.css" rel="stylesheet" type="text/css" />

<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var featureList=["calendar"];
function init() {
	var returnedDate = function(date) { document.getElementById('publishdate').value=date[0] + "-" + date[1] + "-" + date[2]; }
	var calendar = new OAT.Calendar(true);

	var showCallback = function(event) {
		calendar.show(event.clientX, event.clientY, returnedDate);
}

	OAT.Dom.attach("mybutton","click",showCallback);
}
</script>
<script type="text/javascript" src="datepickercontrol/datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol/datepickercontrol.css">
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>

    <script type="text/javascript">
var timeout    = 500;
var closetimer = 0;
var ddmenuitem = 0;

function jsddm_open()
{  jsddm_canceltimer();
   jsddm_close();
   ddmenuitem = $(this).find('ul').css('visibility', 'visible');}

function jsddm_close()
{  if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}

function jsddm_timer()
{  closetimer = window.setTimeout(jsddm_close, timeout);}

function jsddm_canceltimer()
{  if(closetimer)
   {  window.clearTimeout(closetimer);
      closetimer = null;}}

$(document).ready(function()
{  $('#jsddm > li').bind('mouseover', jsddm_open)
   $('#jsddm > li').bind('mouseout',  jsddm_timer)});

document.onclick = jsddm_close;

</script>
<style type="text/css">
#jsddm {margin: 0;
        padding: 0}
#jsddm li
{float: left;
list-style: none;
font: 11px Tahoma, arial}
#jsddm li a
{display: block;
background: #20548E;
padding: 5px 12px;
text-decoration: none;
border-right: 1px solid white;
border-bottom: 1px solid white;
width: 70px;
color: #EAFFED;
white-space: nowrap}
#jsddm li a:hover
{background: #1A4473}
#jsddm li ul
{margin: 0;
padding: 0;
position: absolute;
visibility: hidden;
border-top: 1px solid white}
#jsddm li ul li
{float: none;
display: inline}
#jsddm li ul li a
{width: auto;
background: #9F1B1B}
#jsddm li ul li a:hover
{background: #7F1616}
</style>
</head>
<body>

<script src="style_files/js/jquery.min.js"></script>
<script src="style_files/js/bootstrap.min.js"></script>

<!-- Begin Header -->
<div class="lfm_admin_header"><?=$sitename;?> Admin</div>
<!-- End Header -->

<!-- Begin Navigation Menu -->

                <div class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
			    </button>
                        </div>
                        <div class="navbar-collapse collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                            
<?
	// Generate the admin menu
	// Get the top level items and then query their child items
	$menures=@lfmsql_query("SELECT * FROM ".$prefix."adminmenu WHERE menu_parent=0 ORDER BY menu_order, id");
	while($amenu=@lfmsql_fetch_object($menures)) {
		// Display top level item
		
		// Now get any submenu items
		$smcountres=@lfmsql_query("SELECT COUNT(*) FROM ".$prefix."adminmenu WHERE menu_parent=".$amenu->id);
		$smcount=@lfmsql_result($smcountres,0);
		if($smcount > 0) {
			// This is a category
			echo('<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$amenu->menu_label.'<b class="caret"></b></a>');
				echo('<ul class="dropdown-menu">');
				// Get sub items
				$smenures=@lfmsql_query("SELECT * FROM ".$prefix."adminmenu WHERE menu_parent=".$amenu->id." ORDER BY menu_order");
				while($asubmenu=@lfmsql_fetch_object($smenures)) { 
					echo('<li><a href="'.$asubmenu->menu_url.'" target="'.$asubmenu->menu_target.'">'.$asubmenu->menu_label.'</a></li>');
				}
				echo('</ul>');
			echo('</li>');
		} else {
			// This is a menu link
			echo('<li><a href="'.$amenu->menu_url.'" target="'.$amenu->menu_target.'">'.$amenu->menu_label.'</a></li>');
		}
	}
?>
                            
                            </ul>
                        </div><!-- /.nav-collapse -->
                </div><!-- /.navbar -->


<!-- End Navigation Menu -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>

          <?

if (isset($_GET['permission']) && $_GET['permission'] == "denied") {
	?>
	<br><center>
	<div class="lfm_infobox" style="width: 700px;">
	<table width="700" border="0" cellpadding="2">
	  <tr>
	    <td align="center"><div class="lfm_infobox_heading" style="color:darkred;">Not Authorized</div></td>
	  </tr>
	  
	  <tr>
	    <td align="center"><div class="lfm_descr">
	    	<br>Your administrator account does not have the required permissions to access this page.
	    </div></td>
	  </tr>
	      
	</table>
	</div>
	<?
	exit;
}

        if(!isset($_GET["f"])) {
        	
        	$rightwidgets = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."widgetpositions WHERE adminid='".$_SESSION["adminidnum"]."' AND col='right' AND enabled='1'"), 0);
        	if ($rightwidgets > 0) {
        		$widgetswidth = "1200";
        	} else {
        		$widgetswidth = "800";
        	}
        	
        	?>
        	
        	<center>
        	
        	<table width="<? echo($widgetswidth); ?>" border=0 cellpadding=10>
        	<tr><td align=left valign=top>
        	<div align="center">
        	
        	<?
        	
        	// Begin Left Side Widgets
        	$getwidgets = lfmsql_query("SELECT a.widgetfile AS widgetfile FROM ".$prefix."widgets a LEFT JOIN ".$prefix."widgetpositions b ON (a.id=b.widgetid) WHERE b.adminid='".$_SESSION["adminidnum"]."' AND b.col='left' AND b.enabled='1' ORDER BY b.row ASC");
        	if (lfmsql_num_rows($getwidgets) > 0) {
        		for ($i = 0; $i < lfmsql_num_rows($getwidgets); $i++) {
        			$widgetfile = lfmsql_result($getwidgets, $i, "widgetfile");
        			if (file_exists("widgets/".$widgetfile)) {
	        			include "widgets/".$widgetfile;
	        		}
        		}
        	} else {
        		echo("&nbsp;");
        	}
        	
        	?>
        	
        	</div>
        	</td>
        	<td align=right valign=top>
        	<div align="center">
        	
        	<?
        	
        	// Begin Center Widgets
        	$getwidgets = lfmsql_query("SELECT a.widgetfile AS widgetfile FROM ".$prefix."widgets a LEFT JOIN ".$prefix."widgetpositions b ON (a.id=b.widgetid) WHERE b.adminid='".$_SESSION["adminidnum"]."' AND b.col='center' AND b.enabled='1' ORDER BY b.row ASC");
        	if (lfmsql_num_rows($getwidgets) > 0) {
        		for ($i = 0; $i < lfmsql_num_rows($getwidgets); $i++) {
        			$widgetfile = lfmsql_result($getwidgets, $i, "widgetfile");
        			if (file_exists("widgets/".$widgetfile)) {
	        			include "widgets/".$widgetfile;
	        		}
        		}
        	} else {
        		echo("&nbsp;");
        	}
        	
        	?>
        	
        	<?
        	
        	if ($rightwidgets > 0) {
        		
        		echo('</div>
        		</td>
        		<td align=right valign=top>
        		<div align="center">');
        		
        		// Begin Right Side Widgets
        		$getwidgets = lfmsql_query("SELECT a.widgetfile AS widgetfile FROM ".$prefix."widgets a LEFT JOIN ".$prefix."widgetpositions b ON (a.id=b.widgetid) WHERE b.adminid='".$_SESSION["adminidnum"]."' AND b.col='right' AND b.enabled='1' ORDER BY b.row ASC");
        		if (lfmsql_num_rows($getwidgets) > 0) {
        			for ($i = 0; $i < lfmsql_num_rows($getwidgets); $i++) {
        				$widgetfile = lfmsql_result($getwidgets, $i, "widgetfile");
	        			if (file_exists("widgets/".$widgetfile)) {
		        			include "widgets/".$widgetfile;
	        			}
        			}
	        	} else {
        			echo("&nbsp;");
        		}
        		
        	}
        	
        	?>

</div>
</td></tr>
</table>

  <p>
    <input name="Submit" type="button" class="footer-text" onClick="javascript:viewLog()" value="View Log" />
    </p>
</p>

  <div class="lfm_descr"><? echo(get_script_type());?> v<? echo(get_script_vers());?></div>
  <div class="lfm_descr"><a target="_blank" href="http://thetrafficexchangescript.com/updatecheck.php?ver=<? echo(get_script_vers());?>">Check For Updates</a></div>
  </center>
  <?       // Check if install.php has been deleted
        if((file_exists("install/install_check.php")) && !isset($_GET["f"]))
        {
        ?>
  <br />
  <br />
</div>
<center><font color="#FF0000"><strong>NOTE: THE FILE INSTALL/INSTALL_CHECK.PHP SHOULD BE DELETED.</strong></font></center><br>
          <?
        }

}
        // Include the appropriate page depending on menu selection in admin area
		
		//check if its one of the files that overide the f variable
		if ( isset($_GET["f"]) & !isset($_POST["EmailSelected"]) & !isset($_POST["EmailMT"]) & !isset($_POST["EmailAll"]) & !isset($_POST["PayAll"]) & !isset($_POST["GivePrize"]) & !isset($_POST["DeductCommissions"]) & !isset($_POST["CreateFile"]) & !isset($_POST["PaySelected"])  & !isset($_POST["EmailGeo"]) & !isset($_POST["EmailGrp"]) ) {

			//do the includes that aren't in the menu
			switch ($_GET["f"]){
				case "ps":
					include "payselected.php";
					break;
				case "pa":
					include "payall.php";
					break;
				case "upd":
					include "updater.php";
					break;
				case "sm":
					include "sendemail.php";
					break;
				case "smr":
					include "sendemailregion.php";
					break;
				case "smg":
					include "sendemailgroup.php";
					break;
				case "smmt":
					include "sendemailmtype.php";
					break;
				default:
				//check out the database table and include the file
					$menu_item_res = @lfmsql_query ("SELECT * FROM ".$prefix."adminmenu WHERE f = '".$_GET["f"]."'");
					if (lfmsql_num_rows ($menu_item_res) != 0) {
						$menu_item_row = lfmsql_fetch_array ($menu_item_res);
						include $menu_item_row[filename];
						}
					break;
				}
			}
		//include the files
		if($_POST["GivePrize"] == "Give Prize") { include "giveprize.php"; }
		if($_POST["DeductCommissions"] == "Deduct Commissions") { include "deductcommissions.php"; }
		if($_POST["EmailSelected"] == "Email Members") { include "emailmembers.php"; }
        if($_POST["EmailAll"] == "Email All") { include "emailmembers.php"; }
        if($_POST["EmailMT"] == "Email Member Level") { include "emailmt.php"; }
		if(isset($_POST["EmailGrp"])) { include "emailgroup.php"; }
		if($_POST["PayAll"] == "Pay All" ) { include "payall.php"; }
 		if(isset($_POST["CreateFile"]) || isset($_POST["PaySelected"])) { include "payselected.php"; }
        if(isset($_POST["EmailGeo"])) { include "emailgeo.php"; }

 		
       
?>
    </td>
  </tr>
</table>
<script type="text/javascript" src="timer/jquery.timeTo.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>

</body>
</html>