<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



	// Prevent anyone who isn't logged in from opening this page
	if(!isset($_SESSION["adminid"])) { exit; };

	$msg="";

// Delete an Admin account
if(isset($_GET["delid"]) && is_numeric($_GET["delid"]) && $_GET["delid"] > 0) {
	lfmsql_query("DELETE FROM ".$prefix."admins WHERE id=".$_GET["delid"]) or die(lfmsql_error());
	lfmsql_query("DELETE FROM ".$prefix."admins_noperms WHERE adminidnum=".$_GET["delid"]) or die(lfmsql_error());
	lfmsql_query("DELETE FROM ".$prefix."widgetpositions WHERE adminid=".$_GET["delid"]) or die(lfmsql_error());
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />

<script language="javascript">
	function addAdmin()
	{
		var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=400,height=480"; 
	 
		var URL = "addadmin.php"; 
		popup = window.open(URL,"AddAdminPopup",windowprops);	
	}
	
	function editAdmin(adminid)
	{
		var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=400,height=490"; 
	 
		var URL = "editadmin.php?adminid=" + adminid; 
		popup = window.open(URL,"EditAdminPopup",windowprops);	
	}
	
	function editAdminPerms(adminid)
	{
		var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=550,height=650"; 
	 
		var URL = "adminperms.php?adminid=" + adminid; 
		popup = window.open(URL,"EditAdminPopup",windowprops);	
	}
	
	function viewAdminLog(adminid)
	{
		var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=700,height=550"; 
	 
		var URL = "adminlog.php?adminid=" + adminid; 
		popup = window.open(URL,"AdminLogsPopup",windowprops);	
	}
</script>

<center>
<p>&nbsp;</p>

<a href="javascript:addAdmin()"><input type="button" name="addadmin" value="Add Admin"></a>
<br><br>

<table width="750" align="center" cellpadding="4" cellspacing="0">
          <tr class="admintd">
            <td nowrap="NOWRAP" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Username</font></strong></td>
            <td nowrap="NOWRAP" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Edit Account</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Set Permissions</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">View Log</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Delete Account</font></strong></td>
          </tr>
          
          <?
          $mainadminusername = lfmsql_result(lfmsql_query("SELECT login FROM ".$prefix."admin"), 0);
          ?>
          
          <tr>
            <td nowrap="nowrap" style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><?=$mainadminusername;?></td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><a href="javascript:editAdmin(0)"><input type="button" name="editadmin" value="Edit"></a></td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; ">&nbsp;</td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><a href="javascript:viewAdminLog(0)"><input type="button" name="viewlog" value="View Log"></a></td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; ">&nbsp;</td>
          </tr>
          
		  <?
		  	// List Secondary Admins
		  	
			$adminres=@lfmsql_query("SELECT id, username FROM ".$prefix."admins");
			while($adminrow=@lfmsql_fetch_array($adminres))	{
			
			?>
          <tr>
            <td nowrap="nowrap" style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><?=$adminrow["username"];?></td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><a href="javascript:editAdmin(<?=$adminrow["id"];?>)"><input type="button" name="editadmin" value="Edit"></a></td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><a href="javascript:editAdminPerms(<?=$adminrow["id"];?>)"><input type="button" name="editadminperms" value="Permissions"></a></td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><a href="javascript:viewAdminLog(<?=$adminrow["id"];?>)"><input type="button" name="viewlog" value="View Log"></a></td>
            <td style="border-bottom-style: solid ; border-bottom-color: #B0CBFD; border-bottom-width: 1px; "><a href="admin.php?f=admins&delid=<?=$adminrow["id"];?>"><input Onclick="return confirm('Are you sure you want to delete this Admin account?')" type="button" name="deleteadmin" value="Delete"></a></td>
          </tr>
		  <?
		  }
		  ?>
</table>

<br />
<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  
  <tr>
    <td align="left"><div class="lfm_descr"><p>Creating multiple Admin accounts allows you to give your staff, business partners, and other associates their own unique username and password to access the Admin Panel.  By setting the permissions on each account, you can restrict which sections of the panel each Admin can access.  You can also restrict each account to a specific IP Address, preventing unauthorized parties from accessing them.</p>
      
      <p>As a general web security rule, you should always use extreme caution before giving someone the cPanel or FTP login for your server or hosting account.  These credentials grant direct access to the database, which could allow someone to override any other security measures you've setup.  If you need to give these credentials to a 3rd party, we recommend changing them after the work is complete.</p>
      
      </div></td>
  </tr>
</table>
</div>
<p>&nbsp;</p>