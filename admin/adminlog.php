<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

if (isset($_GET['adminid']) && is_numeric($_GET['adminid']) && $_GET['adminid'] >= 0) {
	$adminquery = " AND adminidnum=".$_GET['adminid'];
} else {
	$adminquery = "";
}

// Get our settings
$logres = lfmsql_query("SELECT logtime, ipaddress, message FROM ".$prefix."admins_log WHERE invalidlogin=0".$adminquery." ORDER BY logtime DESC") or die("Unable to find log!");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>LFM Log Viewer</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="675" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="admintd">
    <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date</font></strong></td>
    <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">IP Address</font></strong></td>
    <td width="100%"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Log Entry</font></strong></td>
  </tr>
<?
while($logrow=@lfmsql_fetch_array($logres))
{
?>  
  <tr>
    <td align="left" valign="top" nowrap="nowrap"><? echo(date('M j Y g:ia', $logrow["logtime"])); ?> </td>
    <td align="left"> <?=$logrow["ipaddress"];?> </td>
    <td align="left"> <?=$logrow["message"];?></td>
  </tr>
<?
}
?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
