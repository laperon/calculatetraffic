<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

if (!isset($_GET['adminid']) || !is_numeric($_GET['adminid']) || $_GET['adminid'] < 1) {
	echo("Invalid Admin ID");
	exit;
}

$permslist = array();

// Add the permissions built into the software
$permslist[] = "Abuse Reports";
$permslist[] = "Administrators";
$permslist[] = "Affiliate Toolbox";
$permslist[] = "Banned Sites/Emails/IP Addresses";
$permslist[] = "Commissions";
$permslist[] = "Custom Fields";
$permslist[] = "Downloads";
$permslist[] = "Edit Members";
$permslist[] = "Login To Member Accounts";
$permslist[] = "Media Library";
$permslist[] = "Member Level Settings";
$permslist[] = "Member Sites/Ads";
$permslist[] = "Offers and Upsells";
$permslist[] = "Payment Settings";
$permslist[] = "Purchases";
$permslist[] = "Sales Packages";
$permslist[] = "Surf Settings";
$permslist[] = "Surf Stats/Logs";
$permslist[] = "SurfingGuard";
$permslist[] = "System Settings";
$permslist[] = "Templates";
$permslist[] = "Surfbar Icons and Site Theme";
$permslist[] = "View Members";
$permslist[] = "View Payments";

// Add any custom permissions added from plugins
$getpermsadded = lfmsql_query("SELECT permission FROM ".$prefix."admins_permsadded ORDER BY permission ASC");
if (lfmsql_num_rows($getpermsadded) > 0) {
	for ($i = 0; $i < lfmsql_num_rows($getpermsadded); $i++) {
		$addedperm = lfmsql_result($getpermsadded, $i, "permission");
		if (strlen($addedperm) > 0) {
			$permslist[] = $addedperm;
		}
	}
}

?>

<html>

<head>
<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<style>
body {
  margin: 0;
  background-color: #FFFFFF;
}
</style>

<body>
<center>

<?

if (isset($_POST['Submit']) && $_POST['Submit'] == "Update Permissions") {
	
	lfmsql_query("DELETE FROM `".$prefix."admins_noperms` WHERE adminidnum='".$_GET['adminid']."'") or die(lfmsql_error());
	
	foreach ($permslist as $value) {
		$checkboxname = str_replace(' ', '', $value);
		if ($_POST[$checkboxname] != "1") {
			lfmsql_query("INSERT INTO `".$prefix."admins_noperms` (adminidnum, permission) VALUES ('".$_GET['adminid']."', '".$value."')") or die(lfmsql_error());
		}
	}
	
	echo("<br><div class=\"lfm_title\">Permissions Updated Successfully</div><br>");
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;	
}

####################

//Begin main page

####################

?>

	<br><div class="lfm_title">Admin Permissions</div>
	
	<br><div class="lfm_descr" style="width:500px;text-align:left;">Check the box for each section of the Admin Panel that you want this Admin to be able to access.</div>
	
	<form action="adminperms.php?adminid=<? echo($_GET['adminid']); ?>" method="post">
	<table width="500" cellpadding="5" cellspacing="0" border="0">
	
	<tr><td>
	
	<?
	
	foreach ($permslist as $value) {
		$valuename = $value;
		$valuename = str_replace("Administrators", "Administrators (Allows an Admin to set their own permissions)", $valuename);
		$permblocked = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."admins_noperms` WHERE adminidnum='".$_GET['adminid']."' AND permission='".$value."'"), 0);
		echo("<input type=\"checkbox\" name=\"".str_replace(' ', '', $value)."\" value=\"1\""); if($permblocked == 0) { echo(" checked"); } echo("> <font size=\"2\">".$valuename."</font><br>");
	}
	
	?>
	
	</td></tr>
	
	<tr><td>
	<INPUT type="submit" name="Submit" value="Update Permissions">
	</td></tr>
	
	</table>
	</form>

<br><br>

</center>
</body>
</html>

<?php
exit;
?>