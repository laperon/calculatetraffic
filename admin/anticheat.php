<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.02
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

		// Get path for affiliate URL
		$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
		$aff_path=substr($fullurl,0,strlen($fullurl)-15);


	// Get path to PHP
	$php_path = @exec( 'which php');
	if(isset($_SERVER['DOCUMENT_ROOT']))
	{
		$fullapp_path = $_SERVER['DOCUMENT_ROOT'];
	}
	else
	if(isset($_ENV['DOCUMENT_ROOT']))
	{
		$fullapp_path = $_ENV['DOCUMENT_ROOT'];
	}
	else
	if(isset($_SERVER['PATH_TRANSLATED']))
	{
		$fullapp_path = $_SERVER['PATH_TRANSLATED'];
	}
	else
	{
		$fullapp_path = $_SERVER['SCRIPT_FILENAME'];
	}

	// Strip filename from $fullapp_path
	$app_path=substr($fullapp_path,0,strlen($fullapp_path)-15);

// Update system settings
if($_POST["Submit"] == "Update AntiCheat Settings")
{

		if((is_numeric($_POST['accuracy'])) && ($_POST['accuracy'] < 1)) {
			$_POST['accuracy'] = $_POST['accuracy']*100;
		}
		
		//If the click accuracy requirement is too low or too high, uses default of 75%
		if ((!is_numeric($_POST['accuracy'])) || ($_POST['accuracy'] < 5) || ($_POST['accuracy'] > 95)) {
			$_POST['accuracy'] = 75;
		}
		
		if ((!is_numeric($_POST['gracep'])) || ($_POST['gracep'] <= 4)) {
			$_POST['gracep'] = 20;
		}
		
		if ((!is_numeric($_POST['sessiontime'])) || ($_POST['sessiontime'] < 1)) {
			$_POST['sessiontime'] = 5;
		}
		
		if ((!is_numeric($_POST['maxtime'])) || ($_POST['maxtime'] <= 1)) {
			$_POST['maxtime'] = 12;
		}
		
		if ((!is_numeric($_POST['enclevel'])) || ($_POST['enclevel'] < 0) || ($_POST['enclevel'] > 4)) {
			$_POST['enclevel'] = 4;
		}
		
		
		if ($_POST['enclevel'] > 0) {
			$encrypt = 1;
		} else {
			$encrypt = 0;
		}
		

		// Update anti-cheat settings
		@mysql_query("Update ".$prefix."anticheat set accuracy=".$_POST['accuracy'].", gracep=".$_POST['gracep'].", sessiontime=".$_POST['sessiontime'].", maxtime=".$_POST['maxtime'].", encrypt=".$encrypt.", enclevel=".$_POST['enclevel']." where id=1");
		
		$msg = "Settings Updated";

}

$res=@mysql_query("SELECT * FROM ".$prefix."anticheat where id=1");
$row=@mysql_fetch_array($res);

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><p>&nbsp;</p><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td>
	<form action="admin.php?f=acheats" method="post" name="acheats" id="acheats">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
      <?
      // Update system settings
      if($_POST["Submit"] == "Update AntiCheat Settings")
      { ?>
        <tr>
          <td  colspan="4" align="center">
            <font color="red"><strong><?=$msg;?></strong></font></td>
        </tr>
    <?
      }    
    ?>
    
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surfbar Encryption Strength</font> </strong></td>
        <td colspan="2" class="formfield">
        
        <select name="enclevel">
        <option value="0"<? if($row["enclevel"] == 0){echo(" selected");} ?>>None</option>
        <option value="1"<? if($row["enclevel"] == 1){echo(" selected");} ?>>Low</option>
        <option value="2"<? if($row["enclevel"] == 2){echo(" selected");} ?>>Medium</option>
        <option value="3"<? if($row["enclevel"] == 3){echo(" selected");} ?>>High</option>
        <option value="4"<? if($row["enclevel"] == 4){echo(" selected");} ?>>Maximum</option>
        </select>
        
        </td>
        <td align="center"><a href="#" title="Helps prevent auto-click programs from reading the surfbar.  The maximum strength is recommended, but can be lowered to reduce the server load."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
    
    <tr>
        <td colspan="4" nowrap="nowrap" class="admintd"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Clicking Accuracy</strong></font> </td>
        </tr>

      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Accuracy Requirement</strong></font></td>
        <td colspan="2" class="formfield"><input name="accuracy" type="text" class="formfield" id="accuracy" value="<?=$row["accuracy"];?>" size="4" />%</td>
        <td align="center"><a href="#" title="The percentage of correct clicks a member must maintain without being suspended.  Must be between 5% and 95%.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Grace Period</strong></font></td>
        <td colspan="2" class="formfield"><input name="gracep" type="text" class="formfield" id="gracep" value="<?=$row["gracep"];?>" size="4" />clicks</td>
        <td align="center"><a href="#" title="The number of clicks a member can make before being suspended for a low clicking accuracy.  This gives new members a little time to become familiar with the surfbar.  Must be at least 5.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>


      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td colspan="4" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Surfing Time Limits</font></strong></td>
        </tr>
        
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Session Surfing Limit</font> </strong></td>
        <td colspan="2" class="formfield"><input name="sessiontime" type="text" class="formfield" id="sessiontime" value="<?=$row["sessiontime"];?>" size="4" /> hours</td>
        <td align="center"><a href="#" title="The maxiumum number of hours a member can surf in one session."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>

      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Daily Surfing Limit</font> </strong></td>
        <td colspan="2" class="formfield"><input name="maxtime" type="text" class="formfield" id="maxtime" value="<?=$row["maxtime"];?>" size="4" /> hours</td>
        <td align="center"><a href="#" title="The maxiumum number of hours a member can surf in one day."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
  
      <tr>
        <td align="left" nowrap="nowrap" class="button"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Update AntiCheat Settings" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>
