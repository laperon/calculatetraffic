<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

$showtype = "";
$timequery = "";

// Operations affecting site records

// Delete multiple sites according to check boxes
if($_POST["MDelete"] == "Yes - Unban")
{
	if(isset($_POST["idarray"]))
	{
		while (list ($key,$val) = @each ($_POST["idarray"])) {
		$dqry="DELETE FROM `".$prefix."banips` WHERE id=".$val;
		lfmsql_query($dqry) or die("Error: Unable to unban ip addresses!");
		}
	}
}

?>
<script src="../inc/jsfuncs.js" type="text/javascript"></script>
<?
	// Get the starting record for site browse
	if(!isset($_GET["limitStart"]))
	{
		$st=0;
	}
	else
	{
		$st=$_GET["limitStart"];
	}

	// Get the total site count for site browse
	$cqry="SELECT COUNT(*) as mcount FROM `".$prefix."banips`";

    $cres=@lfmsql_query($cqry);
    $crow=@lfmsql_fetch_array($cres);

    // Get the first/next 40 records
    $mqry="SELECT * FROM `".$prefix."banips` WHERE id != 0";

    $mqry.=" ORDER BY banned LIMIT $st,40";

    $mres=@lfmsql_query($mqry);


	//
	// Site browsing
	// This is where the search and browse records are diaplayed
	//
	if($_GET["sf"] == "browse" && !isset($_POST["Delchecked"]))
	{
?>
<form name="mbrowse" id="mbrowse" method="post" action="admin.php?f=banipmm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sf=browse">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr>
        <td colspan="13" align="left" valign="bottom">
		<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($crow["mcount"],$_GET["limitStart"],40,'banipmm');
		?></div>
		
		<? echo("<input name=\"Add New Ip Address\" type=\"button\" class=\"lfmtable\" style=\"height: 8; font-size:9px\" onClick=\"javascript:addIpban()\" value=\"Add New Ip Address\" />"); ?>
		
		<input name="Delchecked" type="submit" class="lfmtable" id="Delchecked" style="height: 8; font-size:9px" value="Unban Selected" />
		
		</td>
        </tr>
        
      <tr class="admintd">
        <td width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">ID</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ip Address </font></strong></td>
          
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg">&nbsp;</td>
        <td width="16" align="center" nowrap="NOWRAP"><input type="checkbox" name="checkbox" value="checkbox" onClick="javascript:checkAll(document.getElementById('mbrowse'),'mcheck');" /></td>
      </tr>
<?
	while($mrow=@lfmsql_fetch_array($mres))
	{
		if($bgcolor == "#FFFFFF")
		{
			$bgcolor="#DDDDDD";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}
?>
      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["id"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["banned"];?>
        </font></td>

        <td width="24" align="center" bgcolor="<?=$bgcolor;?>"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:delIpban(<?=$mrow["id"];?>)"><img src="../images/del.png" alt="Unban Member" width="16" height="16" border="0" /></a></font></td>
        <td align="center"><input type="checkbox" class="mcheck" name="memcheck[]" value="<?=$mrow["id"];?>" onclick="HighlightRowIfChecked(this);" />
        </td>
      </tr>
<?
}
?>
    </table>	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>
</form>
<?
}
//
// End of site browsing/searching section
//

// Delete Confirmation section
// This area is for display of details
// relating to multiple site deletion
if($_POST["Delchecked"] == "Unban Selected")
{
	if(!isset($_POST["memcheck"]))
	{
?>
<center>
  <font face="Verdana, Arial, Helvetica, sans-serif">NO RECORDS SELECTED</font>
</center>
<?
		exit;
	}
?>
<br /><br />
<center><strong><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif">You Are About To Unban The Following Ip Addresses</font></strong></center>
<br /><br />
<form method="POST" action="admin.php?f=banipmm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sf=browse">
<table align="center" cellpadding="2" cellspacing="0" class="lfmtable">
<tr class="admintd">
<td><strong>ID</strong></td>
<td align="center"><strong>URL</strong></td>
</tr>
<?
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$dqry="SELECT * FROM ".$prefix."banips WHERE id=".$val;
		$dres=@lfmsql_query($dqry);
		$drow=@lfmsql_fetch_array($dres);
?>
<tr>
<td><?=$drow["id"];?></td><td align="center"><?=$drow["banned"];?><input type="hidden" name="idarray[]" value="<?=$drow["id"];?>" /></td>
</tr>
<?
		}
	}

?>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr><td colspan="2" align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Are You Sure?</strong></font></td>
</tr>
<tr>
  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="No - Cancel" />
    <input name="MDelete" type="submit" id="MDelete" value="Yes - Unban" /></td>
</tr>
</table>
</form>
<?
}

?>