<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



	// Prevent anyone who isn't logged in from opening this page
	if(!isset($_SESSION["adminid"])) { exit; };

	$msg="";

// Add a page entry
if($_POST["Submit"] == "Add Page")
{
		
		if (($_POST["shownum"] < 1) || !is_numeric($_POST["shownum"])) {
			$shownum = 1;
		} else {
			$shownum = $_POST["shownum"];
		}
		
		$errormess = "";
		
		$shownum = $_POST["shownum"];
		$accid = $_POST["accid"];
		$showtype = $_POST["showtype"];
		$url = $_POST["url"];
		
		$timerange = $_POST['timerange'];
		
		if ($timerange == 1) {
		
			// Begin Start Date
			$startdate = $_POST['startdate'];
			$starthour = $_POST['starthour'];
			$startminute = $_POST['startminute'];
			
			if (isset($_POST['startampm']) && $_POST['startampm'] == 1 && $starthour == 12) {
				// Adjust Midnight to 24-hour clock
				$starthour = 0;
			}
			
			if ($starthour < 12) {
				// Check AM/PM
				if (isset($_POST['startampm']) && $_POST['startampm'] == 2) {
						$starthour = $starthour+12;
				}
			}
			
			if ($starthour < 0 || $starthour > 23) {
				$errormess = "Invalid Hour";
			}
			
			if ($startminute < 1 || $startminute > 4) {
				$errormess = "Invalid Minute Selection";
			}
			
			if ($startminute == 1) {
				$realminute = "00";
			} elseif ($startminute == 2) {
				$realminute = "15";
			} elseif ($startminute == 3) {
				$realminute = "30";
			} else {
				$realminute = "45";
			}
			
			if ($starthour < 10) {
				$testhour = "0".$starthour;
			} else {
				$testhour = $starthour;
			}
			
			$testdate = $startdate." ".$testhour.":".$realminute;
			if (($starttime = strtotime($testdate)) === false) {
				$errormess = "Could not parse the date: ".$testdate;
			}
			
			// End Start Date
			
			
			// Begin End Date
			$enddate = $_POST['enddate'];
			$endhour = $_POST['endhour'];
			$endminute = $_POST['endminute'];
			
			if (isset($_POST['endampm']) && $_POST['endampm'] == 1 && $endhour == 12) {
				// Adjust Midnight to 24-hour clock
				$endhour = 0;
			}
			
			if ($endhour < 12) {
				// Check AM/PM
				if (isset($_POST['endampm']) && $_POST['endampm'] == 2) {
						$endhour = $endhour+12;
				}
			}
			
			if ($endhour < 0 || $endhour > 23) {
				$errormess = "Invalid Hour";
			}
			
			if ($endminute < 1 || $endminute > 4) {
				$errormess = "Invalid Minute Selection";
			}
			
			if ($endminute == 1) {
				$realminute = "00";
			} elseif ($endminute == 2) {
				$realminute = "15";
			} elseif ($endminute == 3) {
				$realminute = "30";
			} else {
				$realminute = "45";
			}
			
			if ($endhour < 10) {
				$testhour = "0".$endhour;
			} else {
				$testhour = $endhour;
			}
			
			$testdate = $enddate." ".$testhour.":".$realminute;
			if (($endtime = strtotime($testdate)) === false) {
				$errormess = "Could not parse the date: ".$testdate;
			} else {
				if ($endtime <= (time()+2)) {
					$errormess = "The selected end date has already passed.";
				}
			}
			
			// End End Date
			
			if ($starttime > $endtime) {
				$errormess = "The end date must be after the start date.";
			}
			
		} else {
			$starttime = 0;
			$endtime = 0;
		}
		
		
		$checklastrow = lfmsql_query("Select pagerank from ".$prefix."bonuspages order by pagerank desc");
		if (lfmsql_num_rows($checklastrow) > 0) {
		$lastrow = lfmsql_result($checklastrow, 0, "pagerank");
		} else {
		$lastrow = 0;
		}
		$newpagerank = $lastrow+1;
		
		if(isset($_POST["visible"])) { $visible=1; } else { $visible=0; }
		
		if ($errormess == "") {
			$qry="INSERT INTO ".$prefix."bonuspages(shownum,accid,showtype,url,pagerank,starttime,endtime) VALUES ($shownum,$accid,$showtype,'$url',$newpagerank,$starttime,$endtime)";
			lfmsql_query($qry) or die(lfmsql_error());
		} else {
			echo("<center><p><b>".$errormess."</b></p>");
			exit;
		}
		
}


//Change A pagerank
$pagerank = $_GET['pagerank'];
if ($pagerank == "up" || $pagerank == "down") {

$runchange = "yes";
$id = $_GET['productsid'];

$checkfirstrow = lfmsql_query("Select pagerank from ".$prefix."bonuspages order by pagerank asc");
$firstrow = lfmsql_result($checkfirstrow, 0, "pagerank");

$checklastrow = lfmsql_query("Select pagerank from ".$prefix."bonuspages order by pagerank desc");
$lastrow = lfmsql_result($checklastrow, 0, "pagerank");

if (($pagerank=="up") && ($id > $firstrow)) {
$change=$id-1;
}
elseif (($pagerank=="down") && ($id < $lastrow)) {
$change=$id+1;
}
else {
$runchange = "no";
}

if ($runchange == "yes") {
@lfmsql_query("Update ".$prefix."bonuspages set pagerank=0 where pagerank=$id");
@lfmsql_query("Update ".$prefix."bonuspages set pagerank=$id where pagerank=$change");
@lfmsql_query("Update ".$prefix."bonuspages set pagerank=$change where pagerank=0");
@lfmsql_query("ALTER TABLE ".$prefix."bonuspages ORDER BY pagerank;");
}

}

// Delete a bonus page entry
if(isset($_GET["pd"]))
{
	lfmsql_query("DELETE FROM ".$prefix."bonuspages WHERE id=".$_GET["pd"]);
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<?
echo("<center><p><font size=\"3\"><b>Current Server Time: ".date('F jS Y')." ".date('g:i A')."</b></font></p></center>");
?>
<p align="center">&nbsp;</p>
<form method="post" action="admin.php?f=bonusp">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0">
          <tr class="admintd">
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Rank</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Details</font></strong></td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
    </tr>
		  <?
		  	$t = time();
		  	// List bonus pages
			$prodres=@lfmsql_query("SELECT * FROM ".$prefix."bonuspages");
			while($prodrow=@lfmsql_fetch_array($prodres))
			{
			
			if ($prodrow["starttime"] == 0 || ($prodrow["starttime"]<$t && $prodrow["endtime"] > $t)) {
				// Page is rotating
				$tdcolor = "lightgreen";
			} elseif ($prodrow["starttime"] > $t) {
				// Page is upcoming
				$tdcolor = "lightblue";
			} elseif ($prodrow["endtime"] < $t) {
				// Page has ended
				$tdcolor = "#CCCCCC";
			}
			
			if ($prodrow["starttime"] > 0) {
				$runtime = "Runs <b>".date('F jS Y',$prodrow["starttime"])." ".date('g:i A',$prodrow["starttime"])."</b> - <b>".date('F jS Y',$prodrow["endtime"])." ".date('g:i A',$prodrow["endtime"])."</b><br><br>";
			} else {
				$runtime = "";
			}
			
			?>
          <tr bgcolor="<? echo($tdcolor); ?>">
            <td nowrap="nowrap"><a href=admin.php?f=bonusp&pagerank=up&productsid=<?=$prodrow["pagerank"];?>>Move Up</a><br><a href=admin.php?f=bonusp&pagerank=down&productsid=<?=$prodrow["pagerank"];?>>Move Down</a></td>
            <td nowrap="nowrap"><? echo($runtime); ?>Show the URL <input type=text size=25 value="<?=$prodrow["url"];?>"> to 
            <?

if ($prodrow["accid"] == 0) {

echo("All Members");

} else {

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
if ($accid == $prodrow["accid"]) { echo($accname); }
}

}

if ($prodrow["showtype"] == 1) {
echo(" After Every ");
} else {
echo(" After ");
}
?>
            <?=$prodrow["shownum"];?> clicks.
            
            <td nowrap="nowrap"><a href="javascript:editBonusPage(<?=$prodrow["id"];?>);"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
            <td nowrap="nowrap"><a href="admin.php?f=bonusp&pd=<?=$prodrow["id"];?>"><img src="../images/del.png" alt="Delete Page" width="16" height="16" border="0" /></a></td>

          </tr>
          
          <tr><td colspan="5">&nbsp;</td></tr>
          
		  <? } ?>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">Show the URL <input name="url" type="text" id="url" size="25" value="http://" /> to 
            <?

echo("<select name=accid><option value=0>All Members</option>");

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
echo("<option value=$accid>$accname</option>");
}
echo("</select> ");

echo("<select name=showtype>
<option value=0>After</option>
<option value=1>After Every</option>
</select> ");

?>
<input name="shownum" type="text" id="shownum" size="5" value="10"/> clicks. 

<br><br>
<input name="timerange" type="radio" value="0" checked> Start Rotating Now<br><br>
<input name="timerange" type="radio" value="1"> Rotate During This Time Period:<br>
Start: <input name="startdate" type id="DPC_startdate_YYYY-MM-DD" value="" width="80" text>&nbsp; <input type="text" name="starthour" value="12" size="3" maxlength="2">
<select name="startminute">
	<option value="1">:00</option>
	<option value="2">:15</option>
	<option value="3">:30</option>
	<option value="4">:45</option>
</select>
<select name="startampm">
	<option value="1">AM</option>
	<option value="2">PM</option>
</select>
<br>

End:&nbsp; <input name="enddate" type id="DPC_enddate_YYYY-MM-DD" value="" width="80" text>&nbsp; <input type="text" name="endhour" value="12" size="3" maxlength="2">
<select name="endminute">
	<option value="1">:00</option>
	<option value="2">:15</option>
	<option value="3">:30</option>
	<option value="4">:45</option>
</select>
<select name="endampm">
	<option value="1">AM</option>
	<option value="2">PM</option>
</select>

<br>
<br>
            
<input name="Submit" type="submit" class="formfield" id="Submit" value="Add Page" />
<br><br>
</td>
          </tr>
</table>
</form>
<br />
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  
  <tr>
    <td align="left" valign="top" bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">You can use this feature to rotate surfing games, special offers, or any other pages you choose.  Pages set to show "After xx clicks" will only be shown once per member per day.  Pages set to show "After Every xx clicks" will be shown continuously while members surf.</font></p>
      
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">To prevent members from copying your bonus page URLs and reloading them, you can add the following security code to your bonus page.  This will only work if the bonus page is in your exchange's main HTML folder.</font></p>
      
      <XMP>
require_once "inc/filter.php";
session_start();
include "inc/userauth.php";

if ($_SESSION['bonuscode']==0 || ($_GET['securecode'] != md5($_SESSION['bonuscode']))) {
echo("Access Denied");
exit;
}
$_SESSION['bonuscode'] = 0;
      </XMP>
      
      </font></p>

      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
