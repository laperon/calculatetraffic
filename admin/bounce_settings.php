<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.29
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

	// Prevent anyone who isn't logged in from opening this page
	include "../inc/checkauth.php"; 
	if(!isset($_SESSION["adminid"])) { exit; };

	$err = "";
	if ($_POST['act'] == 'saveset') {
		
		$bouncelimit = 0;
		$bouncetest1 = intval($_POST['bouncetest1']);
		$bouncetest2 = intval($_POST['bouncetest2']);
		
		//if ($bouncelimit < 1) {
		//	$err .= "<b>You must enter a bounce limit greater than zero.</b><br />";
		//}
		if ($bouncetest1 < 1) {
			$err .= "<b>You must enter a time for bounce test one greater than zero.</b><br />";
		}
		if ($bouncetest2 < 1) {
			$err .= "<b>You must enter a time for bounce test two greater than zero.</b><br />";
		}
		if (!$err) {
			@mysql_query("UPDATE ".$prefix."settings SET bouncetest1='$bouncetest1',bouncetest2='$bouncetest2',bouncelimit='$bouncelimit' ");
		}
	}
	
	$setting = mysql_fetch_array(mysql_query("SELECT bouncetest1,bouncetest2,bouncelimit FROM ".$prefix."settings WHERE id=1 "));
	
// Get Cron
$php_path = @exec( 'which php');
if ($php_path == "") {
	$php_path = "/usr/bin/php";
}
$bouncecron = $php_path." -f ".$_SERVER['DOCUMENT_ROOT']."/bouncecron.php > /dev/null 2>&1";

$bouncecronrun = mysql_result(mysql_query("SELECT lastbouncecron FROM `".$prefix."settings`"), 0);

if ($bouncecronrun < (time()-3600)) {
	$bouncecronstatus = "Not Working";
	$bouncecroncolor = "#FFFF66";
} else {
	$bouncecronstatus = "Working";
	$bouncecroncolor = "#339900";
}

// End Get Cron

echo("<style type=\"text/css\" media=\"screen\">
@import url( lfm_admin_style.css );
</style>");

?>
<center>
<br><br>
<table border="0" cellpadding="3" cellspacing="0" width="600">
<tr><td align="center">
	<div class="lfm_title">Bounce E-mail Handler</div>
</td></tr>
<tr><td align="left">
	<p class="lfm_descr">If a member deletes their e-mail address, or if their inbox runs out of space, any e-mails that you send to them will bounce.  
	Sending a lot of e-mails to bouncing addresses will lower the e-mail reputation of your site, which may cause your e-mails to end up in more SPAM
	boxes.  To prevent this, the script can automatically detect a bounce, and require that the member correct the problem before they can login again.
	<br><br>
	To use this feature, you will need to setup a bounce handler in your hosting account.  If you're using cPanel, the instructions below should help
	you get this setup.</p>
</td></tr>
</table>

<br><br>
<div style="border: 2px #AAA solid;padding: 20px;background: #F6F6F6; width: 385px;">
<p><font size=3><b>Bounce Settings</b></font></p>
<form action="admin.php?f=bouncesettings" method="post">
 <table width="385" border="0" align="center" cellpadding="4" cellspacing="0">
 
 <!--
   <tr>
     <td><font size=2>Allowed Bounces:</font></td>
     <td><input class="form-control" type="text" name="bouncelimit" value="<?=$setting['bouncelimit']?>"></td>
     <td nowrap="nowrap"><a href="#" title="Number of bounced emails allowed before the address is tested."><img src="../images/question_new.png" width="30" height="30" border="0" /></a></td>
   </tr>
 -->
   
   <tr>
     <td><font size=2>First Bounce Test:</font></td>
     <td><input class="form-control" type="text" name="bouncetest1" value="<?=$setting['bouncetest1']?>"></td>
      <td nowrap="nowrap"><a href="#" title="Number of hours to wait before testing the address for the first time after a bounce."><img src="../images/question_new.png" width="30" height="30" border="0" /></a></td>
   </tr>
   <tr>
     <td><font size=2>Second Bounce Test:</font></td>
     <td><input class="form-control" type="text" name="bouncetest2" value="<?=$setting['bouncetest2']?>"></td>
     <td nowrap="nowrap"><a href="#" title="Number of hours to wait after the first test to test the address for the second time."><img src="../images/question_new.png" width="30" height="30" border="0" /></a></td>
   </tr>
   <tr>
     <td colspan="3" align="center">
     	<input type="hidden" name="act" value="saveset">
     	<input type="image" name="submit" src="../images/update_large.png" value="Save Settings">
     </td>
   </tr>
 </table>
 </form>
 </div>
 <br><br>
<?

echo("<div style=\"border: 2px #AAA solid;padding: 20px;background: #F6F6F6; width: 500px;\">
<table border=0 width=500 cellpadding=5 cellspacing=0>
<tr><td align=center>
<p><font size=3><b>Cron Job</font></p>
<p align=left><font size=2>The cron job below should be added to your cPanel account.  After adding, it may take up to 24 hours for the status to change to \"Working\".</font></p>
</td></tr>
<tr><td align=center bgcolor=".$bouncecroncolor.">
	<p><font size=3><b>Run Once An Hour</b></font></p>
	<p><font size=1><b>".$bouncecron."</b></font></p>
	<p><font size=2>Status: <b>".$bouncecronstatus."</b></font></p>
</td></tr>
</table>
</div>
<br><br>");

echo("<div style=\"border: 2px #AAA solid;padding: 20px;background: #F6F6F6; width: 500px;\">
<table border=0 width=500 cellpadding=5 cellspacing=0>
<tr><td align=center>
<p><font size=3><b>Bounce Handler</font></p>
<p align=left><font size=2>The following instructions will help you setup your bounce handler address on a Cpanel server.<br><br>
1. Click on \"Fowarders\"<br><br>
2. Click on \"Add Fowarder\"<br><br>
3. You can enter any unused address for \"Address to Forward\".  For example, enter <b>bounces</b> and your bounce handler will be:<br> <b>bounces@".$_SERVER["SERVER_NAME"]."</b><br><br>
4. Click on \"Advanced Options\".<br><br>
5. Choose the option \"Pipe to a Program\" and in the input box enter:<br> <b>public_html/bouncepipe.php</b><br><br>
6. Click \"Add Forwarder\".  Now just go to \"System Settings\" in your Admin Panel and enter the address you setup in step 3 in the \"Bounce Notification Address\" field.
</font></p>
</td></tr>
</table>
</div>
<br><br>");

?>