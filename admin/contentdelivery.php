<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "../inc/filter.php";

	// Prevent anyone who isn't logged in from opening this page
	
	if(!isset($_SESSION["adminid"])) { exit; };
	
	// Number of previous template versions to backup
	$keep_prev = 15;

	if(trim($_POST["Submit"]) == "Add Page")
	{
		$pagetag=trim($_POST["pagetag"]);
		$pagename=trim($_POST["pagename"]);
		$mtype=trim($_POST["mtype"]);
		$delay=trim($_POST["delay"]);
		$pageindex=trim($_POST["pageindex"]);
		$publishdate=date("Y-m-d");
		if(isset($_POST["menu"])) { $menu=1; } else { $menu=0; }
		if($delay == "") { $delay="0"; }

        $qry="INSERT INTO ".$prefix."memberpages(pageindex,pagename,pagedata,mtype,delay,pagetag,menu,publishdate) VALUES('$pageindex','$pagename','',$mtype,0,'$pagetag',$menu,'$publishdate')";
		$res=@lfmsql_query($qry);
	}

	if(trim($_POST["Submit"]) == "Update Template")
	{ 

		$pagetag=trim($_POST["pagetag"]);
		$pagename=trim($_POST["pagename"]);
		$pageid=trim($_POST["pageid"]);
		$mtype=trim($_POST["mtype"]);
		$delay=trim($_POST["delay"]);
		$pagedata=$_POST["pagedata"];

		$getoldtemplate = lfmsql_query("SELECT pagedata from ".$prefix."memberpages WHERE pageid=".$pageid) or die(lfmsql_error());
		if (lfmsql_num_rows($getoldtemplate) > 0) {
			$oldtemplate = addslashes(lfmsql_result($getoldtemplate, 0, "pagedata"));
			
			// Make sure the old template is already saved
			$backupexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."pages_backups where pageid='".$pageid."' and template_data='".$oldtemplate."'"), 0);
			if ($backupexists < 1) {
				// We don't know when this template was saved, so set 1 day ago
				$tempsavetime = date("Y-m-d H:i:s", time()-86400);

				lfmsql_query("INSERT INTO ".$prefix."pages_backups (savetime, pageid, template_data) VALUES ('".$tempsavetime."', '".$pageid."', '".$oldtemplate."')") or die(lfmsql_error());
    	}
			
			// Save new template backup
			lfmsql_query("INSERT INTO ".$prefix."pages_backups (savetime, pageid, template_data) VALUES (NOW(), '".$pageid."', '".$pagedata."')") or die(lfmsql_error());
			
			// Save new template
			$qry="UPDATE ".$prefix."memberpages SET pagedata='".$pagedata."' WHERE pageid='".(int)$pageid."'"; 
			@lfmsql_query($qry);
			
			// Check and remove extra previous versions
			$countprev = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."pages_backups where pageid='".$pageid."'"), 0);
			if ($countprev > $keep_prev) {
				$num_to_del = $countprev - $keep_prev;
				lfmsql_query("DELETE FROM ".$prefix."pages_backups WHERE pageid='".$pageid."' ORDER BY savetime ASC LIMIT ".$num_to_del) or die(lfmsql_error());
			}
			
		}

	}

	if(trim($_POST["Submit"]) == "Update Page")
	{ 
		$pagetag=trim($_POST["pagetag"]);
		$pagename=trim($_POST["pagename"]);
		$pageid=trim($_POST["pageid"]);
		$mtype=trim($_POST["mtype"]);
		$delay=trim($_POST["delay"]);
		$pagedata=$_POST["pagedata"];
		$pageindex=trim($_POST["epageindex"]);
		$publishdate=trim($_POST["publishdate"]);
		if(isset($_POST["menu"])) { $menu=1; } else { $menu=0; }

		$qry="UPDATE ".$prefix."memberpages SET pageindex='$pageindex',pagename='$pagename',mtype=$mtype,delay=0,pagetag='$pagetag',menu=$menu,publishdate='$publishdate' WHERE pageid=$pageid";
		@lfmsql_query($qry);

	}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />

<?
// Get/Set Editors Settings

$enablewysiwyg = lfmsql_result(lfmsql_query("Select enablewysiwyg from `".$prefix."settings`"), 0);
if (isset($_GET['seteditors']) && ($_GET['seteditors'] == 0 || $_GET['seteditors'] == 1)) {
	$enablewysiwyg = $_GET['seteditors'];
	@lfmsql_query("Update `".$prefix."settings` set enablewysiwyg=".$enablewysiwyg);
}
//End Get/Set Editors Settings

?>
<? if($enablewysiwyg == "1") { ?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<center><p><font size="4"><a href="admin.php?f=cd&seteditors=0">Click To Hide The HTML Editor</a></font></p></center>
<? } else { ?>
<center><p><font size="4"><a href="admin.php?f=cd&seteditors=1">Click To Show The HTML Editor</a></font></p></center>
<? } ?>

<p align="center"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif"><br>
Add Page </font></strong></p>
<form method="post" action="admin.php?f=cd">
<table border="0" align="center" cellpadding="3" cellspacing="2">
  <tr class="admintd">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Index</font></strong></td>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL Tag </font></strong></td>
    <td align="left"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Page Name </font></strong></td>
    <td align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Level </font></strong></td>
    <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Menu</font></strong></td>
    <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font></td>
  </tr>
  <tr>
    <td align="center"><input name="pageindex" type="text" id="pageindex" value="0" size="2" /></td>
    <td align="left"><input name="pagetag" type="text" id="pagetag" size="8" maxlength="8" /></td>
    <td align="left"><input name="pagename" type="text" id="pagename" /></td>
    <td align="center"><select name="mtype" id="mtype">
	<option value="0">Select...</option>
	<option value="9999">None</option>
	<? GetMtype(0); ?>
    </select>    </td>
    <td align="center"><input name="menu" type="checkbox" id="menu" value="1" /></td>
    <td><input type="submit" name="Submit" value="Add Page" /></td>
  </tr>
</table>
</form>
<br>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Current Pages </font></strong></td>
  </tr>
</table>
<br>
<table width="550" border="0" align="center" cellpadding="3" cellspacing="2">

  <tr class="admintd">
    <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Index</font></strong></td>
    <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL Tag </font></strong></td>
    <td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Page Name </font></strong></td>
    <td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Level </font></strong></td>
	<td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date</font></strong></td>
	<td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Menu</font></strong></td>
	<td colspan="2" align="center" nowrap="nowrap">&nbsp;</td>
  </tr>
<?
	$res=@lfmsql_query("SELECT p.*,m.accname,DATE_FORMAT(publishdate,'%Y-%m-%d') AS pdate FROM ".$prefix."memberpages p LEFT JOIN ".$prefix."membertypes m ON p.mtype=m.mtid");
	while($mpages=@lfmsql_fetch_object($res))
	{
?>
  <tr id="listpage<?=$mpages->pageid;?>">
    <td align="center" nowrap="nowrap"><?=$mpages->pageindex;?></td>
    <td nowrap="nowrap"><?=$mpages->pagetag;?></td>
    <td align="center" nowrap="nowrap"><?=$mpages->pagename;?></td>
    <td align="center" nowrap="nowrap"><?=$mpages->accname;?></td>
    <td align="center" nowrap="nowrap"><?=$mpages->pdate;?></td>
    <td align="center" nowrap="nowrap"><? if($mpages->menu ==1) { ?><img src="../images/tick.jpg" /><? } else { ?><img src="../images/cancel.jpg" width="16" height="16" /><? } ?></td>
    <td align="center" nowrap="nowrap"><a href="javascript:toggleDisplay('editpage<?=$mpages->pageid;?>','listpage<?=$mpages->pageid;?>');"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
    <td align="center" nowrap="nowrap"><a href="javascript:delPage(<?=$mpages->pageid;?>);"><img src="../images/del.png" width="16" height="16" border="0" /></a></td>
  </tr><tr><td colspan="9">
<div align="center" id="editpage<?=$mpages->pageid;?>" style="display: none;">
<form method="post" action="admin.php?f=cd" name="fcontent">
<table width="100%" style="border: thin solid; border-width: 1px;">
  <tr bgcolor="#FFFF99">
    <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Index</font></strong></td>
    <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL Tag </font></strong></td>
    <td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Page Name </font></strong></td>
    <td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Level </font></strong></td>
    <td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Publish Date</font></strong></td>
    <td align="center" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Menu</font></strong></td>
  </tr>
    <tr>
      <td align="center"><input name="epageindex" type="text" size="2" value="<?=$mpages->pageindex;?>" /></td>
    <td><input name="pagetag" type="text" id="pagetag" size="8" maxlength="8" value="<?=$mpages->pagetag;?>" /></td>
    <td align="center"><input name="pagename" type="text" id="pagename" value="<?=$mpages->pagename;?>" /></td>
    <td align="center"><select name="mtype" id="mtype">
		  <option value="0">-</option>
		  <option value="9999"<? if($mpages->mtype == 9999) { echo(" selected=\"selected\""); } ?>>None</option>
		  	<? GetMtype($mpages->mtype); ?>
          </select></td>
    <td align="center"><input name="publishdate" id="publishdate" type="text" value="<?=$mpages->pdate;?>" width="80" /><input type="button" id="mybutton" value=".." /></td>
    <td align="center"><input name="menu" type="checkbox" id="menu" value="1" <? if($mpages->menu==1) { ?> checked="checked" <? } ?>/></td>
    </tr>
  <tr>
    <td colspan="7" align="center">
	  <input type="hidden" name="pageid" value="<?=$mpages->pageid;?>">
      <input type="submit" name="Submit" value="Update Page" />
      <input name="Cancel2" type="button" id="Cancel2" value="Cancel" onClick="javascript:toggleDisplay('editpage<?=$mpages->pageid;?>','listpage<?=$mpages->pageid;?>');"/>    </td>
    </tr>
</table>
</form>
</div>
<? } ?>
</td></tr>
</table>
<p align="center"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Page Templates</font></strong></p>
<p>
  <?
	if(isset($_POST["pageid"]))
	{
		$pageid=$_POST["pageid"];
		$res=@lfmsql_query("SELECT * FROM ".$prefix."memberpages WHERE pageid=".$pageid);
		$memberpage=@lfmsql_fetch_object($res);
		$pagedata=$memberpage->pagedata;
	}
?>
</p>
<form name="cdtform" id="cdtform" method="post" action="admin.php?f=cd">
<table width="760" border="0" align="center" cellpadding="3" cellspacing="2">
  <tr>
    <td align="center"><table width="100%" border="0" cellpadding="3" cellspacing="0">
      <tr>
        <td><select name="pageid"  onChange="javascript:window.document.cdtform.submit();">
		<option value="0">Select...</option>
		<? GetPages($pageid); ?>
        </select>
          <input name="Submit" type="submit" id="Submit" value="Update Template" />&nbsp; &nbsp;<span style="cursor:pointer; color:blue; size:10px;" onClick="window.open('contentrestore.php?pageid=<? echo($pageid); ?>','restore_template','width=500, height=400');"><b>Restore Previous Version</b></span></td>
        </tr>
      <tr class="admintd">
        <td nowrap="nowrap"><strong>URL Tag:
          <input name="pagetag" type="text" id="pagetag" size="8" maxlength="8" value="<?=$memberpage->pagetag;?>" readonly="readonly" />
          Page Name:</strong>
          <input name="pagename" type="text" id="pagename" value="<?=$memberpage->pagename;?>" readonly="readonly" />
          <strong>Member Level:</strong>
          <select name="mtype" id="mtype" disabled="disabled">
		  <option value="0">-</option>
		  <option value="9999"<? if($memberpage->mtype == 9999) { echo(" selected=\"selected\""); } ?>>None</option>
		  	<? GetMtype($memberpage->mtype); ?>
          </select>
		  </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>
<?
	if(isset($_POST["pageid"]))
	{
		// Decode template data
    	if (function_exists('html_entity_decode'))
    	{
	    	$html=html_entity_decode($pagedata);
		}
    	else
    	{
        	$html=unhtmlentities($pagedata);
    	}
	?>
 <textarea class="ckeditor" name="pagedata" cols="80" rows="8" id="pagedata"><?=$html;?></textarea>
<br />
<?	}
?>
	</td>
  </tr>
</table>
</form>
<table width="760" border="1" align="center" cellpadding="2" cellspacing="3" bordercolor="#333333" bgcolor="#FFFF99">
  <tr>
    <td align="left" valign="top" bordercolor="#FFFF99"><p><font size="2"><strong><font face="Verdana, Arial, Helvetica, sans-serif">From this area you can add new pages to any member level.</font></strong></font></p>
      <ul>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Index </strong>- This option allows you to set the order that the links will be shown across the member menu. Simply number them and they will display in the order of the numbers with the lowest index being the left hand side and the highest index being the right. </font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>URL Tag</strong> - This is the string that will show at the end of the URL <br />
        (e.g. If the tag was set to 'mypage' the URL would show members.php?page=mypage).</font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Page Name</strong> - This is the name that will show on the automatically generated members menu.</font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Member Level </strong>- The membership level that this page will be available to.</font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Date</strong> - The Date field displays the 'Publish Date'. You can use the publish date to prevent members from accessing previously published material. A comparison is done between the Publish Date and the member's join date. If the join date is after the publish date, then the member will not be able to access the page. This feature is enabled on a per-member-level basis. In the member level admin area you have the option of selecting the 'Use Publish Date' option. </font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Menu </strong>- This option defines whether the page will be shown on the member menu. This is handy in situations where you want to define your own menu structure from within a content page. You can add links using the tag (see URL Tag) and hide the page from the member menu. </font></li>
      </ul></td>
    <td width="250" align="left" valign="top" bgcolor="#DFFFDF"><p><strong>The following macros may be used: </strong></p>
      <p>#FIRSTNAME# - Member first name <br />
        #LASTNAME# - Member last name <br />
        #USERNAME# - Member login username <br />
        #PASSWORD# - Member password<br />
        #AFFILIATEID# - The members affiliate ID <br />
        #SITENAME# - The name of this site<br />
    #REFURL# - The referral link</p></td>
  </tr>
</table>
