<?php

// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

echo("<html>
<body>
<center>
");

if (!isset($_GET['pageid']) || !is_numeric($_GET['pageid'])) {
	echo("<p><font size=\"2\">Please select a template.</font></p>");
	exit;
}

$template_name = $_GET['pageid'];

$template_exists = mysql_result(mysql_query("SELECT COUNT(*) from ".$prefix."memberpages WHERE pageid='".$template_name."'"), 0);

if ($template_exists < 1) {
	echo("<p><font size=\"2\">Template not found.</font></p>");
	exit;
}

if ($_GET['restore'] == "go" && is_numeric($_POST['restore_version'])) {

	// Restore Previously Saved Version
	$getrestoretemplate = mysql_query("SELECT template_data from ".$prefix."pages_backups WHERE id='".$_POST['restore_version']."' AND pageid=".$template_name) or die(mysql_error());
	if (mysql_num_rows($getrestoretemplate) > 0) {
		$restoredata = addslashes(mysql_result($getrestoretemplate, 0, "template_data"));
		mysql_query("UPDATE ".$prefix."memberpages SET pagedata='".$restoredata."' WHERE pageid=".$template_name) or die(mysql_error());
		echo "<script language=\"JavaScript\">";
		echo "window.opener.location.href = window.opener.location.href;";
		echo "</script>";
		echo("<h4><b>Template Restored Successfully</b></h4><br><br><input name=\"closewin\" type=\"button\" onClick=\"javascript:self.close();\" value=\"Close Window\" />");
		exit;
	} else {
		echo("<p><font size=\"2\">Could not retrieve previously saved version.</font></p>");
		exit;
	}
	
}

####################

//Begin main page

####################

$get_firstid = mysql_query("SELECT id FROM ".$prefix."pages_backups WHERE pageid='".$template_name."' ORDER BY savetime ASC LIMIT 1");
if (mysql_num_rows($get_firstid) > 0) {
	$firstid = mysql_result($get_firstid, 0, "id");
} else {
	$firstid = 0;
}

?>

<script language="javascript">

var version_num = <? echo($firstid); ?>;

function setPreview() {
	version_num = document.forms["restoreform"]["restore_version"].value;
}

function openPreview() {
	var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=800,height=600";
	var URL = "/templatepreview.php?pageid=<? echo($template_name); ?>&restore_version="+version_num;
	popup = window.open(URL,"PreviewTemplate",windowprops);
}
</script>

<?

echo("<h4><b>Restore Previous Version</b></h4>
<p align=\"left\"><font size=\"2\">You can restore a template to a previous version of this page you saved.</font></p>");

$get_versions = mysql_query("SELECT id, savetime FROM ".$prefix."pages_backups WHERE pageid='".$template_name."' ORDER BY savetime ASC");

if (mysql_num_rows($get_versions) == 0) {
	echo("<p><font size=\"2\">No previous versions found.</font></p>");
	exit;
}

echo("<font size=\"2\">Select a version to restore:</font>
<form name=\"restoreform\" id=\"restoreform\" action=\"contentrestore.php?pageid=".$template_name."&restore=go\" method=\"post\">
<select name=\"restore_version\" id=\"restore_version\" onChange=\"setPreview()\">
");

for ($i = 0; $i < mysql_num_rows($get_versions); $i++) {
	$saveid = mysql_result($get_versions, $i, "id");
	$savetime = mysql_result($get_versions, $i, "savetime");
	echo("<option value=\"".$saveid."\">".$savetime."</option>");
}

echo("
</select>

<input type=\"button\" onclick=\"openPreview()\" name=\"showpreview\" value=\"View Preview\" />

<input type=\"submit\" name=\"restoresubmit\" value=\"Restore Template\" />

</form>
");


echo("
<br><br>

</center>
</body>
</html>");

exit;

?>