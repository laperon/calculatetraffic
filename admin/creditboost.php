<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

$errormess = "";

?>

<link href="styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol.css">

<center><br><br>

<p align=left><b>You can use the Credit Boost to schedule surfing ratio increases, credit specials, or both.  For example, a Surfing Ratio multiplier of 2 will temporarily double the surfing ratios for each account level.  A Credit Purchase multiplier of 2 will double the number of credits members receive when purchasing credits from the Buy Credits page.<br><br>Enter 0 if you don't want to use a multiplier for a Credit Boost special.</b></p>

<?

//Add A New Boost
if ($_GET['addboost'] == "yes") {

	$starttime = mktime(0,0,0,substr($_POST['sdate'],5,2),substr($_POST['sdate'],8),substr($_POST['sdate'],0,4));
	$endtime = mktime(0,0,0,substr($_POST['edate'],5,2),substr($_POST['edate'],8),substr($_POST['edate'],0,4))+86399;

	@lfmsql_query("Insert into ".$prefix."cboost (`starttime`, `endtime`, `surfboost`, `buyboost`, `acctype`, `surfimage`, `surftext`, `buytext`) values ('".$starttime."', '".$endtime."', '".$_POST['surfboost']."', '".$_POST['buyboost']."', ".$_POST['acctype'].", '".$_POST['surfimage']."', '".$_POST['surftext']."', '".$_POST['buytext']."')");
}

//Delete A Boost
if ($_GET['delboost'] == "yes" && is_numeric($_GET['boostid']) && $_GET['boostid'] > 0) {
	@lfmsql_query("Delete from ".$prefix."cboost where id=".$_GET['boostid']." limit 1");
}

//Begin Main Page

if ($errormess != "") {
	echo("<p><b>".$errormess."</b></p>");
}

?>

<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><p>&nbsp;</p><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td>
	<form action="admin.php?f=cboost&addboost=yes" method="post">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Start Date</strong></font></td>
        <td colspan="2" class="formfield">
        <input name="sdate" type id="DPC_sdate_YYYY-MM-DD" value="" width="80" text>
        </td>
        <td align="center"><a href="#" title="The date when the credit boost will begin.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>End Date</strong></font></td>
        <td colspan="2" class="formfield">
        <input name="edate" type id="DPC_edate_YYYY-MM-DD" value="" width="80" text>
        </td>
        <td align="center"><a href="#" title="The date when the credit boost will end.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Surf Ratio Multiplier</strong></font></td>
        <td colspan="2" class="formfield">
        <input name="surfboost" type="text" class="formfield" id="surfboost" value="0" size="20" />
        </td>
        <td align="center"><a href="#" title="The rate in which the surfing ratios will be multiplied during the credit boost.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>

      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Credit Purchase Multiplier</strong></font></td>
        <td colspan="2" class="formfield">
	<input name="buyboost" type="text" class="formfield" id="buyboost" value="0" size="20" />
        </td>
        <td align="center"><a href="#" title="The rate in which credit purchases on the Buy Credits page will be multiplied during the credit boost.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>


      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surf Special Image</font> </strong></td>
        <td colspan="2" class="formfield">
        <input name="surfimage" type="text" class="formfield" id="surfimage" value="/images/cboost.jpg" size="20" />
        </td>
        <td align="center"><a href="#" title="If this is a Surf Ratio boost, the image that will be shown on the surfbar while the ratios are increased."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surf Special Text</font> </strong></td>
        <td colspan="2" class="formfield">
        <input name="surftext" type="text" class="formfield" id="surftext" value="Surfing Ratios Are Doubled Today" size="20" />
        </td>
        <td align="center"><a href="#" title="The text shown if a member clicks on the surf special image."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Buy Page Text</font> </strong></td>
        <td colspan="2" class="formfield">
        <input name="buytext" type="text" class="formfield" id="buytext" value="SPECIAL: Credit Purchases Are Doubled Today" size="20" />
        </td>
        <td align="center"><a href="#" title="If this is a credit purchase boost, this text will appear on the Buy Credits page."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Account Type</font> </strong></td>
        <td colspan="2" class="formfield">
<?

echo("<select name=acctype><option value=0>All Members</option>");

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
echo("<option value=$accid"); if($acctype==$accid){echo(" selected");} echo(">$accname</option>");
}
echo("</select>");

?>
        </td>
        <td align="center"><a href="#" title="The membership level that will receive this credit boost."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>

      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Add Boost" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>

<BR><BR>

<?

echo("<table style=\"border:thin solid #000 ; border-width: 1px;\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\" width=\"400\">
  <tr>
    <td colspan=\"6\" align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Scheduled Credit Boosts</font> </strong></td>
  </tr>
  
  <tr>
    <td align=\"center\" class=\"admintd\">Start Date</td><td align=\"center\" class=\"admintd\">End Date</td><td align=\"center\" class=\"admintd\">Ratio Multiplier</td><td align=\"center\" class=\"admintd\">Purchase Multiplier</td><td align=\"center\" class=\"admintd\">Account Type</td><td align=\"center\" class=\"admintd\">&nbsp;</td>
  </tr>
  ");
  
$t = time();

$getboosts = lfmsql_query("Select * from ".$prefix."cboost where `endtime`>$t order by starttime asc");
for ($j = 0; $j < lfmsql_num_rows($getboosts); $j++) {
$boostid = lfmsql_result($getboosts, $j, "id");
$starttime = lfmsql_result($getboosts, $j, "starttime");
$endtime = lfmsql_result($getboosts, $j, "endtime");
$surfboost = lfmsql_result($getboosts, $j, "surfboost");
$buyboost = lfmsql_result($getboosts, $j, "buyboost");
$acctype = lfmsql_result($getboosts, $j, "acctype");

if ($acctype == 0) {
	$accname = "All Members";
} else {
	$getaccname = lfmsql_query("Select accname from `".$prefix."membertypes` where mtid=$acctype limit 1");
	if (lfmsql_num_rows($getaccname) > 0) {
	$accname = lfmsql_result($getaccname, 0, "accname");
	} else {
	$accname = "None";
	}
}

$starttime = date("Y-m-d",$starttime);
$endtime = date("Y-m-d",$endtime);

echo("<tr>
<td align=\"center\">".$starttime."</td>
<td align=\"center\">".$endtime."</td>
<td align=\"center\">".$surfboost."</td>
<td align=\"center\">".$buyboost."</td>
<td align=\"center\">".$accname."</td>

<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=cboost&delboost=yes&boostid=".$boostid."\">
<td><input type=submit value=\"Delete\"></td>
</form>
</tr>");
}

echo("</table>");

?>

</center>

<p>&nbsp;</p>