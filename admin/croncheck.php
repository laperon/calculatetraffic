<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.29
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>LFMTE Cron Check</title>
<style>
div#navbar {font-family: Verdana, Helvetica, Arial, sans-serif; font-size: .9em; font-weight: bold; text-align: center; background-color: #bbccbb; border:

ridge #2b4460 thin; font-weight: bold; padding: 3px; width: 300px;} #navbar ul li a:link, #navbar ul li a:visited {color: #3d4a5e;
background-color:

#abcbb8; text-decoration: none;
display: inline;} #navbar ul li a:hover, #navbar ul li a.current {color: #3d4a5e;
background-color: #abcbb8;

text-decoration: none;}

#navbar ul li {margin: 0px; padding: 0px; list-style-type: none; display: inline;} #navbar ul {margin: 0px; padding: 0px;}
</style>
</head>
<body>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" bgcolor="#1972CE"><font color="#FFFFFF" size="4" face="Verdana, Arial, Helvetica, sans-serif"><strong><img src="LFM_header.jpg" width="760" height="134" /><br>LFMTE Cron Jobs</strong></font></td>
    <td rowspan="2" align="center" bgcolor="#FFFFFF"><img src="../images/vert.gif" width="1" height="400" /></td>
  </tr>
<?

require_once "../inc/filter.php";
include "../inc/config.php";
include_once "../inc/sql_funcs.php";
require_once "../inc/funcs.php";
mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname) or die( "Unable to select database");


?>
  <tr>
    <td align="center" valign="top"><table border="0" align="center">
      <tr>
        <td align="left" nowrap="nowrap"><p>&nbsp;</p>
          <p>The following cron jobs need to be added to your cPanel account for the LFMTE script to work correctly.<br />
          The status of each cron job is shown below.  Please note it can take up to 24 hours after adding a cron<br />
          job to your cPanel account for the status below to change to "Working".  Also, the f4cron.php cron job<br />
          may show as "No Active Sites" if there are no active sites in the database.</p>
          
          <?php

// Get Cron Paths
$php_path = @exec( 'which php');
if ($php_path == "") {
	$php_path = "/usr/bin/php";
}

if(isset($_SERVER['PATH_TRANSLATED'])) {
	$installerpath=$_SERVER['PATH_TRANSLATED'];
} else {
	$installerpath=$_SERVER['SCRIPT_FILENAME'];
}
$fullpath = $installerpath;
$mail_path=substr($fullpath,0,strrpos($fullpath, "/"));
$maint=$mail_path."/maint.php > /dev/null 2>&1";

$maintcmd = $php_path." -f ".$maint;
$sgf4cron = $php_path." -f ".$_SERVER[DOCUMENT_ROOT]."/f4cron.php > /dev/null 2>&1";
$sgmban = $php_path." -f ".$_SERVER[DOCUMENT_ROOT]."/mbancron.php > /dev/null 2>&1";
// End Get Cron Paths

// Get Cron Status

$currentdate = date("Y-m-d");
$yesterdate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));

$getlastcron = mysql_query("Select tecron from ".$prefix."settings");

if (mysql_num_rows($getlastcron) == 0) {
	$maintstatus = "ERROR";
} else {
	$lastcron = mysql_result($getlastcron, 0, "tecron");
	
	if ($lastcron == $currentdate) {
		$maintstatus = "Working";
	} else {
		$maintstatus = "Not Working";
	}
}

$getlastmon = mysql_query("Select f4check FROM ".$prefix."msites ORDER BY f4check DESC LIMIT 1");
if (mysql_num_rows($getlastmon) > 0) {
	$lastmon = mysql_result($getlastmon, 0, "f4check");
	if ((time() - $lastmon) > 1800) {
		$monitorstatus = "Not Working";
	} else {
		$monitorstatus = "Working";
	}
} else {
	$monitorstatus = "No Active Sites";
}

$getlastmon = mysql_query("Select value FROM sg_settings where name='lastcbmon'");
if (mysql_num_rows($getlastmon) > 0) {
	$lastcbmon = mysql_result($getlastmon, 0, "value");
	if ((time() - $lastcbmon) > 1800) {
		$chargebackstatus = "Not Working";
	} else {
		$chargebackstatus = "Working";
	}
} else {
	$chargebackstatus = "ERROR";
}

// End Get Cron Status

?>

Every night at 12AM: <b><?=$maintcmd;?></b><br />Status: <?=$maintstatus;?><br /><br />
Run every 10 Minutes: <b><?=$sgf4cron;?></b><br />Status: <?=$monitorstatus;?><br /><br />
Run every 10 Minutes: <b><?=$sgmban;?></b><br />Status: <?=$chargebackstatus;?><br /><br />

      </td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>

    </table>	</td>
    <td rowspan="2" align="center" bgcolor="#FFFFFF"><img src="../images/vert.gif" width="1" /></td>
  </tr>
  </table>
  </p>
  <p>&nbsp;  </p>
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#1972CE">
  <tr>
    <td align="center"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>LFMTE Copyright &copy;2006-2014 AKH Media Group and Josh Abbott. All Rights Reserved. </strong></font></td>
  </tr>
</table>
</body>
</html>