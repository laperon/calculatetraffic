<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

$errormess = "";

echo("<center><br><br>");

// Enable/Disable Required
if (is_numeric($_GET['enablerequired'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET required=1 WHERE id=".$_GET['enablerequired']);
}

if (is_numeric($_GET['disablerequired'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET required=0 WHERE id=".$_GET['disablerequired']);
}
// End Enable/Disable Required

// Enable/Disable On Signup
if (is_numeric($_GET['enablesignup'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET onsignup=1 WHERE id=".$_GET['enablesignup']);
}

if (is_numeric($_GET['disablesignup'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET onsignup=0 WHERE id=".$_GET['disablesignup']);
}
// End Enable/Disable On Signup

// Enable/Disable On Profile
if (is_numeric($_GET['enableprofile'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET onprofile=1 WHERE id=".$_GET['enableprofile']);
}

if (is_numeric($_GET['disableprofile'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET onprofile=0 WHERE id=".$_GET['disableprofile']);
}
// End Enable/Disable On Profile

// Enable/Disable Searchable
if (is_numeric($_GET['enablesearchable'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET searchable=1 WHERE id=".$_GET['enablesearchable']);
}

if (is_numeric($_GET['disablesearchable'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET searchable=0 WHERE id=".$_GET['disablesearchable']);
}
// End Enable/Disable Searchable

// Enable/Disable On IPN
if (is_numeric($_GET['enableipn'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET onipn=1 WHERE id=".$_GET['enableipn']);
}

if (is_numeric($_GET['disableipn'])) {
	lfmsql_query("UPDATE `".$prefix."customfields` SET onipn=0 WHERE id=".$_GET['disableipn']);
}
// End Enable/Disable On IPN

//Change A Rank
$rank = $_GET['rank'];
if (is_numeric($_GET['fieldid']) && ($rank == "up" || $rank == "down")) {

	$runchange = "yes";
	$id = $_GET['fieldid'];
	
	$checkfirstrow = lfmsql_query("Select rank from `".$prefix."customfields` order by rank asc");
	$firstrow = lfmsql_result($checkfirstrow, 0, "rank");
	
	$checklastrow = lfmsql_query("Select rank from `".$prefix."customfields` order by rank desc");
	$lastrow = lfmsql_result($checklastrow, 0, "rank");
	
	if (($rank=="up") && ($id > $firstrow)) {
		$change=$id-1;
	} elseif (($rank=="down") && ($id < $lastrow)) {
		$change=$id+1;
	} else {
		$runchange = "no";
	}
	
	if ($runchange == "yes") {
		@lfmsql_query("Update `".$prefix."customfields` set rank=0 where rank=$id");
		@lfmsql_query("Update `".$prefix."customfields` set rank=$id where rank=$change");
		@lfmsql_query("Update `".$prefix."customfields` set rank=$change where rank=0");
		@lfmsql_query("ALTER TABLE `".$prefix."customfields` ORDER BY rank;");
	}
	
}
//End Change A Rank

if ($_GET['changename'] == "yes" && is_numeric($_GET['fieldid']) && trim($_POST['newname']) != "") {
	lfmsql_query("UPDATE `".$prefix."customfields` SET name='".trim($_POST['newname'])."' WHERE id=".$_GET['fieldid']) or die(lfmsql_error());
}

//Begin Main Page

?>
<link href="styles.css" rel="stylesheet" type="text/css" />

<script language="javascript">
	function addField()
	{
		var windowprops = "location=no,scrollbars=no,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=550,height=400"; 
	 
		var URL = "addfield.php"; 
		popup = window.open(URL,"NewFieldPopup",windowprops);	
	}
	function delField(fieldid)
	{
		var windowprops = "location=no,scrollbars=no,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=500,height=300"; 
	 
		var URL = "delfield.php?id="+fieldid; 
		popup = window.open(URL,"DeleteFieldPopup",windowprops);	
	}
</script>

<p><a href="javascript:addField()"><input type="button" value="Add New Field" /></a>

<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td align="center"><table width="750" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr class="admintd">
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Rank</font></strong></td>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Name</font></strong></td>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Type</font></strong></td>
        <td width="50" align="left" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Required</font></strong></td>
        <td width="50" align="left" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">On Signup</font></strong></td>
        <td width="50" align="left" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">On Profile</font></strong></td>
        <td width="50" align="left" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Searchable</font></strong></td>
	<td width="50" align="left" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">In Sales Packages</font></strong></td>
        <td align="center" nowrap="NOWRAP">&nbsp;</td>
      </tr>
<?php

$getfields = lfmsql_query("SELECT * FROM `".$prefix."customfields` ORDER BY rank ASC");
for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {

	$fieldid = lfmsql_result($getfields, $i, "id");
	$rank = lfmsql_result($getfields, $i, "rank");
	$name = lfmsql_result($getfields, $i, "name");
	$fieldtype = lfmsql_result($getfields, $i, "type");
	$required = lfmsql_result($getfields, $i, "required");
	$onsignup = lfmsql_result($getfields, $i, "onsignup");
	$onprofile = lfmsql_result($getfields, $i, "onprofile");
	$searchable = lfmsql_result($getfields, $i, "searchable");
	$onipn = lfmsql_result($getfields, $i, "onipn");
	
	if ($required == 0) {
		$requiredimg = "<a href=\"admin.php?f=cfields&enablerequired=".$fieldid."\"><img border=\"0\" src=\"checkbox_disable.jpg\"></a>";
	} else {
		$requiredimg = "<a href=\"admin.php?f=cfields&disablerequired=".$fieldid."\"><img border=\"0\" src=\"checkbox_enable.jpg\"></a>";
	}
	
	if ($onsignup == 0) {
		$onsignupimg = "<a href=\"admin.php?f=cfields&enablesignup=".$fieldid."\"><img border=\"0\" src=\"checkbox_disable.jpg\"></a>";
	} else {
		$onsignupimg = "<a href=\"admin.php?f=cfields&disablesignup=".$fieldid."\"><img border=\"0\" src=\"checkbox_enable.jpg\"></a>";
	}
	
	if ($onprofile == 0) {
		$onprofileimg = "<a href=\"admin.php?f=cfields&enableprofile=".$fieldid."\"><img border=\"0\" src=\"checkbox_disable.jpg\"></a>";
	} else {
		$onprofileimg = "<a href=\"admin.php?f=cfields&disableprofile=".$fieldid."\"><img border=\"0\" src=\"checkbox_enable.jpg\"></a>";
	}
	
	if ($searchable == 0) {
		$searchableimg = "<a href=\"admin.php?f=cfields&enablesearchable=".$fieldid."\"><img border=\"0\" src=\"checkbox_disable.jpg\"></a>";
	} else {
		$searchableimg = "<a href=\"admin.php?f=cfields&disablesearchable=".$fieldid."\"><img border=\"0\" src=\"checkbox_enable.jpg\"></a>";
	}
	
	if ($onipn == 0) {
		$onipnimg = "<a href=\"admin.php?f=cfields&enableipn=".$fieldid."\"><img border=\"0\" src=\"checkbox_disable.jpg\"></a>";
	} else {
		$onipnimg = "<a href=\"admin.php?f=cfields&disableipn=".$fieldid."\"><img border=\"0\" src=\"checkbox_enable.jpg\"></a>";
	}
	
	if ($fieldtype == 1) {
		$fieldtypetext = "<b>Text Input</b>";
	} else {
		$fieldtypetext = "<b>Dropdown Options</b><br><span style=\"cursor:pointer; color:blue; size:10px;\" onClick=\"window.open('/admin/editfieldops.php?fieldid=".$fieldid."','options','scrollbars=no, menubars=no, width=530, height=300');\">Edit Options</span>";
	}
	
	if($bgcolor == "#FFFFFF") {
		$bgcolor="#DDDDDD";
	} else {
		$bgcolor="#FFFFFF";
	}
	
	?>

      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
      
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <? echo('<a href=admin.php?f=cfields&rank=up&fieldid='.$rank.'><img border=0 src="../images/move_up.gif"></a><a href=admin.php?f=cfields&rank=down&fieldid='.$rank.'><img border=0 src="../images/move_down.gif"></a>'); ?>
        </font></td>
        
        <td align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <form action="admin.php?f=cfields&changename=yes&fieldid=<?=$fieldid;?>" method="post" style="margin:0px; padding:0px;"><input type="text" name="newname" size="20" maxlength="25" value="<?=$name;?>"><input type="submit" name="Submit" value="Edit"></form>
        </font></td>
        
        <td align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <? echo($fieldtypetext); ?>
        </font></td>
        
        <td align="center">
          <? echo($requiredimg); ?>
        </td>
        
        <td align="center">
          <? echo($onsignupimg); ?>
        </td>
        
        <td align="center">
          <? echo($onprofileimg); ?>
        </td>
        
        <td align="center">
          <? echo($searchableimg); ?>
        </td>
        
        <td align="center">
          <? echo($onipnimg); ?>
        </td>

        <td width="24" align="center" bgcolor="<?=$bgcolor;?>"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:delField(<?=$fieldid;?>)"><img src="../images/del.png" alt="Delete Field" width="16" height="16" border="0" /></a>
	</font></td>
		
      </tr>
        
        <?php
}

?>
    </table>	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>

<br />
<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  
  <tr>
    <td align="left"><div class="lfm_descr">
      
      <p>Custom fields are additional pieces of information that you can add to the accounts of your members.  They can be extra fields that you want members to enter when they signup (such as age and gender), or they can be hidden fields that are only accessible in the Admin Panel.</p>
      
      <p>A custom field can also be added to your sales packages, so you can sell them as a product.  For example, if you are running a web site that sells candy, you could create a custom field called "Candy Bars".  Then you can create a sales package of 5 Candy Bars for $5.  Whenever somebody buys that sales package, the software will automatically add 5 Candy Bars to their account.</p>
      
      <p>All custom fields are searchable from the Admin Panel, and can be displayed in your Member Grid.  You can also display a custom field on a template by using the macro #Field Name#, and replacing "Field Name" with the name label you set for that field.</p>
    
      </div></td>
  </tr>
</table>
</div>
<p>&nbsp;</p>

<?php
exit;
?>