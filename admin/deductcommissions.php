<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
$errmsg="";

// Update user record and refresh main admin page
if(($_POST["DeductCommissions"] == "Deduct Commissions") && ($_GET["process"] == "deduct"))
{
	$deducttype = $_POST['deducttype'];
	$deductamount = $_POST['deductamount'];
	$reasontext = $_POST['reasontext'];
	
	if ($deducttype == 0 && !is_numeric($deductamount)) {
		$errmsg = "Invalid deduct amount.";
	} elseif(isset($_POST["memcheck"])) {
	
		$deductamount = 0 - abs($deductamount);
	
		while (list ($key,$val) = @each ($_POST["memcheck"])) {

			if ($deducttype == 0) {
			
				// Added as a prize to not affect leaderboards
				if ($deductamount < 0) {
					@lfmsql_query("Insert into ".$prefix."sales (affid, saledate, itemid, itemname, itemamount, commission, prize) values (".$val.", NOW(), 0, '".$reasontext."', '0.00', '".$deductamount."', 1)");
					$msg = "Commissions Deducted";
				}
				
			} elseif ($deducttype == 1) {
				
				$getcurrentbal = lfmsql_query("SELECT SUM(commission) as ctotal FROM ".$prefix."sales WHERE affid=".$val." AND ".$prefix."sales.status IS NULL");
				$currentbal = lfmsql_result($getcurrentbal, 0, "ctotal");
				
				$deductamount = 0 - abs($currentbal);
				
				// Added as a prize to not affect leaderboards
				if ($deductamount < 0) {
					@lfmsql_query("Insert into ".$prefix."sales (affid, saledate, itemid, itemname, itemamount, commission, prize) values (".$val.", NOW(), 0, '".$reasontext."', '0.00', '".$deductamount."', 1)");
					$msg = "Commissions Deducted";
				}
				
			}
		
		}
	} else {
		$errmsg = "No members selected.";
	}
}

?>

<center>
  <p><font size="2" color="#FF0000"" face="Arial, Helvetica, sans-serif">
  <?=$errmsg;?>
  <?=$msg;?>
  </font></p>
</center>
<br><br>

<script language="javascript">

function disableAmount() {
	if (document.forms["memfrm"]["deducttype"].value == 1) {
		document.memfrm.deductamount.disabled=true;
	} else {
		document.memfrm.deductamount.disabled=false;
	}
}

</script>

<form name="memfrm" id="memfrm" method="post" action="admin.php?f=mm&process=deduct">
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Deduct Commissions</font></strong></td>
  </tr>
  <?
  
  if(isset($_POST["memcheck"])) {
  		echo("<tr><td colspan=\"2\" align=\"center\">
  		<b>Deduct Commissions From: </b><br>");
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
			echo("<input type=\"checkbox\" class=\"mcheck\" name=\"memcheck[]\" value=\"".$val."\" checked />User ID: <b>".$val."</b><br>");
		}
		echo("</td></tr>");
  } else {
  	echo("<tr><td colspan=\"2\" align=\"center\"><b>No Members Selected</b></td></tr>");
  	exit;
  }

  ?>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Deduction Type:</font></strong></td>
    <td align="left"><select name="deducttype" id="deducttype" onChange="disableAmount()" />
    <option value="0">Deduct Amount (Enter In Field Below)</option>
    <option value="1">Subtract Balance To 0.00</option>
    </select></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Deduct Amount:</font></strong></td>
    <td align="left"><input name="deductamount" type="text" class="form" id="deductamount" value="0.00" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Reason:</font></strong></td>
    <td align="left"><input name="reasontext" maxlength="25" type="text" class="form" id="reasontext" value="Purchase" /></td>
  </tr>
  
  <tr>
    <td colspan="2" align="center"><input name="DeductCommissions" type="submit" class="form" value="Deduct Commissions" /></td>
  </tr>
</table>
</form>

</body>
</html>
