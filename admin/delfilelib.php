<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the group ID
	if(isset($_GET["fileid"]))
	{ $fileid=$_GET["fileid"]; }
	 else if(isset($_POST["fileid"]))
	{ $fileid=$_POST["fileid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Yes - Delete")
{
	// Delete the group
	$qry="DELETE FROM ".$prefix."filelib WHERE fileid=".$fileid;
	@mysql_query($qry) or die("Unable to delete file entry: ".mysql_error());

	$msg="<center><font color=\"red\">FILE ENTRY DELETED!</font></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

// Get current group
	$qry="SELECT * FROM ".$prefix."filelib WHERE fileid=".$fileid;
	$res=@mysql_query($qry);
	$mrow=@mysql_fetch_object($res);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delete FIle Entry</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
</head>
<body>

<?
	if($_POST["Submit"] == "Yes - Delete")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#FF0000"><strong><font color="#FFFFFF" size="3" face="Verdana, Arial, Helvetica, sans-serif">Delete File Entry</font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">FILE  ENTRY DELETED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
} else {
?>
<form name="delfrm" method="post" action="delfilelib.php">
<input type="hidden" name="fileid" value="<?=$fileid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#FF0000"><strong><font color="#FFFFFF" size="3" face="Verdana, Arial, Helvetica, sans-serif">Delete File Information </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap">
	<p align="center">&nbsp;</p>
      <p align="center"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Are you sure you <br />
      want to do this?</font> </strong></p>
      <p align="center">&nbsp;</p></td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="No - Cancel" onClick="javascript:self.close();" />
    <input type="submit" name="Submit" value="Yes - Delete" /></td>
  </tr>
</table>
</form>
<?
}
?>
</body>
</html>
