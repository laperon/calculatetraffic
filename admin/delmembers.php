<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<style type="text/css">
<!--
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold; }
-->
</style>
</head>
<body>
<?
	// Check whether this is an email to all or selected members
	if($_POST["Delchecked"] == "Delete Selected")
	{
		if(isset($_POST["memcheck"]))
		{
			$emailarray = array();
?>
<center>
<h2><font face="Verdana, Arial, Helvetica, sans-serif">Delete Members</font></h2>
<font face="Verdana, Arial, Helvetica, sans-serif">The following members will be permanently deleted:
<?
			while (list ($key,$val) = @each ($_POST["memcheck"])) 
			{
				$eqry="SELECT Id,firstname,lastname,email FROM ".$prefix."members WHERE Id=".$val;
				$eres=@mysql_query($eqry);
				$erow=@mysql_fetch_array($eres);
				echo $erow["firstname"]." ".$erow["lastname"]." (".$erow["email"].")<br>";
				array_push($emailarray,$erow["email"]);
			}
		
			$msg="Deleting Selected Members";
			$gvar="sel";
		}
	}
?>
</font>
</center>
end
</body>
</html>
