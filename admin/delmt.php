<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the group ID
	if(isset($_GET["mtid"]))
	{ $mtid=$_GET["mtid"]; }
	 else if(isset($_POST["mtid"]))
	{ $mtid=$_POST["mtid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update membertypes table
if($_POST["Submit"] == "Yes - Delete")
{
	// Move existing members
	mysql_query("UPDATE ".$prefix."members SET mtype=".$_POST["destmt"]." WHERE mtype=$mtid") or die("Unable to move members: ".mysql_error());

	// Delete the membertype
	$qry="DELETE FROM ".$prefix."membertypes WHERE mtid=".$mtid;
	@mysql_query($qry) or die("Unable to delete member type: ".mysql_error());

	$msg="<center><font color=\"red\">MEMBERTYPE DELETED!</font></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

// Get current membertype
	$qry="SELECT * FROM ".$prefix."membertypes WHERE mtid=$mtid";
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
</head>
<body>

<?
	if($_POST["Submit"] == "Yes - Delete")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#FF0000"><strong><font color="#FFFFFF" size="3" face="Verdana, Arial, Helvetica, sans-serif">Delete Group  </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">MEMBER TYPE DELETED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="delfrm" method="post" action="delmt.php">
<input type="hidden" name="mtid" value="<?=$mtid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#FF0000"><strong><font color="#FFFFFF" size="3" face="Verdana, Arial, Helvetica, sans-serif">Delete Group  </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap">
	<p align="center">
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">You are about to delete the member type:</font></p>
	<p align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"> '
	      <?=$mrow["accname"];?>
	  '<br /> 
	  <br />
	  You must choose a destination member type<br />
	  that existing '
	  <?=$mrow["accname"];?>
	  ' members will be moved to.</font></p>
	<p align="center">
	  <select name="destmt" id="destmt">
<?
	// Get the list of destination membertypes
	$dmres=@mysql_query("SELECT * FROM ".$prefix."membertypes WHERE mtid <> $mtid");
	while($dmrow=@mysql_fetch_array($dmres))
	{
?>
	<option value="<?=$dmrow["mtid"];?>"><?=$dmrow["accname"];?></option>
<?
}
?>	  
	    </select>
	</p>
    <p align="center">Are you sure you want to do this?  </p></td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="No - Cancel" onClick="javascript:self.close();" />
    <input type="submit" name="Submit" value="Yes - Delete" /></td>
  </tr>
</table>
</form>
<? 
}
?>
</body>
</html>
