<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

$msg="";

	// Get the sale ID
	if(isset($_REQUEST["saleid"]))
	{ 
		$saleid=$_REQUEST["saleid"]; 
	}
	 else
	{ 
		echo "Error: No Sale ID found!";
		exit; 
	}

// Update user record and refresh main admin page
if($_POST["Submit"] == "Yes - Delete")
{
	$qry="DELETE FROM ".$prefix."sales WHERE salesid=".$saleid;
	mysql_query($qry);
	$msg="<center><font color=\"red\">SALE DELETED!</font></center>";
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
</head>
<body>
<?
	if($_POST["Submit"] == "Yes - Delete")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#FF0000"><strong><font color="#FFFFFF" size="3" face="Verdana, Arial, Helvetica, sans-serif">Delete Sale  </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">SALE DELETED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="salefrm" method="post" action="delsale.php">
<input type="hidden" name="saleid" value="<?=$saleid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#FF0000"><strong><font color="#FFFFFF" size="3" face="Verdana, Arial, Helvetica, sans-serif">Delete Sale  </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">Are you sure you want to do this?  </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="No - Cancel" onClick="javascript:self.close();" />
    <input type="submit" name="Submit" value="Yes - Delete" /></td>
  </tr>
</table>
</form>
<?
}
?>
</body>
</html>
