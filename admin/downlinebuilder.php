<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

// Delete Program
if(trim($_GET["Submit"]) == "Delete Program")
{
 @mysql_query("DELETE FROM ".$prefix."dlbprograms WHERE id = ".$_GET['id']) or die(mysql_error());
}

// Add a new Program
if($_POST["Submit"] == "Add")
{
   @mysql_query("INSERT INTO `".$prefix."dlbprograms` SET `programId` = '".$_POST["progid"]."', `description` = '".$_POST["progdesc"]."', `programUrl` = '".$_POST["progurl"]."', `ownerAfId` = '".$_POST["progaffid"]."'");
}

$ores=@mysql_query("SELECT * FROM ".$prefix."settings");
$orow=@mysql_fetch_array($ores);
$oto = $orow["ototype"];

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;

}
-->
</style>
<script type="text/javascript" src="../inc/ajax.js"></script>
<script type="text/javascript">
var ajax = new sack();
</script>
<script language="javascript" type="text/javascript">

function delProgram(id)
{
	if (confirm("Are you sure you want to delete this program?")) {
		var URL = "admin.php?f=dlb&Submit=Delete+Program&id="+id;
		document.location = URL;
	}
}

function editProgram(mid)
{
	var windowprops = "location=no,scrollbars=no,menubars=no,toolbars=no,resizable=no" + ",left=100,top=50,width=550,height=400"; 
 
	var URL = "editprogram.php?id="+mid; 
	popup = window.open(URL,"MemberPopup",windowprops);	
}

</script>
<body>
<p align="center"><font color="red"><strong><?=$msg;?></strong></font>&nbsp;</p>
<table width="760" border="0" align="center" cellpadding="4" cellspacing="0" bordercolor="#FFFFFF">
  <tr>
    <td>
      <table width="740" border="0" align="center" cellpadding="3" cellspacing="0" style="border: thin solid">
        <tr>
          <td align="center" nowrap="nowrap"><a href="#" title="The unique ID of the program"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
          <td align="center" nowrap="nowrap"><a href="#" title="The affiliate URL link of the program."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
          <td align="center" nowrap="nowrap"><a href="#" title="The Program description"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
          <td align="center" nowrap="nowrap"><a href="#" title="Edit or Delete the program."><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
        </tr>
        <tr>         
          <td nowrap="nowrap" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Program ID</font></strong></td>
          <td align="center" nowrap="nowrap" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Program Url</font></strong></td>
          <td align="center" nowrap="nowrap" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Program Description</font></strong></td>
          <td nowrap="nowrap">&nbsp;</td>
        </tr>
<?php

	$lores=@mysql_query("SELECT * FROM ".$prefix."dlbprograms order by id");
	while($lorow=@mysql_fetch_array($lores)) {
?>
      <form name="litofrm" method="POST" action="admin.php?f=dlb">
        <input type=hidden name="loid" id="loid" value="<?=$lorow['id']?>">
        <tr>
          <td valign="top" ><?=$lorow['programId']?></td>
          <td valign="top" ><?=$lorow['programUrl']?></td>
          <td valign="top" ><?=$lorow['description']?></td>          
          <td align="center" valign="top"><a href="javascript:editProgram(<?=$lorow['id'];?>)"><img src="../images/edit.png" width="16" height="16" border="0" /></a>&nbsp;&nbsp;<a href="javascript:delProgram(<?=$lorow['id'];?>)"><img src="../images/del.png" width="16" height="16" border="0" /></a></td>        

		  </tr>
      </form>
<?php
}
?>

        </table>
      </td>
      <p>&nbsp;</p></td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
<tr>
<td>
<form method="post" action="admin.php?f=dlb">
  <table style="border:thin solid #000 ; border-width: 1px;" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="3" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Add Program</font> </strong></td>
  </tr>

  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>Enter the Unique ID of this program.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Program Id:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="progid" type="text" id="progid" size="10"></td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>Enter the programs description as you wish it to appear in the members area. Some limited HTML tags are allowed.</span></a></td>
    <td align="left" valign="top" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Description:</font></strong></td>
    <td align="left" nowrap="nowrap"><textarea name="progdesc" id="progdesc" rows=5 cols=30></textarea></td>
  </tr>
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>Enter the programs affiliate URL and place {affid} where the affiliate id goes.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Program URL:</font></strong><br><font size=1>Ex. http://hitsconnect.com/index.php?ref_id={affid}</font></td>
    <td align="left" nowrap="nowrap">
	<input name="progurl" type="text" id="progurl" size="50"></td>
  </tr>  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>Enter your affiliate ID for this program. This will be used as default and when no upline affiliate ID exists.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Default Affiliate ID:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="progaffid" type="text" id="progaffid" size="10"></td>
  </tr>    
  <tr>
    <td colspan="6" align="center" bgcolor="#B0CBFD"><input type="submit" name="Submit" value="Add" /></td>
  </tr>
</table>
</form>
</td>
</tr>
</table>

