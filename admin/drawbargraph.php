<?php

// VisuGraph 1.0
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

include("visugraph.php");

// Prepare Graph

$starttime = $_GET['starttime'];
$endtime = $_GET['endtime'];
$charttype = $_GET['charttype'];

//Chart Type 1 = Hits Delivered
//Chart Type 2 = Credits Earned
//Chart Type 3 = Members Surfed
//Chart Type 4 = Per URL
//Chart Type 5 = Per Banner
//Chart Type 6 = Per Text Ad
//Chart Type 7 = New Signups
if ($charttype == "" || !is_numeric($charttype) || $charttype < 1 || $charttype > 7) {
	// Set to default
	$charttype = 1;
}

if ($charttype == 1) {
	$searchvalue = "hitsdelivered";
} elseif ($charttype == 2) {
	$searchvalue = "crdsearned";
} elseif ($charttype == 3) {
	$searchvalue = "memsurfed";
} elseif ($charttype == 4) {
	$searchvalue = "perurl";
} elseif ($charttype == 5) {
	$searchvalue = "perbanner";
} elseif ($charttype == 6) {
	$searchvalue = "pertext";
} else {
	$searchvalue = "signups";
}


if (!is_numeric($starttime) || $starttime < 1) { exit; }
if (!is_numeric($endtime) || $endtime < 1) { exit; }

$startdate = date("Y-m-d",$starttime);
$enddate = date("Y-m-d",$endtime);

$getdays = mysql_query("Select ".$searchvalue.", date from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."' order by date asc");
$numdays = mysql_num_rows($getdays);
$numelems = 0;

if ($numdays > 40) {
	//Switch to monthly view
	
	$getmonths = mysql_query("Select date from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."' order by date asc");
	
	for ($i = 0; $i < mysql_num_rows($getmonths); $i++) {
	
	$selectdate = mysql_result($getmonths, $i, "date");
	$selecteddate = mktime(0,0,0,substr($selectdate,5,2),substr($selectdate,8),substr($selectdate,0,4));
	$selectedday = date("d", $selecteddate);
	
	if ($selectedday == 28) {
		$selectedmonth = date("m", $selecteddate);
		$selectedyear = date("Y", $selecteddate);
		$daya = $selectedyear."-".$selectedmonth."-"."01";
		$dayb = $selectedyear."-".$selectedmonth."-"."31";
		$monthname = date("M", $selecteddate);
		
		if ($monthname == "Jan") {
			$monthname = "Jan
".$selectedyear;
		}
	
		$getmonth = mysql_query("Select SUM(".$searchvalue.") from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
		$currentsum = mysql_result($getmonth, 0);
		
		if ($charttype > 2 && $charttype < 7) {
			// Take the daily average for the month
			$countmonth = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
			$monthcount = mysql_result($countmonth, 0);
			if ($monthcount > 0) {
			$currentsum = round($currentsum/$monthcount);
			}
		}
	
		$statsum[$numelems] = $currentsum;
		$xname[$numelems] = $monthname;
		$numelems = $numelems+1;
	}
	
	}
	
	// Add The Current Month
	$currentdate = date("Y-m-d",time());
	$currentday = date("d", time());
	$currentmonth = date("m", time());
	$currentyear = date("Y", time());
	$daya = $currentyear."-".$currentmonth."-"."01";
	$dayb = $currentyear."-".$currentmonth."-"."31";
	if (($currentday <= 28) && ($daya <= $enddate)) {
		$monthname = date("M", time());
		
		if ($monthname == "Jan") {
			$monthname = "Jan
".$currentyear;
		}
	
		$countmonth = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
		$monthcount = mysql_result($countmonth, 0);
		
		if ($monthcount > 0) {
		
		$getmonth = mysql_query("Select SUM(".$searchvalue.") from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
		$currentsum = mysql_result($getmonth, 0);
		
		if ($charttype > 2) {
			// Take the daily average for the month
			$currentsum = round($currentsum/$monthcount);
		}

		$statsum[$numelems] = $currentsum;
		$xname[$numelems] = $monthname;
		$numelems = $numelems+1;
		}
	}
	
} else {
	
	for ($i = 0; $i < mysql_num_rows($getdays); $i++) {
	
	$thedate = mysql_result($getdays, $i, "date");
	$thetime = mktime(0,0,0,substr($thedate,5,2),substr($thedate,8),substr($thedate,0,4));
	$statsum[$i] = mysql_result($getdays, $i, $searchvalue);
	$xname[$i] = date("d", $thetime);
	$numelems = $numelems+1;
	
	}

}

if ($numelems > 7) {
	$showlabels = 0;
} else {
	$showlabels = 1;
}

if (($numelems > 15) && ($numdays <= 40)) {
	//Modify x names
	$fontsize = 9;
	$xnamecount = ceil($numelems/15);
	for ($i = 1; $i <= $numelems; $i++) {
		if (($i % $xnamecount) > 0) {
			$xname[$i] = '';
		}
	}

} elseif (($numelems > 12) && ($numdays > 40)) {
	//Modify x names
	$fontsize = 9;
	$xnamecount = ceil($numelems/12);
	for ($i = 1; $i <= $numelems; $i++) {
		if (($i % $xnamecount) > 0) {
			if (strpos($xname[$i], 'Jan') === false) {
				$xname[$i] = '';
			} else {
				$xname[$i] = str_replace('Jan', '', $xname[$i]);
			}
		}
	}

} else {

	$fontsize = 0;
}

// End Prepare Graph

if ($numelems < 1) {
	// No Data To Display
	exit;
}

//Show Graph
$mybars = array($statsum);
$colors = array('#333333');

$transparency = 10;

$w = 760; // WIDTH OF GRAPH IMAGE
$h = 500; // HEIGHT OF GRAPH IMAGE
	
drawbargraph($xname, $mybars, $colors, $showlabels, $fontsize, $transparency, $w, $h);

?>