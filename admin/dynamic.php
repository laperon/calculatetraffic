<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



	// Prevent anyone who isn't logged in from opening this page
	if(!isset($_SESSION["adminid"])) { exit; };

	// Add a new rate
	if(($_GET["add"] == "yes") && (is_numeric($_GET["accid"]))) {
		
		if (!is_numeric($_POST['clicks']) || $_POST['clicks'] < 1) {
			$_POST['clicks'] = 1;
		}
		
		if (!is_numeric($_POST['boost']) || $_POST['boost'] <= 0) {
			$_POST['boost'] = 0.5;
		}
		
		@lfmsql_query("Insert into ".$prefix."dynamic (accid, clicks, boost) values (".$_GET["accid"].", ".$_POST['clicks'].", ".$_POST['boost'].")");
	
	}
	
	//Update a rate
	if(($_GET["update"] == "yes") && (is_numeric($_GET["rateid"]))) {
	
		if (!is_numeric($_POST['clicks']) || $_POST['clicks'] < 1) {
			$_POST['clicks'] = 1;
		}
		
		if (!is_numeric($_POST['boost']) || $_POST['boost'] <= 0) {
			$_POST['boost'] = 0.5;
		}
		
		@lfmsql_query("Update ".$prefix."dynamic set clicks=".$_POST['clicks'].", boost=".$_POST['boost']." where id=".$_GET["rateid"]." limit 1");
	
	}
	
	//Delete a rate
	if(($_GET["delete"] == "yes") && (is_numeric($_GET["rateid"]))) {
		@lfmsql_query("Delete from ".$prefix."dynamic where id=".$_GET["rateid"]." limit 1");
	}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>

<center>
<p align="center"><font color="red"><strong><?=$msg;?></strong></font>&nbsp;</p>

<table border=0 cellspacing=0 cellpadding=10>

<?

$getaccounts = lfmsql_query("Select mtid, accname from ".$prefix."membertypes order by mtid asc");
$rowcount = 1;
$resetrow = 1;

for ($i = 0; $i < lfmsql_num_rows($getaccounts); $i++) {

$accid = lfmsql_result($getaccounts, $i, "mtid");
$accname = lfmsql_result($getaccounts, $i, "accname");

if ($resetrow == 1) {
echo("<tr>");
$resetrow = 0;
}

echo("<td align=\"center\" valign=\"top\">");

echo("<table style=\"border:thin solid #000 ; border-width: 1px;\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\" width=\"400\">
  <tr>
    <td colspan=\"4\" align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$accname."</font> </strong></td>
  </tr>
  
  <tr>
    <td align=\"center\" class=\"admintd\">Clicks</td><td align=\"center\" class=\"admintd\">Ratio</td><td align=\"center\" class=\"admintd\">&nbsp;</td><td align=\"center\" class=\"admintd\">&nbsp;</td>
  </tr>
  ");

$getratios = lfmsql_query("Select * from ".$prefix."dynamic where accid=".$accid." order by clicks asc");
for ($j = 0; $j < lfmsql_num_rows($getratios); $j++) {
$rateid = lfmsql_result($getratios, $j, "id");
$clicks = lfmsql_result($getratios, $j, "clicks");
$boost = lfmsql_result($getratios, $j, "boost");
echo("<tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=dynamic&update=yes&rateid=".$rateid."\">
<td><input type=text size=3 name=clicks value=".$clicks."></td>
<td><input type=text size=3 name=boost value=".$boost."></td>
<td><input type=submit value=\"Edit\"></td>
</form>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=dynamic&delete=yes&rateid=".$rateid."\">
<td><input type=submit value=\"Delete\"></td>
</form>
</tr>");
}

$clicksvalue = $clicks+1;
$clicks = 0;
echo("<tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=dynamic&add=yes&accid=".$accid."\">
<td><input type=text size=3 name=clicks value=".$clicksvalue."></td>
<td><input type=text size=3 name=boost value=\"0.0\"></td>
<td><input type=submit value=\"Add\"></td>
</form>
<td>&nbsp;</td>
</tr>");

echo("</table>");

echo("</td>");

if ($rowcount >= 2) {
echo("</tr>");
$resetrow = 1;
$rowcount = 0;
}
$rowcount = $rowcount+1;

}

?>

</table>

</center>

<p>&nbsp;</p>
