<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

if (!isset($_GET['adminid']) || !is_numeric($_GET['adminid']) || $_GET['adminid'] < 0) {
	echo("Invalid Admin ID");
	exit;
}

?>

<html>

<head>
<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<style>
body {
  margin: 0;
  background-color: #FFFFFF;
}
</style>

<body>
<center>

<?

if(isset($_POST["Submit"]) && $_POST["Submit"] == "Save Changes" && strlen($_POST["username"]) > 0) {
	
	$checkexists = 0;
	
	if ($_GET['adminid'] > 0) {
		$checkexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."admin WHERE login='".$_POST["username"]."'"), 0);
	}
	
	if ($checkexists == 0) {
		$checkexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."admins WHERE id!='".$_GET['adminid']."' AND username='".$_POST["username"]."'"), 0);
	}
	
	if ($checkexists > 0) {
		echo("<br><div class=\"lfm_title\">Could Not Update</div><br>");
		echo("<br><div class=\"lfm_descr\">A different Admin account with that same username already exists.</div><br>");
		echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
		exit;
	}
	
	if ($_GET['adminid'] > 0) {
		// Update Secondary Admin Account
		if (!isset($_POST["ipaddresses"]) || strlen($_POST["ipaddresses"]) < 5) {
			$_POST["ipaddresses"] = "";
		}
		lfmsql_query("UPDATE ".$prefix."admins SET username='".$_POST["username"]."', ipaddresses='".$_POST["ipaddresses"]."' WHERE id='".$_GET['adminid']."'") or die(lfmsql_error());
		if (strlen($_POST["newpassword"]) > 0) {
			$passwordhash = lfmMakeAdminPassword($_POST["newpassword"]);
			lfmsql_query("UPDATE ".$prefix."admins SET password='".$passwordhash."' WHERE id='".$_GET['adminid']."'") or die(lfmsql_error());
		}
	} else {
		// Update Main Admin Account
		lfmsql_query("UPDATE ".$prefix."admin SET login='".$_POST["username"]."'") or die(lfmsql_error());
		if (strlen($_POST["newpassword"]) > 0) {
			$passwordhash = lfmMakeAdminPassword($_POST["newpassword"]);
			lfmsql_query("UPDATE ".$prefix."admin SET password='".$passwordhash."'") or die(lfmsql_error());
		}
	}

	echo("<br><div class=\"lfm_title\">Admin Account Updated Successfully</div><br>");
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;
	
}

if ($_GET['adminid'] == 0) {
	// Get Main Admin Account
	$qry="SELECT login AS username FROM ".$prefix."admin";
	$res=@lfmsql_query($qry);
	$admininfo=@lfmsql_fetch_object($res);
} else {
	// Get Secondary Admin Account
	$qry="SELECT username, ipaddresses FROM ".$prefix."admins WHERE id='".$_GET['adminid']."'";
	$res=@lfmsql_query($qry);
	$admininfo=@lfmsql_fetch_object($res);
}

?>

<br><div class="lfm_title">Edit Admin Account</div>

<form action="editadmin.php?adminid=<? echo($_GET['adminid']); ?>" method="post">

<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Username</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="username" type="text" id="username" value="<?=$admininfo->username;?>" size="16" /></td>
  </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Password<br></font></strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">(Leave blank<br>unless changing)</font></td>
    <td align="left" nowrap="nowrap"><input name="newpassword" type="text" size="16" value="" /></td>
  </tr>

  <tr valign="top">
    <td colspan="2" align="left">
    	<hr>
    	<center><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Authorized IP Addresses</font></strong></center>
    	<?
    	if ($_GET['adminid'] > 0) {
    		echo('<span align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">If you know the IP Address of the Admin who will be using this account, you can enter it below to prevent anyone else from logging into the account.  Leave this box blank if you don\'t want to restrict this account by IP Address.</font></span>');
    		echo('<center><br><textarea cols="25" rows="3" name="ipaddresses">'.$admininfo->ipaddresses.'</textarea></center>');
    		echo('<span align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">You can enter multiple addresses by separating them with commas.</font></span>');
    	} else {
    		echo('<span align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The main Admin account can\'t be restricted by IP Address from within the Admin Panel, since you would be locked out of the panel if your IP changes.  However, advanced users can set this up in the database by entering an IP Address into the ipaddresses column in the '.$prefix.'admin table.</font></span>');
    	}
    	?>
    </td>
  </tr>
    
  <tr>
    <td colspan="2" align="center">
      <input type="submit" name="Submit" value="Save Changes" /></td>
    </tr>
</table>
</form>

</body>
</html>
