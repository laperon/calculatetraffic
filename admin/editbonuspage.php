<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.32
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";


include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the fileid
	if(isset($_GET["pageid"]))
	{ $id=$_GET["pageid"]; }
	 else if(isset($_POST["pageid"]))
	{ $id=$_POST["pageid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Save Changes")
{
	$shownum=$_POST["shownum"];
	$url=$_POST["url"];
	$accid=$_POST["accid"];
	$showtype=$_POST["showtype"];
		
		$errormess = "";
		$timerange = $_POST['timerange'];
		
		if ($timerange == 1) {
		
			// Begin Start Date
			$startdate = $_POST['startdate'];
			$starthour = $_POST['starthour'];
			$startminute = $_POST['startminute'];
			
			if (isset($_POST['startampm']) && $_POST['startampm'] == 1 && $starthour == 12) {
				// Adjust Midnight to 24-hour clock
				$starthour = 0;
			}
			
			if ($starthour < 12) {
				// Check AM/PM
				if (isset($_POST['startampm']) && $_POST['startampm'] == 2) {
						$starthour = $starthour+12;
				}
			}
			
			if ($starthour < 0 || $starthour > 23) {
				$errormess = "Invalid Hour";
			}
			
			if ($startminute < 1 || $startminute > 4) {
				$errormess = "Invalid Minute Selection";
			}
			
			if ($startminute == 1) {
				$realminute = "00";
			} elseif ($startminute == 2) {
				$realminute = "15";
			} elseif ($startminute == 3) {
				$realminute = "30";
			} else {
				$realminute = "45";
			}
			
			if ($starthour < 10) {
				$testhour = "0".$starthour;
			} else {
				$testhour = $starthour;
			}
			
			$testdate = $startdate." ".$testhour.":".$realminute;
			if (($starttime = strtotime($testdate)) === false) {
				$errormess = "Could not parse the date: ".$testdate;
			}
			
			// End Start Date
			
			
			// Begin End Date
			$enddate = $_POST['enddate'];
			$endhour = $_POST['endhour'];
			$endminute = $_POST['endminute'];
			
			if (isset($_POST['endampm']) && $_POST['endampm'] == 1 && $endhour == 12) {
				// Adjust Midnight to 24-hour clock
				$endhour = 0;
			}
			
			if ($endhour < 12) {
				// Check AM/PM
				if (isset($_POST['endampm']) && $_POST['endampm'] == 2) {
						$endhour = $endhour+12;
				}
			}
			
			if ($endhour < 0 || $endhour > 23) {
				$errormess = "Invalid Hour";
			}
			
			if ($endminute < 1 || $endminute > 4) {
				$errormess = "Invalid Minute Selection";
			}
			
			if ($endminute == 1) {
				$realminute = "00";
			} elseif ($endminute == 2) {
				$realminute = "15";
			} elseif ($endminute == 3) {
				$realminute = "30";
			} else {
				$realminute = "45";
			}
			
			if ($endhour < 10) {
				$testhour = "0".$endhour;
			} else {
				$testhour = $endhour;
			}
			
			$testdate = $enddate." ".$testhour.":".$realminute;
			if (($endtime = strtotime($testdate)) === false) {
				$errormess = "Could not parse the date: ".$testdate;
			} else {
				if ($endtime <= (time()+2)) {
					$errormess = "The selected end date has already passed.";
				}
			}
			
			// End End Date
			
			if ($starttime > $endtime) {
				$errormess = "The end date must be after the start date.";
			}
			
		} else {
			$starttime = 0;
			$endtime = 0;
		}
		
		if ($errormess == "") {
			$qry="UPDATE ".$prefix."bonuspages SET shownum='".$shownum."', url='".$url."', accid='".$_POST["accid"]."', showtype='".$_POST["showtype"]."', starttime=$starttime, endtime=$endtime WHERE id=$id";
			
			@lfmsql_query($qry) or die("Unable to edit page: ".lfmsql_error());
		} else {
			echo("<center><p><b>".$errormess."</b></p>");
			exit;
		}

	$msg="<center><strong>PAGE UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current file
	$qry="SELECT * FROM ".$prefix."bonuspages WHERE id=".$id;
	$res=@lfmsql_query($qry);
	$product=@lfmsql_fetch_object($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Item</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="datepickercontrol/datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol/datepickercontrol.css">

</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<p align="center">PAGE INFORMATION UPDATED </p>
<?	
	}
?>
<form name="productfrm" method="post" action="editbonuspage.php">
<input type="hidden" name="pageid" value="<?=$id;?>" />
<table width="450" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Bonus Page Information </font> </strong></td>
    </tr>
    
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="url" type="text" id="url" value="<?=$product->url;?>" /></td>
  </tr>
    
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Display Frequency:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="shownum" type="text" id="shownum" value="<?=$product->shownum;?>" /></td>
  </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show To:</font></strong></td>
    <td align="left" nowrap="nowrap">

<?

echo("<select name=accid><option value=0>All Members</option>");

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
echo("<option value=$accid"); if($product->accid==$accid){echo(" selected");} echo(">$accname</option>");
}
echo("</select>");

?>
    
    </td>
  </tr>
  
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show:</font></strong></td>
    <td align="left" nowrap="nowrap">

<?

echo("<select name=showtype>
<option value=0"); if ($product->showtype==0) { echo(" selected"); } echo(">Once Per Day</option>
<option value=1"); if ($product->showtype==1) { echo(" selected"); } echo(">Continuously</option>
</select>");

?>
    
    </td>
  </tr>
  
  <tr valign="top">
  <td colspan="2">
  
	<input name="timerange" type="radio" value="0"<? if($product->starttime==0) { echo(" checked"); } ?>> Start Rotating Now<br><br>
	<input name="timerange" type="radio" value="1"<? if($product->starttime>0) { echo(" checked"); } ?>> Rotate During This Time Period:<br>
	
	<?
	if($product->starttime==0) {
		$formstartdate = date('Y-m-d');
		$formstarthour = 12;
		$formstartmin = 0;
		$formstartampm = "AM";
		
		$formenddate = date('Y-m-d', time() + 87000);
		$formendhour = 12;
		$formendmin = 0;
		$formendampm = "AM";
	} else {
		$formstartdate = date('Y-m-d', $product->starttime);
		$formstarthour = date('g', $product->starttime);
		$formstartmin = date('i', $product->starttime);
		$formstartampm = date('A', $product->starttime);
		
		$formenddate = date('Y-m-d', $product->endtime);
		$formendhour = date('g', $product->endtime);
		$formendmin = date('i', $product->endtime);
		$formendampm = date('A', $product->endtime);
	}
	?>
	
	Start: <input name="startdate" type id="DPC_startdate_YYYY-MM-DD" value="<? echo($formstartdate); ?>" width="80" text>&nbsp; <input type="text" name="starthour" value="<? echo($formstarthour); ?>" size="3" maxlength="2">
	<select name="startminute">
		<option value="1"<? if ($formstartmin == "0" || $formstartmin == "00") { echo(" selected"); } ?>>:00</option>
		<option value="2"<? if ($formstartmin == "15") { echo(" selected"); } ?>>:15</option>
		<option value="3"<? if ($formstartmin == "30") { echo(" selected"); } ?>>:30</option>
		<option value="4"<? if ($formstartmin == "45") { echo(" selected"); } ?>>:45</option>
	</select>
	<select name="startampm">
		<option value="1"<? if ($formstartampm == "AM") { echo(" selected"); } ?>>AM</option>
		<option value="2"<? if ($formstartampm == "PM") { echo(" selected"); } ?>>PM</option>
	</select>
	<br>
	
	End:&nbsp; <input name="enddate" type id="DPC_enddate_YYYY-MM-DD" value="<? echo($formenddate); ?>" width="80" text>&nbsp; <input type="text" name="endhour" value="<? echo($formendhour); ?>" size="3" maxlength="2">
	<select name="endminute">
		<option value="1"<? if ($formendmin == "0" || $formendmin == "00") { echo(" selected"); } ?>>:00</option>
		<option value="2"<? if ($formendmin == "15") { echo(" selected"); } ?>>:15</option>
		<option value="3"<? if ($formendmin == "30") { echo(" selected"); } ?>>:30</option>
		<option value="4"<? if ($formendmin == "45") { echo(" selected"); } ?>>:45</option>
	</select>
	<select name="endampm">
		<option value="1"<? if ($formendampm == "AM") { echo(" selected"); } ?>>AM</option>
		<option value="2"<? if ($formendampm == "PM") { echo(" selected"); } ?>>PM</option>
	</select>
	
  </td>
  </tr>
	
  
  <tr valign="top">
    <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
    </tr>
</table>
</form>
</body>
</html>
