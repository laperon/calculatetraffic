<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the fileid
	if(isset($_GET["productid"]))
	{ $productid=$_GET["productid"]; }
	 else if(isset($_POST["productid"]))
	{ $productid=$_POST["productid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Save Changes")
{
	$credits=$_POST["credits"];
	$price=$_POST["price"];
	$memtype=$_POST["memtype"];
	$subtype=$_POST["subtype"];
	$_2co_id=$_POST["_2co_id"];
	
	if(isset($_POST["visible"])) { $visible=1; } else { $visible=0; }
	
	$qry="UPDATE ".$prefix."products SET credits='".$credits."',price='".$price."', _2co_id='".$_POST["_2co_id"]."',free=0, memtype='".$_POST["memtype"]."', subtype='".$_POST["subtype"]."', visible='".$_POST["visible"]."' WHERE productid=$productid";

	@mysql_query($qry) or die("Unable to edit product: ".mysql_error());

	$msg="<center><strong>PROMO ITEM UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current file
	$qry="SELECT * FROM ".$prefix."products WHERE productid=".$productid;
	$res=@mysql_query($qry);
	$product=@mysql_fetch_object($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Item</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<p align="center">PRODUCT INFORMATION UPDATED </p>
<?	
	}
?>
<form name="productfrm" method="post" action="editcreditsale.php">
<input type="hidden" name="productid" value="<?=$productid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Product Information </font> </strong></td>
    </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Credits:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="credits" type="text" id="credits" value="<?=$product->credits;?>" /></td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Price</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="price" type="text" id="price" value="<?=$product->price;?>" /></td>
  </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show To:</font></strong></td>
    <td align="left" nowrap="nowrap">

<?

echo("<select name=memtype><option value=0>All Members</option>");

$getaccounts = mysql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < mysql_num_rows($getaccounts); $j++) {
$accid = mysql_result($getaccounts, $j, "mtid");
$accname = mysql_result($getaccounts, $j, "accname");
echo("<option value=$accid"); if($product->memtype==$accid){echo(" selected");} echo(">$accname</option>");
}
echo("</select>");

?>
    
    </td>
  </tr>
  
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Purchase Type:</font></strong></td>
    <td align="left" nowrap="nowrap">

<?

echo("<select name=subtype>
<option value=0"); if ($product->subtype==0) { echo(" selected"); } echo(">One Time</option>
<option value=1"); if ($product->subtype==1) { echo(" selected"); } echo(">Monthly Subscription</option>
</select>");

?>
    
    </td>
  </tr>
  
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">2CO ID </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="_2co_id" type="text" id="_2co_id" value="<?=$product->_2co_id;?>" /></td>
  </tr>
  
    <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Visible</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="visible" type="checkbox" id="visible" value="1" <? if($product->visible == 1){ echo "checked=\"checked\""; } ?> /></td>
  </tr>
  
  <tr valign="top">
    <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
    </tr>
</table>
</form>
</body>
</html>
