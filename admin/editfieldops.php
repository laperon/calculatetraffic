<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.17
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

include "../inc/checkauth.php"; 
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

	// Get the field ID
	if(isset($_GET["fieldid"]))
	{ $id=$_GET["fieldid"]; }
	 else if(isset($_POST["fieldid"]))
	{ $id=$_POST["fieldid"]; }
	 else
	{ 
		echo "Error: Invalid Field ID";
		exit; 
	}
	
$getfield = mysql_query("SELECT options FROM `".$prefix."customfields` WHERE id=".$id) or die(mysql_error());
if (mysql_num_rows($getfield) < 1) {
	echo("Field ID not found");
	exit;
}

$currentoptions = mysql_result($getfield, 0, "options");

?>

<html>

<head>
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<body>
<center>

<?

if (isset($_POST['Submit']) && $_POST['Submit'] == "Update Options" && isset($_POST['fieldoptions'])) {
	
	$fieldoptions = preg_replace("/((\r(?!\n))|((?<!\r)\n)|(\r\n))/", ",", $_POST['fieldoptions']);
	if(stristr($fieldoptions, ',') === FALSE) {
		echo("<p><b>You must enter more than one option for a Dropdown Options field.</b></p>");
		exit;
	}
	
	mysql_query("UPDATE `".$prefix."customfields` SET options='".$fieldoptions."' WHERE id=".$id) or die(mysql_error());
	
	echo("<h2><b>Options Updated Successfully</b></h2>");
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;	
}

####################

//Begin main page

####################

?>

	<h2><b>Update Field Options</b></h2>
	
	<table width="500" cellpadding="5" cellspacing="0" border="0">
	<tr><td colspan="2">
	<form action="editfieldops.php?fieldid=<? echo($id); ?>" method="post">
	<center>
	<textarea name="fieldoptions" cols=30 rows=4><? echo($currentoptions); ?></textarea><br>
	<INPUT type="submit" name="Submit" value="Update Options">
	</center>
	</form>
	</td></tr>
	
	</table>

<br><br>

</center>
</body>
</html>

<?php
exit;
?>