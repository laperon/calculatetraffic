<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the group ID
	if(isset($_GET["group"]))
	{ $groupid=$_GET["group"]; }
	 else if(isset($_POST["group"]))
	{ $groupid=$_POST["group"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update groups table
if($_POST["Submit"] == "Save Changes")
{
	// Update the group
	$qry="UPDATE ".$prefix."groups SET groupname='".$_POST["groupname"]."' WHERE groupid=".$groupid;
	@mysql_query($qry) or die("Unable to edit group: ".mysql_error());

	$msg="<center><strong>GROUP UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current group
	$qry="SELECT * FROM ".$prefix."groups WHERE groupid=".$groupid;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Group</font> </strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">GROUP UPDATED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="editfrm" method="post" action="editgroup.php">
<input type="hidden" name="group" value="<?=$groupid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Group</font> </strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Name:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="groupname" type="text" id="groupname" value="<?=$mrow["groupname"];?>"></td>
  </tr>
  <tr>
    <td colspan="2" align="left" nowrap="nowrap">
	<p align="center">&nbsp;</p>      </td>
  </tr>
  
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
  </tr>
</table>
</form>
<? 
}
?>
</body>
</html>
