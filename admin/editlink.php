<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
	// Get the Link ID
	if(isset($_GET["id"]))
	{ $id=$_GET["id"]; }
	 else if(isset($_POST["id"]))
	{ $id=$_POST["id"]; }
	 else
	{ 
		echo "Error: No Link ID found!";
		exit; 
	}

// Update user record and refresh main admin page
if($_POST["Submit"] == "Update")
{
	// Check whether the url is empty
	if(strlen($_POST["url"]) < 7)
	{
		// No url so get the default
		$dres=@mysql_query("SELECT affurl FROM ".$prefix."settings");
		$drow=@mysql_fetch_array($dres);
		$newurl=$drow["affurl"];
	}
	else
	{
		$newurl=$_POST["url"];
	}
	
	$qry="UPDATE ".$prefix."links SET urlpath='".$newurl."' 
	WHERE id=".$_POST["id"];
	mysql_query($qry);
	$msg="<center><font color=\"red\">URL updated!</font></center>";
}

// Get current URL details
	$qry="SELECT * FROM ".$prefix."links WHERE id=".$id;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="linkfrm" method="post" action="editlink.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong>Edit Link URL </strong></td>
  </tr>
  <tr>
    <td width="30" align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">URL:</font></strong></td>
    <td align="left"><input name="url" type="text" id="url" value="<?=$mrow["urlpath"];?>" size="40" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:closeAndRefresh();" />
    <input type="submit" name="Submit" value="Update" /></td>
  </tr>
</table>
<center><?=$msg;?></center>
</body>
</html>
