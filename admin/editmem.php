<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.17
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php";
include "../inc/funcs.php";
include "../inc/bandj/functions.php";


if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
	// Get the member ID
	if(isset($_GET["id"]))
	{ $id=$_GET["id"]; }
	 else if(isset($_POST["id"]))
	{ $id=$_POST["id"]; }
	 else
	{
		echo "Error: No member ID found!";
		exit;
	}

// Update user record and refresh main admin page
if($_POST["Submit"] == "Update")
{
	if(strlen($_POST["password"]) > 3)
	{
		$password=trim($_POST["password"]);
		$encpassword=md5($password);
		// echo "updating  $password to $encpassword";
        $qry="UPDATE ".$prefix."members SET password='$encpassword' WHERE Id=".$_POST["id"];
		@mysql_query($qry) or die(mysql_error());
	}
	$qry="UPDATE ".$prefix."members SET firstname='".$_POST["firstname"]."', lastname='".$_POST["lastname"]."', email='".$_POST["email"]."', telephone='".$_POST["telephone"]."', address='".$_POST["address"]."', city='".$_POST["city"]."', state='".$_POST["state"]."', postcode='".$_POST["postcode"]."', country='".$_POST["country"]."', username='".$_POST["username"]."', status='".$_POST["status"]."', refid=".$_POST["refid"].", paypal_email='".$_POST["paypal_email"]."', otoview=0, groupid=".$_POST["groupid"].", mtype=".$_POST["mtype"].", joindate='".$_POST["joindate"]."',mailbounce=".$_POST["mailbounce"].", cb_id='".$_POST["cb_id"]."', credits='".$_POST["credits"]."', bannerimps='".$_POST["bannerimps"]."', textimps='".$_POST["textimps"]."', upgend='".$_POST["upgend"]."'
	WHERE Id=".$_POST["id"];
	
		// Update Custom Fields
		$getfields = mysql_query("SELECT id FROM `".$prefix."customfields` ORDER BY rank ASC");
		if (mysql_num_rows($getfields) > 0) {
			for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
				$fieldid = mysql_result($getfields, $i, "id");
				
				$fieldpostname = "custom_".$fieldid;
				
				if (isset($_REQUEST[$fieldpostname])) {
					mysql_query("DELETE FROM ".$prefix."customvals WHERE fieldid='".$fieldid."' AND userid='".$_POST["id"]."'") or die(mysql_error());
					mysql_query("INSERT INTO ".$prefix."customvals (fieldid, userid, fieldvalue) VALUES ('".$fieldid."', '".$_POST["id"]."', '".$_REQUEST[$fieldpostname]."')") or die(mysql_error());
				}
			}
		}
		// End Update Custom Fields

	@mysql_query($qry) or die(mysql_error());
	$msg="<center><font color=\"red\">Member record updated!</font></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";

}

// Get current member details
	$qry="SELECT * FROM ".$prefix."members WHERE Id=".$id;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);

	// Get the commission for this member
	$commission="0";

	$commres=@mysql_query("SELECT SUM(commission) as ctotal FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND status IS NULL");
	if($commres)
	{
		$commrow=@mysql_fetch_array($commres);
		$commission = $commrow["ctotal"];
		if($commission == '') { $commission="0.00"; }
	}
	else
	{
		$commission="0.00";
}

$surferrewards = bandjSurferRewardsAllTime($prefix, $id);
$promocredits = bandjPromoCreditsAllTime($prefix, $id);
$bonus = bandjBonusAllTime($prefix , $id);
$prize = bandjPrizeAllTime($prefix , $id);
$wsword = bandjWsWordAllTime($prefix, $id); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="datepickercontrol/datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol/datepickercontrol.css">
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<style>
.form {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #000000;
}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="memfrm" method="post" action="editmem.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Edit Member Details  </font></strong></td>
  </tr>

<?
//Start OTO List
if ($_GET['resetoto'] > 0) {
@mysql_query("Delete from `".$prefix."oto_stats` where userid=$id and groupid=".$_GET['resetoto']." limit 1");
}
$getotolist = mysql_query("Select id from `".$prefix."oto_groups` where splitmirror=0 order by days asc");
if (mysql_num_rows($getotolist) > 0) {
echo("<tr><td colspan=\"2\" align=\"center\">
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr><td align=center colspan=2><b>OTO Groups Viewed</b></td></tr>");?>
    
<form method="post" action="">
  <input type="submit" name="Resend" value="Resend" />
</form>
<?php
if (isset($_POST['Resend'])){
    mysql_query("Delete from ".$prefix."oto_stats where userid=$id");
    echo "Resend applied";
}

for ($i = 0; $i < mysql_num_rows($getotolist); $i++) {
$shownum = $i+1;
$groupid = mysql_result($getotolist, $i, "id");

$getstat = mysql_query("Select id from `".$prefix."oto_stats` where userid=$id and groupid=$groupid limit 1");
if (mysql_num_rows($getstat) > 0) {
echo("<tr><td>Group $shownum</td><td><a href=\"editmem.php?id=$id&resetoto=$groupid\">Show Again</a></td></tr>");
}

}
echo("</table>
</td></tr>");
}
//End OTO List

//Reset Clicking Accuracy
if ($_GET['resetaccuracy'] > 0) {
@mysql_query("Update ".$prefix."members set goodclicks=0, badclicks=0 where Id=$id limit 1");
}
$getclicks = mysql_query("Select goodclicks, badclicks from ".$prefix."members where Id=$id limit 1");
$goodcount = mysql_result($getclicks, 0, "goodclicks");
$badcount = mysql_result($getclicks, 0, "badclicks");
$totalcount = $goodcount+$badcount;
if ($totalcount > 0) {
$accurate = $goodcount/$totalcount;
$accurate = $accurate*100;
$accurate = round($accurate);
} else {
$accurate = 0;
}
?>

  <tr>
    <td width="30" align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">First Name:</font></strong></td>
    <td align="left"><input name="firstname" type="text" class="form" id="firstname" value="<?=$mrow["firstname"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Last Name:</font></strong></td>
    <td align="left"><input name="lastname" type="text" class="form" id="lastname" value="<?=$mrow["lastname"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Email Address: </font></strong></td>
    <td align="left"><input name="email" type="text" class="form" id="email" value="<?=$mrow["email"];?>" size="30" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Credits: </font></strong></td>
    <td align="left"><input name="credits" type="text" class="form" id="credits" value="<?=$mrow["credits"];?>" size="30" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Banner Imps: </font></strong></td>
    <td align="left"><input name="bannerimps" type="text" class="form" id="bannerimps" value="<?=$mrow["bannerimps"];?>" size="30" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Text Imps: </font></strong></td>
    <td align="left"><input name="textimps" type="text" class="form" id="textimps" value="<?=$mrow["textimps"];?>" size="30" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Address:</font></strong></td>
    <td align="left"><input name="address" type="text" class="form" id="address" value="<?=$mrow["address"];?>" size="30" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">City:</font></strong></td>
    <td align="left"><input name="city" type="text" class="form" id="city" value="<?=$mrow["city"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">State/County:</font></strong></td>
    <td align="left"><input name="state" type="text" class="form" id="state" value="<?=$mrow["state"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Zip/Postcode:</font></strong></td>
    <td align="left"><input name="postcode" type="text" class="form" id="postcode" value="<?=$mrow["postcode"];?>" size="12" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Country:</font></strong></td>
    <td align="left"><select name="country" class="form"><? echo select_country($mrow["country"]); ?></select></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Telephone:</font></strong></td>
    <td align="left"><input name="telephone" type="text" class="form" id="telephone" value="<?=$mrow["telephone"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Username:</font></strong></td>
    <td align="left"><input name="username" type="text" class="form" id="username" value="<?=$mrow["username"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Password:</font></strong></td>
    <td align="left"><input name="password" type="text" class="form" id="password" /></td>
  </tr>
  <tr>
    <td colspan="2" align="left" nowrap="nowrap"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Note: Leave password blank unless changing. </font></td>
    </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Status:</font></strong></td>
    <td align="left">          <select name="status" class="form">
		  <option value="Active" <? if($mrow["status"] == "Active") { ?>selected="selected"<? } ?>>Active</option>
		  <option value="Suspended" <? if($mrow["status"] == "Suspended") { ?>selected="selected"<? } ?>>Suspended</option>
		  <option value="Unverified" <? if($mrow["status"] == "Unverified") { ?>selected="selected"<? } ?>>Unverified</option>
          </select>    </td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Paypal Email: </font></strong></td>
    <td align="left"><input name="paypal_email" type="text" class="form" id="paypal_email" value="<?=$mrow["paypal_email"];?>" size="30" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Clickbank ID: </font></strong></td>
    <td align="left"><input name="cb_id" type="text" class="form" id="cb_id" value="<?=$mrow["cb_id"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Type : </font></strong></td>
    <td align="left"><select name="mtype" class="form">
<? 		GetMtype($mrow["mtype"]);
		if($mrow["mtype"] == 0){ ?>
		<option value="0" selected=selected>Pending</option>
<?
	}
?>
	</select>    </td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Upgrade Ends: </font></strong></td>
    <td align="left"><input name="upgend" type id="DPC_sdate_YYYY-MM-DD" value="<?=$mrow["upgend"];?>" width="80" text></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Group: </font></strong></td>
    <td align="left"><select name="groupid" class="form">
    <? GetGroup($mrow["groupid"]); ?>
    </select>    </td>
  </tr>

  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Join Date: </font></strong></td>
    <td align="left"><input name="joindate" type id="DPC_sdate_YYYY-MM-DD" value="<?=$mrow["joindate"];?>" width="80" text></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Comm Owed:</font></strong></td>
    <td align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?=$commission;?>
    </font></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Upline:</font></strong></td>
    <td align="left"><input name="refid" type="text" class="form" id="refid" value="<?=$mrow["refid"];?>" size="4" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Mail Bounce: </font></strong></td>
    <td align="left"><input name="mailbounce" type="text" class="form" value="<?=$mrow["mailbounce"];?>" size="4" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Bonus Pages: </font></strong></td>
    <td align="left"><input readonly type="text" class="form" value="<?=$bonus;?>" size="4" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Prize Pages: </font></strong></td>
    <td align="left"><input readonly type="text" class="form" value="<?=$prize;?>" size="4" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Word Search Game: </font></strong></td>
    <td align="left"><input readonly type="text" class="form" value="<?=$wsword;?>" size="4" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surfing Rewards: </font></strong></td>
    <td align="left"><input readonly type="text" class="form" value="<?=$surferrewards;?>" size="4" /></td> 
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Promo Codes: </font></strong></td>
    <td align="left"><input readonly type="text" class="form" value="<?=$promocredits;?>" size="4" /></td>
  </tr>
<?
// Custom Fields
$getfields = mysql_query("SELECT id, name, type, options FROM `".$prefix."customfields` ORDER BY rank ASC");
if (mysql_num_rows($getfields) > 0) {
	for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
		$fieldid = mysql_result($getfields, $i, "id");
		$fieldname = mysql_result($getfields, $i, "name");
		$fieldtype = mysql_result($getfields, $i, "type");
		$fieldoptions = mysql_result($getfields, $i, "options");
		
		$fieldpostname = "custom_".$fieldid;
		
		$getfieldvalue = mysql_query("SELECT fieldvalue FROM `".$prefix."customvals` WHERE fieldid='".$fieldid."' AND userid='".$id."'");
		if (mysql_num_rows($getfieldvalue) > 0) {
			$fieldvalue = mysql_result($getfieldvalue, 0, "fieldvalue");
		} else {
			$fieldvalue = "";
		}
		
		echo('
		<tr>
    			<td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">'.$fieldname.': </font></strong></td>
    			<td align="left">');
    			
    			if ($fieldtype == 1) {
				echo('<input name="'.$fieldpostname.'" type="text" maxlength="255" id="'.$fieldpostname.'" value="'.$fieldvalue.'" />');
			} else {
				echo('<select name="'.$fieldpostname.'" id="'.$fieldpostname.'">');
				
				$splitoptions = explode(",", $fieldoptions);
				for ($j = 0; $j < count($splitoptions); $j++) {
					if (strlen(trim($splitoptions[$j])) > 0) {
						$splitoptions[$j] = trim($splitoptions[$j]);
						echo('<OPTION VALUE="'.$splitoptions[$j].'"');
						if ($fieldvalue == $splitoptions[$j]) { echo(' selected="selected"'); }
						echo('>'.$splitoptions[$j].'</OPTION>');
					}
				}
				
				echo('</select>');
			}
			
			echo('</td>
		</tr>
		');
		
	}
}
// End Custom Fields
?>
  
  <?
  	$timesurfing = $mrow["recordtime"];
       	$numhours = floor($timesurfing/60/60);
      	$timesurfing = $timesurfing - ($numhours*60*60);
     	
       	$nummins = floor($timesurfing/60);
       	$timesurfing = $timesurfing - ($nummins*60);
       	
       	$numsecs = $timesurfing;
       	
       	$longestsurfing = $numhours." Hours ".$nummins." Minutes ".$numsecs." Seconds";
  ?>
  
  <tr>
    <td align="left" nowrap="nowrap" colspan="2">The User Last Logged in on: <?=$mrow["lastlogin"];?><br>Has Logged in <?=$mrow["numLogins"];?> times<br> Signup IP: <?=$mrow["signupip"];?><br> Last Login IP: <?=$mrow["lastip"];?><br><br>Longest Surfing Session: <?=$longestsurfing;?><br>Clicking Accuracy: <?=$accurate;?>% <a href=editmem.php?id=<?=$id;?>&resetaccuracy=1>Reset</a>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" class="form" id="Submit" onClick="javascript:self.close();" value="Close" />
    <input name="Submit" type="submit" class="form" value="Update" /></td>
  </tr>
</table>
<center><?=$msg;?></center>
</form>
</body>
</html>
