<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.05
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the group ID
	if(isset($_GET["mtid"]))
	{ $mtid=$_GET["mtid"]; }
	 else if(isset($_POST["mtid"]))
	{ $mtid=$_POST["mtid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update groups table
if($_POST["Submit"] == "Save Changes")
{

	if ($_POST["minauto"] < 1) {
		$_POST["minauto"] = $_POST["minauto"]*100;
	}
	
	if ($_POST["refcrds"] < 1) {
		$_POST["refcrds"] = $_POST["refcrds"]*100;
	}
	
	if ($_POST["refcrds2"] < 1) {
		$_POST["refcrds2"] = $_POST["refcrds2"]*100;
	}

	if(isset($_POST["enablepdate"])) { $enablepdate=1; } else { $enablepdate=0; }
	// Update the membertype
	$qry="UPDATE ".$prefix."membertypes SET accname='".$_POST["accname"]."', comm=".$_POST["comm"].", 
	comm2=".$_POST["comm2"].",otocomm=".$_POST["otocomm"].",otocomm2=".$_POST["otocomm2"].", 
	enabled=".$_POST["enabled"].", 
	description='".$_POST["desc"]."',commfixed=".$_POST["commfixed"].",commfixed2=".$_POST["commfixed2"].", 
	enablepdate=$enablepdate,rank=".$_POST["rank"].", surfratio='".$_POST["surfratio"]."',hitvalue='".$_POST["hitvalue"]."',bannervalue='".$_POST["bannervalue"]."',textvalue='".$_POST["textvalue"]."',maxsites='".$_POST["maxsites"]."',maxbanners='".$_POST["maxbanners"]."',maxtexts='".$_POST["maxtexts"]."',minauto='".$_POST["minauto"]."',refcrds='".$_POST["refcrds"]."',refcrds2='".$_POST["refcrds2"]."',manbanners=".$_POST["manbanners"].",mantexts=".$_POST["mantexts"].",mansites=".$_POST["mansites"].",surftimer=".$_POST["surftimer"].",monthcrds=".$_POST['monthcrds'].",randrefs=".$_POST['randrefs'].",monthbimps=".$_POST['monthbimps'].",monthtimps=".$_POST['monthtimps'].",cantransfer='".$_POST['cantransfer']."',mintransfer='".$_POST['mintransfer']."',maxtransfer='".$_POST['maxtransfer']."' WHERE mtid=".$mtid;

	@mysql_query($qry) or die("Unable to edit member type: ".mysql_error());

	$msg="<center><strong>MEMBER TYPE UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Member Level</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?
	// Get current membertype
	$qry="SELECT * FROM {$prefix}membertypes WHERE mtid=".$mtid;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);
?>
<form name="editfrm" method="post" action="editmt.php" onsubmit="return validate_addMembertype(this);">
<input type="hidden" name="mtid" value="<?=$mtid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="6" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Member Type</font> </strong></td>
  </tr>
  <tr>
    <td colspan="6" align="left">
<?
	if($_POST["Submit"] == "Save Changes")
	{
	?>
	<center>
  	<strong><font color="#FF0000" size="2" face="Verdana, Arial, Helvetica, sans-serif">MEMBER LEVEL UPDATED</font></strong>
	</center>
	<?	
	}
	?>	</td>
    </tr>
    
<tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>This is the name for this account type that will appear on the signup page.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Membership Name:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="accname" type="text" id="accname" size="28" value="<?=$mrow["accname"];?>"></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The rank is used to determine which member content should 'pass through' to higher member levels.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Rank:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="rank" type="text" id="rank" value="<?=$mrow["rank"];?>" size="2" /></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  
  <tr>
      <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of credits members will earn per click.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surfing Ratio:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="surfratio" type="text" size="2" value="<?=$mrow["surfratio"];?>"></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of seconds that a member must view each site.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surf Timer:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="surftimer" type="text" size="2" value="<?=$mrow["surftimer"];?>" /> seconds</td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of hits a site will receive per credit assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Hits Per Credit:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="hitvalue" type="text" size="2" value="<?=$mrow["hitvalue"];?>" /></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of impressions a banner will receive per credit assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Banner Imps Per Credit:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="bannervalue" type="text" size="2" value="<?=$mrow["bannervalue"];?>" /></td>
  </tr>
  
   <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of impressions a text ad will receive per credit assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Text Imps Per Credit:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="textvalue" type="text" size="2" value="<?=$mrow["textvalue"];?>" /></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The maximum number of sites a member can submit.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Number of Sites:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="maxsites" type="text" size="2" value="<?=$mrow["maxsites"];?>"  /></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The maximum number of banners a member can submit.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Number of Banners:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="maxbanners" type="text" size="2" value="<?=$mrow["maxbanners"];?>"  /></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The maximum number of text ads a member can submit.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Number of Text Ads:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="maxtexts" type="text" size="2" value="<?=$mrow["maxtexts"];?>" /></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The percentage of credits that must be auto-assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Auto Assign Minimum:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="minauto" type="text" size="2" value="<?=$mrow["minauto"];?>" /> %</td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>If YES, banners will not rotate until approved from the admin area.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Approve Banners:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="manbanners">
    <option value=0<? if ($mrow["manbanners"] == 0) { echo(" selected"); } ?>>No</option>
    <option value=1<? if ($mrow["manbanners"] == 1) { echo(" selected"); } ?>>Yes</option>
    </select>
    </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>If YES, text ads will not rotate until approved from the admin area.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Approve Text Ads:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="mantexts">
    <option value=0<? if ($mrow["mantexts"] == 0) { echo(" selected"); } ?>>No</option>
    <option value=1<? if ($mrow["mantexts"] == 1) { echo(" selected"); } ?>>Yes</option>
    </select>
    </td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>If YES, sites will not rotate until approved from the admin area.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Approve Sites:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="mansites">
    <option value=0<? if ($mrow["mansites"] == 0) { echo(" selected"); } ?>>No</option>
    <option value=1<? if ($mrow["mansites"] == 1) { echo(" selected"); } ?>>Yes</option>
    </select>
    </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of banner impressions members will get at the beginning of each month.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Monthly Banner Bonus:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="monthbimps" type="text" size="2" value="<?=$mrow["monthbimps"];?>"></td>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of text impressions members will get at the beginning of each month.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Monthly Text Ad Bonus:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="monthtimps" type="text" size="2" value="<?=$mrow["monthtimps"];?>"></td>
  </tr>
  
  <tr>
      <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of credits members will get at the beginning of each month.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Monthly Credit Bonus:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="monthcrds" type="text" size="2" value="<?=$mrow["monthcrds"];?>"></td>
    <td align="left" bgcolor="#E0EBFE"></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"></strong></td>
    <td align="left" nowrap="nowrap"></td>
  </tr>
  
  <?
    echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>If YES, members with this account type will get random referrals.</span></a></td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Random Referrals:</font></strong></td>
    <td align=\"left\" nowrap=\"nowrap\">
    <select name=\"randrefs\">
    <option value=0"); if ($mrow["randrefs"] == 0) { echo(" selected"); } echo(">No</option>
    <option value=1"); if ($mrow["randrefs"] == 1) { echo(" selected"); } echo(">Yes</option>
    </select>
    </td>
    <td align=\"left\" bgcolor=\"#E0EBFE\"></td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\"></strong></td>
    <td align=\"left\" nowrap=\"nowrap\"></td>
  </tr>");

echo("<input type=\"hidden\" name=\"enablepdate\" value=\"0\">");
  /*
  echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>You can enable a publish date to prevent members from accessing previously published content.</span></a></td>
    <td align=\"left\" nowrap=\"NOWRAP\" bgcolor=\"#E0EBFE\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Enable Publish Date:</font></strong></td>
    <td align=\"left\" nowrap=\"nowrap\"><input name=\"enablepdate\" type=\"checkbox\" id=\"enablepdate\" value=\"1\" /></td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\">&nbsp;</td>
  </tr>");
  */
  ?>
  
  
  <?
  echo("<input type=\"hidden\" name=\"pp_srt\" id=\"pp_srt\" value=\"0\">");
  /*
  echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>The total number of recurring payments to charge for this account type (0 = forever).</span></a></td>
    <td align=\"left\" nowrap=\"NOWRAP\" bgcolor=\"#E0EBFE\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Recurring Payments </font></strong></td>
    <td align=\"left\" nowrap=\"nowrap\"><select name=\"pp_srt\" id=\"pp_srt\">
      <option value=\"0\" selected=\"selected\">0</option>
      <option value=\"1\">1</option>
      <option value=\"2\">2</option>
      <option value=\"3\">3</option>
      <option value=\"4\">4</option>
      <option value=\"5\">5</option>
      <option value=\"6\">6</option>
      <option value=\"7\">7</option>
      <option value=\"8\">8</option>
      <option value=\"9\">9</option>
      <option value=\"10\">10</option>
      <option value=\"11\">11</option>
      <option value=\"12\">12</option>
      <option value=\"13\">13</option>
      <option value=\"14\">14</option>
      <option value=\"15\">15</option>
      <option value=\"16\">16</option>
      <option value=\"17\">17</option>
      <option value=\"18\">18</option>
      <option value=\"19\">19</option>
      <option value=\"20\">20</option>
      <option value=\"21\">21</option>
      <option value=\"22\">22</option>
      <option value=\"23\">23</option>
      <option value=\"24\">24</option>
      <option value=\"25\">25</option>
      <option value=\"26\">26</option>
      <option value=\"27\">27</option>
      <option value=\"28\">28</option>
      <option value=\"29\">29</option>
      <option value=\"30\">30</option>
    </select></td>
    <td align=\"left\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\">&nbsp;</td>
  </tr>");
  */
  ?>
  
  <tr>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage of credits members earn from their direct referrals surfing.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong>
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referral Credits:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="refcrds" type="text" id="refcrds" size="3" value="<?=$mrow["refcrds"];?>">
      %</td>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage of credits members earn from their second level referrals surfing.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong>
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referral Credits <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="refcrds2" type="text" id="refcrds2" size="3" value="<?=$mrow["refcrds2"];?>">
      %</td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage amount of the membership fee that affiliates/members are paid for referring new members. Set this to 0 if you are paying fixed or no commission.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong>
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Commissions:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="comm" type="text" id="comm" size="3" value="<?=$mrow["comm"];?>">
      %</td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The percentage amount of the membership fee that level 2 affiliates/members are paid for referring new members. Set this to 0 if you are paying fixed or no level 2 commission.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Commissions <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="comm2" type="text" id="comm2" size="3"  value="<?=$mrow["comm2"];?>" />
% </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The fixed amount that affiliates/members are paid for referring new members. Set this to 0 if you are not paying a commission for free accounts.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referring Bonus $:</font></strong></td>
    <td align="left" nowrap="nowrap">    <input name="commfixed" type="text" id="commfixed" size="3" value="<?=$mrow["commfixed"];?>" />
      (Free accounts only) </td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The fixed amount that level 2 affiliates/members are paid for referring new members. Set this to 0 if you are not paying a level 2 commission for free accounts.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referring Bonus $ <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="commfixed2" type="text" id="commfixed2" size="3" value="<?=$mrow["commfixed2"];?>" />       (Free accounts only) </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage amount that affiliates/members are paid on OTO sales to members they referred.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">OTO Commissions:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="otocomm" type="text" id="otocomm" size="3" value="<?=$mrow["otocomm"];?>"> 
      % </td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The percentage amount that level affiliates/members are paid on OTO sales to members they referred.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">OTO Commissions <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="otocomm2" type="text" id="otocomm2" size="3"  value="<?=$mrow["otocomm2"];?>" />
% </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>If YES, the member can transfer their credits to their downline.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Can Transfer Downline Credits:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="cantransfer">
    <option value=0<? if ($mrow["cantransfer"] == 0) { echo(" selected"); } ?>>No</option>
    <option value=1<? if ($mrow["cantransfer"] == 1) { echo(" selected"); } ?>>Yes</option>
    </select>
    </td>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The minimum number of credits a member can transfer.  Enter 0 for no minimum.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Minimum Credit Transfer:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="mintransfer" type="text" size="2" value="<?=$mrow["mintransfer"];?>"></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The maximum number of credits a member can transfer.  Enter 0 for no maximum.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Maximum Credit Transfer:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="maxtransfer" type="text" size="2" value="<?=$mrow["maxtransfer"];?>"></td>
    <td align="left" bgcolor="#E0EBFE"></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"></strong></td>
    <td align="left" nowrap="nowrap"></td>
  </tr>
    
  <tr>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" bgcolor="white"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The description that will appear on the Upgrade page.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Description: </font></strong></td>
    <td align="left" nowrap="nowrap"><textarea name="desc" wrap="soft" rows="5" cols="25"><?=$mrow["description"];?></textarea></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>Set this membership Visible or Not Visible. If the membership level is 'Not Visible' it will not appear on the upgrade page.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Visible:</font></strong></td>
    <td align="left" nowrap="nowrap"><select name="enabled">
      <option value="1" <? if ($mrow["enabled"] == 1) { echo(" selected"); } ?>>Y</option>
      <option value="0" <? if ($mrow["enabled"] == 0) { echo(" selected"); } ?>>N</option>
    </select></td>
  </tr>
  
  <?
  echo("<input type=\"hidden\" name=\"cb_paylink\" id=\"pp_srt\" value=\"\">");
  /*
  echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>The Clickbank paylink <br />for this member level <br />(e.g. 1.vendor.pay.clickbank.net )</span></a></td>
    <td colspan=\"5\" align=\"left\" nowrap=\"nowrap\"><table border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
      <tr>
        <td width=\"150\" nowrap=\"nowrap\" class=\"admintd\"><strong>CB Paylink</strong></td>
        <td><input name=\"cb_paylink\" type=\"text\" class=\"formfield\" id=\"cb_paylink\" size=\"40\" /></td>
      </tr>
    </table>      </td>
    </tr>");
    */
    ?>
    
  
  <tr>
    <td colspan="6" align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
  </tr>
  
  
</table>
</form>
</body>
</html>
