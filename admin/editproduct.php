<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.24
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the fileid
	if(isset($_GET["productid"]))
	{ $productid=$_GET["productid"]; }
	 else if(isset($_POST["productid"]))
	{ $productid=$_POST["productid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Save Changes")
{
	$itemid=$_POST["itemid"];
	$catid=$_POST["catid"];
	$productname=$_POST["productname"];
	$filename=$_POST["filename"];
	$price=$_POST["price"];
	$_2co_id=$_POST["_2co_id"];
	if(isset($_POST["free"])) { $free=1; } else { $free=0; }
	
	$qry="UPDATE ".$prefix."products SET catid='".$catid."', productname='".$productname."',filename='".$filename."',free=$free WHERE productid=$productid";

	@mysql_query($qry) or die("Unable to edit product: ".mysql_error());

	$msg="<center><strong>PRODUCT UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current file
	$qry="SELECT * FROM ".$prefix."products WHERE productid=".$productid;
	$res=@mysql_query($qry);
	$product=@mysql_fetch_object($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Item</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Product Information </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">PRODUCT INFORMATION UPDATED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="productfrm" method="post" action="editproduct.php">
<input type="hidden" name="productid" value="<?=$productid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Product Information </font> </strong></td>
    </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Category </font></strong></td>
    <td align="left" nowrap="nowrap">
            	<select name="catid">
			<option value="0">None</option>
			<?
			$getcats = mysql_query("SELECT id, catname FROM `".$prefix."products_cats` ORDER BY catname ASC") or die(mysql_error());
			if (mysql_num_rows($getcats) > 0) {
				while ($catlist = mysql_fetch_array($getcats)) {
					echo("<option value=\"".$catlist['id']."\""); if ($product->catid == $catlist['id']) { echo(" selected=\"selected\""); } echo(">".$catlist['catname']."</option>");
				}
			}
			?>
			</select>
    </td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product Name </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="productname" type="text" id="productname" value="<?=$product->productname;?>" size="16" /></td>
    </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Filename</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="filename" type="text" size="16" value="<?=$product->filename;?>" /></td>
    </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Free</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="free" type="checkbox" id="free" value="1" <? if($product->free == 1){ echo "checked=\"checked\""; } ?> /></td>
  </tr>
  
  <tr valign="top">
    <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
    </tr>
</table>
</form>
<? 
}
?>
</body>
</html>
