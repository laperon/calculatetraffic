<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

	// Get the fileid
	if(isset($_GET["id"]))
	{ $id=$_GET["id"]; }
	 else if(isset($_POST["id"]))
	{ $id=$_POST["id"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Save Changes")
{
   @mysql_query("UPDATE `".$prefix."dlbprograms` SET `programId` = '".$_POST['progid']."', `description` = '".$_POST['progdesc']."', `programUrl` = '".$_POST['progurl']."', `ownerAfId` = '".$_POST['progaffid']."' WHERE `id` = '".$_POST['id']."'");
	$msg="<center><strong>PROGRAM UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current file
	$qry="SELECT * FROM ".$prefix."dlbprograms WHERE id=".$id;
	$res=@mysql_query($qry);
	$program=@mysql_fetch_object($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Item</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Program Information </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">PROGRAM INFORMATION UPDATED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="productfrm" method="post" action="editprogram.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Program Information</font> </strong></td>
    </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Program ID </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="progid" type="text" id="progid" value="<?=$program->programId;?>" /></td>
  </tr>
  <tr>
    <td align="left" valign="top" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Description</font></strong></td>
    <td align="left" nowrap="nowrap"><textarea name="progdesc" id="progdesc" rows=5 cols=30><?=$program->description;?></textarea></td>
    </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Program URL</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="progurl" type="text" id="progurl" size="50" value="<?=$program->programUrl;?>" /></td>
    </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Default Affiliate ID:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="progaffid" type="text" id="progaffid" size="10" value="<?=$program->ownerAfId;?>" /></td>
  </tr>
  
  <tr valign="top">
    <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
    </tr>
</table>
</form>
<? 
}
?>
</body>
</html>
