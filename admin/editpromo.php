<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the promo ID
	if(isset($_REQUEST["promo"]))
	{ 
        $promoid=$_REQUEST["promo"];
    }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Save Changes")
{
	// Update the group
	if($_POST["type"] == "email" || $_POST["type"] == "tellafriend" || $_POST["type"] == "safelist" || $_POST["type"] == "squeezepage" || $_POST["type"] == "thankyou")
	{
		$qry="UPDATE ".$prefix."promotion SET content='".$_POST["content"]."',subject='".$_POST["subject"]."' WHERE id=".$promoid;
	}
	else
	{
		$qry="UPDATE ".$prefix."promotion SET content='".$_POST["content"]."' WHERE id=".$promoid;
	}
	@mysql_query($qry) or die("Unable to edit promo: ".mysql_error());

	$msg="<center><strong>PROMO ITEM UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current group
	$qry="SELECT * FROM ".$prefix."promotion WHERE id=".$promoid;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Item</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#D9E6D9" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Promo Item</font> </strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">PROMO ITEM UPDATED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="editfrm" method="post" action="editpromo.php">
<input type="hidden" name="promo" value="<?=$promoid;?>" />
<input type="hidden" name="type" value="<?=$mrow["type"];?>">
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr bgcolor="#FBDB79">
    <td align="center" bgcolor="#D9E6D9" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Promo Item </font> </strong></td>
  </tr>
<?  if($mrow["type"] == "email" || $mrow["type"] == "tellafriend" || $mrow["type"] == "safelist") { ?>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Subject:
      <input name="subject" type="text" id="subject" size="32" value="<?=$mrow["subject"];?>">
    </font></strong></td>
  </tr>
<? } else if($mrow["type"] == "squeezepage" || $mrow["type"] == "thankyou") { ?>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Title:
      <input name="subject" type="text" id="subject" size="50" value="<?=$mrow["subject"];?>">
    </font></strong></td>
  </tr>
<? } ?>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Content:</font></strong></td>
    </tr>
  <tr>
    <td align="left" nowrap="nowrap">
	  <textarea name="content" cols="50" rows="8" id="content"><?=$mrow["content"];?></textarea>
</td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
  </tr>
</table>
</form>
<? 
}
?>
</body>
</html>
