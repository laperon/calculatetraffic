<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php";
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
	// Get the site ID
	if(isset($_GET["id"]))
	{ $id=$_GET["id"]; }
	 else if(isset($_POST["id"]))
	{ $id=$_POST["id"]; }
	 else
	{
		echo "Error: No site ID found!";
		exit;
	}

// Get current Site details
	$qry="SELECT * FROM ".$prefix."reports WHERE id=".$id;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);
	
	$reportdate = date('jS \of F Y', $mrow["date"]);
	$reporttime = date('g:i A', $mrow["date"])." (".date('H:i:s', $mrow["date"]).")";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="datepickercontrol/datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol/datepickercontrol.css">
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<style>
.form {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color=#000000;
}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Abuse Report Details</font></strong></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap" colspan="2">Date Reported: <? echo($reportdate." ".$reporttime);?></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap" colspan="2">Reported By: <?=$mrow["userfrom"];?></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap" colspan="2">Site ID: <?=$mrow["siteid"];?></td>
  </tr>
  <tr>
    <td align="left" colspan="2"><center><p><b>Notes:</b></p><textarea wrap="soft" rows="4" cols="30"><?=$mrow["text"];?></textarea></center></td>
  </tr>
  
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" class="form" id="Submit" onClick="javascript:self.close();" value="Close" /></td>
  </tr>
</table>
<center><?=$msg;?></center>
</form>
</body>
</html>
