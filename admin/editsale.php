<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";


include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the fileid
	if(isset($_GET["saleid"]))
	{ $saleid=$_GET["saleid"]; }
	 else if(isset($_POST["saleid"]))
	{ $saleid=$_POST["saleid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Save Changes")
{
	$saleid=$_POST["saleid"];
	$saledate=$_POST["saledate"];
	$affid=$_POST["affid"];
	$purchaserid=$_POST["purchaserid"];
	$itemid=$_POST["itemid"];
	$itemname=$_POST["itemname"];
	$itemamount=$_POST["itemamount"];
	$commission=$_POST["commission"];
	$txn_id=$_POST["txn_id"];
	
	if($_POST["status"]=="" || $_POST["status"]=="U") $status="U"; else $status=$_POST["status"];
	
	if ($status=="U") {
		$qry="UPDATE ".$prefix."sales SET saledate='$saledate',affid=$affid,purchaserid=$purchaserid,itemid='$itemid',itemname='$itemname',itemamount=$itemamount,commission=$commission,txn_id='$txn_id',status=NULL WHERE salesid=$saleid";
	} else {
		$qry="UPDATE ".$prefix."sales SET saledate='$saledate',affid=$affid,purchaserid=$purchaserid,itemid='$itemid',itemname='$itemname',itemamount=$itemamount,commission=$commission,txn_id='$txn_id',status='$status' WHERE salesid=$saleid";
	}

	@lfmsql_query($qry) or die("Unable to edit product: ".lfmsql_error());

	$msg="<center><strong>SALE UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current file
	$qry="SELECT * FROM ".$prefix."sales WHERE salesid=".$saleid;
	$res=@lfmsql_query($qry);
	$sale=@lfmsql_fetch_object($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Item</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Sale </font></strong></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><p align="center">SALE UPDATED </p>
    </td>
  </tr>
  
  <tr>
    <td align="center"><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></td>
  </tr>
</table>

<?	
	}
	else
	{
?>
<form name="productfrm" method="post" action="editsale.php">
<input type="hidden" name="saleid" value="<?=$saleid;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Sale </font> </strong></td>
    </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Sale Date </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="saledate" type="text" id="saledate" value="<?=$sale->saledate;?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Referrer </strong></font></td>
    <td align="left" nowrap="nowrap"><select name="affid" id="affid">
	<option value="0">No Referrer</option>
	<? select_member($sale->affid); ?>
    </select>
    </td>
    </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Buyer</font></strong></td>
    <td align="left" nowrap="nowrap"><select name="purchaserid" id="purchaserid">
	<option value="0">N/A</option>
	<? select_member($sale->purchaserid); ?>
    </select>
    </td>
    </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Item ID </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="itemid" type="text" id="itemid" value="<?=$sale->itemid;?>" size="16" /></td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Item Name  </strong></font></td>
    <td align="left" nowrap="nowrap"><input name="itemname" type="text" id="itemname" value="<?=$sale->itemname;?>" size="30" /></td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Amount</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="itemamount" type="text" id="itemamount" value="<?=$sale->itemamount;?>" size="6" /></td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Commission</strong></font></td>
    <td align="left" nowrap="nowrap"><input name="commission" type="text" id="commission" value="<?=$sale->commission;?>" size="6" /></td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>TXN ID </strong></font></td>
    <td align="left" nowrap="nowrap"><input name="txn_id" type="text" id="txn_id" value="<?=$sale->txn_id;?>" /></td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Status</strong></font></td>
    <td align="left" nowrap="nowrap"><select name="status" id="status">
      <option value="U" <? if($sale->status =="U" || $sale->status == "") { ?>selected="selected" <? } ?>>U</option>
      <option value="P" <? if($sale->status =="P") { ?>selected="selected" <? } ?>>P</option>
      <option value="R" <? if($sale->status =="R") { ?>selected="selected" <? } ?>>R</option>
    </select></td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap">&nbsp;</td>
    <td align="left" nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr valign="top">
    <td align="left" nowrap="nowrap">&nbsp;</td>
    <td align="left" nowrap="nowrap">&nbsp;</td>
  </tr>
  
  <tr valign="top">
    <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
    </tr>
</table>
</form>
<? 
}
?>
</body>
</html>
