<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

	// Get the fileid
	if(isset($_GET["partnerid"]))
	{ $id=$_GET["partnerid"]; }
	 else if(isset($_POST["partnerid"]))
	{ $id=$_POST["partnerid"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}


// Update promo table
if($_POST["Submit"] == "Save Changes")
{
	$name = $_POST["name"];
	$domain = $_POST["domain"];
	$descr = $_POST["descr"];
	$referid = $_POST["referid"];
	
	$qry="UPDATE ".$prefix."spartners SET name='".$name."',domain='".$domain."',descr='".$descr."',referid='".$_POST["referid"]."' WHERE id=$id";

	@mysql_query($qry) or die("Unable to edit partner: ".mysql_error());

	$msg="<center><strong>INFO UPDATED!</strong></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

	// Get current file
	$qry="SELECT * FROM ".$prefix."spartners WHERE id=".$id;
	$res=@mysql_query($qry);
	$product=@mysql_fetch_object($res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Edit Item</title>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?
	if($_POST["Submit"] == "Save Changes")
	{
?>
<p align="center">INFORMATION UPDATED </p>
<?	
	}
?>
<form name="productfrm" method="post" action="editspartner.php">
<input type="hidden" name="partnerid" value="<?=$id;?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr class="membertd">
    <td colspan="2" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Partner Information </font> </strong></td>
    </tr>
    
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Site Name:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="name" type="text" id="name" value="<?=$product->name;?>" /></td>
  </tr>
    
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Domain:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="domain" type="text" id="domain" value="<?=$product->domain;?>" /></td>
  </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referral ID:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="referid" type="text" id="referid" value="<?=$product->referid;?>" /></td>
  </tr>
  
  <tr valign="top">
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Description:</font></strong></td>
    <td align="left" nowrap="nowrap"><textarea name="descr" wrap="soft" rows="4" cols="25"><?=$product->descr;?></textarea></td>
  </tr>

  <tr valign="top">
    <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
    </tr>
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" id="Submit" value="Cancel" onClick="javascript:self.close();" />
      <input type="submit" name="Submit" value="Save Changes" /></td>
    </tr>
</table>
</form>
</body>
</html>
