<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

include "../inc/checkauth.php";
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
	// Get the text ID
	if(isset($_GET["id"]))
	{ $id=$_GET["id"]; }
	 else if(isset($_POST["id"]))
	{ $id=$_POST["id"]; }
	 else
	{
		echo "Error: No text ID found!";
		exit;
	}

// Update user record and refresh main admin page
if($_POST["Submit"] == "Update")
{
	$qry="UPDATE ".$prefix."mtexts SET text='".$_POST["text"]."', target='".$_POST["target"]."', imps='".$_POST["imps"]."', state='".$_POST["state"]."' WHERE Id=".$_POST["id"];

	@mysql_query($qry) or die(mysql_error());
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=".$_POST["memid"]." limit 1");
	
	$msg="<center><font color=\"red\">Text ad record updated!</font></center>";

	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";

}

// Get current Text details
	$qry="SELECT * FROM ".$prefix."mtexts WHERE id=".$id;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<script type="text/javascript" src="datepickercontrol/datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol/datepickercontrol.css">
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<style>
.form {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	color=#000000;
}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form name="memfrm" method="post" action="edittext.php">
<input type="hidden" name="id" value="<?=$id;?>" />
<input type="hidden" name="memid" value="<?=$mrow["memid"];?>" />
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Edit Text Ad Details</font></strong></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap" colspan="2">Total Hits Received: <?=$mrow["hits"];?></td>
  </tr>
  <tr>
    <td width="30" align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ad Text:</font></strong></td>
    <td align="left"><input name="text" type="text" class="form" id="text" value="<?=$mrow["text"];?>" /></td>
  </tr>
  <tr>
    <td width="30" align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Target URL:</font></strong></td>
    <td align="left"><input name="target" type="text" class="form" id="target" value="<?=$mrow["target"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Hits Assigned:</font></strong></td>
    <td align="left"><input name="imps" type="text" class="form" id="imps" value="<?=$mrow["imps"];?>" /></td>
  </tr>
  <tr>
    <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">State: </font></strong></td>
    <td align="left">
    <select name="state">
    <option value=0<? if ($mrow["state"]==0) { echo(" selected"); } ?>>Pending</option>
    <option value=1<? if ($mrow["state"]==1) { echo(" selected"); } ?>>Enabled</option>
    <option value=2<? if ($mrow["state"]==2) { echo(" selected"); } ?>>Paused</option>
    <option value=3<? if ($mrow["state"]==3) { echo(" selected"); } ?>>Suspended</option>
    </select>
    </td>
  </tr>
  
  <tr>
    <td colspan="2" align="center"><input name="Button" type="button" class="form" id="Submit" onClick="javascript:self.close();" value="Close" />
    <input name="Submit" type="submit" class="form" value="Update" /></td>
  </tr>
</table>
<center><?=$msg;?></center>
</form>
</body>
</html>
