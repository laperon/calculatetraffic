<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

?>
<p><strong><?=$msg;?></strong></p>
<form method="POST" action="admin.php?f=smr">
<? if(isset($_POST["memcheck"])){ ?>
<input type="hidden" name="emailarray" value="<?=htmlentities(serialize($emailarray));?>" />
<? } ?>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td width="50">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><table border="0" align="left" cellpadding="2" cellspacing="0">
      <tr>
        <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Region: </font></strong></td>
        <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <select name="region" id="region">
            <option value="USA" selected="selected">USA</option>
            <option value="UK">United Kingdom</option>
            <option value="ME">Mainland Europe</option>
            <option value="OT">Other</option>
          </select>
        </font></strong></td>
      </tr>
      <tr>
        <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Subject:</font></strong></td>
        <td><input name="emailsubject" type="text" id="emailsubject" size="32" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Message:</font></strong></td>
  </tr>
  <tr>
    <td width="30%"><textarea name="emailmessage" cols="40" rows="10" id="emailmessage"></textarea></td>
    <td align="left" valign="top"><p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The following tag substitutions may be used: </font></p>
        <p>#FIRSTNAME# - Member first name <br />
    #LASTNAME# - Member last name <br />
    #USERNAME# - Member login username <br />
    #CREDITS# - Member's unassigned credit balance<br />
    #AFFILIATEID# - The members affiliate ID <br />
    #BALANCE# - Commissions balance not yet paid<br />
    #SITENAME# - The name of this site<br />
    #REFURL# - The referral link </p>    </td>
  </tr>
  <tr>
    <td align="right"><input type="submit" name="Submit" value="Send" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">	</td>
  </tr>
</table>
</form>

