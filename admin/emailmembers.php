<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };


	// Check whether this is an email to all or selected members
	if(!isset($_POST["memcheck"]))
	{
		$msg="Emailing: All Members";
		$gvar="all";
	}
	else
	{ 
		// Get the member id's for emailing into an array
		if(isset($_POST["memcheck"]) || $_POST["EmailSelected"] == "Email Selected")
		{
			$ecnt=0;
			$emailarray = array();
			echo "<br>Email will be sent to: ";
			while (list ($key,$val) = @each ($_POST["memcheck"])) 
			{
				$eqry="SELECT Id,firstname,lastname,email FROM ".$prefix."members WHERE newsletter=1 AND Id=".$val;
				$eres=@lfmsql_query($eqry);
				$erow=@lfmsql_fetch_array($eres);
				echo $erow["email"]." | ";
				array_push($emailarray,$erow["email"]);
				$ecnt++;
			}
			
			if ($ecnt==0) { 
				echo "No members were selected!";
				exit;
			}
			
			$msg="Emailing Selected Members";
			$gvar="sel";
		}
	}
?>
<p align="center"><strong><?=$msg;?></strong></p>

<?
if ($msg == "Emailing: All Members") {
	?>
	<center>
	<form name="mbrowse" id="mbrowse" method="post" action="admin.php?f=mm">
	<input name="EmailMT" type="submit" value="Email Member Level" />
	</form>
	</center>
	<?
}
?>

<form method="POST" action="admin.php?f=sm&r=<?=$gvar;?>">
<? if(isset($_POST["memcheck"])){ ?>
<input type="hidden" name="emailarray" value="<?=htmlentities(serialize($emailarray));?>" />
<? } ?>
<table width="760" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Subject: 
    </font></strong>
    <input name="emailsubject" type="text" id="emailsubject" /></td>
  </tr>
  <tr>
    <td colspan="2"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Message:</font></strong></td>
  </tr>
  <tr>
    <td width="30%"><textarea name="emailmessage" cols="40" rows="10" id="emailmessage"></textarea></td>
    <td align="left" valign="top"><p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The following tag substitutions may be used: </font></p>
    <p>#FIRSTNAME# - Member first name <br />
    #LASTNAME# - Member last name <br />
    #USERNAME# - Member login username <br />
    #CREDITS# - Member's unassigned credit balance<br />
    #AFFILIATEID# - The members affiliate ID <br />
    #BALANCE# - Commissions balance not yet paid<br />
    #SITENAME# - The name of this site<br />
    #REFURL# - The referral link </p>
    </td>
  </tr>
  <tr>
    <td align="right"><input type="submit" name="Submit" value="Send" /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">	</td>
  </tr>
</table>
</form>
