<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
<p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Q. I made a mistake in the homepage and now it won't display at all. How can I reset it?</font></strong></p>
<p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>A.</strong> There is a folder called 'templates'. In this folder you will find three files called header,php, body.php and footer.php. These files contain the default HTML that shipped with this application. Just copy the code from the appropriate file into the text area in the Template Management section and click on the corresponding 'Update' button.</font></p>
<p>&nbsp;</p>
<p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Q. I've updated the homepage but the login form doesn't seem to be working and/or displaying properly. How can I fix it? </strong></font></p>
<p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>A.</strong>  The default html code for the login form is in a file called loginform.html in the 'inc' folder. You can copy and paste this code into your homepage template design. </font></p>
</body>
</html>
