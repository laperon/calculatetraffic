<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "../inc/filter.php";

	// Prevent anyone who isn't logged in from opening this page
	if(!isset($_SESSION["adminid"])) { exit; };

	// Upload a new file
	if(trim($_POST["Submit"]) == "Upload File")
	{
		$msg="";

		if ( ! isset($_SERVER['DOCUMENT_ROOT'] ) )
	  	$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr(
    	$_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF']) ) );
		
		$serverpath=$_SERVER["DOCUMENT_ROOT"];
		$newfilepath = $serverpath.$row["filelibpath"].basename( $_FILES['fname']['name']);
		if(!move_uploaded_file($_FILES['fname']['tmp_name'], $newfilepath))
		{
			$msg="There was an error uploading the file.<br>Check permissions on ".$linkimagepath;
		}
		else
		{
			// Add it to the filelib table
			lfmsql_query("INSERT INTO ".$prefix."filelib(filename) VALUES ('".basename( $_FILES['fname']['name'])."')");
		}
	}

	// Add a new file
	if(trim($_POST["Submit"]) == "Add File")
	{
			// Add it to the filelib table
			$filetitle=trim($_POST["filetitle"]);
			$filename=trim($_POST["filename"]);
			$description=trim($_POST["description"]);
			$filetag=trim($_POST["filetag"]);
			lfmsql_query("INSERT INTO ".$prefix."filelib(filetitle,filename,description,filetag) VALUES ('$filetitle','$filename','".$description."','$filetag')");
	}

?>
<br />
<table align="center" cellpadding="4" cellspacing="0">
  <tr><td>
  <form name="frm" enctype="multipart/form-data" method="POST" action="admin.php?f=fl">
  <table align="center" style="border:thin solid; border-width: 1px; border-color:#000000">
    <tr>
    <td nowrap="nowrap"><input type="file" name="fname" id="fname" /></td>
    <td nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="Upload File" /></td>
  </tr>
  <tr><td colspan="2" align="center">
  Max Upload Size: <? echo ini_get('upload_max_filesize'); ?><br>
  Max POST Size: <? echo ini_get('post_max_size'); ?>
  </td></tr>
  </table></form></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">OR</font></strong></td>
  </tr>
  <tr>
    <td colspan="2">
    <form name="addfilefrm" method="POST" action="admin.php?f=fl">
	<table width="100%" border="1" cellpadding="4" cellspacing="0" bordercolor="#000000">
      <tr bordercolor="#000000" class="membertd">
        <td nowrap="nowrap" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Title</font></strong></td>
        <td nowrap="nowrap" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Tag</font></strong></td>
        <td nowrap="nowrap" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Filename</font></strong></td>
        <td nowrap="nowrap" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes</font></strong></td>
        <td nowrap="nowrap" class="admintd">&nbsp;</td>
      </tr>
      <tr align="center" valign="top" bordercolor="#FFFFFF">
        <td><input name="filetitle" type="text" id="filetitle" /></td>
        <td><input name="filetag" type="text" id="filetag" size="16" /></td>
        <td><input name="filename" type="text" id="filename" size="16" /></td>
        <td><textarea name="description" cols="20" rows="4" id="description"></textarea></td>
        <td><input name="Submit" type="submit" id="Submit" value="Add File" /></td>
      </tr>
    </table>
	</form>
	</td>
  </tr>
</table>
<p>&nbsp;</p>
<p align="center"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Current Files </font></strong></p>
<table border="0" align="center" cellpadding="4" cellspacing="1">
  <tr>
    <td align="center" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Title</font></strong></td>
    <td align="center" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Tag</font></strong></td>
    <td class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Filename</font></strong></td>
    <td width="200" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes</font></strong></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<?
	// Get current files
	$res=@lfmsql_query("SELECT * FROM ".$prefix."filelib");
	while($filelib = @lfmsql_fetch_object($res))
	{
?>  
  <tr valign="top" bgcolor="#EAF0FF">
    <td align="center"><?=$filelib->filetitle;?></td>
    <td align="center"><?=$filelib->filetag;?></td>
    <td><?=$filelib->filename;?></td>
    <td><?=$filelib->description;?></td>
    <td align="center"><a href="javascript:editFilelib(<?=$filelib->fileid;?>);">
	<img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
    <td align="center"><a href="javascript:delFilelib(<?=$filelib->fileid;?>);">
	<img src="../images/del.png" width="16" height="16" border="0" /></td>
  </tr>
<? } ?>  
</table>
<p>&nbsp;</p>
<table width="760" border="1" align="center" cellpadding="2" cellspacing="3" bordercolor="#333333" bgcolor="#FFFF99">
  <tr>
    <td align="left" valign="top" bordercolor="#FFFF99"><p><font size="2"><strong><font face="Verdana, Arial, Helvetica, sans-serif">From this area you can upload files to your designated download folder on your server.</font></strong></font></p>
    <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The folder that these files will be uploaded to is set in the System Settings. The setting is called 'File Library Folder'. You must make sure this folder exists and its permissions are set to 777 before uploading any files.</font></p>
    <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The 'Max Upload Size' and 'Max POST Size' figures at the top of the page are for informational purposes. These are the limits that your server has in place for uploading files.</font></p>
    <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The form at the top of the page with the 'Upload File' button will upload and add the file to the file library. The second form is for files that you have uploaded with your FTP program. </font></p>
    <ul>
      <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Tag</strong> - The tag can be used in any of your pages to provide a download link for the file. For example, if we had a file called <strong>myfile.zip</strong> and the tag was <strong>myfile</strong> we could add a link by placing the following text on a page: ~~myfile~~. <font color="#FF0000">As the intention is that you use the .htaccess file protection system that LFM now provides when delivering these files, the links to these files are not cloaked.</font> </font></li>
      <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Title -</strong> This is the name that will appear as the download link text.</font></li>
      <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Description - </strong>The description is for your own use. You can add any notes here about this file.</font></li>
    </ul></td>
  </tr>
</table>
