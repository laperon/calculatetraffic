<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.14
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

$msg="";
$errmsg="";

// Update user record and refresh main admin page
if(($_POST["GivePrize"] == "Give Prize") && ($_GET["process"] == "prize"))
{
	$creditprize = $_POST['creditprize'];
	$bannerprize = $_POST['bannerprize'];
	$textprize = $_POST['textprize'];
	$peelprize = $_POST['peelprize'];
	$sqbanprize = $_POST['sqbanprize'];
	$porkypprize = $_POST['porkypprize'];
	$xppointprize = $_POST['xppointprize'];
	$cashprize = $_POST['cashprize'];
	
	if (!isset($creditprize) || !is_numeric($creditprize)) { $creditprize = 0; }
	if (!isset($bannerprize) || !is_numeric($bannerprize)) { $bannerprize = 0; }
	if (!isset($textprize) || !is_numeric($textprize)) { $textprize = 0; }
	if (!isset($peelprize) || !is_numeric($peelprize)) { $peelprize = 0; }
	if (!isset($sqbanprize) || !is_numeric($sqbanprize)) { $sqbanprize = 0; }
	if (!isset($porkypprize) || !is_numeric($porkypprize)) { $porkypprize = 0; }
	if (!isset($xppointprize) || !is_numeric($xppointprize)) { $xppointprize = 0; }
	if (!isset($cashprize) || !is_numeric($cashprize)) { $cashprize = 0; }
	
	$prizename = $_POST['prizename'];
	
	if(isset($_POST["memcheck"])) {
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		
			if ($creditprize > 0) {
				@lfmsql_query("Update ".$prefix."members set credits=credits+".$creditprize." where Id=".$val) or die(lfmsql_error());
				$msg .= "Credit prizes given to User #".$val.".<br>";
			}
			
			if ($bannerprize > 0) {
				@lfmsql_query("Update ".$prefix."members set bannerimps=bannerimps+".$bannerprize." where Id=".$val) or die(lfmsql_error());
				$msg .= "Banner prizes given to User #".$val.".<br>";
			}
			
			if ($textprize > 0) {
				@lfmsql_query("Update ".$prefix."members set textimps=textimps+".$textprize." where Id=".$val) or die(lfmsql_error());
				$msg .= "Text prizes given to User #".$val.".<br>";
			}
			
			if (file_exists("peelads.php") && $peelprize > 0) {
				@lfmsql_query("Update ".$prefix."members set peelimps=peelimps+".$peelprize." where Id=".$val) or die(lfmsql_error());
				$msg .= "Peel prizes given to User #".$val.".<br>";
			}
			
			if (file_exists("sqbanners.php") && $sqbanprize > 0) {
				@lfmsql_query("Update ".$prefix."members set sqbanimps=sqbanimps+".$sqbanprize." where Id=".$val) or die(lfmsql_error());
				$msg .= "Square banner prize given to User #".$val.".<br>";
			}
			
			if (file_exists("../inc/porkyp_funcs.php") && $porkypprize > 0) {
				require_once "../inc/porkyp_funcs.php";
				$getmeminfo = lfmsql_query("SELECT email, lastip FROM ".$prefix."members WHERE Id=".$val." LIMIT 1");
				if (lfmsql_num_rows($getmeminfo) > 0) {
					$mememail = lfmsql_result($getmeminfo, 0, "email");
					$memip = lfmsql_result($getmeminfo, 0, "lastip");
					if (strlen($memip) < 1) {
						$memip = "NONE";
					}
					$porkytransfer = porkytransferpoints($mememail, $memip, $porkypprize);
					
					if ($porkytransfer === true) {
						$msg .= "Porky Points given to User #".$val.".<br>";
					} else {
						$msg .= "Inactive user or not enough Porky Points.<br>";
					}
				}
			}
			
			if (file_exists("../inc/xppoints_funcs.php") && $xppointprize > 0) {
				require_once "../inc/xppoints_funcs.php";
				$xpointstransfer = xpointstransfer($xppointprize, $_SERVER["SERVER_NAME"]." - ".$prizename, $val);
				$msg .= "XP Points given to User #".$val.".<br>";
			}
			
			if ($cashprize > 0) {
				@lfmsql_query("Insert into ".$prefix."sales (affid, saledate, itemid, itemname, itemamount, commission, prize) values (".$val.", NOW(), 0, '".$prizename."', '0.00', '".$cashprize."', 1)");
				$msg .= "Cash prizes given.<br>";
			}
		
		}
	} else {
		$errmsg = "No members selected.";
	}
}

?>
<html>
<body>

<center>
  <p><font size="2" color="#FF0000"" face="Arial, Helvetica, sans-serif">
  <?=$errmsg;?>
  <?=$msg;?>
  </font></p>
</center>
<br><br>
<form name="memfrm" method="post" action="giveprize_pop.php?process=prize">
<table width="230" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Give Prizes</font></strong></td>
  </tr>
  <?
  
  if(isset($_POST["memcheck"])) {
  		echo("<tr><td colspan=\"2\" align=\"center\">
  		<b>Give Prize To: </b><br>");
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
			echo("<input type=\"checkbox\" class=\"mcheck\" name=\"memcheck[]\" value=\"".$val."\" checked />User ID: <b>".$val."</b><br>");
		}
		echo("</td></tr>");
  } else {
  	echo("<tr><td colspan=\"2\" align=\"center\"><b>No Members Selected</b></td></tr>");
  	exit;
  }

  ?>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Prize Name:</font></strong></td>
    <td align="left"><input name="prizename" maxlength="25" type="text" class="form" id="prizename" value="Prize" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Credit Prize:</font></strong></td>
    <td align="left"><input name="creditprize" type="text" class="form" id="creditprize" value="0" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Banner Ad Prize:</font></strong></td>
    <td align="left"><input name="bannerprize" type="text" class="form" id="bannerprize" value="0" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Text Ad Prize:</font></strong></td>
    <td align="left"><input name="textprize" type="text" class="form" id="textprize" value="0" /></td>
  </tr>
  
  <? if (file_exists("peelads.php")) { ?>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Peel Ad Prize:</font></strong></td>
    <td align="left"><input name="peelprize" type="text" class="form" id="peelprize" value="0" /></td>
  </tr>
  <? } ?>
  
  <? if (file_exists("sqbanners.php")) { ?>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Square Banner Prize:</font></strong></td>
    <td align="left"><input name="sqbanprize" type="text" class="form" id="sqbanprize" value="0" /></td>
  </tr>
  <? } ?>
  
  <? if (file_exists("../inc/porkyp_funcs.php")) { ?>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Porky Points Prize:</font></strong></td>
    <td align="left"><input name="porkypprize" type="text" class="form" id="porkypprize" value="0" /></td>
  </tr>
  <? } ?>
  
  <? if (file_exists("../inc/xppoints_funcs.php")) { ?>
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">XP Points Prize:</font></strong></td>
    <td align="left"><input name="xppointprize" type="text" class="form" id="xppointprize" value="0" /></td>
  </tr>
  <? } ?>
  
  <tr>
    <td align="left" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Cash Prize:</font></strong></td>
    <td align="left"><input name="cashprize" type="text" class="form" id="cashprize" value="0.00" /></td>
  </tr>
    
  <tr>
    <td colspan="2" align="center"><input name="GivePrize" type="submit" class="form" value="Give Prize" /></td>
  </tr>
</table>
</form>

</body>
</html>
