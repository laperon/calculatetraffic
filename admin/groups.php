<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

$msg="";

// Adding a new group
if($_POST["Submit"] == "Add New Group" && strlen(trim($_POST["newgroup"])) > 0)
{
	// Check if it exists
	$cqry="SELECT * FROM ".$prefix."groups WHERE groupname='".$_POST["newgroup"]."'";
	$cres=@mysql_query($cqry);
	
	if(mysql_num_rows($cres) > 0)
	{
		$msg="Group '".$_POST["newgroup"]."' already exists!";
	}
	else
	{
		$qry="INSERT INTO ".$prefix."groups(groupname) VALUES('".$_POST["newgroup"]."')";
		@mysql_query($qry) or die("Unable to create new group: ".mysql_error());
	}
}

// Move members
if(trim($_POST["Submit"]) == "> Move Selected Members >")
{
	$listvals=$_POST['groupmembers'];

	if(!isset($_POST["grouplist"]))
	{
		$msg="Please select a group to move members to.";
	}
	else
	if(count($listvals)>0)
	{
   		$n=count($listvals);
    	for($i=0;$i<count($listvals);$i++)
		{
			$mvqry="UPDATE ".$prefix."members SET groupid=".$_POST["grouplist"]." WHERE Id=".$listvals[$i];
			@mysql_query($mvqry) or die("Unable to move members: ".mysql_error());
		}
	}
}
?>
<style type="text/css">
<!--
.style1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
}
-->
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />
<center>
<p>&nbsp;</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	<form name="groups" method="post" action="admin.php?f=gr&gn=true">
	  <p>&nbsp;</p>
	  <table width="350" border="0" align="center" cellpadding="3" cellspacing="0" style="border: 1px solid #999;">
      <tr>
        <td colspan="3" align="center" class="admintd"><span class="style1">Group Management </span></td>
        </tr>
      <tr>
        <td width="10%"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Group:</font></strong></td>
        <td nowrap="NOWRAP">
		<select name="group" id="group">
<?
		// Get the list of groups
		$gres=@mysql_query("SELECT * FROM ".$prefix."groups") or die("Unable to get group list: ".mysql_error());
		while($grow=@mysql_fetch_array($gres))
		{
			if($grow["groupid"] == $_POST["group"])
			{
?>
		<option selected value="<?=$grow["groupid"];?>"><?=$grow["groupname"];?></option>
<? 
} else { 
?>
		<option value="<?=$grow["groupid"];?>"><?=$grow["groupname"];?></option>
<?  }
} 
?>
        </select></td><td nowrap="NOWRAP"><input type="Submit" name="Submit" value="Show Group Members" />
        <input type="button" name="EdGrp" value="Edit Group" onClick="javascript:editGroup(document.groups.group.options[document.groups.group.selectedIndex].value)">
         <input name="DelGrp" type="button" id="Submit" value="Delete Group" onClick="javascript:confirmDelGroup(document.groups.group.options[document.groups.group.selectedIndex].value)" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><input name="EmailGrp" type="submit" id="EmailGrp" value="Email Group Members" /></td>
      </tr>
      <tr>
        <td colspan="3" class="admintd"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Groupname</strong></font></td>
        </tr>
      <tr valign="top">
        <td colspan="3" align="left" nowrap="nowrap"><input name="newgroup" type="text" id="newgroup" />          <input type="submit" name="Submit" value="Add New Group" /></td>
        </tr>
      <tr valign="top">
        <td align="center">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
    </table>
	</form>	</td>
  </tr>
  <tr>
    <td align="center"><font color="red"><strong><br />
      <?=$msg;?></strong></font></td>
  </tr>
  <tr>
    <td>
<?
	// Get details/members for selected group
	if(isset($_GET["gn"]))
	{
		$groupid=$_POST["group"];
		$gnres=@mysql_query("SELECT COUNT(*) as gtotal FROM ".$prefix."members WHERE groupid=$groupid");	
		$gnrow=@mysql_fetch_array($gnres);
		
		$gpres=@mysql_query("SELECT comm FROM ".$prefix."groups WHERE groupid=$groupid");
		$gprow=@mysql_fetch_array($gpres);
?><p>&nbsp;</p>
	<table border="0" align="center" cellpadding="3" cellspacing="0" style="border: 1px solid #999;">
      <tr>
        <td align="center" class="admintd">
		<strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Total Members </font></strong></td>
        </tr>
      <tr>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><?=$gnrow["gtotal"];?></font></td>
        </tr>
    </table>
	<p>&nbsp;</p>
	<form name="grpmove" method="post" action="admin.php?f=gr&gn=true">
	<input type="hidden" name="group" value="<?=$_POST["group"];?>" />
	<table width="300" align="center">
	<tr>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  </tr>
	<tr>
<?
	if($gnrow["gtotal"] > 0)
	{
	
?>	
	<td><select name="groupmembers[]" size="20" multiple="multiple">
<?
	// Get the list of group members
	$glres=@mysql_query("SELECT firstname,lastname,Id FROM ".$prefix."members WHERE groupid=$groupid");
	while($glrow=@mysql_fetch_array($glres))
	{
?>
	<option value="<?=$glrow["Id"];?>"><?=$glrow["firstname"];?>&nbsp;<?=$glrow["lastname"];?></option>
<?
}
?>
	</select></td>
	<td align="center" nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="&gt; Move Selected Members &gt;" /></td>
	<td><select name="grouplist" size="20" id="grouplist">
<?
	// Get the list of groups
	$glistres=@mysql_query("SELECT groupid,groupname FROM ".$prefix."groups");
	while($glistrow=@mysql_fetch_array($glistres))
	{
?>		
		<option value="<?=$glistrow["groupid"];?>"><?=$glistrow["groupname"];?></option>
<?	}
?>
	</select>
	</td>
 </tr>
</table>
</form>	
      <?
	}
}
?>
</td>
  </tr>
</table>

<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  <tr>
    <td bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
        <ul>
          <li>Member groups are a convenient way of grouping members together for mailing purposes. You can create very specific groups of members this way and then email them from here. </li>
        </ul>
        <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</center>
