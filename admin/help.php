<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>LFMTE - Admin Help</title>
</head>
<style type="text/css">
body { 
	font-family:Arial, Helvetica, sans-serif ;
}
</style>
<body>
<div align="center">
  <h2>LFMTE - Admin </h2>
</div>

<center>
<table cellpadding=5 cellspacing=0 style="border-style:dashed; border-width:3px;" bordercolor=black width=400><tr><td align=left valign=center>
<center><p><font size=3><b>LFMTE Tutorial Videos</b></font></p></center>
<p><font size=2><b>To learn how to get the most from your LFMTE script, <a target="_blank" href="http://thetrafficexchangescript.com/tutorialvideos.php">Click Here</a> to watch our tutorial videos.</b></font></p>
</td></tr></table>
</center><br>

<table width="300" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><h2><strong>Help Contents</strong></h2></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>1. Installation</strong></p>
      <table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td><p><font size="3"><a href="#requirements">System Requirements </a><br />
          </font></p>          </td>
        </tr>
        <tr>
          <td><font size="3"><a href="#installation">Installing for the first time</a></font></td>
        </tr>
        <tr>
          <td><a href="#filesecurity"><font size="3">File Security </font></a></td>
        </tr>
      </table>
      <br /></td>
  </tr>
   <tr>
    <td><p><strong>2. Upgrade</strong></p>
      <table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td><font size="3"><a href="#updating">Updating Your Script</a> </font></td>
        </tr>
      </table>
      <br /></td>
  </tr>
  
  <tr>
    <td><p><strong>3. System Settings</strong> </p>
      <table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td><font size="3"><a href="#sitename">Sitename</a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#metadesc">Meta Description</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#metakey">Meta Keywords</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#adminname">Admin Name</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#adminlogin">Admin Login</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#adminpass">Admin Password</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#adminemail">Admin Email </a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#mailfrom">Mail List 'From' Address </a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#paypalemail">PayPal Email </a></font></td>
        </tr>
        
        <tr>
          <td><font size="3"><a href="#backgroundmaildespatch">Background Mail Despatch</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#autoresponderemail">Autoresponder Email </a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#smtpserver">SMTP Server </a></font></td>
        </tr>
      </table>      
      <br /></td>
  </tr>
  <tr>
    <td><p><strong>4. Membership Settings</strong> </p>
      <table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td><font size="3"><a href="#membermanagement">Member Management</a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#membertypes">Member Types </a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#freelevel">The Free Level </a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#levelsettings">Member Level Settings </a></font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#membergroups">Member Grouping </a></font></td>
        </tr>
      </table>      
      <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><p><strong>5. OTO</strong> </p>
      <table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td><font size="3"><a href="#displaysetting">Display Setting</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#salescopy">Sales Copy</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#footer">Footer</a></font></td>
        </tr>
      </table>      
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><p><strong>6. Affiliate Links</strong> </p>
      <table width="75%" border="0" align="center" cellpadding="0" cellspacing="0">

        <tr>
          <td><font size="3"><a href="#affiliatelinks">Affiliate Links </a></font></td>
        </tr>
      </table>      
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><p><strong>7. Templates</strong> </p>
      <table width="75%" border="0" align="center" cellpadding="2" cellspacing="0">
        <tr>
          <td><font size="3"><a href="#affmain">Affiliate Main Page</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#affheader">Site Header</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#afffooter">Site Footer</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#affhomepage">Member Homepage</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#suspended">Suspended Page</a> </font></td>
        </tr>
        <tr>
          <td><font size="3"><a href="#salespage">Sales Page</a> </font></td>
        </tr>
      </table>      
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><p><strong>8. LFM Macros</strong> </p>
      <table width="75%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td><font size="3"><a href="#macrolist">Macro List  </a></font></td>
        </tr>
      </table>      
    <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="75%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><h3><a name="installation" id="installation"></a>1. Installation </h3></td>
  </tr>
  <tr>
    <td><p><br />
        <a name="requirements" id="requirements"></a><strong>System Requirements</strong></p>
      <p>The system requirements to run the Launch Formula Marketing script  are a unix based web server (Apache for example), PHP v4.0 or greater and MySQL v4.0 or greater. </p></td>
  </tr>
  <tr>
    <td><p><strong>Installing For The First Time</strong></p>
      <p> 
        Installing for the first time is a simple four step process.</p>
      <ul>
        <li><strong>1. </strong>Create a database. If you only have access to one database, the installer will allow you to define a prefix for the tables. You will need to know your database hostname, database name, database user and password when you run the installer.<br /><br /></li>
        <li><strong>2.</strong> Run the installation wizard. The wizard will ask for your server ftp details so it can upload the files and will also give you the option to choose the location that you will be uploading to. For more information regarding the location please view the installation video.</li>
        <li>Note: <strong>If you are unable to use the install wizard</strong> you can download LFM in zip format. Unzip the files to a location on your hard drive and then upload the files to  your server. You can upload to any directory you like. For example, I could upload to a 'members' directory if I wanted my members to access their member area at http://www.yourserver.com/members/.<br />
          <br />
        </li>
        <li><strong>3.</strong> <strong>If you are using the ZIP file</strong> you will also need to check/set permissions. There is only one file and one directory that need to have specific permissions. This is as follows (assuming that you have installed in a directory called 'members'):<br />
          <br />
          <ul>
            <li>/members/inc/config.php - set permissions on this file to 666 </li>
            <li>/members/1qaz2wsx - set permissions on this directory to 777 <br />
              <br />
              <br />
            </li>
          </ul>
      </li>
        <li><strong>4.</strong> Completing the installation -<strong>If you are using the wizard</strong>, it will automatically open the installation page in your browser. Simply follow the instructions and enter your database details when prompted. If you uploaded the files from the ZIP file you will need to point your browser to the installation page. This is in the admin directory, so if you installed LFM into a directory called 'members' the location of the install page would be:</li>
      </ul>
      <blockquote>
        <p>http://www.yourserver.com/members/admin/install.php</p>
        <p>Once you have completed the install and logged in for the first time, you should delete the install.php file.</p>
      </blockquote>
      <p><strong>If Something Goes Wrong</strong></p>
      <p>DIf you experience any errors during the installation process and are unable to resolve the error(s) please submit a ticket at the LFM  <a href="http://www.akhmediagroup.com/support/" target="_blank">Helpdesk</a> and our staff will assist with the installation. </p>
      <p><strong><a name="filesecurity" id="filesecurity"></a>File Security</strong></p>
      <p>The default file security is basic. However, in version 1.20 we introduced htaccess file protection. You should enable this in the System Settings for maximum file protection.</p>
      <p>In the System Settings you will also see a place where you can define the 'File Library Folder. You should rename the '1qaz2wsx' folder and change the 'File Library Folder' setting to the new folder name for additional protection. </p>
      <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><h3><a name="updating" id="updating"></a>2. Updating </h3></td>
  </tr>
  <tr>
    <td><p><br />
      To check for updates to your LFMTE script, click the "Check For Updates" link at the very bottom of your admin area.</p>
      <p>&nbsp;</p>    </td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><h3>3. System Settings </h3></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="sitename" id="sitename"></a>Sitename</strong> - The sitename will appear in page titles and wherever you use the macro #SITENAME# in outgoing emails.</font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="metadesc" id="metadesc"></a>Meta Description</strong> - appears in the &lt;meta&gt; description in the default homepage for the site. </font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="metakey" id="metakey"></a>Meta Keywords</strong> - appear in the keywords &lt;meta&gt; tag in the default homepage for the site. </font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="adminname" id="adminname"></a>Admin Name</strong> - Your administrator name (e.g. John Bloggs). </font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="adminlogin" id="adminlogin"></a>Admin Login</strong> - Your administrator login. </font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="adminpass" id="adminpass"></a>Admin Password</strong> - Your administrator password. </font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="adminemail" id="adminemail"></a>Admin Email</strong> - Your administrator email. This email is used by notifications that are sent to you when a PayPal subscription is created or cancelled. </font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3"><strong><a name="mailfrom" id="mailfrom"></a>Mail List 'From' Address</strong> - The address that will appear as the reply address on emails that you send out using the internal mailer facility. </font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  
  <tr>
    <td><font size="3">&nbsp;</font></td>
  </tr>
  <tr>
    <td><p><strong><a name="backgroundmaildespatch" id="backgroundmaildespatch"></a>Background Mail Despatch</strong> - This option allows you to have all emails sent out as a background process if your server allows it. The requirements for this to function correctly are as follows:</p>
      <ul>
        <li>PHP Command Line - Your host must allow you access to PHP at the Unix command line. While most hosts will allow this, there are several large hosts which do not.<br />
        </li>
        <li>PHP fsockopen - Your host must allow you to use the PHP fsockopen command. All emails are sent using an internal smtp protocol implementation.</li>
    </ul>      
    <p>Please note that these requirements only apply to shared hosting. If you have a dedicated server then your server should meet these requirements. If not, your admin will be able to enable them. If you intend to use this feature it is recommended that you do a test mail first to ensure that your server meets the requirements. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="autoresponderemail" id="autoresponderemail"></a>Autoresponder Email</strong> - You can enter your autoresponder 'subscribe' address here and an email will be sent to your autoresponder whenever a new member joins. The email has the new members' 'From' address so that a 'confirm' email will be sent to them, allowing the new member to opt in to your list. To disable this setting, simply leave the field blank. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="smtpserver" id="smtpserver"></a>SMTP Server</strong> - This is the SMTP server that messages will be sent through when the 'Background Mail Despatch' option is enabled. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><h3>4. Members </h3></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="membermanagement" id="membermanagement"></a>Member Management</strong> - The member management area allows you to edit, delete, pay and email members. Most functions are fairly self explanatory. To email, pay, suspend selected members, simply tick the check boxes at the right hand side of the members you wish to select. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="membertypes" id="membertypes"></a>Member Types </strong>- Member types are your various membership 'levels'. These can be free or paid membership levels. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong>Overview</strong><br />
    The member types area automates some tasks depending on how you have set your member types. </p>
    <p>Firstly, if you have more than one member type and all member types are set to 'visible', the signup form will display a dropdown list that allows the person to choose their membership type. This is useful if you offer free and paid memberships and/or several levels of paid membership.</p>
    <p>You can also offer 'hidden' levels by using promo codes. If you make a level invisible and provide a promo code for that level, when the form is displayed to someone using the promo code they will see the 'special' level in the dropdown list and it will be selected for them. Note that to use a promo code you need to insert the promo code box on your sales page using one of the promo <a href="#macros">macros</a>. </p>
    <p>If a member is on a recurring paid membership through PayPal and the member cancels their subscription, LFM will change their membership level to the 'Free' level (regardless of whether you have renamed it). It is important to note that if you have made the Free level invisible, members whose subscription is cancelled will no longer be able to log in. They will receive a message on the screen saying 'It appears ytou do not have a valid subscription'. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong><a name="freelevel" id="freelevel"></a>The Free Level</strong> - When you install the Launch Formula Marketing script, a free level is created by default. You can edit this default level, but you cannot delete it. This is because there must always be at least one membership level available. </p>
    <p>The Free level also controls whether the script is a 'paid membership only' script. If you change the visibility of the Free level  to 'invisible' the script will be in 'paid membership only' mode. This means that if a member is signing up and they do not complete their payment, their details will be in the member list, but their account will show as 'Invalid Account'. </p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="levelsettings" id="levelsettings"></a>Member Level Settings</strong> </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><ul>
      <li><strong>Name</strong> -This is the name that is displayed in the signup form if you have multiple levels and/or the person signing up has used a promo code.</li>
      <li><strong>Fee</strong> - The cost of this membership.</li>
      <li><strong>Recur</strong> - If this membership is recurring you can set the recurring period.</li>
      <li><strong>Comm</strong> - The commission paid to members when a member they referred signs up to a paid membership. </li>
      <li><strong>OTO Comm</strong> - The commission paid to your members when someone they referred buys the One Time Offer.</li>
    </ul></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="membergroups" id="membergroups"></a>Member Grouping</strong> - Member groups are a convenient way to group members together for emailing, independent of the membership types. You can add members to a specific group, and then choose to email that group. </td>
  </tr>
  <tr>
    <td><p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><h3>5. OTO</h3></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>This section controls who sees your one time offer, as well as the sales copy. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="displaysetting" id="displaysetting"></a>Display Setting </strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><ul>
      <li><strong>Do Not Display Offer</strong> - The one time offer will not be displayed to anyone. This option, in effect, disables the one time offer. </li>
      <li><strong>Display Offer Only To New Members</strong> - The offer will only be seen by members who sign up. Existing members will not see the one time offer, even if the offer was previously disabled.</li>
      <li><strong>Display Offer Once To Existing Members</strong> - The offer will only be seen by members who have already signed up. New members will not see the offer when they sign up or when they log in to the member area. A scenario where you might use this is if you lowered the price of membership, and wanted to make a special offer only to those who paid the higher membership price.</li>
      <li><strong>Display Offer Once To Existing And New Members</strong> - The offer will be displayed to all current members as well as anyone who signs up. This will be the most commonly used option for sites that create new one time offers from time to time. </li>
    </ul>      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="salescopy" id="salescopy"></a>Sales Copy</strong> - This is the One Time Offer page that your members will see when they sign up or log in. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="footer" id="footer"></a>Footer</strong> - The contents of this footer are displayed below the payment button(s). </td>
  </tr>
  <tr>
    <td><p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><h3>6. Affiliate Links </h3></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="affiliatelinks" id="affiliatelinks"></a></strong>The Launch Formula Marketing script allows you to create many promotional items. Whatever you create in here will be seen by your members in their 'My Links' area. You can use the LFM <a href="#macros">macros</a> to include affiliate URLs, affiliate names etc. </td>
  </tr>
  <tr>
    <td><p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><strong>7. Templates </strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Templates can be created for the member, login and sales areas. To create a template you simply enter the html code and click the update button. The easiest way to do this is to use a html editor such as dreamweaver or MS Frontpage, and then copy the HTML code from there and paste it into the template area. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><p><strong><a name="affmain" id="affmain"></a>Affiliate Main Page</strong> - This is the page that your members see after they sign in. You can create a different page for each of your member levels by selecting the member level from the drop down menu. The various <a href="#macros">macros</a> can be used to substitute for the affiliates referral URL, first name, last name etc. </p>    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="affheader" id="affheader"></a>Site Header</strong> - The site header is shown on the member login page and inside the member area itself. This would generally be your header template graphic. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="afffooter" id="afffooter"></a>Site Footer</strong> - The site footer is also shown on the login and member pages. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="affhomepage" id="affhomepage"></a>Member Homepage</strong> - This is the login page (login.php). A basic template layout is provided.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="suspended" id="suspended"></a>Suspended Page</strong> - The suspended page is shown to any member who you have flagged as 'suspended. You might want to provide contact details in case the suspended member wishes to contact you, or simply a message stating that the member has been suspended. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><a name="salespage" id="salespage"></a>Sales Page</strong> - This is your main sales page (index.php). You will need to provide a link to the signup page (signup.php) in order to allow people to join your membership site. </td>
  </tr>
  <tr>
    <td><p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td><h3>8 .LFM Macros </h3></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>You can us the following macros in your web templates and emails sent out using the LFM mailer. </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><ul>
      <li><strong>#FIRSTNAME#</strong> - Member first name </li>
      <li><strong>#LASTNAME#</strong> - Member last name </li>
      <li><strong>#USERNAME#</strong> - Member login username </li>
      <li><strong>#AFFILIATEID#</strong> - The members affiliate ID </li>
      <li><strong>#CREDITS#</strong> - The number of unassigned credits in an account </li>
      <li><strong>#SITENAME#</strong> - The name of this site</li>
      <li><strong>#REFURL#</strong> - The referral link</li>
      <li><strong>#PROMOCODE#</strong> - Displays an entry box and an image (images/promo.jpg) allowing site visitors to enter a promocode and be taken to the signup page using the promo code. </li>
      <li><strong>#PROMOCODE_2#</strong> - The same as the first one, except that it is formatted in a table so that the promocode entry field is displayed directly below and centered with the image. </li>
    </ul></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><font size="2"><strong><font face="Tahoma"><center>Copyright &copy;2006-2009 AKH Media Group. All Rights Reserved</center></font></strong><font face="Tahoma"><br />
      Launch Formula Marketing (&quot;the script&quot;) is copyrighted to 
       AKH Media Group. 
       The sale, duplication or transfer of the script to any 
       person other than the original purchaser is a violation
       of the purchase agreement and is strictly prohibited.
       Any alteration of the script source code or accompanying 
      materials will void any responsibility that AKH Media Group 
      has regarding the proper functioning of the script.
      By using this script you agree to the terms and conditions 
   of use of the script. </font></font></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
