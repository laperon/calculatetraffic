<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

echo("<center><br><br>");

if ($_GET['action'] == "createtheme") {
	// Open Create Page
	include("iconthemes_create.php");
	exit;
}

if (isset($_GET['settheme'])) {
	// Change Theme
	$setthemename = urldecode($_GET['settheme']);
	mysql_query("UPDATE ".$prefix."settings SET surficontheme='".$_GET['settheme']."'") or die(mysql_error());
}


if (isset($_GET['deltheme'])) {
	// Delete Theme
	$delthemename = urldecode($_GET['deltheme']);
	if ($delthemename != "LFMTE_original") {
		$deldirectory = "../surficon_themes/".$delthemename;
		if (is_dir($deldirectory)) {
			
			$cachedir = $deldirectory."/cache";
			if (is_dir($cachedir)) {
				// Delete Cache Directory First
				$objects = scandir($cachedir);
				foreach ($objects as $object) {
					if ($object != "." && $object != "..") {
						if (filetype($cachedir."/".$object) == "dir") rmdir($cachedir."/".$object); else unlink($cachedir."/".$object);
					}
				}
				reset($objects);
				rmdir($cachedir);
			}
			
			$objects = scandir($deldirectory);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($deldirectory."/".$object) == "dir") rmdir($deldirectory."/".$object); else unlink($deldirectory."/".$object);
				}
			}
			reset($objects);
			rmdir($deldirectory);
		}
	}
}


function get_iconthemes() {
	global $icon_themes, $icon_broken_themes;

	if ( isset($icon_themes) )
		return $icon_themes;

	$themes = array();
	$icon_broken_themes = array();
	$theme_loc = $theme_root = dirname(dirname(__FILE__));

	// Files in themes directory and one subdir down
	$themes_dir = @ opendir($theme_root);
	if ( !$themes_dir )
		return false;

	while ( ($theme_dir = readdir($themes_dir)) !== false ) {
		if ( is_dir($theme_root . '/' . $theme_dir) && is_readable($theme_root . '/' . $theme_dir) ) {
			if ( $theme_dir{0} == '.' || $theme_dir == '..' || $theme_dir == 'CVS' )
				continue;

			$stylish_dir = @ opendir($theme_root . '/' . $theme_dir);
			$found_imagesfile = false;
			while ( ($theme_file = readdir($stylish_dir)) !== false ) {
				if ( $theme_file == 'surfimages.php' ) {
					$theme_files[] = $theme_dir . '/' . $theme_file;
					$found_imagesfile = true;
					break;
				}
			}
			@closedir($stylish_dir);
			if ( !$found_imagesfile ) { // look for themes in that dir
				$subdir = "$theme_root/$theme_dir";
				$subdir_name = $theme_dir;
				$theme_subdir = @ opendir( $subdir );
				while ( ($theme_dir = readdir($theme_subdir)) !== false ) {
					if ( is_dir( $subdir . '/' . $theme_dir) && is_readable($subdir . '/' . $theme_dir) ) {
						if ( $theme_dir{0} == '.' || $theme_dir == '..' || $theme_dir == 'CVS' )
							continue;
						$stylish_dir = @ opendir($subdir . '/' . $theme_dir);
						$found_imagesfile = false;
						while ( ($theme_file = readdir($stylish_dir)) !== false ) {
							if ( $theme_file == 'surfimages.php' ) {
								$theme_files[] = $subdir_name . '/' . $theme_dir . '/' . $theme_file;
								$found_imagesfile = true;
								break;
							}
						}
						@closedir($stylish_dir);
					}
				}
				@closedir($theme_subdir);
				// Missing Main Image File
			}
		}
	}
	if ( is_dir( $theme_dir ) )
		@closedir( $theme_dir );

	if ( !$themes_dir || !$theme_files )
		return $themes;

	sort($theme_files);

	foreach ( (array) $theme_files as $theme_file ) {
		if ( !is_readable("$theme_root/$theme_file") ) {	   
			// Theme Error
			continue;
		}

		$theme_name = $theme_file;
		$name = dirname($theme_file);
		
		// Built Preview Table
		if (!file_exists("$theme_root/$theme_file")) {
			echo("Error including file: ".$theme_root."/".$theme_file);
			exit;
		}
		include("$theme_root/$theme_file");
		if (!isset($images) || !is_array($images)) {
			// Missing Image Array
			continue;
		}
		
		$colcounter = 0;
		$previewhtml = "<table style=\"border: thin black solid;\" cellpadding=\"0\" cellspacing=\"0\">
		<tr>";
		
		foreach ($images as $key=>$value) {
			
			$imagedata = explode("|", $value);
			
			if (count($imagedata) != 5) {
				// Invalid image array format
				continue;
			}
			
			for ($i = 1; $i <= 4; $i++) {
				$imagefile = $imagedata[$i];
				if (stristr($previewhtml, $imagefile) === FALSE) {
				
					if ($colcounter >= 4) {
						// Start New Column
						$previewhtml .= "</tr>
						<tr>";
						$colcounter = 0;
					}
				
					$previewhtml .= "<td><img border=\"0\" width=\"50\" height=\"50\" src=\"/".$name."/".$imagefile."\"></td>";
					$colcounter = $colcounter+1;
				}
			}
			
		}
		$previewhtml .= "</tr>
		</table>";
		// End Built Preview Table

		$themes[$name] = array('Name' => $name, 'Preview' => $previewhtml);
	}

	$icon_themes = $themes;

	return $themes;
}

$themelist = get_iconthemes();

if (!is_array($themelist)) {
	echo("Error getting theme list");
	exit;
}

$current_theme = mysql_result(mysql_query("SELECT surficontheme from ".$prefix."settings LIMIT 1"), 0);

echo("<p><b>Your Current Surfbar Icon Theme Is: ".$current_theme."</b></p>
<p><font size=\"3\" color=\"blue\"><b><a href=\"admin.php?f=iconstheme&action=createtheme\">Create A New Surfbar Icon Theme</a></b></font></p>
<hr>
");

foreach ($themelist as $key=>$value) {

	$displayname = str_replace("surficon_themes/", "", $themelist[$key]['Name']);

	echo("<p><font size=\"3\" color=\"black\"><b>".$displayname."</b></font></p>
	".$themelist[$key]['Preview']."
	");
	
	if ($themelist[$key]['Name'] == $current_theme) {
		echo("<p><font size=\"3\" color=\"blue\"><b>This Theme Is Currently Active</b></font></p>");
	} else {
		echo("<p><font size=\"3\" color=\"blue\"><b><a href=\"admin.php?f=iconstheme&settheme=".urlencode($themelist[$key]['Name'])."\">Choose This Surfbar Icon Theme</a></b></font></p>");
		if ($displayname != "LFMTE_original") {
			echo("<p><a onclick=\"return confirm('Are you sure you want to delete these surfbar icons?')\" href=\"admin.php?f=iconstheme&deltheme=".urlencode($displayname)."\"><font size=\"3\" color=\"red\"><b>Delete This Surfbar Icon Theme</b></font></a></p>");
		}
	}
	
	echo("
	<hr>
	");
}

exit;

?>