<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/lfmsql.php";
require_once "../inc/lfmvers.php";

if($_GET["s"] != "noauth")
{
	include "../inc/checkauth.php";
}
else
{
	include "../inc/config.php";
	lfmsql_connect($dbhost,$dbuser,$dbpass);
	lfmsql_select_db($dbname) or die( "Unable to select database");
}

$res=@lfmsql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
$row=@lfmsql_fetch_array($res);
$sitename=$row["sitename"];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>Admin Login</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<center>

<table width="400" height="475" background="/images/admin_bg.png">
	
	<tr height="50"><td align="center" valign="middle">&nbsp;</td></tr>
	
	<tr height="70"><td align="center" valign="middle"><div style="font-family:Arial; font-size:36px; color:#FFFFFF;">Admin Login</div></td></tr>
	
	<tr height="260"><td align="center" valign="top">
		
		<table border="0" cellpadding="0" cellspacing="15" width="275">
			<form method="post" action="admin.php">
			<tr><td align="center"><input style="width:273px;" type="text" class="form-control" name="login" id="login" placeholder="Username"></td></tr>
			<tr><td align="center"><input style="width:273px;" type="password" class="form-control" name="password" id="password" placeholder="Password"></td></tr>
			<tr><td align="center"><input style="width:275px; height:40px; border-radius:4px;" type="submit" name="Submit" value="Login" /></td></tr>
			</form>
			</table>
			<br>
		
	</td></tr>
	
	<tr height="40"><td align="center" valign="top"><div style="font-family:Arial; font-size:18px; color:#FFFFFF;"><? echo(get_script_type());?> v<? echo(get_script_vers());?></div></td></tr>
	
	<tr height="55"><td align="center" valign="top">&nbsp;</td></tr>


</table>

</center>
</body>
</html>