<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.10
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

session_start();

require_once "../inc/filter.php";
include_once "../inc/sql_funcs.php";

	// Get root path to OTO installation
	if(isset($_SERVER['PATH_TRANSLATED']))
	{
		$installerpath=$_SERVER['PATH_TRANSLATED'];
	}
	else
	{
		$installerpath=$_SERVER['SCRIPT_FILENAME'];
	}
	$rootpath=substr($installerpath,0,strlen($installerpath)-17);
	$configfile=$rootpath."inc/config.php";
	if(isset($_SERVER['SCRIPT_URL']))
	{
		$installurl=$_SERVER['SCRIPT_URL'];
	}
	else
	{
		$installurl=$_SERVER['SCRIPT_NAME'];
	}

	$adminpath="http://".$_SERVER['HTTP_HOST'].substr($installurl,0,strrpos($installurl, "/"))."/";


	if($_POST["Submit"] == "Retry")
	{
		$step=$_GET["s"];
	}
	else
	if($_POST["Submit"] == "<<Back")
	{
		$step=$_GET["s"]-2;
		echo $step;
	}

	if($_POST["Submit"] == "Next>>")
	{
		$step=$_GET["s"]+1;
	}
	else
	if($_POST["Submit"] != "<<Back")
	{
		$step=$_GET["s"];
	}

	$errmsg="";
	$newver="1.11";
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>LFMTE Installation</title>
<style>
div#navbar {font-family: Verdana, Helvetica, Arial, sans-serif; font-size: .9em; font-weight: bold; text-align: center; background-color: #bbccbb; border:

ridge #2b4460 thin; font-weight: bold; padding: 3px; width: 300px;} #navbar ul li a:link, #navbar ul li a:visited {color: #3d4a5e;
background-color:

#abcbb8; text-decoration: none;
display: inline;} #navbar ul li a:hover, #navbar ul li a.current {color: #3d4a5e;
background-color: #abcbb8;

text-decoration: none;}

#navbar ul li {margin: 0px; padding: 0px; list-style-type: none; display: inline;} #navbar ul {margin: 0px; padding: 0px;}
</style>
</head>
<body>
<form method="post" action="install.php?s=<?=$step+1;?>">
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30" align="center" bgcolor="#1972CE"><font color="#FFFFFF" size="4" face="Verdana, Arial, Helvetica, sans-serif"><strong><img src="LFM_header.jpg" width="760" height="134" /><br>LFMTE Installer </strong></font></td>
    <td rowspan="2" align="center" bgcolor="#FFFFFF"><img src="../images/vert.gif" width="1" height="400" /></td>
  </tr>
<?
if(!isset($_GET["s"]))
{

// Check if ionCube loaders are installed
if(!extension_loaded('ionCube Loader')){ $__oc=strtolower(substr(php_uname(),0,3));$__ln='ioncube_loader_'.$__oc.'_'.substr(phpversion(),0,3).(($__oc=='win')?'.dll':'.so');@dl($__ln);if(!function_exists('_il_exec')){$__ln='/ioncube/'.$__ln;$__oid=$__id=realpath(ini_get('extension_dir'));$__here=dirname(__FILE__);if(strlen($__id)>1&&$__id[1]==':'){$__id=str_replace('\\','/',substr($__id,2));$__here=str_replace('\\','/',substr($__here,2));}$__rd=str_repeat('/..',substr_count($__id,'/')).$__here.'/';$__i=strlen($__rd);while($__i--){if($__rd[$__i]=='/'){$__lp=substr($__rd,0,$__i).$__ln;if(file_exists($__oid.$__lp)){$__ln=$__lp;break;}}}@dl($__ln);if(!function_exists('_il_exec')){
		echo("
		<p><b>The ionCube Loaders were not found on this server.</b></p>
		<p>The loaders are required for certain licensing components of the LFMTE.  The server administrator or hosting company can install them through WHM, or by downloading them at: http://www.ioncube.com/loaders.php</p>
		<p>After the loaders are installed, you can continue with the LFMTE installation by going to: </p>http://".$_SERVER["SERVER_NAME"]."/admin/install.php
		  <p>&nbsp;</p>
		  <table width=\"760\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#1972CE\">
		    <tr>
		      <td align=\"center\"><p>&nbsp;</p></td>
		    </tr>
		  </table>
		  </body>
		  </html>
		");
		exit();
} } }
// End check if ionCube loaders are installed

?>
  <tr>
    <td align="center" valign="top"><table border="0" align="center">
      <tr>
        <td align="center" nowrap="nowrap"><p>&nbsp;</p>
          <p><strong><font size="4" face="Verdana, Arial, Helvetica, sans-serif">LFMTE License Verification</font></strong> </p></td>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      
      <?
      
      if (!file_exists("setuplic.php")) {
      		echo("<p><b>File setuplic.php is missing</b></p>
      		  <p>&nbsp;</p>
		  <table width=\"760\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#1972CE\">
		    <tr>
		      <td align=\"center\"><p>&nbsp;</p></td>
		    </tr>
		  </table>
		  </body>
		  </html>");
      		exit;
      }
      
      	include("setuplic.php");

     ?>
      
      <tr>
        <td align="center">&nbsp;</td>
      </tr>

    </table>	</td>
    <td rowspan="2" align="center" bgcolor="#FFFFFF"><img src="../images/vert.gif" width="1" /></td>
  </tr>
<?
//////////////////////////////////////////
//
// NEW INSTALLATION
//
//////////////////////////////////////////
}
if($step == 1) { // STEP 1

// Check for an existing config.php
@include "../inc/config.php";
?>
  <tr>
    <td><p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
      Before continuing, you will need to have a MySQL database created. Once you have created your database in cPanel, please fill in the following details and click on the 'Next' button.</font></p>
      <br />
      <table width="340" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr>
        <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MySQL Hostname: </font></strong></td>
        <td><input name="hostname" type="text" id="hostname" value="<?=$dbhost;?>" /></td>
      </tr>
      <tr>
        <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MySQL Database Name: </font></strong></td>
        <td><input name="db" type="text" id="db" value="<?=$dbname;?>" /></td>
      </tr>
      <tr>
        <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MySQL Username: </font></strong></td>
        <td><input name="username" type="text" id="username" value="<?=$dbuser;?>" /></td>
      </tr>
      <tr>
        <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MySQL Password: </font></strong></td>
        <td><input name="password" type="text" id="password" value="<?=$dbpass;?>" /></td>
      </tr>
        <input name="dbprefix" type="hidden" id="dbprefix" value="oto_" />
    </table>
    <br />
    <br />
    <p>&nbsp; </p></td>
  </tr>
<? } // END STEP 1
	if($step == 2) // STEP 2
	{
?>
  <tr>
    <td><p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
      The installer will now check the connection to your database and make sure that your server has the correct applications and extensions installed.     </font></p>
	<input name="hostname" type="hidden" id="hostname" value="<?=$_POST["hostname"];?>">
	<input name="db" type="hidden" id="db" value="<?=$_POST["db"];?>">
	<input name="username" type="hidden" id="username" value="<?=$_POST["username"];?>">
	<input name="password" type="hidden" id="password" value="<?=$_POST["password"];?>">
	<input name="dbprefix" type="hidden" id="dbprefix" value="<?=$_POST["dbprefix"];?>">
      <table width="340" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr>
        <td width="90%" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Checking MySQL Connection </font></td>
<? // Check MySQL
		// Connect to mysql server
		$testlink=@mysql_connect($_POST["hostname"],$_POST["username"],$_POST["password"]);
		if(!$testlink)
		{
			$mysqlerr = 1;
			$errmsg.="Unable to connect to MySQL server<br>";
		}

		// select database
		if(!@mysql_select_db($_POST["db"]))
		{
			$dberr=1;
			$errmsg.="Unable to select database '".$_POST["db"]."'<br>";
		}

		if($mysqlerr == 1 || $dberr == 1)
		{ ?>
        <td><div align="center"><img src="../images/cancel.jpg" width="15" height="15" /></div></td>
<?		}
		else
		{
?>
        <td><div align="center"><img src="../images/tick.jpg" width="15" height="15" /></div></td>
<?
		}
?>
      </tr>
      <tr>
        <td nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Checking PHP Version </font></td>
        <td><div align="center"><img src="../images/tick.jpg" width="15" height="15" /></div></td>
      </tr>

      <tr>
        <td nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Checking  permissions </font></td>
<?
		// Check permissions on config.php
		$afferr=0;
		$testfile=$rootpath."inc/"."config.php";
		$fp=@fopen($testfile,"w+");
		if(!$fp)
		{
			$afferr = 1;
			$errmsg.="Unable to write to $testfile - check file permissions<br>";
		}
		else
		{
			fclose($fp);
		}

		if($afferr == 1)
		{
?>
        <td><div align="center"><img src="../images/cancel.jpg" width="15" height="15" /></div></td>
<?
} else {
?>
        <td><div align="center"><img src="../images/tick.jpg" width="15" height="15" /></div></td>
<?
}
?>
      </tr>
      <tr>
        <td nowrap="nowrap">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
      <p>&nbsp; </p></td>
  </tr>

<?
    if($afferr == 1)
    {
?>
          <tr>
            <td colspan="2" align="center">
            <table align="center" style="border: thin solid;"><tr><td>
            <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
            <strong><font color="red">The installer is unable to alter your database settings.</font><br>If you have set the permissions to 777 on inc/config.php you will need to edit this file before continuing. If you are unsure how to do this please open a support ticket at our <a href="http://www.akhmediagroup.com/support/">Helpdesk</a> and include the ftp details for your site and your mysql database settings.</strong><br />
            <font color="red"><?=$errmsg;?></font>            </font>
            </td></tr></table>            </td>
          </tr>
<?
    }

	if($conferr == 1 || $mysqlerr ==1 || $dberr == 1)
	{
?>
  		<tr>
    		<td colspan="2" align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Please fix the following errors and click on 'Retry' to try again or 'Back' to check your settings. </strong><br />
   		    <font color="red"><?=$errmsg;?></font>
    		</font></td>
  		</tr>

  		<tr>
    		<td colspan="2" align="right">
			<input name="Submit" type="submit" id="Submit" value="&lt;&lt;Back">
   		    <input type="submit" name="Submit" value="Retry" />			</td>
  		</tr>
  </table>
<?
		exit;
	}

} // END STEP 2
if($step == 3){
?>
  <tr>
    <td><p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
      Please enter a username and password for the Admin user. When you click on the 'Next' button the necessary database tables will be created and the initial configuration will be complete. This may take 20-30 seconds.</font></p>
      <table width="340" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr>
        <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Username : </font></strong></td>
        <td><input name="adminuser" type="text" id="adminuser" /></td>
      </tr>
      <tr>
        <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Password : </font></strong></td>
        <td><input name="adminpass" type="text" id="adminpass" /></td>
      </tr>
    </table>
		<input name="hostname" type="hidden" id="hostname" value="<?=$_POST["hostname"];?>">
		<input name="db" type="hidden" id="db" value="<?=$_POST["db"];?>">
		<input name="username" type="hidden" id="username" value="<?=$_POST["username"];?>">
		<input name="password" type="hidden" id="password" value="<?=$_POST["password"];?>">
		<input name="dbprefix" type="hidden" id="dbprefix" value="<?=$_POST["dbprefix"];?>">

    <p>&nbsp; </p></td>
  </tr>
<?
} // End step 3
if($step==4){

	$fullpath = $installerpath;
	$mail_path=substr($fullpath,0,strrpos($fullpath, "/"));
	$maint=$mail_path."/maint.php";
	// Path to PHP
	$php_path = @exec( 'which php');
	
	if ($php_path == "") {
		$php_path = "/usr/bin/php";
	}
	
	$maintcmd = $php_path." -f ".$maint;
	$sgf4cron = $php_path." -f ".$_SERVER[DOCUMENT_ROOT]."/f4cron.php";
	$sgmban = $php_path." -f ".$_SERVER[DOCUMENT_ROOT]."/mbancron.php";
	$cryptcron = $php_path." -f ".$_SERVER[DOCUMENT_ROOT]."/cryptcron.php";
?>
  <tr>
    <td align="left"><p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br />
      The final task is to add the following cron job entries in your cPanel account.  These will update your stats, downgrade expired upgrades, and check for harmful sites.<br />
      <br />
      Every night at 12AM: <b><?=$maintcmd;?></b><br /><br />
      Run every 10 Minutes: <b><?=$sgf4cron;?></b><br /><br />
      Run every 10 Minutes: <b><?=$sgmban;?></b><br /><br />
      Run every 60 Minutes: <b><?=$cryptcron;?></b><br />
      <br />
      <?
		$dbhost=$_POST["hostname"];
		$db=$_POST["db"];
		$user=$_POST["username"];
		$pass=$_POST["password"];
		$prefix=$_POST["dbprefix"];
        $adminuser=$_POST["adminuser"];
        $adminpass=$_POST["adminpass"];

		// Connect to database
		mysql_connect($dbhost,$user,$pass);
		mysql_select_db($db);

		require_once("sqlupdate.php");

		// Add database details to config.php
		$fpc=@fopen($configfile,"w+");
		if(!$fpc)
		{
			echo "<br>ERROR: UNABLE TO WRITE TO CONFIG.PHP!<br>";
		}
		else
		{
			$strstart="<?\r\n// Database config\r\n\r\n";
			$strdbhost="\$dbhost=\"".$dbhost."\";\r\n";
			$strdbname="\$dbname=\"".$db."\";\r\n";
			$strdbuser="\$dbuser=\"".$user."\";\r\n";
			$strdbpass="\$dbpass=\"".$pass."\";\r\n";
			$strprefix="\$prefix=\"".$prefix."\";\r\n";
			$strsp="\$realscriptpath=\"\";\r\n?>";

			@fputs($fpc,$strstart,strlen($strstart));
			@fputs($fpc,$strdbhost,strlen($strdbhost));
			@fputs($fpc,$strdbname,strlen($strdbname));
			@fputs($fpc,$strdbuser,strlen($strdbuser));
			@fputs($fpc,$strdbpass,strlen($strdbpass));
			@fputs($fpc,$strprefix,strlen($strprefix));
			@fputs($fpc,$strsp,strlen($strsp));
			@fclose($fpc);
		}

			$encpw=0;
			$adm=@mysql_query("SELECT * FROM ".$prefix."admin");
			if(mysql_num_rows($adm) > 0)
			{
				$admvals=@mysql_fetch_object($adm);
				$encpw=$admvals->encpw;
			}

            $adm=@mysql_query("DELETE FROM ".$prefix."admin");
            
            if (isset($_SESSION['newkey']) && ($_SESSION['newkey'] != "")) {
            	$sesskey = $_SESSION['newkey'];
            } else {
            	// Invalid Key
            	$sesskey = "nosession";
            }
            
            
	        $adm=@mysql_query("INSERT INTO ".$prefix."admin(admin_name,login,password,email,encpw,license) VALUES('".$admvals->admin_name."','".$adminuser."','".md5($adminpass)."','".$admvals->email."','$encpw','".$sesskey."')");

?>
     If there are no errors, installation is complete and you may now log in to your admin area at:</font></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="<?=$adminpath;?>"><?=$adminpath;?></a></font></p>
      <p align="center">&nbsp;</p>
      <p align="center">&nbsp;</p>
      <p align="center"><font color="#FF0000" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong> PLEASE DELETE admin/install.php </strong></font></p>
      <p>&nbsp; </p></td>
  </tr>
  </table>
</form>
<?
exit;
}
if(isset($step))
{
?>
  <tr>
    <td colspan="2" align="right"><input type="submit" name="Submit" value="Next &gt;&gt;" /></td>
  </tr>
  <p>
  <?
}
?>
  </table>
  </form>
  </p>
  <p>&nbsp;  </p>
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#1972CE">
  <tr>
    <td align="center"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>LFMTE Copyright &copy;2006-2011 AKH Media Group and Josh Abbott. All Rights Reserved. </strong></font></td>
  </tr>
</table>
</body>
</html>