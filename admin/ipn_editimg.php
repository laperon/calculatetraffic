<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.14
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

echo("<html>
<body>
<center>
");

####################

//Begin main page

####################

if (!isset($_GET['merch']) || !is_numeric($_GET['merch']) || !isset($_GET['id']) || !is_numeric($_GET['id'])) {
	echo ("Invalid GET vars");
	exit;
}

if ($_GET['resetdefault'] == "yes") {
	mysql_query("UPDATE ".$prefix."ipn_prod_img SET merchant".$_GET['merch']."button='' WHERE productid=".$_GET['id']) or die(mysql_error());
}

if ($_GET['update'] == "yes") {
	$bsize=0;
	$banner='';
        $banner=$_FILES['imagepath']['tmp_name'];
        $bsize = filesize($_FILES['imagepath']['tmp_name']);
		
	// Check to make sure we have an image file
	if(!getimagesize($banner)) {
		echo "Image format not recognized.";
		exit;
	}
    	
	$fp=fopen($banner, "r");
    	$mysqlBanner = fread($fp, filesize($_FILES['imagepath']['tmp_name']));
	fclose($fp);
	$encoded = chunk_split(base64_encode($mysqlBanner)); 
	
	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_prod_img WHERE productid=".$_GET['id']), 0);
	if ($checkexists > 0) {
	    	mysql_query("UPDATE ".$prefix."ipn_prod_img SET merchant".$_GET['merch']."button='".$encoded."' WHERE productid=".$_GET['id']) or die(mysql_error());
	} else {
		mysql_query("INSERT INTO ".$prefix."ipn_prod_img (productid, merchant".$_GET['merch']."button) VALUES (".$_GET['id'].", '".$encoded."')") or die(mysql_error());
	}

}

?>

	<h4><b>Change Payment Button</b></h4><br>
	
	<?php
	
	echo("<form action=\"/admin/ipn_editimg.php?merch=".$_GET['merch']."&id=".$_GET['id']."&resetdefault=yes\" method=\"POST\">
	<input type=\"submit\" value=\"Reset To Default\">
	</form>
	<br>
	<form action=\"/admin/ipn_editimg.php?merch=".$_GET['merch']."&id=".$_GET['id']."&update=yes\" method=\"POST\" enctype=\"multipart/form-data\">
	<input type=\"file\" name=\"imagepath\" /><br>
	<input type=\"submit\" value=\"Update\">
	</form>
	<br><br>
	<img border=\"0\" src=\"/ipnimg.php?rnum=".rand(0,999999)."&merch=".$_GET['merch']."&id=".$_GET['id']."\">
	");
	
echo("
<br><br>

</center>
</body>
</html>");

exit;

?>