<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

echo("<html>
<body>
<center>
");

####################

//Begin main page

####################

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	echo ("Invalid GET vars");
	exit;
}

if ($_GET['update'] == "yes") {
	
	if (!isset($_POST["downloadids"]) || !is_array($_POST["downloadids"])) {
		$downloadids = "";
	} else {
		$downloadids = implode(",", $_POST["downloadids"]);
	}
	
	lfmsql_query("UPDATE ".$prefix."ipn_products SET productid='".$downloadids."' WHERE id=".$_GET['id']." LIMIT 1") or die(lfmsql_error());
	
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	
	echo "<h4><b>Updated Successfully</b></h4><br>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;
}

$getproducts = lfmsql_query("SELECT productid FROM ".$prefix."ipn_products WHERE id=".$_GET['id']." LIMIT 1") or die(lfmsql_error());
if (lfmsql_num_rows($getproducts) < 1) {
	echo ("Sales Package Not Found");
	exit;
}

$productlist = lfmsql_result($getproducts, 0, "productid");

?>

	<h4><b>Sales Package Downloads</b></h4><br>
	<p align="left">Here you can add Downloads that members will receive when they purchase this Sales Package.</p>
	</center>
	<?php
	
	echo("<form action=\"/admin/ipn_setdownloads.php?id=".$_GET['id']."&update=yes\" method=\"POST\">
	");
	
	$getpagelist = lfmsql_query("SELECT * FROM ".$prefix."products ORDER BY productid") or die(lfmsql_error());
	while($menu=@lfmsql_fetch_object($getpagelist)) {
		echo("<input type=\"checkbox\" name=\"downloadids[]\" value=\"".$menu->productid."\""); if (array_search($menu->productid, explode(",", $productlist)) !==false) { echo(" checked=\"checked\""); } echo("><b>".$menu->productname."</b><br>");
	}
	
	echo("<br>
	<input type=\"submit\" value=\"Update Pages\">
	</form>
	");

echo("
<br><br>

</body>
</html>");

exit;

?>