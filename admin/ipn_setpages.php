<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

echo("<html>
<body>
<center>
");

####################

//Begin main page

####################

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	echo ("Invalid GET vars");
	exit;
}

if ($_GET['update'] == "yes") {
	
	$getpagelist = lfmsql_query("SELECT * FROM ".$prefix."memberpages ORDER BY pageindex") or die(lfmsql_error());
	while($menu=@lfmsql_fetch_object($getpagelist)) {
		if (isset($_POST['includepage'.$menu->pageid])) {
			lfmsql_query("INSERT INTO `".$prefix."ipn_pages` (productid, pageid) VALUES (".$_GET['id'].", ".$menu->pageid.")") or die(lfmsql_error());
		} else {
			lfmsql_query("DELETE FROM `".$prefix."ipn_pages` WHERE productid=".$_GET['id']." AND pageid=".$menu->pageid) or die(lfmsql_error());
		}
	}
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

?>

	<h4><b>Product Content Pages</b></h4><br>
	<p align="left">You can create content pages under the Site Design menu, and then check the boxes below to give members who purchase this item access to the checked pages.</p>
	</center>
	<?php
	
	echo("<form action=\"/admin/ipn_setpages.php?id=".$_GET['id']."&update=yes\" method=\"POST\">
	");
	
	$getpagelist = lfmsql_query("SELECT * FROM ".$prefix."memberpages ORDER BY pageindex") or die(lfmsql_error());
	while($menu=@lfmsql_fetch_object($getpagelist)) {
		echo("<input name=\"includepage".$menu->pageid."\" type=\"checkbox\""); if (lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."ipn_pages` WHERE productid=".$_GET['id']." AND pageid=".$menu->pageid), 0) >= 1) { echo(" checked=\"checked\""); } echo("><b>".$menu->pagename."</b><br>");
	}
	
	echo("<br>
	<input type=\"submit\" value=\"Update Pages\">
	</form>
	");

echo("
<br><br>

</body>
</html>");

exit;

?>