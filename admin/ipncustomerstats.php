<?php

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// TrafficMods Ultimate IPN Customer Stats Mod.                        //
// �2008, 2015 Josh Abbott                                             //
// Not for resale.  Version included with the LFM script only.         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

if (isset($_GET['dsort']) && $_GET['dsort'] == "yes") {
	$numlimit = $_POST['numlimit'];
	$showtype = $_POST['showtype'];
	$startdate = $_POST['sdate'];
	$enddate = $_POST['edate'];
} else {
	$numlimit = 10;
	$showtype = 1;
	$currentdate = date("Y-m-d");
	$startdate = strftime("%Y-%m-%d", strtotime("$currentdate + 30 days ago"));
	$enddate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
}

$gettotalsales = lfmsql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE added >= '".$startdate." 00:00:00' AND added <= '".$enddate." 23:59:59'");
$totalsales = lfmsql_result($gettotalsales, 0);

$gettotalpurchases = lfmsql_query("SELECT id FROM ".$prefix."ipn_transactions WHERE added >= '".$startdate." 00:00:00' AND added <= '".$enddate." 23:59:59'");
$totalpurchases = lfmsql_num_rows($gettotalpurchases);

?>

<script type="text/javascript" src="datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol.css">

<br><br>
<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  <tr>
    <td align="center"><div class="lfm_infobox_heading">Customer Stats</div></td>
  </tr>
  
  <tr>
    <td align="left"><div class="lfm_descr">
    	
    	<br>These stats show who your highest-paying customers were during a specific time period.  You can see the total sales from each customer, along with the percentage of your site's total sales that came from that particular customer.  You can also see which item the customer paid the most money for during that time period.
    	
    </div></td>
  </tr>
      
</table>
</div>

<br>

<?

echo('<form action=admin.php?f=ipnsales&processor=customers&dsort=yes method="post">
From <input type name="sdate" id="DPC_sdate_YYYY-MM-DD" value="'.$startdate.'"> To <input type name="edate" id="DPC_edate_YYYY-MM-DD" value="'.$enddate.'"><br><br>
Top <input type=text name=numlimit size=2 value="'.$numlimit.'"> Order By
<select name=showtype><option value=1>Sales</option><option value=2'); if ($showtype==2){ echo(' selected'); } echo('>Units Purchased</option></select><br><br>
<input type=submit value=View></form></p>

<table border=1 cellpadding=7 cellspacing=0>
<tr bgcolor="#F8F8F8"><th>User</th><th>Sales</th><th>Units Purchased</th><th>Highest<br>Grossing<br>Item</th><th>View All Payments<br>From This Customer</th></tr>
');

if ($showtype == 2) {
	$getorders = lfmsql_query("SELECT user_id, SUM(amount) AS amountsales, COUNT(*) AS numpurchases FROM ".$prefix."ipn_transactions WHERE added >= '".$startdate." 00:00:00' AND added <= '".$enddate." 23:59:59' GROUP BY user_id ORDER BY 3 DESC");
} else {
	$getorders = lfmsql_query("SELECT user_id, SUM(amount) AS amountsales, COUNT(*) AS numpurchases FROM ".$prefix."ipn_transactions WHERE added >= '".$startdate." 00:00:00' AND added <= '".$enddate." 23:59:59' GROUP BY user_id ORDER BY 2 DESC");
}

if (lfmsql_num_rows($getorders) <= $numlimit) {
	$numlimit = lfmsql_num_rows($getorders);
}

if ($totalsales > 0 && $totalpurchases > 0) {
	
	for ($i = 0; $i < $numlimit; $i++) {
		$userid = lfmsql_result($getorders, $i, "user_id");
		$getname = lfmsql_query("Select firstname, lastname from ".$prefix."members where Id=$userid limit 1");
		if (lfmsql_num_rows($getname) > 0) {
			$firstlastname = lfmsql_result($getname, 0, "firstname")." ".lfmsql_result($getname, 0, "lastname");
		} else {
			$firstlastname = "User Not Found";
		}
		
		$gettopproduct = lfmsql_query("SELECT item_number, item_name, SUM(amount) AS sales FROM ".$prefix."ipn_transactions WHERE user_id=$userid AND added >= '".$startdate." 00:00:00' AND added <= '".$enddate." 23:59:59' GROUP BY item_number ORDER BY sales DESC");
		
		$topproduct = lfmsql_result($gettopproduct, 0, "item_name");
		$topproductsales = lfmsql_result($gettopproduct, 0, "sales");
		
		$amountsales = lfmsql_result($getorders, $i, "amountsales");
		$percentsales = ($amountsales/$totalsales)*100;
		$percentsales = round($percentsales);
		
		$numpurchases = lfmsql_result($getorders, $i, "numpurchases");
		$percentpurchases = ($numpurchases/$totalpurchases)*100;
		$percentpurchases = round($percentpurchases);
		
		echo('<tr>
		<td><font size=2><b>User '.$userid.'</b></font><br><font size=2>'.$firstlastname.'</font></td>
		<td><font size=3><b>$'.$amountsales.'</b></font><br><font size=2>'.$percentsales.'%</font></td>
		<td><font size=3><b>'.$numpurchases.'</b></font><br><font size=2>'.$percentpurchases.'%</font></td>
		<td><font size=2>'.$topproduct.'<br>$'.$topproductsales.'</font></td>
		<td align=center><a href=admin.php?f=ipnsales&showcustomer=yes&q='.$userid.'>View</a></td>
		</tr>');
		
	}

} else {
	echo ("<tr><td colspan=\"5\" align=\"center\"><p><font size=\"2\">No Sales Found</font></p></td></tr>");
}

?>