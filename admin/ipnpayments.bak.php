<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

extract($_GET);
extract($_POST);

echo("<center><br><br>");

//LFM Refund

if ($_GET['refundid'] > 0) {

$getrefundtxn = mysql_query("Select txn_id, user_id from ".$prefix."ipn_transactions where id=".$_GET['refundid']." limit 1");
if (mysql_num_rows($getrefundtxn) > 0) {

	if ($_GET['confirmrefund'] == "yes") {
	
	$refundtxn = mysql_result($getrefundtxn, 0, "txn_id");
	$custom = mysql_result($getrefundtxn, 0, "user_id");

// We need to remove the product from the users downloads
// and add a negative sale for the Tier 1 referrer
$revqry="SELECT * FROM ".$prefix."sales WHERE txn_id='".$refundtxn."' AND salestier=1";
$revres=@mysql_query($revqry);
$revrow=@mysql_fetch_array($revres);

// If the commission has already been paid we need a new entry
if($revrow["status"] == "P")
{
	// Already paid ... add a new entry
	$srqry="INSERT INTO ".$prefix."sales(affid,purchaserid,saledate,itemname,status,commission,txn_id,salestier) VALUES(".$revrow["affid"].",$custom,NOW(),'Refund','R',-".$revrow["commission"].",'".$refundtxn."',1)";
	@mysql_query($srqry);
}
else
{
	@mysql_query("UPDATE ".$prefix."sales SET STATUS='R', commission=0 WHERE txn_id='".$refundtxn."' AND salestier=1");
}
				
// Add a negative sale for the Tier 2 referrer
$revqry="SELECT * FROM ".$prefix."sales WHERE txn_id='".$refundtxn."' AND salestier=2";
				
if($revquery)
{
	$revres=@mysql_query($revqry);
	$revrow=@mysql_fetch_array($revres);

		// If the commission has already been paid we need a new entry
	if($revrow["status"] == "P")
	{
		// Already paid ... add a new entry
		$srqry="INSERT INTO ".$prefix."sales(affid,purchaserid,saledate,itemname,status,commission,txn_id.salestier) VALUES(".$revrow["affid"].",$custom,NOW(),'Refund','R',-".$revrow["commission"].",'".$refundtxn."',1)";
		@mysql_query($srqry);
	}
	else
	{
		@mysql_query("UPDATE ".$prefix."sales SET STATUS='R', commission=0 WHERE txn_id='".$refundtxn."' AND salestier=2");
	}
}

@mysql_query("DELETE FROM ".$prefix."purchases WHERE txn_id='".$refundtxn."'");
@mysql_query("Update ".$prefix."ipn_transactions set refunded=1 where id=".$_GET['refundid']." limit 1");

	} else {
	
		echo("<center><p><b>Process Refund</b></p>
		<p>If you have already issued a refund for this payment through a merchant service, click the link below to remove any commissions and downloads associated with the payment.  Please note that if this was an upgrade or credit purchase, you will have to manually downgrade or remove credits from the account.</p>
		<p><a href=admin.php?f=ipnsales&refundid=".$_GET['refundid']."&confirmrefund=yes>Confirm Refund</a><br><br>
		<a href=admin.php?f=ipnsales>Cancel</a></p>
		</center>");
		exit;

	}


}

}

//End LFM Refund

$action=$_REQUEST[action];
$processor=$_GET[processor];
if(!$processor&&$action<>"edit") { $processor="all"; }
echo '| ';
if($processor=="all") {
	$summary="All processors";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&offset='.$offset.'">All processors</a>';
	}
echo ' | ';
if($processor=="paypal") {
	$summary="PayPal";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&offset='.$offset.'&processor=paypal">PayPal</a>';
	}
echo ' | ';
if($processor=="safepay") {
	$summary="SafePay Solutions";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&offset='.$offset.'&processor=safepay">SafePay</a>';
	}
echo ' | ';
if($processor=="payza") {
	$summary="Payza";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&offset='.$offset.'&processor=payza">Payza</a>';
	}
echo ' | ';
if($processor=="2checkout") {
	$summary="2CheckOut";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&offset='.$offset.'&processor=2checkout">2CheckOut</a>';
	}
	echo ' | ';
if($processor=="commissions") {
	$summary="Paid With Commissions";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&offset='.$offset.'&processor=commissions">Paid With Comms</a>';
	}
	echo ' | ';
if($processor=="products") {
	$summary="Products";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&processor=products">Products</a>';
	}
echo ' | ';
if($processor=="customers") {
	$summary="Customers";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&processor=customers">Customers</a>';
	}
echo ' | ';
if($processor=="summary") {
	$summary="Summary";
	echo "<b>$summary</b>";
	} else {
	echo '<a href="admin.php?f=ipnsales&processor=summary">Summary</a>';
	}
echo ' | ';
if($action=="edit"&&!$_POST[submit]) {
	echo '<b>Add a payment</b> |';
	} else {
	echo '<a href="admin.php?f=ipnsales&action=edit">Add a payment</a> |';
	}

if($processor=="summary") {
	include("ipnsalessummary.php");
	exit;
	}

if($processor=="products") {
	include("ipnproductsales.php");
	exit;
	}

if($processor=="customers") {
	include("ipncustomerstats.php");
	exit;
	}

if($action=="process") {
	$result=mysql_query("SELECT * FROM ".$prefix."ipn_transactions WHERE id='$id' LIMIT 1;");
	$data=mysql_fetch_array($result);
	//FIND ITEM IN DB
	$cnx=mysql_query("SELECT * FROM ".$prefix."ipn_products WHERE id=$data[item_number] AND amount=$data[amount];");
	if ($row=mysql_fetch_assoc($cnx)) {
		//UPDATE USER
		$usr_cnx=@mysql_query("SELECT username, email, upgend, refid FROM ".$prefix."members WHERE Id=$data[user_id];");
		$usr=mysql_fetch_assoc($usr_cnx);
		
		if ($row['credits']>0) {
		mysql_query("UPDATE ".$prefix."members SET credits=credits+$row[credits] WHERE Id=$data[user_id] LIMIT 1");
		echo "<br>".$row["credits"]." credits added<br>";
		}
		
		if ($row['bimps']>0) {
		mysql_query("UPDATE ".$prefix."members SET bannerimps=bannerimps+$row[bimps] WHERE Id=$data[user_id] LIMIT 1");
		echo "<br>".$row["bimps"]." banner impressions added<br>";
		}
		
		if ($row['limps']>0) {
		mysql_query("UPDATE ".$prefix."members SET textimps=textimps+$row[limps] WHERE Id=$data[user_id] LIMIT 1");
		echo "<br>".$row["limps"]." text impressions added<br>";
		}
		
		if ($row['productid']>0) {
		mysql_query("INSERT INTO ".$prefix."purchases(affid,itemid,txn_id) VALUES(".$data["user_id"].",'".$data["item_number"]."','".$data["txn_id"]."')");
		echo "<br>Product ".$data["item_number"]." added to users account<br>";
		}
		
			echo "<p>Processing $data[processor] transaction:
<br>User ID $data[user_id] - $usr[username]
<br>Item # $data[item_number] - $data[item_name]";
		if ($row[upgrade]) {
			$today=date("Y-m-d");
			$exp=$usr['upgend'];
			if ($exp<$today) {
				$exp=strftime("%Y-%m-%d",strtotime("$today + 1 day"));
				}	
			$exp=strftime("%Y-%m-%d",strtotime("$exp + $row[period] months"));
			$upg_query = 'update '.$prefix.'members set mtype='.$row['upgrade'].', upgend="'.$exp.'" where Id='.$data['user_id'].' limit 1';
			mysql_query($upg_query);
			echo "<br>Account upgrade level ".$row['upgrade']." expires ".$exp."<br>";
			}
			
			
			// commissions
			if ($usr['refid'] > 0) {
			
			$affid = $usr['refid'];

				//Process LFM Commission
				    $affqry="SELECT username, email, mtype FROM ".$prefix."members WHERE Id=$affid";
				    $affres=@mysql_query($affqry);
				    $affrow=@mysql_fetch_array($affres);
				
				if ($row['subscription'] == 2) {
				// commission level on OTO
				    $commqry="SELECT otocomm FROM ".$prefix."membertypes WHERE mtid=".$affrow["mtype"];
				    $commres=@mysql_query($commqry);
				    $commrow=@mysql_fetch_array($commres);
				    $comper = $commrow["otocomm"];
				} else {
				// commission level on purchases
				    $commqry="SELECT comm FROM ".$prefix."membertypes WHERE mtid=".$affrow["mtype"];
				    $commres=@mysql_query($commqry);
				    $commrow=@mysql_fetch_array($commres);
				    $comper = $commrow["comm"];
				}
				    
				    if($comper > 0) {
				    $affcom=round((($comper/100)*$data[amount]),2);

				    // Sales table keeps affiliate commission stats
				    $saleqry="INSERT INTO ".$prefix."sales (affid, purchaserid, saledate, itemid, itemname, itemamount, commission, txn_id, salestier) VALUES ($affid, ".$data['user_id'].", NOW(), '".$data['item_number']."', '".$data['item_name']."', ".$data['amount'].", $affcom, '".$data['txn_id']."', 1)";
				    mysql_query($saleqry);
				    echo "User ".$affid." paid ".$affcom." commissions on level 1<br>";
				    }

				    // Check for tier 2 commission
					// Get referrer so we can insert this sale into the database
				    $ul2qry="SELECT refid FROM ".$prefix."members WHERE Id=$affid";
				    $ul2res=@mysql_query($ul2qry);

				    if($ul2res)
				    {
					    $ul2row=@mysql_fetch_array($ul2res);
					    $aff2id=$ul2row["refid"];

						// Find out the affiliate's member type for commission
						$aff2qry="SELECT mtype FROM ".$prefix."members WHERE Id=$aff2id";
						$aff2res=@mysql_query($aff2qry);
						$aff2row=@mysql_fetch_array($aff2res);
					
						if ($row['subscription'] == 2) {
						// commission level on OTO
							$comm2qry="SELECT otocomm2 FROM ".$prefix."membertypes WHERE mtid=".$aff2row["mtype"];
							$comm2res=@mysql_query($comm2qry);
							$comm2row=@mysql_fetch_array($comm2res);
							$comper2 = $comm2row["otocomm2"];
						} else {
						// commission level on purchases
							$comm2qry="SELECT comm2 FROM ".$prefix."membertypes WHERE mtid=".$aff2row["mtype"];
							$comm2res=@mysql_query($comm2qry);
							$comm2row=@mysql_fetch_array($comm2res);
							$comper2 = $comm2row["comm2"];
						}
						
						if($comper2 > 0)
						{
							$aff2com=round((($comper2/100)*$data[amount]),2);
	
							// Sales table keeps affiliate commission stats
							$sale2qry="INSERT INTO ".$prefix." sales(affid, purchaserid, saledate, itemid, itemname, itemamount, commission, txn_id, salestier) VALUES ($aff2id, ".$data['user_id'].", NOW(), '".$data['item_number']."', '".$data['item_name']."', ".$data['amount'].", $aff2com, '".$data['txn_id']."', 2)";
							mysql_query($sale2qry);
							echo "User ".$aff2id." paid ".$aff2com." commissions on level 2<br>";
						}
					
					}

				}
				//End commissions
		}
	$action="edit";
	}

if($action=="edit") {
   $processor=$_GET[processor];
   $id=$_GET[id];
   if($id) {
	$submit="Edit";
	$r=mysql_query("SELECT * FROM ".$prefix."ipn_transactions WHERE id='$id';");
	$row=mysql_fetch_array($r);
	extract($row);
	} else {
	$submit="Add";
	$date="";
	$user_id=0;
	}

   if($_POST[submit]=="Add") {
	extract($_POST);
	if($processor=="2checkout") {
		// is it a recurring billing sale?
		$txn_id=str_replace("#","",$txn_id);
		$txn_id=trim($txn_id);
		$split=explode("/",$txn_id);
		$txn_id=$split[0];
		$r=mysql_query("SELECT * FROM ".$prefix."ipn_transactions WHERE txn_id='$txn_id';");
		if(mysql_num_rows($r)) {
			$custom=$date;
			$row=mysql_fetch_array($r);
			extract($row);
			$order_number=$txn_id.'/'.date("Ym");
			$r=mysql_query("SELECT payee,verify FROM ".$prefix."ipn_merchants WHERE name='2CheckOut';");
			$sid=@mysql_result($r,0,"payee");
			$hash=@mysql_result($r,0,"verify").$sid.$order_number.$amount;
			$key=strtoupper(md5($hash));
			$r=mysql_query("SELECT * FROM ".$prefix."ipn_transactions WHERE txn_id='$order_number';");
			if(!mysql_num_rows($r)) {
				echo '<p>
<h2>Your 2CO Recurring Sale #'.$txn_id.'</h2>
<b>Name:</b> '.$name.'
<br><b>Amount:</b> '.$amount.'
<br><b>User ID:</b> '.$user_id.'
<br><b>Item:</b> '.$item_name.'
<p>
<FORM action="../ipn.php" method="POST" target="2co">
<INPUT type="hidden" name="sid" value="'.$sid.'">
<INPUT type="hidden" name="key" value="'.$key.'">
<INPUT type="hidden" name="date" value="'.$custom.'">
<INPUT type="hidden" name="email" value="'.$email.'">
<INPUT type="hidden" name="product_id" value="'.$item_number.'">
<INPUT type="hidden" name="order_number" value="'.$order_number.'">
<INPUT type="hidden" name="pay_method" value="'.$txn_type.'">
<INPUT type="hidden" name="quantity" value="1">
<INPUT type="hidden" name="total" value="'.$amount.'">
<INPUT type="hidden" name="credit_card_processed" value="Y">
<INPUT type="hidden" name="user_id" value="'.$user_id.'">
<INPUT type="hidden" name="card_holder_name" value="'.$name.'">
<INPUT type="submit" name="submit" value="Process this 2CheckOut transaction!">
</FORM>';
				exit;
				}
			}
		}
	$r=mysql_query("SELECT * FROM ".$prefix."ipn_transactions WHERE txn_id='$txn_id';");
	if(mysql_num_rows($r)) {
		echo "<p><em>That transaction ID already exists in the database table!</em><p>";
		} else {
		$mysql="INSERT INTO ".$prefix."ipn_transactions (processor, date, txn_id, txn_type, item_name, item_number, name, email, amount, fee, user_id, added) VALUES ('$processor', '$date', '$txn_id', '$txn_type', '$item_name', '$item_number', '$name', '$email', '$amount', '$fee', '$user_id', '$added');";
		mysql_query($mysql) or die(mysql_error());
		echo "<p><em>A new row has been added to the transactions database table!</em><p>";
		}
	}

   if($_POST[submit]=="Edit") {
	extract($_POST);
	$mysql="UPDATE ".$prefix."ipn_transactions SET processor='$processor', date='$date', txn_id='$txn_id', txn_type='$txn_type', item_name='$item_name', item_number='$item_number', name='$name', email='$email', amount='$amount', fee='$fee', user_id='$user_id', added='$added' WHERE id='$id';";
	mysql_query($mysql) or die(mysql_error());
	echo "<p><em>Row ID $id has been updated!</em><p>";
	}

	echo '	
<p><em>Use the form below to add or edit entries in the transactions database table:</em>
<p>
<FORM action="admin.php?f=ipnsales&action=edit" method="POST">
<table border=1 cellpadding=2 cellspacing=0>
<tr><td>Processor:</td><td><select name="processor">
';

	if(!$processor) { $processor="2checkout"; }
	$res=mysql_query("SELECT * FROM ".$prefix."ipn_merchants ORDER BY id;");
	while($row=mysql_fetch_array($res)) {
		$p=strtolower($row[name]);
		echo '<option value="'.$p.'"';
		if($processor==$p) { echo ' SELECTED'; }
		echo '>'.$row[name].'</option>
';
		}
	echo '</select></td></tr>';
	if($submit=="Edit") { echo '
<tr><td>Row ID:</td><td><INPUT type="text" name="id" value="'.$id.'"></td></tr>'; }
	echo '
<tr><td>Date:</td><td><INPUT type="text" name="date" value="'.$date.'"></td></tr>
<tr><td>Txn ID:</td><td><INPUT type="text" name="txn_id" value="'.$txn_id.'"></td></tr>
<tr><td>Txn Type:</td><td><INPUT type="text" name="txn_type" value="'.$txn_type.'"></td></tr>
<tr><td>Item Name:</td><td><INPUT type="text" name="item_name" value="'.htmlentities(stripslashes($item_name)).'"></td></tr>
<tr><td>Item Number:</td><td><INPUT type="text" name="item_number" value="'.$item_number.'"></td></tr>
<tr><td>Name:</td><td><INPUT type="text" name="name" value="'.$name.'"></td></tr>
<tr><td>Email:</td><td><INPUT type="text" name="email" value="'.$email.'"></td></tr>
<tr><td>Amount:</td><td><INPUT type="text" name="amount" value="'.$amount.'"></td></tr>
<tr><td>Fee:</td><td><INPUT type="text" name="fee" value="'.$fee.'"></td></tr>
<tr><td>User ID:</td><td><INPUT type="text" name="user_id" value="'.$user_id.'"></td></tr>';
	if($submit=="Edit") { echo '
<tr><td>Added:</td><td><INPUT type="text" name="added" value="'.$added.'"></td></tr>'; }
	echo '
<tr><td align="center" colspan=2><INPUT type="submit" name="submit" value="'.$submit.'"></td></tr>
</table>
</FORM>
<br><br>';
	if($submit=="Edit") {
		echo 'Not processed?<br><a href="admin.php?f=ipnsales&action=process&processor='.$processor.'&id='.$id.'">Process this payment now!</a><br><br>';
		}
	exit;
	}

$limit=20; 
$Prev = "Previous"; 
$Next = "Next";
$number=0;
$offset=$_GET[offset];
if (empty($offset)) { $offset=0; } 
$field=$_GET[field];
if(!$field) { $field="id"; }
$ob=$_GET[ob];
if(!$ob) { $ob="DESC"; }
if($ob=="ASC") { $oa="DESC"; }
if($ob=="DESC") { $oa="ASC"; }
$q=trim($_REQUEST[q]);

echo '<p>
<FORM action="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field='.$field.'&ob='.$ob.'" method="POST">
<b>Search transactions:</b> <INPUT type="text" name="q"> <INPUT type="submit" name="submit" value="GO">
</FORM>
<p>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
<tr bgcolor="#F8F8F8">
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=id&ob='.$oa.'&q='.$q.'">ID</a></th>
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=txn_id&ob='.$oa.'&q='.$q.'">Txn ID</a></th>
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=user_id&ob='.$oa.'&q='.$q.'">User ID</a></th>
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=email&ob='.$oa.'&q='.$q.'">Email</a></th>
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=item_number&ob='.$oa.'&q='.$q.'">Item Name</a></th>
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=amount&ob='.$oa.'&q='.$q.'">Amount</a></th>
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=fee&ob='.$oa.'&q='.$q.'">Fee</a></th>
<th><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor='.$processor.'&offset='.$offset.'&field=added&ob='.$oa.'&q='.$q.'">Date</a></th>
</tr>';

if($processor<>"all") {
	$where="WHERE processor='".$processor."'";
	} else {
	$where="WHERE 1";
	}
if($q>1) {
$showcustomer = $_GET['showcustomer'];
if ($showcustomer == "yes") {
$where.=" AND (user_id='$q')";
}else {
$where.=" AND (id='$q' OR txn_id LIKE '%$q%' OR item_name LIKE '%$q%' OR name LIKE '%$q%' OR email LIKE '%$q%' OR amount='$q' OR fee='$q' OR user_id='$q' OR added LIKE '$q%')";
}
	} elseif($q>"") {
	$where.=" AND (txn_id LIKE '%$q%' OR item_name LIKE '%$q%' OR name LIKE '%$q%' OR email LIKE '%$q%' OR added LIKE '$q%')";
	}

$result=mysql_query("SELECT id FROM ".$prefix."ipn_transactions $where;"); 
$number=mysql_num_rows($result);
$result=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions $where;"); 
$gross=mysql_result($result,0);
$result=mysql_query("SELECT SUM(fee) FROM ".$prefix."ipn_transactions $where;"); 
$fee=mysql_result($result,0);
$net=$gross-$fee;

echo '
<tr bgcolor="#F8F8F8">
<th colspan=8>'.$summary.' - '.$number.' IPN transactions';
if($q) { echo ' matching \''.$q.'\''; }
echo ', total value $ '.decimal($gross).' gross, $ '.decimal($net).' net</th>
</tr>';

$total=0;
$fees=0;
$mysql="SELECT * FROM ".$prefix."ipn_transactions $where ORDER BY $field $ob LIMIT $offset, $limit";
// echo "<p>$mysql<p>"; 
$result=mysql_query($mysql); 
while ($row = mysql_fetch_array($result)) {
	echo '<tr align="center"';
	if($row[fee]=="0.00") { echo ' bgcolor="#FFF0F5" title="This Payment is Pending. PayPal have not notified us of the Fee yet..."'; }
	echo '><td title="Edit this transaction"><a style="text-decoration:none;" href="admin.php?f=ipnsales&action=edit&id='.$row[id].'">'.$row[id].'</a></td>';
	echo '<td title="View this transaction in your '.$row[processor].' account. (Opens in a new window)">';
	if($row[processor]=="paypal") {
		echo '<a style="text-decoration:none;" href="https://www.paypal.com/vst/id='.$row[txn_id].'" target="_blank">'.$row[txn_id].'</a>';
		} elseif($row[processor]=="2checkout") {
		echo '<a style="text-decoration:none;" href="https://www.2checkout.com/2co/login" target="_blank">'.$row[txn_id].'</a>';
		} elseif($row[processor]=="safepay") {
		echo '<a style="text-decoration:none;" href="https://www.safepaysolutions.com/ipnpayments.php" target="_blank">'.$row[txn_id].'</a>';
		} elseif($row[processor]=="payza") {
		echo '<a style="text-decoration:none;" href="http://payza.com" target="_blank">'.$row[txn_id].'</a>';
		} else {
		echo $row[txn_id];
		}
		
		//LFM Refund
		if ($row[refunded]==0) {
			echo("<br><br><a href=admin.php?f=ipnsales&refundid=".$row[id].">Process Refund</a>");
		} else {
			echo("<br><br><b>Refunded</b>");
		}
		//End LFM Refund
		
	echo '</td>';
	$r = mysql_query("SELECT upgend FROM ".$prefix."members WHERE Id=$row[user_id]");
	if(mysql_num_rows($r)) {
		echo '<td title="Upgrade ends: '.mysql_result($r,0).'"><a style="text-decoration:none;" href="admin.php?f=mm&sf=browse&searchfield=id&searchtext='.$row[user_id].'">'.$row[user_id].'</a></td>';
		} else {
		echo '<td bgcolor="red" title="User cannot be found!"><font color="white"><b>'.$row[user_id].'</b></td>';
		}
	echo '<td align="left" title="View all IPN payments received from this email address">&nbsp;';
	if($row[name]) { echo '<i>'.$row[name].'</i><br>&nbsp;'; }
	echo "<a style=\"text-decoration:none;\" href=\"admin.php?f=ipnsales&processor=$row[processor]&offset=$prevoffset&field=$field&ob=$ob&q=$row[email]\">$row[email]</td>";
	echo '<td align="left" title="Item Number '.$row[item_number].'">&nbsp;'.$row[item_name].'</td>';
	echo '<td>'.decimal($row[amount]).'</td><td>-'.decimal($row[fee]).'</td>';
	$d=substr($row[added],0,10);
	$t=substr($row[added],10);
	echo "</td><td title=\"Search for other transactions on this date\"><a style=\"text-decoration:none;\" href=\"admin.php?f=ipnsales&processor=$processor&offset=$prevoffset&field=$field&ob=$ob&q=$d\">$d</a>$t</td></tr>";    
	$total=$total+$row[amount];
	$fees=$fees+$row[fee];
	}
 
echo '<tr><th colspan=5 align="right">Totals: $</th><th>'.decimal($total).'</th><th>-'.decimal($fees).'</th><th align="left">&nbsp; = '.decimal($total-$fees).'</th></tr></table>
<p>'; 

$pages=intval($number/$limit); 

if ($offset>1) { 
	$prevoffset=$offset-$limit; 
	echo "<a href=\"admin.php?f=ipnsales&processor=$processor&offset=$prevoffset&field=$field&ob=$ob&q=$q\">$Prev</a> \n"; 
	} 

for ($i=1;$i<$pages+2;$i++) { 
	$newoffset=$limit*($i-1);
	if($newoffset==$offset) {
		echo "<b>$i</b> ";
		} else {
		echo "<a href=\"admin.php?f=ipnsales&processor=$processor&offset=$newoffset&field=$field&ob=$ob&q=$q\">$i</a> \n";
		}
	} 

if ($number>($offset+$limit)) { 
	$nextoffset=$offset+$limit; 
	echo "<a href=\"admin.php?f=ipnsales&processor=$processor&offset=$nextoffset&field=$field&ob=$ob&q=$q\">$Next</a><p>\n"; 
	} 

echo "<p>$number rows found in database<br><br>";

?>