<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

$m=$_GET[m];
if($m) {
	$y=substr($m,0,4);
	$m=substr($m,5);
	$i=mktime(0,0,0,$m,1,$y);
	$j=mktime(0,0,0,$m+1,0,$y);
	} else {
	$i=$_GET[i];
	$d=$_GET[d];
	$finish=date("Y-m-d").' 23:59:59';
	}

if(!$i) {
	$start="0000-00-00 00:00:00";
	} else {
	// show from time...
	$start=date("Y-m-d",$i).' 00:00:00';
	if($d) { $finish=date("Y-m-d",$i).' 23:59:59'; }
	if($j) { $finish=date("Y-m-d",$j).' 23:59:59'; }
	}

$products=array();
$result=mysql_query("SELECT id,name,amount,subscription FROM ".$prefix."ipn_products ORDER BY subscription;");
$f=0;
while($row=mysql_fetch_array($result)) {
	$products[$f]['item']=$row['id'];
	$products[$f]['name']=$row['name'];
	$products[$f]['amount']=$row['amount'];
	$products[$f]['type']=$row['subscription'];
	$f++;
	}

echo '	
<p>
<b>Products Sold between '.$start.' and '.$finish.'</b>
<p>
<table border=1 cellpadding=0 cellspacing=0>
<tr bgcolor="#F8F8F8"><th width=180>Item</th><th width=60>PayPal<br>Txns</th><th width=60>2CO<br>Txns</th><th width=60>SafePay<br>Txns</th><th width=60>Payza<br>Txns</th><th width=60>PayPal<br>Total</th><th width=60>2CO<br>Total</th><th width=60>SafePay<br>Total</th><th width=60>Payza<br>Total</th><th width=100>Product Totals</th></tr>';

$f = $buy = $sub = $oto = $bppt = $btct = $bspt = $bapt = $sppt = $stct = $sspt = $sapt = $oppt = $otct = $ospt = $oapt = $tpp = $ttc = $tsp = $tap = $total = 0;

while($f<count($products)) {
	$item=$products[$f]['item'];
	$and=" AND item_number=$item AND added>='$start' AND added<='$finish';";
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='paypal' $and");
	$pptrans=mysql_result($r,0); if(!$pptrans) { $pptrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='2checkout' $and");
	$tctrans=mysql_result($r,0); if(!$tctrans) { $tctrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='safepay' $and");
	$sptrans=mysql_result($r,0); if(!$sptrans) { $sptrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='payza' $and");
	$aptrans=mysql_result($r,0); if(!$aptrans) { $aptrans="0"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='paypal' $and");
	$pptotal=mysql_result($r,0); if(!$pptotal) { $pptotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='2checkout' $and");
	$tctotal=mysql_result($r,0); if(!$tctotal) { $tctotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='safepay' $and");
	$sptotal=mysql_result($r,0); if(!$sptotal) { $sptotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='payza' $and");
	$aptotal=mysql_result($r,0); if(!$aptotal) { $aptotal="0.00"; }
	$prototal=decimal($pptotal+$tctotal+$sptotal+$aptotal);
	$tpp=decimal($tpp+$pptotal);
	$ttc=decimal($ttc+$tctotal);
	$tsp=decimal($tsp+$sptotal);
	$tap=decimal($tap+$aptotal);
	$total=decimal($total+$prototal);
	if($products[$f]['type']==0) {
		$bgc="#FFFFCC";
		$bppt=$bppt+$pptrans;
		$btct=$btct+$tctrans;
		$bspt=$bspt+$sptrans;
		$bapt=$bapt+$aptrans;
		$bpp=decimal($bpp+$pptotal);
		$btc=decimal($btc+$tctotal);
		$bsp=decimal($bsp+$sptotal);
		$bap=decimal($bap+$aptotal);
		$buy=$buy+$prototal;
		} elseif($products[$f]['type']==1) {
		$bgc="#FFF0F5";
		$sppt=$sppt+$pptrans;
		$stct=$stct+$tctrans;
		$sspt=$sspt+$sptrans;
		$sapt=$sapt+$aptrans;
		$spp=decimal($spp+$pptotal);
		$stc=decimal($stc+$tctotal);
		$ssp=decimal($ssp+$sptotal);
		$sap=decimal($sap+$aptotal);
		$sub=$sub+$prototal;
		} elseif($products[$f]['type']==2) {
		$bgc="#C8E7FF";
		$oppt=$oppt+$pptrans;
		$otct=$otct+$tctrans;
		$ospt=$ospt+$sptrans;
		$oapt=$oapt+$aptrans;
		$opp=decimal($opp+$pptotal);
		$otc=decimal($otc+$tctotal);
		$osp=decimal($osp+$sptotal);
		$oap=decimal($oap+$aptotal);
		$oto=$oto+$prototal;
		}
	if($pptrans||$tctrans||$sptrans||$aptrans) {
		echo '<tr bgcolor="'.$bgc.'" title="Item #'.$item.' - $'.$products[$f]['amount'].'" align="center"><td>&nbsp; '.$products[$f]['name'].' &nbsp;</td><td>'.$pptrans.'</td><td>'.$tctrans.'</td><td>'.$sptrans.'</td><td>'.$aptrans.'</td><td>'.decimal($pptotal).'</td><td>'.decimal($tctotal).'</td><td>'.decimal($sptotal).'</td><td>'.decimal($aptotal).'</td><td>'.$prototal.'</td></tr>';
		}
	$f++;
	}

echo '<tr bgcolor="#F8F8F8"><th colspan=5 align="right">Sales total: </th><th>'.$tpp.'</th><th>'.$ttc.'</th><th>'.$tsp.'</th><th>'.$tap.'</th><th>'.$total.'</th></tr>
</table>
<p>
<b>Categories</b>
<p>
<table border=1 cellpadding=0 cellspacing=0>
<tr bgcolor="#F8F8F8"><th width=180>Type</th><th width=60>PayPal<br>Txns</th><th width=60>2CO<br>Txns</th><th width=60>SafePay<br>Txns</th><th width=60>Payza<br>Txns</th><th width=60>PayPal<br>Total</th><th width=60>2CO<br>Total</th><th width=60>SafePay<br>Total</th><th width=60>Payza<br>Total</th><th width=100>Grand Totals</th></tr>
';

echo '<tr bgcolor="#FFFFCC" align="center"><td>&nbsp;Buy Now&nbsp;</td><td align="center">'.$bppt.'</td><td align="center">'.$btct.'</td><td align="center">'.$bspt.'</td><td align="center">'.$bapt.'</td><td align="center">'.decimal($bpp).'</td><td align="center">'.decimal($btc).'</td><td align="center">'.decimal($bsp).'</td><td align="center">'.decimal($bap).'</td><td align="center">'.decimal($buy).'</td></tr>
';

echo '<tr bgcolor="#FFF0F5" align="center"><td>&nbsp;Subscription&nbsp;</td><td align="center">'.$sppt.'</td><td align="center">'.$stct.'</td><td align="center">'.$sspt.'</td><td align="center">'.$sapt.'</td><td align="center">'.decimal($spp).'</td><td align="center">'.decimal($stc).'</td><td align="center">'.decimal($ssp).'</td><td align="center">'.decimal($sap).'</td><td align="center">'.decimal($sub).'</td></tr>
';

echo '<tr bgcolor="#C8E7FF" align="center"><td>&nbsp;One-Time-Offer&nbsp;</td><td align="center">'.$oppt.'</td><td align="center">'.$otct.'</td><td align="center">'.$ospt.'</td><td align="center">'.$oapt.'</td><td align="center">'.decimal($opp).'</td><td align="center">'.decimal($otc).'</td><td align="center">'.decimal($osp).'</td><td align="center">'.decimal($oap).'</td><td align="center">'.decimal($oto).'</td></tr>
';

echo '<tr bgcolor="#F8F8F8"><th colspan=5 align="right">Sales total: </th><th>'.$tpp.'</th><th>'.$ttc.'</th><th>'.$tsp.'</th><th>'.$tap.'</th><th>'.$total.'</th></tr>
</table>
<p>&nbsp;';

?>