<?php
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2013 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

extract($_GET);
extract($_POST);

echo("<center><br><br>");

//Start Delete or Disable IPN Mod

$delete = $_GET['delete'];
$disable = $_GET['disable'];
$enable = $_GET['enable'];
$did = $_GET['did'];
if ($delete == "yes") {
$confirmdelete = $_GET['confirmdelete'];
if (($confirmdelete == "yes") && ($did > 1)) {
$deleteitem = lfmsql_query("Delete from ".$prefix."ipn_products where id=$did");
} else{
echo("<i>Warning: If the item is a subscription, make sure there are no active recurring subscriptions for the item.<br><br>If you would rather just remove the item from the Buy or Upgrade pages, go back and disable the item instead.</i><br><br><b>Are you sure you want to permanently delete this item?</b><br><br><a href=admin.php?f=scrds&delete=yes&confirmdelete=yes&did=$did><b>Yes</b></a><br><br><a href=admin.php?f=scrds><b>No</b></a>");
exit;
}
}
if ($disable == "yes") {
$disableitem = lfmsql_query("Update ".$prefix."ipn_products set disable=1 where id=$did");
}
if ($enable == "yes") {
$enable = lfmsql_query("Update ".$prefix."ipn_products set disable=0 where id=$did");
}

//End Delete or Disable IPN Mod

if (is_numeric($ffrm)) {
	if($merchant1=="on") { $merchant1=1; } else { $merchant1=0; }
	if($merchant2=="on") { $merchant2=1; } else { $merchant2=0; }
	if($merchant3=="on") { $merchant3=1; } else { $merchant3=0; }
	if($merchant4=="on") { $merchant4=1; } else { $merchant4=0; }
	if($merchant5=="on") { $merchant5=1; } else { $merchant5=0; }  
	if($merchant6=="on") { $merchant6=1; } else { $merchant6=0; }
	
	if (!isset($bimps) || !is_numeric($bimps)) { $bimps=0; }
	if (!isset($limps) || !is_numeric($limps)) { $limps=0; }
	
      if ($ffrm == 0) {
      
      		$checklastrow = lfmsql_query("Select rank from ".$prefix."ipn_products where id>1 order by rank desc");
		if (lfmsql_num_rows($checklastrow) > 0) {
		$lastrow = lfmsql_result($checklastrow, 0, "rank");
		} else {
		$lastrow = 0;
		}
		$newrank = $lastrow+1;
		
		if (!isset($showcat) || !is_numeric($showcat)) { $insertcat = "0"; } else { $insertcat = $showcat; }
		
		$defaultreturn = addslashes(lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_vals` WHERE field='return'"), 0));
		$defaultsubject = addslashes(lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_vals` WHERE field='memsubj'"), 0));
		$defaultbody = addslashes(lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_vals` WHERE field='membody'"), 0));

            lfmsql_query("INSERT INTO ".$prefix."ipn_products (catid, name, credits, bimps, limps, upgrade, amount, period, type, subscription, merchant1, merchant2, merchant3, merchant4, merchant5, merchant6, showaccount, twocoid, rank, return_url, email_subject, email_body) VALUES ('$insertcat', '$name', '$credits', '$bimps', '$limps', '$upgrade', '$amount', '$period', '$type', '$subscription', '$merchant1', '$merchant2', '$merchant3', '$merchant4', '$merchant5', '$merchant6', '$showaccount', '$twocoid', '$newrank', '$defaultreturn', '$defaultsubject', '$defaultbody')");
      	            // Custom Fields 1
            $salespackid = lfmsql_insert_id();
            $getfields = lfmsql_query("SELECT id FROM `".$prefix."customfields` WHERE onipn='1' ORDER BY rank ASC");
		if (lfmsql_num_rows($getfields) > 0) {
			for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
				$fieldid = lfmsql_result($getfields, $i, "id");
				$fieldpostname = "custom_".$fieldid;
				if (isset($_POST[$fieldpostname]) && is_numeric($_POST[$fieldpostname])) {
					lfmsql_query("DELETE FROM ".$prefix."customipnvals WHERE fieldid='".$fieldid."' AND ipnid='".$salespackid."'") or die(lfmsql_error());
					lfmsql_query("INSERT INTO ".$prefix."customipnvals (fieldid, ipnid, fieldvalue) VALUES ('".$fieldid."', '".$salespackid."', '".$_POST[$fieldpostname]."')") or die(lfmsql_error());
				}
			}
		}
            // End Custom Fields 1
            
      	} elseif($submit=="Save") {
      		
      		// Custom Fields 1b
      		if(!$_GET[more]) {
	      		$salespackid = $ffrm;
      			$getfields = lfmsql_query("SELECT id FROM `".$prefix."customfields` WHERE onipn='1' ORDER BY rank ASC");
			if (lfmsql_num_rows($getfields) > 0) {
				for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
					$fieldid = lfmsql_result($getfields, $i, "id");
					$fieldpostname = "custom_".$fieldid;
					if (isset($_POST[$fieldpostname]) && is_numeric($_POST[$fieldpostname])) {
						lfmsql_query("DELETE FROM ".$prefix."customipnvals WHERE fieldid='".$fieldid."' AND ipnid='".$salespackid."'") or die(lfmsql_error());
						lfmsql_query("INSERT INTO ".$prefix."customipnvals (fieldid, ipnid, fieldvalue) VALUES ('".$fieldid."', '".$salespackid."', '".$_POST[$fieldpostname]."')") or die(lfmsql_error());
					}
				}
			}
      		}
      		// End Custom Fields 1b
      		
		if($_GET[more]) {
			$query = "UPDATE ".$prefix."ipn_products SET catid='$catid', promocode='$promocode', description='$description', trial_amount='$trial_amount', trial_period='$trial_period', trial_type='$trial_type', return_url='$return_url', email_subject='$email_subject', email_body='$email_body' WHERE id=$ffrm";
			} else {
			$query = "UPDATE ".$prefix."ipn_products SET id='$id', name='$name', credits='$credits', bimps='$bimps', limps='$limps', upgrade='$upgrade', amount='$amount', period='$period', type='$type', subscription='$subscription', merchant1='$merchant1', merchant2='$merchant2', merchant3='$merchant3', merchant4='$merchant4', merchant5='$merchant5', merchant6='$merchant6', showaccount='$showaccount', twocoid='$twocoid' where id=$ffrm";
			} 
		lfmsql_query($query);
	     	}
	}

//Change A Rank
$rank = $_GET['rank'];
if ($rank == "up" || $rank == "down") {

$runchange = "yes";
$id = $_GET['productsid'];

$checkfirstrow = lfmsql_query("Select rank from ".$prefix."ipn_products where id>1 order by rank asc");
$firstrow = lfmsql_result($checkfirstrow, 0, "rank");

$checklastrow = lfmsql_query("Select rank from ".$prefix."ipn_products where id>1 order by rank desc");
$lastrow = lfmsql_result($checklastrow, 0, "rank");

if (($rank=="up") && ($id > $firstrow)) {
$change=$id-1;
}
elseif (($rank=="down") && ($id < $lastrow)) {
$change=$id+1;
}
else {
$runchange = "no";
}

if ($runchange == "yes") {
@lfmsql_query("Update ".$prefix."ipn_products set rank=0 where rank=$id");
@lfmsql_query("Update ".$prefix."ipn_products set rank=$id where rank=$change");
@lfmsql_query("Update ".$prefix."ipn_products set rank=$change where rank=0");
@lfmsql_query("ALTER TABLE ".$prefix."ipn_products ORDER BY rank;");
}

}
//End Change A Rank

    $res=lfmsql_query("SELECT * FROM ".$prefix."ipn_merchants ORDER BY id LIMIT 6;");
    $tot=lfmsql_num_rows($res);
    $merchant=array();
    for($f=1;$f<=$tot;$f++) {
		$row=lfmsql_fetch_array($res);
		$merchant[$f]=$row[name];
		}

if($more > 1) {

    $res = lfmsql_query("SELECT * FROM ".$prefix."ipn_products WHERE id=$more");
    $row=lfmsql_fetch_array($res);
    extract($row);
    echo '<form action="admin.php?f=scrds&more='.$more.'" method="post">
<input type=hidden name=ffrm value="'.$id.'">
<table width=500 border=1 cellpadding=5 cellspacing=0 align="center">
<tr><td align="right">ID #</td>
<td><input type=text name=id value="'.$id.'" size=1></td></tr>
<tr><td align="right">Name</td><td>'.$name.'</td></tr>

<tr>
<td align="right">Category<br><span style="cursor:pointer; color:blue; size:8px;" onClick="window.open(\'/admin/ipn_editcats.php\',\'edit_cats\',\'scrollbars=yes, menubars=no, width=550, height=550\');">Edit Categories</span></td>
<td>
<select name="catid">
<option value="0">None</option>
';
$getcats = lfmsql_query("SELECT id, catname FROM `".$prefix."ipn_cats` ORDER BY catname ASC") or die(lfmsql_error());
if (lfmsql_num_rows($getcats) > 0) {
	while ($catlist = lfmsql_fetch_array($getcats)) {
		echo("<option value=\"".$catlist['id']."\""); if ($catid == $catlist['id']) { echo(" selected=\"selected\""); } echo(">".$catlist['catname']."</option>");
	}
}
echo '
</select>
</td></tr>

<tr><td align="right">Promo Code</td><td><input type=text name=promocode value="'.$promocode.'" size=15></td></tr>';
    if($subscription) {
	echo '
<tr><td align="right">Cycle Price</td><td>$ '.$amount.' USD</td></tr>
<tr><td align="right">Cycle Period</td><td>'.$period.' ';
	if($type=="D") { echo ' Day';	}
		elseif($type=="M") { echo ' Month'; }
		elseif($type=="Y") { echo ' Year'; }
	if($period<>1) { echo 's'; }
	echo  '</td></tr>
<tr><td align="right">Trial Price</td><td>$<INPUT type=text name=trial_amount value="'.$trial_amount.'" size=2> USD</td><tr><td align="right">Trial Period</td><td><INPUT type=text name=trial_period value="'.$trial_period.'" size=1> <SELECT name=trial_type>
<OPTION value="N">N</OPTION>
<OPTION value="D"';
	if($trial_type=="D") { echo ' SELECTED'; }
	echo '>D</OPTION>
<OPTION value="M"';
	if($trial_type=="M") { echo ' SELECTED'; }
	echo '>M</OPTION>
<OPTION value="Y"';
	if($trial_type=="Y") { echo ' SELECTED'; }
	echo '>Y</OPTION>
</SELECT></td></tr>
</tr>';
	} else {
	echo '<INPUT type="hidden" name="trial_amount" value="0.00">
<INPUT type="hidden" name="trial_period" value="0">
<INPUT type="hidden" name="trial_type" value="N">
<tr><td align="right">Price</td><td>$ '.$amount.' USD</td></tr>';
	}
    echo  '
<tr><td align="right">Return URL</td><td><input type=text name=return_url value="'.$return_url.'" size=50><br><p align="left"><font size="1">This is your Thank You page customers are taken to after purchasing this product.  Note that many payment processors will encode this URL into their button HTML, so a user could potentially access it without completing the purchase. Therefore, we do not recommend putting any download links or codes directly on your Thank You page.</font></p></td></tr>

<tr><td align="right">Thank You Subject</td><td><input type=text name=email_subject value="'.$email_subject.'" size=50></td></tr>

<tr><td align="right" valign="top">Thank You Message</td><td><textarea name="email_body" cols=50 rows=7>'.$email_body.'</textarea></td></tr>

<tr><td align="right" valign="top">Description</td><td><textarea name="description" cols=50 rows=4>'.$description.'</textarea></td></tr>
<tr><td align="right">Credits</td><td>'.$credits.'</td></tr>';
    if($upgrade>1) {
		echo '<tr><td align="right">Acct Type</td><td>';
		echo lfmsql_result(lfmsql_query("Select accname from `".$prefix."membertypes` where mtid=$upgrade"),0);
		echo '</td></tr>';
		}
    echo '
<tr><td align="right">Item Type</td><td>';
    if($subscription==1) { echo 'Subscription'; } 
      elseif($subscription==2) { echo 'One Time Offer'; }
	else { echo 'Buy Now Item'; }
    echo '</td></tr>';
    for($f=1;$f<=$tot;$f++) {
    $merc=lfmsql_result(lfmsql_query("SELECT name FROM ".$prefix."ipn_merchants WHERE id=$f;"),0);
	$name="merchant$f";
	echo '
<tr><td align="right">Accept '.$merc.'</td><td>
<input type=hidden name='.$name.' value="';
if($$name) { echo 'on">Yes'; } else { echo 'off">No'; }
echo '</td></tr>';
	}
echo '
<tr><td colspan=2 align="center"><input type=submit name=submit value="Save">
</form>
</td></tr>
</table>';
 
    } else {
    
    $getimpsetting = lfmsql_query("Select storeimps from ".$prefix."settings limit 1");
    $impsetting = lfmsql_result($getimpsetting, 0, "storeimps");
    
?>
<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  <tr>
    <td align="center"><div class="lfm_infobox_heading">Sales Packages</div></td>
  </tr>
  
  <tr>
    <td align="left"><div class="lfm_descr">
    	
    	<p>Sales Packages are the items that you're selling on your site. A Sales Package could be a downloadable file, a subscription to a higher membership level, or a special offer containing a combination of items.</p>
    	
    	You can put a payment button into your templates and custom pages by using the macro #IPNid# where "id" is the Sales Package ID number.  So, if you wanted to display the payment button for item #4 you would enter <b>#IPN4#</b> into your template code.
    	
    </div></td>
  </tr>
      
</table>
</div>
<p>&nbsp;</p>

<p><font size=3><b>Show:</b>
<? if (!isset($showcat)) { $showcat = "0"; } ?>
<a href="admin.php?f=scrds"><? if (!isset($showcat) || strlen($showcat) < 1 || (is_numeric($showcat) && $showcat == 0)) { echo("<b>All</b>"); } else { echo("All"); } ?></a> | 
<a href="admin.php?f=scrds&showcat=enabled"><? if ($showcat=="enabled") { echo("<b>Enabled</b>"); } else { echo("Enabled"); } ?></a> | 
<a href="admin.php?f=scrds&showcat=disabled"><? if ($showcat=="disabled") { echo("<b>Disabled</b>"); } else { echo("Disabled"); } ?></a> | 
<?php
$getcats = lfmsql_query("SELECT id, catname FROM `".$prefix."ipn_cats` ORDER BY catname ASC") or die(lfmsql_error());
if (lfmsql_num_rows($getcats) > 0) {
	while ($catlist = lfmsql_fetch_array($getcats)) {
		echo("<a href=\"admin.php?f=scrds&showcat=".$catlist['id']."\">"); if ($showcat == $catlist['id']) { echo("<b>".$catlist['catname']."</b>"); } else { echo($catlist['catname']); } echo("</a> | ");
	}
}
?>
</font></p>

<?

    echo '<p>
<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="#F8F8F8">
<th>Rank</th>
<th>IPN ID</th>
<th>Title</th>
<th>Price</th>
<th>Show to Account</th>
';
// Custom Fields 2
$getfields = lfmsql_query("SELECT name FROM `".$prefix."customfields` WHERE onipn='1' ORDER BY rank ASC");
if (lfmsql_num_rows($getfields) > 0) {
	for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
		$fieldname = lfmsql_result($getfields, $i, "name");
		echo('<th>'.$fieldname.'</th>');
	}
}
// End Custom Fields 2
echo'
<th>Credits</th>';

if ($impsetting == 1) {
echo '<th>Banners</th>
<th>Texts</th>';
}

echo '<th>Downloads</th>
<th>Content<br>Pages</th>
<th>Upgrade</th>
<th>Period</th>
<th>Type</th>';
    for($f=1;$f<=$tot;$f++) {
	echo '
<th width=35 style="font-size:8px">'.$merchant[$f].'</th>';
	}
echo '
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
<form action="admin.php?f=scrds&showcat='.$showcat.'" method="post">
<input type=hidden name=ffrm value="0">
<tr align="center">
<td>&nbsp</td>
<td><b>New</b><br>2CO ID: <input type=text name=twocoid size=3 value=0></td>
<td><input type=text name=name size=10></td>
<td><input type=text name=amount size=2 value="0.00"></td>
<td><select name=showaccount><option value=0>All Accounts</option>';
$getaccounts=lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
while($row=lfmsql_fetch_array($getaccounts)) {
echo '<option value='.$row[mtid].'>'.$row[accname].'</option>';
}
echo '</select></td>
';
// Custom Fields 3
$getfields = lfmsql_query("SELECT id, type, options FROM `".$prefix."customfields` WHERE onipn='1' ORDER BY rank ASC");
if (lfmsql_num_rows($getfields) > 0) {
	for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
		$fieldid = lfmsql_result($getfields, $i, "id");
		$fieldtype = lfmsql_result($getfields, $i, "type");
		$fieldoptions = lfmsql_result($getfields, $i, "options");
		$fieldpostname = "custom_".$fieldid;
		
		echo('<td>');
    			
    			if ($fieldtype == 1) {
				echo('<input type="text" name="'.$fieldpostname.'" size="2" value="0" />');
			} else {
				echo('<select name="'.$fieldpostname.'">
				<option value="0" selected="selected">--No Action--</option>');
				
				$splitoptions = explode(",", $fieldoptions);
				for ($j = 0; $j < count($splitoptions); $j++) {
					$optionindex = $j + 1;
					if (strlen(trim($splitoptions[$j])) > 0) {
						$splitoptions[$j] = trim($splitoptions[$j]);
						echo('<OPTION VALUE="'.$optionindex.'"');
						echo('>'.$splitoptions[$j].'</OPTION>');
					}
				}
				
				echo('</select>');
			}
			
			echo('</td>');
		
	}
}
// End Custom Fields 3
echo '<td><input type=text name=credits size=2 value=0></td>';

if ($impsetting == 1) {
echo '<td><input type=text name=bimps size=2 value=0></td>
<td><input type=text name=limps size=2 value=0></td>';
}

echo '<td>N/A</td>
<td>N/A</td>
<td><SELECT name=upgrade>
<OPTION value=0></OPTION>
';
$res=lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
while($row=lfmsql_fetch_array($res)) {
	echo '<OPTION value='.$row[mtid].'>'.$row[accname].'</OPTION>
';
	}
echo '</SELECT></td>
<td><input type=text name=period size=1 style="font-size:10px;text-align:right"><br><SELECT name=type style="font-size:10px">
<OPTION value="N">N</OPTION>
<OPTION value="D">D</OPTION>
<OPTION value="M">M</OPTION>
<OPTION value="Y">Y</OPTION>
</SELECT></td>
<td style="font-size:10px">
<INPUT type=radio name=subscription value=0 CHECKED>BUY
<br><INPUT type=radio name=subscription value=1>SUB
<br><INPUT type=radio name=subscription value=2>OTO</td>
';
    for($f=1;$f<=$tot;$f++) {
	echo '
<td><input type=checkbox name=merchant'.$f.'></td>';
	}
echo '
<td><input type=submit name=submit value="Add"></td>
<td>&nbsp;</td>
</tr>
</form>
';

if ($showcat == "enabled") {
	$resultwhere = " AND disable='0'";
} elseif ($showcat == "disabled") {
	$resultwhere = " AND disable='1'";
} elseif (isset($showcat) && is_numeric($showcat) && $showcat > 0) {
	$resultwhere = " AND catid='".$showcat."'";
} else {
	$resultwhere = "";
}

    $result = lfmsql_query("SELECT * FROM ".$prefix."ipn_products WHERE id>1".$resultwhere." ORDER BY rank ASC");
    for ($i = 0; $i < lfmsql_num_rows($result); $i++) {
        $row=lfmsql_fetch_array($result);
		extract($row);
        echo '<tr align="center" valign="center">
<td><form style="margin:0px" action="admin.php?f=scrds&showcat='.$showcat.'" method="post">
<input type=hidden name=ffrm value="'.$id.'">
<input type=hidden name=id value="'.$id.'">
<a href=admin.php?f=scrds&showcat='.$showcat.'&rank=up&productsid='.$rank.'><img border=0 src="../images/move_up.gif"></a><a href=admin.php?f=scrds&showcat='.$showcat.'&rank=down&productsid='.$rank.'><img border=0 src="../images/move_down.gif"></a></td>
<td><a href="admin.php?f=scrds&more='.$id.'"><b># '.$id.'</b></a><br>2CO ID: <input type=text name=twocoid value="'.$twocoid.'" size=3></td>
<td><input type=text name=name value="'.$name.'" size=10></td>
<td><input type=text name=amount value="'.$amount.'" size=2></td>
<td><select name=showaccount><option value=0>All Accounts</option>';
$getaccounts=lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
while($row=lfmsql_fetch_array($getaccounts)) {
echo '<option value='.$row[mtid];
if($showaccount==$row[mtid]) { echo ' selected'; }
echo '>'.$row[accname].'</option>';
}
echo '</select></td>
';
// Custom Fields 4
$getfields = lfmsql_query("SELECT id, type, options FROM `".$prefix."customfields` WHERE onipn='1' ORDER BY rank ASC");
if (lfmsql_num_rows($getfields) > 0) {
	for ($k = 0; $k < lfmsql_num_rows($getfields); $k++) {
		$fieldid = lfmsql_result($getfields, $k, "id");
		$fieldtype = lfmsql_result($getfields, $k, "type");
		$fieldoptions = lfmsql_result($getfields, $k, "options");
		$fieldpostname = "custom_".$fieldid;
		
		$getfieldvalue = lfmsql_query("SELECT fieldvalue FROM `".$prefix."customipnvals` WHERE fieldid='".$fieldid."' AND ipnid='".$id."'");
		if (lfmsql_num_rows($getfieldvalue) > 0) {
			$fieldvalue = lfmsql_result($getfieldvalue, 0, "fieldvalue");
		} else {
			$fieldvalue = "0";
		}
		
		echo('<td>');
    			
    			if ($fieldtype == 1) {
				echo('<input type="text" name="'.$fieldpostname.'" size="2" value="'.$fieldvalue.'" />');
			} else {
				echo('<select name="'.$fieldpostname.'">
				<option value="0"'); if ($fieldvalue == "0") { echo(' selected="selected"'); } echo('>--No Action--</option>');
				
				$splitoptions = explode(",", $fieldoptions);
				for ($j = 0; $j < count($splitoptions); $j++) {
					$optionindex = $j + 1;
					if (strlen(trim($splitoptions[$j])) > 0) {
						$splitoptions[$j] = trim($splitoptions[$j]);
						echo('<OPTION VALUE="'.$optionindex.'"');
						if ($fieldvalue == $optionindex) { echo(' selected="selected"'); }
						echo('>'.$splitoptions[$j].'</OPTION>');
					}
				}
				
				echo('</select>');
			}
			
			echo('</td>');
		
	}
}
// End Custom Fields 4
echo '<td><input type=text name=credits value="'.$credits.'" size=2></td>';

if ($impsetting == 1) {
echo '<td><input type=text name=bimps value="'.$bimps.'" size=2></td>
<td><input type=text name=limps value="'.$limps.'" size=2></td>';
}

echo '<td>';
$getprodlist = lfmsql_query("SELECT productid, productname FROM ".$prefix."products ORDER BY productid");
while($proddata = @lfmsql_fetch_object($getprodlist)) {
	if (array_search($proddata->productid, explode(",", $productid)) !==false) {
		echo '<font size="1">'.$proddata->productname.'</font><br>';
	}
}
echo '<br><span style="cursor:pointer; color:blue; size:8px;" onClick="window.open(\'/admin/ipn_setdownloads.php?id='.$id.'\',\'set_downloads\',\'scrollbars=yes, menubars=no, width=550, height=550\');">Add/Edit</span></td>';

$countpages = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."ipn_pages` WHERE productid=".$id), 0);
if ($countpages < 1) {
	$countpages = "None";
}

echo ('<td>'.$countpages.'<br><span style="cursor:pointer; color:blue; size:8px;" onClick="window.open(\'/admin/ipn_setpages.php?id='.$id.'\',\'set_pages\',\'scrollbars=yes, menubars=no, width=550, height=550\');">Add</span></td>');

echo '<td><SELECT name=upgrade>
<OPTION value=0></OPTION>
';
$res=lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
while($row=lfmsql_fetch_array($res)) {
	echo '<OPTION value='.$row[mtid];
	if($upgrade==$row[mtid]) { echo ' SELECTED'; }
	echo '>'.$row[accname].'</OPTION>
';
	}
echo '</SELECT></td>
<td nowrap="nowrap"><input type=text name=period value="'.$period.'" size=1 style="font-size:10px;text-align:right"><br><SELECT name=type style="font-size:10px">
<OPTION value="N">N</OPTION>
<OPTION value="D"';
	if($type=="D") { echo ' SELECTED'; }
	echo '>D</OPTION>
<OPTION value="M"';
	if($type=="M") { echo ' SELECTED'; }
	echo '>M</OPTION>
<OPTION value="Y"';
	if($type=="Y") { echo ' SELECTED'; }
	echo '>Y</OPTION>
</SELECT></td>
<td style="font-size:10px">
<INPUT type=radio name=subscription value=0';
if($subscription==0) { echo ' CHECKED'; } 
echo '>BUY
<br><INPUT type=radio name=subscription value=1';
if($subscription==1) { echo ' CHECKED'; } 
echo '>SUB
<br><INPUT type=radio name=subscription value=2';
if($subscription==2) { echo ' CHECKED'; } 
echo '>OTO</td>
';
    for($f=1;$f<=$tot;$f++) {
	$name="merchant$f";
	echo '
<td><input type=checkbox name='.$name;
	if($$name) { echo ' checked'; }
		echo '><br><span style="cursor:pointer; color:blue; size:8px;" onClick="window.open(\'/admin/ipn_editimg.php?merch='.$f.'&id='.$id.'\',\'edit_img\',\'scrollbars=yes, menubars=no, width=550, height=550\');">Image</span></td>';
		}
    echo '
<td>
<input type=submit name=submit value="Save">
</form><br>
';

if ($disable == 0) {
echo ("<form style=\"margin:0px\" action=\"admin.php?f=scrds&showcat=".$showcat."&disable=yes&did=$id\" method=\"post\"><input type=submit name=submit value=\"Disable\"></form><br>");
} else {
echo ("<form style=\"margin:0px\" action=\"admin.php?f=scrds&showcat=".$showcat."&enable=yes&did=$id\" method=\"post\"><input type=submit name=submit value=\"Enable\"></form><br>");
}

echo '
<form style=\"margin:0px\" action="admin.php?f=scrds&showcat='.$showcat.'&delete=yes&did='.$id.'" method="post">
<input type=submit name=submit value="Delete">
</form>

</td>

</tr>
</form>
';
        }
echo '</table>';
    }

?>