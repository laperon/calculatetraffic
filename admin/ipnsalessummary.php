<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

$i=$_GET[i];
if(!$i) { $i=time(); }
$finish=$i-(31*86400);
$p=date("Y-m",$i);

while($i>$finish) {
	$ym=date("Y-m",$i);
	if($_GET[i]) { if($p<>$ym) { $p=0; } }
	if($p) { $day[]=date("Y-m-d",$i); $time[]=$i; }
	$i=$i-86400;
	}

echo '	
<p>
<b>Daily Summary</b>
<p>
<table border=1 cellpadding=0 cellspacing=0>
<tr bgcolor="#F8F8F8"><th width=100>Date</th><th width=60>PayPal<br>Txns</th><th width=60>2CO<br>Txns</th><th width=60>SafePay<br>Txns</th><th width=60>Payza<br>Txns</th><th width=60>PayPal<br>Total</th><th width=60>2CO<br>Total</th><th width=60>SafePay<br>Total</th><th width=60>Payza<br>Total</th><th width=100>Daily Totals</th></tr>';

$f=0;
$finish=count($day);

while($f<$finish) {
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='paypal' AND added LIKE '$day[$f]%';");
	$pptrans=mysql_result($r,0); if(!$pptrans) { $pptrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='2checkout' AND added LIKE '$day[$f]%';");
	$tctrans=mysql_result($r,0); if(!$tctrans) { $tctrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='safepay' AND added LIKE '$day[$f]%';");
	$sptrans=mysql_result($r,0); if(!$sptrans) { $sptrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='payza' AND added LIKE '$day[$f]%';");
	$aptrans=mysql_result($r,0); if(!$aptrans) { $aptrans="0"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='paypal' AND added LIKE '$day[$f]%';");
	$pptotal=mysql_result($r,0); if(!$pptotal) { $pptotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='2checkout' AND added LIKE '$day[$f]%';");
	$tctotal=mysql_result($r,0); if(!$tctotal) { $tctotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='safepay' AND added LIKE '$day[$f]%';");
	$sptotal=mysql_result($r,0); if(!$sptotal) { $sptotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='payza' AND added LIKE '$day[$f]%';");
	$aptotal=mysql_result($r,0); if(!$aptotal) { $aptotal="0.00"; }
	$daytotal=decimal($pptotal+$tctotal+$sptotal+$aptotal);
	if($pptrans||$tctrans||$sptrans||$aptrans) {
		echo '<tr><td align="center">&nbsp; '.$day[$f].' &nbsp;</td><td align="center">'.$pptrans.'</td><td align="center">'.$tctrans.'</td><td align="center">'.$sptrans.'</td><td align="center">'.$aptrans.'</td><td align="center">'.decimal($pptotal).'</td><td align="center">'.decimal($tctotal).'</td><td align="center">'.decimal($sptotal).'</td><td align="center">'.decimal($aptotal).'</td><td align="center"><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor=products&i='.$time[$f].'&d=1">'.$daytotal.'</a></td></tr>';
		}
	$tpp=decimal($tpp+$pptotal);
	$ttc=decimal($ttc+$tctotal);
	$tsp=decimal($tsp+$sptotal);
	$tap=decimal($tap+$aptotal);
	$total=decimal($total+$daytotal);
	$f++;
	if($f==7||$f==14||$f==21||$f==28||$f==$finish) {
		$i=$time[$f]+86400;
		if($f==$finish) { $i=$time[$f-1]; }
		echo "<tr bgcolor=\"#F8F8F8\"><th colspan=5 align=\"right\">$f day total: </th><th>$tpp</th><th>$ttc</th><th>$tsp</th><th>$tap</th><th>&nbsp; <a style=\"text-decoration:none;\" href=\"admin.php?f=ipnsales&processor=products&i=$i&d=0\">$total</a> &nbsp;</th></tr>";
		}
	}

echo '
</table>
<p>
<b>Monthly Summary</b>
';

$i=time();
$finish=$i-(365*86400);
$p=0;

while($i>$finish) {
	$ym=date("Y-m",$i);
	if($ym<>$p) { $mth[]=$ym; $p=$ym; }
	$i=$i-(28*86400);
	}

echo '	
<p>
<table border=1 cellpadding=0 cellspacing=0>
<tr bgcolor="#F8F8F8"><th width=100>Date</th><th width=60>PayPal<br>Txns</th><th width=60>2CO<br>Txns</th><th width=60>SafePay<br>Txns</th><th width=60>Payza<br>Txns</th><th width=60>PayPal<br>Total</th><th width=60>2CO<br>Total</th><th width=60>SafePay<br>Total</th><th width=60>Payza<br>Total</th><th width=100>Monthly Totals</th></tr>';

$tpp=0; $tsp=0; $total=0;

$f=0;
$finish=count($mth);
$tpp=0; $ttc=0; $tsp=0; $tap=0;
$total=0;

while($f<$finish) {
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='paypal' AND added LIKE '$mth[$f]%';");
	$pptrans=mysql_result($r,0); if(!$pptrans) { $pptrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='2checkout' AND added LIKE '$mth[$f]%';");
	$tctrans=mysql_result($r,0); if(!$tctrans) { $tctrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='safepay' AND added LIKE '$mth[$f]%';");
	$sptrans=mysql_result($r,0); if(!$sptrans) { $sptrans="0"; }
	$r=mysql_query("SELECT COUNT(*) FROM ".$prefix."ipn_transactions WHERE processor='payza' AND added LIKE '$mth[$f]%';");
	$aptrans=mysql_result($r,0); if(!$aptrans) { $aptrans="0"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='paypal' AND added LIKE '$mth[$f]%';");
	$pptotal=mysql_result($r,0); if(!$pptotal) { $pptotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='2checkout' AND added LIKE '$mth[$f]%';");
	$tctotal=mysql_result($r,0); if(!$tctotal) { $tctotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='safepay' AND added LIKE '$mth[$f]%';");
	$sptotal=mysql_result($r,0); if(!$sptotal) { $sptotal="0.00"; }
	$r=mysql_query("SELECT SUM(amount) FROM ".$prefix."ipn_transactions WHERE processor='payza' AND added LIKE '$mth[$f]%';");
	$aptotal=mysql_result($r,0); if(!$aptotal) { $aptotal="0.00"; }
	$mthtotal=decimal($pptotal+$tctotal+$sptotal+$aptotal);
	if($pptrans||$tctrans||$sptrans||$aptrans) {
		$m=$mth[$f];
		$timestamp=gmmktime(0,0,0,substr($m,5),31,substr($m,0,4));
		echo '<tr><td align="center">&nbsp; <a style="text-decoration:none;" href="admin.php?f=ipnsales&processor=summary&i='.$timestamp.'">'.$mth[$f].'</a>&nbsp;</td><td align="center">'.$pptrans.'</td><td align="center">'.$tctrans.'</td><td align="center">'.$sptrans.'</td><td align="center">'.$aptrans.'</td><td align="center">'.decimal($pptotal).'</td><td align="center">'.decimal($tctotal).'</td><td align="center">'.decimal($sptotal).'</td><td align="center">'.decimal($aptotal).'</td><td align="center"><a style="text-decoration:none;" href="admin.php?f=ipnsales&processor=products&m='.$mth[$f].'">'.$mthtotal.'</a></td></tr>';
		}
	$tpp=decimal($tpp+$pptotal);
	$ttc=decimal($ttc+$tctotal);
	$tsp=decimal($tsp+$sptotal);
	$tap=decimal($tap+$aptotal);
	$total=decimal($total+$mthtotal);
	$f++;
	}
$i=mktime(0,0,0,substr($mth[$f-1],5),1,substr($mth[$f-1],0,4));
echo "<tr bgcolor=\"#F8F8F8\"><th colspan=5 align=\"right\">Last 12 months total: </th><th>$tpp</th><th>$ttc</th><th>$tsp</th><th>$tap</th><th><a style=\"text-decoration:none;\" href=\"admin.php?f=ipnsales&processor=products&i=$i\">$total</a></th></tr>
</table>
<p>&nbsp;";

?>