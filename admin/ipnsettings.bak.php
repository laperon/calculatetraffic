<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

extract($_GET);
extract($_POST);

echo("<center><br><br>");

?>
    <ol style="text-align: left">
    <li>Be sure that the IPN feature is turned on at all payment processors you want to use.  This step is not required for PayPal.</li>
    <li>Create a new sales item on the <a href="admin.php?f=scrds">IPN Items</a> page. Leave unwanted values at zero.</li>
    <li>The order the buttons are shown in is determined by the order below. You can change the button order by using the up and down arrow keys in the ID column.</li>
    <li>PayPal confirms each transaction by requiring the data received to be sent back for verification. Other payment services send a secret code or payment hash to verify authenticity. The IPN will work whether or not you enter a verify code. However, it is strongly advised that you take the time to set these up correctly to avoid spoofed transactions.</li>
    </ol>
<p>
<table align="center" border="1" cellpadding="2" cellspacing="0">
<tr bgcolor="#F8F8F8">
<th>ID</th>
<th>Merchant</th>
<th>Merchant ID</th>
<th>Verify Code</th>
<th>Buy Now IPN</th>
<th>Subscribe IPN</th>
<th>Action</th>
</tr>
<?

if ($acti == 'sort') {
	if($move=="up") { $move=$id-1; }
	if($move=="down") { $move=$id+1; }
	mysql_query("UPDATE ".$prefix."ipn_merchants SET id=0 WHERE id=$id;"); 
	mysql_query("UPDATE ".$prefix."ipn_merchants SET id=$id WHERE id=$move;"); 
	mysql_query("UPDATE ".$prefix."ipn_merchants SET id=$move WHERE id=0;"); 
	mysql_query("ALTER TABLE ".$prefix."ipn_merchants ORDER BY id;");
	}

if ($acti == 'code') {
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$notify' WHERE field='notify';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$return' WHERE field='return';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$cancel' WHERE field='cancel';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$images' WHERE field='images';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$sitename' WHERE field='sitename';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$memsubj' WHERE field='memsubj';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$membody' WHERE field='membody';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$refsubj' WHERE field='refsubj';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$refbody' WHERE field='refbody';");
	mysql_query("UPDATE ".$prefix."ipn_vals SET value='$bcc' WHERE field='bcc';");
	mysql_query("UPDATE ".$prefix."ipn_settings SET value='$adnotify' WHERE field='adnotify';");
	mysql_query("UPDATE ".$prefix."ipn_settings SET value='$memnotify' WHERE field='memnotify';");
	mysql_query("UPDATE ".$prefix."ipn_settings SET value='$refnotify' WHERE field='refnotify';");
	}

if (is_numeric($ffrm)) {
      if ($ffrm == 0) {
            $zquery = "INSERT INTO ".$prefix."ipn_merchants (name, payee, verify, buy_now, subscribe) VALUES ('$name', '$payee', '$buy_now', '$subscribe')";
      	} else {
            $zquery = "UPDATE ".$prefix."ipn_merchants SET payee='$payee', verify='$verify', buy_now='$buy_now', subscribe='$subscribe' where id=$ffrm";
      	}
	// echo '<p>'.$zquery.'<p>';	
      $resaz = mysql_query($zquery) or die (mysql_error());
	}

    $res=mysql_query("SELECT * FROM ".$prefix."ipn_merchants ORDER BY id;");
    $tot=mysql_num_rows($res);
    $merchant=array();
    for($f=1;$f<=$tot;$f++) {
		$row=mysql_fetch_array($res);
		echo '<form action="admin.php?f=ipnset" method="post">
<input type=hidden name=ffrm value="'.$row[id].'">
<tr align="center">
<td>';
if($row[id]>1) { echo '<a href="admin.php?f=ipnset&acti=sort&move=up&id='.$row[id].'"><img border=0 src="../images/move_up.gif"></a><br>'; } 
echo '#'.$row[id];
if($row[id]<$tot) { echo '<br><a href="admin.php?f=ipnset&acti=sort&move=down&id='.$row[id].'"><img border=0 src="../images/move_down.gif"></a>'; } 
echo '</td>
<td><b>'.$row[name].'</b></td>
<td><INPUT type=text name=payee value="'.$row[payee].'" size=8></td>
<td>';
if($row[name]=="PayPal") {
	echo 'n/a';
	} else {
	echo '<INPUT type=text name=verify value="'.$row[verify].'" size=8>';
	}
echo '</td>
<td><textarea name="buy_now" cols=32 rows=3>'.$row[buy_now].'</textarea></td>
<td><textarea name="subscribe" cols=32 rows=3>'.$row[subscribe].'</textarea></td>
<td><input type=submit value="Save"></form></td>
</tr>';
		}

$res=mysql_query("SELECT field,value FROM ".$prefix."ipn_settings WHERE id>0 LIMIT 3;");
while($row=mysql_fetch_array($res)) {
	${$row['field']}=$row['value'];
	}
$res=mysql_query("SELECT field,value FROM ".$prefix."ipn_vals WHERE id>0 LIMIT 10;");
while($row=mysql_fetch_array($res)) {
	${$row['field']}=$row['value'];
	}
?>
</table>
<p>
<table>
<tr><td colspan=2>
<FORM action="admin.php?f=ipnset&acti=code" method="POST">
<h3>Button Code Variables</h3>
The following placeholders are used in your button code above and will be replaced when the buttons are shown with the following information:</td></tr>
<tr><td width=100 height=24><b>[merchant_id]</b></td><td> - the Merchant ID info from the columns above.</td></tr>
<tr><td height=24><b>[item_name]</b></td><td> - the Title of the item from the Sales Packages page.</td></tr>
<tr><td height=24><b>[item_number]</b></td><td> - the ID of the item from the Sales Packages page.</td></tr>
<tr><td height=24><b>[user_id]</b></td><td> - the User ID of the Member who is currently logged in.</td></tr>
<tr><td height=24><b>[amount]</b></td><td> - the Price of the item from the Sales Packages page.</td></tr>
<tr><td height=24><b>[period]</b></td><td> - the recurring subscription period.</td></tr>
<tr><td height=24><b>[type]</b></td><td> - the PayPal recurring subscription type: D=Days M=Months Y=Years.</td></tr>
<tr><td height=24><b>[days]</b></td><td> - the SafePay length in days that the upgrade will last.</td></tr>
<tr><td height=24><b>[unit]</b></td><td> - the Payza recurring subscription type as a word: day, month or year.</td></tr>
<tr><td height=24><b>[trial_amount]</b></td><td> - the trial price (where applicable). Can be left at zero for a free trial period or set to a different amount than the regular price.</td></tr>
<tr><td height=24><b>[trial_period]</b></td><td> - the length in days of the trial period. If you have left this at zero it will use the regular [amount] and [days].</td></tr>
<tr><td height=24><b>[trial_type]</b></td><td> - the PayPal type of trial period: D=Days M=Months Y=Years.</td></tr>
<tr><td height=24><b>[trial_days]</b></td><td> - the SafePay length in days of the trial period.</td></tr>
<tr><td height=24><b>[trial_unit]</b></td><td> - the Payza trial type as a word: day, month or year.</td></tr>
<tr><td><b>[notify]</b></td><td><INPUT type="text" name="notify" value="<?=$notify?>" size=40> - the page that the IPN information will be posted to whenever a payment is received.</td></tr>
<tr><td><b>[return]</b></td><td><INPUT type="text" name="return" value="<?=$return?>" size=40> - the thank you page that the customer will be sent to after completing payment.</td></tr>
<tr><td><b>[cancel]</b></td><td><INPUT type="text" name="cancel" value="<?=$cancel?>" size=40> - the page the Member will be sent to if they do not complete the checkout process.</td></tr>
<tr><td><b>[images]</b></td><td><INPUT type="text" name="images" value="<?=$images?>" size=40> - the folder where the button images have been uploaded.</td></tr>
<tr><td><b>[sitename]</b></td><td><INPUT type="text" name="sitename" value="<?=$sitename?>" size=40> - the name of the website that is added to the item description for the customers reference.</td></tr>
<tr><td></td><td><INPUT type="submit" name="submit" value="Update">
</td></tr>
<tr><td colspan=2>
<h3>Adding your Merchant ID and Verify Code</h3>
<p><b>PayPal</b> - your Merchant ID is the email address at which you wish to receive payments. It can be any of the verified email addresses associated with your account.
<p><b>2CheckOut</b> - log into your 2CheckOut account and click 'Set Up Products'. Your Merchant ID is the number showing on this page next to 'Products for #'. For the 2CheckOut IPN to work all items on your Sales Packages page must be added with corresponding Product ID numbers in your 2CheckOut account. You must then go to the 'Look and Feel' page and set the Approved URL to the value of your [notify] URL shown above. Set the Pending URL to your [return] URL shown above. Finally, choose a Secret Word, enter it in the box near the bottom of the page and click the Save Changes button. Your Secret Word is also your Verify Code so add it above and Save your 2CheckOut settings.
<p><b>SafePay</b> - your Merchant ID is the username you chose when registering your account. Your Verify Code can be found by logging into your SafePay Solutions account and clicking Seller Tools, then Settings. Scroll down the page to IPN Security Settings. Enter a Secret Passphrase. Also make sure Resend Payment Notification is set to On. Save IPN Settings. Copy and paste the 32 character Hash below your Secret Passphrase into the Verify Code box above and Save your SafePay settings.
<p><b>Payza</b> - your Merchant ID is the email address at which you wish to receive payments. It can any of the verified email addresses associated with your account.
<br>To get your Verify Code log into your Payza account. Click Sell Online, choose the business account you want to manage and scroll down to the IPN Setup heading. Check the Enable IPN box.
<br>Enter a Security Code in the box below and click the Submit button.
<br>Copy and paste the Encrypted Security Code that has now been generated into the Verify Code box above and save your Payza settings.
<p>
<h3>Email Templates</h3>
<p>Each time an IPN page is requested Admin will be sent an email detailing all POST and GET variables. Should the script fail to initiate the transaction (e.g. a MySQL or server error) the data from the email can be used to manually <a href="admin.php?f=ipnsales&action=edit">Add a payment</a>. The script can also send out the following additional emails which can be turned on or off:
<center>
<table>
<tr><td>
<SELECT name="adnotify">
<OPTION value="1">Yes</OPTION>
<OPTION value="0"<? if(!$adnotify) { echo ' SELECTED'; } ?>>No</OPTION>
</SELECT></td><td><b>&nbsp;Send Admin an email with details of the actions taken for each completed transaction.</b></td></tr>
<tr><td>
<SELECT name="memnotify">
<OPTION value="1">Yes</OPTION>
<OPTION value="0"<? if(!$memnotify) { echo ' SELECTED'; } ?>>No</OPTION>
</SELECT></td><td><b>&nbsp;Send the Member an email notification of purchase.</b></td></tr>
<tr><td>
<SELECT name="refnotify">
<OPTION value="1">Yes</OPTION>
<OPTION value="0"<? if(!$refnotify) { echo ' SELECTED'; } ?>>No</OPTION>
</SELECT></td><td><b>&nbsp;Send the Referrer an email notification of commission earned (where applicable).</b></td></tr>
</table>
<p>The email templates can be edited below and an alternate admin email can be added for receiving a Blind Carbon Copy:
<p>
<table border="1" cellpadding="2" cellspacing="0">
<tr>
<th>&nbsp;</th>
<th bgcolor="#F8F8F8">Member Email</th>
<th bgcolor="#F8F8F8">Referrer Email</th>
<th bgcolor="#F8F8F8">Action</th>
</tr>
<tr>
<td><b>Subject</b></td>
<td style="vertical-align:middle;"><INPUT type="text" name="memsubj" value="<?=$memsubj?>" size=50></td>
<td style="vertical-align:middle;"><INPUT type="text" name="refsubj" value="<?=$refsubj?>" size=50></td>
<td rowspan=3 style="vertical-align:middle;"><INPUT type="submit" name="submit" value="Update"></td>
</tr>
<tr>
<td><b>Body</b></td>
<td><textarea name="membody" cols=37 rows=5><?=$membody?></textarea></td>
<td><textarea name="refbody" cols=37 rows=5><?=$refbody?></textarea></td>
</tr>
<tr>
<td><b>Bcc:</b></td>
<td colspan=2 style="vertical-align:middle;"><INPUT type="text" name="bcc" value="<?=$bcc?>" size=104></td>
</tr>
</table>
</FORM>
<br>Email body substitutions:
<br>[username] = User's username
<br>[details] = Details of purchase
<br>[sitename] = <?=$sitename?>
</td></tr>
</table>
</center>