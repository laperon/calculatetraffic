<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.35
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

if (isset($_GET['boardid']) && is_numeric($_GET['boardid'])) {

	$loadstats = lfmsql_query("Select * from `".$prefix."savedstats` where id=".$_GET['boardid']." limit 1");
	if (lfmsql_num_rows($loadstats) > 0) {
		
		$statsname = lfmsql_result($loadstats, 0, "statsname");
		$boardtype = lfmsql_result($loadstats, 0, "boardtype");
		$showid = lfmsql_result($loadstats, 0, "showid");
		$showfirst = lfmsql_result($loadstats, 0, "showfirst");
		$showlast = lfmsql_result($loadstats, 0, "showlast");
		$showuser = lfmsql_result($loadstats, 0, "showuser");
		$topnumber = lfmsql_result($loadstats, 0, "topnumber");
		
		$tablecolor = lfmsql_result($loadstats, 0, "tablecolor");
		$tablecolor2 = lfmsql_result($loadstats, 0, "tablecolor2");
		$bordercolor = lfmsql_result($loadstats, 0, "bordercolor");
		$textcolor = lfmsql_result($loadstats, 0, "textcolor");
		
		if ($tablecolor == "") { $tablecolor = "white"; }
		if ($tablecolor2 == "") { $tablecolor = "gray"; }
		if ($bordercolor == "") { $bordercolor = "black"; }
		if ($textcolor == "") { $textcolor = "black"; }

	} else {
		echo("Invalid Leaderboard");
		exit;
	}

} else {
	echo("Invalid Leaderboard");
	exit;
}

	// Update Settings
	
	if ($_GET['settings'] == "update") {
		
		$boardtype = $_POST['boardtype'];
		$tablecolor = $_POST['tablecolor'];
		$tablecolor2 = $_POST['tablecolor2'];
		$bordercolor = $_POST['bordercolor'];
		$textcolor = $_POST['textcolor'];
		$topnumber = $_POST['topnumber'];
		
		if(isset($_POST["showid"])){ $showid=1; } else { $showid=0; }
		if(isset($_POST["showfirst"])){ $showfirst=1; } else { $showfirst=0; }
		if(isset($_POST["showlast"])){ $showlast=1; } else { $showlast=0; }
		if(isset($_POST["showuser"])){ $showuser=1; } else { $showuser=0; }
		
		@lfmsql_query("Update `".$prefix."savedstats` set boardtype=$boardtype, tablecolor='$tablecolor', tablecolor2='$tablecolor2', bordercolor='$bordercolor', textcolor='$textcolor', showid=$showid, showfirst=$showfirst, showlast=$showlast, showuser=$showuser, topnumber=$topnumber where id=".$_GET['boardid']." limit 1");
	
	}
	
	$htmlcolors = array("white", "gray", "black", "red", "orange", "yellow", "green", "blue", "indigo", "violet");
	
	?>
<html>
<body>

<link href="styles.css" rel="stylesheet" type="text/css" />

<center>

<form action="leaderboard.php?boardid=<? echo($_GET['boardid']); ?>&settings=update" method="post">
<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	
<tr>
  <td colspan="2" align="center" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><? echo($statsname); ?> Leaderboard Settings</font> </strong></td>
</tr>

<tr>
  <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Leaderboard Type</font> </strong></td>
  <td class="formfield">
  <select name="boardtype">
  <option value=1<? if ($boardtype == 1) { echo(" selected"); } ?>>Most Clicks</option>
  <option value=2<? if ($boardtype == 2) { echo(" selected"); } ?>>Most Referrals</option>
  <option value=3<? if ($boardtype == 3) { echo(" selected"); } ?>>Highest Sales</option>
  <option value=4<? if ($boardtype == 4) { echo(" selected"); } ?>>Highest Number of Sales</option>
  </select>
  </td>
</tr>

<tr>
  <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Positions to Show</font> </strong></td>
  <td class="formfield">
  <input type="text" size="4" name="topnumber" value="<? echo($topnumber); ?>">
  </td>
</tr>

<tr>
  <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show on Leaderboard</font> </strong></td>
  <td class="formfield">
  User ID <input type="checkbox" name="showid"<? if ($showid == 1) { echo(" checked"); } ?>><br>
  User Name <input type="checkbox" name="showuser"<? if ($showuser == 1) { echo(" checked"); } ?>><br>
  First Name <input type="checkbox" name="showfirst"<? if ($showfirst == 1) { echo(" checked"); } ?>><br>
  Last Name <input type="checkbox" name="showlast"<? if ($showlast == 1) { echo(" checked"); } ?>><br>
  </td>
</tr>

<tr>
  <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Table Background 1</font> </strong></td>
  <td class="formfield">
  <select name="tablecolor">
  <?
  $htmlcolorlist = $htmlcolors;
  while (list ($key,$val) = @each ($htmlcolorlist)) {
  echo("<option value=\"$val\""); if ($tablecolor == $val) { echo(" selected"); } echo(">$val</option>");
  }
  ?>
  </select>
  </td>
</tr>

<tr>
  <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Table Background 2</font> </strong></td>
  <td class="formfield">
  <select name="tablecolor2">
  <?
  $htmlcolorlist = $htmlcolors;
  while (list ($key,$val) = @each ($htmlcolorlist)) {
  echo("<option value=\"$val\""); if ($tablecolor2 == $val) { echo(" selected"); } echo(">$val</option>");
  }
  ?>
  </select>
  </td>
</tr>

<tr>
  <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Border Color</font> </strong></td>
  <td class="formfield">
  <select name="bordercolor">
  <?
  $htmlcolorlist = $htmlcolors;
  while (list ($key,$val) = @each ($htmlcolorlist)) {
  echo("<option value=\"$val\""); if ($bordercolor == $val) { echo(" selected"); } echo(">$val</option>");
  }
  ?>
  </select>
  </td>
</tr>

<tr>
  <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Text Color</font> </strong></td>
  <td class="formfield">
  <select name="textcolor">
  <?
  $htmlcolorlist = $htmlcolors;
  while (list ($key,$val) = @each ($htmlcolorlist)) {
  echo("<option value=\"$val\""); if ($textcolor == $val) { echo(" selected"); } echo(">$val</option>");
  }
  ?>
  </select>
  </td>
</tr>

<tr>
  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Update Settings" /></td>
</tr>

</table>
</form>

<b>Leaderboard URL:</b><br>
<a target="_blank" href="http://<? echo($_SERVER["SERVER_NAME"]); ?>/view_leaderboard.php?boardid=<? echo($_GET['boardid']); ?>"><b>http://<? echo($_SERVER["SERVER_NAME"]); ?>/view_leaderboard.php?boardid=<? echo($_GET['boardid']); ?></b></a>
<br><br>

<b>Leaderboard HTML Code:</b><br>
<textarea wrap="soft" rows="4" cols="30"><script language="JavaScript" src="http://<? echo($_SERVER["SERVER_NAME"]); ?>/leaderboard.php?boardid=<? echo($_GET['boardid']); ?>"></script></textarea>
<br>

</center>
</body>
</html>