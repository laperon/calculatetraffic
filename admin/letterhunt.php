<?php

// Word Search Game
// �2011, 2015 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

// Update - LFMTE v2.04
// Promotes user when a word is deleted or edited

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

$errormess = "";

echo("<center><br><br>");

//Update Settings
if ($_GET['updatesettings'] == "yes" && is_numeric($_POST['showcount']) && is_numeric($_POST['daysreset']) && $_POST['showcount'] > 0 && $_POST['daysreset'] > 0) {

	if ($_POST['showcount'] > 99) {
	$errormess = "The letter display frequency must be less than 100.";
	} else {
	
	if (isset($_POST['enabled'])) {
		$enabled = 1;
	} else {
		$enabled = 0;
	}
	
	@lfmsql_query("Update ".$prefix."wssettings set value=".$enabled." where field='enabled' limit 1");
	@lfmsql_query("Update ".$prefix."wssettings set value='".$_POST['instructions']."' where field='instructions' limit 1");
	@lfmsql_query("Update ".$prefix."wssettings set value=".$_POST['showcount']." where field='showcount' limit 1");
	@lfmsql_query("Update ".$prefix."wssettings set value=".$_POST['random']." where field='random' limit 1");
	@lfmsql_query("Update ".$prefix."wssettings set value=".$_POST['claimpage']." where field='claimpage' limit 1");
	@lfmsql_query("Update ".$prefix."wssettings set value=".$_POST['daysreset']." where field='daysreset' limit 1");
	}
}

//Add A New Word
if ($_GET['addword'] == "yes" && is_numeric($_POST['newcvalue']) && $_POST['newcvalue'] > 0) {
	@lfmsql_query("Insert into ".$prefix."wswords (`phrase`, `cvalue`) values ('".$_POST['newphrase']."', ".$_POST['newcvalue'].")");
}

//Edit A Word
if ($_GET['editword'] == "yes" && is_numeric($_POST['cvalue']) && $_POST['cvalue'] > 0 && is_numeric($_GET['wordid']) && $_GET['wordid'] > 0) {
	@lfmsql_query("Update ".$prefix."wswords set phrase='".$_POST['phrase']."', cvalue=".$_POST['cvalue']." where id=".$_GET['wordid']." limit 1");

	// Promote Any Users To The Next Word
	$getnextword = lfmsql_query("Select id from ".$prefix."wswords where id>".$_GET['wordid']." order by id asc limit 1");
	if (lfmsql_num_rows($getnextword) > 0) {
		$nextword = lfmsql_result($getnextword, 0, "id");
	} else {
		// Get The First Word
		$nextword = lfmsql_result(lfmsql_query("Select id from ".$prefix."wswords order by id asc limit 1"), 0);
	}
	@lfmsql_query("Update ".$prefix."members set claimws=0, wsword=$nextword, wsletter=1 where wsword=".$_GET['wordid']);
	// End Promote Any Users To The Next Word
	
}

//Delete A Word
if ($_GET['delword'] == "yes" && is_numeric($_GET['wordid']) && $_GET['wordid'] > 0) {
	@lfmsql_query("Delete from ".$prefix."wswords where id=".$_GET['wordid']." limit 1");
	
	// Promote Any Users To The Next Word
	$getnextword = lfmsql_query("Select id from ".$prefix."wswords where id>".$_GET['wordid']." order by id asc limit 1");
	if (lfmsql_num_rows($getnextword) > 0) {
		$nextword = lfmsql_result($getnextword, 0, "id");
	} else {
		// Get The First Word
		$nextword = lfmsql_result(lfmsql_query("Select id from ".$prefix."wswords order by id asc limit 1"), 0);
	}
	@lfmsql_query("Update ".$prefix."members set claimws=0, wsword=$nextword, wsletter=1 where wsword=".$_GET['wordid']);
	// End Promote Any Users To The Next Word
	
}

//Begin Main Page

if ($errormess != "") {
	echo("<p><b>".$errormess."</b></p>");
}

$getvalue = lfmsql_query("Select value from ".$prefix."wssettings where field='enabled' limit 1");
$enabled = lfmsql_result($getvalue, 0, "value");

$getvalue = lfmsql_query("Select value from ".$prefix."wssettings where field='instructions' limit 1");
$instructions = lfmsql_result($getvalue, 0, "value");

$getvalue = lfmsql_query("Select value from ".$prefix."wssettings where field='showcount' limit 1");
$showcount = lfmsql_result($getvalue, 0, "value");

$getvalue = lfmsql_query("Select value from ".$prefix."wssettings where field='random' limit 1");
$random = lfmsql_result($getvalue, 0, "value");

$getvalue = lfmsql_query("Select value from ".$prefix."wssettings where field='claimpage' limit 1");
$claimpage = lfmsql_result($getvalue, 0, "value");

$getvalue = lfmsql_query("Select value from ".$prefix."wssettings where field='daysreset' limit 1");
$daysreset = lfmsql_result($getvalue, 0, "value");

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><p>&nbsp;</p><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td>
	<form action="admin.php?f=lhunt&updatesettings=yes" method="post">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Word Game Enabled</strong></font></td>
        <td colspan="2" class="formfield"><input name="enabled" type="checkbox" <? if($enabled == 1) { echo(" checked"); } ?>></td>
        <td align="center"><a href="#" title="Uncheck this box to completely disable the Word Search game.  Check the box to enable it again.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Word Game Instructions</strong></font></td>
        <td colspan="2" class="formfield"><textarea name="instructions" wrap="soft" rows="4" cols="30"><? echo($instructions); ?></textarea></td>
        <td align="center"><a href="#" title="The instructions for the Word Search game shown in the bottom surfbar.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Find Letter Frequency</strong></font></td>
        <td colspan="2" class="formfield">
        Find a letter <SELECT name=random><OPTION value=1<? if ($random == 1) { echo(" selected"); } ?>>Approximately every</OPTION><OPTION value=0<? if ($random == 0) { echo(" selected"); } ?>>Every</OPTION></SELECT> <input type="text" name="showcount" size="3" value="<? echo($showcount); ?>"> clicks.
        </td>
        <td align="center"><a href="#" title="How often surfers will find a letter while surfing.  Selecting 'Approximately Every' will make the letters appear more randomly.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>

      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Use The Claim Page</strong></font></td>
        <td colspan="2" class="formfield">
        <SELECT name=claimpage><OPTION value=1<? if ($claimpage == 1) { echo(" selected"); } ?>>Yes</OPTION><OPTION value=0<? if ($claimpage == 0) { echo(" selected"); } ?>>No</OPTION></SELECT>
        </td>
        <td align="center"><a href="#" title="When the claim page is enabled, members will need to click a 'Claim Letter' link to earn the letters.  This can encourage members to pay closer attention while surfing.  The claim page can be customized in the LFMTE templates.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>


      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Word Reset Frequency</font> </strong></td>
        <td colspan="2" class="formfield">Every <input name="daysreset" type="text" class="formfield" id="daysreset" value="<? echo($daysreset); ?>" size="4" /> days</td>
        <td align="center"><a href="#" title="How frequently the words will be reset and all members will start back on word one."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>


     <tr>
        <td align="left" nowrap="nowrap" class="button"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Update Settings" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>

<BR><BR>

<?

echo("<table style=\"border:thin solid #000 ; border-width: 1px;\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\" width=\"400\">
  <tr>
    <td colspan=\"4\" align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Word Search Phrases</font> </strong></td>
  </tr>
  
  <tr>
    <td align=\"center\" class=\"admintd\">Word/Phrase</td><td align=\"center\" class=\"admintd\">Credit Prize</td><td align=\"center\" class=\"admintd\">&nbsp;</td><td align=\"center\" class=\"admintd\">&nbsp;</td>
  </tr>
  ");
  
$getratios = lfmsql_query("Select * from ".$prefix."wswords order by id asc");
for ($j = 0; $j < lfmsql_num_rows($getratios); $j++) {
$wordid = lfmsql_result($getratios, $j, "id");
$phrase = lfmsql_result($getratios, $j, "phrase");
$cvalue = lfmsql_result($getratios, $j, "cvalue");
echo("<tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=lhunt&editword=yes&wordid=".$wordid."\">
<td><input type=text size=20 name=phrase value=\"".$phrase."\"></td>
<td><input type=text size=3 name=cvalue value=".$cvalue."></td>
<td><input type=submit value=\"Edit\"></td>
</form>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=lhunt&delword=yes&wordid=".$wordid."\">
<td><input type=submit value=\"Delete\"></td>
</form>
</tr>");
}

echo("<tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=lhunt&addword=yes\">
<td><input type=text size=20 name=newphrase value=\"\"></td>
<td><input type=text size=3 name=newcvalue value=\"0\"></td>
<td><input type=submit value=\"Add\"></td>
</form>
<td>&nbsp;</td>
</tr>");

echo("</table>");

?>

</center>

<p>&nbsp;</p>