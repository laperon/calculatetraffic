<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.32
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };
	
	//add the html headers
	if (isset($_POST[add_header_email])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('email')");
	}
	if (isset($_POST[add_header_review])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('review')");
	}
	if (isset($_POST[add_header_tweet])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('tweet')");
	}
	if (isset($_POST[add_header_facebooklike])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('facebooklike')");
	}
	if (isset($_POST[add_header_tellafriend])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('tellafriend')");
	}
	if (isset($_POST[add_header_emailsig])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('emailsig')");
	}
	if (isset($_POST[add_header_forumsig])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('forumsig')");
	}
	if (isset($_POST[add_header_topsponser])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('topsponser')");
	}
	if (isset($_POST[add_header_squeezepage])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('squeezepage')");
	}
	if (isset($_POST[add_header_splashpage])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('splashpage')");
	}
	if (isset($_POST[add_header_safelist])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('safelist')");
	}
	if (isset($_POST[add_header_popup])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('popup')");
	}
	if (isset($_POST[add_header_thankyou])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('thankyou')");
	}
	if (isset($_POST[add_header_banner])){
		lfmsql_query ("INSERT INTO ".$prefix."promotion_headers (type) VALUES ('banner')");
	}
	
	//remove the html headers
	if (isset($_POST[remove_header_email])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='email'");
	}
	if (isset($_POST[remove_header_review])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='review'");
	}
	if (isset($_POST[remove_header_tweet])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='tweet'");
	}
	if (isset($_POST[remove_header_facebooklike])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='facebooklike'");
	}
	if (isset($_POST[remove_header_tellafriend])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='tellafriend'");
	}
	if (isset($_POST[remove_header_emailsig])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='emailsig'");
	}
	if (isset($_POST[remove_header_forumsig])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='forumsig'");
	}
	if (isset($_POST[remove_header_topsponser])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='topsponser'");
	}
	if (isset($_POST[remove_header_squeezepage])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='squeezepage'");
	}
	if (isset($_POST[remove_header_splashpage])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='splashpage'");
	}
	if (isset($_POST[remove_header_safelist])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='safelist'");
	}
	if (isset($_POST[remove_header_popup])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='popup'");
	}
	if (isset($_POST[remove_header_thankyou])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='thankyou'");
	}
	if (isset($_POST[remove_header_banner])){
		lfmsql_query ("DELETE FROM ".$prefix."promotion_headers WHERE type ='banner'");
	}
	
	
	//update the html headers
	if (isset($_POST[update_header_email])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data = '".$_POST[header_html_email]."' WHERE type = 'email'") or die (lfmsql_error());
	}
	if (isset($_POST[update_header_review])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_review]."' WHERE type = 'review'");
	}
	if (isset($_POST[update_header_tellafriend])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_tellafriend]."' WHERE type = 'tellafriend'");
	}
	if (isset($_POST[update_header_tweet])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_tweet]."' WHERE type = 'tweet'");
	}
	if (isset($_POST[update_header_facebooklike])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_facebooklike]."' WHERE type = 'facebooklike'");
	}
	if (isset($_POST[update_header_emailsig])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_emailsig]."' WHERE type = 'emailsig'");
	}
	if (isset($_POST[update_header_forumsig])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_forumsig]."' WHERE type = 'forumsig'");
	}
	if (isset($_POST[update_header_topsponser])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_topsponser]."' WHERE type = 'topsponser'");
	}
	if (isset($_POST[update_header_squeezepage])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_squeezepage]."' WHERE type = 'squeezepage'");
	}
	if (isset($_POST[update_header_splashpage])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_splashpage]."' WHERE type = 'splashpage'");
	}
	if (isset($_POST[update_header_safelist])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_safelist]."' WHERE type = 'safelist'");
	}
	if (isset($_POST[update_header_popup])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_popup]."' WHERE type = 'popup'");
	}
	if (isset($_POST[update_header_thankyou])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_thankyou]."' WHERE type = 'thankyou'");
	}
	if (isset($_POST[update_header_banner])){
		lfmsql_query ("UPDATE ".$prefix."promotion_headers SET template_data ='".$_POST[header_html_banner]."' WHERE type = 'banner'");
	}
	
	
	
	
    if($_POST["Submit"] == "Upload" && filesize($_FILES['imagepath']['tmp_name']) > 0)
    {
		$bsize=0;
		$banner='';
        $banner=$_FILES['imagepath']['tmp_name'];
        $bsize = filesize($_FILES['imagepath']['tmp_name']);

		
		// Check to make sure we have an image file
		if(!getimagesize($banner))
		{
			echo "File is not a valid image!";
			exit;
		}
    
		$fp=fopen($banner, "r");
	    $lfmsqlBanner = fread($fp, filesize($_FILES['imagepath']['tmp_name']));
		fclose($fp);
		$encoded = chunk_split(base64_encode($lfmsqlBanner)); 
	    lfmsql_query("INSERT INTO ".$prefix."banners SET banner='$encoded'") or die(lfmsql_error());
    }
  
    if($_POST["Submit"] == "Update Image" && filesize($_FILES['imagepath']['tmp_name']) > 0)
    {
		$bsize=0;
		$banner='';
        $banner=$_FILES['imagepath']['tmp_name'];
        $bsize = filesize($_FILES['imagepath']['tmp_name']);
		$img_id=$_POST["img_id"];
		
		// Check to make sure we have an image file
		if(!getimagesize($banner))
		{
			echo "File is not a valid image!";
			exit;
		}
    
		if($img_id > 0)
		{
			$fp=fopen($banner, "r");
	    	$lfmsqlBanner = fread($fp, filesize($_FILES['imagepath']['tmp_name']));
			fclose($fp);
			$encoded = chunk_split(base64_encode($lfmsqlBanner)); 
	    	lfmsql_query("UPDATE ".$prefix."banners SET banner='$encoded' WHERE id=".$img_id) or die(lfmsql_error());
		}
    }
    

	// Add an email
	if(trim($_POST["Submit"]) == "Add An Email")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, subject, content) VALUES('email','New Subject','New Email')") or die(lfmsql_error());
	}
	
	// Add a tweet
	if(trim($_POST["Submit"]) == "Add A Tweet")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, subject, content) VALUES('tweet','','Enter Tweet Here')") or die(lfmsql_error());
	}
	
		// Add a facebook like button
	if(trim($_POST["Submit"]) == "Add A Facebook Like Button")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, subject, content) VALUES('facebooklike','','#REFURL#')") or die(lfmsql_error());
	}


	// Add a review
	if(trim($_POST["Submit"]) == "Add A Review")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content) VALUES('review','New Review')") or die(lfmsql_error());
	}

	// Add a tell-a-friend 	
	if(trim($_POST["Submit"]) == "Add A Tell-A-Friend")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, subject, content) VALUES('tellafriend','New Subject','New Tell-A-Friend')") or die(lfmsql_error());
	}

	// Add an Email Sig 	
	if(trim($_POST["Submit"]) == "Add An Email Sig")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content) VALUES('emailsig', 'New Email Sig')") or die(lfmsql_error());
	}

	// Add a Forum Sig 	
	if(trim($_POST["Submit"]) == "Add A Forum Sig")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content) VALUES('forumsig', 'New Forum Sig')") or die(lfmsql_error());
	}

	// Add a top sponsor ad 	
	if(trim($_POST["Submit"]) == "Add A Top Sponsor Ad")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content) VALUES('topsponsor', 'New Top Sponsor Ad')") or die(lfmsql_error());
	}

	// Add a squeeze page 	
	if(trim($_POST["Submit"]) == "Add A Squeeze Page")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content) VALUES('squeezepage', 'New Squeeze Page')") or die(lfmsql_error());
	}
	
	// Add a splash page 	
	if(trim($_POST["Submit"]) == "Add A Splash Page")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content) VALUES('splashpage', 'New Splash Page')") or die(lfmsql_error());
	}

	// Add a safelist email 	
	if(trim($_POST["Submit"]) == "Add A Safelist Email")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, subject, content) VALUES('safelist', 'New Safelist Subject', 'New Safelist Email')") or die(lfmsql_error());
	}

	// Add a popup 	
	if(trim($_POST["Submit"]) == "Add A Popup")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content) VALUES('popup', 'New Popup')") or die(lfmsql_error());
	}

	// Add a thank you page 	
	if(trim($_POST["Submit"]) == "Add A Thank-You Page")
	{
		@lfmsql_query("INSERT INTO ".$prefix."promotion(type, content,subject) VALUES('thankyou', 'New Thank-you Page','New Thank-you Page')") or die(lfmsql_error());
	}


//function to find out if the html headers is turned on, and output either the edit box or a button to turn it on
function check_for_header_do_output ($type, $prefix) {
	$header_enabled = "no";
	//check if the header is turned on
	$html_headers = lfmsql_query ("SELECT * FROM ".$prefix."promotion_headers WHERE type = '".$type."'");
	while ($html_header = lfmsql_fetch_array($html_headers)){
		$header_enabled = "yes";
		$template_data = $html_header[template_data];
	}
	if ($header_enabled == "yes"){
		//output the text area and button to populate the header
		print '<tr>
		<td colspan="4">HTML HEADER:</td>
		</tr>
		<tr><td colspan="4"><textarea  name="header_html_'.$type.'"  id="header_html_'.$type.'"  cols="48" rows="8">'.$template_data.'</textarea></td>
		</tr>
		<tr>
		<td colspan="4"><input name="update_header_'.$type.'" type="submit" class="smallbutton" id="update_header_'.$type.'" value="Update HTML Header" /><input name="remove_header_'.$type.'" type="submit" class="smallbutton" id="remove_header_'.$type.'" value="Remove HTML Header" /></td>';
	} else {
		//output the button to turn on the header
		print '<td colspan="4"><input name="add_header_'.$type.'" type="submit" class="smallbutton" id="add_header_'.$type.'" value="Add An HTML Header" /><td>';
	}
	
}


?>
<center><font color="#FF0000"><strong><?=$msg;?></strong></font></center>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr bordercolor="#333333">
    <td>
  
  
    <center>
    <div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  <tr>
    <td align="center"><div class="lfm_infobox_heading">Affiliate Toolbox</div></td>
  </tr>
  
  <tr>
    <td align="left"><div class="lfm_descr">
    	
    	<p>The Affiliate Toolbox is where you can add promotional items for your affiliates to use to promote your site.  These can include e-mails, forum signatures, squeeze pages, banner ads, and more.</p>
    	
    	In the text of each promo tool, you can use the macro #REFURL# to insert the member's affiliate link.  This should be used as the "Click Here" action link in your e-mails, splash pages, etc.
    	
    </div></td>
  </tr>
      
</table>
</div>
<p>&nbsp;</p>
</center>
  
  
  </td></tr>
  <tr>
    <td>
<form name="frm" method="POST" action="admin.php?f=lm">
	<table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 1</strong> - Email your subscribers</font></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(email, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add An Email" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? $emres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='email'");
	while($emrow=@lfmsql_fetch_array($emres))
	{ ?>
      <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<strong>Subject:</strong> <?=$emrow["subject"];?></font> </td>
      </tr>
      <tr>
        <td align="left" valign="top" style="border: solid thin; size: 1px; color:#CCCCCC">
		<font color="#000000"><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$emrow["content"];?></textarea>
		</font></td>
      </tr>
      <tr>
        <td align="right" nowrap="nowrap" bgcolor="#9DBDFF">
		<input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$emrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$emrow["id"];?>);">		</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
	<? } ?>
    </table>
</form>
    </td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
</table>  

<form name="frm" method="POST" action="admin.php?f=lm">
	<table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
     <tr class="admintd">
        <td class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 2</strong> - Send Tweet to Twitter Followers</font></td>
      </tr><tr> 
	  	<?=check_for_header_do_output(tweet, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Tweet" /></td>
      </tr>
       
      <tr>
        <td>&nbsp;</td>
      </tr>
<? $emres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='tweet'");
	while($emrow=@lfmsql_fetch_array($emres))
	{ ?>
      <tr>
        <td align="left" valign="top" style="border: solid thin; size: 1px; color:#CCCCCC">
		<font color="#000000"><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$emrow["content"];?></textarea>
		</font></td>
      </tr>
      <tr>
        <td align="right" nowrap="nowrap" bgcolor="#9DBDFF">
		<input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$emrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$emrow["id"];?>);">		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
	<? } ?>
    </table>
</form>
    </td>
  </tr>
  
</table>  
<form name="frm" method="POST" action="admin.php?f=lm">
	<table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
     <tr class="admintd">
        <td class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 3</strong> - Share this site on Facebook</font></td>
      </tr><tr> 
	  	<?=check_for_header_do_output(facebooklike, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Facebook Like Button" /></td>
      </tr>
       
      <tr>
        <td>&nbsp;</td>
      </tr>
<? $emres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='facebooklike'");
	while($emrow=@lfmsql_fetch_array($emres))
	{ ?>
      <tr>
        <td align="left" valign="top" style="border: solid thin; size: 1px; color:#CCCCCC">
		<font color="#000000"><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$emrow["content"];?></textarea>
		</font></td>
      </tr>
      <tr>
        <td align="right" nowrap="nowrap" bgcolor="#9DBDFF">
		<input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$emrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$emrow["id"];?>);">		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
	<? } ?>
    </table>
</form>

<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 4 </strong> - Add these reviews to your site </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(review, $prefix);  ?>      
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Review" /></td>
      </tr>
<? $revres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='review'");
	while($revrow=@lfmsql_fetch_array($revres))
	{ ?>
      <tr>
        <td style="border: solid thin; size: 1px; color:#CCCCCC"><font color="#000000">
		<textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$revrow["content"];?></textarea></font></td>
      </tr>
      <tr>
        <td align="right" nowrap="nowrap" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$revrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$revrow["id"];?>);">		</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><div align="center"></div></td>
  </tr>
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="3" cellspacing="0">
      <tr class="admintd">
        <td nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><span class="style5"><strong>Step 5 </strong> - Tell a friend</span></font></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(tellafriend, $prefix);  ?>
      </tr>
      <tr>
        <td nowrap="nowrap"><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Tell-A-Friend" /></td>
      </tr>
<? $tafres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='tellafriend'");
	while($tafrow=@lfmsql_fetch_array($tafres))
	{ ?>
      <tr>
        <td nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<strong>Subject:</strong> <?=$tafrow["subject"];?></font></td>
      </tr>
      <tr>
        <td align="left" valign="top"><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$tafrow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" valign="top" bgcolor="#9DBDFF">		
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$tafrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$tafrow["id"];?>);">
</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 6 </strong> - Change your email signature to this </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(emailsig, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add An Email Sig" /></td>
      </tr>
<? $sigres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='emailsig'");
	while($sigrow=@lfmsql_fetch_array($sigres))
	{ ?>
      <tr>
        <td><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$sigrow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$sigrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$sigrow["id"];?>);">
	   </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 7 </strong> - Change your forum sig to this </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(forumsig, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Forum Sig" /></td>
      </tr>
<? $fsigres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='forumsig'");
	while($fsigrow=@lfmsql_fetch_array($fsigres))
	{ ?>
      <tr>
        <td><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$fsigrow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$fsigrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$fsigrow["id"];?>);">
		</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>  

<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 8 </strong> - Use this top sponsor ad </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(topsponser, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Top Sponsor Ad" /></td>
      </tr>
<? $tsres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='topsponsor'");
	while($tsrow=@lfmsql_fetch_array($tsres))
	{ ?>
      <tr>
        <td><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$tsrow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$tsrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$tsrow["id"];?>);">
		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>	  
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>

<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 9 </strong> - Use this squeeze page to get visitors to your affiliate URL </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(squeezepage, $prefix);  ?>
      </tr>
      <tr>
        <td align="left"><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Squeeze Page" /></td>
      </tr>
<? $spres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='squeezepage'");
	while($sprow=@lfmsql_fetch_array($spres))
	{ ?>
  
      <tr>
        <td align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>Title:</b> <?=$sprow["subject"];?></font></td>
      </tr>
      <tr>
        <td align="center"><textarea cols="60" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$sprow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$sprow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$sprow["id"];?>);">
		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>


<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 10 </strong> - Promote a splash page to get visitors to your affiliate URL </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(splashpage, $prefix);  ?>
      </tr>
      <tr>
        <td align="left"><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Splash Page" /></td>
      </tr>
<? $spres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='splashpage'");
	while($sprow=@lfmsql_fetch_array($spres))
	{ ?>
  
      <tr>
        <td align="center"><textarea cols="60" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$sprow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$sprow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$sprow["id"];?>);">
		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>

<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 11 </strong> - use this Safe list email to get visitors <br />
          to your sqeeze page
          or affiliate URL </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(safelist, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Safelist Email" /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? $slres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='safelist'");
	while($slrow=@lfmsql_fetch_array($slres))
	{ ?>
      <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<strong>Subject:</strong> <?=$slrow["subject"];?></font> </td>
      </tr>
      <tr>
        <td><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$slrow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$slrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$slrow["id"];?>);">
		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>

<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 12 </strong> - Add this popup to your site </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(popup, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Popup" /></td>
      </tr>
<? $popres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='popup'");
	while($poprow=@lfmsql_fetch_array($popres))
	{ ?>
      <tr>
        <td><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$poprow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$poprow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$poprow["id"];?>);">
		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>

<form name="frm" method="POST" action="admin.php?f=lm">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr class="admintd">
        <td><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 13 </strong> - Add this html to your thank you page <br />
          or log out page
          on your site. </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(thankyou, $prefix);  ?>
      </tr>
      <tr>
        <td><input name="Submit" type="submit" class="smallbutton" id="Submit" value="Add A Thank-You Page" /></td>
      </tr>
<? $tyres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='thankyou'");
	while($tyrow=@lfmsql_fetch_array($tyres))
	{ ?>
	  <tr>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<strong>Title:</strong> <?=$tyrow["subject"];?></font> </td>
      </tr>	
      <tr>
        <td><textarea cols="48" rows="8" onClick="javascript:alert('Please click the EDIT button');"><?=$tyrow["content"];?></textarea></td>
      </tr>
      <tr>
        <td align="right" bgcolor="#9DBDFF">
		  <input name="Delete" type="button" class="smallbutton" value="Delete" onClick="javascript:delPromo(<?=$tyrow["id"];?>);">
        <input name="Submit" type="button" class="smallbutton" value="Edit" onClick="javascript:editPromo(<?=$tyrow["id"];?>);">
		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
<? } ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</form>

<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td>
	<form name="frm" enctype="multipart/form-data" method="POST" action="admin.php?f=lm">
	<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<table width="400" border="0" align="center" cellpadding="2" cellspacing="0">
      <tr class="admintd">
        <td colspan="2" align="left"><span class="style5"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Step 14 </strong> - Add these banners to your site </font></span></td>
      </tr>
      <tr> 
	  	<?=check_for_header_do_output(banner, $prefix);  ?>
      </tr>
      <tr>
        <td colspan="2" style="border:none;" align="center"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Upload a new banner/image </font></strong></td>
        </tr>
      <tr>
        <td colspan="2" style="border:none;" align="center"><input name="imagepath" type="file" id="imagepath" size="32" /></td>
      </tr>
      <tr>
        <td colspan="2" style="border:none;" align="center"><input type="submit" name="Submit" value="Upload" /></td>
        </tr>
    </table>
    </form>
	</td>
  </tr>
  <tr><td align="center">
	<? 
        $bresult=@lfmsql_query("SELECT * FROM ".$prefix."banners") or die("Can't Perform Query");
        While($brow=@lfmsql_fetch_object($bresult)) {
     ?> 
     <p>&nbsp;</p>
	<form name="frm" enctype="multipart/form-data" method="POST" action="admin.php?f=lm">
	<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	<input type="hidden" name="img_id" value="<?=$brow->id;?>" />
    <table border="0" style="border:solid;border-width:1px;border-color:#333;" align="center">
      <tr>
	<td align="center" colspan="2">
		<img src="getimg.php?id=<?=$brow->id;?>"><br><br>	
		</td>
	</tr>
      <tr>
        <td align="center">
        <input name="imagepath" type="file" id="imagepath" size="32" />
        </td>
        <td align="center" valign="top" nowrap="nowrap"><span style="border:solid thin #666666;">
          <input type="submit" name="Submit" value="Update Image" />
        </span></td>
      </tr>
      <tr>
        <td colspan="2" align="center"><span style="border:solid thin #666666;">
          <input type="button" onclick="javascript:delLink(<?=$brow->id;?>)" value="Delete This Image" />
        </span></td>
        </tr>
	</table>
	</form>
	<? } ?>
  </td></tr>
</table>
</body>
</html>
