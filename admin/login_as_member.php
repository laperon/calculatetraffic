<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright ©2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };


 $authquery= mysql_query ("SELECT * FROM ".$prefix."members 
							WHERE Id = '".$_REQUEST[userid]."'") 
 							or die (mysql_error());
 $authrow = mysql_fetch_array ($authquery);
	 $_SESSION["uid"] = $authrow["username"];
	$_SESSION["user_password"]=$authrow["password"];
	$_SESSION["userid"]=$authrow["Id"];
	setcookie("lfmpr3s5gy", session_name(), time()+3600);
	$authenticated=1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Logging in as member...</title>
</head>

<body>

<p align="center">Logging you in as the member...</p>
<script language="javascript">
window.location = "../members.php";
</script>


</body>
</html>