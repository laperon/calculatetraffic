<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.32
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

?>

<br><br>

<font size="3"><b>#FIRSTNAME#</b> - Member's first name</font><br>
<font size="3"><b>#LASTNAME#</b> - Member's last name</font><br>
<font size="3"><b>#USERNAME#</b> - Member's login username</font><br>
<font size="3"><b>#AFFILIATEID#</b> - Member's affiliate ID number</font><br>
<font size="3"><b>#REFURL#</b> - Member's referral URL/affiliate link</font><br><br>

<font size="3"><b>#MEMBERPHOTO#</b> - Member's Gravatar photo</font><br>
<font size="3"><b>#MEMBERPHOTOURL#</b> - Member's Gravatar photo URL</font><br><br>

<font size="3"><b>#SPONSORID#</b> - Member's referrer's User ID number</font><br>
<font size="3"><b>#SPONSORNAME#</b> - Member's referrer's name</font><br>
<font size="3"><b>#SPONSORPHOTO#</b> - Member's referrer's Gravatar photo</font><br>
<font size="3"><b>#SPONSORPHOTOURL#</b> - Member's referral's Gravatar photo URL</font><br><br>

<font size="3"><b>#NUMREFS#</b> - Member's total number of referrals</font><br>
<font size="3"><b>#BALANCE#</b> - Member's unpaid commissions balance</font><br>
<font size="3"><b>#CREDITS#</b> - Member's unassigned credits balance</font><br>
<font size="3"><b>#BANNERIMPS#</b> - Member's unassigned banner impressions</font><br>
<font size="3"><b>#TEXTIMPS#</b> - Member's unassigned text impressions</font><br><br>

<font size="3"><b>#SITENAME#</b> - The name of your site</font><br>
<font size="3"><b>#IPNid#</b> - Inserts a payment button.  Replace "id" with a Sales Package ID</font><br>
<font size="3"><b>#VERIFY#</b> - The verification link for use in welcome e-mails</font><br><br>

<font size="3"><b>#CLICKSTODAY#</b> - The number of clicks the member has made today</font><br>
<font size="3"><b>#CREDITSTODAY#</b> - The number of credits the member has earned from surfing today</font><br>
<font size="3"><b>#CLICKSYESTERDAY#</b> - The number of clicks the member made yesterday</font><br>
<font size="3"><b>#CREDITSYESTERDAY#</b> - The number of credits the member earned from surfing yesterday</font><br><br>

<font size="3"><b>#HITSTODAY#</b> - The number of hits that have been delivered to the member's sites today</font><br>
<font size="3"><b>#HITSYESTERDAY#</b> - The number of hits that have been delivered to the member's sites yesterday</font><br>
<font size="3"><b>#BANNERSTODAY#</b> - The number of hits that have been delivered to the member's banners today</font><br>
<font size="3"><b>#BANNERSYESTERDAY#</b> - The number of hits that have been delivered to the member's sites yesterday</font><br>
<font size="3"><b>#TEXTSTODAY#</b> - The number of hits that have been delivered to the member's text ads today</font><br>
<font size="3"><b>#TEXTSYESTERDAY#</b> - The number of hits that have been delivered to the member's text ads yesterday</font><br><br>

<font size="3"><b>#BANNERAD#</b> - Shows a random member banner ad</font><br>
<font size="3"><b>#TEXTAD#</b> - Shows a random member text ad</font><br><br>