<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.18
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";




	// path to admin dir without trailing slash
	if(isset($_SERVER['PATH_TRANSLATED']))
	{
		$fullpath = $_SERVER['PATH_TRANSLATED'];
		$mail_path=substr($fullpath,0,strlen($fullpath)-15);
		$inc_path=$mail_path."inc";
	}
	else
	{
		$fullpath = $_SERVER['SCRIPT_FILENAME'];
		$mail_path=substr($fullpath,0,strlen($fullpath)-15);
		$inc_path=$mail_path."inc";
	}


	if(trim($_SERVER['argv'][1]) == "bg")
	{
		$fullpath= $_ENV["PWD"];
		$mail_path=substr($fullpath,0,strlen($fullpath)-5);
		$inc_path=$mail_path."inc";

		include $inc_path."/funcs.php";
		AddLog("Opening include: ".$inc_path."/funcs.php");
		include $inc_path."/config.php";
		AddLog("Opening include: ".$inc_path."/config.php");
		AddLog("PWD: ".$_ENV["PWD"]);

	}

	// Use the domain from replyaddress setting when connecting to remote mail servers
	$mres=@mysql_query("SELECT replyaddress FROM ".$prefix."settings");
	$mrow=@mysql_fetch_array($mres);
	$emaildomain=substr($mrow["replyaddress"],strpos($mrow["replyaddress"],"@")+1);

if(!isset($_SERVER['argv'][1]) && !isset($_SESSION["adminid"]))
{
	AddLog("Exiting: Session ID not set! (IP:".$_SERVER['REMOTE_ADDR'].")");
	exit;
}

function validate_email($email,$emaildomain){
  $ms_resp="";
  $mailparts=explode("@",$email);
  $hostname = $mailparts[1];
  // validate email address syntax
  $exp = "^[a-z\'0-9]+([._-][a-z\'0-9]+)*@([a-z0-9]+([._-][a-z0-9]+))+$";
  $b_valid_syntax=eregi($exp, $email);
  // get mx addresses by getmxrr
  $b_mx_avail=getmxrr( $hostname, $mx_records, $mx_weight );
  $b_server_found=0;
  if($b_valid_syntax && $b_mx_avail){
    // copy mx records and weight into array $mxs
    $mxs=array();
    for($i=0;$i<count($mx_records);$i++){
      $mxs[$mx_weight[$i]]=$mx_records[$i];
    }
    // sort array mxs to get servers with highest prio
    ksort ($mxs, SORT_NUMERIC );
    reset ($mxs);
    while (list ($mx_weight, $mx_host) = each ($mxs) ) {
      if($b_server_found == 0){
        //try connection on port 25
        $fp = @fsockopen($mx_host,25, $errno, $errstr, 2);
        if($fp){
		  // say HELO to mailserver
          $ms_resp.=send_command($fp, "RSET");
		  AddLog("ms_resp : ".$ms_resp);
          if(substr( $ms_resp, 0, 3) == "250" || substr( $ms_resp, 0, 3) == "554" || substr( $ms_resp, 0, 3) == "220")
		  {
            $b_server_found=1;
			AddLog("Valid server found for : ".$email);
          	// quit mail server connection
		  }
          	$ms_resp.=send_command($fp, "QUIT");
	        fclose($fp);
        }
      }
   }
 }
 return $b_server_found;
}

function send_command($fp, $out){
 fwrite($fp, $out . "\r\n");
 return get_data($fp);
}

function get_data($fp){
 $s="";
 stream_set_timeout($fp, 2);
 for($i=0;$i<2;$i++)
   $s.=fgets($fp, 1024);
 return $s;
}

// support windows platforms
if (!function_exists ('getmxrr') ) {
 function getmxrr($hostname, &$mxhosts, &$mxweight) {
   if (!is_array ($mxhosts) ) {
     $mxhosts = array ();
   }
   if (!empty ($hostname) ) {
     $output = "";
     @exec ("nslookup.exe -type=MX $hostname.", $output);
     $imx=-1;
     foreach ($output as $line) {
       $imx++;
       $parts = "";
       if (preg_match ("/^$hostname\tMX preference = ([0-9]+), mail exchanger = (.*)$/", $line, $parts) ) {
         $mxweight[$imx] = $parts[1];
         $mxhosts[$imx] = $parts[2];
       }
     }
     return ($imx!=-1);
   }
   return false;
 }
}

//
// START MAIN PROCESSING HERE
//
mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname) or die( "Unable to select database");

// Check emailmsg table for unsent messages
$res=@mysql_query("SELECT * FROM ".$prefix."emailmsg");

// We have mail waiting to be sent
if(mysql_num_rows($res) > 0)
{
	// Use the domain from replyaddress setting when connecting to remote mail servers
	$mres=@mysql_query("SELECT replyaddress FROM ".$prefix."settings");
	$mrow=@mysql_fetch_array($mres);
	$emaildomain=substr($mrow["replyaddress"],strpos($mrow["replyaddress"],"@")+1);

	// Get the SMTP server from settings table
	$smres=@mysql_query("SELECT smtp_server FROM ".$prefix."settings");
	if(!$smres)
	{
		// No setting found - default to localhost
		$smtp_server = "localhost";
	}
	else
	{
		$smrow=@mysql_fetch_array($smres);
		$smtp_server=$smrow["smtp_server"];
	}
	
	// Get reply address
	$rres=@mysql_query("SELECT replyaddress,bounceaddress,sitename FROM ".$prefix."settings");
	{
		$rrow=@mysql_fetch_array($rres);
		$from=$rrow["replyaddress"];
		$bounceaddress=$rrow["bounceaddress"];
		$sitename=$rrow["sitename"];
	}
	
	// Set message header
	$headers = 'From: '.$sitename.' <'.$from.'>';
	$mailFrom = "-f$bounceaddress";

	// Cycle through any messages that are waiting to be sent	
	while($row=@mysql_fetch_array($res))
	{
		// Find out if it is message to all (0) or a message to selected members (1)
		// Also check status. 0=Unsent, 1=sending, 2=complete
		if($row["msgtype"] == 0 && $row["status"] == 0)
		{
			// This is a message to all members
			
			// Update the status
			$updqry="UPDATE ".$prefix."emailmsg SET status=1 WHERE Id=".$row["Id"];
			mysql_query($updqry);

			// Send messages to every email in the members table if newsletter=1
			$ures=@mysql_query("SELECT Id,email FROM ".$prefix."members WHERE newsletter=1");
			while($urow=@mysql_fetch_array($ures))
			{
				// Check for bounce
				// $valid=validate_email(trim($urow["email"]),$emaildomain);
				
				// Bad email address
				if($valid == 10)
				{
					$email=$urow["email"];
					AddLog("Bounce detected: ".$email);
					$id=$urow["Id"];
					mysql_query("UPDATE ".$prefix."members SET mailbounce=mailbounce+1 WHERE email='$email' AND Id=$id");
				}
				else
				{
					//AddLog("Sending: ".$urow["email"]);
					$subject=translate_site_tags($row["subject"]);
					$subject=translate_user_tags($subject,$urow["email"]);
				
					$msgbody=translate_site_tags($row["body"]);
					$msgbody=translate_user_tags($msgbody,$urow["email"]);
					$spath="http://".$_SERVER['HTTP_HOST'].htmlspecialchars($_SERVER['REQUEST_URI'],ENT_QUOTES);
					$lastslash=strrpos($spath,"admin/");
					$unsubscribe=substr($spath,0,$lastslash)."unsubscribe.php?user=".$urow["Id"]."&email=".$urow["email"];
					$msgbody.="\n\n\nYou are subscribed to receive updates with the e-mail address ".$urow["email"].". \nClick on the following link or paste it into your web browser to unsubscribe:\n\n$unsubscribe";
				
					if(trim($_SERVER['argv'][1]) == "bg")
					{
						mail($urow["email"],$subject,$msgbody,$headers,$mailFrom);
					}
					else
					{
						mail($urow["email"],$subject,$msgbody,$headers,$mailFrom);
					}
				}			
			}

			// Once the emails are all sent we can delete the message
			$dqry="DELETE FROM ".$prefix."emailmsg WHERE Id=".$row["Id"];
			@mysql_query($dqry);

		}
		else
		if($row["msgtype"] == 1 && $row["status"] == 0)
		{
			// This is a message to selected recipients

			// Update the status
			$updqry="UPDATE ".$prefix."emailmsg SET status=1 WHERE Id=".$row["Id"];
			mysql_query($updqry);

			// Send message to selected users which are temporarily stored in emailrcpt table
			$qry="SELECT * FROM ".$prefix."emailrcpt WHERE msgid=".$row["Id"];
			$ures=@mysql_query($qry);
			while($urow=@mysql_fetch_array($ures))
			{
				$idres=@mysql_query("SELECT id FROM ".$prefix."members WHERE email='".$urow["address"]."'");
				$memberid=@mysql_result($idres,0);
				
				// Check for bounce
				// $valid=validate_email(trim($urow["address"]),$emaildomain);
				
				// Bad email address
				if($valid == 10)
				{
					$email=$urow["address"];
					AddLog("Bounce detected: ".$email);
					mysql_query("UPDATE ".$prefix."members SET mailbounce=mailbounce+1 WHERE email='$email'");
				}
				else
				{
					//AddLog("Sending: ".$urow["address"]);
					$subject=translate_site_tags($row["subject"]);
					$subject=translate_user_tags($subject,$urow["address"]);
				
					$msgbody=translate_site_tags($row["body"]);
					$msgbody=translate_user_tags($msgbody,$urow["address"]);
					$spath="http://".$_SERVER['HTTP_HOST'].htmlspecialchars($_SERVER['REQUEST_URI'],ENT_QUOTES);
					$lastslash=strrpos($spath,"admin/");
					$unsubscribe=substr($spath,0,$lastslash)."unsubscribe.php?user=".$memberid."&email=".$urow["address"];
					$msgbody.="\n\n\nYou are subscribed to receive updates with the e-mail address ".$urow["address"].". \nClick on the following link or paste it into your web browser to unsubscribe:\n\n$unsubscribe";
				
					if(trim($_SERVER['argv'][1]) == "bg")
					{
						AddLog("Background: sending to ".$urow["address"]." about ".$subject." (using $smtp_server)");
						mail($urow["address"],$subject,$msgbody,$headers,$mailFrom);
					}
					else
					{
						AddLog("Foreground: sending to ".$urow["address"]." about ".$subject." ");
						mail($urow["address"],$subject,$msgbody,$headers,$mailFrom);
					}
				}				

				// Once this message has been sent - delete the recipient from the emailrcpt table
				$drqry="DELETE FROM ".$prefix."emailrcpt WHERE msgid=".$row["Id"]." AND address='".$urow["address"]."'";
				@mysql_query($drqry);
			}
			
			// Once the emails are all sent we can delete the message
			$dqry="DELETE FROM ".$prefix."emailmsg WHERE Id=".$row["Id"];
			@mysql_query($dqry);
		}
	
	}
}
?>