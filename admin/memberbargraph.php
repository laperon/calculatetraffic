<?php

// VisuGraph 1.0
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

include("visugraph.php");

// Prepare Graph

$starttime = $_GET['starttime'];
$endtime = $_GET['endtime'];
$charttype = $_GET['charttype'];
$memberid = $_GET['memberid'];

if (!is_numeric($memberid) || $memberid < 1) { exit; }

//Chart Type 1 = Clicks
//Chart Type 2 = Hits Delivered
//Chart Type 3 = Banners Delivered
//Chart Type 4 = Text Ads Delivered
//Chart Type 5 = New Refs
if ($charttype == "" || !is_numeric($charttype) || $charttype < 1 || $charttype > 5) {
	// Set to default
	$charttype = 1;
}

if ($charttype == 1) {
	$searchvalue = "clicks";
} elseif ($charttype == 2) {
	$searchvalue = "hitsdeliver";
} elseif ($charttype == 3) {
	$searchvalue = "bandeliver";
} elseif ($charttype == 4) {
	$searchvalue = "textdeliver";
} else {
	$searchvalue = "newrefs";
}


if (!is_numeric($starttime) || $starttime < 1) { exit; }
if (!is_numeric($endtime) || $endtime < 1) { exit; }

$startdate = date("Y-m-d",$starttime);
$enddate = date("Y-m-d",$endtime);

$numdays = countdays($startdate, $enddate);
$numelems = 0;

if ($numdays > 40) {
	//Switch to monthly view

	$currdate = $startdate;
	
	while ($currdate <= $enddate) {
	
	$selecteddate = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));;
	
	$selectedmonth = date("m", $selecteddate);
	$selectedyear = date("Y", $selecteddate);
	$daya = $selectedyear."-".$selectedmonth."-"."01";
	$dayb = $selectedyear."-".$selectedmonth."-"."31";
	$monthname = date("M", $selecteddate);
		
	if ($monthname == "Jan") {
		$monthname = "Jan
".$selectedyear;
	}
	
	$getmonth = mysql_query("Select SUM(".$searchvalue.") from ".$prefix."surflogs where date>='".$daya."' and date<='".$dayb."' and userid=".$memberid);
	
	$currentsum = mysql_result($getmonth, 0);
	if ($currentsum == NULL) {
		$currentsum = 0;
	}

	$statsum[$numelems] = $currentsum;
	$xname[$numelems] = $monthname;
	$numelems = $numelems+1;
	
	$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 month"));
	
	}

} else {

	$numelems = $numdays;

	$getdays = mysql_query("Select ".$searchvalue.", date from ".$prefix."surflogs where date>='".$startdate."' and date<='".$enddate."' and userid=".$memberid." order by date asc");
	
	if (mysql_num_rows($getdays) > 0) {
		for ($i = 0; $i < mysql_num_rows($getdays); $i++) {
			$thedate = mysql_result($getdays, $i, "date");
			$datelist[$i] = $thedate;
			$sumlist[$i] = mysql_result($getdays, $i, $searchvalue);
		}
	} else {
		// Set Empty Date Array
		$datelist[0] = 0;
	}
	
	$currdate = $startdate;
	$i = 0;
	while ($currdate <= $enddate) {
	
		$dateposition = array_search($currdate, $datelist);
		if ($dateposition === false) {
			//No Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = 0;
			$xname[$i] = date("d", $thetime);
		} else {
			//Enter Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = $sumlist[$dateposition];
			$xname[$i] = date("d", $thetime);
		}
	
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
		$i++;
	}
}

if ($numelems > 7) {
	$showlabels = 0;
} else {
	$showlabels = 1;
}

if (($numelems > 15) && ($numdays <= 40)) {
	//Modify x names
	$fontsize = 9;
	$xnamecount = ceil($numelems/15);
	for ($i = 1; $i <= $numelems; $i++) {
		if (($i % $xnamecount) > 0) {
			$xname[$i] = '';
		}
	}

} elseif (($numelems > 12) && ($numdays > 40)) {
	//Modify x names
	$fontsize = 9;
	$xnamecount = ceil($numelems/12);
	for ($i = 1; $i <= $numelems; $i++) {
		if (($i % $xnamecount) > 0) {
			if (strpos($xname[$i], 'Jan') === false) {
				$xname[$i] = '';
			} else {
				$xname[$i] = str_replace('Jan', '', $xname[$i]);
			}
		}
	}

} else {

	$fontsize = 0;
}

// End Prepare Graph

if ($numelems < 1) {
	// No Data To Display
	exit;
}

//Show Graph
$mybars = array($statsum);
$colors = array('#333333');

$transparency = 10;

$w = 760; // WIDTH OF GRAPH IMAGE
$h = 500; // HEIGHT OF GRAPH IMAGE
	
drawbargraph($xname, $mybars, $colors, $showlabels, $fontsize, $transparency, $w, $h);

?>