<?php

// VisuGraph 1.0
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

$errormess = "";
$graphurl = "";
$canzoomin = "no";
$canzoomout = "no";
$memberid = $_GET['memberid'];

if (isset($_POST['memberid'])) {
	$memberid = $_POST['memberid'];
}

if (!is_numeric($memberid) || $memberid < 1) {
	echo("Invalid Member ID");
	exit;
}

?>

<link href="styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol.css">

<center>

<?

// Show New Graph
if ($_GET['drawgraph'] == "yes") {

	$starttime = $_GET['starttime'];
	$endtime = $_GET['endtime'];
	$datatype = $_GET['datatype'];
	$graphtype = $_GET['graphtype'];
	
	// Show Weekly If Date Range Is Too Large Or Small
	$startdate = date("Y-m-d", $starttime);
	$enddate = date("Y-m-d", $endtime);
	if (($startdate < "2007-01-01") || $startdate==$enddate) {
		$startdate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")." + 7 days ago"));
		$enddate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")));
		$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
		$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;
	}
	
	if (isset($_POST['sdate'])) {
	
		$datatype = $_POST['datatype'];
		$graphtype = $_POST['graphtype'];
		$starttime = mktime(0,0,0,substr($_POST['sdate'],5,2),substr($_POST['sdate'],8),substr($_POST['sdate'],0,4));
		$endtime = mktime(0,0,0,substr($_POST['edate'],5,2),substr($_POST['edate'],8),substr($_POST['edate'],0,4))+86399;
	}
	
	if ($endtime > $starttime) {
	
		$startdate = date("Y-m-d",$starttime);
		$enddate = date("Y-m-d",$endtime);
		$numdays = countdays($startdate, $enddate);
		

		if ($numdays > 0) {
		
			//Generate Image URL
			if ($graphtype == 1) {
				$graphurl = "memberlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype."&memberid=".$memberid;
			} elseif ($graphtype == 2) {
				$graphurl = "memberbargraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype."&memberid=".$memberid;
			} else {
				//Draw Text Chart
				include "memberstatschart.php"; 		
			}
			
			//Check Zoom Levels
			if ($numdays > 7) {
				$canzoomin = "yes";
			} else {
				$canzoomin = "no";
			}
		
		} else {
			$errormess = "There is no data for this date range.";
		}
	} else {
		$errormess = "The 'Start' date must be before the 'End' date.";
	}
} elseif (($_GET['clickzoom'] == "yes") && is_numeric($_GET['starttime']) && is_numeric($_GET['endtime']) && is_numeric($_POST['x'])) {

// Zoom In Current Graph

	$starttime = $_GET['starttime'];
	$endtime = $_GET['endtime'];
	$datatype = $_GET['datatype'];
	$graphtype = $_GET['graphtype'];
	$clickedpixel = $_POST['x'] - 60;
	
	$timedistance = $endtime - $starttime;
	$distancefourths = round($timedistance/4);
	
	// Zoom The Start And End Times
	
	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
	$daysingraph = countdays($startdate, $enddate);
	
	$width = 660;
	$pixelsperday = $width/$daysingraph;
	
	$daystomove = ($clickedpixel/$pixelsperday)/2;
	
	$secondstomove = $daystomove*86400;
	
	$starttime = $starttime + $secondstomove;
	$endtime = $starttime + ($distancefourths*2);

	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
	
	$numdays = countdays($startdate, $enddate);
	
	if ($numdays > 0) {
	
		//Generate Image URL
		if ($graphtype == 1) {
			$graphurl = "memberlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype."&memberid=".$memberid;
		} else {
			$graphurl = "memberbargraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype."&memberid=".$memberid;
		}
		
		//Check Zoom Levels
		if ($numdays > 7) {
			$canzoomin = "yes";
		} else {
			$canzoomin = "no";
		}
	
	} else {
	//	$errormess = "There is no data for this date range.";
	}
} elseif ((($_GET['changezoom'] == "in") || ($_GET['changezoom'] == "out")) && is_numeric($_GET['starttime']) && is_numeric($_GET['endtime'])) {

// Change Zoom

	$starttime = $_GET['starttime'];
	$endtime = $_GET['endtime'];
	$datatype = $_GET['datatype'];
	$graphtype = $_GET['graphtype'];
	
	$timedistance = $endtime - $starttime;
	$distancefourths = round($timedistance/4);
	
	// Zoom The Start And End Times
	if ($_GET['changezoom'] == "in") {
		$starttime = $starttime + $distancefourths;
		$endtime = $starttime + ($distancefourths*2);
	} else {
		$checkdate = date("Y-m-d",$starttime - $distancefourths);
		$starttime = $starttime - ($distancefourths*2);
		$endtime = $starttime + ($distancefourths*8);
	}
	
	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
	
	$numdays = countdays($startdate, $enddate);
	
	if ($numdays > 0) {
	
		//Generate Image URL
		if ($graphtype == 1) {
			$graphurl = "memberlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype."&memberid=".$memberid;
		} else {
			$graphurl = "memberbargraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype."&memberid=".$memberid;
		}
		
		//Check Zoom Levels
		if ($numdays > 7) {
			$canzoomin = "yes";
		} else {
			$canzoomin = "no";
		}
	
	} else {
		$errormess = "There is no data for this date range.";
	}

} else {
	//Show the weekly hits graph
	$currentdate = date("Y-m-d");
	$startdate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));
	$enddate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
	$datatype = 1;
	$graphtype = 1;
	
	$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
	$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;
	
	$numdays = countdays($startdate, $enddate);
	
	if ($numdays > 0) {
	
		//Generate Image URL
		$graphurl = "memberlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype."&memberid=".$memberid;
		
		//Check Zoom Levels
		if ($numdays > 7) {
			$canzoomin = "yes";
		} else {
			$canzoomin = "no";
		}
	}
}

if ($starttime>0 && $endtime>0) {
	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
} else {
	$currentdate = date("Y-m-d");
	$startdate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));
	$enddate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
}

if ($errormess != "") {
echo("<p>".$errormess."</p>");
}

?>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td>
	<form action="membergraphs.php?drawgraph=yes" method="post">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	
  <tr>
    <td colspan="4" align="center" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Stats of Member #<? echo($memberid); ?></font> </strong></td>
  </tr>
	
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Start Date: </strong></font></td>
        <td colspan="2" class="formfield">
        <input name="sdate" type id="DPC_sdate_YYYY-MM-DD" value="<? echo($startdate); ?>" width="80" text>
        </td>
        <td align="center"><a href="#" title="The starting date that will be shown in the graph.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
 
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>End Date: </strong></font></td>
        <td colspan="2" class="formfield">
        <input name="edate" type id="DPC_edate_YYYY-MM-DD" value="<? echo($enddate); ?>" width="80" text>
        </td>
        <td align="center"><a href="#" title="The ending date that will be shown in the graph.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Data Type</font> </strong></td>
        <td colspan="2" class="formfield">
<select name="datatype">
<option value=1<? if ($datatype == 1) { echo(" selected"); } ?>>Number of Clicks</option>
<option value=2<? if ($datatype == 2) { echo(" selected"); } ?>>Hits Received</option>
<option value=3<? if ($datatype == 3) { echo(" selected"); } ?>>Banner Imps Received</option>
<option value=4<? if ($datatype == 4) { echo(" selected"); } ?>>Text Ad Imps Received</option>
<option value=5<? if ($datatype == 5) { echo(" selected"); } ?>>New Referrals</option>
</select>
        </td>
        <td align="center"><a href="#" title="The type of data you want to view."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Graph Type</font> </strong></td>
        <td colspan="2" class="formfield">
<select name="graphtype">
<option value=1<? if ($graphtype == 1) { echo(" selected"); } ?>>Line Graph</option>
<option value=2<? if ($graphtype == 2) { echo(" selected"); } ?>>Bar Graph</option>
<option value=3<? if ($graphtype == 3) { echo(" selected"); } ?>>Text Chart</option>
</select>
        </td>
        <td align="center"><a href="#" title="The type of graph that will be displayed."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <input type="hidden" name="memberid" value="<? echo($memberid); ?>">
      
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Show Member Stats" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>

<?

if ($graphurl != "") {

	if ($canzoomin == "yes") {
		echo("<table border=0 cellpadding=0 cellspacing=0 width=760 height=500 id=\"graphimg\"><tr><td><form style=\"margin:0px\" action=\"membergraphs.php?clickzoom=yes&starttime=".$starttime."&endtime=".$endtime."&datatype=".$datatype."&memberid=".$memberid."&graphtype=".$graphtype."#graphimg\" method=\"post\">
		<input type=\"image\" src=\"".$graphurl."\">
		</form></td></tr></table>");
		
		echo("<a href=\"membergraphs.php?changezoom=in&starttime=".$starttime."&endtime=".$endtime."&datatype=".$datatype."&memberid=".$memberid."&graphtype=".$graphtype."#graphimg\">Zoom In</a><br><br>");
		
	} else {
		echo("<table border=0 cellpadding=0 cellspacing=0 width=760 height=500 id=\"graphimg\"><tr><td><img border=\"0\" src=\"".$graphurl."\"></td></tr></table>");
	}
	
	echo("<a href=\"membergraphs.php?changezoom=out&starttime=".$starttime."&endtime=".$endtime."&datatype=".$datatype."&memberid=".$memberid."&graphtype=".$graphtype."#graphimg\">Zoom Out</a><br>");

} elseif ($charthtml != "") {
	echo($charthtml);
}

?>
<br>

<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  <tr>
    <td align="left" valign="top" bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The member stats graphs are updated daily at midnight.</font></p>
      
      </font></p>

      <p>&nbsp;</p></td>
  </tr>
</table>

</body>
</html>

<?
exit;

function countdays($firstdate, $lastdate) {
	$currdate = $firstdate;
	$daycount = 0;
	while ($currdate <= $lastdate) {
		$daycount++;
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
	}
	return $daycount;
}
?>