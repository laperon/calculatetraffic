<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

//Get Show Type From TE Dashboard

$currenttime = time();

$nowtime = $currenttime-120;
$hourtime = $currenttime-3600;
//$daytime = $currenttime-86400;
$daytime_from = strtotime(date('Y-m-d') . " 00:00:00");
$daytime_to = strtotime(date('Y-m-d') . " 23:59:59");
//$weektime = $currenttime-604800;
$weektime_from = firstDayOf('week')->format('U');
$weektime_to = lastDayOf('week')->format('U');

$currentdate = date("Y-m-d");
$yesterdate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
$weekdate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));

if ($_GET['show'] == "surfing") {
	$showtype = "surfing";
	$timequery = "lastclick >= ".$nowtime;

} elseif ($_GET['show'] == "surfhour") {
	$showtype = "surfhour";
	$timequery = "lastclick >= ".$hourtime;

} elseif ($_GET['show'] == "surftoday") {
	$showtype = "surftoday";
	$timequery = "lastclick >= " . $daytime_from . " and lastclick <= " . $daytime_to;

} elseif ($_GET['show'] == "surfweek") {
	$showtype = "surfweek";
	$timequery = "lastclick >= ".$weektime_from . " and lastclick <= " . $weektime_to;
    
} elseif ($_GET['show'] == "newtoday") {
	$showtype = "newtoday";
	$timequery = "joindate <= '".$currentdate." 23:59:59' and joindate >= '".$currentdate."'";
	
} elseif ($_GET['show'] == "newyester") {
	$showtype = "newyester";
	$timequery = "joindate <= '".$yesterdate." 23:59:59' and joindate >= '".$yesterdate."'";
	
} elseif ($_GET['show'] == "newweek") {
	$showtype = "newweek";
	$timequery = "joindate <= '".$currentdate." 23:59:59' and joindate >= '".$weekdate."'";
	
} elseif ($_GET['show'] == "subscribed") {
	$showtype = "subscribed";
	$timequery = "status='Active' AND newsletter=1";
	
} elseif ($_GET['show'] == "unverified") {
	$showtype = "unverified";
	$timequery = "status='Unverified'";
	
} elseif ($_GET['show'] == "unsubscribed") {
	$showtype = "unsubscribed";
	$timequery = "status='Active' AND newsletter=0";
	
} elseif ($_GET['show'] == "suspended") {
	$showtype = "suspended";
	$timequery = "status='Suspended'";
	
} elseif ($_GET['show'] == "bounced") {
	$showtype = "bounced";
	$timequery = "status='Active' AND bouncemode=3";
	
} else {
	$showtype = "";
	$timequery = "";
}

//Get Search From TE Dashboard

if ($_GET['tdb'] == "yes") {

$userid = $_POST['userid'];
$username = $_POST['username'];
$email = $_POST['email'];
$referrer = $_POST['referrer'];
$ipaddress = $_POST['ipaddress'];

if ($userid != "") {

	$searchfield = "Id";
	$searchtext = $userid;
	
} elseif ($username != "") {

	$searchfield = "username";
	$searchtext = $username;
	
} elseif ($email != "") {

	$searchfield = "email";
	$searchtext = $email;
	
} elseif ($referrer != "") {

	$searchfield = "refid";
	$searchtext = $referrer;
	
} elseif ($ipaddress != "") {

	$searchfield = "ipadd";
	$searchtext = $ipaddress;
	
}

} elseif (isset($_POST["searchtext"])) {
            $searchfield=trim($_POST["searchfield"]);
            $searchtext=trim($_POST["searchtext"]);
            
} elseif (($_GET['searchfield'] != "") && ($_GET['searchtext'] != "")) {
            $searchfield=trim($_GET["searchfield"]);
            $searchtext=trim($_GET["searchtext"]);
}

//LFMTE Sorting

if (($_GET["sortby"] != "") && ($_GET["sortorder"] != "")) {
$sortby = $_GET["sortby"];
$sortorder = $_GET["sortorder"];

//Set Orders
if ($sortby == "Id" && $sortorder == "ASC") {
$idorder = "DESC";
} else {
$idorder = "ASC";
}
if ($sortby == "firstname" && $sortorder == "ASC") {
$firstorder = "DESC";
} else {
$firstorder = "ASC";
}
if ($sortby == "lastname" && $sortorder == "ASC") {
$lastorder = "DESC";
} else {
$lastorder = "ASC";
}
if ($sortby == "username" && $sortorder == "ASC") {
$userorder = "DESC";
} else {
$userorder = "ASC";
}
if ($sortby == "mtype" && $sortorder == "ASC") {
$mtypeorder = "DESC";
} else {
$mtypeorder = "ASC";
}
if ($sortby == "credits" && $sortorder == "DESC") {
$creditsorder = "ASC";
} else {
$creditsorder = "DESC";
}
if ($sortby == "_clickstoday" && $sortorder == "DESC") {
$clicksorder = "ASC";
} else {
$clicksorder = "DESC";
}
if ($sortby == "tempcomm" && $sortorder == "DESC") {
$commsorder = "ASC";
} else {
$commsorder = "DESC";
}
if ($sortby == "tempcommp" && $sortorder == "DESC") {
$commsporder = "ASC";
} else {
$commsporder = "DESC";
}
if ($sortby == "temprefs" && $sortorder == "DESC") {
$refsorder = "ASC";
} else {
$refsorder = "DESC";
}

if ($sortby == "email" && $sortorder == "DESC") {
$emailorder = "ASC";
} else {
$emailorder = "DESC";
}
if ($sortby == "paypal_email" && $sortorder == "DESC") {
$ppemailorder = "ASC";
} else {
$ppemailorder = "DESC";
}
if ($sortby == "joindate" && $sortorder == "DESC") {
$joindateorder = "ASC";
} else {
$joindateorder = "DESC";
}
if ($sortby == "lastlogin" && $sortorder == "DESC") {
$logindateorder = "ASC";
} else {
$logindateorder = "DESC";
}
if ($sortby == "bannerimps" && $sortorder == "DESC") {
$bannersorder = "ASC";
} else {
$bannersorder = "DESC";
}
if ($sortby == "textimps" && $sortorder == "DESC") {
$textsorder = "ASC";
} else {
$textsorder = "DESC";
}
if ($sortby == "clicksyesterday" && $sortorder == "DESC") {
$clicksyesterdayorder = "ASC";
} else {
$clicksyesterdayorder = "DESC";
}

} else {
$sortby = "Id";
$sortorder = "ASC";

$idorder = "ASC";
$firstorder = "ASC";
$lastorder = "ASC";
$userorder = "ASC";
$mtypeorder = "ASC";
$creditsorder = "DESC";
$clicksorder = "DESC";
$commsorder = "DESC";
$commsporder = "DESC";
$refsorder = "DESC";

$emailorder = "ASC";
$ppemailorder = "ASC";
$joindateorder = "DESC";
$logindateorder = "DESC";
$bannersorder = "DESC";
$textsorder = "DESC";
$clicksyesterdayorder = "DESC";

}

//End LFMTE Sorting

// Determine Which Fields To Show

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showgrav'");
if (lfmsql_num_rows($getfield) > 0) {
	$showgrav = lfmsql_result($getfield, 0, "value");
} else {
	$showgrav = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showid'");
if (lfmsql_num_rows($getfield) > 0) {
	$showid = lfmsql_result($getfield, 0, "value");
} else {
	$showid = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showfirst'");
if (lfmsql_num_rows($getfield) > 0) {
	$showfirst = lfmsql_result($getfield, 0, "value");
} else {
	$showfirst = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showlast'");
if (lfmsql_num_rows($getfield) > 0) {
	$showlast = lfmsql_result($getfield, 0, "value");
} else {
	$showlast = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showusername'");
if (lfmsql_num_rows($getfield) > 0) {
	$showusername = lfmsql_result($getfield, 0, "value");
} else {
	$showusername = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showemail'");
if (lfmsql_num_rows($getfield) > 0) {
	$showemail = lfmsql_result($getfield, 0, "value");
} else {
	$showemail = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showpaypal'");
if (lfmsql_num_rows($getfield) > 0) {
	$showpaypal = lfmsql_result($getfield, 0, "value");
} else {
	$showpaypal = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showmtype'");
if (lfmsql_num_rows($getfield) > 0) {
	$showmtype = lfmsql_result($getfield, 0, "value");
} else {
	$showmtype = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showjoined'");
if (lfmsql_num_rows($getfield) > 0) {
	$showjoined = lfmsql_result($getfield, 0, "value");
} else {
	$showjoined = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showlogin'");
if (lfmsql_num_rows($getfield) > 0) {
	$showlogin = lfmsql_result($getfield, 0, "value");
} else {
	$showlogin = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showcredits'");
if (lfmsql_num_rows($getfield) > 0) {
	$showcredits = lfmsql_result($getfield, 0, "value");
} else {
	$showcredits = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showbanners'");
if (lfmsql_num_rows($getfield) > 0) {
	$showbanners = lfmsql_result($getfield, 0, "value");
} else {
	$showbanners = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showtexts'");
if (lfmsql_num_rows($getfield) > 0) {
	$showtexts = lfmsql_result($getfield, 0, "value");
} else {
	$showtexts = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showcomm'");
if (lfmsql_num_rows($getfield) > 0) {
	$showcomm = lfmsql_result($getfield, 0, "value");
} else {
	$showcomm = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showcommp'");
if (lfmsql_num_rows($getfield) > 0) {
	$showcommp = lfmsql_result($getfield, 0, "value");
} else {
	$showcommp = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showsales'");
if (lfmsql_num_rows($getfield) > 0) {
	$showsales = lfmsql_result($getfield, 0, "value");
} else {
	$showsales = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showrefs'");
if (lfmsql_num_rows($getfield) > 0) {
	$showrefs = lfmsql_result($getfield, 0, "value");
} else {
	$showrefs = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showclicks'");
if (lfmsql_num_rows($getfield) > 0) {
	$showclicks = lfmsql_result($getfield, 0, "value");
} else {
	$showclicks = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showclicksyester'");
if (lfmsql_num_rows($getfield) > 0) {
	$showclicksyester = lfmsql_result($getfield, 0, "value");
} else {
	$showclicksyester = 0;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showstatus'");
if (lfmsql_num_rows($getfield) > 0) {
	$showstatus = lfmsql_result($getfield, 0, "value");
} else {
	$showstatus = 1;
}

$getfield = lfmsql_query("SELECT value FROM ".$prefix."memgrid WHERE field='showcustom'");
if (lfmsql_num_rows($getfield) > 0) {
	$showcustom = lfmsql_result($getfield, 0, "value");
	$showcustom = explode(",", $showcustom);
} else {
	$showcustom = array();
}

// End Determine Which Fields To Show

if ($_GET['sortcomm'] == "go") {
	// Calculate commission totals for sorting
	$mqry="SELECT Id FROM ".$prefix."members WHERE `status` != 'Pending'";
	$mres=@lfmsql_query($mqry);
	while($mrow=@lfmsql_fetch_array($mres)) {
		$commission=0;
		$commres=@lfmsql_query("SELECT SUM(commission) as ctotal FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND ".$prefix."sales.status IS NULL");
		if($commres) {
			$commrow=@lfmsql_fetch_array($commres);
			$commission=$commrow["ctotal"];
			if(!is_numeric($commission) || $commission <= 0) { $commission="0.00"; }
		} else {
			$commission="0.00";
		}
		
		@lfmsql_query("Update ".$prefix."members set tempcomm=".$commission." where Id=".$mrow["Id"]." limit 1");
	}
}

if ($_GET['sortcommp'] == "go") {
	// Calculate paid commission totals for sorting
	$mqry="SELECT Id FROM ".$prefix."members WHERE `status` != 'Pending'";
	$mres=@lfmsql_query($mqry);
	while($mrow=@lfmsql_fetch_array($mres)) {
		$commissionpaid=0;
		$commres=@lfmsql_query("SELECT SUM(commission) as ctotal FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND commission>0 AND status = 'P'");
		if($commres) {
			$commrow=@lfmsql_fetch_array($commres);
			$commissionpaid=$commrow["ctotal"];
			if(!is_numeric($commissionpaid) || $commissionpaid <= 0) { $commissionpaid="0.00"; }
		} else {
			$commissionpaid="0.00";
		}
		
		@lfmsql_query("Update ".$prefix."members set tempcommp=".$commissionpaid." where Id=".$mrow["Id"]." limit 1");
	}
}

if ($_GET['sortrefs'] == "go") {
	// Calculate refs for sorting
	$mqry="SELECT Id FROM ".$prefix."members WHERE `status` != 'Pending'";
	$mres=@lfmsql_query($mqry);
	while($mrow=@lfmsql_fetch_array($mres)) {
		$reftotal=0;
		$commres=@lfmsql_query("SELECT COUNT(*) as reftotal FROM ".$prefix."members WHERE refid=".$mrow["Id"]);
		if($commres) {
			$commrow=@lfmsql_fetch_array($commres);
			$reftotal=$commrow["reftotal"];
			if(!is_numeric($reftotal) || $reftotal <= 0) { $reftotal=0; }
		} else {
			$reftotal=0;
		}
		
		@lfmsql_query("Update ".$prefix."members set temprefs=".$reftotal." where Id=".$mrow["Id"]." limit 1");
	}
}

// Operations affecting member records

// Suspend members according to check boxes
if($_POST["Submit"] == "Suspend Selected")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$sqry="UPDATE ".$prefix."members SET status='Suspended' WHERE Id=".$val;
		@lfmsql_query($sqry) or die("Error: Unable to suspend multiple members!");
		}
	}
}

// Un-Suspend members according to check boxes
if($_POST["Submit"] == "Set Selected Active")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$uqry="UPDATE ".$prefix."members SET status='Active', disableddate=NULL WHERE Id=".$val;
		@lfmsql_query($uqry) or die("Error: Unable to un-suspend multiple members!");
		}
	}
}

// Delete multiple members according to check boxes
if($_POST["MDelete"] == "Yes - Delete")
{
	if(isset($_POST["idarray"]))
	{
		while (list ($key,$val) = @each ($_POST["idarray"])) {
		$dqry="DELETE FROM ".$prefix."members WHERE Id=".$val;
		lfmsql_query($dqry) or die("Error: Unable to delete members!");
		
		lfmsql_query("DELETE FROM ".$prefix."customvals WHERE userid=".$val);
		}
	}
}

// Ban members according to check boxes
if($_POST["Submit"] == "Ban Selected")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$getmem = lfmsql_query("SELECT email, lastip, signupip from ".$prefix."members WHERE Id=".$val);
		$emailtoban = lfmsql_result($getmem, 0, "email");
		$ipban1 = lfmsql_result($getmem, 0, "lastip");
		$ipban2 = lfmsql_result($getmem, 0, "signupip");

		// Ban Email Addresses
		if ($emailtoban != "") {
		$checkban = lfmsql_query("Select id from `".$prefix."banemails` where banned='$emailtoban' limit 1");
		if (lfmsql_num_rows($checkban) == 0) {
		$sqry = "Insert into `".$prefix."banemails` (banned) values ('$emailtoban')";
		@lfmsql_query($sqry) or die("Error: Unable to ban emails");
		@lfmsql_query("Update ".$prefix."members set status='Suspended' where email LIKE '%".$emailtoban."%'");
		}
		}
		
		// Ban Last IP Addresses
		if(strpos($ipban1, ".") !== false) {
		$checkban = lfmsql_query("Select id from `".$prefix."banips` where banned='$ipban1' limit 1");
		if (lfmsql_num_rows($checkban) == 0) {
		$sqry = "Insert into `".$prefix."banips` (banned) values ('$ipban1')";
		@lfmsql_query($sqry) or die("Error: Unable to ban last ip addresses");
		@lfmsql_query("Update ".$prefix."members set status='Suspended' where lastip LIKE '%".$ipban1."%' OR signupip LIKE '%".$ipban1."%'");
		}
		}
		
		// Ban Signup IP Addresses
		if ($ipban1 != $ipban2) {
		if(strpos($ipban2, ".") !== false) {
		$checkban = lfmsql_query("Select id from `".$prefix."banips` where banned='$ipban2' limit 1");
		if (lfmsql_num_rows($checkban) == 0) {
		$sqry = "Insert into `".$prefix."banips` (banned) values ('$ipban2')";
		@lfmsql_query($sqry) or die("Error: Unable to ban signup ip addresses");
		@lfmsql_query("Update ".$prefix."members set status='Suspended' where lastip LIKE '%".$ipban2."%' OR signupip LIKE '%".$ipban2."%'");
		}
		}
		}
		
		}
	}
}

?>
<script src="../inc/jsfuncs.js" type="text/javascript"></script>
<?
	// Hide the search form when displaying mass delete confirmation
	if(!isset($_POST["Delchecked"]))
	{

?>
<link href="styles.css" rel="stylesheet" type="text/css" />

<script language="javascript">
	function customizeGrid()
	{
		var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=550,height=550"; 
	 
		var URL = "memgrid.php"; 
		popup = window.open(URL,"MemGridPopup",windowprops);	
	}
</script>

<br />
<center>

<!-- Start Search Box -->
<table border="0" cellpadding="0" cellspacing="1" width="500">
<form name="searchfrm" method="post" action="admin.php?f=mm&show=<?=$showtype?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<tr>
<td><select class="form-control" name="searchfield">
      <option <? if($searchfield=="") { echo "selected=\"selected\""; } ?>>Search By...</option>
      <option <? if($searchfield=="Id") { echo "selected=\"selected\""; } ?> value="Id">User ID</option>
      <option <? if($searchfield=="firstname") { echo "selected=\"selected\""; } ?> value="firstname">First Name</option>
      <option <? if($searchfield=="lastname") { echo "selected=\"selected\""; } ?> value="lastname">Last Name</option>
      <option <? if($searchfield=="username") { echo "selected=\"selected\""; } ?> value="username">Username</option>
      <option <? if($searchfield=="email") { echo "selected=\"selected\""; } ?> value="email">Email</option>
      <option <? if($searchfield=="refid") { echo "selected=\"selected\""; } ?> value="refid">Referrer</option>
      <option <? if($searchfield=="ipadd") { echo "selected=\"selected\""; } ?> value="ipadd">IP Address</option>
      <option <? if($searchfield=="status") { echo "selected=\"selected\""; } ?> value="status">Status</option>
      <option <? if($searchfield=="mtype") { echo "selected=\"selected\""; } ?> value="mtype">Member Level</option>
<?
// Custom Search Fields
$getfields = lfmsql_query("SELECT id, name FROM `".$prefix."customfields` WHERE searchable=1 ORDER BY rank ASC");
if (lfmsql_num_rows($getfields) > 0) {
	for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
		$fieldid = lfmsql_result($getfields, $i, "id");
		$fieldname = lfmsql_result($getfields, $i, "name");
		$fieldselval = "custom_".$fieldid;
		echo('      <option'); if($searchfield==$fieldselval) { echo " selected=\"selected\""; } echo(' value="'.$fieldselval.'">'.$fieldname.'</option>
		');
	}
}
// End Custom Search Fields
?>
                </select></td>
    <td><input class="form-control" name="searchtext" type="text" id="searchtext" value="<?=$searchtext;?>"/></td>
    <td><input type="submit" name="Submit" value="Search" /></td>
</tr>
</form>
</table>
<br>
<!-- End Search Box -->

<a href="javascript:customizeGrid()"><input type="button" name="customizegrid" value="Customize Member Grid"></a>
<br><br>


<?
}
	// Get the starting record for member browse
	if(!isset($_GET["limitStart"]))
	{
		$st=0;
	}
	else
	{
		$st=$_GET["limitStart"];
	}

	// Get the total member count for member browse
	$cqry="SELECT COUNT(*) as mcount FROM ".$prefix."members";
	// Check if there is search criteria
	if($searchtext != "")
	{

		// Handle membertype search
		if($searchfield == "mtype" && !is_numeric($searchtext))
		{
			$mtsqry="SELECT mtid FROM ".$prefix."membertypes WHERE accname='".$searchtext."'";
			$mtsres=@lfmsql_query($mtsqry);
			$mtsrow=@lfmsql_fetch_array($mtsres);
			$searchtext=$mtsrow["mtid"];
		}
		
		// Handle referral search
		if ($searchfield == "refid" && !is_numeric($searchtext)) {

			$getrefid = lfmsql_query("Select Id from ".$prefix."members where username='".$searchtext."' or email='".$searchtext."'");
			if (lfmsql_num_rows($getrefid) > 0) {
				$searchtext = lfmsql_result($getrefid, 0, "Id");
			}		
		}
		
		// Handle ip address search
		if ($searchfield == "ipadd") {
			$searchfield = "lastip='".$searchtext."' or signupip";
		}
		
		// Handle email address search
		if ($searchfield == "email") {
			$searchtext = "%".$searchtext."%";
		}
		
		// Handle custom field search
		if (stristr($searchfield,"custom_")) {
			$splitsearchfield = explode("_", $searchfield);
			$searchfieldnum = $splitsearchfield[1];
			if (is_numeric($searchfieldnum) && $searchfieldnum > 0) {
				$getuserids = lfmsql_query("SELECT userid FROM ".$prefix."customvals WHERE fieldid='".$searchfieldnum."' AND fieldvalue LIKE '%".$searchtext."%'");
				if (lfmsql_num_rows($getuserids) > 0) {
					$memidlist = "";
					for ($i = 0; $i < lfmsql_num_rows($getuserids); $i++) {
						$memid = lfmsql_result($getuserids, $i, "userid");
						if ($i > 0) { $memidlist .= " OR "; }
						$memidlist .= "Id=".$memid;
					}
					$cqry .= " WHERE (".$memidlist.")";
				} else {
					$memid = 0;
					$cqry .= " WHERE (Id=".$memid.")";
				}
			} else {
				$memid = 0;
				$cqry .= " WHERE (Id=".$memid.")";
			}
			
			if ($timequery != "") {
				$cqry .= " and ".$timequery;
			}
		// End Handle custom field search
		} elseif ($timequery != "") {
			$cqry.=" WHERE ".$searchfield." = '".$searchtext."' and ".$timequery;
		} else {
			$cqry.=" WHERE ".$searchfield." = '".$searchtext."'";
		}
	
	} else {
		if ($timequery != "") {
			$cqry.=" WHERE ".$timequery;
		}
	}

    $cres=@lfmsql_query($cqry);
    $crow=@lfmsql_fetch_array($cres);

    // Get the first/next 40 records
    $mqry = "
        SELECT
            m.*, t.*, COUNT(sal.id) _clickstoday
        FROM 
            ".$prefix."members m
        LEFT JOIN
            ".$prefix."membertypes t ON m.mtype=t.mtid
        LEFT JOIN
            ".$prefix."surf_amount_logs sal ON m.id=sal.userid
            and sal.`created_at` >= '" . date('Y-m-d') . " 00:00:00'
            and sal.`created_at` <= '" . date('Y-m-d') . " 23:59:59'
        WHERE
            m.`status` != 'Pending'
    ";
    
    // Add search criteria if applicable
    if($searchtext != "") {
    
		// Handle custom field search
		if (stristr($searchfield,"custom_")) {
			$splitsearchfield = explode("_", $searchfield);
			$searchfieldnum = $splitsearchfield[1];
			if (is_numeric($searchfieldnum) && $searchfieldnum > 0) {
				$getuserids = lfmsql_query("SELECT userid FROM ".$prefix."customvals WHERE fieldid='".$searchfieldnum."' AND fieldvalue LIKE '%".$searchtext."%'");
				if (lfmsql_num_rows($getuserids) > 0) {
					$memidlist = "";
					for ($i = 0; $i < lfmsql_num_rows($getuserids); $i++) {
						$memid = lfmsql_result($getuserids, $i, "userid");
						if ($i > 0) { $memidlist .= " OR "; }
						$memidlist .= "Id=".$memid;
					}
					$mqry .= " AND (".$memidlist.")";
				} else {
					$memid = 0;
					$mqry .= " AND (Id=".$memid.")";
				}
			} else {
				$memid = 0;
				$mqry .= " AND (Id=".$memid.")";
			}
			
			if ($timequery != "") {
				$mqry .= " and ".$timequery;
			}
		// End Handle custom field search
		} elseif ($timequery != "") {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' AND ".$timequery;
		} else {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."'";
		}
		
		$searchtext = str_replace("%", "", $searchtext);

    }
    else
    {
    		if ($timequery != "") {
			$mqry.=" AND ".$timequery;
		}
    }
    
    $mqry.=" GROUP BY m.`id` ORDER BY ".$sortby." ".$sortorder." LIMIT $st,40";

    $mres=@lfmsql_query($mqry);


	//
	// Member browsing
	// This is where the search and browse records are displayed
	//
	if($_GET["sf"] == "browse" && !isset($_POST["Delchecked"]))
	{
?>
<form name="mbrowse" id="mbrowse" method="post" action="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr>
        <td colspan="18" align="left" valign="bottom">
		<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($crow["mcount"],$_GET["limitStart"],40,"mm&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&sortby=$sortby&sortorder=$sortorder");
		?></div>
		
		<? if ($timequery == "") { echo("<input name=\"Add New Member\" type=\"button\" class=\"lfmtable\" style=\"height: 8; font-size:15px\" onClick=\"javascript:addMember()\" value=\"Add New Member\" />"); } ?>
		
		<input name="EmailSelected" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Email Members" />
		
		
		<input name="PaySelected" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Pay Members" />
		
		<input name="GivePrize" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Give Prize" />
		
		<input name="DeductCommissions" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Deduct Commissions" />
		
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Show Stats Of Selected" />
		
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Set Selected Active" />
		
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Suspend Selected" />
		
		<input name="Delchecked" type="submit" class="lfmtable" id="Delchecked" style="height: 8; font-size:15px" value="Delete Selected" />
		
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Ban Selected" />
		</td>
        </tr>
      <tr class="admintd">
      
        <? if ($showgrav == 1) { ?>
        <td width="50" align="left" nowrap="NOWRAP">&nbsp;</td>
        <? } ?>
	
	<? if ($showid == 1) { ?>
        <td align="center" width="50" align="left" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=Id&sortorder=<?=$idorder?>&sf=browse">ID</a></font></strong></td>
	<? } ?>
	
	<? if ($showfirst == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=firstname&sortorder=<?=$firstorder?>&sf=browse">First Name</a></font></strong></td>
	<? } ?>
	
	<? if ($showlast == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=lastname&sortorder=<?=$lastorder?>&sf=browse">Last Name</a></font></strong></td>
	<? } ?>
	
	<? if ($showusername == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=username&sortorder=<?=$userorder?>&sf=browse">Username</a></font></strong></td>
	<? } ?>
	
	<? if ($showemail == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=email&sortorder=<?=$emailorder?>&sf=browse">E-mail</a></font></strong></td>
	<? } ?>
	
	<? if ($showpaypal == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=paypal_email&sortorder=<?=$ppemailorder?>&sf=browse">PayPal</a></font></strong></td>
	<? } ?>
	
	<? if ($showmtype == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=mtype&sortorder=<?=$mtypeorder?>&sf=browse">MType</a></font></strong> </td>
        <? } ?>
        
        <? if ($showjoined == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=joindate&sortorder=<?=$joindateorder?>&sf=browse">Joined</a></font></strong></td>
	<? } ?>
	
	<? if ($showlogin == 1) { ?>
	<td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=lastlogin&sortorder=<?=$logindateorder?>&sf=browse">Last Login</a></font></strong></td>
        <? } ?>
        
        <? if ($showcredits == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=credits&sortorder=<?=$creditsorder?>&sf=browse">Credits</a></font></strong></td>
	<? } ?>
	
	<? if ($showbanners == 1) { ?>
	<td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=bannerimps&sortorder=<?=$bannersorder?>&sf=browse">Banners</a></font></strong></td>
	<? } ?>
	
	<? if ($showtexts == 1) { ?>
	<td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=textimps&sortorder=<?=$textsorder?>&sf=browse">Texts</a></font></strong></td>
	<? } ?>
	
	<? if ($showcomm == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=tempcomm&sortorder=<?=$commsorder?>&sf=browse&sortcomm=go">Comm.</a></font></strong></td>
	<? } ?>
	
	<? if ($showcommp == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=tempcommp&sortorder=<?=$commsporder?>&sf=browse&sortcommp=go">Comm. Paid</a></font></strong></td>
	<? } ?>
	
	<? if ($showsales == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Sales</font></strong></td>
        <? } ?>
        
        <? if ($showrefs == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=temprefs&sortorder=<?=$refsorder?>&sf=browse&sortrefs=go">Refs</a></font></strong></td>
        <? } ?>
        
        <? if ($showclicks == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=_clickstoday&sortorder=<?=$clicksorder?>&sf=browse">Clicks Today</a></font></strong></td>
        <? } ?>
        
        <? if ($showclicksyester == 1) { ?>
        <td align="center" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=clicksyesterday&sortorder=<?=$clicksyesterdayorder?>&sf=browse">Clicks Yesterday</a></font></strong></td>
        <? } ?>
        
        <?

        if ($showtype == "surfing") {
        echo("<td align=\"center\" nowrap=\"NOWRAP\"><strong>
          <font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Time Surfing</font></strong></td>");
        } elseif ($showstatus == 1) {
        echo("<td align=\"center\" nowrap=\"NOWRAP\"><strong>
          <font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Status</font></strong></td>");
        }
        ?>
          
          <?
          // Display Custom Fields
          if (count($showcustom) > 0) {
          foreach ($showcustom as $key=>$value) {
          	$getfield = lfmsql_query("SELECT name FROM ".$prefix."customfields WHERE id='".$value."'") or die(lfmsql_error());
          	if (lfmsql_num_rows($getfield) > 0) {
          		$fieldname = lfmsql_result($getfield, 0, "name");
          		echo("<td align=\"center\" nowrap=\"NOWRAP\"><strong>
          		<font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$fieldname."</font></strong></td>");
          	}
          }
          }
          // End Display Custom Fields
          ?>
          
        <td align="center" nowrap="NOWRAP">&nbsp;</td>
        <td align="center" nowrap="NOWRAP">&nbsp;</td>
        <td align="center" nowrap="NOWRAP">&nbsp;</td>
        <td width="16" align="center" nowrap="NOWRAP"><input type="checkbox" name="checkbox" value="checkbox" onClick="javascript:checkAll(document.getElementById('mbrowse'),'mcheck');" /></td>
      </tr>
<?
	while($mrow=@lfmsql_fetch_array($mres))
	{
		// Get the commission for this member
		$commission=0;
		$commres=@lfmsql_query("SELECT SUM(commission) as ctotal,COUNT(*) as tsales FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND ".$prefix."sales.status IS NULL");
		if($commres)
		{
			$commrow=@lfmsql_fetch_array($commres);
			$commission=$commrow["ctotal"];
			$tsales=$commrow["tsales"];
			if($commission < 1) { $commission="0.00"; }
		}
		else
		{
			$commission="0.00";
		}
		
		// Get the paid commission for this member
		$commissionpaid=0;
		$commres=@lfmsql_query("SELECT SUM(commission) as ctotal,COUNT(*) as tsales FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND commission>0 AND status = 'P'");
		if($commres)
		{
			$commrow=@lfmsql_fetch_array($commres);
			$commissionpaid=$commrow["ctotal"];
			$tsales=$commrow["tsales"];
			if($commissionpaid < 1) { $commissionpaid="0.00"; }
		}
		else
		{
			$commissionpaid="0.00";
		}

		// Get the number of referrals for this member
		$refs=0;
		$refres=@lfmsql_query("SELECT COUNT(*) AS refs FROM ".$prefix."members WHERE refid=".$mrow["Id"]);
		$refrow=@lfmsql_fetch_array($refres);
		if(lfmsql_num_rows($refres) > 0)
		{
			$refs=$refrow["refs"];
		}

		if($bgcolor == "#FFFFFF")
		{
			$bgcolor="#DDDDDD";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}
		
		$mrow["credits"] = floor($mrow["credits"]);
?>
      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
        
        <? if ($showgrav == 1) { ?>
        <td width="75"><img border="0" src="http://www.gravatar.com/avatar/<? echo(md5($mrow["email"])); ?>?d=mm"></td>
        <? } ?>
        
        <? if ($showid == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="admin.php?f=mstats&sf=browse&genstats=go&searchfield=Id&searchtext=<?=$mrow["Id"];?>"><?=$mrow["Id"];?></a>
        </font></td>
        <? } ?>
        
        <? if ($showfirst == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["firstname"];?>
        </font></td>
        <? } ?>
        
        <? if ($showlast == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["lastname"];?>
        </font></td>
        <? } ?>
        
        <? if ($showusername == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["username"];?>
        </font></td>
        <? } ?>
        
        <? if ($showemail == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["email"];?>
        </font></td>
        <? } ?>
        
        <? if ($showpaypal == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["paypal_email"];?>
        </font></td>
        <? } ?>
        
        <? if ($showmtype == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["accname"];?>
        </font></td>
        <? } ?>
        
        <? if ($showjoined == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=date('M j Y', strtotime($mrow["joindate"]));?>
        </font></td>
        <? } ?>
        
        <? if ($showlogin == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?
          if (strlen($mrow["lastlogin"]) > 1 && $mrow["lastlogin"] != "0000-00-00 00:00:00") {
          	echo(date('M j Y', strtotime($mrow["lastlogin"])));
          } else {
          	echo("Never");
          }
          ?>
        </font></td>
        <? } ?>
        
        <? if ($showcredits == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["credits"];?>
        </font></td>
        <? } ?>
        
        <? if ($showbanners == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["bannerimps"];?>
        </font></td>
        <? } ?>
        
        <? if ($showtexts == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["textimps"];?>
        </font></td>
        <? } ?>
        
        <? if ($showcomm == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:showSales(<?=$mrow["Id"];?>);"><?=$commission;?></a>
        </font></td>
        <? } ?>
        
        <? if ($showcommp == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:showSales(<?=$mrow["Id"];?>);"><?=$commissionpaid;?></a>
        </font></td>
        <? } ?>
        
        <? if ($showsales == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:showSales(<?=$mrow["Id"];?>);"><?=$tsales;?></a>
        </font></td>
        <? } ?>
        
        <? if ($showrefs == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="admin.php?f=mm&searchfield=refid&searchtext=<?=$mrow["Id"];?>&sf=browse&"><?=$refs;?></a>
        </font></td>
        <? } ?>
        
        <? if ($showclicks == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["_clickstoday"];?>
        </font></td>
        <? } ?>
        
        <? if ($showclicksyester == 1) { ?>
        <td align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["clicksyesterday"];?>
        </font></td>
        <? } ?>

        <?
        
        if ($showtype == "surfing") {
        
        	$surfingtime = $mrow["starttime"];
        	$timesurfing = $currenttime-$surfingtime;
        	
        	$numhours = floor($timesurfing/60/60);
        	$timesurfing = $timesurfing - ($numhours*60*60);
        	
        	$nummins = floor($timesurfing/60);
        	$timesurfing = $timesurfing - ($nummins*60);
        	
        	$numsecs = $timesurfing;
        	
        	echo("<td align=\"center\"><font size=\"3\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$numhours."H ".$nummins."M ".$numsecs."S</font></td>");
        
        } elseif ($showstatus == 1) {
        
                echo("<td align=\"center\"><font size=\"3\" face=\"Verdana, Arial, Helvetica, sans-serif\">");
		if($mrow["mtype"] > 0) {
			echo $mrow["status"];
		} else {
			echo "Pending";
		}
        	echo("</font></td>");
        }
        ?>
        
        <?
          // Display Custom Fields
          if (count($showcustom) > 0) {
          foreach ($showcustom as $key=>$value) {
          	$getfield = lfmsql_query("SELECT name FROM ".$prefix."customfields WHERE id='".$value."'") or die(lfmsql_error());
          	if (lfmsql_num_rows($getfield) > 0) {
          		echo("<td align=\"center\"><font size=\"3\" face=\"Verdana, Arial, Helvetica, sans-serif\">");
          		$getfieldval = lfmsql_query("SELECT fieldvalue FROM ".$prefix."customvals WHERE fieldid='".$value."' AND userid='".$mrow["Id"]."' LIMIT 1") or die(lfmsql_error());
          		if (lfmsql_num_rows($getfieldval) > 0) {
          			$fieldval = lfmsql_result($getfieldval, 0, "fieldvalue");
	          		echo($fieldval);
        	  	} else {
        	  		echo("N/A");
        	  	}
        	  	echo("</font></td>");
          	}
          }
          }
          // End Display Custom Fields
          ?>

        <td width="24" align="center" bgcolor="<?=$bgcolor;?>"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:delMember(<?=$mrow["Id"];?>)"><img src="../images/del.png" alt="Delete Member" width="16" height="16" border="0" /></a></font></td>
        <td width="24" align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:editMember(<?=$mrow["Id"];?>)"><img src="../images/edit.png" alt="Edit Member" width="16" height="16" border="0" /></a></font></td>
	<td align="center"><a href="login_as_member.php?userid=<?=$mrow["Id"];?>" target="_blank">Login</a></td>
        <td align="center"><input type="checkbox" class="mcheck" name="memcheck[]" value="<?=$mrow["Id"];?>" onclick="HighlightRowIfChecked(this);" />
        </td>
      </tr>
<?
}
?>
    </table>	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>
</form>
<?
}
//
// End of member browsing/searching section
//

// Delete Confirmation section
// This area is for display of details
// relating to multiple member deletion
if($_POST["Delchecked"] == "Delete Selected")
{
	if(!isset($_POST["memcheck"]))
	{
?>
<center>
  <font face="Verdana, Arial, Helvetica, sans-serif">NO RECORDS SELECTED</font>
</center>
<?
		exit;
	}
?>
<br /><br />
<center><strong><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif">You Are About To Delete The Following Members</font></strong></center>
<br /><br />
<form method="POST" action="admin.php?f=mm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<table align="center" cellpadding="2" cellspacing="0" class="lfmtable">
<tr class="admintd">
<td><strong>ID</strong></td>
<td align="center"><strong>Name</strong></td>
</tr>
<?
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$dqry="SELECT Id,firstname,lastname FROM ".$prefix."members WHERE Id=".$val;
		$dres=@lfmsql_query($dqry);
		$drow=@lfmsql_fetch_array($dres);
?>
<tr>
<td><?=$drow["Id"];?></td><td align="center"><?=$drow["firstname"]." ".$drow["lastname"];?><input type="hidden" name="idarray[]" value="<?=$drow["Id"];?>" /></td>
</tr>
<?
		}
	}

?>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr><td colspan="2" align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Are You Sure?</strong></font></td>
</tr>
<tr>
  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="No - Cancel" />
    <input name="MDelete" type="submit" id="MDelete" value="Yes - Delete" /></td>
</tr>
</table>
</form>
<?
}

?>
