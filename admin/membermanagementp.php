<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

// Operations affecting member records

// Suspend members according to check boxes
if($_POST["Submit"] == "Suspend Selected")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$sqry="UPDATE ".$prefix."members SET status='Suspended' WHERE Id=".$val;
		@mysql_query($sqry) or die("Error: Unable to suspend multiple members!");
		} 
	}
}

// Delete multiple members according to check boxes
if($_POST["MDelete"] == "Yes - Delete")
{
	if(isset($_POST["idarray"]))
	{
		while (list ($key,$val) = @each ($_POST["idarray"])) {
		$dqry="DELETE FROM ".$prefix."members WHERE Id=".$val;
		mysql_query($dqry) or die("Error: Unable to delete members!");
		} 
	}
}
?>
<script src="../inc/jsfuncs.js" type="text/javascript"></script>
<?
	// Hide the search form when displaying mass delete confirmation
	if(!isset($_POST["Delchecked"]))
	{
        if(isset($_POST["searchtext"]))
        {
            $searchfield=trim($_POST["searchfield"]);
            $searchtext=trim($_POST["searchtext"]);
        }
?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>
<form name="searchfrm" method="post" action="admin.php?f=mmp&sf=browse">
<table width="300" border="0" align="center" cellpadding="4" cellspacing="0" class="lfmtable">
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td><select name="searchfield">
      <option <? if($searchfield=="") { echo "selected=\"selected\""; } ?>>Search By...</option>
      <option <? if($searchfield=="firstname") { echo "selected=\"selected\""; } ?> value="firstname">First Name</option>
      <option <? if($searchfield=="lastname") { echo "selected=\"selected\""; } ?> value="lastname">Last Name</option>
      <option <? if($searchfield=="username") { echo "selected=\"selected\""; } ?> value="username">Username</option>
      <option <? if($searchfield=="email") { echo "selected=\"selected\""; } ?> value="email">Email</option>
      <option <? if($searchfield=="status") { echo "selected=\"selected\""; } ?> value="status">Status</option>
      <option <? if($searchfield=="commission") { echo "selected=\"selected\""; } ?> value="commission">Commission</option>
      <option <? if($searchfield=="mtype") { echo "selected=\"selected\""; } ?> value="mtype">Member Level</option>
                </select></td>
    <td><input name="searchtext" type="text" id="searchtext" /></td>
    <td><input type="submit" name="Submit" value="Search" /></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
</table>
</form>
<?
}
	// Get the starting record for member browse
	if(!isset($_GET["limitStart"]))
	{ 
		$st=0; 
	}
	else
	{
		$st=$_GET["limitStart"];
	}

	// Get the total member count for member browse
	$cqry="SELECT COUNT(*) as mcount FROM ".$prefix."members";
	// Check if there is search criteria
	if(isset($_POST["searchtext"]))
	{

		// Handle membertype search
		if($searchfield == "mtype")
		{
			$mtsqry="SELECT mtid FROM ".$prefix."membertypes WHERE accname='".$searchtext."'";
			$mtsres=@mysql_query($mtsqry);
			$mtsrow=@mysql_fetch_array($mtsres);
			$searchtext=$mtsrow["mtid"];	
		}
		
		$cqry.=" WHERE ".$searchfield." = '".$searchtext."'";
	}		

    $cres=@mysql_query($cqry);
    $crow=@mysql_fetch_array($cres);
     
    // Get the first/next 40 records
    $mqry="SELECT * FROM ".$prefix."members m LEFT JOIN ".$prefix."membertypes t ON m.mtype=t.mtid WHERE `status` = 'Pending'";
        
    // Add search criteria if applicable
    if(isset($_POST["searchtext"])) {
	    $mqry.=" WHERE ".$searchfield." = '".$searchtext."' LIMIT $st,40";
    }
    else
    {
	    $mqry.=" ORDER BY Id LIMIT $st,40";
    }

    $mres=@mysql_query($mqry);


	//
	// Member browsing
	// This is where the search and browse records are diaplayed
	//
	if($_GET["sf"] == "browse" && !isset($_POST["Delchecked"]))
	{
?>
<form name="mbrowse" id="mbrowse" method="post" action="admin.php?f=mmp&sf=browse">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr>
        <td colspan="13" align="left" valign="bottom">
		<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($crow["mcount"],$_GET["limitStart"],40);
		?></div>
		<input name="EmailSelected" type="submit" class="footer-text" style="height: 8; font-size:9px" value="Email Selected" />
		<input name="Submit" type="submit" class="footer-text" style="height: 8; font-size:9px" value="Suspend Selected" /> 
		<input name="Delchecked" type="submit" class="footer-text" id="Delchecked" style="height: 8; font-size:9px" value="Delete Selected" />		</td>
        </tr>
      <tr class="admintd">
        <td width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">ID</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">First Name </font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Last Name </font></strong></td>
        <td nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Username</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Email</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">MType</font></strong> </td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Comm.</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Sales</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Refs</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
          <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Status</font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg">&nbsp;</td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg">&nbsp;</td>
        <td width="16" align="center" nowrap="NOWRAP"><input type="checkbox" name="checkbox" value="checkbox" onClick="javascript:checkAll(document.getElementById('mbrowse'),'mcheck');" /></td>
      </tr>
<?
	while($mrow=@mysql_fetch_array($mres))
	{
		// Get the commission for this member
		$commission=0;
		$commres=@mysql_query("SELECT SUM(commission) as ctotal,COUNT(*) as tsales FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND ".$prefix."sales.status IS NULL");
		if($commres)
		{
			$commrow=@mysql_fetch_array($commres);
			$commission=$commrow["ctotal"];
			$tsales=$commrow["tsales"];
			if($commission < 1) { $commission="0.00"; }
		}
		else
		{
			$commission="0.00";
		}
		
		// Get the number of referrals for this member
		$refs=0;
		$refres=@mysql_query("SELECT COUNT(*) AS refs FROM ".$prefix."members WHERE refid=".$mrow["Id"]);
		$refrow=@mysql_fetch_array($refres);
		if(mysql_num_rows($refres) > 0)
		{
			$refs=$refrow["refs"];
		}
		
		if($bgcolor == "#FFFFFF")
		{
			$bgcolor="#DDDDDD";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}
?>  
      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:showSales(<?=$mrow["Id"];?>);"><?=$mrow["Id"];?></a>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["firstname"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["lastname"];?>
        </font></td>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["username"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["email"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["accname"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$commission;?>
        </font></td>
        <td align="center" bgcolor="<?=$bgcolor;?>"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$tsales;?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$refs;?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
<? 		if($mrow["mtype"] > 0) {
			echo $mrow["status"];
		} else {
			echo "Pending";
		}
?>
        </font></td>
        <td width="24" align="center" bgcolor="<?=$bgcolor;?>"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:delMember(<?=$mrow["Id"];?>)"><img src="../images/del.png" alt="Delete Member" width="16" height="16" border="0" /></a></font></td>
        <td width="24" align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:editMember(<?=$mrow["Id"];?>)"><img src="../images/edit.png" alt="Edit Member" width="16" height="16" border="0" /></a></font></td>
        <td align="center"><input type="checkbox" class="mcheck" name="memcheck[]" value="<?=$mrow["Id"];?>" onclick="HighlightRowIfChecked(this);" />
        </td>
      </tr>
<?
}
?>  
    </table>	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>
</form>
<?
}
//
// End of member browsing/searching section
//

// Delete Confirmation section
// This area is for display of details
// relating to multiple member deletion
if($_POST["Delchecked"] == "Delete Selected")
{
	if(!isset($_POST["memcheck"]))
	{
?>
<center>
  <font face="Verdana, Arial, Helvetica, sans-serif">NO RECORDS SELECTED</font>
</center>
<?
		exit;
	}
?>
<br /><br />
<center><strong><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif">You Are About To Delete The Following Members</font></strong></center>
<br /><br />
<form method="POST" action="admin.php?f=mmp&sf=browse">
<table align="center" cellpadding="2" cellspacing="0" class="lfmtable">
<tr class="admintd">
<td><strong>ID</strong></td>
<td align="center"><strong>Name</strong></td>
</tr>
<?
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$dqry="SELECT Id,firstname,lastname FROM ".$prefix."members WHERE Id=".$val;
		$dres=@mysql_query($dqry);
		$drow=@mysql_fetch_array($dres);
?>
<tr>
<td><?=$drow["Id"];?></td><td align="center"><?=$drow["firstname"]." ".$drow["lastname"];?><input type="hidden" name="idarray[]" value="<?=$drow["Id"];?>" /></td>
</tr>
<?		
		} 
	}

?>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr><td colspan="2" align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Are You Sure?</strong></font></td>
</tr>
<tr>
  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="No - Cancel" />
    <input name="MDelete" type="submit" id="MDelete" value="Yes - Delete" /></td>
</tr>
</table>
</form>
<?
}

?>
