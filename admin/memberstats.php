<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

// Get The Basic Variables
if (is_numeric($_GET['statstype'])) {
	$statstype = $_GET['statstype'];
} else {
	$statstype = 1;
	
	if ($_GET['genstats'] != "go") {
		// Unset Selected Values
		unset($_SESSION["getidquery"]);
	}
}

if (is_numeric($_GET['starttime'])) {
	$starttime = $_GET['starttime'];
	$startdate = date("Y-m-d", $starttime);
} else {
	$starttime = 0;
	$startdate = "0000-00-00";
}

if (is_numeric($_GET['endtime'])) {
	$endtime = $_GET['endtime'];
	$enddate = date("Y-m-d", $endtime);
} else {
	$endtime = 0;
	$enddate = "0000-00-00";
}
// End Get The Basic Variables

// Delete Stats Options
if (isset($_GET['delstats']) && is_numeric($_GET['delstats'])) {
	@lfmsql_query("Delete from `".$prefix."savedstats` where id=".$_GET['delstats']." limit 1");
}

// Update For One Day Stats
if ($starttime > $endtime) {
	$endtime = $starttime;
	$enddate = $startdate;
}
// End Update For One Day Stats

// Get The Search Terms
if (isset($_POST["searchtext"])) {
            $searchfield=trim($_POST["searchfield"]);
            $searchtext=trim($_POST["searchtext"]);
            
} elseif (($_GET['searchfield'] != "") && ($_GET['searchtext'] != "")) {
            $searchfield=trim($_GET["searchfield"]);
            $searchtext=trim($_GET["searchtext"]);
}
// End Get The Search Terms

//LFMTE Sorting
if (($_GET["sortby"] != "") && ($_GET["sortorder"] != "")) {
$sortby = $_GET["sortby"];
$sortorder = $_GET["sortorder"];

//Set Data Types

if ($sortby == "temprefs") {
	$datatype = 5;
} else {
	$datatype = 1;
}

//Set Orders
if ($sortby == "Id" && $sortorder == "ASC") {
$idorder = "DESC";
} else {
$idorder = "ASC";
}
if ($sortby == "firstname" && $sortorder == "ASC") {
$firstorder = "DESC";
} else {
$firstorder = "ASC";
}
if ($sortby == "lastname" && $sortorder == "ASC") {
$lastorder = "DESC";
} else {
$lastorder = "ASC";
}
if ($sortby == "username" && $sortorder == "ASC") {
$userorder = "DESC";
} else {
$userorder = "ASC";
}
if ($sortby == "tempclicks" && $sortorder == "DESC") {
$clicksorder = "ASC";
} else {
$clicksorder = "DESC";
}
if ($sortby == "clicksyesterday" && $sortorder == "DESC") {
$clicksyesterorder = "ASC";
} else {
$clicksyesterorder = "DESC";
}

if ($sortby == "tempcomm" && $sortorder == "DESC") {
$commorder = "ASC";
} else {
$commorder = "DESC";
}

if ($sortby == "tempsales" && $sortorder == "DESC") {
$salesorder = "ASC";
} else {
$salesorder = "DESC";
}

if ($sortby == "tempnumsales" && $sortorder == "DESC") {
$salesnumorder = "ASC";
} else {
$salesnumorder = "DESC";
}

if ($sortby == "temprefs" && $sortorder == "DESC") {
$refsorder = "ASC";
} else {
$refsorder = "DESC";
}

if ($sortby == "temptime" && $sortorder == "DESC") {
$timeorder = "ASC";
} else {
$timeorder = "DESC";
}

if ($sortby == "recordtime" && $sortorder == "DESC") {
$recordorder = "ASC";
} else {
$recordorder = "DESC";
}

if ($sortby == "tempaccuracy" && $sortorder == "DESC") {
$accuracyorder = "ASC";
} else {
$accuracyorder = "DESC";
}

} else {
$sortby = "Id";
$sortorder = "ASC";

$idorder = "ASC";
$firstorder = "ASC";
$lastorder = "ASC";
$userorder = "ASC";
$clicksorder = "DESC";

$clicksyesterorder = "DESC";
$commorder = "DESC";
$salesorder = "DESC";
$salesnumorder = "DESC";
$refsorder = "DESC";
$timeorder = "DESC";
$recordorder = "DESC";
$accuracyorder = "DESC";

$datatype = 1;

}
//End LFMTE Sorting


if ($_GET['genstats'] == "go") {

if (isset($_GET['loadstats']) && is_numeric($_GET['loadstats'])) {

	$loadstats = lfmsql_query("Select * from `".$prefix."savedstats` where id=".$_GET['loadstats']." limit 1");
	if (lfmsql_num_rows($loadstats) > 0) {
		$statstype = lfmsql_result($loadstats, 0, "statstype");
		$mtype = lfmsql_result($loadstats, 0, "mtype");
		
		if ($statstype == 1) {
			$thedate = date("Y-m-d");
			$startdate = $thedate;
			$enddate = $thedate;
		} else {
			$startdate = lfmsql_result($loadstats, 0, "startdate");
			$enddate = lfmsql_result($loadstats, 0, "enddate");
		}
		
		$searchfield = "mtype";
		$searchtext = lfmsql_result($loadstats, 0, "mtype");
		
		$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
		$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;
	}
	
	unset($_SESSION["getidquery"]);

} else {

	// Get The Start and End Dates
	
	if (isset($_POST['statstype'])) {
		$statstype = $_POST['statstype'];
	}
	
	if ($statstype == 1) {
		// Today Stats
		$thedate = date("Y-m-d");
		$starttime = mktime(0,0,0,substr($thedate,5,2),substr($thedate,8),substr($thedate,0,4));
		$endtime = mktime(0,0,0,substr($thedate,5,2),substr($thedate,8),substr($thedate,0,4))+86399;
		$startdate = $thedate;
		$enddate = $thedate;
		
	} elseif ($statstype == 2) {
		// Lifetime Stats
		$startdate = "2000-01-01";
		$enddate = "2030-12-31";
		$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
		$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;
		
	} else {
		// Date Range Stats
		$starttime = mktime(0,0,0,substr($_POST['sdate'],5,2),substr($_POST['sdate'],8),substr($_POST['sdate'],0,4));
		$endtime = mktime(0,0,0,substr($_POST['edate'],5,2),substr($_POST['edate'],8),substr($_POST['edate'],0,4))+86399;
		$startdate = $_POST['sdate'];
		$enddate = $_POST['edate'];
	}
	
	// Show Weekly If Dates Are Invalid
	if ($startdate < "2000-01-01") {
		$startdate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")." + 7 days ago"));
	}
	if ($enddate < "2000-01-01") {
		$enddate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")));
	}

	// Update For One Day Stats
	if ($starttime > $endtime) {
		$endtime = $starttime;
		$enddate = $startdate;
	}
	// End Update For One Day Stats

	// Save Stats Option
	
	if($searchfield == "mtype" && is_numeric($searchtext)) {
		$mtypevalue = $searchtext;
	} else {
		$mtypevalue = 0;
	}
	
	if ($_POST['statsname'] != "") {
		$statsname = $_POST['statsname'];
		@lfmsql_query("Insert into ".$prefix."savedstats (statstype, statsname, startdate, enddate, mtype) VALUES (".$statstype.", '".$statsname."', '".$startdate."', '".$enddate."', ".$mtypevalue.")");
	}

	// End Save Stats Option
}

	// Show Weekly If Dates Are Invalid
	if ($startdate < "2000-01-01") {
		$startdate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")." + 7 days ago"));
	}
	if ($enddate < "2000-01-01") {
		$enddate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")));
	}

	// Update For One Day Stats
	if ($starttime > $endtime) {
		$endtime = $starttime;
		$enddate = $startdate;
	}
	// End Update For One Day Stats

	// Handle membertype search
	if($searchfield == "mtype" && $searchtext == 0) {
		$searchtext = "";
		$searchfield = "";
	}

	// Check if there is search criteria
	if($searchtext != "")
	{
		
		if($searchfield == "mtype" && !is_numeric($searchtext))
		{
			$mtsqry="SELECT mtid FROM ".$prefix."membertypes WHERE accname='".$searchtext."'";
			$mtsres=@lfmsql_query($mtsqry);
			$mtsrow=@lfmsql_fetch_array($mtsres);
			$searchtext=$mtsrow["mtid"];
		}
		
		// Handle referral search
		if ($searchfield == "refid" && !is_numeric($searchtext)) {

			$getrefid = lfmsql_query("Select Id from ".$prefix."members where username='".$searchtext."' or email='".$searchtext."'");
			if (lfmsql_num_rows($getrefid) > 0) {
				$searchtext = lfmsql_result($getrefid, 0, "Id");
			}		
		}
		if ($searchfield == "refid") {
			unset($_SESSION["newgetid"]);
			unset($_SESSION["getidquery"]);
		}
		
		// Handle ip address search
		if ($searchfield == "ipadd") {
			$searchfield = "lastip='".$searchtext."' or signupip";
		}
		
		// Handle email address search
		if ($searchfield == "email") {
			$searchtext = "%".$searchtext."%";
		}

	}
	// End Search Checks


    // Get the records
    $mqry="SELECT Id, firstname, lastname, username, tempclicks, temprefs, tempsales, tempnumsales, tempclicks, tempcomm, clicksyesterday, temptime, recordtime, tempaccuracy FROM ".$prefix."members WHERE `status` != 'Pending'";

    // Add search criteria if applicable
    if($searchtext != "") {
    
    		if ($timequery != "") {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' AND ".$timequery;
		} else {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."'";
		}
		
		$searchtext = str_replace("%", "", $searchtext);

    }
    else
    {
    		if ($timequery != "") {
			$mqry.=" AND ".$timequery;
		}
    }

	// Get IDs from member management
	if (!isset($_SESSION["getidquery"]) || isset($_SESSION["newgetid"])) {
	
    		if(isset($_SESSION["newgetid"])) {
			$getidquery = " AND (";
			while (list ($key,$val) = @each ($_SESSION["newgetid"])) {
				$getidquery .= "Id=".$val." OR ";
			}
			unset($_SESSION["newgetid"]);
			// Remove Last OR
			$getidquery = substr($getidquery, 0, -4);
			$getidquery .= ")";
		} else {
			$getidquery = "";
		}
	
		$_SESSION["getidquery"] = $getidquery;
	} else {
		$getidquery = $_SESSION["getidquery"];
	}
	
	$mqry .= $getidquery;
	// End Get IDs from member management

    
	$mres=@lfmsql_query($mqry);

	while($mrow=@lfmsql_fetch_array($mres))
	{
		// Get the commissions and sales for this member
		$commission=0;
		$tsales=0;
		$tnumsales=0;
		$commres=@lfmsql_query("SELECT SUM(commission) as ctotal, SUM(itemamount) as tsales, COUNT(*) as tnumsales FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND saledate <= '".$enddate." 23:59:59' AND saledate >= '".$startdate."' AND prize=0");
		if($commres)
		{
			$commrow=@lfmsql_fetch_array($commres);
			$commission=$commrow["ctotal"];
			$tnumsales=$commrow["tnumsales"];
			$tsales=$commrow["tsales"];
			if($commission < 1) { $commission="0.00"; }
			if($tsales < 1) { $tsales="0.00"; }
		}
		else
		{
			$commission="0.00";
			$tsales = "0.00";
			$tnumsales = 0;
		}
		if ($commission < 0 || $commission == "") { $commission = 0; }
		if ($tsales < 0 || $tsales == "") { $tsales = 0; }
		@lfmsql_query("Update ".$prefix."members set tempcomm=".$commission.", tempnumsales=".$tnumsales.", tempsales=".$tsales." where Id=".$mrow["Id"]." limit 1");

		// Get the number of referrals for this member
		$refs=0;
		$refresquery = "SELECT COUNT(*) AS refs FROM ".$prefix."members WHERE status='Active' AND refid=".$mrow["Id"]." AND joindate <= '".$enddate." 23:59:59' AND joindate >= '".$startdate."'";
		$refres=@lfmsql_query($refresquery);
		$refrow=@lfmsql_fetch_array($refres);
		if(lfmsql_num_rows($refres) > 0)
		{
			$refs=$refrow["refs"];
		} else {
			$refs = 0;
		}
		@lfmsql_query("Update ".$prefix."members set temprefs=".$refs." where Id=".$mrow["Id"]." limit 1");
		
		
		// Get the clicks, times, and accuracy for this member
		$clicks=0;
		$time=0;
		$accuracy=0;
		if ($statstype == 1) {
			// Today Stats
			$memres = lfmsql_query("Select clickstoday, timetoday from ".$prefix."members where Id=".$mrow["Id"]." limit 1");
			$clicks = lfmsql_result($memres, 0, "clickstoday");
			$time = lfmsql_result($memres, 0, "timetoday");
		} elseif ($statstype == 2) {
			// Lifetime Stats
			$memres = lfmsql_query("Select clickstoday, timetoday, goodclicks, badclicks from ".$prefix."members where Id=".$mrow["Id"]." limit 1");
			$clicks = lfmsql_result($memres, 0, "clickstoday");
			$time = lfmsql_result($memres, 0, "timetoday");
			
			$goodclicks = lfmsql_result($memres, 0, "goodclicks");
			$badclicks = lfmsql_result($memres, 0, "badclicks");
			
			if ($goodclicks > 0) {
				$accuracy = $goodclicks/($goodclicks+$badclicks);
				$accuracy = round($accuracy*100);
			} else {
				$accuracy = 0;
			}
			
			$histres = lfmsql_query("Select SUM(clicks) AS clicks, SUM(time) AS time from ".$prefix."surflogs where userid=".$mrow["Id"]);
			$clicks = $clicks + lfmsql_result($histres, 0, "clicks");
			$time = $time + lfmsql_result($histres, 0, "time");
		} else {
			// Date Range Stats
			$histres = lfmsql_query("Select SUM(clicks) AS clicks, SUM(time) AS time from ".$prefix."surflogs where userid=".$mrow["Id"]." AND date <= '".$enddate."' AND date >= '".$startdate."'");
			$clicks = lfmsql_result($histres, 0, "clicks");
			$time = lfmsql_result($histres, 0, "time");
			
			if (date("Y-m-d") <= $enddate) {
			// Add Today's Clicks
			$memres = lfmsql_query("Select clickstoday from ".$prefix."members where Id=".$mrow["Id"]." limit 1");
			$clicks = $clicks + lfmsql_result($memres, 0, "clickstoday");
			}
		}
		if ($clicks < 0 || $clicks == "") { $clicks = 0; }
		if ($time < 0 || $time == "") { $time = 0; }
		@lfmsql_query("Update ".$prefix."members set tempclicks=".$clicks.", temptime=".$time.", tempaccuracy=".$accuracy." where Id=".$mrow["Id"]." limit 1");
		
	}

// End Update The LFMTE Stats

}


// Operations affecting member records

// Suspend members according to check boxes
if($_POST["Submit"] == "Suspend Selected" && array_search('Edit Members', $blockedperms) === false)
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$sqry="UPDATE ".$prefix."members SET status='Suspended' WHERE Id=".$val;
		@lfmsql_query($sqry) or die("Error: Unable to suspend multiple members!");
		}
	}
}

// Un-Suspend members according to check boxes
if($_POST["Submit"] == "Set Selected Active" && array_search('Edit Members', $blockedperms) === false)
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$uqry="UPDATE ".$prefix."members SET status='Active', disableddate=NULL WHERE Id=".$val;
		@lfmsql_query($uqry) or die("Error: Unable to un-suspend multiple members!");
		}
	}
}

// Flag selected for removal
if($_POST["Submit"] == "Flag Selected For Removal" && array_search('Edit Members', $blockedperms) === false)
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$uqry="UPDATE ".$prefix."members SET status='Removal' WHERE Id=".$val;
		@lfmsql_query($uqry) or die("Error: Unable to flag multiple members!");
		}
	}
}

// Delete multiple members according to check boxes
if($_POST["MDelete"] == "Yes - Delete" && array_search('Edit Members', $blockedperms) === false)
{
	if(isset($_POST["idarray"]))
	{
		while (list ($key,$val) = @each ($_POST["idarray"])) {
		$dqry="DELETE FROM ".$prefix."members WHERE Id=".$val;
		lfmsql_query($dqry) or die("Error: Unable to delete members!");
		}
	}
}
?>
<script src="../inc/jsfuncs.js" type="text/javascript"></script>
<?
	// Hide the search form when displaying mass delete confirmation
	if(!isset($_POST["Delchecked"]))
	{

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol.css">
<p>&nbsp;</p>

<form method="post" action="admin.php?f=mstats&sf=browse&genstats=go">
<table width="400" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
  <tr>
    <td colspan="2" align="center" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Stats Options</font> </strong></td>
  </tr>
  <tr>
    <td valign="center" align="center"><b>Time Range:</b></td>
    <td>
    <INPUT type=radio name=statstype value=1 <? if($statstype == 1) { echo (" checked");} ?>> Today<br>
    <INPUT type=radio name=statstype value=2 <? if($statstype == 2) { echo (" checked");} ?>> Lifetime<br>
    <INPUT type=radio name=statstype value=3 <? if($statstype == 3) { echo (" checked");} ?>> Custom (enter below)</td>
  </tr>
  <tr>
    <td valign="center" align="center"><b>Custom Range:</b><br>(If selected above)</td>
   
    <td>
    
    Start Date: <input name="sdate" type="text" id="DPC_sdate_YYYY-MM-DD" value="<? if($statstype == 3) { echo($startdate); } ?>" width="80"><br>
    End Date: <input name="edate" type="text" id="DPC_edate_YYYY-MM-DD" value="<? if($statstype == 3) { echo($enddate); } ?>" width="80">
    
    </td>
  </tr>
  <tr>
    <td valign="center" align="center"><b>Account Type:</b></td>
    <input type="hidden" name="searchfield" value="mtype">
    <td>
    <?
	echo("<select name=searchtext><option value=0>All Members</option>");
	
	$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
	for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
	$accid = lfmsql_result($getaccounts, $j, "mtid");
	$accname = lfmsql_result($getaccounts, $j, "accname");
	echo("<option value=$accid"); if($searchfield=="mtype" && $searchtext==$accid){echo(" selected");} echo(">$accname</option>");
	}
	echo("</select>");
    ?>
    </td>
  </tr>
  <tr>
    <td valign="center" align="center"><b>Save As:</b><br>(Optional)</td>
    <td><input type="text" name="statsname" value=""></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" name="submit" value="Show Stats"></td>
  </tr>
</table><br><br>
</form>

<table width="400" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
  <tr>
    <td colspan="4" align="center" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Saved Stats Options</font> </strong></td>
  </tr>
<?

$loadstats = lfmsql_query("Select id, statsname from `".$prefix."savedstats` order by statsname asc");
if (lfmsql_num_rows($loadstats) > 0) {
	for ($i = 0; $i < lfmsql_num_rows($loadstats); $i++) {
		$loadstatsid = lfmsql_result($loadstats, $i, "id");
		$loadstatsname = lfmsql_result($loadstats, $i, "statsname");
		echo("<tr>
		<td align=left>".$loadstatsname."</td>
		<td align=center><a href=\"admin.php?f=mstats&sf=browse&genstats=go&loadstats=".$loadstatsid."\">View Stats</a></td>
		<td align=center><a target=\"_blank\" href=\"leaderboard.php?boardid=".$loadstatsid."\">Leaderboard</a></td>
		<td align=center><a href=\"admin.php?f=mstats&delstats=".$loadstatsid."\">Delete</a></td>
		</tr>");
	}
} else {
	echo("<tr><td colspan=\"4\" align=\"center\">No Saved Stats Options</td></tr>");
}

?>
</table><br><br>
<?
}

	// Handle membertype search
	if($searchfield == "mtype" && $searchtext == 0) {
		$searchtext = "";
		$searchfield = "";
	}

	// Get the starting record for member browse
	if(!isset($_GET["limitStart"]))
	{
		$st=0;
	}
	else
	{
		$st=$_GET["limitStart"];
	}

	// Get the total member count for member browse
	$cqry="SELECT COUNT(*) as mcount FROM ".$prefix."members";
	
	// Check if there is search criteria
	if($searchtext != "")
	{

		// Handle membertype search
		if($searchfield == "mtype" && !is_numeric($searchtext))
		{
			$mtsqry="SELECT mtid FROM ".$prefix."membertypes WHERE accname='".$searchtext."'";
			$mtsres=@lfmsql_query($mtsqry);
			$mtsrow=@lfmsql_fetch_array($mtsres);
			$searchtext=$mtsrow["mtid"];
		}
		
		// Handle referral search
		if ($searchfield == "refid" && !is_numeric($searchtext)) {

			$getrefid = lfmsql_query("Select Id from ".$prefix."members where username='".$searchtext."' or email='".$searchtext."'");
			if (lfmsql_num_rows($getrefid) > 0) {
				$searchtext = lfmsql_result($getrefid, 0, "Id");
			}		
		}
		
		// Handle ip address search
		if ($searchfield == "ipadd") {
			$searchfield = "lastip='".$searchtext."' or signupip";
		}
		
		// Handle email address search
		if ($searchfield == "email") {
			$searchtext = "%".$searchtext."%";
		}
		
		if ($timequery != "") {
			$cqry.=" WHERE ".$searchfield." = '".$searchtext."' and ".$timequery;
		} else {
			$cqry.=" WHERE ".$searchfield." = '".$searchtext."'";
		}
	
	} else {
		if ($timequery != "") {
			$cqry.=" WHERE ".$timequery;
		}
	}

    $cres=@lfmsql_query($cqry);
    $crow=@lfmsql_fetch_array($cres);

    // Get the first/next 40 records
    $mqry="SELECT Id, firstname, lastname, username, tempclicks, temprefs, tempsales, tempnumsales, tempclicks, tempcomm, clicksyesterday, temptime, recordtime, tempaccuracy FROM ".$prefix."members WHERE `status` != 'Pending'";

    // Add search criteria if applicable
    if($searchtext != "") {
    
    		if ($timequery != "") {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' AND ".$timequery;
		} else {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."'";
		}
		
		$searchtext = str_replace("%", "", $searchtext);

    }
    else
    {
    		if ($timequery != "") {
			$mqry.=" AND ".$timequery;
		}
    }
    
    if (isset($_SESSION["getidquery"])) {
    	$mqry .= $_SESSION["getidquery"];
    }
    
    $mqry.=" ORDER BY ".$sortby." ".$sortorder." LIMIT $st,40";

    $mres=@lfmsql_query($mqry);

	//
	// Member browsing
	// This is where the search and browse records are displayed
	//
	if($_GET["sf"] == "browse" && !isset($_POST["Delchecked"]))
	{
?>
<form name="mbrowse" id="mbrowse" method="post" action="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr>
        <td colspan="13" align="left" valign="bottom">
		<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($crow["mcount"],$_GET["limitStart"],40,"mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=$sortby&sortorder=$sortorder");
		?></div>

		<input name="EmailSelected" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Email Members" />
		<input name="GivePrize" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Give Prize" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Suspend Selected" />
		<input name="Delchecked" type="submit" class="lfmtable" id="Delchecked" style="height: 8; font-size:15px" value="Delete Selected" />		</td>
        </tr>
      <tr class="admintd">
        <td width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=Id&sortorder=<?=$idorder?>&sf=browse">ID</a></font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=firstname&sortorder=<?=$firstorder?>&sf=browse">First Name</a></font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=lastname&sortorder=<?=$lastorder?>&sf=browse">Last Name</a></font></strong></td>
        <td nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=username&sortorder=<?=$userorder?>&sf=browse">Username</a></font></strong></td>

        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=tempcomm&sortorder=<?=$commorder?>&sf=browse">Comm.</a></font></strong></td>
		
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=tempsales&sortorder=<?=$salesorder?>&sf=browse">Sales</a></font></strong></td>
		
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=tempnumsales&sortorder=<?=$salesnumorder?>&sf=browse"># of Sales</a></font></strong></td>
        
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=temprefs&sortorder=<?=$refsorder?>&sf=browse">Refs</a></font></strong></td>
        
        <?

	if ($statstype == 1) {
	
	// Today Stats
        
        echo("<td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=tempclicks&sortorder=$clicksorder&sf=browse\">Clicks</a></font></strong></td>
        
        <td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=clicksyesterday&sortorder=$clicksyesterorder&sf=browse\">Clicks Yesterday</a></font></strong></td>
                
        <td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=temptime&sortorder=$timeorder&sf=browse\">Time Surfing</a></font></strong></td>");
        
        } elseif ($statstype == 2) {
        
        // Life Time Stats
        
        echo("<td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=tempclicks&sortorder=$clicksorder&sf=browse\">Clicks</a></font></strong></td>
                
        <td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=temptime&sortorder=$timeorder&sf=browse\">Time Surfing</a></font></strong></td>
        
        <td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=recordtime&sortorder=$recordorder&sf=browse\">Longest Surf Session</a></font></strong></td>
        
        <td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=tempaccuracy&sortorder=$accuracyorder&sf=browse\">Click Accuracy</a></font></strong></td>");
        
        } else {
        
        echo("<td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=tempclicks&sortorder=$clicksorder&sf=browse\">Clicks</a></font></strong></td>
        
        <td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"admin.php?f=mstats&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&statstype=$statstype&starttime=$starttime&endtime=$endtime&sortby=temptime&sortorder=$timeorder&sf=browse\">Time Surfing</a></font></strong></td>");
        
        }
        
        ?>
              
        <td width="16" align="center" nowrap="NOWRAP"><input type="checkbox" name="checkbox" value="checkbox" onClick="javascript:checkAll(document.getElementById('mbrowse'),'mcheck');" /></td>
      </tr>
<?
	while($mrow=@lfmsql_fetch_array($mres))
	{

		if($bgcolor == "#FFFFFF")
		{
			$bgcolor="#DDDDDD";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}
		
?>
      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a target="_blank" href="membergraphs.php?drawgraph=yes&memberid=<?=$mrow["Id"];?>&starttime=<?=$starttime;?>&endtime=<?=$endtime;?>&datatype=<?=$datatype;?>&graphtype=1"><?=$mrow["Id"];?></a>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["firstname"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["lastname"];?>
        </font></td>
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["username"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:showSales(<?=$mrow["Id"];?>);"><?=$mrow["tempcomm"];?></a>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:showSales(<?=$mrow["Id"];?>);"><?=$mrow["tempsales"];?></a>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a href="javascript:showSales(<?=$mrow["Id"];?>);"><?=$mrow["tempnumsales"];?></a>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a target="_blank" href="viewrefs.php?userid=<?=$mrow["Id"];?>&starttime=<?=$starttime;?>&endtime=<?=$endtime;?>"><?=$mrow["temprefs"];?></a>
        </font></td>
        
        <?

	if ($statstype == 1) {

		//Today Stats
		
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$mrow["tempclicks"]."</font></td>");
        	
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$mrow["clicksyesterday"]."</font></td>");
        
        	// Time Today
        	$timesurfing = $mrow["temptime"];
        	
        	$numhours = floor($timesurfing/60/60);
        	$timesurfing = $timesurfing - ($numhours*60*60);
        	
        	$nummins = floor($timesurfing/60);
        	$timesurfing = $timesurfing - ($nummins*60);
        	
        	$numsecs = $timesurfing;
        	
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$numhours."H ".$nummins."M ".$numsecs."S</font></td>");
        
        } elseif ($statstype == 2) {
        
        	// Life Time Clicks
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$mrow["tempclicks"]."</font></td>");
        	
        	// Total Time
        	$timesurfing = $mrow["temptime"];
        	
        	$numhours = floor($timesurfing/60/60);
        	$timesurfing = $timesurfing - ($numhours*60*60);
        	
        	$nummins = floor($timesurfing/60);
        	$timesurfing = $timesurfing - ($nummins*60);
        	
        	$numsecs = $timesurfing;
        	
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$numhours."H ".$nummins."M ".$numsecs."S</font></td>");
        	
        	// Record Time
        	$timesurfing = $mrow["recordtime"];
        	
        	$numhours = floor($timesurfing/60/60);
        	$timesurfing = $timesurfing - ($numhours*60*60);
        	
        	$nummins = floor($timesurfing/60);
        	$timesurfing = $timesurfing - ($nummins*60);
        	
        	$numsecs = $timesurfing;
        	
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$numhours."H ".$nummins."M ".$numsecs."S</font></td>");
        	
        	// Clicking Accuracy
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$mrow["tempaccuracy"]."%</font></td>");
        
        } else {
        
        	// Clicks
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$mrow["tempclicks"]."</font></td>");
        	
        	// Total Time
        	$timesurfing = $mrow["temptime"];
        	
        	$numhours = floor($timesurfing/60/60);
        	$timesurfing = $timesurfing - ($numhours*60*60);
        	
        	$nummins = floor($timesurfing/60);
        	$timesurfing = $timesurfing - ($nummins*60);
        	
        	$numsecs = $timesurfing;
        	
        	echo("<td align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$numhours."H ".$nummins."M ".$numsecs."S</font></td>");
        
        }

        ?>
        
        <td align="center"><input type="checkbox" class="mcheck" name="memcheck[]" value="<?=$mrow["Id"];?>" onclick="HighlightRowIfChecked(this);" />
        </td>
      </tr>
<?
}
?>
    </table>	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>
</form>

<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  <tr>
    <td align="left" valign="top" bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Click on a User ID # to view graphical stats for that member.  Click on a Refs value to view a list of that member's referrals.</font></p>
      
      </font></p>

      <p>&nbsp;</p></td>
  </tr>
</table>

<?
}
//
// End of member browsing/searching section
//

// Delete Confirmation section
// This area is for display of details
// relating to multiple member deletion
if($_POST["Delchecked"] == "Delete Selected" && array_search('Edit Members', $blockedperms) === false)
{
	if(!isset($_POST["memcheck"]))
	{
?>
<center>
  <font face="Verdana, Arial, Helvetica, sans-serif">NO RECORDS SELECTED</font>
</center>
<?
		exit;
	}
?>
<br /><br />
<center><strong><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif">You Are About To Delete The Following Members</font></strong></center>
<br /><br />
<form method="POST" action="admin.php?f=mstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&statstype=<?=$statstype?>&starttime=<?=$starttime?>&endtime=<?=$endtime?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<table align="center" cellpadding="2" cellspacing="0" class="lfmtable">
<tr class="admintd">
<td><strong>ID</strong></td>
<td align="center"><strong>Name</strong></td>
</tr>
<?
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$dqry="SELECT Id,firstname,lastname FROM ".$prefix."members WHERE Id=".$val;
		$dres=@lfmsql_query($dqry);
		$drow=@lfmsql_fetch_array($dres);
?>
<tr>
<td><?=$drow["Id"];?></td><td align="center"><?=$drow["firstname"]." ".$drow["lastname"];?><input type="hidden" name="idarray[]" value="<?=$drow["Id"];?>" /></td>
</tr>
<?
		}
	}

?>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr><td colspan="2" align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Are You Sure?</strong></font></td>
</tr>
<tr>
  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="No - Cancel" />
    <input name="MDelete" type="submit" id="MDelete" value="Yes - Delete" /></td>
</tr>
</table>
</form>
<?
}

?>
