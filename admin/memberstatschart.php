<?php

// VisuGraph 1.0
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

if(!isset($_SESSION["adminid"])) { exit; };
if(!isset($datatype)) { exit; };

//Draw Text Chart

if ($datatype == 1) {
	$searchvalue = "clicks";
	$chartheading = "Clicks";
} elseif ($datatype == 2) {
	$searchvalue = "hitsdeliver";
	$chartheading = "Hits";
} elseif ($datatype == 3) {
	$searchvalue = "bandeliver";
	$chartheading = "Banner Impressions";
} elseif ($datatype == 4) {
	$searchvalue = "textdeliver";
	$chartheading = "Text Impressions";
} else {
	$searchvalue = "newrefs";
	$chartheading = "New Referrals";
}

$startdate = $_POST['sdate'];
$enddate = $_POST['edate'];

$charthtml = "<table width=\"300\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\" class=\"lfmtable\" style=\"border: 1px solid #999;\">
  <tr>
    <td align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Date</font> </strong></td>
    <td align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$chartheading."</font> </strong></td>
  </tr>";
  
$numdays = countdays($startdate, $enddate);
$numelems = 0;

if ($numdays > 40) {
	//Switch to monthly view

	$currdate = $startdate;
	
	while ($currdate <= $enddate) {
	
	$selecteddate = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));;
	
	$selectedmonth = date("m", $selecteddate);
	$selectedyear = date("Y", $selecteddate);
	$daya = $selectedyear."-".$selectedmonth."-"."01";
	$dayb = $selectedyear."-".$selectedmonth."-"."31";
	$monthname = date("M", $selecteddate);
		
	if ($monthname == "Jan") {
		$monthname = "Jan
".$selectedyear;
	}
	
	$getmonth = mysql_query("Select SUM(".$searchvalue.") from ".$prefix."surflogs where date>='".$daya."' and date<='".$dayb."' and userid=".$memberid);
	
	$currentsum = mysql_result($getmonth, 0);
	if ($currentsum == NULL) {
		$currentsum = 0;
	}

	$statsum[$numelems] = $currentsum;
	$xname[$numelems] = $monthname;
	$numelems = $numelems+1;
	
	$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 month"));
	
	}

} else {

	$numelems = $numdays;

	$getdays = mysql_query("Select ".$searchvalue.", date from ".$prefix."surflogs where date>='".$startdate."' and date<='".$enddate."' and userid=".$memberid." order by date asc");
	
	if (mysql_num_rows($getdays) > 0) {
		for ($i = 0; $i < mysql_num_rows($getdays); $i++) {
			$thedate = mysql_result($getdays, $i, "date");
			$datelist[$i] = $thedate;
			$sumlist[$i] = mysql_result($getdays, $i, $searchvalue);
		}
	} else {
		// Set Empty Date Array
		$datelist[0] = 0;
	}
	
	$currdate = $startdate;
	$i = 0;
	while ($currdate <= $enddate) {
	
		$dateposition = array_search($currdate, $datelist);
		if ($dateposition === false) {
			//No Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = 0;
			$xname[$i] = date("d", $thetime);
		} else {
			//Enter Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = $sumlist[$dateposition];
			$xname[$i] = date("d", $thetime);
		}
	
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
		$i++;
	}
}

if ($numdays == 0) {
	$charthtml .= "<tr><td colspan=2 align=\"center\"><b>There is no data for this date range.</b></td></tr>";
} else {
				
	for ($i = 0; $i < $numdays; $i++) {
		$charthtml .= "<tr><td align=\"center\">".$xname[$i]."</td><td align=\"center\">".$statsum[$i]."</td></tr>";
	}
}

$charthtml .= "</table>";

?>