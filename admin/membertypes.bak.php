<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.05
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



	// Prevent anyone who isn't logged in from opening this page
	include "../inc/checkauth.php"; 
	if(!isset($_SESSION["adminid"])) { exit; };

	$msg="";
	// Add a new group
	if($_POST["Submit"] == "Add")
	{
		//  Do some sanity checks
		$scres=@lfmsql_query("SELECT count(*) as ncount FROM ".$prefix."membertypes WHERE accname='".$_POST["accname"]."'");
		$scrow=@lfmsql_fetch_array($scres);
		if($scrow["ncount"] > 0 || $_POST["comm"] < 0 || $_POST["otocomm"] < 0 || strlen($_POST["accname"]) < 1)
		{
			if($scrow["ncount"] > 0)
				$msg="The member type '".$_POST["accname"]."' already exists!";
			if(!isset($_POST["comm"]))
				$msg="The Commission must be 0 or greater!";
			if(!isset($_POST["otocomm"]))
				$msg="The OTO Commission must be 0 or greater!";
			if(strlen($_POST["accname"]) < 1)
				$msg="The member type has no name!";
			

		}
		else
		{	
			if ($_POST["minauto"] < 1) {
				$_POST["minauto"] = $_POST["minauto"]*100;
			}
			
			if ($_POST["refcrds"] < 1) {
				$_POST["refcrds"] = $_POST["refcrds"]*100;
			}
			
			if ($_POST["refcrds2"] < 1) {
				$_POST["refcrds2"] = $_POST["refcrds2"]*100;
			}
			
			if(!isset($_POST["rank"])) { $rank = 0; } else { $rank=$_POST["rank"]; }	
			@lfmsql_query("INSERT INTO ".$prefix."membertypes(accname,comm,comm2,otocomm,otocomm2,commfixed,commfixed2,rank,surfratio,hitvalue,bannervalue,textvalue,maxsites,maxbanners,maxtexts,minauto,refcrds,refcrds2,manbanners,mantexts,mansites,surftimer,description,monthcrds,monthbimps,monthtimps,cantransfer,mintransfer,maxtransfer,enabled) VALUES('".$_POST["accname"]."', '".$_POST["comm"]."', '".$_POST["comm2"]."', '".$_POST["otocomm"]."', '".$_POST["otocomm2"]."','".$_POST["commfixed"]."','".$_POST["commfixed2"]."','".$rank."','".$_POST["surfratio"]."','".$_POST["hitvalue"]."','".$_POST["bannervalue"]."','".$_POST["textvalue"]."','".$_POST["maxsites"]."','".$_POST["maxbanners"]."','".$_POST["maxtexts"]."','".$_POST["minauto"]."','".$_POST["refcrds"]."','".$_POST["refcrds2"]."',".$_POST["manbanners"].",".$_POST["mantexts"].",".$_POST["mansites"].",".$_POST["surftimer"].",'".$_POST['desc']."',".$_POST['monthcrds'].",".$_POST['monthbimps'].",".$_POST['monthtimps'].",'".$_POST['cantransfer']."','".$_POST['mintransfer']."','".$_POST['maxtransfer']."',".$_POST['enabled'].")") or die(lfmsql_error());
		}
	}
?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>
<center><p><a href="admin.php?f=dynamic"><font size="3" face="Verdana, Arial, Helvetica, sans-serif"><b>Click Here To Add/Edit Dynamic Surfing Ratios</b></font></a></p><p><b>After creating a new account type, <a href=admin.php?f=scrds>Click Here</a> to create a sales package for the Upgrade page or an OTO offer.</b></p></center>
<br />
<table border="0" align="center" cellpadding="3" cellspacing="0" style="border:thin solid #000 ; border-width: 1px;" >
  
  <tr>
    <td width="33%" align="center" class="admintd">&nbsp;</td>
    <td align="center" class="admintd"><font size="3" face="Arial, Helvetica, sans-serif"><strong>Membership Account Types</strong></font></td>
    <td width="33%" align="center" class="admintd">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">
	<table border="0" cellspacing="0" cellpadding="2">
      <tr align="center" bgcolor="#E0EBFE">
        <td valign="top" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Name</font></strong></td>
        <td valign="top" nowrap="nowrap" bgcolor="#C6DBFD"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Rank</font></strong></td>
        
        <? //echo("<td valign=\"top\" nowrap=\"nowrap\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Enable<br />Publish<br />Date</font></strong></td>"); ?>
        
        <? //echo("<td valign=\"top\" nowrap=\"nowrap\" bgcolor=\"#C6DBFD\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">2CO<br />Prod ID</font> </strong></td>"); ?>
        
        <td valign="top" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surfing<br />
          Ratio</font></strong></td>
        <td valign="top" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surfing<br />
          Timer</font></strong></td>
        <td valign="top" nowrap="nowrap" bgcolor="#C6DBFD"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ref<br />
        %</font></strong></td>
        <td valign="top" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ref<br />
          %<br />
          <font color="#FF0000">Level 2</font> </font></strong></td>
        <td valign="top" nowrap="nowrap" bgcolor="#C6DBFD"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ref<br />
          $        </font></strong></td>
        <td valign="top" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Ref<br />
          $<br />
          <font color="#FF0000">Level 2</font></font></strong></td>
        <td valign="top" nowrap="nowrap" bgcolor="#C6DBFD"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">OTO<br />
          %
        </font></strong></td>
        <td valign="top" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">OTO<br />
          %<br />
          <font color="#FF0000">Level 2</font> </font></strong></td>
        <td width="24" valign="top" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Visible</font></strong></td>
        <td width="24" valign="top" nowrap="nowrap" bgcolor="#C6DBFD"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Total<br />
          Memb.</font></strong></td>
        <td colspan="3" valign="top" nowrap="nowrap">&nbsp;</td>
        </tr>
<?
	// Get the list of member account types
	$mtres=@lfmsql_query("SELECT * FROM ".$prefix."membertypes ORDER BY acclevel");
	while($mtrow=@lfmsql_fetch_array($mtres))
	{
		// Get total members at this level
		$totalmres=@lfmsql_query("SELECT COUNT(*) AS mcount FROM ".$prefix."members WHERE mtype=".$mtrow["mtid"]);
		$totalm=@lfmsql_result($totalmres,0);
		
?>	  
      <tr>
        <td align="center" valign="top" nowrap="nowrap"><?=$mtrow["accname"];?></td>
        <td align="center" valign="top"><?=$mtrow["rank"];?></td>
        
        <?
        /*
        echo("<td align=\"center\" valign=\"top\">");
        if($mtrow["enablepdate"] == 1){ 
        echo("<img src=\"../images/tick.jpg\">");
        } else { 
        echo("<img src=\"../images/cancel.jpg\">");
        }
        echo("</td>");
        */
        ?>
        
        <?
        /*
        echo("<td align=\"center\" valign=\"top"\>$mtrow['_2co_productid']</td>");
        */
        ?>
        
        <td align="center" valign="top"><?=$mtrow["surfratio"];?></td>
        <td align="center" valign="top"><?=$mtrow["surftimer"];?> seconds</td>
        <td align="center" valign="top"><?=$mtrow["comm"];?></td>
        <td align="center" valign="top"><?=$mtrow["comm2"];?></td>
        <td align="center" valign="top"><?=$mtrow["commfixed"];?></td>
        <td align="center" valign="top"><?=$mtrow["commfixed2"];?></td>
        <td align="center" valign="top"><?=$mtrow["otocomm"];?></td>
        <td align="center" valign="top"><?=$mtrow["otocomm2"];?></td>
        <td width="24" align="center" valign="top" nowrap="NOWRAP">
<?		if($mtrow["enabled"] == 1) { ?>
		<img src="../images/tick.jpg" />
<?		} else { ?>
		<img src="../images/cancel.jpg" />
<? } ?>		</td>
        <td width="24" align="center" valign="top" nowrap="NOWRAP"><?=$totalm;?></td>
        <? 	if($mtrow["mtid"] > 1)
	{
?>
        <td width="24" align="center" valign="top"><a href="javascript:editMembertype(<?=$mtrow["mtid"];?>)"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
        <td width="24" align="center" valign="top"><a href="javascript:delMembertype(<?=$mtrow["mtid"];?>)"><img src="../images/del.png" width="16" height="16" border="0" /></a></td>
        <? } else { ?>
        <td width="24" align="center" valign="top"><a href="javascript:editMembertype(<?=$mtrow["mtid"];?>)"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
        <? } ?>
      </tr>
<?
}
?>
    </table>	</td>
  </tr>
</table>
<p align="center"><font color="red"><strong><?=$msg;?></strong></font>&nbsp;</p>
<form method="post" action="admin.php?f=mt" onsubmit="return validate_addMembertype(this);">
  <table style="border:thin solid #000 ; border-width: 1px;" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td colspan="6" align="center" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Add Membership/Upgrade Type</font> </strong></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>This is the name for this account type that will appear on the signup page.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Membership Name:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="accname" type="text" id="accname" size="28" value="Upgraded Membership"></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The rank is used to determine which member content should 'pass through' to higher member levels.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Rank:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="rank" type="text" id="rank" value="0" size="2" /></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of credits members will earn per click.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surfing Ratio:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="surfratio" type="text" size="2" value="0.5"></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of seconds that a member must view each site.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surf Timer:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="surftimer" type="text" size="2" value="10" /> seconds</td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of hits a site will receive per credit assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Hits Per Credit:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="hitvalue" type="text" size="2" value="1" /></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of impressions a banner will receive per credit assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Banner Imps Per Credit:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="bannervalue" type="text" size="2" value="30" /></td>
  </tr>
  
   <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The number of impressions a text ad will receive per credit assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Text Imps Per Credit:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="textvalue" type="text" size="2" value="40" /></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The maximum number of sites a member can submit.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Number of Sites:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="maxsites" type="text" size="2" value="30"  /></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The maximum number of banners a member can submit.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Number of Banners:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="maxbanners" type="text" size="2" value="30"  /></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The maximum number of text ads a member can submit.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Number of Text Ads:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="maxtexts" type="text" size="2" value="30" /></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The percentage of credits that must be auto-assigned.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Auto Assign Minimum:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="minauto" type="text" size="2" value="0" /> %</td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>If YES, banners will not rotate until approved from the admin area.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Approve Banners:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="manbanners">
    <option value=0 selected>No</option>
    <option value=1>Yes</option>
    </select>
    </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>If YES, text ads will not rotate until approved from the admin area.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Approve Text Ads:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="mantexts">
    <option value=0 selected>No</option>
    <option value=1>Yes</option>
    </select>
    </td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>If YES, sites will not rotate until approved from the admin area.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Approve Sites:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="mansites">
    <option value=0 selected>No</option>
    <option value=1>Yes</option>
    </select>
    </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of banner impressions members will get at the beginning of each month.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Monthly Banner Bonus:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="monthbimps" type="text" size="2" value="0"></td>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of text impressions members will get at the beginning of each month.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Monthly Text Ad Bonus:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="monthtimps" type="text" size="2" value="0"></td>
  </tr>
  
  <tr>
      <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The number of credits members will get at the beginning of each month.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Monthly Credit Bonus:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="monthcrds" type="text" size="2" value="0"></td>
    <td align="left" bgcolor="#E0EBFE"></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"></strong></td>
    <td align="left" nowrap="nowrap"></td>
  </tr>
  
  <?
  echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>If YES, members with this account type will get random referrals.</span></a></td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Random Referrals:</font></strong></td>
    <td align=\"left\" nowrap=\"nowrap\">
    <select name=\"randrefs\">
    <option value=0 selected>No</option>
    <option value=1>Yes</option>
    </select>
    </td>
    <td align=\"left\" bgcolor=\"#E0EBFE\"></td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\"></strong></td>
    <td align=\"left\" nowrap=\"nowrap\"></td>
  </tr>");

echo("<input type=\"hidden\" name=\"enablepdate\" value=\"0\">");
  /*
  echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>You can enable a publish date to prevent members from accessing previously published content.</span></a></td>
    <td align=\"left\" nowrap=\"NOWRAP\" bgcolor=\"#E0EBFE\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Enable Publish Date:</font></strong></td>
    <td align=\"left\" nowrap=\"nowrap\"><input name=\"enablepdate\" type=\"checkbox\" id=\"enablepdate\" value=\"1\" /></td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\">&nbsp;</td>
  </tr>");
  */
  ?>
  
  
  <?
  echo("<input type=\"hidden\" name=\"pp_srt\" id=\"pp_srt\" value=\"0\">");
  /*
  echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>The total number of recurring payments to charge for this account type (0 = forever).</span></a></td>
    <td align=\"left\" nowrap=\"NOWRAP\" bgcolor=\"#E0EBFE\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Recurring Payments </font></strong></td>
    <td align=\"left\" nowrap=\"nowrap\"><select name=\"pp_srt\" id=\"pp_srt\">
      <option value=\"0\" selected=\"selected\">0</option>
      <option value=\"1\">1</option>
      <option value=\"2\">2</option>
      <option value=\"3\">3</option>
      <option value=\"4\">4</option>
      <option value=\"5\">5</option>
      <option value=\"6\">6</option>
      <option value=\"7\">7</option>
      <option value=\"8\">8</option>
      <option value=\"9\">9</option>
      <option value=\"10\">10</option>
      <option value=\"11\">11</option>
      <option value=\"12\">12</option>
      <option value=\"13\">13</option>
      <option value=\"14\">14</option>
      <option value=\"15\">15</option>
      <option value=\"16\">16</option>
      <option value=\"17\">17</option>
      <option value=\"18\">18</option>
      <option value=\"19\">19</option>
      <option value=\"20\">20</option>
      <option value=\"21\">21</option>
      <option value=\"22\">22</option>
      <option value=\"23\">23</option>
      <option value=\"24\">24</option>
      <option value=\"25\">25</option>
      <option value=\"26\">26</option>
      <option value=\"27\">27</option>
      <option value=\"28\">28</option>
      <option value=\"29\">29</option>
      <option value=\"30\">30</option>
    </select></td>
    <td align=\"left\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\" bgcolor=\"#E0EBFE\">&nbsp;</td>
    <td align=\"left\" nowrap=\"nowrap\">&nbsp;</td>
  </tr>");
  */
  ?>
  
  <tr>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage of credits members earn from their direct referrals surfing.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong>
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referral Credits:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="refcrds" type="text" id="refcrds" size="3" value="0">
      %</td>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage of credits members earn from their second level referrals surfing.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong>
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referral Credits <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="refcrds2" type="text" id="refcrds2" size="3" value="0">
      %</td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage amount of the membership fee that affiliates/members are paid for referring new members. Set this to 0 if you are paying fixed or no commission.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong>
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">Commissions:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="comm" type="text" id="comm" size="3" value="0">
      %</td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The percentage amount of the membership fee that level 2 affiliates/members are paid for referring new members. Set this to 0 if you are paying fixed or no level 2 commission.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Commissions <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="comm2" type="text" id="comm2" size="3"  value="0" />
% </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The fixed amount that affiliates/members are paid for referring new members. Set this to 0 if you are not paying a commission for free accounts.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referring Bonus $:</font></strong></td>
    <td align="left" nowrap="nowrap">    <input name="commfixed" type="text" id="commfixed" size="3" value="0" />
      (Free accounts only) </td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The fixed amount that level 2 affiliates/members are paid for referring new members. Set this to 0 if you are not paying a level 2 commission for free accounts.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referring Bonus $ <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="commfixed2" type="text" id="commfixed2" size="3" value="0" />       (Free accounts only) </td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The percentage amount that affiliates/members are paid on OTO sales to members they referred.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">OTO Commissions:</font></strong></td>
    <td align="left" nowrap="nowrap"><input name="otocomm" type="text" id="otocomm" size="3" value="0"> 
      % </td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>The percentage amount that level affiliates/members are paid on OTO sales to members they referred.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">OTO Commissions <font color="#FF0000">Level 2:</font> </font></strong></td>
    <td align="left" nowrap="nowrap"><input name="otocomm2" type="text" id="otocomm2" size="3"  value="0" />
% </td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>If YES, the member can transfer their credits to their downline.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Can Transfer Downline Credits:</font></strong></td>
    <td align="left" nowrap="nowrap">
    <select name="cantransfer">
    <option value=0>No</option>
    <option value=1 selected="selected">Yes</option>
    </select>
    </td>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The minimum number of credits a member can transfer.  Enter 0 for no minimum.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Minimum Credit Transfer:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="mintransfer" type="text" size="2" value="10"></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The maximum number of credits a member can transfer.  Enter 0 for no maximum.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE">
	  <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Maximum Credit Transfer:</font></strong></td>
    <td align="left" nowrap="nowrap">
	<input name="maxtransfer" type="text" size="2" value="0"></td>
    <td align="left" bgcolor="#E0EBFE"></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"></strong></td>
    <td align="left" nowrap="nowrap"></td>
  </tr>
  
  <tr>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
    <td align="left" nowrap="nowrap"><br></td>
  </tr>
  
  <tr>
    <td align="left" bgcolor="#E0EBFE"><a class=info href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
	  <span>The description that will appear on the Upgrade page.</span></a></td>
    <td align="left" nowrap="NOWRAP" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Description: </font></strong></td>
    <td align="left" nowrap="nowrap"><textarea name="desc" wrap="soft" rows="5" cols="25"></textarea></td>
    <td align="left" bgcolor="#E0EBFE"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>Set this membership Visible or Not Visible. If the membership level is 'Not Visible' it will not appear on the upgrade page.</span></a></td>
    <td align="left" nowrap="nowrap" bgcolor="#E0EBFE"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Visible:</font></strong></td>
    <td align="left" nowrap="nowrap"><select name="enabled">
      <option value="1" selected="selected">Y</option>
      <option value="0">N</option>
    </select></td>
  </tr>
  
  <?
  echo("<input type=\"hidden\" name=\"cb_paylink\" id=\"pp_srt\" value=\"\">");
  /*
  echo("<tr>
    <td align=\"left\" bgcolor=\"#E0EBFE\"><a class=\"info\" href=\"#\"><img src=\"../images/question.jpg\" width=\"15\" height=\"15\" border=\"0\" /> <span>The Clickbank paylink <br />for this member level <br />(e.g. 1.vendor.pay.clickbank.net )</span></a></td>
    <td colspan=\"5\" align=\"left\" nowrap=\"nowrap\"><table border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
      <tr>
        <td width=\"150\" nowrap=\"nowrap\" class=\"admintd\"><strong>CB Paylink</strong></td>
        <td><input name=\"cb_paylink\" type=\"text\" class=\"formfield\" id=\"cb_paylink\" size=\"40\" /></td>
      </tr>
    </table>      </td>
    </tr>");
    */
    ?>
    
  <tr>
    <td colspan="6" align="center" bgcolor="#B0CBFD"><input type="submit" name="Submit" value="Add" /></td>
  </tr>
</table>
</form>
<br />
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  
  <tr>
    <td bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <ul>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The price and subscription periods must be added on the <a href=admin.php?f=scrds>Sales Packages</a> page.  You may create multiple sales packages for each membership level.  For example, you could create one package for the regular monthly subscription, and another one for an OTO offer. </font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif">You <strong>MUST</strong> use the existing free membership level and it must be visible. You can rename it if required. </font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Leave the Level 2 values set to 0 if you don't want to have a second commission level.</font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The fixed commission (Ref $) applies<strong> <font color="#FF0000">only to free accounts.</font></strong></font></li>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The 'Rank' is used to designate the order of your memberships when using the 'flow through' feature. A higher rank means a higher level membership. For example, if you had Gold and Silver levels and gave Silver a rank of 1 and Gold a rank of 2, your Gold members would have access to any content pages you created for Silver members. To use this feature enable the <strong>'Member Levels Flow Through'</strong> option in the System Settings.  </font></li>
      </ul>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
