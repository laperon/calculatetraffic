<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

?>

<html>

<head>
<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<style>
body {
  margin: 0;
  background-color: #FFFFFF;
}
</style>

<body>
<center>

<?

if (isset($_POST['Submit']) && $_POST['Submit'] == "Update Grid") {
	
	if ($_POST['showgrav'] != "1") { $_POST['showgrav'] = 0; }
	if ($_POST['showid'] != "1") { $_POST['showid'] = 0; }
	if ($_POST['showfirst'] != "1") { $_POST['showfirst'] = 0; }
	if ($_POST['showlast'] != "1") { $_POST['showlast'] = 0; }
	if ($_POST['showusername'] != "1") { $_POST['showusername'] = 0; }
	if ($_POST['showemail'] != "1") { $_POST['showemail'] = 0; }
	if ($_POST['showpaypal'] != "1") { $_POST['showpaypal'] = 0; }
	if ($_POST['showmtype'] != "1") { $_POST['showmtype'] = 0; }
	if ($_POST['showjoined'] != "1") { $_POST['showjoined'] = 0; }
	if ($_POST['showlogin'] != "1") { $_POST['showlogin'] = 0; }
	if ($_POST['showcredits'] != "1") { $_POST['showcredits'] = 0; }
	if ($_POST['showbanners'] != "1") { $_POST['showbanners'] = 0; }
	if ($_POST['showtexts'] != "1") { $_POST['showtexts'] = 0; }
	if ($_POST['showcomm'] != "1") { $_POST['showcomm'] = 0; }
	if ($_POST['showcommp'] != "1") { $_POST['showcommp'] = 0; }
	if ($_POST['showsales'] != "1") { $_POST['showsales'] = 0; }
	if ($_POST['showrefs'] != "1") { $_POST['showrefs'] = 0; }
	if ($_POST['showclicks'] != "1") { $_POST['showclicks'] = 0; }
	if ($_POST['showclicksyester'] != "1") { $_POST['showclicksyester'] = 0; }
	if ($_POST['showstatus'] != "1") { $_POST['showstatus'] = 0; }
	
	if (!isset($_POST["showcustom"]) || !is_array($_POST["showcustom"])) {
		$showcustom = "";
	} else {
		$showcustom = implode(",", $_POST["showcustom"]);
	}
	
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showgrav']."' WHERE field='showgrav'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showid']."' WHERE field='showid'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showfirst']."' WHERE field='showfirst'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showlast']."' WHERE field='showlast'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showusername']."' WHERE field='showusername'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showemail']."' WHERE field='showemail'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showpaypal']."' WHERE field='showpaypal'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showmtype']."' WHERE field='showmtype'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showjoined']."' WHERE field='showjoined'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showlogin']."' WHERE field='showlogin'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showcredits']."' WHERE field='showcredits'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showbanners']."' WHERE field='showbanners'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showtexts']."' WHERE field='showtexts'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showcomm']."' WHERE field='showcomm'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showcommp']."' WHERE field='showcommp'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showsales']."' WHERE field='showsales'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showrefs']."' WHERE field='showrefs'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showclicks']."' WHERE field='showclicks'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showclicksyester']."' WHERE field='showclicksyester'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$_POST['showstatus']."' WHERE field='showstatus'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."memgrid` SET value='".$showcustom."' WHERE field='showcustom'") or die(lfmsql_error());
	
	echo("<br><div class=\"lfm_title\">Grid Updated Successfully</div><br>");
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;	
}

####################

//Begin main page

####################

$showgrav = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showgrav'"), 0);
$showid = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showid'"), 0);
$showfirst = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showfirst'"), 0);
$showlast = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showlast'"), 0);
$showusername = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showusername'"), 0);
$showemail = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showemail'"), 0);
$showpaypal = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showpaypal'"), 0);
$showmtype = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showmtype'"), 0);
$showjoined = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showjoined'"), 0);
$showlogin = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showlogin'"), 0);
$showcredits = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showcredits'"), 0);
$showbanners = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showbanners'"), 0);
$showtexts = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showtexts'"), 0);
$showcomm = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showcomm'"), 0);
$showcommp = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showcommp'"), 0);
$showsales = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showsales'"), 0);
$showrefs = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showrefs'"), 0);
$showclicks = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showclicks'"), 0);
$showclicksyester = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showclicksyester'"), 0);
$showstatus = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showstatus'"), 0);
$showcustom = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."memgrid` WHERE field='showcustom'"), 0);

?>

	<br><div class="lfm_title">Member Grid</div>
	
	<form action="memgrid.php" method="post">
	<table width="500" cellpadding="5" cellspacing="0" border="0">
	
	<tr><td>
	
	<input type="checkbox" name="showgrav" value="1"<? if($showgrav == 1) { echo(" checked"); } ?>> <font size="2">Gravitar Photo</font><br>
	<input type="checkbox" name="showid" value="1"<? if($showid == 1) { echo(" checked"); } ?>> <font size="2">User ID</font><br>
	<input type="checkbox" name="showfirst" value="1"<? if($showfirst == 1) { echo(" checked"); } ?>> <font size="2">First Name</font><br>
	<input type="checkbox" name="showlast" value="1"<? if($showlast == 1) { echo(" checked"); } ?>> <font size="2">Last Name</font><br>
	<input type="checkbox" name="showusername" value="1"<? if($showusername == 1) { echo(" checked"); } ?>> <font size="2">Username</font><br>
	<input type="checkbox" name="showemail" value="1"<? if($showemail == 1) { echo(" checked"); } ?>> <font size="2">E-mail</font><br>
	<input type="checkbox" name="showpaypal" value="1"<? if($showpaypal == 1) { echo(" checked"); } ?>> <font size="2">PayPal E-mail</font><br>
	<input type="checkbox" name="showmtype" value="1"<? if($showmtype == 1) { echo(" checked"); } ?>> <font size="2">Membership Type</font><br>
	<input type="checkbox" name="showjoined" value="1"<? if($showjoined == 1) { echo(" checked"); } ?>> <font size="2">Date Joined</font><br>
	<input type="checkbox" name="showlogin" value="1"<? if($showlogin == 1) { echo(" checked"); } ?>> <font size="2">Last Login</font><br>
	<input type="checkbox" name="showcredits" value="1"<? if($showcredits == 1) { echo(" checked"); } ?>> <font size="2">Credits</font><br>
	<input type="checkbox" name="showbanners" value="1"<? if($showbanners == 1) { echo(" checked"); } ?>> <font size="2">Banner Impressions</font><br>
	<input type="checkbox" name="showtexts" value="1"<? if($showtexts == 1) { echo(" checked"); } ?>> <font size="2">Text Impressions</font><br>
	<input type="checkbox" name="showcomm" value="1"<? if($showcomm == 1) { echo(" checked"); } ?>> <font size="2">Commissions Owed</font><br>
	<input type="checkbox" name="showcommp" value="1"<? if($showcommp == 1) { echo(" checked"); } ?>> <font size="2">Commissions Paid</font><br>
	<input type="checkbox" name="showsales" value="1"<? if($showsales == 1) { echo(" checked"); } ?>> <font size="2">Number of Sales</font><br>
	<input type="checkbox" name="showrefs" value="1"<? if($showrefs == 1) { echo(" checked"); } ?>> <font size="2">Number of Referrals</font><br>
	<input type="checkbox" name="showclicks" value="1"<? if($showclicks == 1) { echo(" checked"); } ?>> <font size="2">Clicks Today</font><br>
	<input type="checkbox" name="showclicksyester" value="1"<? if($showclicksyester == 1) { echo(" checked"); } ?>> <font size="2">Clicks Yesterday</font><br>
	<input type="checkbox" name="showstatus" value="1"<? if($showstatus == 1) { echo(" checked"); } ?>> <font size="2">Account Status</font><br>
	
	<?
	// List Custom Fields
	$getfields = lfmsql_query("SELECT id, name FROM `".$prefix."customfields` ORDER BY rank ASC");
     	for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
     		$fieldid = lfmsql_result($getfields, $i, "id");
     		$fieldname = lfmsql_result($getfields, $i, "name");
     		echo('<input type="checkbox" name="showcustom[]" value="'.$fieldid.'"'); if(array_search($fieldid, explode(",", $showcustom)) !==false) { echo(" checked"); } echo('> <font size="2">'.$fieldname.'<br>');
     	}
	// End List Custom Fields
	?>
	
	</td></tr>
	
	<tr><td>
	<INPUT type="submit" name="Submit" value="Update Grid">
	</td></tr>
	
	</table>
	</form>

<br><br>

</center>
</body>
</html>

<?php
exit;
?>