<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////
//    Duplicate Finder Originally Developed By:    //
//           Chris Houg, LFMTE-Host.com            //
/////////////////////////////////////////////////////

mysql_connect($dbhost, $dbuser, $dbpass);
mysql_select_db($dbname);

$action = $_GET['action'];

switch ($action) {
	case "url":
		$table = $prefix."msites";
		$scolumn = "url";
		$memid = "memid";
		$like = "LIKE";
		$title = "Duplicate URLs";
		$action2 = 2;
		$f = "smm";
		$searchfield="url";
		break;
	case "banners":
		$table = $prefix."mbanners";
		$scolumn = "target";
		$memid = "memid";
		$like = "LIKE";
		$title = "Duplicate Banner Targets";
		$action2 = 2;
		$f = "bmm";
		$searchfield = "target";
		break;
	case "textads":
		$table = $prefix."mtexts";
		$scolumn = "target";
		$like = "LIKE";
		$memid = "memid";
		$title = "Duplicate Textad Targets";
		$action2 = 2;
		$f = "tmm";
		$searchfield = "target";
		break;
	case "signup":
		$table = $prefix."members";
		$scolumn = "signupip";
		$memid = Id;
		$like = "=";
		$title = "Duplicate Signup IPs";
		$action2 = 1;
		$searchfield = "ipadd";
		break;
	case "ppemail":
		$table = $prefix."members";
		$scolumn = "paypal_email";
		$memid = "Id";
		$like = "LIKE";
		$title = "Duplicate PayPal Email Addresses";
		$action2 = 1;
		$searchfield = "paypal_email";
		break;
	default:
		$table = $prefix."members";
		$scolumn = "lastip";
		$memid = "Id";
		$like = "=";
		$title = "Duplicate Last Login IP";
		$action2 = 1;
		$searchfield = "ipadd";
		break;

}

$query = "SELECT `$scolumn`, count(*) FROM `$table` GROUP BY `$scolumn` ORDER BY `count(*)` DESC";

$result = mysql_query($query) or die(mysql_error());

$text = "<link href=\"mod_dupe_css.css\" rel=\"stylesheet\" type=\"text/css\" />
		<div class=\"wrapper\">
		<div class=\"contenturls\">";
$text .= get_menu($action);
$text .= get_header($action);

$count = 0;
if ($action2 == 1) {

	for  ($i=0; $i<mysql_num_rows($result); $i++) {
	    $row = mysql_fetch_array($result);
	    if ($row[1] > 1 & $row[0] != "") {
	    $text .= "<tr class=\"trdata\"><td class=\"tddata\"><a href='admin.php?f=mm&show=&sortby=Id&sortorder=ASC&sf=browse&searchfield=$searchfield&searchtext=$row[0]' target='_blank'>$row[0]</a>&nbsp;&nbsp;</td><td class=\"tddata\" align=\"center\">$row[1]</td><td class=\"tddata\">".get_num($memid,$table,$scolumn,$row[0],$like)."</td></tr>";
	    $count++;
	    }
	}
} else {
		for  ($i=0; $i<mysql_num_rows($result); $i++) {
	    $row = mysql_fetch_array($result);
	    if ($row[1] > 1 & $row[0] != "") {
	    	$urlquery = "SELECT `memid`, count(*) from `".$table."` where ".$scolumn." ".$like." '".$row[0]."' group by `memid`";
//	    	echo $urlquery; exit;
	    	$aresult = mysql_query($urlquery);
	    	$number = mysql_num_rows($aresult);
	    	$row2 = mysql_fetch_array($aresult);
	    	if ($number > 1) {
	    		$text .= "<tr class=\"trdata\"><td class=\"tddata\"><a href='admin.php?f=".$f."&show=&sortby=Id&sortorder=ASC&sf=browse&searchfield=".$searchfield."&searchtext=$row[0]' target='_blank'>$row[0]</A>&nbsp;&nbsp;</td><td class=\"tddata\" align=\"center\">$row2[1]</td><td class=\"tddata\">".get_num($memid,$table,$scolumn,$row[0],$like)."</td></tr>";
				$count++;
	    	}
	    }
	}

}

if ($count < 1) {
	$text .= "<tr class=\"trdata\"><td colspan=\"3\" class=\"tddata\" align=\"center\"><p><span class=\"s8black\">No Duplicates Found</span></p></td></tr>";
}

$text .= "</table>
</div>
</div>
";

echo $text;


function  get_num($memid,$table,$scolumn,$item,$like){
//	echo "SELECT $memid,count(*) FROM ".$table." where ".$scolumn." $like '".$item."' GROUP BY ".$memid." ORDER BY `".$memid."` ASC";
//	exit;
	$aresult = mysql_query("SELECT $memid,count(*) FROM ".$table." where ".$scolumn." $like '".$item."' GROUP BY ".$memid." ORDER BY `".$memid."` ASC")
	or die("Query failed: " . mysql_error());
	if ($number = mysql_num_rows($aresult) > 1) {
		while ($row = mysql_fetch_array($aresult)) {
			$members .= "<a href = \"admin.php?f=mm&show=&sortby=Id&sortorder=ASC&sf=browse&searchfield=id&searchtext=$row[0]\" target=\"_blank\">$row[0]</a><br>";
		}
	}
//	echo $members; exit;
	return $members;
}

function get_header($action){

	switch ($action){
		case "url":
			$content = "
<p><span class=\"s8black\">Duplicate URLs</span></p>
<p class=\"s9black\"> The database is searched for duplicate URLs for member sites.&nbsp;&nbsp; Affiliate URLs should be unique to each member so a URL listed here is an indication that a member has more than one account.</p>
<p class=\"s9black\">A member may have a URL listed more than once, those won't be listed unless that exact URL is used by more than one member.</p>
<p class=\"s9black\"><br />
</p>
<table class=\"tdata\" width=\"700\" cellpadding=\"0\" cellspacing=\"0\">
  <thead class=\"thead\">
    <tr class=\"trdata\">
    <td class=\"tdata\" align=\"center\" width=\"500\"><span class=\"thdata\">URL</span></td>
    <td class=\"tdata\" align=\"center\" width=\"75\"><span class=\"thdata\">Number</span></td>
    <td class=\"tdata\" align=\"center\" width=\"125\"><span class=\"thdata\">Member IDs</span></td>
    </tr>
  </thead>";
			break;

		case "textads":
			$content = "
<p><span class=\"s8black\">Duplicate Textad Targets</span></p>
<p class=\"s9black\"> The database is searched for duplicate Target URLs for member's Textads.&nbsp;&nbsp;  The Target URL  should be unique as it will be their affiliate URL. </p>
<p class=\"s9black\">A member may have a Textad listed more than once, so those won't be listed unless that exact Target URL is used by more than one member.</p>
<p class=\"s9black\"><br />
</p>
<table class=\"tdata\" width=\"700\" cellpadding=\"0\" cellspacing=\"0\">
  <thead class=\"thdata\">
    <tr class=\"trdata\">
    <td class=\"tddata\" align=\"center\" width=\"500\"><span class=\"thdata\">Target URL</span></td>
    <td class=\"tddata\" align=\"center\" width=\"75\"><span class=\"thdata\">Number</span></td>
    <td class=\"tddata\" align=\"center\" width=\"125\"><span class=\"thdata\">Member IDs</span></td>
    </tr>
  </thead>";

			break;

		case "signup":
			$content = "<p><span class=\"s8black\">Duplicate  Signup IPs</span></p>
<p class=\"s9black\">
Below are the IPs that have been signing up multiple accounts. This doesn't always mean someone is signing up for more than one account.</p>
<p class=\"s9black\">Cases where it may be ok is if you allow a household to have more than one account or perhaps someone from the same area as another member signed up and they were assigned an IP that once belonged to another member.  </p>
<p class=\"s9black\">Judgement must be used for punitive action. <br />
  <br />
  <br />
</p>
<table class=\"tdata\" width=\"325\" cellpadding=\"0\" cellspacing=\"0\">
  <thead class=\"thdata\">
    <tr class=\"trdata\">
    <td class=\"tddata\" align=\"center\" width=\"125\"><span class=\"thdata\">IP</span></td>
    <td class=\"tddata\" align=\"center\" width=\"75\"><span class=\"thdata\">Number</span></td>
    <td class=\"tddata\" align=\"center\" width=\"125\"><span class=\"thdata\">Member IDs</span></td>
    </tr>
  </thead>";

			break;

		case "banners":
			$content = "
<p><span class=\"s8black\">Duplicate Banner Targets</span></p>
<p class=\"s9black\"> The database is searched for duplicate Target URLs for member's banners.&nbsp;&nbsp; Image URLs are often used by different members so these are ignored. The Target URL, though, should be unique as it will be their affiliate URL. </p>
<p class=\"s9black\">A member may have a Banner listed more than once, so those won't be listed unless that exact Target URL is used by more than one member.</p>
<p class=\"s9black\"><br />
</p>
<table class=\"tdata\" width=\"700\" cellpadding=\"0\" cellspacing=\"0\">
  <thead class=\"thdata\">
	 <tr class=\"trdata\">
    <td class=\"tddata\" align=\"center\" width=\"500\"><span class=\"thdata\">Target URL</span></td>
    <td class=\"tddata\" align=\"center\" width=\"75\"><span class=\"thdata\">Number</span></td>
    <td class=\"tddata\" align=\"center\" width=\"125\"><span class=\"thdata\">Member IDs</span></td>
    </tr>
  </thead>
  <tr>";

			break;

		case "ppemail":
			$content = "<p><span class=\"s8black\">Duplicate PayPal Email Addresses</span></p>
<p class=\"s9black\">
Below are duplicates of the same PayPal Email address used in multiple accounts.  This may indicate a member that has more than one account.</p>
<p class=\"s9black\"><span style=\"color:red\"><b>It could also mean a hacker changed the PayPal Email address to their own PayPal email address in multiple accounts.</b></span><br />
  <br />
  <br />
</p>
<table class=\"tdata\" width=\"400\" cellpadding=\"0\" cellspacing=\"0\">
  <thead class=\"thdata\">
    <tr class=\"trdata\">
    <td class=\"tddata\" align=\"center\" width=\"200\"><span class=\"thdata\">PayPal Email Address</span></td>
    <td class=\"tddata\" align=\"center\" width=\"75\"><span class=\"thdata\">Number</span></td>
    <td class=\"tddata\" align=\"center\" width=\"125\"><span class=\"thdata\">Member IDs</span></td>
    </tr>
  </thead>";
			break;

		default:
			$content = "<p><span class=\"s8black\">Duplicate Login IPs</span></p>
<p class=\"s9black\">
Below are the IPs that have been logging in to multiple accounts. This doesn't always mean someone has more than one account, but will often be the case.</p>
<p class=\"s9black\">Cases where it may be ok is if you allow a household to have more than one account or perhaps someone from the same area as another member logged in and they were assigned an IP that once belonged to another member.</p>
<p class=\"s9black\">Judgement must be used for punitive action. <br />
  <br />
  <br />
</p>
<table class=\"tdata\" width=\"325\" cellpadding=\"0\" cellspacing=\"0\">
  <thead class=\"thdata\">
    <tr class=\"trdata\">
    <td class=\"tddata\" align=\"center\" width=\"125\"><span class=\"thdata\">IP</span></td>
    <td class=\"tddata\" align=\"center\" width=\"75\"><span class=\"thdata\">Number</span></td>
    <td class=\"tddata\" align=\"center\" width=\"125\"><span class=\"thdata\">Member IDs</span></td>
    </tr>
  </thead>";

			break;
	}

	return $content;

}

function get_menu($action) {


	switch ($action) {
		case "url":
			$menu = "<p><a href=\"/admin/admin.php?f=dupe&amp;action=\">Login IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=signup\">Signup IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=ppemail\">PayPal Email Address</a> | <b>URLs</b> | <a href=\"/admin/admin.php?f=dupe&amp;action=banners\">Banners</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=textads\">Text Ads</a></p>";
			break;

		case "banners":
			$menu = "<p><a href=\"/admin/admin.php?f=dupe&amp;action=\">Login IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=signup\">Signup IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=ppemail\">PayPal Email Address</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=url\">URLs</a> | <b>Banners</b> | <a href=\"/admin/admin.php?f=dupe&amp;action=textads\">Text Ads</a></p>";
			break;

		case "textads":
			$menu = "<p><a href=\"/admin/admin.php?f=dupe&amp;action=\">Login IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=signup\">Signup IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=ppemail\">PayPal Email Address</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=url\">URLs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=banners\">Banners</a> | <b>Text Ads</b></p>";
			break;

		case "signup":
			$menu = "<p><a href=\"/admin/admin.php?f=dupe&amp;action=\">Login IPs</a> | <b>Signup IPs</b> | <a href=\"/admin/admin.php?f=dupe&amp;action=ppemail\">PayPal Email Address</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=url\">URLs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=banners\">Banners</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=textads\">Text Ads</a></p>";
			break;

		case "ppemail":
			$menu = "<p><a href=\"/admin/admin.php?f=dupe&amp;action=\">Login IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=signup\">Signup IPs</a> | <b>PayPal Email Address</b> |  <a href=\"/admin/admin.php?f=dupe&amp;action=url\">URLs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=banners\">Banners</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=textads\">Text Ads</a></p>";
			break;

		Default:
			$menu = "<p><b>Login IPs</b> | <a href=\"/admin/admin.php?f=dupe&amp;action=signup\">Signup IPs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=ppemail\">PayPal Email Address</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=url\">URLs</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=banners\">Banners</a> | <a href=\"/admin/admin.php?f=dupe&amp;action=textads\">Text Ads</a></p>";
			break;

	}

	return $menu;
}

?>