<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;

}
-->
</style>
<br><br><center>
<?

//Split test page
if ($_GET['splittest'] == 1) {
	include("otosplittest.php");
	exit;
}

$add = $_GET['addgroup'];
$update = $_GET['editgroup'];
$delete = $_GET['deletegroup'];
$reset = $_GET['resetgroup'];


//Group Functions

//Add a group
if ($add == "yes") {
	$days = $_POST['days'];
	$acctype = $_POST['acctype'];
	@lfmsql_query("Insert into `".$prefix."oto_groups` (days, acctype, percent, highnum, lownum) values ($days, $acctype, 100, 100, 0)");
	$resmessage = "New Group Added";
}

//Update a group
elseif ($update == "yes") {
	$groupid = $_GET['groupid'];
	$days = $_POST['days'];
	$acctype = $_POST['acctype'];
	@lfmsql_query("Update `".$prefix."oto_groups` set days=$days, acctype=$acctype where id=$groupid limit 1");
	$resmessage = "Group Updated";
}

//Delete a group
elseif ($delete == "yes") {
	$confirmdelete = $_GET['confirmdelete'];
	$groupid = $_GET['groupid'];
	
	if ($confirmdelete == "yes") {
		
		$getsplittests = lfmsql_query("Select id from `".$prefix."oto_groups` where splitmirror=$groupid");
		if (lfmsql_num_rows($getsplittests) > 0) {
		for ($i = 0; $i < lfmsql_num_rows($getsplittests); $i++) {
			$splitid = lfmsql_result($getsplittests, $i, "id");
			@lfmsql_query("Delete from `".$prefix."oto_offers` where groupid=$splitid");
		}
		}
		@lfmsql_query("Delete from `".$prefix."oto_offers` where groupid=$groupid");
		@lfmsql_query("Delete from `".$prefix."oto_stats` where groupid=$groupid");
		@lfmsql_query("Delete from `".$prefix."oto_groups` where splitmirror=$groupid or id=$groupid");
		$resmessage = "Group Deleted";
	} else {
		echo("<b>Are you sure you want to delete this group?  All offers, stats, and split-testing groups within this group will be deleted.</b><br><br><a href=admin.php?f=oto&deletegroup=yes&groupid=$groupid&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=oto><b>No</b></a>");
		exit;
	}

}

//Reset a group
elseif ($reset == "yes") {
	$confirmreset = $_GET['confirmreset'];
	$groupid = $_GET['groupid'];
	
	if ($confirmreset == "yes") {
		@lfmsql_query("Delete from `".$prefix."oto_stats` where groupid=$groupid");
		@lfmsql_query("Update `".$prefix."oto_offers` set timesshown=0, timesbought=0, sales=0.00 where groupid=$groupid");
		$resmessage = "Group Reset";
	} else {
		echo("<b>When you reset a group, any members who have already seen these offers will see them again.  Are you sure you want to do this?</b><br><br><a href=admin.php?f=oto&resetgroup=yes&groupid=$groupid&confirmreset=yes><b>Yes</b></a><br><br><a href=admin.php?f=oto><b>No</b></a>");
		exit;
	}

}


//Offer Functions

$add = $_GET['addoffer'];
$update = $_GET['editoffer'];
$delete = $_GET['deleteoffer'];
$rank = $_GET['rank'];
$reset = $_GET['resetoffer'];

//Add an offer
if ($add == "yes") {
	$groupid = $_GET['groupid'];
	$offername = $_POST['offername'];
	$ipn = $_POST['ipn'];
	$text = $_POST['text'];
	
	$checklastrow = lfmsql_query("Select rank from `".$prefix."oto_offers` where groupid=$groupid order by rank desc");
	if (lfmsql_num_rows($checklastrow) > 0) {
		$lastrow = lfmsql_result($checklastrow, 0, "rank");
	} else {
		$lastrow = 0;
	}
	$newrank = $lastrow+1;
	
	@lfmsql_query("Insert into `".$prefix."oto_offers` (offername, rank, groupid, ipn, text) values ('$offername', $newrank, $groupid, '$ipn', '$text')");
	$resmessage = "Offer Added to Group ".$groupid;
}

//Update an offer
elseif ($update == "yes") {
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	$offername = $_POST['offername'];
	$ipn = $_POST['ipn'];
	$text = $_POST['text'];
	
	@lfmsql_query("Update `".$prefix."oto_offers` set offername='$offername', ipn='$ipn', text='$text' where groupid=$groupid and rank=$offerid limit 1");
	$resmessage = "Offer Updated";
}

//Reset an offer
elseif ($reset == "yes") {
	$confirmreset = $_GET['confirmreset'];
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	@lfmsql_query("Update `".$prefix."oto_offers` set timesshown=0, timesbought=0, sales=0.00 where groupid=$groupid and rank=$offerid limit 1");
	$resmessage = "Offer Reset";
}

//Delete an offer
elseif ($delete == "yes") {
	$confirmdelete = $_GET['confirmdelete'];
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	if ($confirmdelete == "yes") {
		@lfmsql_query("Delete from `".$prefix."oto_offers` where groupid=$groupid and rank=$offerid limit 1");
		$resmessage = "Offer Deleted";
	} else {
		echo("<b>Are you sure you want to delete this offer?  All stats for this offer will also be deleted.</b><br><br><a href=admin.php?f=oto&deleteoffer=yes&groupid=$groupid&offerid=$offerid&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=oto><b>No</b></a>");
		exit;
	}
}

//Change A Rank
elseif ($rank == "up" || $rank == "down") {
	
	$runchange = "yes";
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	
	$checkfirstrow = lfmsql_query("Select rank from `".$prefix."oto_offers` where groupid=$groupid order by rank asc");
	$firstrow = lfmsql_result($checkfirstrow, 0, "rank");
	
	$checklastrow = lfmsql_query("Select rank from `".$prefix."oto_offers` where groupid=$groupid order by rank desc");
	$lastrow = lfmsql_result($checklastrow, 0, "rank");
	
	if (($rank=="up") && ($offerid > $firstrow)) {
		$change=$offerid-1;
	} elseif (($rank=="down") && ($offerid < $lastrow)) {
		$change=$offerid+1;
	} else {
		$runchange = "no";
	}

	if ($runchange == "yes") {
		@lfmsql_query("Update `".$prefix."oto_offers` set rank=0 where groupid=$groupid and rank=$offerid");
		@lfmsql_query("Update `".$prefix."oto_offers` set rank=$offerid where groupid=$groupid and rank=$change");
		@lfmsql_query("Update `".$prefix."oto_offers` set rank=$change where groupid=$groupid and rank=0");
		@lfmsql_query("ALTER TABLE `".$prefix."oto_offers` ORDER BY rank;");
		$resmessage = "Rank Changed";
	}	

}


//Begin Main Page

$bgcolors = array("#C0C0C0", "#00FF66", "#CC9966", "#33CCCC", "#CCFF66");
$pickcolor = 0;

echo("<center>");

if ($resmessage != "") {
	echo("<p><font face=$fontface size=2>$resmessage</font></p>");
}

// echo("<p><b>Manage Groups</b> | <a href=\"admin.php?f=oto&splittest=1\">Stats and Split Testing</a></p>");

//Add New Group

echo("<table style=\"border: 2px #AAA solid;\" cellpadding=3 cellspacing=0>
<tr><td colspan=3 align=center><b>Add A New Group</b></td></tr>
<tr><td align=center><b>Days After Joining:</b></td>
<td align=center><b>Show To:</b></td>
<td align=center><b>&nbsp;</b></td></tr>");

echo("
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&addgroup=yes\">
<tr>
<td align=center><input type=text size=3 name=days value=0></td>

<td align=center><select name=acctype><option value=0>All Members</option>");

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
echo("<option value=$accid>$accname</option>");
}
echo("</select></td>

<td align=center><input type=submit value=Add></td></tr>
</table>
</form><br><br>");

//Get Existing Groups

$getgroups = lfmsql_query("Select * from `".$prefix."oto_groups` where splitmirror=0 order by days asc");
if (lfmsql_num_rows($getgroups) != 0) {
for ($i = 0; $i < lfmsql_num_rows($getgroups); $i++) {

$groupcolor = $bgcolors[$pickcolor];
$pickcolor = $pickcolor+1;
if ($pickcolor == count($bgcolors)) {
	$pickcolor = 0;
}

$groupnum = $i+1;
$groupid = lfmsql_result($getgroups, $i, "id");
$days = lfmsql_result($getgroups, $i, "days");
$acctype = lfmsql_result($getgroups, $i, "acctype");

echo("<table border=2 bgcolor=".$groupcolor." bordercolor=blue cellpadding=8 cellspacing=0>
<tr><td align=center><b>Group #$groupnum</b></td></tr>
<tr>

<td align=center>

<table border=0 cellpadding=0 cellspacing=7 width=550>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&editgroup=yes&groupid=$groupid\">
<tr>
<td><div class=\"lfm_descr\">Show To</div></td>
<td><select class=\"form-control\" name=acctype><option value=0>All Members</option>");

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
echo("<option value=$accid"); if($acctype==$accid){echo(" selected");} echo(">$accname</option>");
}
echo("</select></td>
<td><input class=\"form-control\" type=text size=2 name=days value=$days></td>
<td><div class=\"lfm_descr\">Days After Joining</div></td>
<td><input type=submit value=Update></td>
</tr>
</form>
</table>


</td>
</tr>

<tr>
<td align=center>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&deletegroup=yes&groupid=$groupid\">
<a href=\"admin.php?f=oto&resetgroup=yes&groupid=$groupid\"><input type=button onClick=\"window.location.href='admin.php?f=oto&resetgroup=yes&groupid=$groupid'\" value=\"Reset Group\"></a> &nbsp; <input type=submit value=\"Delete Group\"></td></tr>
</form>
<tr><td><br><br>");

//Get existing offers

$getoffers = lfmsql_query("Select * from `".$prefix."oto_offers` where groupid=$groupid order by rank asc");

if (lfmsql_num_rows($getoffers) != 0) {
for ($j = 0; $j < lfmsql_num_rows($getoffers); $j++) {

$offerid = lfmsql_result($getoffers, $j, "id");
$rank = lfmsql_result($getoffers, $j, "rank");
$ipn = lfmsql_result($getoffers, $j, "ipn");
$text = lfmsql_result($getoffers, $j, "text");
$offername = lfmsql_result($getoffers, $j, "offername");

$shownsum = lfmsql_result($getoffers, $j, "timesshown");
$buysum = lfmsql_result($getoffers, $j, "timesbought");
$salessum = lfmsql_result($getoffers, $j, "sales");

if ($shownsum > 0) {
	$avgbuys = $buysum/$shownsum;
	$avgbuys = $avgbuys*100;
	$avgbuys = round($avgbuys,1);
	$avgsales = $salessum/$shownsum;
	$avgsales = round($avgsales,2);
} else {
	$avgbuys = 0;
	$avgsales = 0.00;
}

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=5 align=center><b>Offer $rank</b><br><a href=\"admin.php?f=oto&rank=up&groupid=$groupid&offerid=$rank\">Move Up</a><br><a href=\"admin.php?f=oto&rank=down&groupid=$groupid&offerid=$rank\">Move Down</a><br><br>
<a href=\"admin.php?f=oto&groupid=$groupid&resetoffer=yes&offerid=$rank\">Reset Stats</a><br><br>
<a target=_blank href=\"$site_url/previewoffer.php?otoid=$offerid\">Preview Offer</a>
</td></tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&editoffer=yes&groupid=$groupid&offerid=$rank\">
<tr><td align=left colspan=5>Offer Name: <input type=text size=30 name=offername value=\"$offername\"> <input type=hidden name=ipn value=\"$ipn\"></td></tr>

<tr><td align=center>Views: $shownsum</td><td align=center>Purchases: $buysum</td><td align=center>Sales: $$salessum</td><td align=center>Conversion: $avgbuys%</td><td align=center>Avg. Sales/View: $$avgsales</td></tr>

<tr><td colspan=5 align=center valign=center>

<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center>
		<table border=0 cellpadding=3 cellspacing=0>
			<tr><td align=center colspan=2><b>Offer Macros</b></td></tr>
			<tr><td align=right><b>#IPNid#</b> </td><td align=left> Insert payment button</td></tr>
			<tr><td align=right><b>[themeheader]</b> </td><td align=left> Insert your site's header</td></tr>
			<tr><td align=right><b>[themefooter]</b> </td><td align=left> Insert your site's footer</td></tr>
			<tr><td align=right><b>#FIRSTNAME#</b> </td><td align=left> Member first name</td></tr>
			<tr><td align=right><b>#LASTNAME#</b> </td><td align=left> Member last name</td></tr>
		</table>
</td><td align=left valign=center><textarea name=text cols=50 rows=7>$text</textarea></td></tr></table>

</td></tr>
<tr><td colspan=5 align=center><input type=submit value=Edit>
</form>
<br>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&deleteoffer=yes&groupid=$groupid&offerid=$rank\">
<input type=submit value=\"Delete Offer\">
</td></tr>
</form></table><br><br><br>
");


}
}

//Add a new offer

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=2 align=center><b>Add A New Offer</b></td></tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&addoffer=yes&groupid=$groupid\">
<tr><td align=left colspan=2>Offer Name: <input type=text size=30 name=offername value=\"\"> <input type=hidden name=ipn value=0></td></tr>
<tr><td colspan=4 align=center valign=center>

<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center>
		<table border=0 cellpadding=3 cellspacing=0>
			<tr><td align=center colspan=2><b>Offer Macros</b></td></tr>
			<tr><td align=right><b>#IPNid#</b> </td><td align=left> Insert payment button</td></tr>
			<tr><td align=right><b>[themeheader]</b> </td><td align=left> Insert your site's header</td></tr>
			<tr><td align=right><b>[themefooter]</b> </td><td align=left> Insert your site's footer</td></tr>
			<tr><td align=right><b>#FIRSTNAME#</b> </td><td align=left> Member first name</td></tr>
			<tr><td align=right><b>#LASTNAME#</b> </td><td align=left> Member last name</td></tr>
		</table>
</td><td align=left valign=center><textarea name=text cols=50 rows=7></textarea><center><br>Your offer's decline link must go to \"showoto.php\".<br><xmp>Example: <a href=\"showoto.php\">No Thanks</a></xmp></center></td></tr></table>

</td></tr>
<tr><td colspan=2 align=center><input type=submit value=Add></td></tr>
</form></table><br><br><br>
");

echo("</td></tr></table><br><br><br>");


}
}

?>