<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

$add = $_GET['addsplittest'];
$update = $_GET['editgroup'];
$delete = $_GET['deletegroup'];
$reset = $_GET['resetgroup'];
$updaterotate = $_GET['editrotate'];

//Group Functions

//Add a split test
if ($add == "yes") {
	$groupname = $_POST['groupname'];
	
	$getmaingroup = lfmsql_query("Select * from `".$prefix."oto_groups` where id=".$_GET['selgroup']." limit 1");
	if (lfmsql_num_rows($getmaingroup) > 0) {
		$maindays = lfmsql_result($getmaingroup, 0, "days");
		$mainacc = lfmsql_result($getmaingroup, 0, "acctype");
		@lfmsql_query("Insert into `".$prefix."oto_groups` (groupname, splitmirror, days, acctype) values ('$groupname', ".$_GET['selgroup'].", $maindays, $mainacc)");
		$newgroupid = lfmsql_insert_id();
		
		$getoffers = lfmsql_query("Select * from `".$prefix."oto_offers` where groupid=".$_GET['selgroup']." order by rank asc");
		if (lfmsql_num_rows($getoffers) > 0) {
		for ($i = 0; $i < lfmsql_num_rows($getoffers); $i++) {
			$offername = trim(addslashes(lfmsql_result($getoffers, $i, "offername")));
			$rank = lfmsql_result($getoffers, $i, "rank");
			$ipn = lfmsql_result($getoffers, $i, "ipn");
			$text = trim(addslashes(lfmsql_result($getoffers, $i, "text")));
			
			@lfmsql_query("Insert into `".$prefix."oto_offers` (offername, groupid, rank, ipn, text) values ('$offername', $newgroupid, $rank, '$ipn', '$text')");
		}
		}
		
		$resmessage = "New Group Added";
	} else {
		$resmessage = "Invalid Group";
	}

}

//Update a group
elseif ($update == "yes") {
	$groupid = $_GET['groupid'];
	$groupname = $_POST['groupname'];
	
	@lfmsql_query("Update `".$prefix."oto_groups` set groupname='$groupname' where id=$groupid limit 1");
	$resmessage = "Group Updated";
}

//Delete a group
elseif ($delete == "yes") {
	$confirmdelete = $_GET['confirmdelete'];
	$groupid = $_GET['groupid'];
	if ($confirmdelete == "yes") {
		@lfmsql_query("Delete from `".$prefix."oto_offers` where groupid=$groupid");
		@lfmsql_query("Delete from `".$prefix."oto_groups` where id=$groupid");
		$resmessage = "Group Deleted";
	} else {
		echo("<b>Are you sure you want to delete this split test?  All offers and stats within this test will be deleted.</b><br><br><a href=admin.php?f=oto&splittest=1&deletegroup=yes&groupid=$groupid&selgroup=".$_GET['selgroup']."&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=oto&splittest=1><b>No</b></a>");
		exit;
	}
}

//Reset a group
elseif ($reset == "yes") {
	$confirmreset = $_GET['confirmreset'];
	$groupid = $_GET['groupid'];
	if ($confirmreset == "yes") {
		@lfmsql_query("Update `".$prefix."oto_offers` set timesshown=0, timesbought=0, sales=0.00 where groupid=$groupid");
		@lfmsql_query("Update `".$prefix."oto_groups` set groupshown=0, groupbought=0, groupsales=0.00 where id=$groupid limit 1");
		$resmessage = "Stats Reset";
	} else {
		echo("<b>Are you sure you want to reset the stats?  The stats for all related offers will also be reset.</b><br><br><a href=admin.php?f=oto&splittest=1&resetgroup=yes&groupid=$groupid&selgroup=".$_GET['selgroup']."&confirmreset=yes><b>Yes</b></a><br><br><a href=admin.php?f=oto&splittest=1><b>No</b></a>");
exit;
	}
}

//Update rotate percentages
elseif ($updaterotate == "yes") {

	$checkgroups = lfmsql_query("Select id, splitmirror from `".$prefix."oto_groups` order by id asc");
	
	for ($i = 0; $i < lfmsql_num_rows($checkgroups); $i++) {
		$checkid = lfmsql_result($checkgroups, $i, "id");
		$splitmirror = lfmsql_result($checkgroups, $i, "splitmirror");
		
		if ($splitmirror == 0) {
			$maingroup = $checkid;
		} else {
			$maingroup = $splitmirror;
		}
		
		$getrotatepost = 'rotate'.$checkid;
		$percent = $_POST[$getrotatepost];
		
		if ($percent != "") {
			
			if (($percent != 0) && ($percent < 1)) {
				$percent = 1;
			}
			if (is_numeric($percent) && $percent>=0 && $percent<=100) {
				$totala = $percent;
				$checkothers = lfmsql_query("Select percent from `".$prefix."oto_groups` where (id=$maingroup or splitmirror=$maingroup) and id!=$checkid");
				for ($j = 0; $j < lfmsql_num_rows($checkothers); $j++) {
					$thisa = lfmsql_result($checkothers, $j, "percent");
					$totala = $totala+$thisa;
				}
				if ($totala > 100) {
					$resmessage = "ERROR: The percent values for all offers should total 100.";
				} else {
					@lfmsql_query("Update `".$prefix."oto_groups` set percent=$percent where id=$checkid limit 1");
					$getoffer = lfmsql_query("select * from `".$prefix."oto_groups` where (id=$maingroup or splitmirror=$maingroup)");
					$numoffers = lfmsql_num_rows($getoffer);
					$cdownrand = 100;
					for ($j = 0; $j < $numoffers; $j++) {
						$offerid = lfmsql_result($getoffer, $j, "id");
						$offerpercent = lfmsql_result($getoffer, $j, "percent");
						$highnum = $cdownrand;
						$cdownrand = $cdownrand-$offerpercent;
						$lownum = $cdownrand;
						@lfmsql_query("Update `".$prefix."oto_groups` set highnum=$highnum, lownum=$lownum where id=$offerid limit 1");
					}
					$resmessage = "Rotate Percentages Updated";
					if ($cdownrand < 0) {
						@lfmsql_query("Update `".$prefix."oto_groups` set percent=0, highnum=0, lownum=0 where (id=$maingroup or splitmirror=$maingroup)");
						$resmessage = "An unknown error has occurred.  Please try updating the percentages for your offers again.";
					}
				}
			}
		}
	}
}


//Offer Functions

$add = $_GET['addoffer'];
$update = $_GET['editoffer'];
$delete = $_GET['deleteoffer'];
$rank = $_GET['rank'];
$reset = $_GET['resetoffer'];

//Add an offer
if ($add == "yes") {
	$groupid = $_GET['groupid'];
	$offername = $_POST['offername'];
	$ipn = $_POST['ipn'];
	$text = $_POST['text'];
	
	$checklastrow = lfmsql_query("Select rank from `".$prefix."oto_offers` where groupid=$groupid order by rank desc");
	if (lfmsql_num_rows($checklastrow) > 0) {
		$lastrow = lfmsql_result($checklastrow, 0, "rank");
	} else {
		$lastrow = 0;
	}
	$newrank = $lastrow+1;
	
	@lfmsql_query("Insert into `".$prefix."oto_offers` (offername, rank, groupid, ipn, text) values ('$offername', $newrank, $groupid, '$ipn', '$text')");
	$resmessage = "Offer Added to Group ".$groupid;
}

//Update an offer
elseif ($update == "yes") {
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	$offername = $_POST['offername'];
	$ipn = $_POST['ipn'];
	$text = $_POST['text'];
	
	@lfmsql_query("Update `".$prefix."oto_offers` set offername='$offername', ipn='$ipn', text='$text' where groupid=$groupid and rank=$offerid limit 1");
	$resmessage = "Offer Updated";
}

//Delete an offer
elseif ($delete == "yes") {
	$confirmdelete = $_GET['confirmdelete'];
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	if ($confirmdelete == "yes") {
		@lfmsql_query("Delete from `".$prefix."oto_offers` where groupid=$groupid and rank=$offerid limit 1");
		$resmessage = "Offer Deleted";
	} else {
		echo("<b>Are you sure you want to delete this offer?  All stats for this offer will also be deleted.</b><br><br><a href=admin.php?f=oto&splittest=1&deleteoffer=yes&groupid=$groupid&selgroup=".$_GET['selgroup']."&offerid=$offerid&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=oto&splittest=1><b>No</b></a>");
		exit;
	}
}

//Reset an offer
elseif ($reset == "yes") {
	$confirmreset = $_GET['confirmreset'];
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	if ($confirmreset == "yes") {
		@lfmsql_query("Update `".$prefix."oto_offers` set timesshown=0, timesbought=0, sales=0.00 where groupid=$groupid and rank=$offerid limit 1");
		$resmessage = "Offer Reset";
	} else {
		echo("<b>Are you sure you want to reset the stats for this offer?</b><br><br><a href=admin.php?f=oto&splittest=1&resetoffer=yes&groupid=$groupid&selgroup=".$_GET['selgroup']."&offerid=$offerid&confirmreset=yes><b>Yes</b></a><br><br><a href=admin.php?f=oto&splittest=1><b>No</b></a>");
		exit;
	}
}

//Change A Rank
elseif ($rank == "up" || $rank == "down") {
	
	$runchange = "yes";
	$groupid = $_GET['groupid'];
	$offerid = $_GET['offerid'];
	
	$checkfirstrow = lfmsql_query("Select rank from `".$prefix."oto_offers` where groupid=$groupid order by rank asc");
	$firstrow = lfmsql_result($checkfirstrow, 0, "rank");
	
	$checklastrow = lfmsql_query("Select rank from `".$prefix."oto_offers` where groupid=$groupid order by rank desc");
	$lastrow = lfmsql_result($checklastrow, 0, "rank");
	
	if (($rank=="up") && ($offerid > $firstrow)) {
		$change=$offerid-1;
	} elseif (($rank=="down") && ($offerid < $lastrow)) {
		$change=$offerid+1;
	} else {
		$runchange = "no";
	}
	
	if ($runchange == "yes") {
		@lfmsql_query("Update `".$prefix."oto_offers` set rank=0 where groupid=$groupid and rank=$offerid");
		@lfmsql_query("Update `".$prefix."oto_offers` set rank=$offerid where groupid=$groupid and rank=$change");
		@lfmsql_query("Update `".$prefix."oto_offers` set rank=$change where groupid=$groupid and rank=0");
		@lfmsql_query("ALTER TABLE `".$prefix."oto_offers` ORDER BY rank;");
		$resmessage = "Rank Changed";
	}

}



//Begin Main Page

echo("<center>");

if ($resmessage != "") {
echo("<p><font face=$fontface size=2>$resmessage</font></p>");
}

echo("<p><a href=\"admin.php?f=oto&splittest=0\">Manage Groups</a> | <b>Stats and Split Testing</b></p>");

//Select A Main Group
$getmains = lfmsql_query("Select * from `".$prefix."oto_groups` where splitmirror=0 order by days asc");
echo("<table border=1 bordercolor=blue cellpadding=5 cellspacing=0>
<tr><td align=center><b>Select A Group:</b></td></tr>");
if (lfmsql_num_rows($getmains) != 0) {
for ($i = 0; $i < lfmsql_num_rows($getmains); $i++) {
$groupnum = $i+1;
$groupid = lfmsql_result($getmains, $i, "id");
$groupdays = lfmsql_result($getmains, $i, "days");
echo("<tr><td ");
if ($_GET['selgroup'] == $groupid) {
echo("bgcolor=#EEEEEE ");
}
echo("align=center><a href=\"admin.php?f=oto&splittest=1&selgroup=$groupid\"><b>Group $groupnum</b> Day $groupdays</a></td></tr>");
}
} else {
echo("<tr><td align=center>You have no OTO groups</td></tr>");
}
echo("</table><br><br>");
//End Select Group


if ($_GET['selgroup'] > 0) {

$selgroup = $_GET['selgroup'];
$groupid = $selgroup;

//Show Stats Summary
echo("<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&editrotate=yes&groupid=$groupid&selgroup=$selgroup\">
<table border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=7 align=center><b>Stats Summary</b></td></tr>
<tr><td align=center>Split Test</td><td align=center>Num. Views</td><td align=center>Num. Purchases</td><td align=center>Total Sales</td><td align=center>Conversion</td><td align=center>Avg. Sales per View</td><td align=center>Rotation %</td></tr>");

$getrotate = lfmsql_query("Select percent, groupshown, groupbought, groupsales from `".$prefix."oto_groups` where id=$selgroup");
$rotate = lfmsql_result($getrotate, 0, "percent");
$shownsum = lfmsql_result($getrotate, 0, "groupshown");
$buysum = lfmsql_result($getrotate, 0, "groupbought");
$salessum = lfmsql_result($getrotate, 0, "groupsales");

if ($shownsum > 0) {
$avgbuys = $buysum/$shownsum;
$avgbuys = $avgbuys*100;
$avgbuys = round($avgbuys,1);
$avgsales = $salessum/$shownsum;
$avgsales = round($avgsales,2);
} else {
$avgbuys = 0;
$avgsales = 0.00;
}

echo("<tr><td align=center>Main Group</td><td align=center>$shownsum</td><td align=center>$buysum</td><td align=center>$$salessum</td><td align=center>$avgbuys%</td><td align=center>$$avgsales</td><td align=center><input type=text size=3 name=rotate$selgroup value=$rotate></td></tr>");


//Get split test groups

$getsubgroups = lfmsql_query("Select id, groupname, percent, groupshown, groupbought, groupsales from `".$prefix."oto_groups` where splitmirror=$selgroup order by id asc");

if (lfmsql_num_rows($getsubgroups) != 0) {
for ($i = 0; $i < lfmsql_num_rows($getsubgroups); $i++) {

$groupid = lfmsql_result($getsubgroups, $i, "id");
$groupname = lfmsql_result($getsubgroups, $i, "groupname");
$rotate = lfmsql_result($getsubgroups, $i, "percent");
$shownsum = lfmsql_result($getsubgroups, $i, "groupshown");
$buysum = lfmsql_result($getsubgroups, $i, "groupbought");
$salessum = lfmsql_result($getsubgroups, $i, "groupsales");

if ($shownsum > 0) {
$avgbuys = $buysum/$shownsum;
$avgbuys = $avgbuys*100;
$avgbuys = round($avgbuys,1);
$avgsales = $salessum/$shownsum;
$avgsales = round($avgsales,2);
} else {
$avgbuys = 0;
$avgsales = 0.00;
}

echo("<tr><td align=center>$groupname</td><td align=center>$shownsum</td><td align=center>$buysum</td><td align=center>$$salessum</td><td align=center>$avgbuys%</td><td align=center>$$avgsales</td><td align=center><input type=text size=3 name=rotate$groupid value=$rotate></td></tr>");

}
echo("<tr><td align=center colspan=6>&nbsp;</td><td align=center><input type=submit name=submit value=\"Update\"></td></tr>");
}

echo("</table></form><br><br>");
//End Split-Test Summary


//Add New Split-Test Group

echo("<table border=1 bordercolor=green cellpadding=3 cellspacing=0>
<tr><td colspan=3 align=center><b>Create A New Split Test</b></td></tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&selgroup=$selgroup&addsplittest=yes\">
<tr><td align=center>Split Test Name: <input type=text name=groupname value=\"\"><br>
<input type=submit value=Create></td></tr>
</table>
</form><br><br>");




//Get Main Group
$getgroups = lfmsql_query("Select * from `".$prefix."oto_groups` where id=$selgroup limit 1");
if (lfmsql_num_rows($getgroups) != 0) {
$groupid = $selgroup;
echo("<table border=2 bordercolor=blue cellpadding=8 cellspacing=0>
<tr><td colspan=4 align=center><b>Main Group</b><br><a href=\"admin.php?f=oto&splittest=1&groupid=$groupid&selgroup=$selgroup&resetgroup=yes\">Reset Split Test Stats</a></td></tr>

<tr><td colspan=4 align=center>
");

//Get existing offers

$getoffers = lfmsql_query("Select * from `".$prefix."oto_offers` where groupid=$groupid order by rank asc");

if (lfmsql_num_rows($getoffers) != 0) {
for ($j = 0; $j < lfmsql_num_rows($getoffers); $j++) {

$offerid = lfmsql_result($getoffers, $j, "id");
$rank = lfmsql_result($getoffers, $j, "rank");
$ipn = lfmsql_result($getoffers, $j, "ipn");
$text = lfmsql_result($getoffers, $j, "text");
$offername = lfmsql_result($getoffers, $j, "offername");

$getshownsum = lfmsql_query("Select timesshown from `".$prefix."oto_offers` where groupid=$groupid and rank=$rank");
$shownsum = lfmsql_result($getshownsum, 0, "timesshown");
$getbuysum = lfmsql_query("Select timesbought from `".$prefix."oto_offers` where groupid=$groupid and rank=$rank");
$buysum = lfmsql_result($getbuysum, 0, "timesbought");
$getsalessum = lfmsql_query("Select sales from `".$prefix."oto_offers` where groupid=$groupid and rank=$rank");
$salessum = lfmsql_result($getsalessum, 0, "sales");

if ($shownsum > 0) {
$avgbuys = $buysum/$shownsum;
$avgbuys = $avgbuys*100;
$avgbuys = round($avgbuys,1);
$avgsales = $salessum/$shownsum;
$avgsales = round($avgsales,2);
} else {
$avgbuys = 0;
$avgsales = 0.00;
}

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=5 align=center><b>Offer $rank</b><br><a href=\"admin.php?f=oto&splittest=1&rank=up&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">Move Up</a><br><a href=\"admin.php?f=oto&splittest=1&rank=down&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">Move Down</a><br><br><a target=_blank href=\"$site_url/previewoffer.php?otoid=$offerid\">Preview Offer</a><br><br><a href=\"admin.php?f=oto&splittest=1&groupid=$groupid&selgroup=$selgroup&resetoffer=yes&offerid=$rank\">Reset Offer Stats</a></td></tr>

<tr><td align=center>Views: $shownsum</td><td align=center>Purchases: $buysum</td><td align=center>Sales: $$salessum</td><td align=center>Conversion: $avgbuys%</td><td align=center>Avg. Sales/View: $$avgsales</td></tr>

<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&editoffer=yes&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">
<tr><td align=left colspan=5>Offer Name: <input type=text size=30 name=offername value=\"$offername\"></td></tr>
<tr><td align=left colspan=5>IPN ID: <input type=text size=30 name=ipn value=\"$ipn\"></td></tr>
<tr><td colspan=5 align=center valign=center>

<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7>$text</textarea></td></tr></table>

</td></tr>
<tr><td colspan=5 align=center><input type=submit value=Edit>
</form>
<br>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&deleteoffer=yes&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">
<input type=submit value=\"Delete Offer\">
</td></tr>
</form></table><br><br><br>
");


}
}

//Add a new offer

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=2 align=center><b>Add A New Offer</b></td></tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&addoffer=yes&groupid=$groupid&selgroup=$selgroup\">
<tr><td align=left colspan=2>Offer Name: <input type=text size=30 name=offername value=\"\"></td></tr>
<tr><td align=left colspan=2>IPN ID: <input type=text size=30 name=ipn value=0></td></tr>
<tr><td colspan=4 align=center valign=center>

<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7></textarea></td></tr></table>

</td></tr>
<tr><td colspan=2 align=center><input type=submit value=Add></td></tr>
</form></table><br><br><br>
");

echo("</td></tr></table><br><br><br>");

}





//Get Split Test Groups

$getgroups = lfmsql_query("Select * from `".$prefix."oto_groups` where splitmirror=$selgroup order by id asc");
if (lfmsql_num_rows($getgroups) != 0) {
for ($i = 0; $i < lfmsql_num_rows($getgroups); $i++) {

$groupid = lfmsql_result($getgroups, $i, "id");
$groupname = lfmsql_result($getgroups, $i, "groupname");

echo("<table border=2 bordercolor=green cellpadding=8 cellspacing=0>
<tr><td colspan=4 align=center><b>$groupname</b><br><a href=\"admin.php?f=oto&splittest=1&groupid=$groupid&selgroup=$selgroup&resetgroup=yes\">Reset Split Test Stats</a><br><br><a href=\"admin.php?f=oto&splittest=1&groupid=$groupid&selgroup=$selgroup&deletegroup=yes\">Delete Split Test</a></td></tr>

<tr><td colspan=4 align=center>");

//Get existing offers

$getoffers = lfmsql_query("Select * from `".$prefix."oto_offers` where groupid=$groupid order by rank asc");

if (lfmsql_num_rows($getoffers) != 0) {
for ($j = 0; $j < lfmsql_num_rows($getoffers); $j++) {

$offerid = lfmsql_result($getoffers, $j, "id");
$rank = lfmsql_result($getoffers, $j, "rank");
$ipn = lfmsql_result($getoffers, $j, "ipn");
$text = lfmsql_result($getoffers, $j, "text");
$offername = lfmsql_result($getoffers, $j, "offername");

$getshownsum = lfmsql_query("Select timesshown from `".$prefix."oto_offers` where groupid=$groupid and rank=$rank");
$shownsum = lfmsql_result($getshownsum, 0, "timesshown");
$getbuysum = lfmsql_query("Select timesbought from `".$prefix."oto_offers` where groupid=$groupid and rank=$rank");
$buysum = lfmsql_result($getbuysum, 0, "timesbought");
$getsalessum = lfmsql_query("Select sales from `".$prefix."oto_offers` where groupid=$groupid and rank=$rank");
$salessum = lfmsql_result($getsalessum, 0, "sales");

if ($shownsum > 0) {
$avgbuys = $buysum/$shownsum;
$avgbuys = $avgbuys*100;
$avgbuys = round($avgbuys,1);
$avgsales = $salessum/$shownsum;
$avgsales = round($avgsales,2);
} else {
$avgbuys = 0;
$avgsales = 0.00;
}

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=5 align=center><b>Offer $rank</b><br><a href=\"admin.php?f=oto&splittest=1&rank=up&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">Move Up</a><br><a href=\"admin.php?f=oto&splittest=1&rank=down&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">Move Down</a><br><br><a target=_blank href=\"$site_url/previewoffer.php?otoid=$offerid\">Preview Offer</a><br><br><a href=\"admin.php?f=oto&splittest=1&groupid=$groupid&selgroup=$selgroup&resetoffer=yes&offerid=$rank\">Reset Offer Stats</a></td></tr>

<tr><td align=center>Views: $shownsum</td><td align=center>Purchases: $buysum</td><td align=center>Sales: $$salessum</td><td align=center>Conversion: $avgbuys%</td><td align=center>Avg. Sales/View: $$avgsales</td></tr>

<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&editoffer=yes&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">
<tr><td align=left colspan=5>Offer Name: <input type=text size=30 name=offername value=\"$offername\"></td></tr>
<tr><td align=left colspan=5>IPN ID: <input type=text size=30 name=ipn value=\"$ipn\"></td></tr>
<tr><td colspan=5 align=center valign=center>

<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7>$text</textarea></td></tr></table>

</td></tr>
<tr><td colspan=5 align=center><input type=submit value=Edit>
</form>
<br>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&deleteoffer=yes&groupid=$groupid&selgroup=$selgroup&offerid=$rank\">
<input type=submit value=\"Delete Offer\">
</td></tr>
</form></table><br><br><br>
");


}
}

//Add a new offer

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=2 align=center><b>Add A New Offer</b></td></tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=oto&splittest=1&addoffer=yes&groupid=$groupid&selgroup=$selgroup\">
<tr><td align=left colspan=2>Offer Name: <input type=text size=30 name=offername value=\"\"></td></tr>
<tr><td align=left colspan=2>IPN ID: <input type=text size=30 name=ipn value=0></td></tr>
<tr><td colspan=4 align=center valign=center>

<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7></textarea></td></tr></table>

</td></tr>
<tr><td colspan=2 align=center><input type=submit value=Add></td></tr>
</form></table><br><br><br>
");

echo("</td></tr></table><br><br><br>");


}
}


}

?>