<?
require_once "../inc/filter.php";


	session_start();

	include "../inc/checkauth.php";

	$itemid=$_GET["pid"];
	
	$sres=@mysql_query("SELECT sitename,paypal_enable,paypal_email,pptestmode,pptestemail,_2co_enable,_2co_id,authnet_enable,authnet_login,authnet_key,pp_curr FROM ".$prefix."settings");
	$srow=@mysql_fetch_array($sres);
	
	$prodres=@mysql_query("SELECT * FROM ".$prefix."products WHERE itemid='".$itemid."'");
	$prodrow=@mysql_fetch_array($prodres);
	
	// Get our local variables
	$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	$root_path=substr($fullurl,0,strrpos($fullurl, "/"));
	$root_path=substr($root_path,0,strrpos($root_path, "/"));

	if($srow["pptestmode"] == 0)
	{
		$paypal_email=$srow["paypal_email"];
		$paypal_action="https://www.paypal.com/cgi-bin/webscr";	
        $authnet_url="https://secure.authorize.net/gateway/transact.dll";
	}
	else
	{
		// Testmode
		$paypal_email=$srow["pptestemail"];
		$paypal_action="https://www.sandbox.paypal.com/cgi-bin/webscr";
		$_2CHECKOUT_DEMO="Y";
        $authnet_url="https://certification.authorize.net/gateway/transact.dll";
	}
	
	$_2co_id=$srow["_2co_id"];
	$_2co_productid=$prodrow["_2co_id"];
	$sitename=$srow["sitename"];
	$item_name=$prodrow["productname"];
	$item_number=$prodrow["itemid"];
	$item_price=$prodrow["price"];
    $authnet_login=$srow["authnet_login"];
    $authnet_key=$srow["authnet_key"];
    $pp_curr=$srow["pp_curr"];
    	
    if(strlen($realscriptpath) > 10)
    {
        $notifyurl=$realscriptpath."/admin/prod_ipn.php";
        $returnurl=$realscriptpath."/members.php";
        $relayurl=$realscriptpath."/thankyou.php";
        $loginurl=$realscriptpath."/login.php?s=noauth";
   }
    else
    {
        $notifyurl=$root_path."/admin/prod_ipn.php";
	    $returnurl=$root_path."/members.php";
        $relayurl=$root_path."/thankyou.php";
        $loginurl=$root_path."/login.php?s=noauth";
    }

////////////////////////////////////
// BEGIN PAYMENT PROCESSOR FORM CODE
////////////////////////////////////

// This is the block of code that will replace the #PAYMENT# macro
////////////////////////////////
// PayPal Form
////////////////////////////////
$ppblock="====================
PAYPAL FORM
====================";
$ppblock.="
---------- Copy below this line --------------------------
<form action=\"".$paypal_action."\" method=\"post\">
<input type=\"hidden\" name=\"lc\" value=\"US\">
<input type=\"hidden\" name=\"cmd\" value=\"_xclick\">
<input type=\"hidden\" name=\"business\" value=\"[paypal_email]\">
<input type=\"hidden\" name=\"item_name\" value=\"[item_name]\">
<input type=\"hidden\" name=\"item_number\" value=\"[item_number]\">
<input type=\"hidden\" name=\"amount\" value=\"[item_price]\">
<input type=\"hidden\" name=\"no_shipping\" value=\"2\">
<input type=\"hidden\" name=\"return\" value=\"[returnurl]\">
<input type=\"hidden\" name=\"notify_url\" value=\"[notifyurl]\">
<input type=\"hidden\" name=\"custom\" value=\"#AFFILIATEID#\">
<input type=\"hidden\" name=\"no_note\" value=\"1\">
<input type=\"hidden\" name=\"currency_code\" value=\"[pp_curr]\">
<input type=\"hidden\" name=\"oto_num\" value=\"0\">
<input type=\"hidden\" name=\"bn\" value=\"PP-BuyNowBF\">
<input type=\"image\" src=\"images/order.gif\" border=\"0\" name=\"submit\" alt=\"Order Now!\">
<img alt=\"\" border=\"0\" src=\"https://www.paypal.com/en_AU/i/scr/pixel.gif\" width=\"1\" height=\"1\">
</form>
---------- Copy above this line --------------------------
";
////////////////////////////////
// End PayPal Form
////////////////////////////////
$ppblock.="====================
2CHECKOUT FORM
====================";
////////////////////////////////
// 2Checkout Form
////////////////////////////////
$ppblock.="
---------- Copy below this line --------------------------
<form action=\"https://www.2checkout.com/2co/buyer/purchase\" method=\"post\">
<input type=\"hidden\" name=\"sid\" value=\"[_2co_id]\">
<input type=\"hidden\" name=\"product_id\" value=\"[_2co_productid]\">
<input type=\"hidden\" name=\"quantity\" value=\"1\">
<input type=\"hidden\" name=\"lfm_mem_id\" value=\"#AFFILIATEID#\">
<input type=\"hidden\" name=\"lfm_prod_id\" value=\"1\">
<input type=\"hidden\" name=\"fixed\" value=\"Y\">
<input type=\"hidden\" name=\"oto_num\" value=\"0\">";
//demo mode?
if($_2CHECKOUT_DEMO=="Y")
{
$ppblock.="<input type=\"hidden\" name=\"demo\" value=\"Y\">";
}//end demo check

$ppblock.="<input name=\"submit\" type=\"image\" src=\"images/payment_2co.gif\" alt=\"Make Payment\" border=\"0\">
<br>
<font size=\"1\">2CheckOut.com Inc. (Ohio, USA) is<br>an authorized retailer for<br>goods and services provided by<br>[sitename]
.</font>
</form>
---------- Copy above this line --------------------------
";
////////////////////////////////
// End 2CO Form
////////////////////////////////
$ppblock.="====================
Authorize.Net FORM
====================";
////////////////////////////////
// Authorize.net Form
////////////////////////////////

include "../inc/authnet_simlib.php";

$ppblock.="
---------- Copy below this line --------------------------
<form action=\"$authnet_url\" method=\"post\">";
// Insert the form elements required for SIM by calling InsertFP
$ppblock.="<input type=\"hidden\" name=\"x_login\" value=\"[authnet_login]\">
[authnet_fields]
<input type=\"hidden\" name=\"x_show_form\" value=\"PAYMENT_FORM\">
<input type=\"hidden\" name=\"item_number\" value=\"[item_number]\"> 
<input type=\"hidden\" name=\"x_description\" value=\"[item_name]\">
<input type=\"hidden\" name=\"x_amount\" value=\"[item_price]\">
<input type=\"hidden\" name=\"x_relay_response\" value=\"True\">
<input type=\"hidden\" name=\"x_relay_url\" value=\"[relay_url]\">
<input type=\"hidden\" name=\"loginurl\" value=\"[loginurl]\">
<input type=\"hidden\" name=\"x_custom\" value=\"#AFFILIATEID#\">
<input type=\"hidden\" name=\"lfm_sale_id\" value=\"#AFFILIATEID#\">
<input type=\"hidden\" name=\"lfm_prod_id\" value=\"1\">
<input type=\"hidden\" name=\"oto_num\" value=\"\">";

//demo mode?
if($_2CHECKOUT_DEMO=="Y"){
$ppblock.="\n"."<input type=\"hidden\" name=\"x_test_request\" value=\"TRUE\">";
}
$ppblock.="\n"."<input name=\"submit\" type=\"image\" src=\"images/payment_authnet.gif\" alt=\"Make Payment\" border=\"0\">";
$ppblock.="\n"."</form>
---------- Copy above this line --------------------------";
////////////////////////////////
// End Auth.net form
////////////////////////////////

//////////////////////////////////
// END PAYMENT PROCESSOR FORM CODE
//////////////////////////////////

	// Translate the various payment tags
	$ppblock=str_replace("[paypal_email]", $paypal_email, $ppblock);
	$ppblock=str_replace("[item_name]", $item_name, $ppblock);
	$ppblock=str_replace("[item_number]", $item_number, $ppblock);
	$ppblock=str_replace("[item_price]", $item_price, $ppblock);
	$ppblock=str_replace("[returnurl]", $returnurl, $ppblock);
	$ppblock=str_replace("[notifyurl]", $notifyurl, $ppblock);
	$ppblock=str_replace("[userid]", $_SESSION[userid], $ppblock);
	$ppblock=str_replace("[_2co_id]", $_2co_id, $ppblock);
	$ppblock=str_replace("[_2co_productid]", $_2co_productid, $ppblock);
    $ppblock=str_replace("[authnet_login]", $authnet_login, $ppblock);
    $ppblock=str_replace("[relay_url]", $relayurl, $ppblock);
    $ppblock=str_replace("[loginurl]", $loginurl, $ppblock);
    $ppblock=str_replace("[pp_curr]", $pp_curr, $ppblock);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>
<body>

<textarea name="textarea" cols="90" rows="50"><? echo $ppblock; ?></textarea>
</body>
</html>
