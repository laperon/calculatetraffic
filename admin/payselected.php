<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

if(!isset($_POST["memcheck"])) {
	include "payall.php";
	exit;
}

	// Prevent anyone who isn't logged in from opening this page
	if(!isset($_SESSION["adminid"])) { exit; };

	if(!isset($_POST["CreateFile"]))
	{
		// Get the current max id in case a sale comes in while we're processing
		// We only want this once - then we'll post it to create the file
		$res=@lfmsql_query("SELECT MAX(salesid) as msalesid FROM ".$prefix."sales") or die(lfmsql_error());
		$row=@lfmsql_fetch_array($res) or die(lfmsql_error());
		$msalesid=$row["msalesid"];
	}


		

?>
<script language="JavaScript">
  DatePickerControl.onSelect = function(inputid)
  {
    document.forms.ppfile.submit();
	}
  </script>
<p>&nbsp;</p>
<p align="center"><strong><font size="4" face="Verdana, Arial, Helvetica, sans-serif">Pay Selected Members</font></strong></p>
<div align="center">
  <p>
    <?
	// We only want this form and table to display before the file is created
	if(isset($_POST["CreateFile"]))
{
?>
    <font face="Verdana, Arial, Helvetica, sans-serif">P</font><font face="Verdana, Arial, Helvetica, sans-serif">ayment details have been emailed to your admin address.<br />
    </font><? 
} else {
?>
  </p>
  
    <script language="javascript">
    	var paymentsarray = [];
    </script>
  
  <form name="ppfile" method="post" action="admin.php?f=ps">
  <?
 
  ?>
<input type="hidden" name="msalesid" value="<?=$msalesid;?>"> 
  <table>
  <tr>
  <td colspan="3" align="center">Pay To:
    <input name="msaledate" onchange="this.form.submit()" type="text" id="DPC_edate_YYYY-MM-DD" value="<? if (isset($_REQUEST[msaledate])){echo $_REQUEST[msaledate]; } else { echo date("Y-m-d");} ?>" width="80"> </td>
</tr>
    <tr>
      <td align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Name</strong></font></td>
      <td align="center"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Email</font></strong></td>
      <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Amount</strong></font></td>
      <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Pay?</strong></font></td>
    </tr>
<? 
$min_comm=get_setting("min_comm");
// Get the sales for the selected people
$cnt=0;
while (list ($key,$val) = @each ($_REQUEST["memcheck"])) 
{

	$dqry="SELECT Id FROM ".$prefix."members WHERE Id=".$val;
	$dres=@lfmsql_query($dqry) or die(lfmsql_error());
	$drow=@lfmsql_fetch_array($dres) or die(lfmsql_error());
	$id=$drow["Id"];
	if (isset($_REQUEST['msaledate'])){
		$date_query = " (saledate < '".$_REQUEST['msaledate']. " 23:59:59' OR ".$prefix."sales.commission < 0) AND ";
	}
	$sqry="SELECT SUM(".$prefix."sales.commission) AS payout,saledate,Id,firstname,lastname,paypal_email, ".$prefix."members.status AS memberstatus FROM ".$prefix."sales LEFT JOIN ".$prefix."members ON ".$prefix."members.Id=".$prefix."sales.affid WHERE ".$date_query." salesid <= $msalesid AND(".$prefix."sales.status IS NULL OR ".$prefix."sales.status = 'R' ) AND ".$prefix."sales.commission != 0  AND affid=$id GROUP BY affid ORDER BY saledate";
	$sres=lfmsql_query($sqry) or die(lfmsql_error());

	// Fetch the records and display table
	while($srow=lfmsql_fetch_array($sres))
	{ 
		if($srow["payout"] >= $min_comm)
		{
			$cnt++;
	?>	
	
<script language="javascript">
	var paymentamount = parseFloat("<?=$srow["payout"];?>");
	paymentsarray.push(paymentamount);
</script>
	
    <tr>
      <td align="left">
           <font size="2" face="Verdana, Arial, Helvetica, sans-serif">
        <?=$srow["firstname"]." ".$srow["lastname"]; if ($srow["memberstatus"] == "Suspended") { echo(" (Suspended)"); } ?>
      </font></td>
      <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
        <?=$srow["paypal_email"];?>
      </font></td>
      <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
        <?=$srow["payout"];?>
      </font></td>
       <td align="center"><input onclick="updatePayTotal();" id="pay_checkbox<? echo($cnt); ?>" name="pay_id[]" type="checkbox" value="<?=$srow["Id"];?>" 
      	<? if ($srow["paypal_email"] != "" && $srow["memberstatus"] != "Suspended") { ?> checked="checked" <? } ?> 
        /></td>
    </tr>
	<? 
		}
	} 
	
	$sqry="SELECT SUM(".$prefix."sales.commission) AS payout,saledate,firstname,lastname,paypal_email FROM ".$prefix."sales LEFT JOIN ".$prefix."members ON ".$prefix."members.Id=".$prefix."sales.affid WHERE salesid <= $msalesid AND ".$prefix."sales.status IS NULL AND affid=$id AND ".$prefix."sales.commission > 0 GROUP BY affid ORDER BY saledate";
	$sres=lfmsql_query($sqry) or die(lfmsql_error());

	// Fetch the records and display table
	while($srow=lfmsql_fetch_array($sres))
	{ 
	
	?>	
    <tr>
      <td align="left">	<input type="hidden" value="<?=$val;?>" name="memcheck[]" /></td>
    </tr>
	<? 
		
	} 
}

	?>
	<tr>
	   <td colspan="3" align="right"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Total To Pay: </b></font></td>
	   <td align="left"><div id="paytotalval"></div></td>
	</tr>
	<?
	
if($cnt == 0) {
?>
<tr><td colspan="3" align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">No payments found</font></td>
</tr>
<? } ?>

<tr>
  <td colspan="3" align="center">&nbsp;</td>
</tr>

<tr>
<td colspan="4" align="center">
<input name="CreateFile" type="submit" id="CreateFile" value="Create PayPal File" /></td>
</tr>
</table>
</form>

<script language="javascript">
	function updatePayTotal() {
		var arrlength = paymentsarray.length;
		var paytotal = 0.00;
		if (arrlength > 0) {
			for (var i = 0; i < arrlength; i++) {
				checkboxid = i + 1;
				checkboxname = "pay_checkbox" + checkboxid;
				if (document.getElementById(checkboxname).checked) {
					paytotal = paytotal + paymentsarray[i];
				}
			}
		}
		document.getElementById("paytotalval").innerHTML = '<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>' + paytotal.toFixed(2) + '</b></font>';
	}
	updatePayTotal();
</script>

<?
}
?>

</div>
