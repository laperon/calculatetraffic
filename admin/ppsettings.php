<?
///////////////////////////////////////////////////////////////////////////////////////////////
// Simole Prizepage Clone Mod v1.0 written By Keith Janet 
// Copyright 2015 - ALL RIGHTS RESERVED!
// This Mod consists of several files - all are covered by this copyright
// This is not free software - Do NOT Copy, Transfer, Modify, or Sell! 
// Licenses and Installation Available
// Contact: computrec@yahoo.com
// http://www.LFMTEmods.com
////////////////////////////////////////////////////////////////////////////////////////////////

$date = date("Y-m-d");
$title = "Prizepage Settings";

if (isset($_POST["Edit"])) {
$rmaxu1 = $_POST["rmax1"];
$rmaxu2 = $_POST["rmax2"];
$rmaxu3 = $_POST["rmax3"];
$rmaxu4 = $_POST["rmax4"];
$rminu1 = $_POST["rmin1"];
$rminu2 = $_POST["rmin2"];
$rminu3 = $_POST["rmin3"];
$rminu4 = $_POST["rmin4"];
$rpctu1 = $_POST["rpct1"];
$rpctu2 = $_POST["rpct2"];
$rpctu3 = $_POST["rpct3"];
$rpctu4 = $_POST["rpct4"];
$actu = $_POST["act"];

mysql_query("UPDATE oto_prizesettings SET max1='$rmaxu1', max2='$rmaxu2', max3='$rmaxu3', max4='$rmaxu4', min1='$rminu1', min2='$rminu2', min3='$rminu3', min4='$rminu4', pct1='$rpctu1', pct2='$rpctu2', pct3='$rpctu3', pct4='$rpctu4', active='$actu' WHERE id='1'") or die("Update Failed!" . mysql_error());
echo "<center><font color=red><h4>Settings Updated</h4></font></center>";
}
$mqry = mysql_query("SELECT * from oto_prizesettings WHERE id='1'") or die(mysql_error());
$mres = mysql_fetch_array($mqry);

$max1 = $mres["max1"];
$max2 = $mres["max2"];
$max3 = $mres["max3"];
$max4 = $mres["max4"];
$min1 = $mres["min1"];
$min2 = $mres["min2"];
$min3 = $mres["min3"];
$min4 = $mres["min4"];
$pct1 = $mres["pct1"];
$pct2 = $mres["pct2"];
$pct3 = $mres["pct3"];
$pct4 = $mres["pct4"];
$act = $mres["active"];

/*if ($type1 == 'cash') {
$max1 = $max1 . ".00";
}
*/
$def = "0";
$def1 = "0.00";
$qry1 = mysql_query("select SUM(amount) AS cash from winners where type='cash'");
if (($qry1 < '0.01') || ($qry1 == '')) { $cash='0.00'; } else {
   $cash = mysql_result($qry1, 0, "cash");
   $cash = round($cash,2);
}
$qry2 = mysql_query("select SUM(amount) AS cred from winners where type='credits'");
if ($qry2 == '') { $cred='$def'; } else {
   $cred = mysql_result($qry2, 0, "cred");
}

$qry3 = mysql_query("select SUM(amount) AS ban from winners where type='banners'"); 
if ($qry3 == '') { $ban='$def'; } else {
   $ban = mysql_result($qry3, 0, "ban");
} 

$qry4 = mysql_query("select SUM(amount) AS txt from winners where type='text'"); 
if ($qry4 == '') { $txt='$def'; } else {
   $txt = mysql_result($qry4, 0, "txt"); 
}

?>
<center>
<table width=760 border=0>
<tr>
<td align=center width=760>
<center><h3>Prize Page Winnings</h3></center>
</td>
</tr>
</table>
<table width=750 border=0>
<th width=100><u>Total Cash Given</u></th><th width=100><u>Total Credits Given</u></th><th width=100><u>Total Banners Given</u></th><th width=100><u>Total Text Ads Given</u></th>
</tr>

<tr>
<td width=100><center><?=$cash;?></td><td width=100><center><?=$cred;?></td><td width=100><center><?=$ban;?></td><td width=100><center><?=$txt;?></td>
</tr>
<tr><td>&nbsp; &nbsp;</td></tr>
</table>
<table width=760 border=0>
<tr>
<td align=center width=760>
<center><h3><?=$title;?></h3><h4>These are your current settings</h4>
</td>
</tr>
</table>

<table width=750 border=0>
<th width=100>Cash Minimum Prize</th> <th width=100>Credits Minimum Prize</th><th width=100>Banners Minimum Prize</th><th width=100>Text Minimum Prize</th><th width=100>Active (yes/no)</th>
</tr>
<tr>
<form action="admin.php?f=ppc" method="POST">
<td width=100 align=center><input type="text" name="rmin1" size="7" value="<?=$min1;?>"></td><td width=100 align=center> <input type="text" name="rmin2" size="7" value="<?=$min2;?>"></td><td width=100 align=center> <input type="text" name="rmin3" size="7" value="<?=$min3;?>"></td><td width=100 align=center> <input type="text" name="rmin4" size="7" value="<?=$min4;?>"></td><td width=100 align=center> <input type="text" name="act" size="7" value="<?=$act;?>"></td>
</tr>
<tr><td>&nbsp;</td></tr><tr>
<th width=100>Cash Maximum Prize</th> <th width=100>Credits Maximum Prize</th><th width=100>Banners Maximum Prize</th><th width=100>Text Maximum Prize</th>
</tr>
<tr>
<td width=100 align=center><input type="text" name="rmax1" size="7" value="<?=$max1;?>"></td><td width=100 align=center> <input type="text" name="rmax2" size="7" value="<?=$max2;?>"></td><td width=100 align=center> <input type="text" name="rmax3" size="7" value="<?=$max3;?>"></td><td width=100 align=center> <input type="text" name="rmax4" size="7" value="<?=$max4;?>"><br></td>
</tr>
<tr><td>&nbsp;</td></tr><tr>
<tr>
<th width=100>*Cash Chance To Win</th> <th width=100>Credits Chance To Win</th><th width=100>Banners Chance To Win</th><th width=100>Text Chance To Win</th>
</tr>
<tr>
<td width=100 align=center><input type="text" name="rpct1" size="7" value="<?=$pct1;?>">%</td><td width=100 align=center> <input type="text" name="rpct2" size="7" value="<?=$pct2;?>">%</td><td width=100 align=center> <input type="text" name="rpct3" size="7" value="<?=$pct3;?>">%</td><td width=100 align=center> <input type="text" name="rpct4" size="7" value="<?=$pct4;?>">% </td>
</tr>
</table>
<br><br>
<input type="submit" name="Edit" value="Edit">
<br>
<center><h4>To Edit Your Settings
Click in the text box, make the change, then click the Edit button.</h4></center>
<br><br><font size='5'>IMPORTANT:</font><font size='4'> You may now set your own default.<br>
Your percentages (when added together) MUST NOT exceed 100 or you will cause the page to malfunction.</font> <br><br>
A setting of 50 will make that prize the default prize. <br>For the best random prize allotment 
a setting of 10, 30, 30, 30 is suggested.<br><br>
<center><font size='1'>&copy; 2015 Janet Online Enterprises DBA <a href="http://www.LFMTEmods.com" target="_blank">LFMTEmods.com</a> - All Rights Reserved.<br><br>