<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.24
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

echo("<html>
<body>
<center>
");

####################

//Begin main page

####################

if ($_GET['addcat'] == "yes" && strlen($_POST['catname']) > 0) {
	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."products_cats` WHERE catname='".$_POST['catname']."'"), 0);
	if ($checkexists == 0) {
		mysql_query("INSERT INTO `".$prefix."products_cats` (catname, description) VALUES ('".$_POST['catname']."', '".$_POST['description']."')") or die(mysql_error());
		echo "<script language=\"JavaScript\">";
		echo "window.opener.location.href = window.opener.location.href;";
		echo "</script>";
	}
}

if (isset($_GET['editcat']) && is_numeric($_GET['editcat']) && strlen($_POST['catname']) > 0) {
	mysql_query("UPDATE `".$prefix."products_cats` SET catname='".$_POST['catname']."', description='".$_POST['description']."' WHERE id='".$_GET['editcat']."'") or die(mysql_error());
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

if ($_GET['delcat'] == "yes" && is_numeric($_GET['catid'])) {
	mysql_query("UPDATE `".$prefix."products` SET catid='0' WHERE catid='".$_GET['catid']."'") or die(mysql_error());
	mysql_query("DELETE FROM `".$prefix."products_cats` WHERE id='".$_GET['catid']."'") or die(mysql_error());
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

?>

	<h4><b>Add/Delete Product Categories</b></h4><br>
	<p align="left">You can put your Products into categories, to help keep them organized for you and your members.  A category will be listed on the "Downloads" page, if the member has an available download in that category.</p>
	
	<form action="/admin/prod_editcats.php?addcat=yes" method="POST">
	<font size="2"><b>New Category Name: </b></font> <input type="text" name="catname">
	<br><br>
	<font size="2"><b>Description:<br></b></font> <textarea name="description" cols=25 rows=4></textarea>
	<br><br>
	<input type="submit" value="Create Category">
	</form>
	<hr>
	
	<table border="1" bordercolor="black" width="300">
		<tr><td colspan="2" align="center"><p><font size="2"><b>Manage Categories</b></font></p></td></tr>
		<?php
			$getcats = mysql_query("SELECT id, catname, description FROM `".$prefix."products_cats` ORDER BY catname ASC") or die(mysql_error());
			if (mysql_num_rows($getcats) > 0) {
				while ($catlist = mysql_fetch_array($getcats)) {
					echo("<tr><td align=\"center\"><form action=\"/admin/prod_editcats.php?editcat=".$catlist['id']."\" method=\"post\"> <input size=\"33\" type=\"text\" name=\"catname\" value=\"".$catlist['catname']."\"> <br> <textarea name=\"description\" cols=25 rows=4>".$catlist['description']."</textarea> <br> <input type=\"submit\" value=\"Edit Category\"> </form></td><td align=\"center\"><a href=\"/admin/prod_editcats.php?delcat=yes&catid=".$catlist['id']."\"><img src=\"../images/del.png\" alt=\"Delete Category\" width=\"16\" height=\"16\" border=\"0\" /></a></td></tr>");
				}
			} else {
				echo("<tr><td colspan=\"2\" align=\"center\"><p><font size=\"2\">No Categories Found</font></p></td></tr>");
			}
		?>
	</table>
	
</center>
</body>
</html>

<?php
exit;
?>