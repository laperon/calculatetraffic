<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



	// Prevent anyone who isn't logged in from opening this page
	include "../inc/checkauth.php"; 
	if(!isset($_SESSION["adminid"])) { exit; };

	$msg="";

// Add a product entry
if($_POST["Submit"] == "Add Product")
{
	if(isset($_POST["freeproduct"]))
	{
		$free = 1;
	}
	else
	{
		$free = 0;
	}
	$qry="INSERT INTO ".$prefix."products(filename,productname,free) VALUES('".$_POST["prodfile"]."','".$_POST["prodname"]."',$free)";
	mysql_query($qry);
}

// Delete a product entry
if(isset($_GET["pd"]))
{
	mysql_query("DELETE FROM ".$prefix."products WHERE productid=".$_GET["pd"]);
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>
<p align="center">&nbsp;</p>
<form method="post" action="admin.php?f=pd">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0">
          <tr class="admintd">
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product ID</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product Name </font></strong></td>
            <td width="150" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Filename</font></strong></td>
            <td width="30" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Free</font></strong></td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
    </tr>
		  <?
		  	// List product IDs/Filenames
			$prodres=@mysql_query("SELECT * FROM ".$prefix."products where credits=0");
			while($prodrow=@mysql_fetch_array($prodres))
			{ ?>
          <tr>
            <td nowrap="nowrap"><?=$prodrow["productid"];?></td>
            <td><?=$prodrow["productname"];?></td>
            <td nowrap="nowrap"><?=$prodrow["filename"];?></td>
            <td align="center" nowrap="nowrap"><? if($prodrow["free"] == 1){ ?><img src="../images/tick.jpg" /><? } else { echo "No"; } ?></td>
            <td nowrap="nowrap"><a href="javascript:editProduct(<?=$prodrow["productid"];?>);"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
            <td nowrap="nowrap"><a href="admin.php?f=pd&pd=<?=$prodrow["productid"];?>"><img src="../images/del.png" alt="Delete Product" width="16" height="16" border="0" /></a></td>
          </tr>
		  <? } ?>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap"><input name="prodname" type="text" id="prodname" size="16" /></td>
            <td nowrap="nowrap"><input name="prodfile" type="text" id="prodfile" size="16" /></td>
            <td align="center" nowrap="nowrap"><input name="freeproduct" type="checkbox" id="freeproduct" value="freeproduct" /></td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap"><input name="Submit" type="submit" class="formfield" id="Submit" value="Add Product" /></td>
          </tr>
</table>
</form>
<br />
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  
  <tr>
    <td align="left" valign="top" bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The products section is where you add any downloadable products that you will sell or give away.  If a product is not free, be sure to add it to the <a href=admin.php?f=scrds>Sales Packages</a> page.  You can then sell the item in the members area, or add it to an OTO offer. </font></p>
      <ul>
        <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Don't forget to upload the product file to the file library!</font></li>
      </ul>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
