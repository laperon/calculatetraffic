<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



	// Prevent anyone who isn't logged in from opening this page
	if(!isset($_SESSION["adminid"])) { exit; };

	$msg="";

// Add a product entry
if($_POST["Submit"] == "Add Product")
{
	if(isset($_POST["freeproduct"]))
	{
		$free = 1;
	}
	else
	{
		$free = 0;
	}
	$qry="INSERT INTO ".$prefix."products(catid,filename,productname,free) VALUES('".$_POST['catid']."','".$_POST["prodfile"]."','".$_POST["prodname"]."',$free)";
	lfmsql_query($qry);
}

// Delete a product entry
if(isset($_GET["pd"]))
{
	lfmsql_query("DELETE FROM ".$prefix."products WHERE productid=".$_GET["pd"]);
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<center>
<p>&nbsp;</p>

<p><font size=3><b>Show:</b>
<? if (!isset($_GET['showcat'])) { $showcat = "0"; } else { $showcat = $_GET['showcat']; } ?>
<a href="admin.php?f=pd"><? if (!isset($showcat) || strlen($showcat) < 1 || (is_numeric($showcat) && $showcat == 0)) { echo("<b>All</b>"); } else { echo("All"); } ?></a> | 
<?php
$getcats = lfmsql_query("SELECT id, catname FROM `".$prefix."products_cats` ORDER BY catname ASC") or die(lfmsql_error());
if (lfmsql_num_rows($getcats) > 0) {
	while ($catlist = lfmsql_fetch_array($getcats)) {
		echo("<a href=\"admin.php?f=pd&showcat=".$catlist['id']."\">"); if ($showcat == $catlist['id']) { echo("<b>".$catlist['catname']."</b>"); } else { echo($catlist['catname']); } echo("</a> | ");
	}
}
?>
<span style="cursor:pointer; color:blue; size:8px;" onClick="window.open('/admin/prod_editcats.php','edit_cats','scrollbars=yes, menubars=no, width=550, height=550');">Add/Edit Categories</span>
</p>

<p align="center">&nbsp;</p>
<form method="post" action="admin.php?f=pd">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0">
          <tr class="admintd">
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product ID</font></strong></td>
            <td nowrap="NOWRAP" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Category</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product Name </font></strong></td>
            <td width="150" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Filename</font></strong></td>
            <td width="30" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Free</font></strong></td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
    </tr>
		  <?
		  	// List product IDs/Filenames
		  	
		  	if ($showcat > 0) {
		  		$showcatquery = " WHERE catid='".$showcat."'";
		  	} else {
		  		$showcatquery = "";
		  	}
		  	
			$prodres=@lfmsql_query("SELECT * FROM ".$prefix."products".$showcatquery);
			while($prodrow=@lfmsql_fetch_array($prodres))
			{
			
				if ($prodrow["catid"] > 0) {
					$catname = @lfmsql_result(@lfmsql_query("SELECT catname FROM `".$prefix."products_cats` WHERE id='".$prodrow["catid"]."'"), 0);
				} else {
					$catname = "None";
				}
			
			?>
          <tr>
            <td nowrap="nowrap"><?=$prodrow["productid"];?></td>
            <td nowrap="nowrap"><?=$catname;?></td>
            <td><?=$prodrow["productname"];?></td>
            <td nowrap="nowrap"><?=$prodrow["filename"];?></td>
            <td align="center" nowrap="nowrap"><? if($prodrow["free"] == 1){ ?><img src="../images/tick.jpg" /><? } else { echo "No"; } ?></td>
            <td nowrap="nowrap"><a href="javascript:editProduct(<?=$prodrow["productid"];?>);"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
            <td nowrap="nowrap"><a href="admin.php?f=pd&pd=<?=$prodrow["productid"];?>"><img src="../images/del.png" alt="Delete Product" width="16" height="16" border="0" /></a></td>
          </tr>
		  <? } ?>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">
            	<select name="catid">
			<option value="0">None</option>
			<?
			$getcats = lfmsql_query("SELECT id, catname FROM `".$prefix."products_cats` ORDER BY catname ASC") or die(lfmsql_error());
			if (lfmsql_num_rows($getcats) > 0) {
				while ($catlist = lfmsql_fetch_array($getcats)) {
					echo("<option value=\"".$catlist['id']."\""); if ($showcat == $catlist['id']) { echo(" selected=\"selected\""); } echo(">".$catlist['catname']."</option>");
				}
			}
			?>
			</select>
            </td>
            <td nowrap="nowrap"><input name="prodname" type="text" id="prodname" size="16" /></td>
            <td nowrap="nowrap"><input name="prodfile" type="text" id="prodfile" size="16" /></td>
            <td align="center" nowrap="nowrap"><input name="freeproduct" type="checkbox" id="freeproduct" value="freeproduct" /></td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap"><input name="Submit" type="submit" class="formfield" id="Submit" value="Add Product" /></td>
          </tr>
</table>
</form>
<br />
<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  
  <tr>
    <td align="left"><div class="lfm_descr"><p>These are products and files that your members can download from your site.  You can make a download available to all members for free, or you could sell a download separately by adding it to a Sales Package.</p>
      
      <p>The Product Name is the name that will be visible to members when they go to download the file.  The Filename is the actual name of the file (such as filename.zip).</p>
      
      <p>You will need to upload the downloadable file into your site's protected directory (the default is /1qaz2wsx).  You can do this either manually via FTP, or by using the Media Library page in your Admin Panel.</p>
      
      </div></td>
  </tr>
</table>
</div>
<p>&nbsp;</p>
