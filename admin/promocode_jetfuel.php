<?php

// LFMTE Surf Promo Code Plugin v3
// �2013 LFM Wealth Systems
// http://thetrafficexchangescript.com

// LFM Surfer Jetfuel Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE and LFMVM scripts

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

if (!file_exists("jetfuel.php")) {
	echo("Missing Jetfuel Files");
	exit;
}

echo("<html>
<body>
<center>
");

####################

//Begin main page

####################

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	echo ("Invalid GET vars");
	exit;
}

if ($_GET['update'] == "yes" && isset($_POST['jetfuelid']) && is_numeric($_POST['jetfuelid'])) {
	
	mysql_query("UPDATE `".$prefix."promo_prizes` SET jetfuel='".$_POST['jetfuelid']."' WHERE id='".$_GET['id']."'") or die(mysql_error());
	
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
}

$currentsetting = mysql_query("SELECT jetfuel FROM `".$prefix."promo_prizes` WHERE id='".$_GET['id']."'") or die(mysql_error());

if (mysql_num_rows($currentsetting) < 1) {
	echo ("Sales Package Not Found");
	exit;
}

$currentid = mysql_result($currentsetting, 0, "jetfuel");

?>

	<h4><b>Add Surfer Jetfuel</b></h4><br>
	<p align="left">You can add a Surfer Jetfuel pack to be included with this promo code reward.</p>
	</center>
	<?php
	
	echo("
	<form action=\"/admin/promocode_jetfuel.php?id=".$_GET['id']."&update=yes\" method=\"POST\">
	<table border=\"1\" bordercolor=\"black\" cellpadding=\"3\" cellspacing=\"0\">
	<tr>
	  <td width=\"70\"><b>Enabled</b></td>
	  <td><b>Item</b></td>
	</tr>
	<tr>
	  <td align=\"right\"><input name=\"jetfuelid\" type=\"radio\" value=\"0\""); if ($currentid == 0) { echo(" checked=\"checked\""); } echo("></td>
	  <td><font size=\"2\">None</font></td>
 	</tr>
	");
	
	$getbonuses = mysql_query("SELECT id, name FROM `".$prefix."jetfuel` ORDER BY id ASC") or die(mysql_error());
	while($bonusitem=@mysql_fetch_object($getbonuses)) {
		
		echo("
		<tr>
		  <td align=\"right\"><input name=\"jetfuelid\" type=\"radio\" value=\"".$bonusitem->id."\""); if ($bonusitem->id == $currentid) { echo(" checked=\"checked\""); } echo("></td>
		  <td><font size=\"2\">".$bonusitem->name."</font></td>
	 	</tr>
	 	");
	}

echo("
<tr><td colspan=\"5\" align=\"center\"><input type=\"submit\" value=\"Update Reward\"></td></tr>
</table>
</form>

<br><br>

</body>
</html>");

exit;

?>