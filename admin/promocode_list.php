<?php

// LFMTE Surf Promo Code Plugin v3
// �2013 LFM Wealth Systems
// http://thetrafficexchangescript.com

include "../inc/checkauth.php"; 
include "../inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

?>
<html>

<head>
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<body>
<center>

<?

$limit=50;
$offset=$_GET[offset];
if (empty($offset)) { $offset=0; } 

####################

//Begin main page

####################

echo("<h4><b>Members Who Completed This Code</b></h4>
");

if(!is_numeric($_GET['codeid'])) {
echo("<p><b>Invalid Code ID</b></p>");
exit;
}

$res = mysql_query("SELECT COUNT(*) FROM `{$prefix}promo_users` WHERE state=1 and codeid=".$_GET['codeid']);
$number = @mysql_result($res,0);
$limit = 50;
$finish = $offset + $limit;
if($finish > $number) { $finish = $number; }

$numtodraw = 5;
if (isset($_POST['numtodraw']) && is_numeric($_POST['numtodraw'])) {
	$numtodraw = $_POST['numtodraw'];
} elseif (isset($_GET['numtodraw']) && is_numeric($_GET['numtodraw'])) {
	$numtodraw = $_GET['numtodraw'];
}
if ($numtodraw > $number || $numtodraw < 1) { $numtodraw = $number; }

echo '<table align="center" border="1" cellpadding="5" cellspacing="0">
<tr bgcolor="#F0F0F0"><td align="center">
<form action="promocode_list.php?codeid='.$_GET['codeid'].'&drawmembers=yes" method="post">
<font size="3"><b>Randomly Draw <input type="text" name="numtodraw" value="'.$numtodraw.'" size="3"> Members Who Completed This Promo Code</b></font><br />
<input type="submit" value="Draw">
</form>
</td></tr>
</table>
<br>

';

if ($_GET['drawmembers'] == "yes" && is_numeric($numtodraw)) {
	echo '<br>Randomly selected '.$numtodraw.' out of '.$number.' members who completed this promo code.<br><a href="promocode_list.php?codeid='.$_GET['codeid'].'">Show All Members</a>
	<br><br>If you want to remove a member from the list (your own account, for example) you can click "Exclude".  The list will then be redrawn without that account.<br>';
} else {
	echo '<br>A total of '.$number.' members have completed this promo code.<br>';
}

// Being Sorting
if (($_GET["sortby"] != "") && ($_GET["sortorder"] != "")) {
	
	$sortorder = $_GET["sortorder"];
	if ($sortorder == "ASC") {
		$nextsort = "DESC";
	} else {
		$nextsort = "ASC";
	}
	
	if ($_GET["sortby"] == "claimdate") {
		$sortby = "a.date";
	} elseif ($_GET["sortby"] == "dateclicks") {
		$sortby = "s.clicks";
	} elseif ($_GET["sortby"] == "Id") {
		$sortby = "a.userid";
	} else {
		$sortby = "m.".trim($_GET["sortby"]);
	}
	
} else {
	$sortby = "a.id";
	$sortorder = "ASC";
	$nextsort = "ASC";
}

// End Sorting

?>

<form name="mbrowse" id="mbrowse" method="post" target="_blank" action="giveprize_pop.php">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td align="center"><table width="1100" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr>
        <td colspan="14" align="left" valign="bottom">
		<div align="left">Page: <?
		if (!isset($_GET['drawmembers']) || $_GET['drawmembers'] != "yes") {
			// Page Navigation
			$pages=intval($number/$limit);
			if ($offset>1) {
				$prevoffset=$offset-$limit; 
				echo "<a href=\"promocode_list.php?offset=$prevoffset&sortby=".$_GET['sortby']."&sortorder=".$_GET['sortorder']."&codeid=".$_GET['codeid']."\">Prev</a> \n"; 
			}
			for ($i=1;$i<$pages+2;$i++) {
				$newoffset=$limit*($i-1);
				if($newoffset==$offset) {
					echo "<b>$i</b> ";
				} else {
					echo "<a href=\"promocode_list.php?offset=$newoffset&sortby=".$_GET['sortby']."&sortorder=".$_GET['sortorder']."&codeid=".$_GET['codeid']."\">$i</a> \n";
				}
			}
			if ($number>($offset+$limit)) {
				$nextoffset=$offset+$limit;
				echo "<a href=\"promocode_list.php?offset=$nextoffset&sortby=".$_GET['sortby']."&sortorder=".$_GET['sortorder']."&codeid=".$_GET['codeid']."\">Next</a><p>\n";
			}
			// End Page Navigation
		} else {
			echo "<b>1</b>";
		}
		?></div>
		<input name="GivePrize" type="submit" class="lfmtable" style="font-size:9px" value="Give Prize" />
	</td>
      </tr>
      <tr class="admintd">
        <th align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="promocode_list.php?sortby=Id&sortorder=<?=$nextsort?>&codeid=<?=$_GET['codeid']?>">User ID</a></font></strong></th>
        <th align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="promocode_list.php?sortby=firstname&sortorder=<?=$nextsort?>&codeid=<?=$_GET['codeid']?>">First Name</a></font></strong></th>
        <th align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="promocode_list.php?sortby=lastname&sortorder=<?=$nextsort?>&codeid=<?=$_GET['codeid']?>">Last Name</a></font></strong></th>
        <th nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="promocode_list.php?sortby=username&sortorder=<?=$nextsort?>&codeid=<?=$_GET['codeid']?>">Username</a></font></strong></th>
        <th align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="promocode_list.php?sortby=mtype&sortorder=<?=$nextsort?>&codeid=<?=$_GET['codeid']?>">MType</a></font></strong> </th>
 	
        <th align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="promocode_list.php?sortby=claimdate&sortorder=<?=$nextsort?>&codeid=<?=$_GET['codeid']?>">Date Completed</a></font></strong></th>
        
        <th align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="promocode_list.php?sortby=dateclicks&sortorder=<?=$nextsort?>&codeid=<?=$_GET['codeid']?>">Clicks On Date</a></font></strong></th>
        
        <th align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg">&nbsp;</th>
        <th width="16" align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><input type="checkbox" name="checkbox" value="checkbox" onClick="javascript:checkAll(document.getElementById('mbrowse'),'mcheck');" /></th>
      </tr>
      
<?

if ($_GET['drawmembers'] == "yes" && is_numeric($numtodraw)) {
	
	// Randomly Draw A Set Number Of Members
	
	if (isset($_GET['excludelist']) && strlen($_GET['excludelist']) > 1) {
		$excludequery = " AND a.userid NOT IN (".substr($_GET['excludelist'], 0, -1).")";
	} else {
		$excludequery = "";
	}
	
	$res = mysql_query("SELECT a.userid AS userid, a.date AS claimed, m.username AS username, m.firstname AS firstname, m.lastname AS lastname, m.mtype AS mtype, s.clicks AS dateclicks FROM `".$prefix."promo_users` a LEFT JOIN `".$prefix."members` m ON a.userid=m.Id LEFT JOIN `".$prefix."surflogs` s ON (a.userid=s.userid AND a.date=s.date) WHERE a.state = 1 and a.codeid='".$_GET['codeid']."'".$excludequery." ORDER BY RAND() LIMIT $numtodraw") or die(mysql_error());

} else {

	// Show All Members Who Claimed The Reward

	$res = mysql_query("SELECT a.userid AS userid, a.date AS claimed, m.username AS username, m.firstname AS firstname, m.lastname AS lastname, m.mtype AS mtype, s.clicks AS dateclicks FROM `".$prefix."promo_users` a LEFT JOIN `".$prefix."members` m ON a.userid=m.Id LEFT JOIN `".$prefix."surflogs` s ON (a.userid=s.userid AND a.date=s.date) WHERE a.state = 1 and a.codeid='".$_GET['codeid']."' ORDER BY ".$sortby." ".$sortorder." LIMIT $offset,$limit") or die(mysql_error());
	
}

if(mysql_num_rows($res)) {
   while( $row=mysql_fetch_array($res) ) {
   	
   	if($bgcolor == "#FFFFFF") { $bgcolor="#DDDDDD"; } else { $bgcolor="#FFFFFF"; }
	
   	$useraccname = "n/a";
   	$clicksondate = "n/a";
   	
   	if($row[mtype]>0) { $useraccname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$row[mtype]"),0); }
   	
   	if (isset($row[dateclicks]) && is_numeric($row[dateclicks]) && $row[dateclicks] >= 0) { $clicksondate = $row[dateclicks]; }
   	
	echo '<tr bgcolor="'.$bgcolor.'" onMouseOver="this.bgColor=\'#99bb99\';" onMouseOut="this.bgColor=\''.$bgcolor.'\';">
	<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a target="_blank" href="admin.php?f=mm&searchfield=Id&searchtext='.$row[userid].'&sf=browse">'.$row[userid].'</a></font></td>
	<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">'.$row[firstname].'</font></td>
	<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">'.$row[lastname].'</font></td>
	<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">'.$row[username].'</font></td>
	<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">'.$useraccname.'</font></td>
	<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">'.$row[claimed].'</font></td>
	<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">'.$clicksondate.'</font></td>';
	
	if ($_GET['drawmembers'] == "yes" && is_numeric($numtodraw)) {
		echo '<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><input type="button" value="Exclude" onclick="window.location=\'promocode_list.php?codeid='.$_GET['codeid'].'&drawmembers=yes&numtodraw='.$numtodraw.'&excludelist='.$_GET['excludelist'].$row[userid].',\';"></font></td>';
	} else {
		echo '<td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font></td>';
	}
	
	echo '<td align="center"><input type="checkbox" class="mcheck" name="memcheck[]" value="'.$row[userid].'" onclick="HighlightRowIfChecked(this);" />
	</tr>';
   }

   echo '</table></td></tr><tr><td></td></tr></table></form><p>';

} else {
   echo '<tr><td colspan=14 align="center"><br>There are no members to show</td></tr>
   </table></td></tr><tr><td></td></tr></table></form>';
}

echo '
</center>
</body>
</html>';

?>