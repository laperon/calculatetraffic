<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

if ($_GET['deletepurchase'] == "yes" && isset($_GET['pagepurchaseid']) && is_numeric($_GET['pagepurchaseid'])) {

	$confirmdelete = $_GET['confirmdelete'];
	$id = $_GET['pagepurchaseid'];
	
	if ($confirmdelete == "yes") {
		@lfmsql_query("Delete from ".$prefix."purchasedpages where id=$id limit 1");
	} else {
		echo("<br><br><center><b>Are you sure you want to remove this page from the member's account?</b><br><br><a href=admin.php?f=purchasedpages&deletepurchase=yes&pagepurchaseid=$id&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=purchasedpages><b>No</b></a>");
		exit;
	}
	
}

if (isset($_GET['searchuser']) && $_GET['searchuser'] == "go" && isset($_POST['searchuser']) && is_numeric($_POST['searchuser'])) {
	$searchquery = " WHERE m.Id=".$_POST['searchuser'];
	$searchvalue = " value=".$_POST['searchuser'];
	$searchget = "&searchuser=".$_POST['searchuser'];
} elseif (isset($_GET['searchuser']) && is_numeric($_GET['searchuser'])) {
	$searchquery = " WHERE m.Id=".$_GET['searchuser'];
	$searchvalue = " value=".$_GET['searchuser'];
	$searchget = "&searchuser=".$_GET['searchuser'];
} else {
	$searchquery = "";
	$searchvalue = "";
	$searchget = "";
}

// Get the total sales records for browse nav
$cres=@lfmsql_query("SELECT COUNT(*) as scount FROM ".$prefix."purchasedpages s LEFT JOIN ".$prefix."members m ON s.userid=m.Id".$searchquery);
$scount=@lfmsql_result($cres,0);

// Get the starting record for browse
if(!isset($_GET["limitStart"]))
{ 
	$st=0; 
}
else
{
	$st=$_GET["limitStart"];
}

$sres=@lfmsql_query("SELECT m.firstname,m.lastname,s.* FROM ".$prefix."purchasedpages s LEFT JOIN ".$prefix."members m ON s.userid=m.Id".$searchquery." ORDER BY id LIMIT $st,40");
?>
<link href="styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function addPurchasedPage()
{
	var windowprops = "location=no,scrollbars=no,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=500,height=500"; 
 
	var URL = "addpurchasedpage.php"; 
	popup = window.open(URL,"GroupPopup",windowprops);	
}
</script>

<center>

<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  <tr>
    <td align="center"><div class="lfm_infobox_heading">Purchased Pages</div></td>
  </tr>
  
  <tr>
    <td align="left"><div class="lfm_descr">
    	
    	Purchased pages are content pages that are available to specific members, because they either purchased a Sales Package that included access to the page, or because you manually added it to their account.  Members will have access to their purchased pages, plus any content pages that you have made available to their membership level.
    	
    </div></td>
  </tr>
      
</table>
</div>
<p>&nbsp;</p>

<!-- Start Search Box -->
<table border="0" cellpadding="0" cellspacing="0" width="250">
<form name="searchfrm" method="post" action="admin.php?f=purchasedpages&searchuser=go">
<tr>
<td><input type="text" class="form-control" name="searchuser" placeholder="Search For User ID"<? echo($searchvalue); ?>></td>
<td><input type="submit" name="Submit" value="Search" /></td>
</tr>
</form>
</table>
<br>
<!-- End Search Box -->

<p align="center">
  <input type="button" value="Add Page" onClick="javascript:addPurchasedPage();" />
</p>

<table width="100" border="0" align="center" cellpadding="4" cellspacing="0" class="lfmtable">
  <tr>
    <td colspan="10">
	<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($scount,$_GET["limitStart"],40,"purchasedpages".$searchget);
		?></div>	</td>
  </tr>
  <tr>
    <td class="admintd">User</td>
    <td align="center" class="admintd">Page Title</td>
    <td align="center" class="admintd">&nbsp;</td>
  </tr>
<? while($sale=@lfmsql_fetch_object($sres)) { 

	// Get referrer name
	if(strlen($sale->firstname) < 1 && strlen($sale->lastname) < 1) {
	        $affname="User Not Found (ID #".$sale->userid.")"; }
	else {
	        $affname=$sale->firstname." ".$sale->lastname; }
	        
	// Get Page Title
	$getpagetitle = lfmsql_query("SELECT pagename FROM ".$prefix."memberpages WHERE pageid=".$sale->pageid) or die(lfmsql_error());
	if (lfmsql_num_rows($getpagetitle) > 0) {
		$showpagetitle = lfmsql_result($getpagetitle, 0, "pagename");
	} else {
		$showpagetitle = "Page Not Found (ID #".$sale->pageid.")";
	}

?>
  <tr>
    <td nowrap="nowrap"><?=$affname;?></td>
    <td align="center" nowrap="nowrap"><?=$showpagetitle;?></td>
    <td align="center" nowrap="nowrap"><a href="admin.php?f=purchasedpages&deletepurchase=yes&pagepurchaseid=<? echo($sale->id); ?>">Delete</a> </td>
  </tr>
<? } ?>
</table>
<br>