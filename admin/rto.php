<?php

// Grouping One-Time-Offer Mod
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;

}
-->
</style>
<script type="text/javascript" src="../inc/ajax.js"></script>
<script type="text/javascript">
var ajax = new sack();
</script>
<script type="text/javascript" src="../inc/ajaxfuncs.js"></script>
<script language="javascript" type="text/javascript" src="tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
 theme : "advanced",
    mode: "exact",
    relative_urls : false,
    elements : "oto,otofooter",
    theme_advanced_toolbar_location : "top",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
    + "bullist,numlist,outdent,indent",
    theme_advanced_buttons2 : "fontselect,fontsizeselect,forecolor,backcolor,link,unlink,anchor,image,separator,"
    +"undo,redo,cleanup,code,separator,sub,sup,charmap",
    theme_advanced_buttons3 : "",
    apply_source_formatting : true,
    cleanup : false,
    cleanup_on_startup : false,
    height:"480px",
    width:"760px"
  });

  function toggleEditor(id) {
	var elm = document.getElementById(id);

	if (tinyMCE.getInstanceById(id) == null)
		tinyMCE.execCommand('mceAddControl', false, id);
	else
		tinyMCE.execCommand('mceRemoveControl', false, id);
}
</script><br><br><center>
<?

$add = $_GET['add'];
$update = $_GET['update'];
$delete = $_GET['delete'];

//Update Total Percent
if ($update == "total") {
$totalpercent = $_POST['totalpercent'];
if (is_numeric($totalpercent) && $totalpercent>=0 && $totalpercent<=100) {

$checktotal = lfmsql_query("Select totalpercent from ".$prefix."rto where id=1 limit 1");
if (lfmsql_num_rows($checktotal) == 0) {
@lfmsql_query("Insert into ".$prefix."rto (id, totalpercent) values (1, $totalpercent)");
}

@lfmsql_query("Update ".$prefix."rto set totalpercent=$totalpercent where id=1 limit 1");
}

}

//Update An Offer
elseif ($update == "yes") {
$id = $_GET['rtoid'];
$ipn = $_POST['ipn'];
$text = $_POST['text'];

$percent = $_POST['percent'];

if (($percent != 0) && ($percent < 1)) {
$percent = 1;
}
if (is_numeric($percent) && $percent>=0 && $percent<=100) {
$totala = $percent;
$checkothers = lfmsql_query("Select percent from ".$prefix."rtoads where id != $id");
for ($i = 0; $i < lfmsql_num_rows($checkothers); $i++) {
$thisa = lfmsql_result($checkothers, $i, "percent");
$totala = $totala+$thisa;
}
if ($totala > 100) {
$resmessage = "ERROR: The percent values for all offers should total 100.";
} else {
@lfmsql_query("Update ".$prefix."rtoads set ipn='$ipn', content='$text', percent=$percent where id=$id limit 1");
$getoffer = lfmsql_query("select * from ".$prefix."rtoads where id>0");
$numoffers = lfmsql_num_rows($getoffer);
$cdownrand = 100;
for ($i = 0; $i < $numoffers; $i++) {
$offerid = lfmsql_result($getoffer, $i, "id");
$offerpercent = lfmsql_result($getoffer, $i, "percent");
$highnum = $cdownrand;
$cdownrand = $cdownrand-$offerpercent;
$lownum = $cdownrand;
@lfmsql_query("Update ".$prefix."rtoads set highnum=$highnum, lownum=$lownum where id=$offerid limit 1");
}
$resmessage = "Offer Settings Updated";
if ($cdownrand < 0) {
@lfmsql_query("Update ".$prefix."rtoads set percent=0, highnum=0, lownum=0 where id>0");
$resmessage = "An unknown error has occurred.  Please try updating the percentages for your offers again.";
}
}
} else {
$resmessage = "ERROR: The percent value for an offer must be 100 or less.";
}

}

//Add An Offer
elseif ($add == "yes") {
$ipn = $_POST['ipn'];
$text = $_POST['text'];

$percent = $_POST['percent'];
@lfmsql_query("Insert into ".$prefix."rtoads (ipn, content) values ('$ipn', '$text')");
$resmessage = "Offer added.  Please enter a percent value.";
}

//Delete An Offer
elseif ($delete == "yes") {
$confirmdelete = $_GET['confirmdelete'];
$id = $_GET['rtoid'];
if ($confirmdelete == "yes") {
@lfmsql_query("Delete from ".$prefix."rtoads where id=$id limit 1");
$resmessage = "Offer Deleted";
} else{
echo("<b>Are you sure you want to delete this offer?</b><br><br><a href=admin.php?f=rto&delete=yes&rtoid=$id&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=rto><b>No</b></a>");
exit;
}
}


//Begin Main Page

echo("<center>");

if ($resmessage != "") {
echo("<p><font face=$fontface size=2>$resmessage</font></p>");
}

$checktotal = lfmsql_query("Select totalpercent from ".$prefix."rto where id=1 limit 1");
if (lfmsql_num_rows($checktotal) == 0) {
@lfmsql_query("Insert into ".$prefix."rto (id, totalpercent) values (1, 0)");
} else {
$totalpercent = lfmsql_result($checktotal, 0, "totalpercent");
}

echo("<table border=1 bordercolor=black cellpadding=3 cellspacing=0 width=500>
<tr><td align=left>
<center><p><b>Total RTO Display Percentage</b></p></center>
<p>This is the percent of the time in which a member will see one of your RTO offers when he or she logs into their account.  If you enter 100, members will see a RTO every time they access their account.  If you enter 0, they will never see a RTO.  You can set the percentage for each individual offer below.</p>
<center>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=rto&update=total\">
<input name=totalpercent type=text size=4 value=$totalpercent>
<input name=submit type=submit value=Update></form><br>
</td></tr>
</table><br><br>");

//Show Offers

$getdata = lfmsql_query("Select * from ".$prefix."rtoads order by id asc");

if (lfmsql_num_rows($getdata) != 0) {

for ($i = 0; $i < lfmsql_num_rows($getdata); $i++) {

$id = lfmsql_result($getdata, $i, "id");
$ipn = lfmsql_result($getdata, $i, "ipn");
$text = lfmsql_result($getdata, $i, "content");
$percent = lfmsql_result($getdata, $i, "percent");
$shown = lfmsql_result($getdata, $i, "shown");
$showid = $i+1;

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=4 align=center><b>Offer Details</b><br><a target=_blank href=\"$site_url/previewoffer.php?rtoid=$id\">Preview Offer</a></td></tr>
<tr><td align=center><b>&nbsp;</b></td>
<td align=center><b>IPN ID</b></td>
<td align=center><b>Percent</b></td>
<td align=center><b>Action</b></td></tr>");

echo("
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=rto&update=yes&rtoid=$id\">
<tr>
<td align=center>Views: $shown</td>
<td align=center><input type=text size=5 name=ipn value=\"$ipn\"></td>
<td align=center><input type=text size=3 name=percent value=$percent></td>");

echo("<td align=center><input type=submit value=Update><br><font size=2><a href=\"admin.php?f=rto&delete=yes&rtoid=$id\">Delete Offer</a></font></td></tr>
<tr><td colspan=4 align=center valign=center>
<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7>$text</textarea></td></tr></table>
</td></tr></table>
</form><br><br>");

}

}

//Add New Offer

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=3 align=center><b>Add A New Offer</b></td></tr>
<tr><td align=center><b>&nbsp;</b></td>
<td align=center><b>IPN ID</b></td>
<td align=center><b>Action</b></td></tr>");

echo("
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=rto&add=yes\">
<tr>
<td align=center>Views: 0</td>
<td align=center><input type=text size=5 name=ipn value=0></td>");

echo("<td align=center><input type=submit value=Add></td></tr>
<tr><td colspan=3 align=center valign=center>
<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7></textarea></td></tr></table>
</td></tr></table>
</form><br><br>");

?>