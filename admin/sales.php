<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

// Get the total sales records for browse nav
$cres=@lfmsql_query("SELECT COUNT(*) as scount FROM ".$prefix."sales");
$scount=@lfmsql_result($cres,0);

// Get the starting record for browse
if(!isset($_GET["limitStart"]))
{ 
	$st=0; 
}
else
{
	$st=$_GET["limitStart"];
}


$sres=@lfmsql_query("SELECT m.firstname,m.lastname,s.* FROM ".$prefix."sales s LEFT JOIN ".$prefix."members m ON s.affid=m.Id ORDER BY saledate LIMIT $st,20");
?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<h3 align="center">&nbsp;</h3>
<h3 align="center">Commission Details </h3>
<table width="100" border="0" align="center" cellpadding="4" cellspacing="0" class="lfmtable">
  <tr>
    <td colspan="10">
	<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($scount,$_GET["limitStart"],20,"sa");
		?></div>	</td>
  </tr>
  <tr>
    <td class="admintd">Date</td>
    <td align="center" class="admintd">Ref</td>
    <td align="center" class="admintd">Buyer</td>
    <td align="center" nowrap="nowrap" class="admintd">Item ID </td>
    <td align="center" nowrap="nowrap" class="admintd">Item Name </td>
    <td align="center" class="admintd">Amount</td>
    <td align="center" class="admintd">Comm.</td>
    <td align="center" nowrap="nowrap" class="admintd">TXN ID </td>
    <td align="center" class="admintd">Status</td>
    <td align="center" class="admintd">&nbsp;</td>
  </tr>
<? while($sale=@lfmsql_fetch_object($sres)) { 
	// Get purchaser
	$buyername="";
	$pres=@lfmsql_query("SELECT firstname,lastname FROM ".$prefix."members WHERE Id='".$sale->purchaserid."'");
	if($pres)
	{
		$purchase=@lfmsql_fetch_object($pres);
		$buyername=$purchase->firstname." ".$purchase->lastname;
	}
	// Get referrer name
	if(strlen($sale->firstname) < 1 && strlen($sale->lastname) < 1) {
	        $affname="Referrer Not Found ($sale->affid)"; }
	else {
	        $affname=$sale->firstname." ".$sale->lastname; }

?>
  <tr>
    <td nowrap="nowrap"><?=substr($sale->saledate,0,10);?></td>
    <td nowrap="nowrap"><?=$affname;?></td>
    <td nowrap="nowrap"><?=$buyername;?></td>
    <td align="center" nowrap="nowrap"><?=$sale->itemid;?></td>
    <td align="left" nowrap="nowrap"><?=$sale->itemname;?></td>
    <td align="center" nowrap="nowrap"><?=$sale->itemamount;?></td>
    <td align="center" nowrap="nowrap"><?=$sale->commission;?></td>
    <td align="center" nowrap="nowrap"><?=$sale->txn_id;?></td>
    <td align="center" nowrap="nowrap"><?=$sale->status;?></td>
    <td align="center" nowrap="nowrap"><a href="javascript:editSale(<?=$sale->salesid;?>);">Edit</a> | <a href="javascript:delSale(<?=$sale->salesid;?>);">Delete</a> </td>
  </tr>
<? } ?>
  <tr>
    <td colspan="10" align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($scount,$_GET["limitStart"],20,"sa");
		?></td>
  </tr>
</table>
