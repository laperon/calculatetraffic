<?php

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// SalesGlance - Created by Josh Abbott                                //
// Not for resale.  Version included with the LFM script only.         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// -- Graph Functions --
// VisuGraph JS 1.0
// �2012 Josh Abbott, http://visugraph.com
// Licensed for the LFM script

$currentdate = date("Y-m-d");
$startdate = strftime("%Y-%m-%d", strtotime("$currentdate + 30 days ago"));
$enddate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;

$logtype = "range";

?>

<script language="javascript">
	function choosePayTypes()
	{
		var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=550,height=550"; 
	 
		var URL = "salesglance_paytypes.php"; 
		popup = window.open(URL,"PayTypesPopup",windowprops);	
	}
	function chooseSalesPacks()
	{
		var windowprops = "location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100,width=550,height=550"; 
	 
		var URL = "salesglance_packs.php"; 
		popup = window.open(URL,"SalesPacksPopup",windowprops);	
	}
</script>

<script type="text/javascript" src="datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol.css">

<br><br>
<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  <tr>
    <td align="center"><div class="lfm_infobox_heading">SalesGlance</div></td>
  </tr>
  
  <tr>
    <td align="left"><div class="lfm_descr">
    	
    	<br>SalesGlance is a powerful tool that shows the sales at your site over time, as well as a breakdown of individual product sales during that time period.  You can also limit the SalesGlance data to specific payment types and sales packages, so you can see how well a specific product has sold over its lifecycle.
    	
    </div></td>
  </tr>
      
</table>
</div>

<?
$showpaypal = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showpaypal'"), 0);
$showpayza = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showpayza'"), 0);
$show2co = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='show2co'"), 0);
$showclickbank = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showclickbank'"), 0);
$showcomms = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showcomms'"), 0);
if ($showpaypal == 1 && $showpayza == 1 && $show2co == 1 && $showclickbank == 1 && $showcomms == 1) {
	$countpaytypes = "All";
} else {
	$countpaytypes = "";
	if ($showpaypal == 1) {
		$countpaytypes .= "PayPal ";
	}
	if ($showpayza == 1) {
		$countpaytypes .= "Payza ";
	}
	if ($show2co == 1) {
		$countpaytypes .= "2Checkout ";
	}
	if ($showclickbank == 1) {
		$countpaytypes .= "ClickBank ";
	}
	if ($showcomms == 1) {
		$countpaytypes .= "Commissions ";
	}
}
?>
<br>
<div class="lfm_descr">Showing Payment Types: <? echo($countpaytypes); ?></div>
<a href="javascript:choosePayTypes()"><input type="button" name="choosePayTypes" value="Choose Pay Types"></a>
<br><br>

<?
$salespacks = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='salespacks'"), 0);
if ($salespacks == "") {
	$countpacks = "All";
} else {
	$countpacks = count(explode(",", $salespacks));
}
?>
<div class="lfm_descr">Showing Sales Packages: <? echo($countpacks); ?></div>
<a href="javascript:chooseSalesPacks()"><input type="button" name="chooseSalesPacks" value="Choose Sales Packages"></a>
<br><br>

<!-- Start Main Table -->

<table width="1000" border="0" cellpadding="0" cellspacing="5">

<tr><td align="left" valign="top">
	
	<div class="lfm_infobox" style="width: 625px;">
	<table border=0 cellpadding=0 cellspacing=0 width=625 id="graphimg">
	<tr><td align="center">
		<br>
		<select name="datatype" id="datatype">
		<option value=1 selected>Sales</option>
		<option value=2>Units Sold</option>
		</select>
		<input type="hidden" name="tabletype" id="tabletype" value="<? echo($logtype); ?>">
		<input type name="sdate" id="DPC_sdate_YYYY-MM-DD" value="<? echo($startdate); ?>">
		<input type name="edate" id="DPC_edate_YYYY-MM-DD" value="<? echo($enddate); ?>">
		<button onclick="newGraph(1, document.getElementById('datatype').value, document.getElementById('tabletype').value, (new Date(document.getElementById('DPC_sdate_YYYY-MM-DD').value).getTime()/1000), (new Date(document.getElementById('DPC_edate_YYYY-MM-DD').value).getTime()/1000))">View</button>
		<br>
		
		<div id="graphdiv" style="width:600px;height:400px"></div>
		
		<table border=0 cellpadding=0 cellspacing=0>
			<tr>
				<td>&nbsp;</td>
				<td><input id="zoominbtn" type="button" style="height:30px;width:30px;" OnClick="changeZoom(0, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value="+"></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><input id="panleftbtn" type="button" style="height:30px;width:30px;" OnClick="changePan(0, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value="<"></td>
				<td><img id="loadingimg" src="graphblankimg.jpg"></td>
				<td><input id="panrightbtn" type="button" style="height:30px;width:30px;" OnClick="changePan(1, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value=">"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input id="zoomoutbtn" type="button" style="height:30px;width:30px;" OnClick="changeZoom(1, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value="-"></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<br>
	</td></tr></table>
	</div>
	
</td>

<td align="left" valign="top">
	
	<div class="lfm_infobox" style="width: 370px;">
	<table border=0 cellpadding=0 cellspacing=0 width="370">
	<tr><td align="center"><span style="display: block; margin-bottom: 10px; margin-top: 10px;" id="charttitle"><font size=4><b>Items Sold</b></font></span></td></tr>
	<tr><td align="left">
	
		<table width="370" border="1" bordercolor="gray" cellpadding="2" cellspacing="0" id="statstable">
		<thead>
			<tr>
				<td bgcolor="#EEEEEE"><font size=3>Item</font></td>
				<td bgcolor="#EEEEEE"><font size=3>Sales</font></td>
				<td bgcolor="#EEEEEE"><font size=3>Units<br>Sold</font></td>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
	
	</td></tr></table>
	</div>

</td></tr>

</table>

<!-- End Main Table -->

<link href="salesglanceflotlayout.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="../flotjquery.js"></script>
<script language="javascript" type="text/javascript" src="../jquery.flot.js"></script>
<script language="JavaScript" src="../startxmlhttp.js"></script>

<script language="javascript">

// Connection Function
function ajax_connect(url) {

	graph_xmlhttp = start_xml_http();

	if(!graph_xmlhttp) {
		alert('Your browser is blocking or does not support Ajax');
		return false;
	}

	graph_xmlhttp.onreadystatechange = function() {
	if (graph_xmlhttp.readyState == 4) {
	
		graphresponse = graph_xmlhttp.responseText;
		
		// Pull The Start Date
		var startsdate = graphresponse.indexOf('#STARTSTARTDATE#') + 16;
		if (graphresponse.indexOf('#STARTSTARTDATE#') > -1) {
			var endsdate = graphresponse.indexOf('#ENDSTARTDATE#');
			var graphsdate = graphresponse.substring(startsdate, endsdate);	
			document.getElementById('DPC_sdate_YYYY-MM-DD').value = graphsdate;
			tempdateobj = new Date(graphsdate);
			stimestamp = (tempdateobj.getTime()) / 1000;
		}
		
		// Pull The End Date
		var startedate = graphresponse.indexOf('#STARTENDDATE#') + 14;
		if (graphresponse.indexOf('#STARTENDDATE#') > -1) {
			var endedate = graphresponse.indexOf('#ENDENDDATE#');
			var graphedate = graphresponse.substring(startedate, endedate);	
			document.getElementById('DPC_edate_YYYY-MM-DD').value = graphedate;
			tempdateobj = new Date(graphedate);
			etimestamp = (tempdateobj.getTime()) / 1000;	
		}
		
		// Pull The Graph Type
		var startgraphtype = graphresponse.indexOf('#STARTGRAPHTYPE#') + 16;
		if (graphresponse.indexOf('#STARTGRAPHTYPE#') > -1) {
			var endgraphtype = graphresponse.indexOf('#ENDGRAPHTYPE#');
			var graphgraphtype = graphresponse.substring(startgraphtype, endgraphtype);	
			if (graphgraphtype == 'monthly') {
				MonthlyGraph = true;
			} else {
				MonthlyGraph = false;
			}
		}
		
		// Pull The Graph Data Label
		var startdatalabel = graphresponse.indexOf('#STARTDATALABEL#') + 16;
		if (graphresponse.indexOf('#STARTDATALABEL#') > -1) {
			var enddatalabel = graphresponse.indexOf('#ENDDATALABEL#');
			var graphdatalabel = graphresponse.substring(startdatalabel, enddatalabel);	
		}
		
		// Pull The Graph Data
		var startdata = graphresponse.indexOf('#STARTDATA#') + 11;
		if (graphresponse.indexOf('#STARTDATA#') > -1) {
			var enddata = graphresponse.indexOf('#ENDDATA#');
			var graphdata = graphresponse.substring(startdata, enddata);
			
			// Break up the results and put them back in usable arrays
			var arraygraphdata = graphdata.split('|');
			var arrlength = arraygraphdata.length;
			var temparray = [];
			for (var i = 0; i < arrlength; i++) {
				temparray = arraygraphdata[i].split(',');
				arraygraphdata[i] = [temparray[0], temparray[1]];
			}
		} else {
			var arraygraphdata = [];
		}
		
		// Pull The Ticks Data
		var startticks = graphresponse.indexOf('#STARTTICKS#') + 12;
		if (graphresponse.indexOf('#STARTTICKS#') > -1) {
			var endticks = graphresponse.indexOf('#ENDTICKS#');
			var graphticks = graphresponse.substring(startticks, endticks);
			
			// Break up the results and put them back in usable arrays
			var arraygraphticks = graphticks.split('|');
			var arrlength = arraygraphticks.length;
			var temparray = [];
			for (var i = 0; i < arrlength; i++) {
				temparray = arraygraphticks[i].split(',');
				arraygraphticks[i] = [temparray[0], temparray[1]];
			}
			
		} else {
			var arraygraphticks = [];
		}
		
		// Pull The Max Stat (For Top Y Axis)
		var startmax = graphresponse.indexOf('#STARTMAX#') + 10;
		if (graphresponse.indexOf('#STARTMAX#') > -1) {
			var endmax = graphresponse.indexOf('#ENDMAX#');
			var graphmax = graphresponse.substring(startmax, endmax);		
		}
		
		// Pull The Zoom In Data
		var startzoomin = graphresponse.indexOf('#STARTZOOMIN#') + 13;
		if (graphresponse.indexOf('#STARTZOOMIN#') > -1) {
			var endzoomin = graphresponse.indexOf('#ENDZOOMIN#');
			var canzoomin = graphresponse.substring(startzoomin, endzoomin);
			if (canzoomin == "yes") {
				document.getElementById("zoominbtn").disabled = false;
			} else {
				document.getElementById("zoominbtn").disabled = true;
			}
		} else {
			document.getElementById("zoominbtn").disabled = true;
		}
		
		// Pull The Zoom Out Data
		var startzoomout = graphresponse.indexOf('#STARTZOOMOUT#') + 14;
		if (graphresponse.indexOf('#STARTZOOMOUT#') > -1) {
			var endzoomout = graphresponse.indexOf('#ENDZOOMOUT#');
			var canzoomout = graphresponse.substring(startzoomout, endzoomout);
			if (canzoomout == "yes") {
				document.getElementById("zoomoutbtn").disabled = false;
			} else {
				document.getElementById("zoomoutbtn").disabled = true;
			}	
		} else {
			document.getElementById("zoomoutbtn").disabled = true;
		}
		
		// Pull The Pan Left Data
		var startpanleft = graphresponse.indexOf('#STARTPANLEFT#') + 14;
		if (graphresponse.indexOf('#STARTPANLEFT#') > -1) {
			var endpanleft = graphresponse.indexOf('#ENDPANLEFT#');
			var canpanleft = graphresponse.substring(startpanleft, endpanleft);
			if (canpanleft == "yes") {
				document.getElementById("panleftbtn").disabled = false;
			} else {
				document.getElementById("panleftbtn").disabled = true;
			}		
		} else {
			document.getElementById("panleftbtn").disabled = true;
		}
		
		// Pull The Pan Right Data
		var startpanright = graphresponse.indexOf('#STARTPANRIGHT#') + 15;
		if (graphresponse.indexOf('#STARTPANRIGHT#') > -1) {
			var endpanright = graphresponse.indexOf('#ENDPANRIGHT#');
			var canpanright = graphresponse.substring(startpanright, endpanright);
			if (canpanright == "yes") {
				document.getElementById("panrightbtn").disabled = false;
			} else {
				document.getElementById("panrightbtn").disabled = true;
			}	
		} else {
			document.getElementById("panrightbtn").disabled = true;
		}
		
		// Pull The Sales Data
		    var titlemonths = new Array();
                    titlemonths[0] = "Jan";
                    titlemonths[1] = "Feb";
                    titlemonths[2] = "Mar";
                    titlemonths[3] = "Apr";
                    titlemonths[4] = "May";
                    titlemonths[5] = "Jun";
                    titlemonths[6] = "Jul";
                    titlemonths[7] = "Aug";
                    titlemonths[8] = "Sep";
                    titlemonths[9] = "Oct";
                    titlemonths[10] = "Nov";
                    titlemonths[11] = "Dec";
                    
                    var titlesdateobj = graphsdate.split("-");
                    var titlesyear = titlesdateobj[0];
                    var titlesmonth = titlemonths[parseInt(titlesdateobj[1])-1];
                    var titlesday = titlesdateobj[2];
                    
                    var titleedateobj = graphedate.split("-");
                    var titleeyear = titleedateobj[0];
                    var titleemonth = titlemonths[parseInt(titleedateobj[1])-1];
                    var titleeday = titleedateobj[2];
                    
                    var titletext = titlesmonth + " " + titlesday + " " + titlesyear + " - " + titleemonth + " " + titleeday + " " + titleeyear;
                    
                    createTable();
                
		var startsalesdata = graphresponse.indexOf('#STARTSALESDATA#') + 16;
		if (graphresponse.indexOf('#STARTSALESDATA#') > -1) {
			var endsalesdata = graphresponse.indexOf('#ENDSALESDATA#');
			var salesdata = graphresponse.substring(startsalesdata, endsalesdata);
			
			// Build The Sales Data Table
			if (salesdata.indexOf('|') > -1) {
				
				var arraydata = salesdata.split('|');
				var arrlength = arraydata.length;
				
				// Set The Totals
				var temptotaldata = String(arraydata[0]);
				var splittotaldata = temptotaldata.split(',');
				var totalsales = parseInt(splittotaldata[0]);
				var totalunits = parseInt(splittotaldata[1]);
				document.getElementById("charttitle").innerHTML = '<font size=4><b>' + titletext + '</b></font><br><font size=3>Total Sales: <b>$' + totalsales + '</b></font><br><font size=3>Units Sold: <b>' + totalunits + '</b></font>';
				
				// Loop Through Each Item
				if (arrlength > 1) {
					for (var i = 1; i+1 < arrlength; i++) {
						var tempitemdata = String(arraydata[i]);
						var splititemdata = tempitemdata.split(',');
						
						var newitemname = String(splititemdata[0]);
						var newitemsales = parseInt(splititemdata[1]);
						var newitemsalesp = parseInt(splititemdata[2]);
						var newitemunits = parseInt(splititemdata[3]);
						var newitemunitsp = parseInt(splititemdata[4]);
						
						if (newitemname.length > 30) {
							newitemname = newitemname.substring(0,27) + '...';
						}
						
						addItem(newitemname, newitemsales, newitemsalesp, newitemunits, newitemunitsp);
					}
				}
			
			} else {
				addTableText(salesdata);
			}
			// End Build The Sales Data Table
			
		}
		
		document.getElementById("loadingimg").src = 'graphblankimg.jpg';
		
		var maxy = ceilpow10(graphmax);
		drawgraph(graphdatalabel, arraygraphdata, maxy, arraygraphticks);
	}
	}
	
	// Disable Buttons While The Graph Is Loading
	document.getElementById("zoominbtn").disabled = true;
	document.getElementById("zoomoutbtn").disabled = true;
	document.getElementById("panleftbtn").disabled = true;
	document.getElementById("panrightbtn").disabled = true;
	
	document.getElementById("loadingimg").src = 'graphloading.gif';

	graph_xmlhttp.open('GET',url,true);
	graph_xmlhttp.send(null);
}

function newGraph(userdate, datatype, tabletype, starttime, endtime) {

	// Create Dates From Time Stamps
	startdateobj = new Date(starttime*1000);
	startyear = startdateobj.getFullYear();
	startmonth = (startdateobj.getMonth())+1;
	startday = startdateobj.getDate();
	startdate = startyear + '-' + startmonth + '-' + startday;
	enddateobj = new Date(endtime*1000);
	endyear = enddateobj.getFullYear();
	endmonth = (enddateobj.getMonth())+1;
	endday = enddateobj.getDate();
	enddate = endyear + '-' + endmonth + '-' + endday;
	
	if (startdate==enddate) {
		return 0;
	}
	
	if (endtime < starttime) {
		return 0;
	}
	
	numdays = countDays(startdate, enddate);
	
	if (numdays <= 0) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "salesglance_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&datatype=" + datatype + "&tabletype=" + tabletype + "&userdate=" + userdate;
	ajax_connect(graphurl);

}

function clickZoomIn(datatype, tabletype, starttime, endtime, clickx) {
	
	timedistance = endtime - starttime;
	distancefourths = Math.round(timedistance/4);
	
	// Last Center Time
	centertime = (starttime+endtime)/2;
	
	// Unix Timestamp Of Clicked Date
	clickx = clickx/1000;
	
	if (timedistance < 1209600) {
		// Change table to clicked date
		dateobj = new Date((clickx+40000)*1000);
		clickxyear = dateobj.getFullYear();
		clickxmonth = dateobj.getMonth() + 1;
		clickxday = dateobj.getDate();
		
		if (clickxmonth < 10) {
			clickxmonth = "0" + clickxmonth;
		}
		
		if (clickxday < 10) {
			clickxday = "0" + clickxday;
		}
		
		clickxfulldate = clickxyear + "-" + clickxmonth + "-" + clickxday;
		
		oneday_stats(clickxfulldate, ((clickx+40000)*1000), datatype);
		return 1;
	}
	
	// Number Of Seconds From The Center Date
	clickx = Math.round(clickx - centertime);
	
	// Center To The Clicked Point
	starttime = starttime + clickx;
	endtime = starttime + timedistance;
	
	// Zoom Into Clicked Point
	if (document.getElementById("zoominbtn").disabled == false) {
		starttime = starttime + distancefourths;
		endtime = starttime + (distancefourths*2);
	}
	
	if ((endtime - starttime) < 86400) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "salesglance_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&datatype=" + datatype + "&tabletype=" + tabletype + "&userdate=0";
	ajax_connect(graphurl);

}

function changeZoom(zoom, datatype, tabletype, starttime, endtime) {
	
	timedistance = endtime - starttime;
	distancefourths = Math.round(timedistance/4);
	
	if (zoom == 0 && document.getElementById("zoominbtn").disabled == false) {
		// Zoom In
		starttime = starttime + distancefourths + 86400;
		endtime = starttime + (distancefourths*2);
	} else if (document.getElementById("zoomoutbtn").disabled == false) {
		// Zoom Out
		starttime = starttime - (distancefourths*2) + 86400;
		endtime = starttime + (distancefourths*8);
	} else {
		// Zoom Disabled
		return 0;
	}
	
	if ((endtime - starttime) < 86400) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "salesglance_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&datatype=" + datatype + "&tabletype=" + tabletype + "&userdate=0";
	ajax_connect(graphurl);
	
}

function changePan(direction, datatype, tabletype, starttime, endtime) {
	
	timedistance = endtime - starttime;
	
	if (timedistance < 1209600) {
		pandistance = 86400/2;
		rightpandistance = 86400*2;
	} else if (timedistance < 3456000) {
		pandistance = Math.round(timedistance/8);
		rightpandistance = pandistance*1.5;
	} else {
		pandistance = Math.round(timedistance/3);
		rightpandistance = pandistance;
	}
	
	if (direction == 0 && document.getElementById("panleftbtn").disabled == false) {
		// Pan Left
		starttime = starttime - pandistance;
		endtime = starttime + timedistance;
	} else if (document.getElementById("panrightbtn").disabled == false) {
		// Pan Right
		starttime = starttime + rightpandistance;
		endtime = starttime + timedistance;
	} else {
		// Pan Disabled
		return 0;
	}
	
	if ((endtime - starttime) < 86400) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "salesglance_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&datatype=" + datatype + "&tabletype=" + tabletype + "&userdate=0";
	ajax_connect(graphurl);
	
}

function countDays(firstdate, lastdate) {

	daycount = 0;
	
	d = firstdate.match(/\d+/g);
	firstdatetime = new Date(d[0], d[1] - 1, d[2]).getTime();

	d = lastdate.match(/\d+/g);
	lastdatetime = new Date(d[0], d[1] - 1, d[2]).getTime();

	currdate = firstdatetime;
	
	while (currdate <= lastdatetime) {
		daycount++;
		currdate = currdate + 86400000;
	}
	return daycount;
}

function ceilpow10(val) {
	return Math.ceil(val/10)*10;
}

</script>

<script language="JavaScript" src="salesglancetablefuncs.js"></script>


<script type="text/javascript">

	var onedayURL = "salesglance_ajax.php?tabletype=oneday";
	
	// Pull Detailed Stats Function
	function oneday_stats(date, datetimestamp, datatype) {
		
                    var titlemonths = new Array();
                    titlemonths[0] = "Jan";
                    titlemonths[1] = "Feb";
                    titlemonths[2] = "Mar";
                    titlemonths[3] = "Apr";
                    titlemonths[4] = "May";
                    titlemonths[5] = "Jun";
                    titlemonths[6] = "Jul";
                    titlemonths[7] = "Aug";
                    titlemonths[8] = "Sep";
                    titlemonths[9] = "Oct";
                    titlemonths[10] = "Nov";
                    titlemonths[11] = "Dec";
                    
                    var titledateobj = new Date(datetimestamp);
                    var titleyear = titledateobj.getFullYear();
                    var titlemonth = titlemonths[titledateobj.getMonth()];
                    var titleday = titledateobj.getDate();
                    var titletext = titlemonth + " " + titleday + " " + titleyear;
		
		document.getElementById("charttitle").innerHTML = '<font size=4><b>Sales Data For ' + titletext + '</b></font>';
		
		createTable();
		
		xmlhttpdetails = start_xml_http();
		
		if(!xmlhttpdetails) {
			alert('Your browser is blocking or does not support Ajax');
			return false;
		}
		
		xmlhttpdetails.onreadystatechange = function() {
			if (xmlhttpdetails.readyState == 4) {
				var buildresponse = xmlhttpdetails.responseText;
				
				if (buildresponse.indexOf('|') > -1) {
					
					var arraydata = buildresponse.split('|');
					var arrlength = arraydata.length;
					
					// Set The Totals
					var temptotaldata = String(arraydata[0]);
					var splittotaldata = temptotaldata.split(',');
					var totalsales = parseInt(splittotaldata[0]);
					var totalunits = parseInt(splittotaldata[1]);
					document.getElementById("charttitle").innerHTML = '<font size=4><b>Sales Data For ' + titletext + '</b></font><br><font size=3>Total Sales: <b>$' + totalsales + '</b></font><br><font size=3>Units Sold: <b>' + totalunits + '</b></font>';
					
					// Loop Through Each Item
					if (arrlength > 1) {
						for (var i = 1; i+1 < arrlength; i++) {
							var tempitemdata = String(arraydata[i]);
							var splititemdata = tempitemdata.split(',');
							
							var newitemname = String(splititemdata[0]);
							var newitemsales = parseInt(splititemdata[1]);
							var newitemsalesp = parseInt(splititemdata[2]);
							var newitemunits = parseInt(splititemdata[3]);
							var newitemunitsp = parseInt(splititemdata[4]);
							
							if (newitemname.length > 30) {
								newitemname = newitemname.substring(0,27) + '...';
							}
							
							addItem(newitemname, newitemsales, newitemsalesp, newitemunits, newitemunitsp);
						}
					}
				
				} else {
					addTableText(buildresponse);
				}
			}
		}
		
		newDate = new Date();
		milliCount = newDate.getTime();
		xmlhttpdetails.open('GET',onedayURL + "&date=" + date + "&datatype=" + datatype + "&m=" + milliCount,true);
		xmlhttpdetails.send(null);
		
	}
</script>



<script type="text/javascript">
	
	// Set Initial VisuGraph Values
	var ShowTooltips = true;
	var MonthlyGraph = false;
	var stimestamp = <? echo($starttime); ?>;
	var etimestamp = <? echo($endtime); ?>;
	
</script>

<script language="javascript" type="text/javascript" src="salesglanceflotfunctions.js"></script>

<script type="text/javascript">
	// Draw The Initial Graph
	<? echo("newGraph(0, 1, '".$logtype."', ".$starttime.", ".$endtime.");"); ?>
</script>
