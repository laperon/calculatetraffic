<?php

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// SalesGlance - Created by Josh Abbott                                //
// Not for resale.  Version included with the LFM script only.         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// -- Graph Functions --
// VisuGraph JS 1.0
// �2012 Josh Abbott, http://visugraph.com
// Licensed for the LFM script

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

function countdays($firstdate, $lastdate) {
	$currdate = $firstdate;
	$daycount = 0;
	while ($currdate <= $lastdate) {
		$daycount++;
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
	}
	return $daycount;
}

// Prepare Graph

$datatype = $_GET['datatype'];
$tabletype = $_GET['tabletype'];

// Data Type 1 = Dollar Amount Of Sales
// Data Type 2 = Number Of Units Sold
if ($datatype == "" || !is_numeric($datatype) || $datatype < 1 || $datatype > 2) {
	// Set to default
	$datatype = 1;
}

if ($datatype == 1) {
	$searchvalue = "SUM(amount) AS sales";
	$sortvalue = "sales";
	$datalabel = "Sales";
} else {
	$searchvalue = "COUNT(*) AS units";
	$sortvalue = "units";
	$datalabel = "Units Sold";
}

// Check for extra conditions for payment types and sales packages

$extracondition = "";
$salespacks = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='salespacks'"), 0);

if ($salespacks != "" && strlen($salespacks) > 0) {
	$extracondition = "item_number IN (".$salespacks.")";
}

$showpaypal = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showpaypal'"), 0);
$showpayza = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showpayza'"), 0);
$show2co = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='show2co'"), 0);
$showclickbank = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showclickbank'"), 0);
$showcomms = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showcomms'"), 0);

if ($showpaypal != 1 || $showpayza != 1 || $show2co != 1 || $showclickbank != 1 || $showcomms != 1) {
	$paytypeconds = array();
	if ($showpaypal == 1) {
		$paytypeconds[] = "'paypal'";
	}
	if ($showpayza == 1) {
		$paytypeconds[] = "'payza'";
	}
	if ($show2co == 1) {
		$paytypeconds[] = "'2checkout'";
	}
	if ($showclickbank == 1) {
		$paytypeconds[] = "'clickbank'";
	}
	if ($showcomms == 1) {
		$paytypeconds[] = "'commissions'";
	}
	if (count($paytypeconds) > 0) {
		$paytypecond = implode(",", $paytypeconds);
		if (strlen($extracondition) > 0) {
			$extracondition .= " AND processor IN(".$paytypecond.")";
		} else {
			$extracondition = "processor IN(".$paytypecond.")";
		}
	}
}

if (strlen($extracondition) > 0) {
	$extrawhere = " WHERE ".$extracondition;
	$extraand = " AND ".$extracondition;
} else {
	$extrawhere = "";
	$extraand = "";
}


if ($tabletype == "oneday") {
	
	// Pull the items list sold on a specific day
	
	if (isset($_GET['date']) && strlen($_GET['date']) == 10) {
		$date = $_GET['date'];	
	} else {
		echo("Invalid Date");
		exit;
	}
	
	// Get the totals for that day
	$gettotals = lfmsql_query("Select SUM(amount) AS sales, COUNT(*) AS units from ".$prefix."ipn_transactions WHERE added >= '".$date." 00:00:00' AND added <= '".$date." 23:59:59'".$extraand);
	$totalunits = lfmsql_result($gettotals, 0, "units");
	if ($totalunits < 1) {
		echo("No sales found on this date.");
		exit;
	}
	$totalsales = lfmsql_result($gettotals, 0, "sales");
	if (!isset($totalsales) || !is_numeric($totalsales) || $totalsales < 0) {
		$totalsales = "0.00";
	}
	$totalsales = round($totalsales, 2);
	
	echo($totalsales.",".$totalunits."|");
	
	// Get the totals of each item sold on that day
	$getitems = lfmsql_query("Select item_number, item_name, sum(amount) AS sales, count(*) AS units from ".$prefix."ipn_transactions WHERE added >= '".$date." 00:00:00' AND added <= '".$date." 23:59:59'".$extraand." group by item_number order by ".$sortvalue." desc");
	
	for ($i = 0; $i < lfmsql_num_rows($getitems); $i++) {
		
		$itemname = lfmsql_result($getitems, $i, "item_name");
		$itemsales = lfmsql_result($getitems, $i, "sales");
		$itemunits = lfmsql_result($getitems, $i, "units");
		
		if (strpos($itemname, '-') !== false) {
			// Removes any transaction-specific details from the name
			$splititemname = explode('-', $itemname);
			$itemname = trim($splititemname[0]);
		}
		$itemname = str_replace(",", '', $itemname);
		$itemname = str_replace("|", '', $itemname);
		
		$percentunits = ($itemunits/$totalunits)*100;
		$percentunits = round($percentunits, 2);
		
		if ($totalsales > 0) {
			$percentsales = ($itemsales/$totalsales)*100;
			$percentsales = round($percentsales, 2);
		} else {
			$percentsales = "0";
		}
		
		echo($itemname.",".$itemsales.",".$percentsales.",".$itemunits.",".$percentunits."|");
	}
	
	exit;
	
}
	
// Pull the items list and graph sold over a date range

$starttime = $_GET['starttime'];
$endtime = $_GET['endtime'];

if (!is_numeric($starttime) || $starttime < 1) { exit; }
if (!is_numeric($endtime) || $endtime < 1) { exit; }

$startdate = date("Y-m-d",$starttime);
$enddate = date("Y-m-d",$endtime);

// Check The Date Range Available
$currentdate = date("Y-m-d");

$getfirstdate = lfmsql_query("Select added from ".$prefix."ipn_transactions".$extrawhere." order by added asc limit 1");
if (lfmsql_num_rows($getfirstdate) > 0) {
	$firstavaildate = strftime("%Y-%m-%d", strtotime(lfmsql_result($getfirstdate, 0, "added")));
} else {
	$firstavaildate = strftime("%Y-%m-%d", strtotime("$currentdate + 30 days ago"));
}

$getlastdate = lfmsql_query("Select added from ".$prefix."ipn_transactions".$extrawhere." order by added desc limit 1");
if (lfmsql_num_rows($getlastdate) > 0) {
	$lastavaildate = strftime("%Y-%m-%d", strtotime(lfmsql_result($getlastdate, 0, "added")));
} else {
	$lastavaildate = $currentdate;
}

if (((strtotime($lastavaildate) - strtotime($firstavaildate)) > 604800) && (isset($_GET['userdate']) && $_GET['userdate'] == 0)) {
	if (strtotime($startdate) < strtotime($firstavaildate)) {
		$startdate = $firstavaildate;
	}
	if (strtotime($enddate) > strtotime($lastavaildate)) {
		$enddate = $lastavaildate;
	}
	if (strtotime($startdate) >= strtotime($enddate)) {
		$startdate = strftime("%Y-%m-%d", strtotime("$enddate + 30 days ago"));
	}
}

$numdays = countdays($startdate, $enddate);
$numelems = 0;

if ($numdays > 40) {
	//Switch to monthly view
	
	$graphtype = "monthly";

	$currdate = $startdate;
	
	while ($currdate <= $enddate) {
	
	$selecteddate = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
	
	$selectedmonth = date("m", $selecteddate);
	$selectedyear = date("Y", $selecteddate);
	$daya = $selectedyear."-".$selectedmonth."-"."01";
	$dayb = $selectedyear."-".$selectedmonth."-"."31";
	$monthname = date("M", $selecteddate);
		
	if ($monthname == "Jan") {
		$monthname = "Jan<br>".$selectedyear;
	} else {
		$monthname = $monthname."<br><br>";
	}
	
	$getmonth = lfmsql_query("Select ".$searchvalue." from ".$prefix."ipn_transactions WHERE added >= '".$daya." 00:00:00' AND added <= '".$dayb." 23:59:59'".$extraand);
	
	$currentsum = lfmsql_result($getmonth, 0, $sortvalue);
	if ($currentsum == NULL) {
		$currentsum = 0;
	}

	$statsum[$numelems] = $currentsum;
	$xname[$numelems] = $monthname;
	$xvals[$numelems] = mktime(0,0,0,substr($daya,5,2),2,substr($daya,0,4)) * 1000;
	$numelems = $numelems+1;
	
	$currdate = strftime("%Y-%m-%d", strtotime("$daya + 1 month"));
	
	}
	
	
	if ($selectedmonth != date("m", strtotime($enddate))) {
		// Run the last month
		
		$selecteddate = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
		
		$selectedmonth = date("m", $selecteddate);
		$selectedyear = date("Y", $selecteddate);
		$daya = $selectedyear."-".$selectedmonth."-"."01";
		$dayb = $selectedyear."-".$selectedmonth."-"."31";
		$monthname = date("M", $selecteddate);
			
		if ($monthname == "Jan") {
			$monthname = "Jan<br>".$selectedyear;
		} else {
			$monthname = $monthname."<br><br>";
		}
		
		$getmonth = lfmsql_query("Select ".$searchvalue." from ".$prefix."ipn_transactions WHERE added >= '".$daya." 00:00:00' AND added <= '".$dayb." 23:59:59'".$extraand);
		
		$currentsum = lfmsql_result($getmonth, 0, $sortvalue);
		if ($currentsum == NULL) {
			$currentsum = 0;
		}
		
		$statsum[$numelems] = $currentsum;
		$xname[$numelems] = $monthname;
		$xvals[$numelems] = mktime(0,0,0,substr($daya,5,2),2,substr($daya,0,4)) * 1000;
		$numelems = $numelems+1;
	}
	

} else {

	$graphtype = "daily";

	$numelems = $numdays;

	$getdays = lfmsql_query("Select ".$searchvalue.", DATE_FORMAT(added, '%Y-%m-%d') AS date from ".$prefix."ipn_transactions where added >= '".$startdate." 00:00:00' and added <= '".$enddate." 23:59:59'".$extraand." GROUP BY DATE_FORMAT(added, '%Y-%m-%d') ORDER BY date ASC");
	
	if (lfmsql_num_rows($getdays) > 0) {
		for ($i = 0; $i < lfmsql_num_rows($getdays); $i++) {
			$thedate = lfmsql_result($getdays, $i, "date");
			$datelist[$i] = $thedate;
			$sumlist[$i] = lfmsql_result($getdays, $i, $sortvalue);
		}
	} else {
		// Set Empty Date Array
		$datelist[0] = 0;
	}
	
	$currdate = $startdate;
	$i = 0;
	while ($currdate <= $enddate) {
	
		$dateposition = array_search($currdate, $datelist);
		if ($dateposition === false) {
			//No Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = 0;
			$xname[$i] = date("d", $thetime);
			if ($xname[$i] == 1) { $xname[$i] = $xname[$i]."<br>".date("M", $thetime); } else { $xname[$i] = $xname[$i]."<br><br>"; }
			$xvals[$i] = $thetime * 1000;
		} else {
			//Enter Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = $sumlist[$dateposition];
			$xname[$i] = date("d", $thetime);
			if ($xname[$i] == 1) { $xname[$i] = $xname[$i]."<br>".date("M", $thetime); } else { $xname[$i] = $xname[$i]."<br><br>"; }
			$xvals[$i] = $thetime * 1000;
		}
	
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
		$i++;
	}
}

// Check Zoom In
if ($numdays > 7) {
	$canzoomin = "yes";
} else {
	$canzoomin = "no";
}

// Check Zoom Out
if ($numdays >= 1095) {
	// Show A Max Of 3 Years
	$canzoomout = "no";
} elseif ($firstavaildate < $startdate || $lastavaildate > $enddate) {
	$canzoomout = "yes";
} else {
	$canzoomout = "no";
}

// Check Pan Left
if ($firstavaildate < $startdate) {
	$canpanleft = "yes";
} else {
	$canpanleft = "no";
}

// Check Pan Right
if ($lastavaildate > $enddate) {
	$canpanright = "yes";
} else {
	$canpanright = "no";
}


// End Prepare Graph

if ($numelems < 1) {
	// No Data To Display
	exit;
}

// Make Data Array And Ticks (Axis Labels)
$datatext = $xvals[0].",".$statsum[0];
$tickstext = $xvals[0].",".$xname[0];
for ($i = 1; $i < $numelems; $i++) {
	$datatext .= "|".$xvals[$i].",".$statsum[$i];
	$tickstext .= "|".$xvals[$i].",".$xname[$i];			
}

// Set The Top Line On The Y Axis By Finding The Highest Stat
if ($numdays <= 40) {
	$checkstartdate = strftime("%Y-%m-%d", strtotime("$startdate + 45 days ago"));
	$checkenddate = strftime("%Y-%m-%d", strtotime("$enddate + 45 days"));
	$dailysums = lfmsql_query("SELECT ".$searchvalue." FROM `".$prefix."ipn_transactions` where added >= '".$checkstartdate." 00:00:00' and added <= '".$checkenddate." 23:59:59'".$extraand." GROUP BY DATE_FORMAT(added, '%Y-%m-%d')");
	if (lfmsql_num_rows($dailysums) > 1) {
		while ($row = lfmsql_fetch_array($dailysums)) {
			$sumsarray[] = $row[$sortvalue];
		}
		$maxval = max($sumsarray);
		$avgval = array_sum($sumsarray) / count($sumsarray);
		if (($avgval*15) >= $maxval) {
			// Keeps A Consistent Axis Unless Max Value Is An Outlier
			$maxstat = $maxval;
		} else {
			$maxstat = max($statsum);
		}
	} else {
		$maxstat = max($statsum);
	}
} else {
	$monthlysums = lfmsql_query("SELECT ".$searchvalue." FROM `".$prefix."ipn_transactions`".$extrawhere." group by YEAR(added), MONTH(added)");
	if (lfmsql_num_rows($monthlysums) > 1) {
		while ($row = lfmsql_fetch_array($monthlysums)) {
			$sumsarray[] = $row[$sortvalue];
		}
		$maxval = max($sumsarray);
		$avgval = array_sum($sumsarray) / count($sumsarray);
		if (($avgval*15) >= $maxval) {
			// Keeps A Consistent Axis Unless Max Value Is An Outlier
			$maxstat = $maxval;
		} else {
			$maxstat = max($statsum);
		}
	} else {
		$maxstat = max($statsum);
	}
}

// Get The Sales Data
$gettotals = lfmsql_query("Select SUM(amount) AS sales, COUNT(*) AS units from ".$prefix."ipn_transactions WHERE added >= '".$startdate." 00:00:00' AND added <= '".$enddate." 23:59:59'".$extraand);
$totalunits = lfmsql_result($gettotals, 0, "units");
if ($totalunits < 1) {
	$salesdata = "No sales found during this period.";
} else {
	$totalsales = lfmsql_result($gettotals, 0, "sales");
	if (!isset($totalsales) || !is_numeric($totalsales) || $totalsales < 0) {
		$totalsales = "0.00";
	}
	$totalsales = round($totalsales, 2);
	
	$salesdata = $totalsales.",".$totalunits."|";
	
	// Get the totals of each item sold on that day
	$getitems = lfmsql_query("Select item_number, item_name, sum(amount) AS sales, count(*) AS units from ".$prefix."ipn_transactions WHERE added >= '".$startdate." 00:00:00' AND added <= '".$enddate." 23:59:59'".$extraand." group by item_number order by ".$sortvalue." desc");
	
	for ($i = 0; $i < lfmsql_num_rows($getitems); $i++) {
		
		$itemname = lfmsql_result($getitems, $i, "item_name");
		$itemsales = lfmsql_result($getitems, $i, "sales");
		$itemunits = lfmsql_result($getitems, $i, "units");
		
		if (strpos($itemname, '-') !== false) {
			// Removes any transaction-specific details from the name
			$splititemname = explode('-', $itemname);
			$itemname = trim($splititemname[0]);
		}
		$itemname = str_replace(",", '', $itemname);
		$itemname = str_replace("|", '', $itemname);
		
		$percentunits = ($itemunits/$totalunits)*100;
		$percentunits = round($percentunits, 2);
		
		if ($totalsales > 0) {
			$percentsales = ($itemsales/$totalsales)*100;
			$percentsales = round($percentsales, 2);
		} else {
			$percentsales = "0";
		}
		
		$salesdata .= $itemname.",".$itemsales.",".$percentsales.",".$itemunits.",".$percentunits."|";
	}
}

// Output The Data
echo("#STARTSTARTDATE#".$startdate."#ENDSTARTDATE#");
echo("#STARTENDDATE#".$enddate."#ENDENDDATE#");

echo("#STARTGRAPHTYPE#".$graphtype."#ENDGRAPHTYPE#");
echo("#STARTDATALABEL#".$datalabel."#ENDDATALABEL#");

echo("#STARTDATA#".$datatext."#ENDDATA#");
echo("#STARTTICKS#".$tickstext."#ENDTICKS#");
echo("#STARTMAX#".$maxstat."#ENDMAX#");

echo("#STARTZOOMIN#".$canzoomin."#ENDZOOMIN#");
echo("#STARTZOOMOUT#".$canzoomout."#ENDZOOMOUT#");
echo("#STARTPANLEFT#".$canpanleft."#ENDPANLEFT#");
echo("#STARTPANRIGHT#".$canpanright."#ENDPANRIGHT#");

echo("#STARTSALESDATA#".$salesdata."#ENDSALESDATA#");

exit;

?>