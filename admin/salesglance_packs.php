<?php

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// SalesGlance - Created by Josh Abbott                                //
// Not for resale.  Version included with the LFM script only.         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

?>

<html>

<head>
<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<style>
body {
  margin: 0;
  background-color: #FFFFFF;
}
</style>

<body>
<center>

<?

if (isset($_POST['Submit']) && $_POST['Submit'] == "Update Sales Packages") {
	
	if (!isset($_POST["salespacks"]) || !is_array($_POST["salespacks"])) {
		$salespacks = "";
	} else {
		$countpacks = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."ipn_products`"), 0);
		if (count($_POST["salespacks"]) == $countpacks) {
			$salespacks = "";
		} else {
			$salespacks = implode(",", $_POST["salespacks"]);
		}
	}
	
	lfmsql_query("UPDATE `".$prefix."ipn_salesglance` SET value='".$salespacks."' WHERE field='salespacks'") or die(lfmsql_error());
	
	echo("<br><div class=\"lfm_title\">Updated Successfully</div><br>");
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;	
}

if (isset($_GET['selectall']) && $_GET['selectall'] == 1) {
	lfmsql_query("UPDATE `".$prefix."ipn_salesglance` SET value='' WHERE field='salespacks'") or die(lfmsql_error());
}

####################

//Begin main page

####################

$salespacks = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='salespacks'"), 0);

?>

	<br><div class="lfm_title">SalesGlance Sales Packages</div>
	<br><div class="lfm_descr" style="text-align:left; margin:5px;">Choose which Sales Packages you want to be calculated in the graph and table.</div>
	<br><div class="lfm_descr"><a href="salesglance_packs.php?selectall=1">Select All</a> | <a href="salesglance_packs.php?deselectall=1">Deselect All</a></div>
	
	<form action="salesglance_packs.php" method="post">
	<table width="500" cellpadding="5" cellspacing="0" border="0">
	
	<tr><td>
	
	<?
	
	if (isset($_GET['deselectall']) && $_GET['deselectall'] == 1) {
		
		// No Packs Are Selected
		$getfields = lfmsql_query("SELECT id, name FROM `".$prefix."ipn_products` ORDER BY name ASC");
	     	for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
     			$fieldid = lfmsql_result($getfields, $i, "id");
     			$fieldname = lfmsql_result($getfields, $i, "name");
	     		echo('<input type="checkbox" name="salespacks[]" value="'.$fieldid.'"> <font size="2">'.$fieldname.'<br>');
	     	}
	     	
	} elseif ($salespacks == "") {
		
		// All Packs Are Selected
		$getfields = lfmsql_query("SELECT id, name FROM `".$prefix."ipn_products` ORDER BY name ASC");
	     	for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
     			$fieldid = lfmsql_result($getfields, $i, "id");
     			$fieldname = lfmsql_result($getfields, $i, "name");
	     		echo('<input type="checkbox" name="salespacks[]" value="'.$fieldid.'" checked> <font size="2">'.$fieldname.'<br>');
	     	}
	     	
	} else {
		
		// List Selected Packs
		$getfields = lfmsql_query("SELECT id, name FROM `".$prefix."ipn_products` ORDER BY name ASC");
	     	for ($i = 0; $i < lfmsql_num_rows($getfields); $i++) {
     			$fieldid = lfmsql_result($getfields, $i, "id");
     			$fieldname = lfmsql_result($getfields, $i, "name");
	     		echo('<input type="checkbox" name="salespacks[]" value="'.$fieldid.'"'); if(array_search($fieldid, explode(",", $salespacks)) !==false) { echo(" checked"); } echo('> <font size="2">'.$fieldname.'<br>');
	     	}
		// End List Selected Packs
		
	}
	?>
	
	</td></tr>
	
	<tr><td>
	<INPUT type="submit" name="Submit" value="Update Sales Packages">
	</td></tr>
	
	</table>
	</form>

<br><br>

</center>
</body>
</html>

<?php
exit;
?>