<?php

/////////////////////////////////////////////////////////////////////////
//                                                                     //
// SalesGlance - Created by Josh Abbott                                //
// Not for resale.  Version included with the LFM script only.         //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/funcs.php";
require_once "../inc/sql_funcs.php";

include "../inc/checkauth.php"; 

if(!isset($_SESSION["adminid"])) { exit; };

?>

<html>

<head>
<link href="lfm_admin_style.css" rel="stylesheet" type="text/css" />
<link href="styles.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../inc/qtip.js"></script>
<script type="text/javascript" src="../inc/jsfuncs.js"></script>
<script type="text/javascript" src="../inc/jquery.js"></script>
</head>

<style>
body {
  margin: 0;
  background-color: #FFFFFF;
}
</style>

<body>
<center>

<?

if (isset($_POST['Submit']) && $_POST['Submit'] == "Update Payment Types") {
	
	if ($_POST['showpaypal'] != "1") { $_POST['showpaypal'] = 0; }
	if ($_POST['showpayza'] != "1") { $_POST['showpayza'] = 0; }
	if ($_POST['show2co'] != "1") { $_POST['show2co'] = 0; }
	$_POST['showclickbank'] = 0;
	if ($_POST['showcomms'] != "1") { $_POST['showcomms'] = 0; }
	
	lfmsql_query("UPDATE `".$prefix."ipn_salesglance` SET value='".$_POST['showpaypal']."' WHERE field='showpaypal'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."ipn_salesglance` SET value='".$_POST['showpayza']."' WHERE field='showpayza'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."ipn_salesglance` SET value='".$_POST['show2co']."' WHERE field='show2co'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."ipn_salesglance` SET value='".$_POST['showclickbank']."' WHERE field='showclickbank'") or die(lfmsql_error());
	lfmsql_query("UPDATE `".$prefix."ipn_salesglance` SET value='".$_POST['showcomms']."' WHERE field='showcomms'") or die(lfmsql_error());
	
	echo("<br><div class=\"lfm_title\">Updated Successfully</div><br>");
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = window.opener.location.href;";
	echo "</script>";
	echo "<input name=\"Button\" type=\"button\" id=\"Submit\" value=\"Close\" onClick=\"javascript:self.close();\" />";
	exit;	
}

####################

//Begin main page

####################

$showpaypal = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showpaypal'"), 0);
$showpayza = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showpayza'"), 0);
$show2co = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='show2co'"), 0);
$showcomms = lfmsql_result(lfmsql_query("SELECT value FROM `".$prefix."ipn_salesglance` WHERE field='showcomms'"), 0);

?>

	<br><div class="lfm_title">SalesGlance Payment Types</div>
	<br><div class="lfm_descr" style="text-align:left; margin:5px;">Choose which payment types you want to be calculated in the graph and table.</div>
	
	<form action="salesglance_paytypes.php" method="post">
	<table width="500" cellpadding="5" cellspacing="0" border="0">
	
	<tr><td>
	
	<input type="checkbox" name="showpaypal" value="1"<? if($showpaypal == 1) { echo(" checked"); } ?>> <font size="2">PayPal</font><br>
	<input type="checkbox" name="showpayza" value="1"<? if($showpayza == 1) { echo(" checked"); } ?>> <font size="2">Payza</font><br>
	<input type="checkbox" name="show2co" value="1"<? if($show2co == 1) { echo(" checked"); } ?>> <font size="2">2Checkout</font><br>
	<input type="checkbox" name="showcomms" value="1"<? if($showcomms == 1) { echo(" checked"); } ?>> <font size="2">Paid With Commissions</font><br>
	
	</td></tr>
	
	<tr><td>
	<INPUT type="submit" name="Submit" value="Update Payment Types">
	</td></tr>
	
	</table>
	</form>

<br><br>

</center>
</body>
</html>

<?php
exit;
?>