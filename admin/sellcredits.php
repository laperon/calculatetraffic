<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



	// Prevent anyone who isn't logged in from opening this page
	include "../inc/checkauth.php"; 
	if(!isset($_SESSION["adminid"])) { exit; };

	$msg="";

// Add a product entry
if($_POST["Submit"] == "Add Product")
{
		
		if (($_POST["credits"] < 1) || !is_numeric($_POST["credits"])) {
			$credits = 1;
		} else {
			$credits = $_POST["credits"];
		}
		
		$memtype = $_POST["memtype"];
		$subtype = $_POST["subtype"];
		
		$checklastrow = mysql_query("Select prodrank from ".$prefix."products where credits>0 order by prodrank desc");
		if (mysql_num_rows($checklastrow) > 0) {
		$lastrow = mysql_result($checklastrow, 0, "prodrank");
		} else {
		$lastrow = 0;
		}
		$newprodrank = $lastrow+1;
		
		if(isset($_POST["visible"])) { $visible=1; } else { $visible=0; }
		
	$qry="INSERT INTO ".$prefix."products(price,_2co_id,free,credits,memtype,subtype,visible,prodrank) VALUES('".$_POST["price"]."','".$_POST["_2co_id"]."',0,$credits,$memtype,$subtype,$visible,$newprodrank)";
	mysql_query($qry);
}


//Change A prodrank
$prodrank = $_GET['prodrank'];
if ($prodrank == "up" || $prodrank == "down") {

$runchange = "yes";
$id = $_GET['productsid'];

$checkfirstrow = mysql_query("Select prodrank from ".$prefix."products where credits>0 order by prodrank asc");
$firstrow = mysql_result($checkfirstrow, 0, "prodrank");

$checklastrow = mysql_query("Select prodrank from ".$prefix."products where credits>0 order by prodrank desc");
$lastrow = mysql_result($checklastrow, 0, "prodrank");

if (($prodrank=="up") && ($id > $firstrow)) {
$change=$id-1;
}
elseif (($prodrank=="down") && ($id < $lastrow)) {
$change=$id+1;
}
else {
$runchange = "no";
}

if ($runchange == "yes") {
@mysql_query("Update ".$prefix."products set prodrank=0 where prodrank=$id and credits>0");
@mysql_query("Update ".$prefix."products set prodrank=$id where prodrank=$change and credits>0");
@mysql_query("Update ".$prefix."products set prodrank=$change where prodrank=0 and credits>0");
@mysql_query("ALTER TABLE ".$prefix."products ORDER BY prodrank;");
}

}

// Delete a product entry
if(isset($_GET["pd"]))
{
	mysql_query("DELETE FROM ".$prefix."products WHERE productid=".$_GET["pd"]);
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>
<p align="center">&nbsp;</p>
<form method="post" action="admin.php?f=scrds">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0">
          <tr class="admintd">
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Rank</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product ID</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Price</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"># Credits</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show To</font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Purchase Type</font></strong></td>
            <td width="150" nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">2CO ID </font></strong></td>
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Visible</font></strong></td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
    </tr>
		  <?
		  	// List product IDs/Filenames
			$prodres=@mysql_query("SELECT * FROM ".$prefix."products where credits>0");
			while($prodrow=@mysql_fetch_array($prodres))
			{ ?>
          <tr>
            <td nowrap="nowrap"><a href=admin.php?f=scrds&prodrank=up&productsid=<?=$prodrow["prodrank"];?>>Move Up</a><br><a href=admin.php?f=scrds&prodrank=down&productsid=<?=$prodrow["prodrank"];?>>Move Down</a></td>
            <td nowrap="nowrap"><?=$prodrow["productid"];?></td>
            <td nowrap="nowrap"><?=$prodrow["price"];?></td>
            <td><?=$prodrow["credits"];?></td>
            
            <td>
            <?

if ($prodrow["memtype"] == 0) {

echo("All Members");

} else {

$getaccounts = mysql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < mysql_num_rows($getaccounts); $j++) {
$accid = mysql_result($getaccounts, $j, "mtid");
$accname = mysql_result($getaccounts, $j, "accname");
if ($accid == $prodrow["memtype"]) { echo($accname); }
}

}

?>
            </td>
            
            
            <td>
<?

if ($prodrow["subtype"] == 1) {
echo("Monthly Subscription");
} else {
echo("One Time");
}
?>
            </td>
            
            
            <td nowrap="nowrap"><?=$prodrow["_2co_id"];?></td>
            
            <td><input name="visible" type="checkbox" id="visible" value="1"<? if ($prodrow["visible"]==1) { echo(" checked=\"checked\""); } ?> disabled /></td>
            
            <td nowrap="nowrap"><a href="javascript:editCreditSale(<?=$prodrow["productid"];?>);"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
            <td nowrap="nowrap"><a href="admin.php?f=scrds&pd=<?=$prodrow["productid"];?>"><img src="../images/del.png" alt="Delete Product" width="16" height="16" border="0" /></a></td>
            <td nowrap="nowrap"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>Generate product purchase<br />
              button code for placement<br />
              in your member area.</span></a></td>
            <td nowrap="nowrap"><input name="Button" type="button" class="formfield" id="Submit" value="Get Code" onClick="javascript:getCode('<?=$prodrow["itemid"];?>');" /></td>
          </tr>
		  <? } ?>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
             <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap"><input name="price" type="text" id="price" value="0.00" size="4" /></td>
            <td nowrap="nowrap"><input name="credits" type="text" id="credits" size="5" /></td>
            
<td>
            <?

echo("<select name=memtype><option value=0>All Members</option>");

$getaccounts = mysql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < mysql_num_rows($getaccounts); $j++) {
$accid = mysql_result($getaccounts, $j, "mtid");
$accname = mysql_result($getaccounts, $j, "accname");
echo("<option value=$accid>$accname</option>");
}
echo("</select>");

?>
</td>

            <td>
<?

echo("<select name=subtype>
<option value=0>One Time</option>
<option value=1>Monthly Subscription</option>
</select>");

?>
            </td>
            
            <td nowrap="nowrap"><input name="_2co_id" type="text" id="_2co_id" size="6" /></td>
            <td><input name="visible" type="checkbox" id="visible" value="1" checked="checked"/></td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap">&nbsp;</td>
            <td nowrap="nowrap"><a class="info" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /> <span>Add new credit<br />
              packages for your<br />
              One Time Offer<br />
              or member area.</span></a></td>
            <td nowrap="nowrap"><input name="Submit" type="submit" class="formfield" id="Submit" value="Add Product" /></td>
          </tr>
</table>
</form>
<br />
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  
  <tr>
    <td align="left" valign="top" bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The sell credits section is where you define the names, ID's and prices of credits that you will be selling either through your One Time Offer(s) or in your member area. These credits are available in the OTO settings as soon as they are added. To add a product to your member and/or content pages, simply click the 'Get Code' button and copy/paste the relevant HTML code into the HTML code of the page you want to sell from. </font></p>

      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
