<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.10
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

?>
<?
// Insert the message details into the emailmsg table and then retrieve the msgid
$mqry="INSERT INTO ".$prefix."emailmsg(status,subject,body,msgtype) VALUES (0,'".$_POST["emailsubject"]."','".$_POST["emailmessage"]."',1)";
@mysql_query($mqry) or die(mysql_error());

$ires=@mysql_query("SELECT max(id) as maxid FROM ".$prefix."emailmsg");
$irow=@mysql_fetch_array($ires);
$msgid=$irow["maxid"];

// If the email message is to be sent to a selected
// region, these members email addresses will be
// queued into the emailrcpt table.

	// Insert email addresses into emailrcpt for this message
	$mtid=trim($_POST["mtid"]);
	$rres=@mysql_query("SELECT email FROM ".$prefix."members WHERE newsletter=1 AND mtype='$mtid'");
	while($rrow=@mysql_fetch_array($rres))
	{
		$uqry="INSERT INTO ".$prefix."emailrcpt(address,msgid,status) VALUES('".$rrow["email"]."',$msgid,0)";
		mysql_query($uqry) or die(mysql_error());
	}

	// Now send the email in the background
	// path to admin dir without trailing slash
	if(isset($_SERVER['PATH_TRANSLATED']))
	{
		$fullpath = $_SERVER['PATH_TRANSLATED'];
	}
	else
	{
		$fullpath = $_SERVER['SCRIPT_FILENAME'];
	}

	// Find out if background processing is enabled
	$bres=@mysql_query("SELECT bgemail FROM ".$prefix."settings") or die(mysql_error());
	$brow=@mysql_fetch_array($bres);
	$bgemail=$brow["bgemail"];
	if($bgemail == 0)
	{
		AddLog("Running mailq.php in foreground");
		include "mailq.php";
	}
	else
	{
		$mail_path=substr($fullpath,0,strrpos($fullpath, "/"));
		$mailq=$mail_path."/mailq.php";
		// Path to PHP
		$php_path = exec( 'which php');
		$mailcmd = $php_path." -f ".$mailq;
		AddLog("Running ".$mailcmd." in background");
		$handle=popen($mailcmd,"r");
		AddLog("Popen handle: ".$read);
		pclose($handle);
	}
?>
<p><strong><?=$msg;?></strong></p>
<p align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Email Message Has Been Sent</strong></font></p>
<p align="center"><strong><font face="Verdana, Arial, Helvetica, sans-serif"><br />
    <font color="#FF0000">Please Do Not Refresh This Page As<br />
    This May Cause The Email Message</font></font><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif"><br />
  To Be Sent Again!</font></strong></p>
