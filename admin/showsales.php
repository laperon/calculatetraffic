<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "../inc/filter.php";

	// Prevent anyone who isn't logged in from opening this page
	include "../inc/checkauth.php"; 
	if(!isset($_SESSION["adminid"])) { exit; };

	include "../inc/funcs.php";

	$memberid=$_GET["mid"];
	$mqry="SELECT firstname,lastname FROM ".$prefix."members WHERE Id=".$memberid;
	$mres=@mysql_query($mqry);
	$mrow=@mysql_fetch_array($mres); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
 <div align="center"><font size="3" face="Verdana, Arial, Helvetica, sans-serif"><strong>Commissions Earned By 
  <?=$mrow["firstname"]." ".$mrow["lastname"];?>
 </strong> </font> </div>
 <br />
 <table align="center">
 <tr>
   <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Date</font></strong></td>
   <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product ID </font></strong></td>
   <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Product Name </font></strong></td>
   <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Price</font></strong></td>
   <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Commission</font></strong></td>
   <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Status</font></strong></td>
 </tr>
<?
	// Get sales for this member
	$sres=@mysql_query("SELECT * FROM ".$prefix."sales WHERE affid=$memberid");
	if(mysql_num_rows($sres) > 0)
	{
		while($srow=@mysql_fetch_array($sres))
		{
			if($srow["status"] == "P")
			{
				$status="Paid";
			}
			else
			{
				$status="Unpaid";
			}
?> 
 <tr>
 	<td align="center" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
 	  <?=$srow["saledate"];?>
 	</font></td>
    <td align="center" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
      <?=$srow["itemid"];?>
    </font></td>
    <td align="center" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
      <?=$srow["itemname"];?>
    </font></td>
    <td align="center" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
      <?=$srow["itemamount"];?>
    </font></td>
    <td align="center" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
      <?=$srow["commission"];?>
    </font></td>
    <td align="center" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
      <?=$status;?>
    </font></td>
 </tr>
<?
}
}
?>
 </table>
</body>
</html>
