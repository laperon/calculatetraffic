<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.02
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "../inc/filter.php";

	// Prevent anyone who isn't logged in from opening this page
	include "../inc/checkauth.php"; 
	if(!isset($_SESSION["adminid"])) { exit; };

	// Update the signupform table
	if($_POST["Submit"] == "Update Signup Settings")
	{
		@mysql_query("TRUNCATE TABLE ".$prefix."signupform");
		foreach($_POST as $key => $value)
		{
			if($key != "Submit") {			
				if(substr($key,strlen($key)-1,1) == "s") { $fieldname="fieldsize"; }
				if(substr($key,strlen($key)-1,1) == "e") { $fieldname="enable"; }
				if(substr($key,strlen($key)-1,1) == "r") { $fieldname="require"; }
				$field=substr($key,0,strlen($key)-1);

				// Check if the field exists .. insert if it's not there (unless its the checkbox text)
    	        $fcres=mysql_query($qry="SELECT COUNT(*) AS fcount FROM ".$prefix."signupform WHERE fieldname='$field'");
				$fcount=mysql_result($fcres,0,"fcount");
				
				if($fcount == 0 && $field != "signup_checkbox_tex")
				{
					$qry="INSERT INTO ".$prefix."signupform(`fieldname`,`fieldsize`,`enable`,`require`) VALUES('$field',0,0,0)";
					mysql_query($qry);
				}
			
    	        $qry="UPDATE ".$prefix."signupform SET `$fieldname`=$value WHERE fieldname='$field'";
        	    mysql_query($qry);
			}
		}
	//update the checkbox text
	$qry="UPDATE ".$prefix."settings SET `signup_checkbox_text`= '".$_POST[signup_checkbox_text]."' WHERE id='1'";
        	    mysql_query($qry);
	}
	// Get signup field settings
	$ssres=@mysql_query("SELECT * FROM ".$prefix."signupform");
?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><p>&nbsp;</p>
    </td>
  </tr>

  <tr>
    <td>

	<form method="post" action="admin.php?f=sgs">
	<table width="300" align="center" cellpadding="4" cellspacing="0" style="border: 1px solid #999;">
      <?
      // Update system settings
      if($_POST["Submit"] == "Update Signup Settings")
      { ?>
        <tr>
          <td align="center">
            <font color="red"><strong><?=$msg;?></strong></font>          </td>
        </tr>
    <?
      }    
    ?>
    <tr>
        <td nowrap="nowrap" class="admintd"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Field Settings</strong></font></td>
        </tr>
      
      <tr>
        <td align="center" nowrap="nowrap"><table width="100%" border="0" cellspacing="0" cellpadding="4">
          <tr bgcolor="#D6E3FE">
            <td nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Field Name</font></strong></td>
            <td align="center"><strong>Size</strong></td>
            <td align="center"><strong>Enabled</strong></td>
            <td align="center"><strong>Require</strong></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE">&nbsp;</td>
            <td align="center">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">First Name:</font></strong></td>
            <td align="center"><input name="firstnames" type="text" class="formfield" id="firstnames" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="firstnamee" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="firstnamee" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table>              </td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="firstnamer" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="firstnamer" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table>              </td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Last Name: </font></strong></td>
            <td align="center"><input name="lastnames" type="text" class="formfield" id="lastnames" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="lastnamee" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="lastnamee" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="lastnamer" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="lastnamer" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Address:</font></strong></td>
            <td align="center"><input name="addresss" type="text" class="formfield" id="addresss" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="addresse" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="addresse" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="addressr" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="addressr" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">City:</font></strong></td>
            <td align="center"><input name="citys" type="text" class="formfield" id="citys" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="citye" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="citye" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="cityr" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="cityr" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">State:</font></strong></td>
            <td align="center"><input name="states" type="text" class="formfield" id="states" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="statee" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="statee" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="stater" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="stater" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Zip:</font></strong></td>
            <td align="center"><input name="zips" type="text" class="formfield" id="zips" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="zipe" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="zipe" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="zipr" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="zipr" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Country:</font></strong></td>
            <td align="center">N/A</td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="countrye" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="countrye" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="countryr" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="countryr" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Telephone:</font></strong></td>
            <td align="center"><input name="telephones" type="text" class="formfield" id="telephones" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="telephonee" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="telephonee" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="telephoner" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="telephoner" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">PayPal Email: </font></strong></td>
            <td align="center"><input name="paypals" type="text" class="formfield" id="paypals" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="paypale" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="paypale" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="paypalr" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="paypalr" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            </tr>
<?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Geo Region: </font></strong></td>
            <td align="center">N/A</td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="geoe" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="geoe" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center">Y</td>
                <td align="center">N</td>
              </tr>
              <tr>
                <td align="center"><input name="geor" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                <td align="center"><input name="geor" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
              </tr>
            </table></td>
          </tr>
  <?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>        
          
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Clickbank ID: </font></strong></td>
            <td align="center"><input name="clickbanks" type="text" class="formfield" id="clickbanks" value="<?=$ssrow["fieldsize"];?>" size="2" /></td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center">Y</td>
                  <td align="center">N</td>
                </tr>
                <tr>
                  <td align="center"><input name="clickbanke" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                  <td align="center"><input name="clickbanke" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
                </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center">Y</td>
                  <td align="center">N</td>
                </tr>
                <tr>
                  <td align="center"><input name="clickbankr" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                  <td align="center"><input name="clickbankr" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
                </tr>
                
            </table></td>
          </tr> 
          
 <?
            // Get the next field
            $ssrow=@mysql_fetch_array($ssres);
?>         
          
          <tr>
            <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Check Box: </font></strong></td>
            <td align="center">N/A</td>
            <td align="center" nowrap="nowrap" bgcolor="#D6E3FE"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center">Y</td>
                  <td align="center">N</td>
                </tr>
                <tr>
                  <td align="center"><input name="checkboxe" type="radio" value="1" <? if($ssrow["enable"] == 1) { echo "checked=checked"; } ?> /></td>
                  <td align="center"><input name="checkboxe" type="radio" value="0" <? if($ssrow["enable"] == 0) { echo "checked=checked"; } ?> /></td>
                </tr>
            </table></td>
            <td align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center">Y</td>
                  <td align="center">N</td>
                </tr>
                <tr>
                  <td align="center"><input name="checkboxr" type="radio" value="1" <? if($ssrow["require"] == 1) { echo "checked=checked"; } ?> /></td>
                  <td align="center"><input name="checkboxr" type="radio" value="0" <? if($ssrow["require"] == 0) { echo "checked=checked"; } ?> /></td>
                </tr>
                
            </table></td>
          </tr> 
        <?
		//get the checkbox text
		$settings=@mysql_query("SELECT * FROM ".$prefix."settings");
		$text_settings=@mysql_fetch_array($settings);

		?>
        <tr>
             <td align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Check Box Text: </font></strong></td>
            <td align="center" colspan="6" ><input type="text" name="signup_checkbox_text" value="<?=$text_settings[signup_checkbox_text]; ?>" /></td>
          </tr>
      
      
      
      
      <tr>
        <td colspan="6" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="formfield" value="Update Signup Settings" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>
<br />
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  <tr>
    <td bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
        <ul>
          <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Using these controls you can set which entry fields will be displayed on your signup.php page as well as which of those fields will prompt the member to fill in a value if they leave it blank. The 'Enabled' column causes the field to be displayed (Y) or hidden (N) and the 'Require' column determines whether the person filling the form will be required to fill in the value (Y) or whether they can leave the field blank (N).</font></li>
        </ul>
        <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
