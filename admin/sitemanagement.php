<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

//Check for approval link
if (is_numeric($_GET['approve'])) {
	@lfmsql_query("Update ".$prefix."msites set state=1 WHERE id=".$_GET['approve']." limit 1");
}

//Get Show Type From TE Dashboard

if ($_GET['show'] == "rotsites") {
	$showtype = "rotsites";
	$timequery = "state=1 and credits >= 1";

} else {
	$showtype = "";
	$timequery = "";
}

if ($_POST["searchtext"] != "") {
            $searchfield=trim($_POST["searchfield"]);
            $searchtext=trim($_POST["searchtext"]);
            
} elseif (($_GET['searchfield'] != "") && ($_GET['searchtext'] != "")) {
            $searchfield=trim($_GET["searchfield"]);
            $searchtext=trim($_GET["searchtext"]);
}

//LFMTE Sorting

if (($_GET["sortby"] != "") && ($_GET["sortorder"] != "")) {
$sortby = $_GET["sortby"];
$sortorder = $_GET["sortorder"];

//Set Orders
if ($sortby == "id" && $sortorder == "ASC") {
$idorder = "DESC";
} else {
$idorder = "ASC";
}
if ($sortby == "memid" && $sortorder == "ASC") {
$memorder = "DESC";
} else {
$memorder = "ASC";
}
if ($sortby == "url" && $sortorder == "ASC") {
$urlorder = "DESC";
} else {
$urlorder = "ASC";
}
if ($sortby == "credits" && $sortorder == "DESC") {
$creditsorder = "ASC";
} else {
$creditsorder = "DESC";
}
if ($sortby == "hits" && $sortorder == "DESC") {
$hitsorder = "ASC";
} else {
$hitsorder = "DESC";
}

} else {
$sortby = "id";
$sortorder = "ASC";

$idorder = "ASC";
$memorder = "ASC";
$urlorder = "ASC";
$creditsorder = "DESC";
$hitsorder = "DESC";
}

//End LFMTE Sorting

// Operations affecting site records

// Suspend sites according to check boxes
if($_POST["Submit"] == "Suspend Selected")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$sqry="UPDATE ".$prefix."msites SET state=3 WHERE id=".$val;
		@lfmsql_query($sqry) or die("Error: Unable to suspend multiple sites!");
		}
	}
}

// Pause sites according to check boxes
if($_POST["Submit"] == "Pause Selected")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$sqry="UPDATE ".$prefix."msites SET state=2 WHERE id=".$val;
		@lfmsql_query($sqry) or die("Error: Unable to pause multiple sites!");
		}
	}
}

// Un-Suspend sites according to check boxes
if($_POST["Submit"] == "Enable Selected")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$uqry="UPDATE ".$prefix."msites SET state=1 WHERE id=".$val;
		@lfmsql_query($uqry) or die("Error: Unable to un-suspend multiple sites!");
		}
	}
}

// Delete multiple sites according to check boxes
if($_POST["MDelete"] == "Yes - Delete")
{
	if(isset($_POST["idarray"]))
	{
		while (list ($key,$val) = @each ($_POST["idarray"])) {
		$dqry="DELETE FROM ".$prefix."msites WHERE id=".$val;
		lfmsql_query($dqry) or die("Error: Unable to delete sites!");
		}
	}
}

// Ban sites according to check boxes
if($_POST["Submit"] == "Ban Selected URLs")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$geturl = lfmsql_query("SELECT url from ".$prefix."msites WHERE id=".$val);
		$urltoban = lfmsql_result($geturl, 0, "url");
		$checkban = lfmsql_query("Select id from `banned_sites` where domain='$urltoban' limit 1");
		if (lfmsql_num_rows($checkban) == 0) {
		$sqry = "Insert into `banned_sites` (domain) values ('$urltoban')";
		@lfmsql_query($sqry) or die("Error: Unable to ban multiple sites!");
		@lfmsql_query("Update ".$prefix."msites set state=3 where url='$urltoban'");
		}
		}
	}
}

// Ban domains according to check boxes
if($_POST["Submit"] == "Ban Selected Domains")
{
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$geturl = lfmsql_query("SELECT url from ".$prefix."msites WHERE id=".$val);
		$urltoban = lfmsql_result($geturl, 0, "url");
		if ($urltoban != "") {
			$splittoban = explode("/", $urltoban);
			$urltoban = $splittoban[2];
			$urltoban = ereg_replace("www.", "", $urltoban);
		}
		$checkban = lfmsql_query("Select id from `banned_sites` where domain='$urltoban' limit 1");
		if (lfmsql_num_rows($checkban) == 0) {
		$sqry = "Insert into `banned_sites` (domain) values ('$urltoban')";
		@lfmsql_query($sqry) or die("Error: Unable to ban multiple domains!");
		@lfmsql_query("Update ".$prefix."msites set state=3 where url LIKE '%$urltoban%'");
		}
		}
	}
}

?>
<script src="../inc/jsfuncs.js" type="text/javascript"></script>
<?
	// Hide the search form when displaying mass delete confirmation
	if(!isset($_POST["Delchecked"]))
	{

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>

<?
if ($timequery == "") {
?>

<center>
<p><font size=3><b>Show:</b>

<a href="admin.php?f=smm&show=<?=$showtype?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse"><? if ($searchfield!="state") { echo("<b>All</b>"); } else { echo("All"); } ?></a> | 

<a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=state&searchtext=1&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse"><? if ($searchfield=="state" && $searchtext==1) { echo("<b>Enabled</b>"); } else { echo("Enabled"); } ?></a> | 

<a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=state&searchtext=2&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse"><? if ($searchfield=="state" && $searchtext==2) { echo("<b>Paused</b>"); } else { echo("Paused"); } ?></a> | 

<a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=state&searchtext=3&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse"><? if ($searchfield=="state" && $searchtext==3) { echo("<b>Suspended</b>"); } else { echo("Suspended"); } ?></a> | 

<a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=state&searchtext=0&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse"><? if ($searchfield=="state" && $searchtext==0) { echo("<b>Pending Approval</b>"); } else { echo("Pending Approval"); } ?></a>

</p>
</center>

<?
}
?>

<center>

<!-- Start Search Box -->
<table border="0" cellpadding="0" cellspacing="1" width="500">
<form name="searchfrm" method="post" action="admin.php?f=smm&show=<?=$showtype?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<tr>
<td><select class="form-control" name="searchfield">
      <option <? if($searchfield=="" || $searchfield=="state") { echo "selected=\"selected\""; } ?>>Search By...</option>
      <option <? if($searchfield=="id") { echo "selected=\"selected\""; } ?> value="id">Site ID</option>
      <option <? if($searchfield=="memid") { echo "selected=\"selected\""; } ?> value="memid">Member ID</option>
      <option <? if($searchfield=="memusername") { echo "selected=\"selected\""; } ?> value="memusername">Member Username or Email</option>
      <option <? if($searchfield=="url") { echo "selected=\"selected\""; } ?> value="url">URL</option>
                </select></td>
    <td><input class="form-control" name="searchtext" type="text" id="searchtext" value="<? if ($searchfield != "state") { echo("$searchtext"); } ?>"/></td>
    <td><input type="submit" name="Submit" value="Search" /></td>
</tr>
</form>
</table>
<br>
<!-- End Search Box -->

<?
}
	// Get the starting record for site browse
	if(!isset($_GET["limitStart"]))
	{
		$st=0;
	}
	else
	{
		$st=$_GET["limitStart"];
	}

	// Get the total site count for site browse
	$cqry="SELECT COUNT(*) as mcount FROM ".$prefix."msites";
	// Check if there is search criteria
	if($searchtext != "")
	{

		// Handle member username
		if($searchfield == "memusername") {
			$getuserid = lfmsql_query("Select Id from ".$prefix."members where username='".$searchtext."' or email='".$searchtext."'");
			if (lfmsql_num_rows($getuserid) > 0) {
				$searchtext = lfmsql_result($getuserid, 0, "Id");
				$searchfield = "memid";
			}		
		}
		
		//Handle URL search
		if ($searchfield == "url") {
			$searchtext = "%".$searchtext."%";
		}
		
		if ($timequery != "") {
			$cqry.=" WHERE ".$searchfield." = '".$searchtext."' and ".$timequery;
		} else {
			$cqry.=" WHERE ".$searchfield." = '".$searchtext."'";
		}
	
	} else {
		if ($timequery != "") {
			$cqry.=" WHERE ".$timequery;
		}
	}

    $cres=@lfmsql_query($cqry);
    $crow=@lfmsql_fetch_array($cres);

    // Get the first/next 40 records
    $mqry="SELECT * FROM ".$prefix."msites WHERE id != 0";

    // Add search criteria if applicable
    if($searchtext != "") {
    
    		if ($timequery != "") {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' AND ".$timequery;
		} else {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."'";
		}
		
		$searchtext = str_replace("%", "", $searchtext);

    }
    else
    {
    		if ($timequery != "") {
			$mqry.=" AND ".$timequery;
		}
    }
    
    $mqry.=" ORDER BY ".$sortby." ".$sortorder." LIMIT $st,40";

    $mres=@lfmsql_query($mqry);


	//
	// Site browsing
	// This is where the search and browse records are diaplayed
	//
	if($_GET["sf"] == "browse" && !isset($_POST["Delchecked"]))
	{
?>
<form name="mbrowse" id="mbrowse" method="post" action="admin.php?f=smm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      <tr>
        <td colspan="13" align="left" valign="bottom">
		<div align="left"><?
    		if(!isset($_GET["limitStart"])){$limitStart = 0;}
    		pageNav($crow["mcount"],$_GET["limitStart"],40,"smm&show=$showtype&searchfield=$searchfield&searchtext=$searchtext&sortby=$sortby&sortorder=$sortorder");
		?></div>
		
		<? echo("<input name=\"Add New Site\" type=\"button\" class=\"lfmtable\" style=\"height: 8; font-size:15px\" onClick=\"javascript:addSite()\" value=\"Add New Site\" />"); ?>
		
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Enable Selected" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Pause Selected" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Suspend Selected" />
		<input name="Delchecked" type="submit" class="lfmtable" id="Delchecked" style="height: 8; font-size:15px" value="Delete Selected" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Ban Selected URLs" />
		<input name="Submit" type="submit" class="lfmtable" style="height: 8; font-size:15px" value="Ban Selected Domains" />
		
		</td>
        </tr>
        
      <tr class="admintd">
        <td width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=id&sortorder=<?=$idorder?>&sf=browse">ID</a></font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=memid&sortorder=<?=$memorder?>&sf=browse">Member ID</a></font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=url&sortorder=<?=$urlorder?>&sf=browse">URL</a></font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=credits&sortorder=<?=$creditsorder?>&sf=browse">Hits Assigned</a></font></strong></td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="admin.php?f=smm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=hits&sortorder=<?=$hitsorder?>&sf=browse">Hits Received</a></font></strong> </td>
        
        <?
        echo("<td align=\"center\" nowrap=\"NOWRAP\" background=\"images/lfmtablethbg.jpg\"><strong>
          <font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Status</font></strong></td>");
        ?>
          
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg">&nbsp;</td>
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg">&nbsp;</td>
        <td width="16" align="center" nowrap="NOWRAP"><input type="checkbox" name="checkbox" value="checkbox" onClick="javascript:checkAll(document.getElementById('mbrowse'),'mcheck');" /></td>
      </tr>
<?
$getrotation = lfmsql_query("Select rotationtype from ".$prefix."settings");
$rotationtype = lfmsql_result($getrotation, 0, "rotationtype");

	while($mrow=@lfmsql_fetch_array($mres))
	{
	
	$activemember = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."members WHERE Id=".$mrow["memid"]." AND status='Active' AND actsite=1"), 0);
	
	if ($showtype != "rotsites" || $rotationtype != 0 || $activemember > 0) {
		
		if ($mrow["state"] == 0) {
			$showstate = "Pending Approval<br><a target=\"_self\" href=\"admin.php?f=smm&show=".$showtype."&searchfield=".$searchfield."&searchtext=".$searchtext."&sortby=".$sortby."&sortorder=".$sortorder."&sf=browse&approve=".$mrow["id"]."\">Approve Site</a>";
		} elseif ($mrow["state"] == 1) {
			$showstate = "Enabled";
		} elseif ($mrow["state"] == 2) {
			$showstate = "Paused";
		} elseif ($mrow["state"] == 3) {
			$showstate = "Suspended";
		}

		if($bgcolor == "#FFFFFF")
		{
			$bgcolor="#DDDDDD";
		}
		else
		{
			$bgcolor="#FFFFFF";
		}
		
		$mrow["credits"] = floor($mrow["credits"]);
?>
      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
        <td><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["id"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a target="_self" href="admin.php?f=mm&searchfield=Id&searchtext=<?=$mrow["memid"];?>&sf=browse"><?=$mrow["memid"];?></a>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <a target="_blank" href="/adminchecksite.php?siteid=<?=$mrow["id"];?>"><?=$mrow["url"];?></a>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["credits"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$mrow["hits"];?>
        </font></td>
        <td align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
          <?=$showstate;?>
        </font></td>

        <td width="24" align="center" bgcolor="<?=$bgcolor;?>"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:delSite(<?=$mrow["id"];?>)"><img src="../images/del.png" alt="Delete Site" width="16" height="16" border="0" /></a></font></td>
        <td width="24" align="center"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
		<a href="javascript:editSite(<?=$mrow["id"];?>)"><img src="../images/edit.png" alt="Edit Site" width="16" height="16" border="0" /></a></font></td>
        <td align="center"><input type="checkbox" class="mcheck" name="memcheck[]" value="<?=$mrow["id"];?>" onclick="HighlightRowIfChecked(this);" />
        </td>
      </tr>
<?
}
}
?>
    </table>	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>
</form>
<?
}
//
// End of site browsing/searching section
//

// Delete Confirmation section
// This area is for display of details
// relating to multiple site deletion
if($_POST["Delchecked"] == "Delete Selected")
{
	if(!isset($_POST["memcheck"]))
	{
?>
<center>
  <font face="Verdana, Arial, Helvetica, sans-serif">NO RECORDS SELECTED</font>
</center>
<?
		exit;
	}
?>
<br /><br />
<center><strong><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif">You Are About To Delete The Following Sites</font></strong></center>
<br /><br />
<form method="POST" action="admin.php?f=smm&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>&sf=browse">
<table align="center" cellpadding="2" cellspacing="0" class="lfmtable">
<tr class="admintd">
<td><strong>ID</strong></td>
<td align="center"><strong>URL</strong></td>
</tr>
<?
	if(isset($_POST["memcheck"]))
	{
		while (list ($key,$val) = @each ($_POST["memcheck"])) {
		$dqry="SELECT * FROM ".$prefix."msites WHERE id=".$val;
		$dres=@lfmsql_query($dqry);
		$drow=@lfmsql_fetch_array($dres);
?>
<tr>
<td><?=$drow["id"];?></td><td align="center"><?=$drow["url"];?><input type="hidden" name="idarray[]" value="<?=$drow["id"];?>" /></td>
</tr>
<?
		}
	}

?>
<tr>
  <td colspan="2" align="center">&nbsp;</td>
</tr>
<tr><td colspan="2" align="center"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>Are You Sure?</strong></font></td>
</tr>
<tr>
  <td colspan="2" align="center" nowrap="nowrap"><input name="Submit" type="submit" id="Submit" value="No - Cancel" />
    <input name="MDelete" type="submit" id="MDelete" value="Yes - Delete" /></td>
</tr>
</table>
</form>
<?
}

?>