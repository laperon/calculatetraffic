<?php

// LFMTE Social Surfbar
// �2012 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

$errormess = "";

echo("<center><br><br>");

//Update Settings
if ($_GET['updatesettings'] == "yes") {
	
	if (isset($_POST['enable_digg'])) {
		$enable_digg = 1;
	} else {
		$enable_digg = 0;
	}
	@mysql_query("Update `socialsurfbar_footer` set value='".$enable_digg."' where field='enable_digg'") or die(mysql_error());
	
	if (isset($_POST['enable_stumbleupon'])) {
		$enable_stumbleupon = 1;
	} else {
		$enable_stumbleupon = 0;
	}
	@mysql_query("Update `socialsurfbar_footer` set value='".$enable_stumbleupon."' where field='enable_stumbleupon'") or die(mysql_error());
	
	if (isset($_POST['enable_facebook'])) {
		$enable_facebook = 1;
	} else {
		$enable_facebook = 0;
	}
	@mysql_query("Update `socialsurfbar_footer` set value='".$enable_facebook."' where field='enable_facebook'") or die(mysql_error());
	
	if (isset($_POST['enable_twitter'])) {
		$enable_twitter = 1;
	} else {
		$enable_twitter = 0;
	}
	@mysql_query("Update `socialsurfbar_footer` set value='".$enable_twitter."' where field='enable_twitter'") or die(mysql_error());
	
}

//Begin Main Page

if ($errormess != "") {
	echo("<p><b>".$errormess."</b></p>");
}

$enable_digg = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_digg'"), 0);
$enable_stumbleupon = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_stumbleupon'"), 0);
$enable_facebook = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_facebook'"), 0);
$enable_twitter = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_twitter'"), 0);

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td>
	<form action="admin.php?f=socialsb&updatesettings=yes" method="post">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Enable Digg</strong></font></td>
        <td colspan="2" class="formfield"><input name="enable_digg" type="checkbox" <? if($enable_digg == 1) { echo(" checked"); } ?>></td>
        <td align="center"><a href="#" title="Check to include Digg in the surfbar footer.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Enable Stumbleupon</strong></font></td>
        <td colspan="2" class="formfield"><input name="enable_stumbleupon" type="checkbox" <? if($enable_stumbleupon == 1) { echo(" checked"); } ?>></td>
        <td align="center"><a href="#" title="Check to include Stumbleupon in the surfbar footer.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Enable Facebook</strong></font></td>
        <td colspan="2" class="formfield"><input name="enable_facebook" type="checkbox" <? if($enable_facebook == 1) { echo(" checked"); } ?>></td>
        <td align="center"><a href="#" title="Check to include Facebook in the surfbar footer.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Enable Twitter</strong></font></td>
        <td colspan="2" class="formfield"><input name="enable_twitter" type="checkbox" <? if($enable_twitter == 1) { echo(" checked"); } ?>></td>
        <td align="center"><a href="#" title="Check to include Twitter in the surfbar footer.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
     <tr>
        <td align="left" nowrap="nowrap" class="button"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Update Settings" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>

</center>

<p>&nbsp;</p>