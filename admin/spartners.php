<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

	// Prevent anyone who isn't logged in from opening this page
	if(!isset($_SESSION["adminid"])) { exit; };

	$msg="";

// Add a page entry
if($_GET["add"] == "yes")
{
		
		$name = $_POST["name"];
		$domain = $_POST["domain"];
		$descr = $_POST["descr"];
		$referid = $_POST["referid"];
		
		$checklastrow = lfmsql_query("Select rank from ".$prefix."spartners order by rank desc");
		if (lfmsql_num_rows($checklastrow) > 0) {
		$lastrow = lfmsql_result($checklastrow, 0, "rank");
		} else {
		$lastrow = 0;
		}
		$newrank = $lastrow+1;
		
		if(isset($_POST["visible"])) { $visible=1; } else { $visible=0; }
		
	$qry="INSERT INTO ".$prefix."spartners(name,domain,descr,referid,rank) VALUES('$name','$domain','$descr',$referid,$newrank)";
	lfmsql_query($qry);
}

//Change A rank
$rank = $_GET['rank'];
if ($rank == "up" || $rank == "down") {

$runchange = "yes";
$id = $_GET['productsid'];

$checkfirstrow = lfmsql_query("Select rank from ".$prefix."spartners order by rank asc");
$firstrow = lfmsql_result($checkfirstrow, 0, "rank");

$checklastrow = lfmsql_query("Select rank from ".$prefix."spartners order by rank desc");
$lastrow = lfmsql_result($checklastrow, 0, "rank");

if (($rank=="up") && ($id > $firstrow)) {
$change=$id-1;
}
elseif (($rank=="down") && ($id < $lastrow)) {
$change=$id+1;
}
else {
$runchange = "no";
}

if ($runchange == "yes") {
	@lfmsql_query("Update ".$prefix."spartners set rank=0 where rank=$id");
	@lfmsql_query("Update ".$prefix."spartners set rank=$id where rank=$change");
	@lfmsql_query("Update ".$prefix."spartners set rank=$change where rank=0");
	@lfmsql_query("ALTER TABLE ".$prefix."spartners ORDER BY rank;");
}

}

// Delete a bonus page entry
if(isset($_GET["pd"]))
{
	lfmsql_query("DELETE FROM ".$prefix."spartners WHERE id=".$_GET["pd"]);
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<p>&nbsp;</p>
<p align="center">&nbsp;</p>
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0">
          <tr class="admintd">
            <td nowrap="NOWRAP"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Partner</font></strong></td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
            <td width="16" nowrap="nowrap">&nbsp;</td>
    </tr>
		  <?
		  	// List partners
			$prodres=@lfmsql_query("SELECT * FROM ".$prefix."spartners ORDER BY rank ASC");
			while($prodrow=@lfmsql_fetch_array($prodres))
			{ ?>
          <tr>
            <td nowrap="nowrap"><a href=admin.php?f=spartners&rank=up&productsid=<?=$prodrow["rank"];?>>Move Up</a><br><a href=admin.php?f=spartners&rank=down&productsid=<?=$prodrow["rank"];?>>Move Down</a></td>
            <td nowrap="nowrap"><?=$prodrow["domain"];?></td>
            
            <td nowrap="nowrap"><a href="javascript:editSpartner(<?=$prodrow["id"];?>);"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>
            <td nowrap="nowrap"><a href="admin.php?f=spartners&pd=<?=$prodrow["id"];?>"><img src="../images/del.png" alt="Delete Partner" width="16" height="16" border="0" /></a></td>

          </tr>
		  <? } ?>
</table>

<center>
<br><br>

<table border="0" cellpadding="0" cellspacing="0" width="700">
<tr><td align="left">
<font size="2" face="Verdana, Arial, Helvetica, sans-serif">You can add other LFMTE exchanges as signup partners, and they will be shown on your Thank You page when members signup at your exchange. Members can then join those exchanges with one click, and they will be added as your referrals.<br /><br />One way to use this feature is to partner with other LFMTE owners, and add each other as signup partners.</font>
</td></tr>
</table>

<br><br>

<?

echo("<table border=1 cellpadding=5 cellspacing=0>
<tr><td colspan=\"4\" align=\"center\" class=\"admintd\"><strong><font size=\"3\" face=\"Verdana, Arial, Helvetica, sans-serif\">Add A New Partner</font></strong></td>
<tr><td align=center>

<table border=0 cellpadding=3 cellspacing=0>
<form method=post action=\"admin.php?f=spartners&add=yes\">
<tr><td align=right>Site Name: </td><td align=left><input type=text name=name value=\"\"></td></tr>
<tr><td align=right>Domain: <br>ex. hitsconnect.com</td><td align=left><input type=text name=domain value=\"\"></td></tr>
<tr><td align=right>Referral ID: </td><td align=left><input type=text name=referid value=\"\"></td></tr>
<tr><td align=right>Description: </td><td align=left><textarea name=\"descr\" wrap=\"soft\" rows=\"4\" cols=\"25\"></textarea></td></tr>
<tr><td align=center colspan=2><input type=submit value=\"Add\"></td></tr>
</form>
</table>

</td></tr>
</table>
");

?>


<p>&nbsp;</p>
