<?php

// Grouping One-Time-Offer Mod
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;

}
-->
</style>
<script type="text/javascript" src="../inc/ajax.js"></script>
<script type="text/javascript">
var ajax = new sack();
</script>
<script type="text/javascript" src="../inc/ajaxfuncs.js"></script>
<script language="javascript" type="text/javascript" src="tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
 theme : "advanced",
    mode: "exact",
    relative_urls : false,
    elements : "oto,otofooter",
    theme_advanced_toolbar_location : "top",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
    + "bullist,numlist,outdent,indent",
    theme_advanced_buttons2 : "fontselect,fontsizeselect,forecolor,backcolor,link,unlink,anchor,image,separator,"
    +"undo,redo,cleanup,code,separator,sub,sup,charmap",
    theme_advanced_buttons3 : "",
    apply_source_formatting : true,
    cleanup : false,
    cleanup_on_startup : false,
    height:"480px",
    width:"760px"
  });

  function toggleEditor(id) {
	var elm = document.getElementById(id);

	if (tinyMCE.getInstanceById(id) == null)
		tinyMCE.execCommand('mceAddControl', false, id);
	else
		tinyMCE.execCommand('mceRemoveControl', false, id);
}
</script><br><br><center>
<?

$add = $_GET['add'];
$update = $_GET['update'];
$reset = $_GET['reset'];
$delete = $_GET['delete'];
$rank = $_GET['rank'];
$pause = $_GET['pause'];
$unpause = $_GET['unpause'];

//Update An Offer
if ($update == "yes") {
$id = $_GET['spoid'];
$offername = $_POST['offername'];
$ipn = $_POST['ipn'];
$acctype = $_POST['acctype'];
$reptype = $_POST['reptype'];
$mdays = $_POST['mdays'];
$text = $_POST['text'];

@lfmsql_query("Update ".$prefix."spo set offername='$offername', ipn='$ipn', acctype=$acctype, reptype=$reptype, mdays=$mdays, text='$text' where id=$id limit 1");
$resmessage = "Offer Settings Updated";
}

//Add An Offer
elseif ($add == "yes") {

$offername = $_POST['offername'];
$ipn = $_POST['ipn'];
$acctype = $_POST['acctype'];
$memtype = $_POST['memtype'];
$reptype = $_POST['reptype'];
$mdays = $_POST['mdays'];
$text = $_POST['text'];

$checklastrow = lfmsql_query("Select rank from ".$prefix."spo order by rank desc");
if (lfmsql_num_rows($checklastrow) > 0) {
$lastrow = lfmsql_result($checklastrow, 0, "rank");
} else {
$lastrow = 0;
}
$newrank = $lastrow+1;

@lfmsql_query("Insert into ".$prefix."spo (rank, offername, ipn, acctype, memtype, reptype, mdays, text) values ($newrank, '$offername', '$ipn', $acctype, $memtype, $reptype, $mdays, '$text')");
$newid = lfmsql_insert_id();

//Only all user 0 memtypes use spostats
if ($memtype == 1) {
$colname = "spo".$newid;
@lfmsql_query("ALTER TABLE `".$prefix."members` ADD `$colname` INT( 11 ) NULL DEFAULT '2000000000'");
@lfmsql_query("Update ".$prefix."members set $colname=0 where Id>0");
}
elseif ($memtype == 2) {
$colname = "spo".$newid;
@lfmsql_query("ALTER TABLE `".$prefix."members` ADD `$colname` INT( 11 ) NULL DEFAULT '0'");
@lfmsql_query("Update ".$prefix."members set $colname=2000000000 where Id>0");
}

$resmessage = "Offer Added";

}

//Reset Offer Stats
elseif ($reset == "yes") {
$confirmreset = $_GET['confirmreset'];
$id = $_GET['spoid'];

if ($confirmreset == "yes") {

$getdata = lfmsql_query("Select memtype from ".$prefix."spo where id=$id limit 1");
$memtype = lfmsql_result($getdata, 0, "memtype");
if ($memtype == 0) {
@lfmsql_query("Delete from ".$prefix."spostats where spoid=$id");
} else {
$colname = "spo".$id;
@lfmsql_query("Update ".$prefix."members set $colname=0 where Id>0");
}
$resmessage = "Offer Stats Reset";
} else{
echo("<b>Are you sure you want to reset the stats for this offer?</b><br><br><a href=admin.php?f=spo&reset=yes&spoid=$id&confirmreset=yes><b>Yes</b></a><br><br><a href=admin.php?f=spo><b>No</b></a>");
exit;
}

}

//Delete An Offer
elseif ($delete == "yes") {
$confirmdelete = $_GET['confirmdelete'];
$id = $_GET['spoid'];

if ($confirmdelete == "yes") {

$getdata = lfmsql_query("Select memtype from ".$prefix."spo where id=$id limit 1");
$memtype = lfmsql_result($getdata, 0, "memtype");
if ($memtype == 0) {
@lfmsql_query("Delete from ".$prefix."spostats where spoid=$id");
} else {
$colname = "spo".$id;
@lfmsql_query("ALTER TABLE `".$prefix."members` DROP `$colname`");
}
@lfmsql_query("Delete from ".$prefix."spo where id=$id limit 1");
$resmessage = "Offer Deleted";
} else{
echo("<b>Are you sure you want to delete this offer?</b><br><br><a href=admin.php?f=spo&delete=yes&spoid=$id&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=spo><b>No</b></a>");
exit;
}

}

//Change A Rank
elseif ($rank == "up" || $rank == "down") {

$runchange = "yes";
$id = $_GET['spoid'];

$checkfirstrow = lfmsql_query("Select rank from ".$prefix."spo order by rank asc");
$firstrow = lfmsql_result($checkfirstrow, 0, "rank");

$checklastrow = lfmsql_query("Select rank from ".$prefix."spo order by rank desc");
$lastrow = lfmsql_result($checklastrow, 0, "rank");

if (($rank=="up") && ($id > $firstrow)) {
$change=$id-1;
}
elseif (($rank=="down") && ($id < $lastrow)) {
$change=$id+1;
}
else {
$runchange = "no";
}

if ($runchange == "yes") {
@lfmsql_query("Update ".$prefix."spo set rank=0 where rank=$id");
@lfmsql_query("Update ".$prefix."spo set rank=$id where rank=$change");
@lfmsql_query("Update ".$prefix."spo set rank=$change where rank=0");
@lfmsql_query("ALTER TABLE ".$prefix."spo ORDER BY rank;");
$resmessage = "Rank Changed";
}

}

//Pause and Unpause
elseif ($pause == "yes") {
$id = $_GET['spoid'];
@lfmsql_query("Update ".$prefix."spo set paused=1 where id=$id limit 1");
$resmessage = "Offer Paused";
}
elseif ($unpause == "yes") {
$id = $_GET['spoid'];
@lfmsql_query("Update ".$prefix."spo set paused=0 where id=$id limit 1");
$resmessage = "Offer Unpaused";
}


//Begin Main Page

echo("<center>");

if ($resmessage != "") {
echo("<p><font face=$fontface size=2>$resmessage</font></p>");
}

//Show Offers

$getdata = lfmsql_query("Select * from ".$prefix."spo order by rank asc");

if (lfmsql_num_rows($getdata) != 0) {

for ($i = 0; $i < lfmsql_num_rows($getdata); $i++) {

$id = lfmsql_result($getdata, $i, "id");
$realrank = lfmsql_result($getdata, $i, "rank");
$rank = $i+1;
$offername = lfmsql_result($getdata, $i, "offername");
$ipn = lfmsql_result($getdata, $i, "ipn");
$acctype = lfmsql_result($getdata, $i, "acctype");
$memtype = lfmsql_result($getdata, $i, "memtype");
$reptype = lfmsql_result($getdata, $i, "reptype");
$mdays = lfmsql_result($getdata, $i, "mdays");
$paused = lfmsql_result($getdata, $i, "paused");
$text = lfmsql_result($getdata, $i, "text");

echo("
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=spo&update=yes&spoid=$id\">
<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=9 align=center><b>Edit Offer</b><br><a target=_blank href=\"$site_url/previewoffer.php?spoid=$id\">Preview Offer</a></td></tr>
<tr><td colspan=9 align=left><b>Offer Name:</b> <input type=text size=30 name=offername value=\"$offername\"></td></tr>
<tr><td align=center><b>Rank</b></td>
<td align=center><b>IPN ID</b></td>
<td align=center><b>Show To Account</b></td>
<td align=center><b>Show To</b></td>
<td align=center><b>Display Frequency</b></td>
<td align=center><b>Min. Days</b></td>
<td align=center><b>Options</b></td>
<td align=center><b>Action</b></td></tr>");


echo("
<tr><td align=center>$rank<br><font size=2><a href=admin.php?f=spo&rank=up&spoid=$realrank>Move Up</a><br><a href=admin.php?f=spo&rank=down&spoid=$realrank>Move Down</a></font></td>
<td align=center><input type=text size=5 name=ipn value=\"$ipn\"></td>

<td align=center><select name=acctype><option value=0>All Members</option>");

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
echo("<option value=$accid"); if($acctype==$accid){echo(" selected");} echo(">$accname</option>");
}
echo("</select></td>

<td align=center>");
if($memtype==0){echo("All<br>Members");}
if($memtype==1){echo("Existing<br>Members");}
if($memtype==2){echo("Incoming<br>Members");}
echo("</td>

<td align=center><select name=reptype><option value=0>Once Per Member</option>");
echo("<option value=1"); if($reptype==1){echo(" selected");} echo(">Every Login</option>");
echo("<option value=2"); if($reptype==2){echo(" selected");} echo(">Daily</option>");
echo("<option value=3"); if($reptype==3){echo(" selected");} echo(">Weekly</option>");
echo("<option value=4"); if($reptype==4){echo(" selected");} echo(">Every 2 Weeks</option>");
echo("<option value=5"); if($reptype==5){echo(" selected");} echo(">Monthly</option>");
echo("<option value=6"); if($reptype==6){echo(" selected");} echo(">Every 2 Months</option>");
echo("<option value=7"); if($reptype==7){echo(" selected");} echo(">Yearly</option>");
echo("</select></td>

<td align=center><input type=text size=2 name=mdays value=$mdays></td>

<td align=center>");

if($paused == 0) { echo("<font size=2><a href=\"admin.php?f=spo&pause=yes&spoid=$id\">Pause Offer</a>"); }
if($paused == 1) { echo("<font size=2><a href=\"admin.php?f=spo&unpause=yes&spoid=$id\">Unpause Offer</a>"); }

echo("<br><a href=\"admin.php?f=spo&reset=yes&spoid=$id\">Reset Offer</a>
<br><a href=\"admin.php?f=spo&delete=yes&spoid=$id\">Delete Offer</a></font></td>
<td align=center><input type=submit value=Update></td></tr>
<tr><td colspan=9 align=center valign=center>
<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7>$text</textarea></td></tr></table>
</td></tr></table>
</form><br><br>");

}

}

//Add New Offer

echo("
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=spo&add=yes\">
<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td colspan=9 align=center><b>Add New Offer</b></td></tr>
<tr><td colspan=9 align=left><b>Offer Name:</b> <input type=text size=30 name=offername value=\"\"></td></tr>
<tr><td align=center><b>Rank</b></td>
<td align=center><b>IPN ID</b></td>
<td align=center><b>Show To Account</b></td>
<td align=center><b>Show To</b></td>
<td align=center><b>Display Frequency</b></td>
<td align=center><b>Min. Days</b></td>
<td align=center><b>Options</b></td>
<td align=center><b>Action</b></td></tr>");

echo("
<tr><td align=center>New:</td>
<td align=center><input type=text size=5 name=ipn></td>
<td align=center><select name=acctype><option value=0>All Members</option>");

$getaccounts = lfmsql_query("Select mtid, accname from `".$prefix."membertypes` order by mtid asc");
for ($j = 0; $j < lfmsql_num_rows($getaccounts); $j++) {
$accid = lfmsql_result($getaccounts, $j, "mtid");
$accname = lfmsql_result($getaccounts, $j, "accname");
echo("<option value=$accid>$accname</option>");
}
echo("</select></td>

<td align=center><select name=memtype><option value=0>All Members</option><option value=1>Existing Members</option><option value=2>Incoming Members</option></select></td>

<td align=center><select name=reptype><option value=0>Once Per Member</option>");
echo("<option value=1>Every Login</option>");
echo("<option value=2>Daily</option>");
echo("<option value=3>Weekly</option>");
echo("<option value=4>Every 2 Weeks</option>");
echo("<option value=5>Monthly</option>");
echo("<option value=6>Every 2 Months</option>");
echo("<option value=7>Yearly</option>");
echo("</select></td>

<td align=center><input type=text size=2 name=mdays value=$mdays></td>

<td align=center>N/A</td>
<td align=center><input type=submit value=Add></td></tr>
<tr><td colspan=9 align=center valign=center>
<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center><b>Offer HTML</b><br>[paymentcode] - IPN payment buttons<br>[firstname] - User's first name<br>[lastname] - User's last name<br>[username] - User's username</td><td align=left valign=center><textarea name=text cols=50 rows=7></textarea></td></tr></table>
</td></tr></table>
</form>");

?>