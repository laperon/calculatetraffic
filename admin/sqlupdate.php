<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.35
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////
// v3.00.01�
// * Added missing cb_id field
//

$domainurl = "http://".$_SERVER["SERVER_NAME"];

/*Table structure for table `admin` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."admin` (`id` tinyint(3) unsigned NOT NULL auto_increment,PRIMARY KEY  (`id`))");
@mysql_query("ALTER TABLE `".$prefix."admin` ADD `admin_name` varchar(64) default NULL");
@mysql_query("ALTER TABLE `".$prefix."admin` ADD `login` varchar(32) NOT NULL default 'admin'");
@mysql_query("ALTER TABLE `".$prefix."admin` ADD `password` varchar(32) NOT NULL default 'password'");
@mysql_query("ALTER TABLE `".$prefix."admin` ADD `email` varchar(64) NOT NULL default 'noreply@pleasesetthisnow.com'");

/*Table structure for table `banners` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."banners` (`id` int(11) NOT NULL auto_increment,`banner` mediumblob NOT NULL,  PRIMARY KEY  (`id`))");

/*Table structure for table `emailmsg` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."emailmsg` (`Id` int(11) NOT NULL auto_increment,  PRIMARY KEY  (`Id`))");
@mysql_query("ALTER TABLE `".$prefix."emailmsg` ADD `status` int(11) unsigned NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."emailmsg` ADD `subject` varchar(150) NOT NULL default 'No Subject'");
@mysql_query("ALTER TABLE `".$prefix."emailmsg` ADD `body` text NOT NULL");
@mysql_query("ALTER TABLE `".$prefix."emailmsg` ADD `msgtype` int(11) unsigned NOT NULL default '0'");

/*Table structure for table `emailrcpt` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."emailrcpt` (`Id` int(11) NOT NULL auto_increment,  PRIMARY KEY  (`Id`))");
@mysql_query("ALTER TABLE `".$prefix."emailrcpt` ADD `address` varchar(150) default NULL");
@mysql_query("ALTER TABLE `".$prefix."emailrcpt` ADD `msgid` int(11) unsigned NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."emailrcpt` ADD `status` int(11) unsigned NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."emailrcpt` ADD `result` varchar(200) default NULL");

/*Table structure for table `groups` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."groups` (`groupid` int(11) NOT NULL auto_increment,PRIMARY KEY  (`groupid`))");
@mysql_query("ALTER TABLE `".$prefix."groups` ADD `groupname` varchar(30) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."groups` ADD `comm` int(11) NOT NULL default '0'");

/*Table structure for table `links` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."links` (`id` int(10) unsigned NOT NULL auto_increment,  PRIMARY KEY  (`id`))");
@mysql_query("ALTER TABLE `".$prefix."links` ADD `imagepath` varchar(254) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."links` ADD `urlpath` varchar(254) NOT NULL default ''");

/*Table structure for table `log` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."log` (`logid` bigint(20) NOT NULL auto_increment,  PRIMARY KEY  (`logid`))");
@mysql_query("ALTER TABLE `".$prefix."log` ADD `logtime` datetime NOT NULL default '0000-00-00 00:00:00'");
@mysql_query("ALTER TABLE `".$prefix."log` ADD `logentry` text NOT NULL");

/*Table structure for table `members` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."members` (`Id` int(11) NOT NULL auto_increment,`username` varchar(32) NOT NULL default '', PRIMARY KEY  (`Id`), UNIQUE KEY `username` (`username`))");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `firstname` varchar(20) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `lastname` varchar(30) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `email` varchar(100) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `address` varchar(50) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `city` varchar(40) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `state` varchar(40) default NULL");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `postcode` varchar(16) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `country` varchar(3) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `telephone` varchar(30) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `password` varchar(32) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `refid` int(11) unsigned default '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `commission` float(6,2) unsigned default '0.00'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `mailbounce` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `status` varchar(16) NOT NULL default 'Active'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `otoview` int(11) default '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `joindate` datetime default '0000-00-00 00:00:00'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `disableddate` datetime default NULL");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `geo` varchar(4) default NULL");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `paypal_email` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `groupid` int(11) unsigned default '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `mtype` int(11) unsigned default '0'");

/* Table structure for deleted members table */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."membersd` (`Id` int(11) NOT NULL auto_increment,`username` varchar(32) NOT NULL default '', PRIMARY KEY  (`Id`), UNIQUE KEY `username` (`username`))");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `firstname` varchar(20) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `lastname` varchar(30) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `email` varchar(100) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `address` varchar(50) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `city` varchar(40) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `state` varchar(40) default NULL");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `postcode` varchar(16) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `country` varchar(3) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `telephone` varchar(30) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `password` varchar(32) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `refid` int(11) unsigned default '0'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `commission` float(6,2) unsigned default '0.00'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `mailbounce` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `status` varchar(16) NOT NULL default 'Active'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `otoview` int(11) default '0'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `joindate` datetime default '0000-00-00 00:00:00'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `disableddate` datetime default NULL");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `geo` varchar(4) default NULL");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `paypal_email` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `groupid` int(11) unsigned default '0'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `mtype` int(11) unsigned default '0'");


/*Table structure for table `membertypes` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."membertypes` (`mtid` int(11) NOT NULL auto_increment,  PRIMARY KEY  (`mtid`))");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `accname` varchar(30) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `accfee` float(6,2) NOT NULL default '0.00'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `acclevel` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `comm` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `enabled` tinyint(4) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `otocomm` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `promocode` varchar(10) default NULL");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `template_data` mediumtext");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `recur` char(1) NOT NULL default 'N'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `_2co_productid` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `utemplate_data` mediumtext");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `description` mediumtext");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `commfixed` float(6,2) default '0.00'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `enablepdate` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `pp_p3` int(11) NOT NULL default '0'");

/*Table structure for table `products` */
@mysql_query("CREATE TABLE IF NOT EXISTs `".$prefix."products` (`productid` int(11) unsigned NOT NULL auto_increment,  PRIMARY KEY  (`productid`))");
@mysql_query("ALTER TABLE `".$prefix."products` ADD `itemid` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."products` ADD `productname` varchar(100) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."products` ADD `filename` varchar(50) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."products` ADD `price` float(4,2) NOT NULL default '0.00'");
@mysql_query("ALTER TABLE `".$prefix."products` ADD `free` smallint(6) NOT NULL default '0'");

/*Table structure for table `promotion` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."promotion` (`id` int(11) NOT NULL auto_increment,  PRIMARY KEY  (`id`))");
@mysql_query("ALTER TABLE `".$prefix."promotion` ADD `type` varchar(20) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."promotion` ADD `content` text NOT NULL");
@mysql_query("ALTER TABLE `".$prefix."promotion` ADD `subject` varchar(50) NOT NULL default ''");

/*Table structure for table `purchases` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."purchases` (`purchaseid` int(11) unsigned NOT NULL auto_increment,  PRIMARY KEY  (`purchaseid`))");
@mysql_query("ALTER TABLE `".$prefix."purchases` ADD `affid` int(11) unsigned NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."purchases` ADD `itemid` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."purchases` ADD `txn_id` varchar(100) NOT NULL default ''");

/*Table structure for table `sales` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."sales` (`salesid` int(11) unsigned NOT NULL auto_increment,  PRIMARY KEY  (`salesid`))");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `affid` int(11) unsigned NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `saledate` datetime NOT NULL default '0000-00-00 00:00:00'");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `itemid` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `itemname` varchar(50) default NULL");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `itemamount` float(4,2) default NULL");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `status` char(1) default NULL");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `commission` float(4,2) default NULL");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `txn_id` varchar(100) NOT NULL default ''");

/*Table structure for table `settings` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."settings` (`id` tinyint(3) unsigned NOT NULL auto_increment,  PRIMARY KEY  (`id`))");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `sitename` varchar(200) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `ototype` int(11) unsigned NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `meta_description` varchar(254) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `meta_keywords` varchar(254) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `paypal_email` varchar(100) NOT NULL default 'youraddress@paypal.com'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item_id` varchar(20) NOT NULL default 'None'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item_name` varchar(40) NOT NULL default 'One Time Offer'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item_price` float(6,2) default '0.00'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `affcomm` float(5,2) NOT NULL default '0.00'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `php_path` varchar(255) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `documentroot` varchar(255) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `smtp_server` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `replyaddress` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `bounceaddress` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `affurl` varchar(255) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `auto_email` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `bgemail` int(11) unsigned NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `pptestmode` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `pptestemail` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `ver` float(3,2) NOT NULL default '1.1'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `_2co_id` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `_2co_productid` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `_2co_secret` varchar(16) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `paypal_enable` tinyint(4) NOT NULL default '1'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `_2co_enable` tinyint(4) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `authnet_enable` tinyint(4) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item2_id` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item3_id` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `_2co_productid2` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `_2co_productid3` varchar(20) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item2_name` varchar(40) default 'None'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item3_name` varchar(40) default 'None'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item2_price` float(6,2) default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `item3_price` float(6,2) default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbb_path` varchar(255) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbb_host` varchar(150) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbb_db` varchar(50) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbb_dbuser` varchar(50) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbb_dbpass` varchar(50) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbb_prefix` varchar(32) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbb_group` varchar(50) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `autoresponder` int(11) default '0' COMMENT '0=None,1=Aweber,2=Other'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `a_meta_web_id` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `a_unit` varchar(50) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `a_req_fields` varchar(100) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `addtophpbb` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `authnet_login` varchar(40) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `authnet_key` varchar(40) default NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `firstcookie` tinyint(4) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `bannerurls` tinyint(4) NOT NULL default '1'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `flow` tinyint(4) NOT NULL default '1'");

/*Table structure for table `templates` */
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."templates` (`template_id` int(11) unsigned NOT NULL auto_increment,  PRIMARY KEY  (`template_id`))");
@mysql_query("ALTER TABLE `".$prefix."templates` ADD `template_name` varchar(20) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."templates` ADD `template_data` mediumtext");
@mysql_query("ALTER TABLE `".$prefix."templates` ADD `sflag` int(11) NOT NULL default '1'");

// Check if the admin table has any data in it
$admres=@mysql_query("SELECT * FROM ".$prefix."admin");
if(@mysql_num_rows($admres) < 1)
{
	// No admin data found. Must be new install
	/*Data for the table `groups` */
	@mysql_query("INSERT INTO `".$prefix."groups` (`groupid`,`groupname`,`comm`) VALUES (1,'Everyone',0)");
	/*Data for the table `members` */
	@mysql_query("INSERT INTO `".$prefix."members` (`Id`,`firstname`,`lastname`,`email`,`address`,`city`,`state`,`postcode`,`country`,`telephone`,`username`,`password`,`refid`,`commission`,`mailbounce`,`status`,`otoview`,`joindate`,`disableddate`,`geo`,`paypal_email`,`groupid`,`mtype`) values (1,'John','Doe','no-one@nowhere.com','200 Jdoe street','Doeville','CA','90210','CA','32323232','testuser','testpass',0,0.00,0,'Active',1,'2006-07-20 15:26:29',NULL,'USA','johndoe@paypal.org',1,1)");
	/*Data for the table `oto_membertypes` */
	@mysql_query("INSERT INTO `".$prefix."membertypes` (`mtid`,`accname`,`accfee`,`acclevel`,`comm`,`enabled`,`otocomm`,`promocode`,`template_data`,`recur`,`_2co_productid`,`utemplate_data`,`description`,`commfixed`) values (1,'Free',0.00,1,0,1,0,'','Welcome ... free member','N','','','',0.00)");
	/*Data for the table `settings` */
	@mysql_query("INSERT INTO `".$prefix."settings` (`id`,`sitename`,`ototype`,`meta_description`,`meta_keywords`,`paypal_email`,`item_id`,`item_name`,`item_price`,`affcomm`,`php_path`,`documentroot`,`smtp_server`,`replyaddress`,`bounceaddress`,`affurl`,`auto_email`,`bgemail`,`pptestmode`,`pptestemail`,`ver`,`_2co_id`,`_2co_productid`,`_2co_secret`,`paypal_enable`,`_2co_enable`,`authnet_enable`,`item2_id`,`item3_id`,`_2co_productid2`,`_2co_productid3`,`item2_name`,`item3_name`,`item2_price`,`item3_price`,`phpbb_host`,`phpbb_db`,`phpbb_dbuser`,`phpbb_dbpass`,`phpbb_prefix`,`phpbb_group`,`autoresponder`,`a_meta_web_id`,`a_unit`,`a_req_fields`,`addtophpbb`,`authnet_login`,`authnet_key`) values (1,'".$yoursitename."',0,'Free Traffic Exchange','traffic,exchange,marketing','".$paypalemail."','None','One Time Offer',50.00,20.00,'/usr/bin/php','','localhost','".$publicemail."','".$publicemail."','".$domainurl."/','',0,1,'',1.20,'','','',0,0,0,'None','None','','','-','-',20.00,10.00,'','','','','','',0,'','','',0,'','')");
	/*Data for the table `templates` */
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Site Header','<table width=\"100%\"><tr>\r\n    <td><img src=\"images/header-green.jpg\" width=\"700\" height=\"125\" /></td>\r\n</tr></table>')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Homepage Body','<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">\r\n  <tr>\r\n    <td bgcolor=\"#D9E6D9\">\r\n	<a href=\"/\">Home</a> | <a href=\"aboutus.php\">About Us</a> | <a href=\"contactus.php\">Contact Us</a></td>\r\n  </tr>\r\n</table>\r\n<table width=\"100%\" border=\"0\" cellpadding=\"4\" cellspacing=\"0\">\r\n  <tr>\r\n    <td align=\"left\" valign=\"top\"><br>\r\n    <br>\r\n    <p> </p>\r\n    <p>\r\n<form name=\"loginfrm\" method=\"post\" action=\"members.php\">\r\n<table width=\"230\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\">\r\n  <tr bgcolor=\"#FBDB79\">\r\n    <td colspan=\"2\" align=\"center\" bgcolor=\"#D9E6D9\"><strong>Member Login </strong></td>\r\n  </tr>\r\n  <tr>\r\n    <td width=\"30\"><strong>Username:</strong></td>\r\n    <td><input type=\"text\" name=\"login\" /></td>\r\n  </tr>\r\n  <tr>\r\n    <td><strong>Password:</strong></td>\r\n    <td><input type=\"password\" name=\"password\" /></td>\r\n  </tr>\r\n  <tr>\r\n    <td colspan=\"2\" align=\"right\"><input type=\"submit\" name=\"Submit\" value=\"Login\" /></td>\r\n  </tr>\r\n  <tr>\r\n    <td colspan=\"2\" align=\"center\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\"><a href=\"signup.php\">Register</a> | <a href=\"lostpass.php\">Lost Password</a> </font></td>\r\n  </tr>\r\n</table>\r\n</form>\r\n</p>\r\n    <p> </p>\r\n    <p> </p>\r\n    <p>All template regions (ie header, body, footer) are complete tables, so there are no problems with adding rows, columns or completely changing the layout in each region. </p>    <p>Add as much information, text, ads etc as you want to down here. </p></td>\r\n  </tr>\r\n</table>\r\n')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Site Footer','<table width=\"100%\"><tr>\r\n    <td bgcolor=\"#D9E6D9\">\r\n    <center><font face=\"Verdana, Arial, Helvetica, sans-serif\">Copyright &copy;2006 One Time Offer</font></center>\r\n    </td>\r\n</tr></table>')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('One Time Offer','The offer (1)\r\n&lt;br&gt;&lt;br&gt;\r\n#PAYMENT#')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('News','<b>Affiliate News and Updates</b>\r\n<p>News, updates and other important information for affiliates can go here')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('OTO Footer','<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">\r\n  <tr>\r\n    <td bgcolor=\"#99bb99\"><div align=\"center\">\r\nCopyright &amp;copy;2006 All Rights Reserved\r\n    </div></td>\r\n  </tr>\r\n</table>\r\n')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Welcome Email','Welcome #FIRSTNAME#,\r\n\r\nWelcome to #SITENAME#. To activate your account, please visit: #VERIFY#\r\n\r\nYour Login is: #USERNAME#\r\nYour Password is: #PASSWORD#\r\n\r\nYou can log in at http://your.domain.com/\r\n\r\nTo earn more money, send people to #REFURL#\r\n\r\nThank you,\r\n\r\nAdmin,\r\n#SITENAME#')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Suspended','This is for suspended members')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Sales Page','This is your sales page\r\n\r\nthe promo....\r\n\r\n#PROMOCODE_2#\r\n<br><br>\r\nHere\'s an affiliate link: http://www.yourdomain.com/?rid=#AFFILIATEID#')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Signup','')");
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Welcome Email','Welcome #FIRSTNAME#,\r\n\r\nWelcome to #SITENAME#. To activate your account, please visit: #VERIFY#\r\n\r\nYour Login is: #USERNAME#\r\nYour Password is: #PASSWORD#\r\n\r\nYou can log in at http://your.domain.com/\r\n\r\nTo earn more money, send people to #REFURL#\r\n\r\nThank you,\r\n\r\nAdmin,\r\n#SITENAME#')");
}

////////////////////////////////////
// Version 1.18b
////////////////////////////////////

// signupform table
@mysql_query("CREATE TABLE  IF NOT EXISTS `".$prefix."signupform` (`id` int(10) unsigned NOT NULL auto_increment, PRIMARY KEY  (`id`))");
@mysql_query("ALTER TABLE `".$prefix."signupform` ADD `fieldname` varchar(50) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."signupform` ADD `fieldsize` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."signupform` ADD `enable` tinyint(4) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."signupform` ADD `require` tinyint(4) NOT NULL default '0'");

// Check if signupform table has data
$sfres=@mysql_query("SELECT * FROM ".$prefix."signupform");
if(@mysql_num_rows($sfres) < 1)
{
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (1, 'firstname', 20, 1, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (2, 'lastname', 20, 1, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (3, 'address', 32, 0, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (4, 'city', 20, 0, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (5, 'state', 20, 0, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (6, 'zip', 12, 0, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (7, 'country', 0, 0, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (8, 'telephone', 20, 0, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (9, 'paypal', 32, 0, 0)");
    @mysql_query("INSERT INTO `".$prefix."signupform` VALUES (10, 'geo', 0, 0, 0)");
}

// Version 1.19 updates
// Filelib table
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."filelib` (`fileid` int(11) NOT NULL auto_increment,`filename` varchar(255) NOT NULL default '',`description` text,`filetag` varchar(50) NOT NULL default '',`filetitle` varchar(100) NOT NULL default 'New File',PRIMARY KEY  (`fileid`))");
// Member Pages
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."memberpages` (`pageid` int(11) NOT NULL auto_increment,`pagename` varchar(50) NOT NULL default 'New Page',`pagedata` mediumtext NOT NULL,`mtype` int(11) NOT NULL default '0',`delay` int(11) NOT NULL default '0',`pagetag` varchar(8) NOT NULL default 'none',`menu` int(11) NOT NULL default '1', PRIMARY KEY  (`pageid`))");
@mysql_query("ALTER TABLE `".$prefix."memberpages` ADD `pageindex` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."memberpages` ADD `menu` int(11) NOT NULL default '1'");
@mysql_query("ALTER TABLE `".$prefix."memberpages` ADD `publishdate` datetime default '0000-00-00 00:00:00'");

@mysql_query("ALTER TABLE `".$prefix."members` MODIFY COLUMN `country` CHAR(3) NOT NULL DEFAULT ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` MODIFY COLUMN `country` CHAR(3) NOT NULL DEFAULT ''");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD COLUMN `comm2` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD COLUMN `otocomm2` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD COLUMN `commfixed2` FLOAT(6,2) NOT NULL DEFAULT '0.00'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD COLUMN `pp_p1` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD COLUMN `pp_t1` CHAR(1) NOT NULL DEFAULT 'N'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD COLUMN `rank` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD COLUMN `salestier` INTEGER(11) NOT NULL DEFAULT '1'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `filelibpath` VARCHAR(255) DEFAULT '/1qaz2wsx/'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `fileprot` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `pp_curr` CHAR(3) NOT NULL DEFAULT 'USD'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `offer1email` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `offer2email` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `offer3email` INTEGER(11) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `otoemailtxt1` MEDIUMTEXT");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `otoemailtxt2` MEDIUMTEXT");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `otoemailtxt3` MEDIUMTEXT");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `otoemailsubj` VARCHAR(255) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."memberpages` ADD COLUMN `pageindex` INT DEFAULT 0");


// Table alterations/additions
@mysql_query("ALTER TABLE `".$prefix."products` ADD COLUMN `_2co_id` VARCHAR(20) NOT NULL DEFAULT ''");
@mysql_query("ALTER TABLE `".$prefix."products` MODIFY COLUMN `price` FLOAT(6,2) NOT NULL DEFAULT '0.00'");
@mysql_query("ALTER TABLE `".$prefix."sales` MODIFY COLUMN `itemamount` FLOAT(6,2) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."sales` MODIFY COLUMN `commission` FLOAT(6,2) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` MODIFY COLUMN `item_price` FLOAT(6,2) DEFAULT '0.00'");
@mysql_query("ALTER TABLE `".$prefix."settings` MODIFY COLUMN `item2_price` FLOAT(6,2) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` MODIFY COLUMN `item3_price` FLOAT(6,2) DEFAULT NULL");

// v3.00.01� Alterations
@mysql_query("INSERT INTO `".$prefix."templates` (`template_id`,`template_name`,`template_data`) values (12,'Sales Page 2','This is your second sales page')");
@mysql_query("INSERT INTO `".$prefix."templates` (`template_id`,`template_name`,`template_data`) values (13,'Sales Page 3','This is your third sales page')");
@mysql_query("INSERT INTO `".$prefix."templates` (`template_id`,`template_name`,`template_data`) values (14,'One Time Offer2','This is OTO 2')");
@mysql_query("INSERT INTO `".$prefix."templates` (`template_id`,`template_name`,`template_data`) values (15,'One Time Offer3','This is OTO 3')");
@mysql_query("INSERT INTO `".$prefix."templates` (`template_id`,`template_name`,`template_data`) values (16,'Deleted','')");
@mysql_query("INSERT INTO `".$prefix."templates` (`template_id`,`template_name`,`template_data`) values (17,'Delete Confirmation','')");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `splittest` INTEGER(11) NOT NULL DEFAULT '1'");
// Add splittest results table
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."splitresults` (`id` int(11) NOT NULL auto_increment,`memberid` int(11) NOT NULL default '0', `salespage` varchar(16) NOT NULL default '',  PRIMARY KEY  (`id`))");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."splithits` (`id` int(11) NOT NULL auto_increment,`remotehost` varchar(255) NOT NULL default '',`salespage` varchar(16) NOT NULL default '',  PRIMARY KEY  (`id`))");

// v3.00.01� Table alterations
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `cb_enable` INTEGER NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `cb_hoplink` VARCHAR(255) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `cb_publisherid` VARCHAR(100) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD COLUMN `cb_secret` VARCHAR(50) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `pp_p3` INTEGER(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `pp_srt` INTEGER(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `cb_paylink` VARCHAR(255) DEFAULT NULL");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `pp_a1` float(6,2) default '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `cb_id` varchar(50) default NULL");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `cb_id` varchar(50) default NULL");

// v2.10 table alterations
@mysql_query("ALTER TABLE `".$prefix."admin` ADD `remotekey` varchar(64) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."admin` ADD `license` varchar(32) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."admin` ADD `encpw` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `newsletter` int(11) NOT NULL default '1'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `lastip` VARCHAR(16) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `signupip` VARCHAR(16) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `lastlogin`  datetime NOT NULL default '0000-00-00 00:00:00'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `newsletter` int(11) NOT NULL default '1'");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `lastip` VARCHAR(16) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `signupip` VARCHAR(16) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."membersd` ADD `lastlogin`  datetime NOT NULL default '0000-00-00 00:00:00'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `phpbbver` int(11) NOT NULL default '2'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `verifyemail` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `notifysale` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `notifydownline` int(11) NOT NULL default '0'");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD `purchaserid` int(11) default '0'");

// New email templates
$tmres=@mysql_query("SELECT COUNT(*) FROM ".$prefix."templates WHERE template_name='Downline Email'");
$tmdcount=@mysql_result($tmres, 0);
if($tmdcount==0)
{
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Downline Email','Hello #FIRSTNAME#,\r\n\r\nYou have referred a new member.\r\n\r\n')");
}
$tmres=@mysql_query("SELECT COUNT(*) FROM ".$prefix."templates WHERE template_name='Commission Email'");
$tmccount=@mysql_result($tmres, 0);
if($tmccount==0)
{
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`,`template_data`) values ('Commission Email','Congratulations #FIRSTNAME#,\r\n\r\nYou have just earned a commission!')");
}

// ** IMPORTANT ** v2.10 uses MD5 passwords
// MD5 password conversion
$chkenc=@mysql_query("SELECT encpw FROM `".$prefix."admin`");
$encstatus=@mysql_result($chkenc,0);
if($encstatus == 0)
{
	$mdres=@mysql_query("SELECT Id,password FROM `".$prefix."members`");
	while($memb=@mysql_fetch_object($mdres))
	{
		$newpass=md5($memb->password);
		@mysql_query("UPDATE `".$prefix."members` SET `password`='$newpass' WHERE Id=".$memb->Id);
	}
}
mysql_query("UPDATE ".$prefix."admin SET encpw=1");

// v3.02 Table alterations JM Era
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `theme` varchar(50) NOT NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `sitedesc` VARCHAR( 40 ) NOT NULL AFTER `sitename`");

// For Themes
@mysql_query("UPDATE `".$prefix."settings` SET `theme` = 'themes/LFMTE_sidebar'");
@mysql_query("DELETE FROM `".$prefix."templates` WHERE `template_name` = 'Site Header'");
@mysql_query("DELETE FROM `".$prefix."templates` WHERE `template_name` = 'Site Footer'");

// For number of logins tracking
@mysql_query("ALTER TABLE `".$prefix."members` ADD `numLogins` INT( 11 ) NOT NULL DEFAULT '0';");

// For Login Offers
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."loginoffers`(`id` int(11) NOT NULL auto_increment, `days` int(4) NOT NULL default '0', `mtype` int(4) NOT NULL default '0', `productid` int(11) NOT NULL, `active` char(1) NOT NULL default 'N', `stats1` int(11) NOT NULL default '0', `stats2` int(11) NOT NULL default '0', `stats3` int(11) NOT NULL default '0', `lastview` int(10) NOT NULL, `offer` mediumtext NOT NULL, PRIMARY KEY  (`id`))");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `lastlogoffer` INT( 10 ) NOT NULL , ADD `logoffertime` INT( 10 ) NOT NULL ;");

// For Downline Builder
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."dlbaffiliateids` ( `program_id` int(11) NOT NULL default '0', `member_id` int(11) NOT NULL default '0', `affId` varchar(40) NOT NULL default '', `category` char(1) NOT NULL default '', KEY `member_id` (`member_id`),  KEY `program_id` (`program_id`))");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."dlbprograms` ( `id` int(11) NOT NULL auto_increment, `programId` varchar(30) NOT NULL default '', `description` longtext NOT NULL default '', `programUrl` varchar(255) NOT NULL default '', `category` char(1) NOT NULL default 'P', `ownerAfId` varchar(50) NOT NULL default '', PRIMARY KEY  (`id`), UNIQUE KEY `programId` (`programId`))");

$tmres=@mysql_query("SELECT COUNT(*) FROM ".$prefix."templates WHERE template_name='Downline Bldr Header'");
$tmccount=@mysql_result($tmres,0);
if($tmccount==0)
{
   @mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `template_data`, `sflag`) VALUES('Downline Bldr Header', 'Unless stated otherwise, joining these  programs is FREE. You are totally at liberty to choose which programs to join (or not). Any that you do not join will default to your sponsors referral id. <br><br>You can change any of these codes at any time. Simply enter the new referral id and click the &quot;Update&quot; button. <br><br>Always check each link.	Never leave one that doesn''t work (readers don''t like it !!).', 1);");
}

$tmres=@mysql_query("SELECT COUNT(*) FROM ".$prefix."templates WHERE template_name='Download Page'");
$tmccount=@mysql_result($tmres,0);
if($tmccount==0)
{
   @mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `template_data`, `sflag`) VALUES('Download Page', '', 1);");
}

// For OTO Mod
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."oto_groups` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,`groupname` varchar( 50 ) NOT NULL DEFAULT '',`splitmirror` INT( 11 ) NOT NULL DEFAULT '0',`percent` INT( 11 ) NOT NULL DEFAULT '0',`highnum` INT( 11 ) NOT NULL DEFAULT '0',`lownum` INT( 11 ) NOT NULL DEFAULT '0',`days` INT( 11 ) NOT NULL DEFAULT '0',`acctype` INT( 11 ) NOT NULL DEFAULT '1',`groupshown` INT( 11 ) NOT NULL DEFAULT '0',`groupbought` INT( 11 ) NOT NULL DEFAULT '0',`groupsales` decimal(5,2) NOT NULL default '0.00') ENGINE = MYISAM ;");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."oto_offers` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,`offername` varchar( 50 ) NOT NULL DEFAULT '',`groupid` INT( 11 ) NOT NULL DEFAULT '0',`rank` INT( 11 ) NOT NULL DEFAULT '0',`ipn` varchar( 50 ) NOT NULL DEFAULT '0',`text` longtext NOT NULL ,`timesshown` INT( 11 ) NOT NULL DEFAULT '0',`timesbought` INT( 11 ) NOT NULL DEFAULT '0',`sales` decimal(5,2) NOT NULL default '0.00') ENGINE = MYISAM ;");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."oto_stats` (`id` INT( 11 ) NULL AUTO_INCREMENT PRIMARY KEY ,`groupid` INT( 11 ) NULL DEFAULT '0',`userid` INT( 11 ) NULL ) ENGINE = MYISAM ;");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."spo` (`id` INT( 11 ) NULL AUTO_INCREMENT ,`offername` varchar( 50 ) NOT NULL DEFAULT '',`rank` INT( 11 ) NULL ,`ipn` varchar( 50 ) NULL ,`acctype` INT( 11 ) NULL DEFAULT '0',`memtype` INT( 11 ) NULL DEFAULT '0',`reptype` INT( 11 ) NULL DEFAULT '0',`mdays` INT( 11 ) NULL DEFAULT '0',`paused` INT( 11 ) NULL DEFAULT '0',`text` longtext NULL ,PRIMARY KEY ( `id` ) ) ENGINE = MYISAM ;");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."spostats` (`id` INT( 11 ) NULL AUTO_INCREMENT ,`spoid` INT( 11 ) NULL ,`userid` INT( 11 ) NULL ,`lastshown` INT( 11 ) NULL DEFAULT '0',PRIMARY KEY ( `id` ) ) ENGINE = MYISAM ;");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."rto` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,`totalpercent` INT( 3 ) NOT NULL DEFAULT '0',PRIMARY KEY ( `id` ) ) ENGINE = MYISAM ;");
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."rtoads` (`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,`ipn` varchar( 50 ) NOT NULL DEFAULT '0',`shown` INT( 11 ) NOT NULL DEFAULT '0',`percent` INT( 11 ) NOT NULL DEFAULT '0',`highnum` INT( 11 ) NOT NULL DEFAULT '0',`lownum` INT( 11 ) NOT NULL DEFAULT '0',`content` longtext NOT NULL ,PRIMARY KEY ( `id` ) ) ENGINE = MYISAM ;");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `lastotoid` varchar(20) NOT NULL default '0/0/0';");

// START LFMTE UPDATES

// Main Tables

@mysql_query("CREATE TABLE `".$prefix."msites` (
`id` INT( 11 ) NOT NULL auto_increment ,
`state` TINYINT( 1 ) NOT NULL DEFAULT '0',
`memid` INT( 11 ) NOT NULL ,
`url` VARCHAR( 100 ) NOT NULL ,
`credits` FLOAT NOT NULL DEFAULT '0',
`assign` TINYINT( 3 ) NOT NULL DEFAULT '0',
`hits` INT( 11 ) NOT NULL DEFAULT '0',
`hitstoday` INT( 11 ) NOT NULL DEFAULT '0',
`hitsyesterday` INT( 11 ) NOT NULL DEFAULT '0',
`lasthit` INT( 11 ) NOT NULL DEFAULT '0',
`f4check` INT( 11 ) NOT NULL DEFAULT '0',
PRIMARY KEY ( `id` ) ,
INDEX ( `state` , `memid` , `credits` , `lasthit` )
) ENGINE = MYISAM ");

@mysql_query("CREATE TABLE `".$prefix."mbanners` (
`id` INT( 11 ) NOT NULL auto_increment ,
`state` TINYINT( 1 ) NOT NULL DEFAULT '0',
`memid` INT( 11 ) NOT NULL ,
`img` VARCHAR( 100 ) NOT NULL ,
`target` VARCHAR( 100 ) NOT NULL ,
`imps` INT( 11 ) NOT NULL DEFAULT '0',
`hits` INT( 11 ) NOT NULL DEFAULT '0',
`clicks` INT( 11 ) NOT NULL DEFAULT '0',
`hitstoday` INT( 11 ) NOT NULL DEFAULT '0',
`hitsyesterday` INT( 11 ) NOT NULL DEFAULT '0',
`lasthit` INT( 11 ) NOT NULL DEFAULT '0',
PRIMARY KEY ( `id` ) ,
INDEX ( `state` , `memid` , `imps` , `lasthit` ) 
) ENGINE = MYISAM ");

@mysql_query("CREATE TABLE `".$prefix."mtexts` (
`id` INT( 11 ) NOT NULL auto_increment ,
`state` TINYINT( 1 ) NOT NULL DEFAULT '0',
`memid` INT( 11 ) NOT NULL ,
`text` VARCHAR( 50 ) NOT NULL ,
`target` VARCHAR( 100 ) NOT NULL ,
`imps` INT( 11 ) NOT NULL DEFAULT '0',
`hits` INT( 11 ) NOT NULL DEFAULT '0',
`clicks` INT( 11 ) NOT NULL DEFAULT '0',
`hitstoday` INT( 11 ) NOT NULL DEFAULT '0',
`hitsyesterday` INT( 11 ) NOT NULL DEFAULT '0',
`numchars` TINYINT( 2 ) NOT NULL DEFAULT '50',
`lasthit` INT( 11 ) NOT NULL DEFAULT '0',
PRIMARY KEY ( `id` ) ,
INDEX ( `state` , `memid` , `imps` , `numchars` , `lasthit` ) 
) ENGINE = MYISAM ");

@mysql_query("INSERT INTO ".$prefix."msites (memid, url, credits, state) VALUES (1, 'http://thetrafficexchangescript.com', 0, 1)");
@mysql_query("INSERT INTO ".$prefix."mbanners (memid, img, target, imps, state) VALUES (1, 'http://intellibanners.com/banners/banner4.jpg', 'http://intellibanners.com', 0, 1)");
@mysql_query("INSERT INTO ".$prefix."mtexts (memid, text, target, imps, state) VALUES (1, 'Text Ad', 'http://thetrafficexchangescript.com', 0, 1)");

@mysql_query("CREATE TABLE `".$prefix."reports` (
`id` INT( 11 ) NOT NULL auto_increment ,
`date` date NOT NULL default '0000-00-00',
`userfrom` INT( 11 ) NOT NULL ,
`siteid` INT( 11 ) NOT NULL ,
`text` VARCHAR( 50 ) NOT NULL ,
`action` INT( 11 ) NOT NULL DEFAULT '0',
`autosuspended` TINYINT( 1 ) NOT NULL DEFAULT '0',
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM ");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."datestats` (
  `id` int(11) NOT NULL auto_increment,
  `date` date NOT NULL default '0000-00-00',
  `crdsearned` int(11) NOT NULL default '0',
  `hitsdelivered` int(11) NOT NULL default '0',
  `perurl` int(11) NOT NULL default '0',
  `perbanner` int(11) NOT NULL default '0',
  `pertext` int(11) NOT NULL default '0',
  `memsurfed` int(11) NOT NULL default '0',
  `signups` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."anticheat` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
`accuracy` INT( 11 ) NOT NULL DEFAULT '75',
`gracep` INT( 11 ) NOT NULL DEFAULT '20',
`sessiontime` INT( 11 ) NOT NULL DEFAULT '5',
`maxtime` INT( 11 ) NOT NULL DEFAULT '12',
`encrypt` INT( 11 ) NOT NULL DEFAULT '1',
PRIMARY KEY ( `id` ) 
) ENGINE = MYISAM ");

@mysql_query("INSERT INTO `".$prefix."anticheat` (
`accuracy` ,
`gracep` ,
`sessiontime` ,
`maxtime` ,
`encrypt` 
)
VALUES (
'75', '20', '5', '12', '1'
)");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."dynamic` (
  `id` int(11) NOT NULL auto_increment,
  `accid` INT( 11 ) NOT NULL DEFAULT '1',
  `clicks` int(11) NOT NULL default '0',
  `boost` decimal(5,2) NOT NULL default '0.00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."bonuspages` (
  `id` int(11) NOT NULL auto_increment,
  `pagerank` INT( 11 ) NOT NULL DEFAULT '0',
  `accid` INT( 11 ) NOT NULL DEFAULT '1',
  `showtype` int(11) NOT NULL default '0',
  `shownum` int(11) NOT NULL default '0',
  `url` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."spartners` (
  `id` int(11) NOT NULL auto_increment,
  `rank` INT( 11 ) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL default '',
  `domain` varchar(255) NOT NULL default '',
  `descr` text,
  `referid` INT( 11 ) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");



@mysql_query("ALTER TABLE `".$prefix."members` ADD `credits` FLOAT NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `bannerimps` INT( 11 ) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `textimps` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."members` ADD `lastsite` INT( 11 ) NOT NULL DEFAULT '0',
ADD `lastbanner` INT( 11 ) NOT NULL DEFAULT '0',
ADD `lasttext` INT( 11 ) NOT NULL DEFAULT '0',
ADD `lastclick` INT( 11 ) NOT NULL DEFAULT '0',
ADD `lasttoday` INT( 11 ) NOT NULL DEFAULT '0',
ADD `starttime` INT( 11 ) NOT NULL DEFAULT '0',
ADD `timetoday` INT( 11 ) NOT NULL DEFAULT '0',
ADD `recordtime` INT( 11 ) NOT NULL DEFAULT '0',
ADD `actsite` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD `actbanner` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD `acttext` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD `goodclicks` INT( 11 ) NOT NULL DEFAULT '0',
ADD `badclicks` INT( 11 ) NOT NULL DEFAULT '0',
ADD `creditstoday` FLOAT NOT NULL DEFAULT '0',
ADD `creditsyesterday` FLOAT NOT NULL DEFAULT '0',
ADD `clickstoday` INT( 11 ) NOT NULL DEFAULT '0',
ADD `clicksyesterday` INT( 11 ) NOT NULL DEFAULT '0',
ADD `currentview` INT( 11 ) NOT NULL DEFAULT '0',
ADD `lastview` INT( 11 ) NOT NULL DEFAULT '0',
ADD `lastbonus` varchar(20) NOT NULL default '0/0',
ADD `claimws` int(1) default '0',
ADD `wssecure` varchar(100) default '0',
ADD `wsreset` int(1) default '0',
ADD `wsword` int(11) default '1',
ADD `wsletter` int(2) default '1'");

@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `lastsite` )");
@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `lastbanner` )");
@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `lasttext` )");
@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `actsite` )");
@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `actbanner` )");
@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `acttext` )");

@mysql_query("ALTER TABLE `".$prefix."members` ADD `vericode` varchar(50) NOT NULL default ''");

@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `surfratio` FLOAT NOT NULL DEFAULT '0.5',
ADD `surftimer` TINYINT( 3 ) NOT NULL DEFAULT '10',
ADD `refcrds` TINYINT( 3 ) NOT NULL DEFAULT '0',
ADD `refcrds2` TINYINT( 3 ) NOT NULL DEFAULT '0',
ADD `hitvalue` INT( 11 ) NOT NULL DEFAULT '1',
ADD `bannervalue` INT( 11 ) NOT NULL DEFAULT '10',
ADD `textvalue` INT( 11 ) NOT NULL DEFAULT '15',
ADD `maxsites` INT( 11 ) NOT NULL DEFAULT '10',
ADD `maxbanners` INT( 11 ) NOT NULL DEFAULT '10',
ADD `maxtexts` INT( 11 ) NOT NULL DEFAULT '10',
ADD `minauto` TINYINT( 3 ) NOT NULL DEFAULT '0',
ADD `mansites` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD `manbanners` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD `mantexts` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD `monthcrds` INT( 11 ) NOT NULL DEFAULT '0',
ADD `monthbimps` INT( 11 ) NOT NULL DEFAULT '0',
ADD `monthtimps` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `tecron` DATE NOT NULL ");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `wsreset` DATE NOT NULL ");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `hitsconnectid` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `storeimps` INT( 11 ) NOT NULL DEFAULT '1'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `defurl` varchar(200) NOT NULL DEFAULT 'http://www.advertisingknowhow.com/members/traffic.php'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `defbanimg` varchar(200) NOT NULL DEFAULT 'http://intellibanners.com/banners/banner4.jpg'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `defbantar` varchar(200) NOT NULL DEFAULT 'http://intellibanners.com'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `deftextad` varchar(200) NOT NULL DEFAULT 'Track Your Hits'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `deftexttar` varchar(200) NOT NULL DEFAULT 'http://hitsconnect.com'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `reqemailver` INT( 11 ) NOT NULL DEFAULT '1'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `rotationtype` INT( 11 ) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `allowduplicates` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `surflogdays` INT( 11 ) NOT NULL DEFAULT '360'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `framebreakblock` INT( 11 ) NOT NULL DEFAULT '1'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `showrefmail` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `showeditors` INT( 11 ) NOT NULL DEFAULT '1'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `unveridel` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."products` ADD `credits` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."products` ADD `memtype` INT( 11 ) NOT NULL DEFAULT '0',
ADD `prodrank` INT( 11 ) NOT NULL DEFAULT '0',
ADD `subtype` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD `visible` TINYINT( 1 ) NOT NULL DEFAULT '1'");

// HitsConnect Checker Tables

@mysql_query("CREATE TABLE IF NOT EXISTS `approved_sites` (
  `id` int(11) NOT NULL auto_increment,
  `domain` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("CREATE TABLE IF NOT EXISTS `banned_sites` (
  `id` int(11) NOT NULL auto_increment,
  `domain` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("CREATE TABLE `".$prefix."f4checks` (
`id` int(11) NOT NULL auto_increment,
`user` int(11) NOT NULL default '0',
`page_checked` varchar(255) NOT NULL default '',
`errors` text NOT NULL,
`warnings` text NOT NULL,
`status` varchar(255) NOT NULL default '',
`from` varchar(255) NOT NULL default '',
PRIMARY KEY (`id`)
)");

// IPN Tables

@mysql_query("ALTER TABLE `".$prefix."members` ADD `upgend` date NOT NULL default '0000-00-00'");

@mysql_query("CREATE TABLE `".$prefix."ipn_settings` (
  `id` int(11) NOT NULL auto_increment,
  `field` varchar(20) default NULL,
  `value` float default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("INSERT INTO `".$prefix."ipn_settings` VALUES (1,'adnotify', 1)");
@mysql_query("INSERT INTO `".$prefix."ipn_settings` VALUES (2,'memnotify', 1)");
@mysql_query("INSERT INTO `".$prefix."ipn_settings` VALUES (3,'refnotify', 1)");

@mysql_query("CREATE TABLE `".$prefix."ipn_vals` (
  `id` int(11) NOT NULL auto_increment,
  `field` varchar(20) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (1, 'notify', '".$domainurl."/ipn.php')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (2, 'return', '".$domainurl."/members.php')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (3, 'cancel', '".$domainurl."/members.php')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (4, 'images', '".$domainurl."/images/')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (5, 'sitename', '".$yoursitename."')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (6, 'memsubj', 'Thank you for your purchase')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (7, 'membody', 'Hi [username],\r\n\r\nYour [sitename] account has been automatically updated:\r\n\r\n[details]\r\n\r\nRegards\r\n\r\n[sitename]')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (8, 'refsubj', 'You have just earned a commission')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (9, 'refbody', 'Hi [username],\r\n\r\nOne of your referrals has just sent a payment to [sitename].\r\n\r\nYour commission has been added to your account.\r\n\r\n[details]\r\n\r\nRegards\r\n\r\n[sitename]')");
@mysql_query("INSERT INTO `".$prefix."ipn_vals` VALUES (10, 'bcc', '')");

@mysql_query("CREATE TABLE `".$prefix."ipn_merchants` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(18) NOT NULL default '',
  `payee` varchar(60) NOT NULL default '',
  `verify` varchar(32) NOT NULL default '',
  `buy_now` text NOT NULL,
  `subscribe` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("INSERT INTO `".$prefix."ipn_merchants` VALUES(1, 'PayPal', '".$paypalemail."', '', '<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">\r\n<input type=\"hidden\" name=\"cmd\" value=\"_xclick\">\r\n<input type=\"hidden\" name=\"business\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"item_name\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"amount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"no_note\" value=\"1\">\r\n<input type=\"hidden\" name=\"currency_code\" value=\"USD\">\r\n<input type=\"hidden\" name=\"item_number\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"custom\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"return\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"notify_url\" value=\"[notify]\">\r\n<input type=\"image\" src=\"[images]paypal_buy.gif\" border=\"0\" name=\"submit\" alt=\"Make payments with PayPal - it''s fast, free and secure!\">\r\n</form>', '<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">\r\n<input type=\"hidden\" name=\"cmd\" value=\"_xclick-subscriptions\">\r\n<input type=\"hidden\" name=\"business\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"item_name\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"no_shipping\" value=\"1\">\r\n<input type=\"hidden\" name=\"no_note\" value=\"1\">\r\n<input type=\"hidden\" name=\"currency_code\" value=\"USD\">\r\n<input type=\"hidden\" name=\"bn\" value=\"PP-SubscriptionsBF\">\r\n<input type=\"hidden\" name=\"a1\" value=\"[trial_amount]\">\r\n<input type=\"hidden\" name=\"p1\" value=\"[trial_period]\">\r\n<input type=\"hidden\" name=\"t1\" value=\"[trial_type]\">\r\n<input type=\"hidden\" name=\"a3\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"p3\" value=\"[period]\">\r\n<input type=\"hidden\" name=\"t3\" value=\"[type]\">\r\n<input type=\"hidden\" name=\"src\" value=\"1\">\r\n<input type=\"hidden\" name=\"sra\" value=\"1\">\r\n<input type=\"hidden\" name=\"item_number\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"custom\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"return\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"notify_url\" value=\"[notify]\">\r\n<input type=\"image\" src=\"[images]paypal_subscribe.gif\" border=\"0\" name=\"submit\" alt=\"Make payments with PayPal - it''s fast, free and secure!\">\r\n</form>')");

@mysql_query("INSERT INTO `".$prefix."ipn_merchants` VALUES(2, 'SafePay', '', '', '<form action=\"https://www.safepaysolutions.com/index.php\" method=\"post\">\r\n<input type=\"hidden\" name=\"_ipn_act\" value=\"_ipn_payment\">\r\n<input type=\"hidden\" name=\"iowner\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"ireceiver\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"iamount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"itemName\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"itemNum\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"idescr\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"idelivery\" value=\"1\">\r\n<input type=\"hidden\" name=\"iquantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"imultiplyPurchase\" value=\"n\">\r\n<input type=\"hidden\" name=\"custom1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"notifyURL\" value=\"[notify]\">\r\n<input type=\"hidden\" name=\"returnURL\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"cancelURL\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[images]safepay_buy.gif\" alt=\"Pay through SafePay Solutions\">\r\n</form>', '<form action=\"https://www.safepaysolutions.com/index.php\" method=\"post\">\r\n<input type=\"hidden\" name=\"_ipn_act\" value=\"_ipn_subscription\">\r\n<input type=\"hidden\" name=\"ireceiver\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"itemName\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"itemNum\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"idescr\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"trialAmount\" value=\"[trial_amount]\">\r\n<input type=\"hidden\" name=\"trialPeriod\" value=\"[trial_days]\">\r\n<input type=\"hidden\" name=\"trialCycles\" value=\"1\">\r\n<input type=\"hidden\" name=\"iamount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"cycleLength\" value=\"[days]\">\r\n<input type=\"hidden\" name=\"cycles\" value=\"0\">\r\n<input type=\"hidden\" name=\"custom1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"notifyURL\" value=\"[notify]\">\r\n<input type=\"hidden\" name=\"returnURL\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"cancelURL\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[images]safepay_subscribe.gif\" alt=\"Pay through SafePay Solutions\">\r\n</form>')");

@mysql_query("INSERT INTO `".$prefix."ipn_merchants` VALUES(3, 'Payza', '', '', '<form action=\"https://secure.payza.com/checkout/PayProcess.aspx\" method=\"post\">\r\n<input type=\"hidden\" name=\"ap_purchasetype\" value=\"Item\">\r\n<input type=\"hidden\" name=\"ap_merchant\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"ap_quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"ap_itemname\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"ap_description\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"apc_1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"apc_2\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"ap_amount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"ap_currency\" value=\"USD\">\r\n<input type=\"hidden\" name=\"ap_returnurl\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"ap_cancelurl\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[images]alertpay_buy.gif\" alt=\"Buy now using Payza\">\r\n</form>', '<form method=\"post\" action=\"https://secure.payza.com/checkout/PayProcess.aspx\">\r\n<input type=\"hidden\" name=\"ap_purchasetype\" value=\"subscription\">\r\n<input type=\"hidden\" name=\"ap_merchant\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"ap_itemname\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"ap_currency\" value=\"USD\">\r\n<input type=\"hidden\" name=\"apc_1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"apc_2\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"ap_quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"ap_description\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"ap_amount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"ap_timeunit\" value=\"[unit]\">\r\n<input type=\"hidden\" name=\"ap_periodlength\" value=\"[period]\">\r\n<input type=\"hidden\" name=\"ap_trialtimeunit\" value=\"[trial_unit]\">\r\n<input type=\"hidden\" name=\"ap_trialperiodlength\" value=\"[trial_period]\">\r\n<input type=\"hidden\" name=\"ap_trialamount\" value=\"[trial_amount]\">\r\n<input type=\"hidden\" name=\"ap_returnurl\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"ap_cancelurl\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[images]alertpay_subscribe.gif\" alt=\"Subscribe using Payza\">\r\n</form>')");

@mysql_query("INSERT INTO `".$prefix."ipn_merchants` VALUES (NULL, '2CheckOut', '', '', '<form action=\"https://www.2checkout.com/2co/buyer/purchase\" method=\"post\">\r\n<input type=\"hidden\" name=\"sid\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"description\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"product_id\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"fixed\" value=\"Y\">\r\n<input type=\"hidden\" name=\"user_id\" value=\"[user_id]\">\r\n<input type=\"image\" src=\"[images]2co_cc.gif\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy from 2CO\">\r\n</form>', '<form action=\"https://www.2checkout.com/2co/buyer/purchase\" method=\"post\">\r\n<input type=\"hidden\" name=\"sid\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"description\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"product_id\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"fixed\" value=\"Y\">\r\n<input type=\"hidden\" name=\"user_id\" value=\"[user_id]\">\r\n<input type=\"image\" src=\"[images]2co_cc.gif\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy from 2CO\">\r\n</form>')");

@mysql_query("CREATE TABLE `".$prefix."ipn_products` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `promocode` varchar(50) NOT NULL default '',
  `description` text NOT NULL,
  `credits` int(10) unsigned NOT NULL default '0',
  `bimps` int(10) unsigned NOT NULL default '0',
  `limps` int(10) unsigned NOT NULL default '0',
  `productid` int(10) unsigned NOT NULL default '0',
  `upgrade` mediumint(8) unsigned NOT NULL default '0',
  `amount` decimal(6,2) NOT NULL default '0.00',
  `period` tinyint(3) unsigned NOT NULL default '0',
  `type` char(1) NOT NULL default 'N',
  `trial_amount` decimal(6,2) NOT NULL default '0.00',
  `trial_period` tinyint(3) unsigned NOT NULL default '0',
  `trial_type` char(1) NOT NULL default 'N',
  `subscription` tinyint(3) unsigned NOT NULL default '0',
  `merchant1` tinyint(1) NOT NULL default '0',
  `merchant2` tinyint(1) NOT NULL default '0',
  `merchant3` tinyint(1) NOT NULL default '0',
  `merchant4` tinyint(1) NOT NULL default '0',
  `merchant5` tinyint(1) NOT NULL default '0',
  `merchant6` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("ALTER TABLE `".$prefix."ipn_products` ADD `showaccount` INT( 11 ) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."ipn_products` ADD `disable` INT( 1 ) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."ipn_products` ADD `twocoid` INT( 11 ) NOT NULL DEFAULT '0'");
@mysql_query("ALTER TABLE `".$prefix."ipn_products` ADD `rank` INT( 11 ) NOT NULL DEFAULT '0'");

@mysql_query("INSERT into `".$prefix."ipn_products` (id, name, description, amount, merchant1) VALUES (1, 'Startpage', 'The startpage is the first site members see when surfing, and you can reserve it for your own site.  Dates in red are already reserved, and dates in green are available.  After purchasing a startpage, <a href=startpages.php>Click Here</a> to enter your URL and check your stats.', '30.00', 1)");

@mysql_query("CREATE TABLE `".$prefix."ipn_transactions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `processor` varchar(20) NOT NULL default '',
  `date` varchar(25) NOT NULL default '',
  `txn_id` varchar(30) NOT NULL default '',
  `txn_type` varchar(30) NOT NULL default '',
  `item_name` varchar(100) NOT NULL default '',
  `item_number` int(10) unsigned NOT NULL default '0',
  `name` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `amount` decimal(6,2) NOT NULL default '0.00',
  `fee` decimal(6,2) NOT NULL default '0.00',
  `user_id` int(10) unsigned NOT NULL default '0',
  `added` datetime NOT NULL default '0000-00-00 00:00:00',
  `refunded` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("CREATE TABLE `".$prefix."startpage` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `from` int(10) unsigned NOT NULL default '0',
  `to` int(10) unsigned NOT NULL default '0',
  `usrid` int(10) unsigned NOT NULL default '0',
  `url` varchar(100) NOT NULL default '',
  `purchased` datetime NOT NULL default '0000-00-00 00:00:00',
  `cost` decimal(5,2) unsigned NOT NULL default '0.00',
  `views` int(10) unsigned NOT NULL default '0',
  `viewstoday` int(10) unsigned NOT NULL default '0',
  `totalunique` int(10) unsigned NOT NULL default '0',
  `uniquetoday` int(10) unsigned NOT NULL default '0',
  `lastview` int(10) unsigned NOT NULL default '0',
  `lastusrid` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("CREATE TABLE `".$prefix."startpage_settings` (
  `id` int(11) NOT NULL default '1',
  `enable` tinyint(1) unsigned NOT NULL default '1',
  `type` char(1) NOT NULL default '',
  `default` varchar(100) NOT NULL default '',
  `showbar` tinyint(1) unsigned NOT NULL default '0',
  `bartext` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

@mysql_query("INSERT INTO `".$prefix."startpage_settings` VALUES(1, 1, 'W', 'http://thetrafficexchangescript.com', 0, '<html><body link=\"#FFFFFF\" bgcolor=\"#000000\" vlink=\"#FFFFFF\" topmargin=\"0\" marginheight=\"0\"><center><font color=white><b>Member Startpage</b><br><a target=_blank href=buycredits.php>Click here to reserve the Startpage for your site</a></center></body></html>')");

@mysql_query("CREATE TABLE `".$prefix."startpage_stats` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `sid` int(10) unsigned NOT NULL default '0',
  `usrid` int(10) unsigned NOT NULL default '0',
  `views` int(10) unsigned NOT NULL default '0',
  `lastview` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

// Ask Admin Tables

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."askadmin_settings` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(50) NOT NULL,
  `notify` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ");


@mysql_query("INSERT INTO `".$prefix."askadmin_settings` (`id`, `email`, `notify`) VALUES
(1, '".$publicemail."', 'yes')");


@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."askadmin_messages` (
  `id` int(11) NOT NULL auto_increment,
  `date_recieved` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `from_user` int(11) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `replied` varchar(10) NOT NULL default 'no',
  `archived` varchar(10) NOT NULL default 'no',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ");

// Bonus Rotator Tables

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."bonusprograms` (
  `id` int(11) NOT NULL auto_increment,
  `rank` int(11) NOT NULL default '0',
  `hits` int(11) NOT NULL default '0',
  `clicks` int(11) NOT NULL default '0',
  `progname` varchar(100) NOT NULL default '',
  `refurl` varchar(100) NOT NULL default '',
  `progdesc` text NOT NULL,
  `bonusp` int(1) NOT NULL default '1',
  `lastshown` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ");

// Preloaded Settings

@mysql_query("INSERT INTO `".$prefix."ipn_products` (`id`, `name`, `promocode`, `description`, `credits`, `productid`, `upgrade`, `amount`, `period`, `type`, `trial_amount`, `trial_period`, `trial_type`, `subscription`, `merchant1`, `merchant2`, `merchant3`, `merchant4`, `merchant5`, `merchant6`, `showaccount`, `disable`, `twocoid`, `rank`) VALUES
(2, 'Yearly Upgrade', '', '', 0, 0, 2, '97.00', 1, 'Y', '0.00', 0, 'N', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1),
(3, 'Yearly Upgrade and LFM', '', '', 0, 1, 2, '69.70', 1, 'Y', '0.00', 0, 'N', 2, 1, 0, 0, 0, 0, 0, 1, 0, 0, 2),
(4, 'Monthly Upgrade', '', '', 0, 0, 2, '9.99', 1, 'M', '0.00', 0, 'N', 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 3),
(5, '1000 Credits', '', '', 1000, 0, 0, '10.00', 0, 'N', '0.00', 0, 'N', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 4),
(6, '3000 Credits', '', '', 3000, 0, 0, '25.00', 0, 'N', '0.00', 0, 'N', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5),
(7, '5000 Credits', '', '', 5000, 0, 0, '35.00', 0, 'N', '0.00', 0, 'N', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 6),
(8, '5000 Credit Subscription', '', '', 5000, 0, 0, '24.85', 1, 'M', '0.00', 0, 'N', 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 7),
(9, 'weekly deal', '', '', 1000, 0, 0, '5.00', 0, 'N', '0.00', 0, 'N', 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 8),
(10, 'loyalty', '', '', 1000, 0, 2, '9.97', 1, 'M', '0.00', 0, 'N', 2, 1, 0, 0, 0, 0, 0, 1, 0, 0, 9),
(11, 'quarter upgrade', '', '', 0, 0, 2, '19.97', 3, 'M', '0.00', 0, 'N', 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 10)");



@mysql_query("INSERT INTO `".$prefix."dlbprograms` (`id`, `programId`, `description`, `programUrl`, `category`, `ownerAfId`) VALUES
(2, '2', '<font size=\"4\"><span style=\"background-color: #FFFF00\">IntelliBanners</span></font> \r\nis the original banner ad rotator and co-op program. This program allows you to \r\nmanage all of your banner campaigns from one location.', 'http://intellibanners.com?ref={affid}', 'P', '1429'),
(3, '3', '<font size=\"4\"><span style=\"background-color: #FFFF00\">Instant Squeeze Page \r\nGenerator</span></font> Create powerful squeeze pages to funnel new subscribers \r\nto your list.\r\n', 'http://www.instantsqueezepagegenerator.com/index.php?rid={affid}', 'P', '29587'),
(4, '4', '<font size=\"4\"><span style=\"background-color: #FFFF00\">FiveHits</span></font> Get more from your click at this powerful and unique traffic exchange.', 'http://fivehits.com?ref={affid}', 'P', '14923'),
(5, '5', '<font size=\"4\"><span style=\"background-color: #FFFF00\">Advertising KnowHow</span></font> As one of the most popular traffic exchanges, Advertising Know How makes traffic generation and list building easy.', 'http://www.advertisingknowhow.com/members/traffic.php?referer={affid}', 'P', 'herme3')");



@mysql_query("INSERT INTO `".$prefix."bonusprograms` (`id`, `rank`, `hits`, `clicks`, `progname`, `refurl`, `progdesc`, `bonusp`, `lastshown`) VALUES
(3, 2, 1, 1, 'ISPG', 'http://www.instantsqueezepagegenerator.com/index.php?rid=29587', '<center>\r\n  <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#0000FF\" width=\"540\">\r\n    <tr>\r\n      <td width=\"532\" bgcolor=\"#0000FF\" height=\"50\">\r\n      <font size=\"5\" color=\"#FFFFFF\">\r\n<p align=\"center\"><strong>\r\nSurprise bonus: Instant Squeeze Page Generator</strong></p>\r\n</font></td>\r\n    </tr>\r\n    <tr>\r\n      <td width=\"532\">\r\n      <h2 align=\"center\"><font face=\"Georgia\"><font color=\"#C81732\">\r\n      <span style=\"text-transform: capitalize\">Professional Squeeze pages </span></font>\r\n      <span style=\"text-transform: capitalize\"><font color=\"#C81732\">In 5 Easy \r\n      Steps</font></span></font></h2>\r\n      <div align=\"center\">\r\n        <center>\r\n        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#111111\" width=\"100%\">\r\n          <tr>\r\n            <td width=\"31%\"><font face=\"Georgia, Times New Roman, Times, serif\">\r\n            <img border=\"0\" src=\"http://instantsqueezepagegenerator.com/images/banners/box-small.jpg\" align=\"left\" width=\"150\" height=\"175\"></font></td>\r\n            <td width=\"69%\">\r\n      <p align=\"justify\" style=\"margin-right: 10\"><font face=\"Gungsuh\">I''ve been able to \r\nget you FREE Lifetime Membership in Instant Squeeze Page Generator - the one \r\nsite where you can build killer squeeze pages with a click and play wizard!\r\n      </font>\r\n      </p>\r\n      <p align=\"justify\" style=\"margin-right: 10\"><font face=\"Gungsuh\">They \r\neven give you multiple choices of free gifts you can use to make that special \r\noffer to get subscribers... But it wasn''t easy - and it won''t last forever!\r\n      </font>\r\n      </p>\r\n            <h3 align=\"center\"><font face=\"Gungsuh\" color=\"#990000\">\r\n            <a target=\"_blank\" href=\"%refurl%\">Claim \r\nyour free membership now!</a></font></h3>\r\n            </td>\r\n          </tr>\r\n        </table>\r\n        </center>\r\n      </div>\r\n      </td>\r\n    </tr>\r\n  </table>\r\n  </center>', 1, 1248906088),
(4, 3, 1, 0, 'AKH', 'http://www.advertisingknowhow.com/members/traffic.php?referer=herme3', '<p align=\"center\"><a target=\"_blank\" href=\"%refurl%\">\r\n<img src=\"http://advertisingknowhow.com/images/squeeze_amend.jpg\" border=\"0\" width=\"587\" height=\"416\">\r\n</a></p>', 1, 1248906181),
(5, 4, 0, 0, 'IntelliBanners', 'http://intellibanners.com/splash3.php?ref=1429', '<center><table style=\"border: 4px dashed ;\" border=0 bordercolor=black width=500 cellpadding=5 cellspacing=0>\r\n<tr>\r\n<td>\r\n<table border=0 cellpadding=4 cellspacing=0>\r\n<tr><td align=center colspan=2>\r\n<p align=center><font color=#D50000 size=6><b>FREE Upgraded Membership!</b></font></p>\r\n</td></tr>\r\n<tr>\r\n<td align=right valign=center>\r\n<a target=_blank href=%refurl%><img src=http://intellibanners.com/banners/banner2.jpg border=0 align=left></a>\r\n</td>\r\n<td align=left>\r\n<p align=left><font face=Tahoma size=4><b>You qualify for a free Upgraded Membership at IntelliBanners.  This offer will not last forever, so be sure to click the link below to get your free account.</font></b></p>\r\n</td>\r\n</tr>\r\n<tr><td align=center colspan=2>\r\n<p align=center><font size=6><b><a target=_blank href=%refurl%>Claim Your Free Upgrade!</a></b></font>\r\n</td></tr>\r\n</table>\r\n</td>\r\n</tr>\r\n</table>\r\n</center>', 1, 1248905663),
(6, 5, 0, 0, 'FiveHits', 'http://fivehits.com?ref=14923', '<br><br><center><table style=\"border: 4px dashed ;\" border=0 bordercolor=black width=500 cellpadding=5 cellspacing=0>\r\n<tr>\r\n<td>\r\n<table border=0 cellpadding=4 cellspacing=0>\r\n<tr><td align=center>\r\n<p align=center><font color=#D50000 size=6><b>Get MORE Per Click!</b></font></p>\r\n</td></tr>\r\n<tr>\r\n<td align=right valign=center>\r\n<a target=_blank href=%refurl%><img src=http://fivehits.com/banners/ban1.jpg border=0 align=center></a>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td align=left>\r\n<p align=left><font face=Tahoma size=4><b>Get more from your click at the FiveHits traffic exchange.</font></b></p>\r\n</td>\r\n</tr>\r\n<tr><td align=center>\r\n<p align=center><font size=6><b><a target=_blank href=%refurl%>Join Free!</a></b></font>\r\n</td></tr>\r\n</table>\r\n</td>\r\n</tr>\r\n</table>\r\n</center>', 1, 1248905689)");



@mysql_query("INSERT INTO `".$prefix."oto_groups` (`id`, `groupname`, `splitmirror`, `percent`, `highnum`, `lownum`, `days`, `acctype`, `groupshown`, `groupbought`, `groupsales`) VALUES
(1, 'First Offer', 0, 100, 100, 0, 0, 0, 0, 0, '0.00')");

$ototext1 = "<title>One Time Offer</title>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n<link href=\"../template/admin/style.css\"  rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style type=\"text/css\">\r\nbody\r\n{ \r\nbackground-color:#000000; \r\n\r\n}\r\n.outermosttableborder {\r\n	border: thin solid #104571;\r\n}\r\n.red {\r\n	color:#ff0000;\r\n	}\r\n</style>\r\n\r\n\r\n<div align=\"center\">\r\n<table  width=\"700\" background=\"".$domainurl."/images/oto/tableback.jpg\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n    <tbody><tr>\r\n      <td >\r\n        <div align=\"center\"><img src=\"".$domainurl."/images/oto/header.jpg\"  style=\"margin: 0pt; padding: 0pt;\" width=\"700\" height=\"268\"> \r\n            <table  width=\"700\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n            <tbody><tr>\r\n              <td > <table  width=\"92%\" align=\"center\" border=\"0\">\r\n                <tbody><tr>\r\n                  <td >\r\n       \r\n\r\n\r\n				   \r\n			\r\n\r\n <p style=\"margin-top: 0pt; margin-bottom: 0pt;\" align=\"center\"> </p>\r\n					<center>\r\n<center>\r\n<table cellSpacing=\"0\" cellPadding=\"0\" width=\"650\" border=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#111111\">\r\n<tr>\r\n<td>\r\n\r\n<align=\"center\">\r\n\r\n<font face=\"Tahoma\">\r\n<p align=\"center\">\r\n<font face=\"Georgia\" color=\"Red\" size=\"5\"><b>Exclusive Upgrade Offer Today Only!<br></b></font>\r\n<font size=\"6\" face=\"Georgia\">\r\n<b>\"65% Discount on a full one year pro upgrade to #SITENAME# PLUS 1000 credits \r\ndeposited instantly into your account...\"</b></font></p>\r\n<blockquote>\r\n  <blockquote>\r\n    <p align=\"center\"><font size=\"4\" face=\"Georgia\"><b><i>Get More Traffic</i> To Your Site. \r\n    Learn How Now...</b></font></p>\r\n  </blockquote>\r\n</blockquote>\r\n<center> \r\n<p><font face=\"Georgia\"><br>\r\n</font>\r\n<font face=\"Georgia\" size=\"4\"><b>Do Not Close This Page! Please Take 30 Seconds And Read The Following...</b></font><font face=\"Georgia\">\r\n<br><br>\r\n</font>\r\n</p>\r\n</center>\r\n<p style=\"margin-left: 15px; margin-right: 15px\" align=\"justify\">\r\n<b>[firstname]</b><font face=\"Georgia\"> Before you move on to the members area to try out your new \r\n</font><span style=\"background-color: #FFFF00\">\r\n<align=\"center\">\r\n<strong><font color=\"#ce0423\">\r\n#SITENAME#</font></strong></span><font face=\"Georgia\"> membership, please take the time to look over this great offer. I have two reasons for showing you this: </font></p>\r\n<p style=\"margin-left: 15px; margin-right: 15px; font-family: Georgia\" align=\"justify\" new=\"\" times=\"\">1) <strong>I want you to model success</strong>. When you start building your list based on my super-profitable model, you must have one time offers for your new members.</p>\r\n<p style=\"margin-left: 15px; margin-right: 15px; font-family: Georgia\" align=\"justify\" new=\"\" times=\"\">2) \r\n<b>I want to give you the opportunity</b> to not only build \r\nhuge amounts of traffic - but be able to set up a complete list building system - and for you to have the tools I use to build my sites to the level of success they now have.</p>\r\n<p style=\"margin-left: 15px; margin-right: 15px; font-family: Georgia\" align=\"justify\" new=\"\" times=\"\"><strong><font color=\"#ce0423\">\r\nFirst and foremost a one Year upgrade in\r\n<span style=\"background-color: #FFFF00\">#SITENAME#</span></font></strong>, \r\nwe do offer a yearly upgrade which you will see when you log in. This currently sells at \r\n<span style=\"background-color: #FFFF00\">\r\n<font color=\"#D2022C\">$97</font></span> \r\na year with unbridled use of all the Upgraded Membership resources. \r\nSuch as...</p>\r\n\r\n<align=\"center\">\r\n<p style=\"margin-left: 15px; margin-right: 15px; font-family: Georgia\" align=\"justify\" new=\"\" times=\"\"><center>\r\n<font size=\"2\" face=\"Georgia\">\r\n<h2>Here is what''s included in your Upgrade</h2>\r\n</font>\r\n<font size=\"2\" face=\"Verdana\">\r\n<table align=\"center\" width=\"592\" border=\"0\">\r\n<tr>\r\n<td width=\"295\">\r\n<p align=\"left\"><font face=\"Georgia\"><b>\r\n� 1000 Credits per Mo.<br>\r\n� List UNLIMITED Websites<br>\r\n� UNLIMITED Banner & Text<br>\r\n� 6 Second Timer<br>\r\n� Surfing Rewards<br>\r\n� Dynamic Surf Ratios<br>\r\n� 40% Residual Income</b></font></p>\r\n</td>\r\n<td width=\"295\">\r\n<p align=\"center\"><font face=\"Georgia\" size=\"3\"><b>\r\n<span style=\"background-color: #FFFF00\">What does this mean for you?</span></b><span style=\"background-color: #FFFF00\"><BR>\r\n</span></font>\r\n</p>\r\n<ol>\r\n  <li><font face=\"Georgia\" size=\"4\">Higher commissions</font></li>\r\n  <li><font face=\"Georgia\" size=\"4\">Lower timer</font></li>\r\n  <li><font face=\"Georgia\" size=\"4\">More credits</font></li>\r\n  <li><font face=\"Georgia\" size=\"4\">Higher surf rewards</font></li>\r\n</ol>\r\n</td>\r\n</tr>\r\n</table>\r\n</font>\r\n</center>\r\n<p>\r\n<font face=\"Georgia\"> \r\n<br>\r\n</font><span style=\"FONT-SIZE: 13.5pt\"><b><span style=\"FONT-SIZE: 20pt\">\r\n<font color=\"#cc0000\">Just Look At Everything You Are Getting...</font></span></b></p>\r\n<p align=\"left\">\r\n<font size=\"5\"><b>Save A Massive Amount Of Surfing Time...</b></font><font size=\"4\"><b> \r\nas an upgraded member you will earn more credits, faster than on \r\n99% of the other traffic exchanges.</b></font></p>\r\n<p align=\"left\"><font size=\"5\"><b>Earn Massive Affiliate Commissions...</b></font><font size=\"4\"><b> \r\nif you recommend </b></font>\r\n<align=\"center\">\r\n<strong><font color=\"#ce0423\">\r\n<span style=\"background-color: #FFFF00\">#SITENAME#</span></font></strong><font size=\"4\"><b> to others I want to reward you \r\nin a big way!</b></font></p>\r\n<p align=\"left\"><font size=\"5\"><b>Get A Ton Of Free Bonus Advertising...</b></font><font size=\"4\"><b> \r\nmonth after month like clock work</b></font></p>\r\n</span>\r\n<p align=\"center\"><font face=\"Georgia\" size=\"4\">\r\n<span style=\"font-weight: 700; background-color: #FFFF00\">PLUS... THIS EXCLUSIVE \r\nSUPER BONUS</span></p>\r\n</font>\r\n</p>\r\n</font>\r\n\r\n<align=\"center\">\r\n\r\n<align=\"center\">\r\n\r\n<font face=\"Tahoma\">\r\n<p style=\"margin-left: 15px; margin-right: 15px; font-family: Georgia\" align=\"justify\" new=\"\" times=\"\">\r\n<strong>You also get Launch \r\nFormula Marketing...</strong></p>\r\n<p style=\"margin-left: 15px; margin-right: 15px; font-family: Georgia\" align=\"justify\" new=\"\" times=\"\">\r\n<strong>I acquired the rights to give you the lite version of LFM that sells for \r\n$97 absolutely free with this yearly upgrade purchase</strong></p>\r\n</font>\r\n\r\n<div align=\"center\">\r\n<center>\r\n\r\n<table style=\"border-collapse: collapse\" bordercolor=\"#c0c0c0\" cellspacing=\"5\" cellpadding=\"10\" width=\"90%\" border=\"1\" height=\"620\">\r\n<tr>\r\n<align=\"center\">\r\n\r\n<font face=\"Tahoma\">\r\n<td width=\"667\" height=\"371\">\r\n<h3>\r\n</h3>\r\n<h3 style=\"font-family: Georgia; margin-left: 5px; margin-right: 5px\" align=\"center\">\r\n<font color=\"#CE0423\">Membership software to help you build your first \r\nmembership site.</font></h3>\r\n<h3></h3>\r\n<p style=\"margin-left: 5px; margin-right: 5px\" align=\"justify\"><font face=\"Georgia\">\r\n<img border=\"0\" src=\"http://launchformulamarketing.com/images/LFM_box_min.jpg\" align=\"left\" width=\"197\" height=\"200\"> \r\nThe lite version of LFM will allow you to put up \r\nunlimited websites and include your own one time offers.</font></p>\r\n<p style=\"margin-left: 5px; margin-right: 5px\" align=\"justify\">\r\n<font face=\"Georgia\">While its only the lite version and \r\ndoesn''t come with all the bells and whistles of the full \r\nversion never the less its still a very powerful script \r\nand has never been offered in this way before.</font></p>\r\n<p style=\"margin-left: 5px; margin-right: 5px\" align=\"justify\">\r\n<font face=\"Georgia\">No where in the world right now can \r\nyou get this ground breaking software with out paying \r\nfor it. This is truly a one time offer on its own, don''t \r\npass this by<br />\r\n<br />\r\n<strong>Normally Priced at $97.00 </strong></font></p>\r\n<p style=\"margin-left: 5px; margin-right: 5px\" align=\"justify\"> </p>\r\n</td>\r\n</font>\r\n\r\n</tr>\r\n<tr>\r\n<align=\"center\">\r\n\r\n<font face=\"Georgia\" color=\"#CE0423\">\r\n<td width=\"667\" height=\"198\">\r\n<h3 align=\"center\"><strong>One Year </strong> \r\n</font>\r\n\r\n<font face=\"Tahoma\">\r\n<strong>\r\n<font color=\"#ce0423\" face=\"Georgia\">Upgrade In </font>\r\n</strong></font>\r\n<align=\"center\">\r\n\r\n<font face=\"Tahoma\">\r\n<align=\"center\">\r\n<span style=\"background-color: #FFFF00\">\r\n<align=\"center\">\r\n<strong><font color=\"#ce0423\">\r\n#SITENAME#</font></strong></span></font><font face=\"Tahoma\"></h3>\r\n<div align=\"center\" style=\"width: 525; height: 179\">\r\n<align=\"center\">\r\n<p align=\"justify\">\r\n\r\n<font face=\"Georgia\" color=\"black\">\r\n\r\nAnd we''re not just saying this upgrade is \r\nvalued packed, we''re backing it up! 1000 page \r\nviews dropped straight into your account... \r\nSurf ratio? Free members get 3:1, \r\nPro members get 1:1 !</font></p>\r\n<p align=\"justify\">\r\n\r\n<font face=\"Georgia\" color=\"black\">\r\n\r\nThere are so many other goodies attached to this upgrade but we know you want to get started right now. </font></p>\r\n</div>\r\n</td>\r\n</font>\r\n\r\n</tr>\r\n</table>\r\n</center>\r\n</div>\r\n\r\n<align=\"center\">\r\n\r\n<font face=\"Georgia\">\r\n<h3 align=\"center\">Get all this for a 65% discount Plus the very \r\nspecial gift of the LFM Lite software, normal cost for all of the \r\nabove including LFM would normally be\r\n</font>\r\n\r\n<strong><span style=\"background-color: #FFFF00\">\r\n\r\n<font face=\"Tahoma\" color=\"#CE0423\">\r\n$194.00</font></span></strong><align=\"center\"><font face=\"Georgia\"><align=\"center\">\r\n\r\n</h3>\r\n<h3 align=\"center\">If your one of those quick thinkers you can snap \r\nit all up now for just... \r\n</font>\r\n\r\n<align=\"center\">\r\n<strong><span style=\"background-color: #FFFF00\">\r\n\r\n<font face=\"Tahoma\" color=\"#CE0423\">\r\n69.70</font></span></strong><align=\"center\"><font face=\"Georgia\"><align=\"center\"><b><sup><u>.00 a year \r\nsubscription </u></sup></b>\r\n</font>\r\n\r\n</h3>\r\n<font face=\"Tahoma\"> \r\n<p align=\"center\"><font size=\"3\" face=\"Georgia\"><b>Sign Up For This Amazing One \r\nTime Offer By Clicking The Secure Order Link Below.</b> </font></p>\r\n<div align=\"center\">\r\n  <table style=\"BORDER-COLLAPSE: collapse\" id=\"table30\"  cellSpacing=\"1\" cellPadding=\"7\" width=\"85%\" bgColor=\"#ffffff\">\r\n    <tr>\r\n      <td style=\"BORDER-BOTTOM: rgb(179,0,0) 4px dashed; BORDER-LEFT: rgb(179,0,0) 4px dashed; BORDER-TOP: rgb(179,0,0) 4px dashed; BORDER-RIGHT: rgb(179,0,0) 4px dashed\"  bgColor=\"#ffffff\">\r\n      <p align=\"justify\">\r\n      <font face=\"Georgia\" size=\"4\"><span class=\"style2\"><font color=\"#ff0000\">Yes, \r\n       \r\n      </font> I want to take advantage of this insane offer and get my hands \r\n      on the 1 Year Upgrade Today.</span></font></p>\r\n      <p align=\"justify\">\r\n      <font face=\"Georgia\" size=\"4\"><span class=\"style2\">I also understand that by \r\n      clicking the link below I will be saving a massive 65%, plus get the Lite \r\n      version of launch Formula marketing membership script</span></font></p>\r\n      <p><font face=\"Georgia\"><span class=\"style2\"><b>Click Here To Order Your \r\n      Upgrade Today!</b></span></font></p>\r\n      <center>\r\n<font face=\"Tahoma\"> \r\n\r\n<p align=\"center\">\r\n<font face=\"Georgia\">\r\n<span style=\"background-color: #FFFF00\"><b>$69.70 per year subscription</b></span></p>\r\n      </font>\r\n<font size=\"2\" face=\"Georgia\">\r\n<div align=\"center\"> [paymentcode] </div> \r\n\r\n      </font>\r\n      </center></td>\r\n    </tr>\r\n  </table>\r\n</div>\r\n<p align=\"center\"> </p>\r\n<font face=\"Georgia\"> \r\n<p align=\"justify\"><font size=\"3\"><b><span style=\"COLOR: red\">You Must Act Now</span> \r\n- before it''s too LATE! <u>If you leave this page and attempt to come back</u>, \r\nthe $69.70 One-Time-Offer will be gone forever!</b></font></p>\r\n<center>\r\n<p align=\"justify\"><font size=\"3\"><b>Your IP is registered and it is not \r\npossible to get access to this offer again. I can only guarantee that it is \r\nstill available right this <i>very second</i>. Don''t delay!</b></font></p>\r\n</font>\r\n<font face=\"Tahoma\"> \r\n<center>\r\n<font face=\"Georgia\">\r\n\r\n<p align=\"left\">Thank you again for choosing \r\n<span style=\"background-color: #FFFF00\">#SITENAME#</span> for your traffic needs.</p>\r\n</font>\r\n<font size=\"2\" face=\"Georgia\">\r\n<p align=\"left\">\r\n<align=\"center\">\r\n\r\n<align=\"center\">\r\n\r\n<align=\"center\">\r\n\r\n<align=\"center\">\r\n\r\n<align=\"center\">\r\n\r\n<align=\"center\">\r\n\r\n<align=\"center\">\r\n<align=\"center\">\r\n<font face=\"Tahoma\"> \r\n<font face=\"Georgia\">\r\n\r\n<span style=\"background-color: #FFFF00\">#SITENAME#</span></font></font><strong> \r\nAdmin</strong></p></font>\r\n<font size=\"2\" face=\"Georgia\">\r\n<p align=\"center\">  </p>\r\n<div align=\"center\">\r\n  <center>\r\n  <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse\" width=\"95%\" bordercolor=\"#C0C0C0\" bgcolor=\"#C0C0C0\">\r\n    <tr>\r\n      <td width=\"100%\">\r\n\r\n<align=\"center\">\r\n\r\n      <align=\"center\">\r\n<font face=\"Tahoma\"> \r\n<align=\"center\">\r\n\r\n      <p style=\"margin-left: 30; margin-right: 30\" align=\"center\">\r\n\r\n<align=\"center\">\r\n\r\n<a href=\"showoto.php\" style=\"margin: 10px; font-weight:700\"><font size=\"3\">No thanks, I''m quite happy surfing for free, and taking advantage of your standard service. I understand I won''t see this offer again but that''s OK, I''ll get there. I''ll just take a little longer than those other guys.</font></a> \r\n      </td>\r\n    </tr>\r\n  </table>\r\n  </center>\r\n</div>\r\n</font>\r\n<align=\"center\">\r\n\r\n<p> \r\n\r\n<p> \r\n\r\n<p> </td>\r\n</tr>\r\n</table>\r\n</center>\r\n<p><b> </b></p>\r\n<b> </b></center><b></b>				    </td>\r\n                </tr>\r\n              </tbody></table>\r\n              </td>\r\n            </tr>\r\n          </tbody></table>\r\n        </div>\r\n      </td>\r\n    </tr>\r\n  </tbody></table>\r\n  <img src=\"".$domainurl."/images/oto/footer.jpg\"  width=\"700\" height=\"231\"></div>";

@mysql_query("INSERT INTO `".$prefix."oto_offers` (`id`, `offername`, `groupid`, `rank`, `ipn`, `text`, `timesshown`, `timesbought`, `sales`) VALUES
(1, 'one year LFM', 1, 1, 3, '".$ototext1."', 0, 0, '0.00')");


$ototext2 = "<title>One Time Offer</title>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n<link href=\"../template/admin/style.css\"  rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style type=\"text/css\">\r\nbody\r\n{ \r\nbackground-color:#000000; \r\n\r\n}\r\n.outermosttableborder {\r\n	border: thin solid #104571;\r\n}\r\n.red {\r\n	color:#ff0000;\r\n	}\r\n</style>\r\n\r\n\r\n<div align=\"center\">\r\n<table  width=\"700\" background=\"".$domainurl."/images/oto/tableback.jpg\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n    <tbody><tr>\r\n      <td >\r\n        <div align=\"center\"><img src=\"".$domainurl."/images/oto/header.jpg\"  style=\"margin: 0pt; padding: 0pt;\" width=\"700\" height=\"268\"> \r\n            <table  width=\"700\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n            <tbody><tr>\r\n              <td > <table  width=\"92%\" align=\"center\" border=\"0\">\r\n                <tbody><tr>\r\n                  <td >\r\n       \r\n\r\n\r\n				   \r\n			\r\n\r\n <p style=\"margin-top: 0pt; margin-bottom: 0pt;\" align=\"center\"> </p>\r\n					<center>\r\n<style>\r\n<!--\r\n.style2 {font-size: 13.5pt}\r\n-->\r\n</style>\r\n</head>\r\n\r\n<body>\r\n\r\n<div align=\"center\">\r\n  <center>\r\n        <table cellSpacing=\"0\" cellPadding=\"0\" width=\"650\" border=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#111111\">\r\n          <tr>\r\n            <td>\r\n                      <align=\"center\">\r\n\r\n<font face=\"Verdana\" color=\"black\">\r\n                                  <center>\r\n\r\n      </font>\r\n                      <align=\"center\">\r\n\r\n<font face=\"Verdana\" size=\"5\" color=\"black\">\r\n<P style=\"MARGIN-LEFT: 15px; MARGIN-RIGHT: 15px\" align=center>\r\n                      <font face=\"Georgia\" color=\"#ce0423\" size=\"5\">WOW [firstname] I was \r\n                      really proud of that offer, I considered that the \r\n                      proverbial Irresistible offer </font>\r\n<font face=\"Georgia\" color=\"#ce0423\">But...</font><SPAN style=\"FONT-FAMILY: Georgia; font-weight:400\">Sometimes I know that no matter how great the offer \r\nprice is a real barrier at times. I know I have had to pass on great offers because I just didn''t have the means right there and then...</SPAN></font><align=\"center\"><font face=\"Georgia\" color=\"black\"><p align=\"center\" style=\"margin-left: 10; margin-right: 10\">\r\n                      <b>So lets make this a 3 month \r\n                      subscription instead of a year</b></p>\r\n                      <p style=\"margin-left: 10; margin-right: 10\">\r\n\r\n                      <align=\"center\">\r\n\r\n<font face=\"Georgia\" size=\"2\" color=\"black\">\r\n\r\n                      <font size=\"4\">And we drop the special bonus of \r\n           LFM Lite and reduce the price down to a fraction of the cost... Lets \r\n           say <br>\r\n           </font><br>\r\n                      </font>\r\n\r\n<font face=\"Georgia\" size=\"4\" color=\"black\">\r\n\r\n                      $19.97 quarterly subscription </font>\r\n\r\n<font face=\"Georgia\" size=\"2\" color=\"black\">\r\n\r\n                      (Payment taken every 3 months)</font></p>\r\n                      <p style=\"margin-left: 10; margin-right: 10\">\r\n\r\n                      <align=\"center\">\r\n\r\n                      You still get all these great features</p>\r\n                      </font>\r\n\r\n                      <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n\r\n<table align=\"center\" width=\"592\" border=\"0\">\r\n<tr>\r\n\r\n<align=\"center\">\r\n\r\n<font face=\"Tahoma\">\r\n<align=\"center\">\r\n<font size=\"2\" face=\"Verdana\">\r\n<td width=\"295\">\r\n<p align=\"left\"><font face=\"Georgia\"><b>\r\n� 1000 Credits per Mo.<br>\r\n� List UNLIMITED Websites<br>\r\n� UNLIMITED Banner & Text<br>\r\n� 6 Second Timer<br>\r\n� Surfing Rewards<br>\r\n� Dynamic Surf Ratios<br>\r\n� 40% Residual Income</b></font></p>\r\n</td>\r\n</font>\r\n</font>\r\n\r\n<align=\"center\">\r\n\r\n<font face=\"Tahoma\">\r\n<align=\"center\">\r\n<font size=\"2\" face=\"Verdana\">\r\n<td width=\"295\">\r\n<p align=\"center\"><font face=\"Georgia\" size=\"3\"><b>\r\n<span style=\"background-color: #FFFF00\">What does this mean for you?</span></b><span style=\"background-color: #FFFF00\"><BR>\r\n</span></font>\r\n</p>\r\n<ol>\r\n  <li><font face=\"Georgia\" size=\"4\">Higher commissions</font></li>\r\n  <li><font face=\"Georgia\" size=\"4\">Lower timer</font></li>\r\n  <li><font face=\"Georgia\" size=\"4\">More credits</font></li>\r\n  <li><font face=\"Georgia\" size=\"4\">Higher surf rewards</font></li>\r\n</ol>\r\n</td>\r\n</font>\r\n</font>\r\n\r\n</tr>\r\n</table>\r\n                      <align=\"center\">\r\n                      <font face=\"Georgia\" size=\"2\" color=\"black\">\r\n        </center><span style=\"font-size: 13.5pt\">\r\n            <p align=\"justify\">Please understand that we can''t offer the Pro membership at such a \r\n            low price on a regular basis... This is your chance to grab it at a \r\n            massive discount!<BR><BR></p>\r\n            </span></font>\r\n                                  <font face=\"Tahoma\" size=\"2\" color=\"black\">\r\n        </font>\r\n            <p> </p>\r\n            <div align=\"center\">\r\n              <center><font face=\"Verdana\" color=\"black\" size=\"2\">\r\n              <table style=\"border:2px dashed #CF331D; border-collapse: collapse\" borderColor=\"#111111\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"2\">\r\n                <tr>\r\n                  <td width=\"100%\"><span style=\"font-size: 13.5pt\">\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b><font color=\"#ff0000\">Yes!</font>\r\n                  I want to take advantage of this Special offer and \r\n                  get my hands on the Special Pro Upgrade Today.</b></span> </p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b>I also understand that by clicking the \r\n                  link below I will be saving a massive 40% </b></span></p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b>Order Your Pro Membership Today!</b></span></p>\r\n                  <p align=\"center\" style=\"margin-left: 10; margin-right: 10\">\r\n\r\n                      <align=\"center\">\r\n\r\n<font size=\"4\" face=\"Tahoma\" color=\"black\">A pro upgrade</font></span><align=\"center\"><font face=\"Tahoma\" color=\"black\" size=\"4\"> \r\n                      is normally $29.91 every 3 months</font><span style=\"font-size: 13.5pt\"><font face=\"Tahoma\" size=\"2\" color=\"black\"><font face=\"Tahoma\" color=\"black\" size=\"4\"><br>\r\n        </font>\r\n          <font color=\"#FF0000\" size=\"4\">$19.97 is a 35% reduction for \r\nacting right now<br>\r\n           </font>\r\n        </font></span>\r\n                      <align=\"center\">\r\n\r\n                  [paymentcode] </p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <font size=\"4\"><span style=\"font-size: 13.5pt\"><b>P.S. After \r\n                  ordering your account will be instantly upgraded via our state \r\n                  of the art IPN system.</b></span></font></p>\r\n                  <p align=\"center\" style=\"margin-left: 10; margin-right: 10\">\r\n                   </td>\r\n                </tr>\r\n              </table>\r\n              </font></center>\r\n            </div>\r\n            <p align=\"justify\"><font face=\"Verdana\" color=\"black\" size=\"2\"><br>\r\n </font></p>\r\n            <p align=\"center\"><font color=\"#9a0000\" size=\"6\" face=\"Verdana\"><b>\r\n            <font size=\"3\">No thanks. </font></b>\r\n            <a style=\"font-weight: 700; margin: 10px\" href=\"showoto.php\">\r\n            <font size=\"3\">I am ready to pass on this.</font></a></font></p>\r\n            <p> </td>\r\n          </tr>\r\n        </table>\r\n      </center>\r\n<p><b> </b></p>\r\n<b> </b></center><b></b>				    </td>\r\n                </tr>\r\n              </tbody></table>\r\n              </td>\r\n            </tr>\r\n          </tbody></table>\r\n        </div>\r\n      </td>\r\n    </tr>\r\n  </tbody></table>\r\n  <img src=\"".$domainurl."/images/oto/footer.jpg\" width=\"700\" height=\"231\"></div>\r\n";

@mysql_query("INSERT INTO `".$prefix."oto_offers` (`id`, `offername`, `groupid`, `rank`, `ipn`, `text`, `timesshown`, `timesbought`, `sales`) VALUES
(2, 'quarterly', 1, 2, 11, '".$ototext2."', 1, 0, '0.00')");


$spotext1 = "\r\n<title>One Time Offer</title>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n<link href=\"../template/admin/style.css\"  rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style type=\"text/css\">\r\nbody\r\n{ \r\nbackground-color:#000000; \r\n\r\n}\r\n.outermosttableborder {\r\n	border: thin solid #104571;\r\n}\r\n.red {\r\n	color:#ff0000;\r\n	}\r\n</style>\r\n\r\n\r\n<div align=\"center\">\r\n<table  width=\"700\" background=\"".$domainurl."/images/oto/tableback.jpg\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n    <tbody><tr>\r\n      <td >\r\n        <div align=\"center\"><img src=\"".$domainurl."/images/oto/header.jpg\"  style=\"margin: 0pt; padding: 0pt;\" width=\"700\" height=\"268\"> \r\n            <table  width=\"700\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n            <tbody><tr>\r\n              <td >&nbsp;<table  width=\"92%\" align=\"center\" border=\"0\">\r\n                <tbody><tr>\r\n                  <td >\r\n       \r\n\r\n\r\n				   \r\n			\r\n\r\n <p style=\"margin-top: 0pt; margin-bottom: 0pt;\" align=\"center\">&nbsp;</p>\r\n					<center>\r\n       <span style=\"font-size: 13.5pt\">\r\n                      <font face=\"Tahoma\">\r\n                      <align=\"center\">\r\n        <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n        Our new credit subscriptions have proven to be a big hit... not a \r\n        surprise really, who wouldn''t want to save big, surf less and have \r\n        credits delivered every month, like clockwork?</p>\r\n        <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n        <font size=\"3\">Yes that''s right, with one click of a button you can save \r\n        yourself hours of time everyday by never having to surf or even log into \r\n        your account. 5000 credits every month added to \r\n        your account, means a hands off marketing strategy.</font></p>\r\n        </font>\r\n            <p align=\"justify\">Please understand that we can''t offer this credit \r\n            package at such a \r\n            low price on a regular basis... This is your chance to grab it at a \r\n            massive discount!</p>\r\n            </span><font face=\"Tahoma\">\r\n\r\n\r\n            <p> </p>\r\n            <div align=\"center\">\r\n              <center><font face=\"Verdana\" color=\"black\" size=\"2\">\r\n              <table style=\"border-collapse: collapse\" borderColor=\"#111111\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"2\">\r\n                <tr>\r\n                  <td width=\"100%\"><span style=\"font-size: 13.5pt\">\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b><font color=\"#ff0000\">Yes!</font> \r\n                  I want to take advantage of this Special offer and \r\n                  get my hands on the Special 5000 credit package Today.</b></span> </p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b>I also understand that by clicking the \r\n                  link below I will be saving a massive amount of time and money. </b></span></p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b>Order Your 5000 Credit Subscription Today!</b></span></p>\r\n                  </span>\r\n              </font>\r\n                  <p align=\"center\" style=\"margin-left: 10; margin-right: 10\">\r\n                      <font face=\"Tahoma\" color=\"black\" size=\"4\">\r\n                      500</font><font face=\"Verdana\" color=\"black\" size=\"2\"><align=\"center\"><font face=\"Tahoma\" color=\"black\" size=\"4\">0 \r\n                      credits are normally priced at $35.00</font><span style=\"font-size: 13.5pt\"><font face=\"Tahoma\" size=\"2\" color=\"black\"><font size=\"4\"> </font>\r\n                  <font face=\"Tahoma\" color=\"black\" size=\"4\">\r\n          <br>\r\n        </font>\r\n          <font color=\"#FF0000\" size=\"4\">$24.85 per month is a massive reduction for \r\nacting right now<br>\r\n           </font>\r\n        </font></span>\r\n                      <align=\"center\">\r\n\r\n                  [paymentcode] </p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <font size=\"4\"><span style=\"font-size: 13.5pt\"><b>P.S. After \r\n                  ordering your account will be instantly upgraded via our state \r\n                  of the art IPN system.</b></span></font></p>\r\n                  </td>\r\n                </tr>\r\n              </table>\r\n              </font></center>\r\n            </div>\r\n            <p align=\"justify\"><font face=\"Verdana\" color=\"black\" size=\"2\"><br>\r\n </font></p>\r\n                      <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n            <p align=\"center\"><font color=\"#9a0000\" size=\"6\" face=\"Verdana\"><b>\r\n            <font size=\"3\">No thanks. </font></b>\r\n            <a style=\"font-weight: 700; margin: 10px\" href=\"showoto.php\">\r\n            <font size=\"3\">I am ready to pass on this.</font></a></font></p>\r\n</div>\r\n\r\n<p><b>&nbsp;</b></p>\r\n<b> </b></center><b></b>				    </td>\r\n                </tr>\r\n              </tbody></table>\r\n              </td>\r\n            </tr>\r\n          </tbody></table>\r\n        </div>\r\n      </td>\r\n    </tr>\r\n  </tbody></table>\r\n  <img src=\"".$domainurl."/images/oto/footer.jpg\"  width=\"700\" height=\"231\"></div>\r\n";

@mysql_query("INSERT INTO `".$prefix."spo` (`id`, `offername`, `rank`, `ipn`, `acctype`, `memtype`, `reptype`, `mdays`, `paused`, `text`) VALUES
(1, '5k Credit Offer', 1, 8, 1, 0, 5, 20, 0, '".$spotext1."')");


$spotext2 = "\r\n<title>One Time Offer</title>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n<link href=\"../template/admin/style.css\"  rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style type=\"text/css\">\r\nbody\r\n{ \r\nbackground-color:#000000; \r\n\r\n}\r\n.outermosttableborder {\r\n	border: thin solid #104571;\r\n}\r\n.red {\r\n	color:#ff0000;\r\n	}\r\n</style>\r\n\r\n\r\n<div align=\"center\">\r\n<table width=\"700\" background=\"".$domainurl."/images/oto/tableback.jpg\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n    <tbody><tr>\r\n      <td>\r\n        <div align=\"center\"><img src=\"".$domainurl."/images/oto/header.jpg\"  style=\"margin: 0pt; padding: 0pt;\" width=\"700\" height=\"268\"> \r\n            <table width=\"700\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n            <tbody><tr>\r\n              <td>&nbsp;<table width=\"92%\" align=\"center\" border=\"0\">\r\n                <tbody><tr>\r\n                  <td>\r\n       \r\n\r\n\r\n				   \r\n			\r\n\r\n <p style=\"margin-top: 0pt; margin-bottom: 0pt;\" align=\"center\">&nbsp;</p>\r\n					<center>\r\n       <style>\r\n<!--\r\n.style2 {font-size: 13.5pt}\r\n-->\r\n       </style>\r\n       <span style=\"font-size: 13.5pt\">\r\n                      <font face=\"Tahoma\">\r\n                      <align=\"center\">\r\n                      <p align=\"center\" style=\"margin-left: 10; margin-right: 20\">\r\n        <align=\"center\">\r\n\r\n<font face=\"Verdana\" size=\"5\" color=\"black\">\r\n      <img src=\"images/attention.jpg\" width=\"143\" height=\"29\"></font><align=\"center\"><font face=\"Georgia\" color=\"#CE0423\" size=\"5\">[firstname] <BR />Daily Deal \r\n        1000 Credits at \r\n        Half Price</font><p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n        Our new credit subscriptions have proven to be a big hit... not a \r\n        surprise really, who wouldn''t want to save big, surf less and have \r\n        credits delivered every month, like clockwork?</p>\r\n        <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n        <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n\r\n<font face=\"Tahoma\" size=\"4\" color=\"black\">\r\n        However Some people have asked if we could offer \r\n        <span style=\"background-color: #FFFF00\">single purchases</span> at the \r\n        discount rate, and in a spirit of fairness we have decided to do so... \r\n        Every day you will see this opportunity to purchase a single package \r\n        of credits worth $1<align=\"center\">0.00 for the price of $5.00... It will be a single \r\n        purchase and not a subscription</font></p>\r\n        </font>\r\n            <p align=\"justify\"> </p>\r\n            </span><font face=\"Tahoma\">\r\n\r\n\r\n            <p> </p>\r\n            <div align=\"center\">\r\n              <center><font face=\"Verdana\" color=\"black\" size=\"2\">\r\n              <table style=\"border-collapse: collapse\" borderColor=\"#111111\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"2\">\r\n                <tr>\r\n                  <td width=\"100%\">\r\n        <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n                  <align=\"center\"><align=\"center\">\r\n                      <align=\"center\">\r\n                  <font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                  <span style=\"font-size: 13.5pt\">\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b><font color=\"#ff0000\">Yes!</font> I want to take advantage of this Special offer and \r\n                  get my hands on the Special 1000 credit package Daily Deal.</b></span> </p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span style=\"background-color: #FFFF00\">I Understand this is a one time payment and I will not be \r\n                  charged again, unless I choose to purchase the \r\n                  package again.</span></p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b>I also understand that by clicking the \r\n                  link below I will be saving a massive 50% </b></span></p>\r\n                  </span>\r\n              </font>\r\n              </font>\r\n              </font>\r\n                  <p align=\"center\" style=\"margin-left: 10; margin-right: 10\">\r\n                      <align=\"center\"><font face=\"Tahoma\" color=\"black\" size=\"4\">\r\n                      100</font><align=\"center\"><font face=\"Tahoma\" color=\"black\" size=\"4\">0 \r\n                      credits are normally priced at $</font></font></font><font color=\"black\" size=\"4\">10.00</font><font face=\"Verdana\" color=\"black\" size=\"2\"><span style=\"font-size: 13.5pt\"><font face=\"Tahoma\" size=\"2\" color=\"black\"><font size=\"4\"> </font>\r\n                  <font face=\"Tahoma\" color=\"black\" size=\"4\">\r\n          <br>\r\n        </font>\r\n          <font color=\"#FF0000\" size=\"4\">$5.00 is a 50% reduction for \r\nacting right now<br>\r\n           </font>\r\n        </font></span>\r\n                      <align=\"center\">\r\n\r\n                  [paymentcode] </p>\r\n                  <font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                      <align=\"center\">\r\n\r\n                  <align=\"center\">\r\n\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <font size=\"4\"><span style=\"font-size: 13.5pt\"><b>P.S. After \r\n                  ordering your account will be instantly upgraded via our state \r\n                  of the art IPN system.</b></span></font></p>\r\n                  </font>\r\n                  <font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                  </td>\r\n                </tr>\r\n              </table>\r\n              </font></center>\r\n            </div>\r\n            <p align=\"justify\"><font face=\"Verdana\" color=\"black\" size=\"2\"><br>\r\n </font></p>\r\n                      <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n            <p align=\"center\"><font color=\"#9a0000\" size=\"6\" face=\"Verdana\"><b>\r\n            <font size=\"3\">No thanks. </font></b>\r\n            <a style=\"font-weight: 700; margin: 10px\" href=\"showoto.php\">\r\n            <font size=\"3\">I am ready to pass on this.</font></a></font></p>\r\n\r\n<p><b>&nbsp;</b></p>\r\n<b> </b></center><b></b>				    </td>\r\n                </tr>\r\n              </tbody></table>\r\n              </td>\r\n            </tr>\r\n          </tbody></table>\r\n        </div>\r\n      </td>\r\n    </tr>\r\n  </tbody></table>\r\n  <img src=\"".$domainurl."/images/oto/footer.jpg\"  width=\"700\" height=\"231\"></div>\r\n";

@mysql_query("INSERT INTO `".$prefix."spo` (`id`, `offername`, `rank`, `ipn`, `acctype`, `memtype`, `reptype`, `mdays`, `paused`, `text`) VALUES
(2, 'weekly deal', 2, 9, 0, 0, 3, 2, 0, '".$spotext2."')");


$spotext3 = "\r\n<title>One Time Offer</title>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\r\n<link href=\"../template/admin/style.css\"  rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style type=\"text/css\">\r\nbody\r\n{ \r\nbackground-color:#000000; \r\n\r\n}\r\n.outermosttableborder {\r\n	border: thin solid #104571;\r\n}\r\n.red {\r\n	color:#ff0000;\r\n	}\r\n</style>\r\n\r\n\r\n<div align=\"center\">\r\n<table width=\"700\" background=\"".$domainurl."/images/oto/tableback.jpg\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n    <tbody><tr>\r\n      <td >\r\n        <div align=\"center\"><img src=\"".$domainurl."/images/oto/header.jpg\"  style=\"margin: 0pt; padding: 0pt;\" width=\"700\" height=\"268\"> \r\n            <table width=\"700\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n            <tbody><tr>\r\n              <td >&nbsp;<table width=\"92%\" align=\"center\" border=\"0\">\r\n                <tbody><tr>\r\n                  <td >\r\n       \r\n\r\n\r\n				   \r\n			\r\n\r\n <p style=\"margin-top: 0pt; margin-bottom: 0pt;\" align=\"center\">&nbsp;</p>\r\n					<center>\r\n       <style>\r\n<!--\r\n.style2 {font-size: 13.5pt}\r\n-->\r\n       </style>\r\n       <span style=\"font-size: 13.5pt\">\r\n                      <font face=\"Tahoma\">\r\n                      <align=\"center\">\r\n                      <align=\"center\">\r\n\r\n                                  <center>\r\n\r\n        <p style=\"MARGIN-LEFT: 20px; MARGIN-RIGHT: 20px\" align=\"center\">\r\n        <font size=\"6\"><b>\"WOW [firstname] if you are seeing this page you have been a \r\n        member for quite a while\"</b></font></p>\r\n      <h3 align=\"left\" style=\"margin-left: 15; margin-right: 15\">\r\n      To Thank you for being a loyal member I want to give you a very \r\n      special loyalty bonus</h3>\r\n                <h2><font color=\"#CE0423\">Get 1000 page views every month plus \r\n                an upgraded account with all the extra privileges that pro \r\n                members get like increased surfing ratios for \r\n                half price...</font></h2>\r\n        </center>\r\n        <p align=\"justify\" style=\"margin-left: 15; margin-right: 15\">\r\n        <font size=\"3\">Yes that''s right, with one click of a button you can save \r\n        yourself hours of time everyday by never having to surf.  1000 credits every month added to \r\n        your account, means a hands off marketing strategy.</font></p>\r\n\r\n        <p align=\"justify\" style=\"margin-left: 15; margin-right: 15\">\r\n        1000 page views plus a pro account are normally priced at $19.99 you can save more than 50% \r\n        by taking this monthly subscription for just $9.97\r\n<font size=\"4\"> but you <u><i>must</i> act right now</u> -</font></p>\r\n\r\n        </font>\r\n            <p align=\"justify\"> </p>\r\n            </span><font face=\"Tahoma\">\r\n\r\n\r\n            <p> </p>\r\n            <div align=\"center\">\r\n              <center><font face=\"Verdana\" color=\"black\" size=\"2\">\r\n              <table style=\"border-collapse: collapse\" borderColor=\"#111111\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"2\">\r\n                <tr>\r\n                  <td width=\"100%\">\r\n        <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n                  <align=\"center\"><align=\"center\">\r\n                      <align=\"center\">\r\n                  <font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                  <span style=\"font-size: 13.5pt\">\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b><font color=\"#ff0000\">Yes!</font> I want to take advantage of this Special offer and \r\n                  get my hands on the Special 1000 credit package weekly Deal.</b></span> </p>\r\n                  <p align=\"center\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span style=\"background-color: #FFFF00\">I Understand this is a \r\n                  monthly subscription</span></p>\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <span class=\"style2\"><b>I also understand that by clicking the \r\n                  link below I will be saving a massive 50% </b></span></p>\r\n                  </span>\r\n              </font>\r\n              </font>\r\n              </font>\r\n              </font>\r\n                  <p align=\"center\" style=\"margin-left: 10; margin-right: 10\">\r\n                      <font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                      <align=\"center\"><font face=\"Tahoma\" color=\"black\" size=\"4\">\r\n                      normal price </font>\r\n              </font>\r\n       <span style=\"font-size: 13.5pt\">\r\n                      <font face=\"Tahoma\">\r\n                      <align=\"center\">\r\n                                  $19.99</font></span><font face=\"Verdana\" color=\"black\" size=\"2\"><align=\"center\"><span style=\"font-size: 13.5pt\"><font face=\"Tahoma\" size=\"2\" color=\"black\">\r\n        </font><font face=\"Tahoma\" size=\"2\" color=\"black\">\r\n                  <font face=\"Tahoma\" color=\"black\" size=\"4\">\r\n          <br>\r\n        </font>\r\n          <font color=\"#FF0000\" size=\"4\">$9.97 is a 50% reduction for \r\nacting right now<br>\r\n           </font>\r\n        </font></span>\r\n                      <align=\"center\">\r\n\r\n                  [paymentcode] </p>\r\n                  <font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                      <align=\"center\">\r\n\r\n                  <align=\"center\">\r\n\r\n                  <p align=\"justify\" style=\"margin-left: 10; margin-right: 10\">\r\n                  <font size=\"4\"><span style=\"font-size: 13.5pt\"><b>P.S. After \r\n                  ordering your account will be instantly upgraded via our state \r\n                  of the art IPN system.</b></span></font></p>\r\n                  </font>\r\n                  <font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                  </font><font face=\"Verdana\" color=\"black\" size=\"2\">\r\n                  </td>\r\n                </tr>\r\n              </table>\r\n              </font></center>\r\n            </div>\r\n            <p align=\"justify\"><font face=\"Verdana\" color=\"black\" size=\"2\"><br>\r\n </font></p>\r\n                      <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n\r\n                      <align=\"center\">\r\n            <p align=\"center\"><font color=\"#9a0000\" size=\"6\" face=\"Verdana\"><b>\r\n            <font size=\"3\">No thanks. </font></b>\r\n            <a style=\"font-weight: 700; margin: 10px\" href=\"showoto.php\">\r\n            <font size=\"3\">I am ready to pass on this.</font></a></font></p>\r\n<p><b>&nbsp;</b></p>\r\n<b> </b></center><b></b>				    </td>\r\n                </tr>\r\n              </tbody></table>\r\n              </td>\r\n            </tr>\r\n          </tbody></table>\r\n        </div>\r\n      </td>\r\n    </tr>\r\n  </tbody></table>\r\n  <img src=\"".$domainurl."/images/oto/footer.jpg\"  width=\"700\" height=\"231\"></div>\r\n";

@mysql_query("INSERT INTO `".$prefix."spo` (`id`, `offername`, `rank`, `ipn`, `acctype`, `memtype`, `reptype`, `mdays`, `paused`, `text`) VALUES
(3, 'loyalty offer', 3, 10, 1, 0, 6, 60, 0, '".$spotext3."')");



@mysql_query("INSERT INTO `".$prefix."products` (`productid`, `itemid`, `productname`, `filename`, `price`, `free`, `_2co_id`, `credits`, `memtype`, `prodrank`, `subtype`, `visible`) VALUES
(1, NULL, 'LFM Lite', 'http://www.lfmgiveaway.com/?rid=2968&pc=free2009', 0.00, 0, '', 0, 0, 0, 0, 1),
(2, NULL, 'LFManuscript', 'LFM_Manuscript.pdf', 0.00, 1, '', 0, 0, 0, 0, 1),
(3, NULL, 'All about TE''s', 'AllAboutTrafficExchanges-29.pdf', 0.00, 1, '', 0, 0, 0, 0, 1);");



@mysql_query("INSERT INTO `".$prefix."membertypes` (`accname`, `accfee`, `acclevel`, `comm`, `enabled`, `otocomm`, `promocode`, `template_data`, `recur`, `_2co_productid`, `utemplate_data`, `description`, `commfixed`, `enablepdate`, `pp_p3`, `comm2`, `otocomm2`, `commfixed2`, `pp_p1`, `pp_t1`, `rank`, `pp_srt`, `cb_paylink`, `pp_a1`, `surfratio`, `surftimer`, `refcrds`, `refcrds2`, `hitvalue`, `bannervalue`, `textvalue`, `maxsites`, `maxbanners`, `maxtexts`, `minauto`, `mansites`, `manbanners`, `mantexts`, `monthcrds`) VALUES
('Upgraded Member', 15.00, 0, 40, 1, 40, '', NULL, 'M', '', NULL, '1:1 Surfing Ratio<br>\r\n6 Second Timer<br>\r\n1000 Bonus Credits Per Month<br>\r\n40% Commissions On Referral Purchases', 0.00, 1, 1, 0, 0, 0.00, 0, 'N', 2, 0, '', 0.00, 1, 6, 20, 0, 1, 25, 30, 1000, 1000, 1000, 0, 0, 0, 0, 1000)");



@mysql_query("Update `".$prefix."membertypes` set comm=10, otocomm=20, surfratio=0.33 where mtid=1 limit 1");



@mysql_query("INSERT INTO `".$prefix."dynamic` (`id`, `accid`, `clicks`, `boost`) VALUES
(1, 2, 50, '1.20'),
(2, 2, 100, '1.40'),
(3, 2, 150, '1.60'),
(4, 2, 200, '1.80')");



@mysql_query("UPDATE `".$prefix."membertypes` set template_data='&lt;TABLE cellSpacing=5 cellPadding=5 width=&quot;98%&quot; border=0&gt;\r\n&lt;TR&gt;\r\n&lt;TD colspan=&quot;2&quot; width=&quot;100%&quot;&gt;\r\n&lt;P align=justify&gt;&lt;FONT face=Georgia size=2&gt;Welcome, here''s an important message from the owner...&lt;BR&gt;&lt;BR&gt;Below are three steps that you should follow to start using #SITENAME# and earn some commissions promoting it! &lt;/FONT&gt;&lt;/P&gt;&lt;FONT face=Georgia size=2&gt;&lt;SPAN style=&quot;BACKGROUND-COLOR: #ffff00&quot;&gt;&lt;b&gt;Step 1 -  Visit the Special &lt;a target=&quot;_blank&quot; href=&quot;/bonuspage.php&quot;&gt;Bonus Page&lt;/a&gt;&lt;/b&gt;&lt;/SPAN&gt;&lt;/FONT&gt;&lt;FONT face=Georgia size=2&gt; \r\n&lt;P align=justify&gt;&lt;SPAN style=&quot;BACKGROUND-COLOR: #ffff00&quot;&gt;&lt;b&gt;Step 2- Visit the &lt;a href=&quot;members.php?mf=li&quot;&gt;Affiliate ToolBox&lt;/a&gt;:&lt;/B&gt;&lt;/SPAN&gt; To help you make money by promoting &lt;FONT face=&quot;Arial, Helvetica, sans-serif&quot; size=2&gt;#SITENAME#&lt;/FONT&gt;, I''ve created a ton of great marketing tools that you can find in the affiliate tool box - so you can get started right away, without worrying about creating emails, banners, etc!&lt;BR&gt;&lt;BR&gt;&lt;SPAN style=&quot;BACKGROUND-COLOR: #ffff00&quot;&gt;&lt;B&gt;So go there right now&lt;/B&gt;&lt;/SPAN&gt; and send one of the professional email promotions to everyone you think would want to know about this great tool. Every person you refer will see the One Time Offer (the same one you saw when you joined) and they''ll have the chance to purchase it too. When they purchase the OTO, you''ll earn commissions on the sale ... Just for copying and pasting some of our promotion tools!&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;SPAN style=&quot;BACKGROUND-COLOR: #ffff00&quot;&gt;&lt;b&gt;Step 3 - Get Started With &lt;FONT face=&quot;Arial, Helvetica, sans-serif&quot; size=2&gt;#SITENAME#&lt;/FONT&gt;!&lt;/b&gt;&lt;/SPAN&gt; \r\n&lt;P align=justify&gt;Use the links in the navigation menu to add your URL to the traffic exchange. Don''t forget to add your banners and text links to the system too.&lt;/FONT&gt;&lt;/P&gt;&lt;/td&gt;&lt;/tr&gt;\r\n\r\n&lt;TR&gt;\r\n&lt;TD align=&quot;center&quot; colspan=&quot;2&quot; width=&quot;100%&quot;&gt;\r\n&lt;P align=center&gt;&lt;FONT color=#000080&gt;Recommended resources&lt;/FONT&gt;&lt;/P&gt;&lt;/TD&gt;&lt;/TR&gt;\r\n\r\n&lt;TR&gt;\r\n&lt;TD align=&quot;right&quot;&gt;\r\n&lt;A href=&quot;http://www.instantsqueezepagegenerator.com/index.php?rid=29587&quot; target=_blank mce_href=&quot;http://www.instantsqueezepagegenerator.com/index.php?rid=29587&quot;&gt;&lt;IMG height=125 src=&quot;http://instantsqueezepagegenerator.com/images/banners/125x125.gif&quot; width=125 border=0 mce_src=&quot;http://instantsqueezepagegenerator.com/images/banners/125x125.gif&quot;&gt;&lt;/A&gt;&lt;/TD&gt;\r\n\r\n&lt;TD&gt;&lt;A href=&quot;http://intellibanners.com/?ref=1429&quot; target=_blank mce_href=&quot;http://intellibanners.com/?ref=1429&quot;&gt;&lt;IMG height=125 src=&quot;http://intellibanners.com/banners/banner3.jpg&quot; width=125 border=0 mce_src=&quot;http://intellibanners.com/banners/banner3.jpg&quot;&gt;&lt;/A&gt;&lt;/TD&gt;\r\n\r\n&lt;/TR&gt;&lt;TR&gt;\r\n\r\n&lt;TD align=&quot;right&quot;&gt;&lt;A href=&quot;http://hitsviral.com/?rid=35&quot; target=_blank mce_href=&quot;http://hitsviral.com/?rid=35&quot;&gt;&lt;IMG height=125 src=&quot;http://hitsviral.com/getimg.php?id=2&quot; width=125 border=0 mce_src=&quot;http://hitsviral.com/getimg.php?id=2&quot;&gt;&lt;/A&gt; &lt;/TD&gt;\r\n\r\n&lt;TD&gt;\r\n&lt;A href=&quot;http://www.advertisingknowhow.com/members/traffic.php?referer=herme3&quot; target=_blank mce_href=&quot;http://www.advertisingknowhow.com/members/traffic.php?referer=herme3&quot;&gt;&lt;IMG height=125 src=&quot;http://www.advertisingknowhow.com/members/banners/125x125.gif&quot; width=125 border=0 mce_src=&quot;http://www.advertisingknowhow.com/members/banners/125x125.gif&quot;&gt;&lt;/A&gt;&lt;/TD&gt;\r\n\r\n&lt;/TR&gt;&lt;/TABLE&gt;' where mtid>0");



@mysql_query("UPDATE `".$prefix."templates` set template_data='&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;4&quot; cellspacing=&quot;0&quot;&gt;\r\n  &lt;tr&gt;\r\n    &lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;br&gt;\r\n    &lt;br&gt;\r\n    &lt;p&gt; &lt;/p&gt;\r\n    &lt;p&gt;\r\n&lt;form name=&quot;loginfrm&quot; method=&quot;post&quot; action=&quot;members.php&quot;&gt;\r\n&lt;table width=&quot;230&quot; border=&quot;0&quot; align=&quot;center&quot; cellpadding=&quot;4&quot; cellspacing=&quot;0&quot;&gt;\r\n  &lt;tr bgcolor=&quot;#FBDB79&quot;&gt;\r\n    &lt;td colspan=&quot;2&quot; align=&quot;center&quot; bgcolor=&quot;#D9E6D9&quot;&gt;&lt;strong&gt;Member Login &lt;/strong&gt;&lt;/td&gt;\r\n  &lt;/tr&gt;\r\n  &lt;tr&gt;\r\n    &lt;td width=&quot;30&quot;&gt;&lt;strong&gt;Username:&lt;/strong&gt;&lt;/td&gt;\r\n    &lt;td&gt;&lt;input type=&quot;text&quot; name=&quot;login&quot; size=&quot;20&quot; /&gt;&lt;/td&gt;\r\n  &lt;/tr&gt;\r\n  &lt;tr&gt;\r\n    &lt;td&gt;&lt;strong&gt;Password:&lt;/strong&gt;&lt;/td&gt;\r\n    &lt;td&gt;&lt;input type=&quot;password&quot; name=&quot;password&quot; size=&quot;20&quot; /&gt;&lt;/td&gt;\r\n  &lt;/tr&gt;\r\n  &lt;tr&gt;\r\n    &lt;td colspan=&quot;2&quot; align=&quot;right&quot;&gt;&lt;input type=&quot;submit&quot; name=&quot;Submit&quot; value=&quot;Login&quot; /&gt;&lt;/td&gt;\r\n  &lt;/tr&gt;\r\n  &lt;tr&gt;\r\n    &lt;td colspan=&quot;2&quot; align=&quot;center&quot;&gt;&lt;font size=&quot;2&quot; face=&quot;Verdana, Arial, Helvetica, sans-serif&quot;&gt;&lt;a href=&quot;signup.php&quot;&gt;Register&lt;/a&gt; | &lt;a href=&quot;lostpass.php&quot;&gt;Lost Password&lt;/a&gt; &lt;/font&gt;&lt;/td&gt;\r\n  &lt;/tr&gt;\r\n&lt;/table&gt;\r\n&lt;/form&gt;\r\n&lt;/p&gt;\r\n    &lt;p&gt; &lt;/p&gt;\r\n    &lt;p&gt; &lt;/p&gt;\r\n&lt;BR /&gt;&lt;div align=&quot;center&quot;&gt;\r\n      &lt;center&gt;\r\n      &lt;table border=&quot;0&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse: collapse&quot; bordercolor=&quot;#111111&quot; width=&quot;100%&quot;&gt;\r\n        &lt;tr&gt;\r\n          &lt;td width=&quot;100%&quot;&gt;\r\n          &lt;p align=&quot;center&quot;&gt;&lt;script language=&quot;JavaScript&quot; src=&quot;".$domainurl."/bonuspage/showad.php&quot;&gt;&lt;/script&gt;&lt;/td&gt;\r\n        &lt;/tr&gt;\r\n      &lt;/table&gt;\r\n      &lt;/center&gt;\r\n    &lt;/div&gt;\r\n    &lt;BR /&gt;\r\n    &lt;div align=&quot;center&quot;&gt;\r\n      &lt;center&gt;\r\n      &lt;table border=&quot;0&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; style=&quot;border-collapse: collapse&quot; bordercolor=&quot;#111111&quot; width=&quot;100%&quot;&gt;\r\n        &lt;tr&gt;\r\n          &lt;td width=&quot;100%&quot;&gt;\r\n          &lt;p align=&quot;center&quot;&gt;&lt;script src=&quot;http://lfmads.com/showad/49256695a508a&quot;&gt;&lt;/script&gt;&lt;/td&gt;\r\n        &lt;/tr&gt;\r\n      &lt;/table&gt;\r\n      &lt;/center&gt;\r\n    &lt;/div&gt;\r\n    \r\n    &lt;/td&gt;\r\n  &lt;/tr&gt;\r\n&lt;/table&gt;' where template_name='Homepage Body' limit 1");



@mysql_query("UPDATE `".$prefix."templates` set template_data='&lt;p align=&quot;center&quot;&gt;\r\n								&amp;nbsp;&lt;/p&gt;\r\n							&lt;p align=&quot;center&quot;&gt;\r\n								&lt;span style=&quot;text-transform: capitalize&quot;&gt;&lt;font color=&quot;#c81732&quot; face=&quot;Georgia&quot; size=&quot;5&quot;&gt;&lt;b&gt;If you&amp;#39;re not using Manual Traffic exchanges you&amp;#39;re leaving 100&amp;#39;s and 1000&amp;#39;s of subscribers and Referrals on the table.&lt;/b&gt;&lt;/font&gt;&lt;/span&gt;&lt;/p&gt;\r\n							&lt;p align=&quot;center&quot;&gt;\r\n								&lt;font face=&quot;Georgia&quot; size=&quot;5&quot;&gt;Increase Website Traffic For Free!&lt;/font&gt;&lt;/p&gt;\r\n							&lt;p align=&quot;justify&quot;&gt;\r\n								&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;&lt;font size=&quot;4&quot;&gt;E&lt;/font&gt;very day thousands of new people join traffic exchanges. #SITENAME# is on the cutting edge of TE programs, bringing in new members to view your affiliate and splash pages. Endorsed and promoted by some of the biggest names in internet marketing world, #SITENAME# is destined to be a player in the TE world.&lt;/font&gt;&lt;/p&gt;\r\n							&lt;p align=&quot;justify&quot;&gt;\r\n								&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;The truth is there are probably many things that you look for in deciding on a new exchange from fast and friendly customer service to earning real cash and commissions on upgrades and credit purchases. Traffic exchange advertising is becoming very fast paced and is one of the best promotional tools on the net.&lt;/font&gt;&lt;/p&gt;\r\n							&lt;p align=&quot;justify&quot;&gt;\r\n								&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;Join now and be a part of a system that allows you to earn both traffic and cash from your promotional efforts. &lt;/font&gt;&lt;/p&gt;\r\n							&lt;p align=&quot;justify&quot;&gt;\r\n								&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;With our custom built Click and Surf System you earn Traffic every time you visit another member&amp;#39;s webpage, Plus you earn credits from your own referral&amp;#39;s surfing as well. Each time they surf to earn credits you will automatically earn credits too. The more members you refer, the more credits and commission you earn... plus you&amp;#39;ll increase your website traffic. &lt;/font&gt;&lt;/p&gt;\r\n							&lt;h3 align=&quot;center&quot;&gt;\r\n								&lt;font face=&quot;Georgia&quot;&gt;The result: Free High Quality traffic to your website&lt;/font&gt;&lt;/h3&gt;\r\n							&lt;ul type=&quot;bullet&quot;&gt;\r\n								&lt;li&gt;\r\n									&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;#SITENAME#&lt;b&gt; is FREE&lt;/b&gt;: You just can&amp;rsquo;t beat the appeal of no-cost online advertising. &lt;/font&gt;&lt;/li&gt;\r\n								&lt;li&gt;\r\n									&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;#SITENAME#&lt;b&gt; is Viral&lt;/b&gt;: Traffic increases automatically and exponentially &lt;/font&gt;&lt;/li&gt;\r\n								&lt;li&gt;\r\n									&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;#SITENAME#&lt;b&gt; is a money maker: &lt;/b&gt;Earn REAL CASH COMMISSIONS when your referrals purchase extra credits or services&lt;b&gt;. &lt;/b&gt;&lt;/font&gt;&lt;/li&gt;\r\n													&lt;li&gt;\r\n									&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;#SITENAME#&lt;b&gt; is&lt;/b&gt; &lt;b&gt;Proven&lt;/b&gt;: thousands of members are benefiting from Promoting multiple web pages, Banner advertising&lt;/font&gt;&lt;/li&gt;\r\n								&lt;li&gt;\r\n									&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;#SITENAME#&lt;b&gt; is&lt;/b&gt; &lt;b&gt;Targeted&lt;/b&gt;: You&amp;rsquo;ll only get live, real-time targeted traffic. Real people visiting your site. No fake traffic here. &lt;/font&gt;&lt;/li&gt;\r\n								&lt;li&gt;\r\n									&lt;font face=&quot;Georgia&quot; size=&quot;2&quot;&gt;&lt;b&gt;Downline Builder:&lt;/b&gt; All your signups get to join your programmes through your link, helping you to build downlines in other sites&lt;/font&gt;&lt;/li&gt;\r\n							&lt;/ul&gt;\r\n							&lt;p align=&quot;center&quot;&gt;\r\n								&amp;nbsp;&lt;/p&gt;\r\n							&lt;p align=&quot;center&quot;&gt;\r\n								#SIGNUPLINK#&lt;/p&gt;\r\n							&lt;p align=&quot;center&quot;&gt;\r\n								&amp;nbsp;&lt;/p&gt;\r\n' where template_name='Sales Page' limit 1");



@mysql_query("UPDATE `".$prefix."templates` set template_data='Welcome #FIRSTNAME#,\r\n\r\nWelcome to #SITENAME#. To activate your account, please visit: #VERIFY#\r\n\r\nYour Login is: #USERNAME#\r\n\r\nYou can log in at ".$domainurl."/login.php\r\n\r\nTo earn more money, send people to #REFURL#\r\n\r\nThank you,\r\n\r\nAdmin,\r\n#SITENAME#' where template_name='Welcome Email' limit 1");

//Word Search Game Tables

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."wssettings` (
  `id` int(11) NOT NULL auto_increment,
  `field` varchar(200) default NULL,
  `value` varchar(200) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("INSERT INTO `".$prefix."wssettings` (`field`, `value`) VALUES
('enabled', '1'),
('instructions', 'Find all the letters by surfing and earn a prize!'),
('showcount', '20'),
('random', '1'),
('claimpage', '1'),
('daysreset', '1');");


@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."wswords` (
  `id` int(11) NOT NULL auto_increment,
  `phrase` varchar(100) default 'Traffic Exchange',
  `cvalue` int(11) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;");

@mysql_query("INSERT INTO `".$prefix."wswords` (`id`, `phrase`, `cvalue`) VALUES
(1, 'Splash Page', 5),
(2, 'Manual Surf', 10),
(3, 'Word Search', 10),
(4, 'TE Exchange', 15),
(5, 'Banner Ads', 15),
(6, 'Hit Credits', 20),
(7, 'Surfing Hit', 20),
(8, 'More Clicks', 25),
(9, 'Fun Surfing', 25),
(10, 'Earn Credits', 30);");


@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `sflag`, `template_data`) VALUES ('Letter Claim', 1, '&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;BR&gt;&lt;BR&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #ff0000&quot; mce_style=&quot;color: #ff0000;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;#SITENAME# Word Search&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;#FIRSTNAME#, you&amp;nbsp;just found a letter in the&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;BR&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;Word Search Game.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;A class=&quot;&quot; href=&quot;letterhuntclaim.php&quot; target=_self mce_href=&quot;/letterhuntclaim.php&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;STRONG&gt;Click Here To Claim Your Letter&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/A&gt;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;');");


@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `sflag`, `template_data`) VALUES ('faqs', 1, '&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;/SPAN&gt;&amp;nbsp;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;#SITENAME# FAQs&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;STRONG&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;What is a traffic exchange?&lt;/SPAN&gt;&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;Traffic exchanges are online services&lt;BR&gt;where web site owners trade traffic.&lt;BR&gt;Other members of #SITENAME# will&lt;BR&gt;view your web site when you visit the&lt;BR&gt;sites of other members.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;/SPAN&gt;&amp;nbsp;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;STRONG&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;How Do I Receive Hits?&lt;/SPAN&gt;&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;Click ''Surf'' on the members area menu&lt;BR&gt;to start viewing sites from other members.&lt;BR&gt;You will earn credits for every site you view.&lt;BR&gt;Your site will receive one hit for each credit&lt;BR&gt;that you assign to your site.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;/SPAN&gt;&amp;nbsp;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;STRONG&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;My Question Isn''t Listed On This Page...&lt;/SPAN&gt;&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;\r\n&lt;SCRIPT language=javascript&gt;\r\nfunction askAdmin()\r\n{\r\n	var windowprops = ''location=no,scrollbars=no,menubars=no,toolbars=no,resizable=no'' + '',left=100,top=50,width=500,height=700''; \r\n	var URL = ''".$domainurl."/askadminquestion.php?user=#AFFILIATEID#''; \r\n	popup = window.open(URL,''MemberPopup'',windowprops);	\r\n}\r\n&lt;/SCRIPT&gt;\r\n&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;Please click the button below to contact us.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;FORM style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;INPUT class=lfmtable style=&quot;FONT-SIZE: 24px; HEIGHT: 30px&quot; onclick=javascript:askAdmin() type=button value=&quot;Ask Admin&quot; name=&quot;Ask Tech&quot;&gt; &lt;/FORM&gt;');");


@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `sflag`, `template_data`) VALUES ('faqshome', 1, '&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;/SPAN&gt;&amp;nbsp;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;#SITENAME# FAQs&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;STRONG&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;What is a traffic exchange?&lt;/SPAN&gt;&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;Traffic exchanges are online services&lt;BR&gt;where web site owners trade traffic.&lt;BR&gt;Other members of #SITENAME# will&lt;BR&gt;view your web site when you visit the&lt;BR&gt;sites of other members.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;/SPAN&gt;&amp;nbsp;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;STRONG&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;How Do I Receive Hits?&lt;/SPAN&gt;&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;Click ''Surf'' on the members area menu&lt;BR&gt;to start viewing sites from other members.&lt;BR&gt;You will earn credits for every site you view.&lt;BR&gt;Your site will receive one hit for each credit&lt;BR&gt;that you assign to your site.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;');");


@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `sflag`, `template_data`) VALUES ('terms', 1, '&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;/SPAN&gt;&amp;nbsp;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;#SITENAME# Terms&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: left&quot; mce_style=&quot;TEXT-ALIGN: left&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;By registering for #SITENAME# you agree that you have read these terms, understand them, and agree to be bound by them.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: left&quot; mce_style=&quot;TEXT-ALIGN: left&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: medium&quot; mce_style=&quot;font-size: medium;&quot;&gt;Unless you&amp;nbsp;received prior permission from #SITENAME#, you may only register for one #SITENAME# account.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;You may not promote #SITENAME# in a way that could be considered spamming. You may only promote your referral URL and/or other promotional material&amp;nbsp;in other traffic exchanges or advertising programs that allow it. Promoting&amp;nbsp;#SITENAME# in a way that could be considered spamming will result in removal of your #SITENAME# account and possible legal action.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;Users are completely responsible for the content they promote using #SITENAME#. Users are responsible for ensuring they have the rights to promote a URL, and any content it contains. #SITENAME# is not responsible if a user is in violation of another legal agreement, copyright infringement, or any other issues.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;&lt;STRONG&gt;URL Rules&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;Before submitting a URL to #SITENAME#, please read these rules very carefully and make sure your URL does not violate any of them. If you violate any of these rules, your site will be deleted along with any credits assigned to them. Your #SITENAME# account may also be deleted.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*No sites with illegal activity.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*No frame-breaking sites.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*No viruses, trojans, or spyware.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*No adult content.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*Rotators may be promoted only if all sites or banners in rotation comply with these terms.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*No HYIPs or &quot;investment autosurfs&quot;. These are illegal in most countries, including the United States.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*No PTP sites.&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P&gt;&lt;SPAN class=&quot;&quot; style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot; mce_style=&quot;FONT-SIZE: medium; FONT-FAMILY: &quot;&gt;*#SITENAME#&amp;nbsp;has the full right to delete any site that&amp;nbsp;is inappropriate or questionable.&lt;/SPAN&gt;&lt;/P&gt;');");


@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `sflag`, `template_data`) VALUES ('Frame Break Blocker', 1, '&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;BR&gt;&lt;BR&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #ff0000&quot; mce_style=&quot;color: #ff0000;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;FRAME BREAKER DETECTION&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;#SITENAME# has detected a frame-breaker&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;These are sites that attempt to interrupt your&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;surfing session by removing the surfbar.&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;If you did not intend to stop surfing, please&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;click the link below to report the&amp;nbsp;detected site.&amp;nbsp;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;\r\n&lt;P style=&quot;TEXT-ALIGN: center&quot; mce_style=&quot;TEXT-ALIGN: center&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;SPAN style=&quot;COLOR: #000000&quot; mce_style=&quot;color: #000000;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: x-large&quot; mce_style=&quot;font-size: x-large;&quot;&gt;&lt;SPAN style=&quot;FONT-SIZE: large&quot; mce_style=&quot;font-size: large;&quot;&gt;&lt;STRONG&gt;&lt;A class=&quot;&quot; href=&quot;".$domainurl."/framebreakreport.php&quot; mce_href=&quot;".$domainurl."/framebreakreport.php&quot;&gt;Click Here To Report The Site&lt;/A&gt;&lt;/STRONG&gt;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/SPAN&gt;&lt;/P&gt;');");


//Credit Boost Table

@mysql_query("CREATE TABLE `".$prefix."cboost` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`starttime` INT( 11 ) NOT NULL DEFAULT '0',
`endtime` INT( 11 ) NOT NULL DEFAULT '0',
`surfboost` FLOAT NOT NULL DEFAULT '0',
`buyboost` FLOAT NOT NULL DEFAULT '0',
`acctype` INT( 11 ) NOT NULL DEFAULT '0',
`surfimage` varchar(200) default '/images/cboost.jpg',
`surftext` varchar(200) default 'Surfing Ratios Are Doubled Today',
`buytext` varchar(200) default 'SPECIAL: Credit Purchases Are Doubled Today'
) ENGINE = MYISAM ;");

//Member Stats Updates

@mysql_query("ALTER TABLE `".$prefix."members` ADD `tempcomm` float(6,2) NOT NULL default '0.00' ;");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `tempsales` float(6,2) NOT NULL default '0.00' ;");

@mysql_query("ALTER TABLE `".$prefix."members` ADD `tempnumsales` INT( 11 ) NOT NULL DEFAULT '0' ;");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `temprefs` INT( 11 ) NOT NULL DEFAULT '0' ;");

@mysql_query("ALTER TABLE `".$prefix."members` ADD `tempclicks` INT( 11 ) NOT NULL DEFAULT '0' ;");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `temptime` INT( 11 ) NOT NULL DEFAULT '0' ;");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `tempaccuracy` INT( 11 ) NOT NULL DEFAULT '0' ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."surflogs` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`date` DATE NOT NULL DEFAULT '0000-00-00',
`userid` INT( 11 ) NOT NULL DEFAULT '0',
`clicks` INT( 11 ) NOT NULL DEFAULT '0',
`time` INT( 11 ) NOT NULL DEFAULT '0',
`hitsdeliver` INT( 11 ) NOT NULL DEFAULT '0',
`bandeliver` INT( 11 ) NOT NULL DEFAULT '0',
`textdeliver` INT( 11 ) NOT NULL DEFAULT '0',
`newrefs` INT( 11 ) NOT NULL DEFAULT '0'
) ENGINE = MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."savedstats` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`statstype` INT( 11 ) NOT NULL DEFAULT '0',
`statsname` varchar(50) default '',
`startdate` DATE NOT NULL DEFAULT '0000-00-00',
`enddate` DATE NOT NULL DEFAULT '0000-00-00',
`mtype` INT( 11 ) NOT NULL DEFAULT '0',
`boardtype` INT( 11 ) NOT NULL DEFAULT '1',
`showid` INT( 11 ) NOT NULL DEFAULT '1',
`showfirst` INT( 11 ) NOT NULL DEFAULT '1',
`showlast` INT( 11 ) NOT NULL DEFAULT '1',
`showuser` INT( 11 ) NOT NULL DEFAULT '0',
`topnumber` INT( 11 ) NOT NULL DEFAULT '10',
`tablecolor` varchar(50) default 'white',
`tablecolor2` varchar(50) default 'gray',
`bordercolor` varchar(50) default 'black',
`textcolor` varchar(50) default 'black'
) ENGINE = MYISAM ;");

@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `refid` );");
@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `status` );");
@mysql_query("ALTER TABLE `".$prefix."members` ADD INDEX ( `joindate` );");

@mysql_query("ALTER TABLE `".$prefix."sales` ADD INDEX ( `affid` );");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD INDEX ( `saledate` );");

@mysql_query("ALTER TABLE `".$prefix."surflogs` ADD INDEX ( `date` );");
@mysql_query("ALTER TABLE `".$prefix."surflogs` ADD INDEX ( `userid` );");

// Banned Emails and IP Addresses

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."banemails` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`banned` varchar(250) default ''
) ENGINE = MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."banips` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`banned` varchar(50) default ''
) ENGINE = MYISAM ;");

@mysql_query("ALTER TABLE `".$prefix."sales` ADD `prize` INT( 11 ) NOT NULL DEFAULT '0';");
@mysql_query("ALTER TABLE `".$prefix."sales` ADD INDEX ( `prize` ) ;");

// Enter default RTO

@mysql_query("INSERT INTO `".$prefix."rto` (`id`, `totalpercent`) VALUES (1, 25)");

@mysql_query("INSERT INTO `".$prefix."rtoads` (`ipn`, `shown`, `percent`, `highnum`, `lownum`, `content`) VALUES
('0', 1, 100, 100, 0, '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n<title>Instant Squeeze Page Generator</title>\r\n\r\n<style>\r\n#body\r\n{\r\n\r\n	color:ffffff;\r\n	 \r\n	 }\r\n#container\r\n{\r\n\r\n	width:686px;\r\n}\r\n#top\r\n{\r\n\r\n	margin:0;\r\n	width:100%;\r\n	height:242px;\r\n	background-image:url(http://instantsqueezepagegenerator.com/images/ispgsqueezehead.png);\r\nbackground-repeat:no-repeat;\r\n\r\n}\r\n#middle\r\n{\r\n	margin:0;\r\n	width:100%;\r\n	height:412px;\r\n		background-image:url(http://instantsqueezepagegenerator.com/images/ispgsqueezemid.png);\r\n		background-repeat:repeat-y;\r\n		\r\n	overflow:visible;\r\n\r\n}\r\n#bottom\r\n{\r\n	margin:0;\r\n	width:686px;\r\n	height:141px;\r\n		background-image:url(http://instantsqueezepagegenerator.com/images/ispgsqueezebottom.png);\r\n		background-repeat:no-repeat;\r\n		padding-top:30px;\r\n\r\n	\r\n}\r\n#signup\r\n{\r\n	height:140px;\r\n	width:250px;\r\n	text-align:center;\r\n	vertical-align:middle;\r\n	float:left;\r\n		margin-left:45px;\r\n	margin-top:240px;\r\n	font-family:Verdana, Geneva, sans-serif;\r\n	font-size:14px;\r\n\r\n\r\n}\r\n#signup2\r\n{\r\n	height:360px;\r\n	width:290px;\r\n	text-align:left;\r\n	vertical-align:middle;\r\n	float:right;\r\n	margin-right:91px;\r\n	margin-top:50px;\r\n	font-family:Verdana, Geneva, sans-serif;\r\n	font-size:12px;\r\n\r\n\r\n}\r\n\r\n.heading\r\n{\r\n	font-family:Verdana, Geneva, sans-serif;\r\n	font-size:16px;\r\n	text-decoration:underline;\r\n	font-weight:bold;\r\n\r\n}\r\n.rightheading\r\n{\r\n		font-family:Verdana, Geneva, sans-serif;\r\n	font-size:14px;\r\n\r\n	font-weight:bold;\r\n	color:000080;\r\n}\r\n</style>\r\n\r\n</head>\r\n\r\n<body >\r\n<center>\r\n\r\n<div id=\"container\">\r\n    \r\n    <div id=\"top\"></div>\r\n    \r\n    \r\n    \r\n     <div id=\"middle\">\r\n      <div id=\"signup\"><center>\r\n      <p><span style=\"color:#ffffff\">A complete done for you solution to list building on traffic exchanges...</span>        </p>\r\n      <p><span style=\"color:#ffffff\">And you have nothing to lose because it''s </span><span class=\"heading\" style=\"color:#ffffff\">ABSOLUTELY FREE!</span></p>\r\n    </center></div>\r\n\r\n\r\n\r\n\r\n     <div id=\"signup2\"><span style=\"color:#000000;\">\r\n       <span class=\"rightheading\">All Traffic Exchanges are best used as a lead generation service, to get surfers and members to sign up to your list.\r\n     </span></span>\r\n        <span style=\"color:#ffffff\">\r\n       <p> You can do this in a couple of ways...BUT</p>\r\n       <p> <strong>By far the best way is to give something away using a squeeze page or splash page. </strong></p>\r\n       <p>Trying to sell them something while they are busy surfing isn''t the best use of traffic exchange page views.</p>\r\n       <p> Offering them a high quality report in exchange for their email address so you can tell them about your opportunity later via email is going to give you far better results.</p>\r\n       <p> To that end <strong>I would like to recommend a free service</strong> called Instant Squeeze Page generator, ISPG is an instant squeeze page wizard a simple point and click system that will create and host your squeeze pages,<strong> it even supplies all the free gifts</strong> you can use to tempt people onto your subscriber list </p></span></div>\r\n    </div>\r\n    \r\n   \r\n     \r\n     \r\n      <div id=\"bottom\">\r\n      \r\n      <center>\r\n        <a target=\"_blank\" href=\"http://instantsqueezepagegenerator.com/?rid=29587\"><img src=\"http://instantsqueezepagegenerator.com/images/ispgsqueezebutton.png\" width=\"375\" height=\"100\" border=\"0\" /></a>\r\n  </center></div>\r\n</div>\r\n\r\n<br>\r\n<center>\r\n<a href=\"showoto.php\"><b>No, thank you. I don''t want my pages to grab attention in traffic exchanges.</b></a>\r\n</center>\r\n</body>\r\n</html>')");

// LFMTE v2 updates (based on LFM v4)

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."promotion_headers` (`id` int(11) NOT NULL auto_increment,`type` varchar(20) NOT NULL,`template_data` varchar(10000) NOT NULL,PRIMARY KEY  (`id`))");
@mysql_query("INSERT INTO `".$prefix."signupform` (`fieldname`, `fieldsize`, `enable`, `require`) VALUES ('checkbox', 0, 1, 0)");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `signup_checkbox_text`  varchar( 100) NOT NULL default 'Check this box if you want to save 65% on our upgraded membership.'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `min_comm` float(6,2) unsigned default '0.00'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_home` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_ref` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_profile` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_promote` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_makemoney` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_downloads` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_dlb` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_upgrade` INT( 11 ) NOT NULL DEFAULT '1';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_forum` INT( 11 ) NOT NULL DEFAULT '0';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `enablemenu_logout` INT( 11 ) NOT NULL DEFAULT '1';");

@mysql_query("ALTER TABLE `".$prefix."memberpages` ADD `filename` VARCHAR( 100 ) NOT NULL");
@mysql_query("ALTER TABLE `".$prefix."memberpages` ADD `isfile` INT NOT NULL DEFAULT '0' ");

// Affiliate Sale Notification
@mysql_query("ALTER TABLE `".$prefix."members` ADD `affnotify` INT( 11 ) NOT NULL DEFAULT '1';");

// End LFMTE v2 updates


// Dynamic admin menu

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."adminmenu` (
  `id` int(10) unsigned NOT NULL,
  `menu_label` varchar(30) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_target` varchar(30) default NULL,
  `menu_parent` int(11) default '0',
  `menu_order` int(11) default NULL,
  `f` varchar(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ;");

@mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES
(100, 'Home', 'admin.php', '_top', 0, 1, '', ''),
(300, 'Settings', 'admin.php?f=ss', '_top', 0, 1, 'ss', 'systemsettings.php'),
(310, 'Payment/IPN Settings', 'admin.php?f=ipnset', '_top', 300, 2, 'ipnset', 'ipnsettings.php'),
(320, 'Signup Settings', 'admin.php?f=sgs', '_top', 300, 3, 'sgs', 'signupsettings.php'),
(330, 'System Settings', 'admin.php?f=ss', '_top', 300, 4, 'ss', 'systemsettings.php'),
(340, 'Member Grouping', 'admin.php?f=gr', '_top', 300, 5, 'gr', 'groups.php'),
(400, 'Members', 'admin.php?f=mm&sf=browse', '_top', 0, 3, 'mm', 'membermanagement.php'),
(500, 'View Members', 'admin.php?f=mm&sf=browse', '_top', 400, 4, 'mm', 'membermanagement.php'),
(600, 'Account Levels', 'admin.php?f=mt', '_top', 400, 5, 'mt', 'membertypes.php'),
(610, 'Member Stats', 'admin.php?f=mstats', '_top', 400, 6, 'mstats', 'memberstats.php'),
(620, 'Support Questions', 'admin.php?f=askadmin', '_top', 400, 7, 'askadmin', '../askadmin/askadmin.php'),
(630, 'Banned Emails', 'admin.php?f=banemm&sf=browse', '_top', 400, 8, 'banemm', 'bannedemanagement.php'),
(640, 'Banned IP Addresses', 'admin.php?f=banipmm&sf=browse', '_top', 400, 9, 'banipmm', 'bannedipmanagement.php'),
(700, 'Sites/Ads', 'admin.php?f=smm&sf=browse', '_top', 0, 3, 'smm', 'sitemanagement.php'),
(710, 'Member Sites', 'admin.php?f=smm&sf=browse', '_top', 700, 4, 'smm', 'sitemanagement.php'),
(720, 'Banner Ads', 'admin.php?f=bmm&sf=browse', '_top', 700, 5, 'bmm', 'bannermanagement.php'),
(730, 'Text Ads', 'admin.php?f=tmm&sf=browse', '_top', 700, 6, 'tmm', 'textmanagement.php'),
(740, 'Pre Approved Sites', 'admin.php?f=appmm&sf=browse', '_top', 700, 7, 'appmm', 'approvedmanagement.php'),
(750, 'Banned Sites', 'admin.php?f=banmm&sf=browse', '_top', 700, 8, 'banmm', 'bannedmanagement.php'),
(760, 'Abuse Reports', 'admin.php?f=areports&sf=browse', '_top', 700, 9, 'areports', 'abusereports.php'),
(900, 'Offers', 'admin.php?f=oto', '_top', 0, 3, 'oto', 'oto.php'),
(1000, 'OTO', 'admin.php?f=oto', '_top', 900, 4, 'oto', 'oto.php'),
(1100, 'Login Offers', 'admin.php?f=spo', '_top', 900, 5, 'spo', 'spo.php'),
(1150, 'Rotating Offers', 'admin.php?f=rto', '_top', 900, 6, 'rto', 'rto.php'),
(1200, 'Downline Builder', 'admin.php?f=dlb', '_top', 0, 3, 'dlb', 'downlinebuilder.php'),
(1300, 'Affiliate Toolbox', 'admin.php?f=lm', '_top', 0, 3, 'lm', 'linkmanagement.php'),
(1350, 'Partners', 'admin.php?f=spartners', '_top', 0, 3, 'spartners', 'spartners.php'),
(1360, 'Signup Partners', 'admin.php?f=spartners', '_top', 1350, 4, 'spartners', 'spartners.php'),
(1400, 'Site Design', 'admin.php?f=tm', '_top', 0, 3, 'tm', 'templatemanagement.php'),
(1410, 'Templates', 'admin.php?f=tm', '_top', 1400, 4, 'tm', 'templatemanagement.php'),
(1420, 'Bonus Page', 'admin.php?f=bonus', '_top', 1400, 5, 'bonus', '../bonuspage/bonusadmin.php'),
(1430, 'Themes', 'admin.php?f=th', '_top', 1400, 6, 'th', 'themes.php'),
(1440, 'Custom Pages', 'admin.php?f=cd', '_top', 1400, 7, 'cd', 'contentdelivery.php'),
(1500, 'Sell Items', 'admin.php', '_top', 0, 3, '', ''),
(1510, 'Sales Packages', 'admin.php?f=scrds', '_top', 1500, 4, 'scrds', 'ipnsalespackages.php'),
(1520, 'Startpages', 'admin.php?f=startp', '_top', 1500, 5, 'startp', 'startpages.php'),
(1530, 'Payments Received', 'admin.php?f=ipnsales', '_top', 1500, 6, 'ipnsales', 'ipnpayments.php'),
(1540, 'Commissions', 'admin.php?f=sa', '_top', 1500, 7, 'sa', 'sales.php'),
(1550, 'Products', 'admin.php?f=pd', '_top', 1500, 8, 'pd', 'products.php'),
(1560, 'File Library', 'admin.php?f=fl', '_top', 1500, 9, 'fl', 'filelib.php'),
(1600, 'SurfingGuard', '/surfingguard/index.php', '_blank', 0, 3, '', ''),
(2100, 'Help', 'help.php', '_blank', 0, 3, '', ''),
(2200, 'Logout', 'admin.php?f=lo', '_top', 0, 3, '', ''),
(2300, 'Surf Options', 'admin.php', '_top', 0, 2, '', ''),
(2310, 'Anti-Cheat Settings', 'admin.php?f=acheats', '_top', 2300, 3, 'acheats', 'anticheat.php'),
(2320, 'Bonus Pages', 'admin.php?f=bonusp', '_top', 2300, 4, 'bonusp', 'bonuspages.php'),
(2330, 'Word Search Game', 'admin.php?f=lhunt', '_top', 2300, 5, 'lhunt', 'letterhunt.php'),
(2340, 'Credit Boost/Special', 'admin.php?f=cboost', '_top', 2300, 6, 'cboost', 'creditboost.php'),
(2350, 'Surfing Stats', 'admin.php?f=surfstats', '_top', 2300, 7, 'surfstats', 'surfstats.php'),
(2360, 'Dynamic Ratios', 'admin.php?f=dynamic', '_top', 2300, 8, 'dynamic', 'dynamic.php')");

// End dynamic admin menu


// Add function plugin table

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."extra_funcs` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(100) NOT NULL,
  `include_path` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
)");

// End add function plugin table

// LFMTE Update v2.02
@mysql_query("ALTER TABLE `".$prefix."anticheat` ADD `enclevel` INT( 11 ) NOT NULL DEFAULT '4' ;");

// LFMTE Update v2.03
@mysql_query("ALTER TABLE `".$prefix."msites` ADD `sitename` VARCHAR( 100 ) NOT NULL DEFAULT 'Site Name' AFTER `memid` ;");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `unverirem` INT( 11 ) NOT NULL DEFAULT '0' AFTER `showeditors` ;");

@mysql_query("INSERT INTO `".$prefix."templates` (
`template_id` ,
`template_name` ,
`template_data` ,
`sflag` 
)
VALUES (NULL , 'Verify Reminder', 'Hello #FIRSTNAME#,\n\nThis is a reminder that you joined #SITENAME#\nbut haven''t activated your account yet.\n\nTo activate your account, please visit: #VERIFY#\n\nThank you,\n#SITENAME#', '1') ;");

// LFMTE Update v2.04
@mysql_query("Update `".$prefix."membertypes` set enablepdate=0");

// LFMTE Update v.2.05
@mysql_query("ALTER TABLE `".$prefix."memberpages` ADD `parent` INT NOT NULL DEFAULT '0'");

// LFMTE Update v.2.07
@mysql_query("CREATE TABLE `".$prefix."surfmods` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `rank` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("ALTER TABLE `".$prefix."surfmods` ADD INDEX (`enabled`) ;");
@mysql_query("ALTER TABLE `".$prefix."surfmods` ADD INDEX (`rank`) ;");

@mysql_query("INSERT INTO `".$prefix."surfmods` (`enabled`, `rank`, `filename`) VALUES ('1', '1', 'surf_bonuspages.php'), ('1', '2', 'surf_wordsearch.php');");

@mysql_query("CREATE TABLE `".$prefix."surf_admin_prefs` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `field` VARCHAR(50) NOT NULL ,
 `value` INT(11) NOT NULL DEFAULT '0',
 UNIQUE (
 `field` 
)) ENGINE=MYISAM ;");

@mysql_query("CREATE TABLE `".$prefix."surf_user_prefs` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `field` VARCHAR(50) NOT NULL ,
 `userid` INT(11) NOT NULL DEFAULT '0',
 `value` INT(11) NOT NULL DEFAULT '0'
) ENGINE=MYISAM ;");

@mysql_query("ALTER TABLE `".$prefix."surf_user_prefs` ADD INDEX (`field`) ;");
@mysql_query("ALTER TABLE `".$prefix."surf_user_prefs` ADD INDEX (`userid`) ;");

@mysql_query("INSERT INTO `".$prefix."surf_admin_prefs` (`field`, `value`) VALUES ('surfbarstyle', '-1'), ('preloadsites', '1'), ('surfbardrop', '-1'), ('surfbartop', '-1'), ('footer_pos', '-1'), ('can_move_footer', '1'), ('footer_transparent', '-1'), ('framesites', '-1');");

@mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES ('2302', 'Surfbar Settings', 'admin.php?f=sbsettings', '_top', '2300', '2', 'sbsettings', 'surfbarsettings.php');");

@mysql_query("ALTER TABLE `".$prefix."adminmenu` ORDER BY id ;");

@mysql_query("ALTER TABLE `".$prefix."msites` ADD `framesite` INT(1) NOT NULL DEFAULT '0';");


// LFMTE Update v.2.08
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `arp_email` varchar(255) NOT NULL default ''");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `gvo_affiliate_name` varchar(100) NOT NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `gvo_campaign` varchar(100) NOT NULL");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `gvo_form_id` varchar(100) NOT NULL");

@mysql_query("INSERT INTO `".$prefix."surf_admin_prefs` (`field`, `value`) VALUES ('footertype', '-1');");
@mysql_query("INSERT INTO `".$prefix."surf_admin_prefs` (`field`, `value`) VALUES ('surftheme', '2');");
@mysql_query("INSERT INTO `".$prefix."surf_admin_prefs` (`field`, `value`) VALUES ('choosetheme', '1');");

@mysql_query("ALTER TABLE `".$prefix."startpage_settings` ADD `alwaysframe` TINYINT(1) NOT NULL DEFAULT '1' AFTER `showbar`;");

@mysql_query("ALTER TABLE `".$prefix."reports` CHANGE `text` `text` TEXT NOT NULL;");


// LFMTE Update v.2.09
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."ipnmods` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("ALTER TABLE `".$prefix."mtexts` CHANGE `text` `text` VARCHAR(30) NOT NULL DEFAULT '';");


// LFMTE Update v.2.11
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."templates_backups` (
  `id` int(11) NOT NULL auto_increment,
  `savetime` datetime NOT NULL,
  `template_name` varchar(20) NOT NULL,
  `template_data` mediumtext,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."templates_defaults` (
  `id` int(11) NOT NULL auto_increment,
  `template_name` varchar(20) NOT NULL,
  `template_data` mediumtext,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."memtemplates_backups` (
  `id` int(11) NOT NULL auto_increment,
  `savetime` datetime NOT NULL,
  `mtid` INT NOT NULL DEFAULT '0',
  `template_data` mediumtext,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."pages_backups` (
  `id` int(11) NOT NULL auto_increment,
  `savetime` datetime NOT NULL,
  `pageid` INT NOT NULL DEFAULT '0',
  `template_data` mediumtext,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."sstats` (
  `id` int(16) NOT NULL auto_increment,
  `userid` varchar(32) NOT NULL default '',
  `url` varchar(128) NOT NULL default '',
  `websiteid` int(14) NOT NULL default '0',
  `adate` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");


@mysql_query("ALTER TABLE `".$prefix."settings` ADD `surficontheme` VARCHAR(50) NOT NULL DEFAULT 'surficon_themes/LFMTE_smart75' AFTER `theme` ;");

@mysql_query("ALTER TABLE `".$prefix."reports` CHANGE `date` `date` INT NOT NULL DEFAULT '0' ;");

@mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES (1432, 'Surfbar Icon Themes', 'admin.php?f=iconstheme', '_top', 1400, 8, 'iconstheme', 'iconthemes.php');");

@mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES (650, 'Find Cheaters', 'admin.php?f=dupe', '_top', 400, 10, 'dupe', 'mod_dupe.php');");

@mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES (651, 'Surf Logs', 'admin.php?f=sstats&sort=1', '_top', 400, 11, 'sstats', 'sstats_main.php');");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `sstats` SMALLINT(3) NOT NULL DEFAULT '7' COMMENT 'surf stats cron' ;");

@mysql_query("ALTER TABLE `".$prefix."templates` ADD `template_type` VARCHAR(50) NOT NULL DEFAULT 'none' ;");
@mysql_query("ALTER TABLE `".$prefix."templates` ADD `template_title` VARCHAR(50) NOT NULL AFTER `template_name` ;");

@mysql_query("DELETE FROM `".$prefix."templates` WHERE template_id=11 AND template_name='Welcome Email';");

@mysql_query("UPDATE `".$prefix."templates` SET template_type='none' WHERE template_name='One Time Offer' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='none' WHERE template_name='One Time Offer2' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='none' WHERE template_name='One Time Offer3' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='none' WHERE template_name='OTO Footer' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='none' WHERE template_name='News' ;");

@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Login Page' WHERE template_name='Homepage Body' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Suspended Page' WHERE template_name='Suspended' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Sales Page' WHERE template_name='Sales Page' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Split Test 1' WHERE template_name='Sales Page 2' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Split Test 2' WHERE template_name='Sales Page 3' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Signup Header' WHERE template_name='Signup' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Downline Builder Header' WHERE template_name='Downline Bldr Header' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Download Page Header' WHERE template_name='Download Page' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Delete Confirmation Page' WHERE template_name='Delete Confirmation' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Deleted Account Page' WHERE template_name='Deleted' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Letter Claim Page' WHERE template_name='Letter Claim' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Frame Breaker Detected' WHERE template_name='Frame Break Blocker' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Members Area FAQs' WHERE template_name='faqs' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Main FAQs' WHERE template_name='faqshome' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='page', template_title='Terms Page' WHERE template_name='terms' ;");

@mysql_query("UPDATE `".$prefix."templates` SET template_type='mail', template_title='Welcome Email' WHERE template_name='Welcome Email' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='mail', template_title='Verify Reminder' WHERE template_name='Verify Reminder' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='mail', template_title='Downline Email' WHERE template_name='Downline Email' ;");
@mysql_query("UPDATE `".$prefix."templates` SET template_type='mail', template_title='Commission Email' WHERE template_name='Commission Email' ;");

// add the theme on sales page option
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `indextheme` INT(1) NOT NULL DEFAULT '1'");



// v2.14 Updates

// Extra function for the one click macros
$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."extra_funcs` where include_path='oneclick_macros.php'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."extra_funcs` (`type`, `include_path`) VALUES ('user', 'oneclick_macros.php')");
}
// End extra function for the one click macros

// Extra function for the ipn macros
$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."extra_funcs` where include_path='ipnmacros.php'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."extra_funcs` (`type`, `include_path`) VALUES ('user', 'ipnmacros.php')");
}
// End extra function for the ipn macros

$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."ipnmods` where filename='ipn_pages.php'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."ipnmods` (`enabled`, `filename`) VALUES ('1', 'ipn_pages.php')");
}

mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."purchasedpages` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL default '0',
  `pageid` int(11) NOT NULL default '0',
  `ipntransid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."ipn_pages` (
  `id` int(11) NOT NULL auto_increment,
  `productid` int(11) NOT NULL default '0',
  `pageid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `productid` (`productid`),
  KEY `pageid` (`pageid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."ipn_prod_img` (
  `id` int(11) NOT NULL auto_increment,
  `productid` int(11) NOT NULL default '0',
  `merchant1button` mediumblob NOT NULL,
  `merchant2button` mediumblob NOT NULL,
  `merchant3button` mediumblob NOT NULL,
  `merchant4button` mediumblob NOT NULL,
  `merchant5button` mediumblob NOT NULL,
  `merchant6button` mediumblob NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("ALTER TABLE `".$prefix."ipn_merchants` ADD `defaultbutton` MEDIUMBLOB NOT NULL");

@mysql_query("UPDATE `".$prefix."ipn_merchants` SET buy_now='<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">\r\n<input type=\"hidden\" name=\"cmd\" value=\"_xclick\">\r\n<input type=\"hidden\" name=\"business\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"item_name\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"amount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"no_note\" value=\"1\">\r\n<input type=\"hidden\" name=\"currency_code\" value=\"USD\">\r\n<input type=\"hidden\" name=\"item_number\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"custom\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"return\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"notify_url\" value=\"[notify]\">\r\n<input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" alt=\"Make payments with PayPal - it''s fast, free and secure!\">\r\n</form>', subscribe='<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">\r\n<input type=\"hidden\" name=\"cmd\" value=\"_xclick-subscriptions\">\r\n<input type=\"hidden\" name=\"business\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"item_name\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"no_shipping\" value=\"1\">\r\n<input type=\"hidden\" name=\"no_note\" value=\"1\">\r\n<input type=\"hidden\" name=\"currency_code\" value=\"USD\">\r\n<input type=\"hidden\" name=\"bn\" value=\"PP-SubscriptionsBF\">\r\n<input type=\"hidden\" name=\"a1\" value=\"[trial_amount]\">\r\n<input type=\"hidden\" name=\"p1\" value=\"[trial_period]\">\r\n<input type=\"hidden\" name=\"t1\" value=\"[trial_type]\">\r\n<input type=\"hidden\" name=\"a3\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"p3\" value=\"[period]\">\r\n<input type=\"hidden\" name=\"t3\" value=\"[type]\">\r\n<input type=\"hidden\" name=\"src\" value=\"1\">\r\n<input type=\"hidden\" name=\"sra\" value=\"1\">\r\n<input type=\"hidden\" name=\"item_number\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"custom\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"return\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"notify_url\" value=\"[notify]\">\r\n<input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" alt=\"Make payments with PayPal - it''s fast, free and secure!\">\r\n</form>', defaultbutton=0x52306c474f446c68506741664150636141502f3135662b744c662b79504a326a707873325a50375068502f373966374356662b784e6637566c662f445a762f7933657a6c79756256734f48640d0a78762b3652502b3757502b724b502f4b647533586f6c4e6a652f2b794f662f3839762f6d76762f753073444d7a38774141502b325062653571662f70782f2f6c75354f71754d6249765470730d0a6e6f756d764a4b6271502f363862334278536c43624f6e6d316c4a39704b7177714d4739725456706e48535773762f677233695671662f4761694d385a2f5078362f2f3537502f717576375a0d0a704d624f31503777793866533161753979762f3437762b6f492f2f4e65454a796f66373134662f7578762b6f49715373736e716275662f6973502f3436762f7179662b754c2f374e667644680d0a754b6d36786458633356317267544a496276376d74574b4a72502b3954465a396e2f2f6974662b3252464a6c67653772342f37516a2f2f392b4770366c4861466e502b304f706d77772f37580d0a6a56364771395054792f2f747a7637636e662b734b2f376c73762f757947527a6832364f704b363075662b774d762f647145783670657a4d6a662f36373950527734366f75397668342f2f360d0a38502f5a6c454255647257367466664e672f37626e495369765556616532694e7249434d6e7637646f662f7731502f76316637526866375367767253682f2f636c6153327644644e63334f420d0a6c45643270443554645435776f502f7832662b3953766e596c4e51704b502f32316c7073694c6137766d7552736e46396931364670762f35356b465765663763704c7a4a30557065662f37590d0a6d3465616f762f5568762f5066634c4c785542786e3078676743354762762f3236502f392b762f3236544a6e6d2f2f392b2f2b704a7637626f762b6e49762f2f2f2f2b714a762f727a762f350d0a37397a67316165357734695570662f686e72724879647261326637677157783368762f3336766a3136576950732f2f4962385447786c397769636a4b77382f507a612f437a39414f43666a620d0a6f2b437a582f6e636f763348594f584d6c50767278762f707774744c533457516e7662596e762f37362f6a7832475a74637269397764545a7a7637586d59796b73662f696f664c713174584d0d0a72762f50683432526a322b52724a6d5769732f4b72767a35387433436a4f2f7536506e7831766279342f2f773047654b70506231384f446b344f4464304b4f307436327a746e422f6c6c566e0d0a6845645964666e5469763752694f2f6f30662f33352f2f33366244427966374963762f656d4c4b3376565741706c574171494f506f744456307637556b502f546b6f4366754957516f72476f0d0a6b762b744d502b3353662b3957662f4561762f4c652f2b714a2f376170762f727a662f6270662f6b76502f536a68497a5a744956436948354241454141426f414c4141414141412b414238410d0a41416a2f41445549314343716f4d474443424d71584d6951346343426f7070644b4a576e6f73574c47444e71334d675249775a2b56455242374a446a6b386d544b464f71584d6d794a6373710d0a41447949314e44766c5947624f41315932466d6c5a382b644f334d4b31516e555a78576751596b69745442554845454d70714a4b4e645747524977726d6a535a2b474f6c42676b444a4d4b4b0d0a48527657514c5a596751594e4f684f457a566579306e41456d704f457846525476777032324d53334c376c584d6c684a495141444267464e56324b6b53534f6a73574d5a5178716253734d6d0d0a6b4b635149547a7879424b6a44574d5a69366e4e3854526e79704279665463744b4a686e67657658433371515536523152416c764242345a3633624647377430553072596d55574f6e4331320d0a4135436f7656566a6b6163744e5844414337496d4569787457304c674141414139674a2b426146672f786850486b2f73455151537852674768414364564a6b4550594c785a7752364b3436730d0a66534f515373514b46734d4d4934496e6459697778546f6f724842474a456c67636b59374378435342336b59454648514252316b6d434552586541784469447047554f474a415234777755510d0a696743525358706b4543434a49393063646f4a7a4c4752677a686d655a4848444b686e677349346e4832547733444a3464454745686832595564416b486a545a4a495a64674c4166444a6f510d0a514141747457774452444341304146444d467751384d63326a78417751413834727344445a53796363676f536138427a5267696f6f4f494a4339635138637346546e6f415255466d7443426f0d0a43304b416363457641355435686853306a4d434147704c4138413074564d49784467456d675067474d63634d736b496a54624467527a5758714250434d3357736f4d347161774470777756510d0a4350387861417676464e524d4b4c6947416b6b7251737a6769676d617147424447443734776767425969436968695a4c4f44444f483162436b414969666f515169414d322b4f4244474930410d0a6963676c6d4e5278535230687744464443325a416b6d736f7942523054774c77776a764a48554949516f41534454436862774f7545454442414571345a774d446778456752625975654f4c430d0a455579414163595232486a79444466644a674d434a69677730386f646b38536277447631464b54444f555955554941652f6267684277564c514c504c48584a3463596338663567416a686a670d0a4f445044456366436f4d494d59464479424165356542487a48536d674d416732597a7742527a525055434b4d472b364959334942526967775430452f6b454a5050517138494973454f3851520d0a78783657374b43324a5875674d593068626869436a696f54694546414d6172306f51556666466a2f6b7662616c61445253783939754f4747467547674c5945734c39696a514430624b4b53440d0a44714245554151435747534f51424542584c37424134553449666f42764d68446d43363848504441426c68555541626e6e6a386775684f46504c443642707448414d726b6b2f397730412b380d0a55783742463056772f6b55452b45515151426b49564a44353578555541344d72427a6942652f4542484a2f383873315834443043434c782b2f4f36394c775438354b43416767382b704b52500d0a756672446630453841687438666e735a583744762f76764a7933383838766f4c487539385a3544314766434143457a67384336486866706834585541544b41454a336a4167387a6a67686a4d0d0a344158397755462f6148416534424f4141436f415067783663494d66544b454b4931415142496a77685442386f5439454f4d4d5a43714347497479414457566f7778373238495a416a43454d0d0a2f77307944336f59385968494e4749482f5545504a6a5a526955394d49684f6e47455571556a474a53497843515342516a7935363859746439456359367946474d6f36786a4638556f78724e0d0a614d59316f68474d58525241516568686a7a7261385935313945636537614648507536786a336673497766393645633947684b5065465241515352776a3059363870474e3747416b4a526c4a0d0a534662796b68783070442f7573556c4c506a4a6b6f72424850305a4a796c4b613870536b3941637156386c4b566a6244567675497053786e53637461787449667473796c4c6e6435447048510d0a41777238434b597768306e4d5968727a6d4d684d4a6a2b67414147426943494246794343507271676a327061383572597a4b593274386c4e61314b54434263517830774951673871304341660d0a3645796e4f74664a7a6e613638353372704145564944424f6944546b6e766a4d70304965456745514144733d0d0a WHERE name='PayPal'");

@mysql_query("UPDATE `".$prefix."ipn_merchants` SET buy_now='<form action=\"https://www.safepaysolutions.com/index.php\" method=\"post\">\r\n<input type=\"hidden\" name=\"_ipn_act\" value=\"_ipn_payment\">\r\n<input type=\"hidden\" name=\"iowner\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"ireceiver\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"iamount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"itemName\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"itemNum\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"idescr\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"idelivery\" value=\"1\">\r\n<input type=\"hidden\" name=\"iquantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"imultiplyPurchase\" value=\"n\">\r\n<input type=\"hidden\" name=\"custom1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"notifyURL\" value=\"[notify]\">\r\n<input type=\"hidden\" name=\"returnURL\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"cancelURL\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[merchant_button]\" alt=\"Pay through SafePay Solutions\">\r\n</form>', subscribe='<form action=\"https://www.safepaysolutions.com/index.php\" method=\"post\">\r\n<input type=\"hidden\" name=\"_ipn_act\" value=\"_ipn_subscription\">\r\n<input type=\"hidden\" name=\"ireceiver\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"itemName\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"itemNum\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"idescr\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"trialAmount\" value=\"[trial_amount]\">\r\n<input type=\"hidden\" name=\"trialPeriod\" value=\"[trial_days]\">\r\n<input type=\"hidden\" name=\"trialCycles\" value=\"1\">\r\n<input type=\"hidden\" name=\"iamount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"cycleLength\" value=\"[days]\">\r\n<input type=\"hidden\" name=\"cycles\" value=\"0\">\r\n<input type=\"hidden\" name=\"custom1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"notifyURL\" value=\"[notify]\">\r\n<input type=\"hidden\" name=\"returnURL\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"cancelURL\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[merchant_button]\" alt=\"Pay through SafePay Solutions\">\r\n</form>', defaultbutton=0x52306c474f446c6855414165415063414141414141444d7a4d325a6d5a706d5a6d656a6f3650447738502f2f2f356c6d5a6d597a4d356b7a4d38777a4d7a4d4141475941414a6b41414d77410d0a41502f7738502f67324d777a41502f6f34502f59794a6b7a41502b6f65502f416f502f5175502f6730502f7736503967415039344b502b4951502b5155502b6761502b7767502b346b502f490d0a7150396f43503977455039774750393449502b414d502b494f502b5153502b5957502b6759502b6f63502b3469502f416d502f597750396f4150393447502b414b502b5151502b5955502b6f0d0a61502b7765502f496f502f5173502f6f324a6c6d4d32597a414d786d41502f77344a6c6d414a6d5a5a6d5a6d4d356d5a4d7a4d7a41475a6d414a6d5a41502f2f384a6e4d414a6e4d4d32615a0d0a414a6e2f4147615a4d356e4d5a6a4e6d4147624d414a6e2f4d32622f4147624d4d7a4f5a414a6e2f5a6a504d4147622f4d7a502f4147615a5a6a4e6d4d356e4d6d544f5a4d32624d5a6a504d0d0a4d77417a4141426d4141435a4141444d4141442f4144502f4d32622f5a706e2f6d51442f4d77444d4d7a502f5a6a504d5a67435a4d32622f6d51442f5a6a4f5a5a6d624d6d51426d4d77444d0d0a5a6a502f6d5a6e2f7a41442f6d54504d6d51435a5a6d622f7a41444d6d54502f7a41442f7a4f446f3647615a6d544e6d5a706e4d7a444f5a6d57624d7a44504d7a41417a4d77426d5a67435a0d0a6d51444d7a41442f2f7a502f2f32622f2f356e2f2f2f442f2f77444d2f77435a7a44504d2f7a4f5a7a41426d6d57624d2f77435a2f394459344e6a67364f446f38444e6d6d57615a7a41417a0d0a5a67426d7a444f5a2f356e4d2f77426d2f384449324d6a51344b4377304e445936444e6d7a41417a6d57615a2f346959774a43677946426f71424177694c4334304c6a4132486949754943510d0a77456867714542596f41417a7a444e6d2f32683473484341754468516f4442496d4368416d4341346b4b436f794b69773046686f71474277734c4334324267776b42416f6941417a2f3569670d0a794b436f30464267714568596f4542516f4468496d41676769496951774a4359794442416d4869417549434977484234754f446736475a6d6d644451344f6a6f384d4441324e6a59364d6a490d0a344c693432444d7a5a706d5a7a4f446738444d7a6d575a6d7a444d7a7a4141414d7741415a6741416d5141417a4141412f7a4d7a2f325a6d2f356d5a2f2f44772f7a4d412f7a4d417a47597a0d0a2f32597a7a444d416d5a6c6d2f3259412f32597a6d5a6c6d7a444d415a6d59417a4a6b7a2f356b412f356b7a7a4759416d5a6b417a4d777a2f3877412f356c6d6d57597a5a706b7a6d63777a0d0a7a444d414d3259415a706b416d6377417a4d77416d63777a6d5a6b415a706b7a5a6d59414d3877415a73777a5a706b414d3877414d79483542414141414141414c41414141414251414234410d0a41416a2f41413049484569776f4d474443424d71584d6977346349436d496964637657716f73574c47444e71334767525669746d4467734d46436d517041475449706d5663765872567374620d0a4d47504b6e456d7a707332597344434e334d6d7a5a452b55686e494a63776b4c315370547755416c586171304b644f6e54714e4354516f73467a53426867512b794f717a71316555427269560d0a67676d4c46616d7244744d794e5054415146757557336d4b4a416d325a4379597257707839636e745a46692f665555472f697359734f48436941316b46526c336131752f66756c4750726c700d0a364375646b2f3036306c564c4d70472b6e3547524773317372734450426a3454436276614c784653745a6a4237727056386b3654666f6e427a425732375741446a6e62395770584d734f70480d0a776c322b2b73524e645850513046474c7a48574b6c3674546b746d654e46334139506274753236682f344a303068477a4f772b574a5374513668637376512b535a634c3079432b7a563764320d0a6f584b507563436d30675573553041796d78516e30694f625942494d4b384f4134686462692f6e6b585865524c64504b4c353859674177724649566979696d6b79414c4c4c634a73386741700d0a7337774379796d796e43544c4c6164777377784c6e53477a436b57306c4c4b4c624c30514935496d76417744535866634f41626841334d6c4f526c336d317849796a4b6e2f474c4c4b3739490d0a4f556f754c65304344536176694e4c4b68613867593041777434526944436932755049494d2f744e365a49746d64527953797346474e4d4c687177395277523346504c5a4a3458333563544b0d0a4c36686773736b7774376953494979624749504b4c635577553473727433785377444253446d574c4c675745637373736d44437a4371487970626d4d4c70384f5764746e535772586e5a4b770d0a6e762b55797932763646495253503739636b6f797367786e674336327550656c53384230536d73724c425a4169693274624e49584b622f51776f32647467534443697a4b6e4d51574e7757770d0a656c4a743279564a5a48644551434d4c53366645496b6f766868547777436533304649414b37634149395976723942437979797a30494c4a4d697257736f7778425267437a4b66637a6e744c0d0a4b51394173736f7474747843444c666237646e746e3639756c346d2b744b797969723637774e4a534c706a59596773786d4141544d584f7a2f4b4a4a415754436f67736d706141794444664b0d0a4d41744e74305871496f6f744a424e6a697969356342744d533668733869715272426135744c685275695231532b365a6b6b7779744c51303469326936474b4d4d446c783438682b7341686a0d0a4379715a6349506c4c6b382f4571557449377048536e6479646b31757755692b616a46625478662f494d7666736d6743754f4359724263674b36333038676b7778577a797943716c37507a410d0a787137304573777933493479793873464a447a356a61643855676f724f374d58722b454a39363236754b716e2f7572566e52762b4e4c6651794b363678647a43766e517455324c536e4f72620d0a75623736384d5350572f7a785337763953796e4964356433383942486a2f776a73376a536d6654595a362f393967566f38494c3334483876766759696a4543432b534e3458373449336f38770d0a7767736b6943432b2b2b4f4c514d4c38365839762f2f7665782f2f432f763254582f67474f443443457641464e5343494245727741673859514158666b77414f5443414247377976424269590d0a7741764578344948327338414e2f67654443346745424f38414155472b4d414c62704161455a7a41414341596e77774e75454878625442384363544142556a6f6767592b63494d53664d454a0d0a2f3344514168524d774158383232414844524144456f4251417955496751347677414d546e4141444b6779425731795941527034373459312f4f494d78336a44424949674253724967416f640d0a71414c76536442374b78694943634f3378425755414951494e4d414d2f6b634544586f6742444549775155794d4949506841434d4e447967445757597749466b4941552b394d4433654943440d0a373531674167594941517a457149454f686941444862794242684934412f493951494d71794351476875684a47594c786c574d3059414974734949453970434e514d5442467a3967414251510d0a73494d63554d4671516c6742413654416533313867516c4961494168476741484b45426b4c46323553444861306759324d4d41744454414247316941434a583048692b6a79636b4f797541460d0a4758686944433767416875456741652b7a434d4952704143626471516b32473859534c48313156496756515266746b3844516641313846343074454148486842422f443467684b34514341790d0a414638434f384248463452766d766945355263314d414954784d436a47784166435434616735434b4e416279512b524955327143456f41504269554634306a7a46774d4748684366336773490d0a4144733d0d0a WHERE name='SafePay'");

@mysql_query("UPDATE `".$prefix."ipn_merchants` SET buy_now='<form action=\"https://secure.payza.com/checkout/PayProcess.aspx\" method=\"post\">\r\n<input type=\"hidden\" name=\"ap_purchasetype\" value=\"Item\">\r\n<input type=\"hidden\" name=\"ap_merchant\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"ap_quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"ap_itemname\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"ap_description\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"apc_1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"apc_2\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"ap_amount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"ap_currency\" value=\"USD\">\r\n<input type=\"hidden\" name=\"ap_returnurl\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"ap_cancelurl\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[merchant_button]\" alt=\"Buy now using Payza\">\r\n</form>', subscribe='<form method=\"post\" action=\"https://secure.payza.com/checkout/PayProcess.aspx\">\r\n<input type=\"hidden\" name=\"ap_purchasetype\" value=\"subscription\">\r\n<input type=\"hidden\" name=\"ap_merchant\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"ap_itemname\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"ap_currency\" value=\"USD\">\r\n<input type=\"hidden\" name=\"apc_1\" value=\"[user_id]\">\r\n<input type=\"hidden\" name=\"apc_2\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"ap_quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"ap_description\" value=\"[sitename] - [item_name]\">\r\n<input type=\"hidden\" name=\"ap_amount\" value=\"[amount]\">\r\n<input type=\"hidden\" name=\"ap_timeunit\" value=\"[unit]\">\r\n<input type=\"hidden\" name=\"ap_periodlength\" value=\"[period]\">\r\n<input type=\"hidden\" name=\"ap_trialtimeunit\" value=\"[trial_unit]\">\r\n<input type=\"hidden\" name=\"ap_trialperiodlength\" value=\"[trial_period]\">\r\n<input type=\"hidden\" name=\"ap_trialamount\" value=\"[trial_amount]\">\r\n<input type=\"hidden\" name=\"ap_returnurl\" value=\"[return]\">\r\n<input type=\"hidden\" name=\"ap_cancelurl\" value=\"[cancel]\">\r\n<input type=\"image\" src=\"[merchant_button]\" alt=\"Subscribe using Payza\">\r\n</form>', defaultbutton=0x6956424f5277304b47676f414141414e53556845556741414146594141414157434149414141445662507a4d414141414358424957584d41414137454141414f784147564b7734624141414b0d0a54326c44513142516147393062334e6f6233416753554e444948427962325a706247554141486a616e564e6e56465070466a333333765243533469416c4574765568554949464a43693441550d0a6b53597149516b51536f67686f646b5655634552525555454738696769414f4f6a6f434d4656457344496f4b3241666b49614b4f67364f49697372373458756a61396138392b624e2f7258580d0a507565733835327a7a7766414341795753444e524e59414d715549654565434478385447346551755149454b4a4841414541697a5a43467a2f534d424150682b5044777249734148766741420d0a654e4d4c43414441545a76414d4279482f772f7151706c6341594345416342306b54684c43494155414542366a6b4b6d41454247415943646d435a54414b41454147444c59324c6a414641740d0a4147416e662b6254414943642b4a6c37415142626c43455641614352414341545a59684541476737414b7a50566f7046414667774142526d53385135414e67744144424a56325a49414c43330d0a414d444f454175794141674d4144425269495570414152374147444949794e344149535a41425247386c633838537575454f6371414142346d624938755351355259466243433178423164580d0a4c68346f7a6b6b584b78513259514a686d6b4175776e6d5a47544b424e412f6738387741414b435246524867672f5039654d344f7273374f4e6f3632446c3874367238472f794a695975502b0d0a35632b7263454141414f46306674482b4c432b7a476f4137426f42742f71496c3767526f586775676466654c5a724950514c55416f4f6e61562f4e772b483438504557686b4c6e5a3265586b0d0a354e684b78454a62596370586666356e776c2f41562f31732b5834382f506631344c37694a4945795859464842506a6777737a30544b55637a35494a68474c63356f39482f4c634c2f2f77640d0a30794c4553574b3557436f5534314553635935456d6f7a7a4d71556969554b534b63556c3076396b347438732b774d2b337a554173476f2b415875524c6168645977503253796351574854410d0a3476634141504b37623848554b41674467476944346339332f2b382f2f5565674a5143415a6b6d5363514141586b516b4c6c544b737a2f4843414141524b43424b724242472f544247437a410d0a42687a4242647a42432f78674e6f52434a4d544351684243436d534148484a674b617943516969477a6241644b6d4176314541644e4d42526149615463413475776c5734446a3177442f70680d0a434a37424b4c794243515242794167545953486169414669696c676a6a6767586d5958344963464942424b4c4a43444a69425252496b75524e556778556f70554946564948664939636749350d0a68317847757045377941417967767947764563786c49477955543355444c564475616733476f52476f6776515a4851786d6f38576f4a765163725161505977326f65665171326750326f382b0d0a51386377774f6759427a50456244417578734e4373546773435a4e6a7937456972417972786871775671774475346e3159382b7864775153675558414354594564304967595235425346684d0d0a57453759534b67674843513045646f4a4e776b44684648434a794b54714575304a726f522b635159596a4978683168494c435057456f38544c784237694550454e79515369554d794a376d510d0a416b6d787046545345744a47306d3553492b6b73715a733053426f6a6b386e615a477579427a6d554c4341727949586b6e6554443544506b472b5168386c734b6e574a4163615434552b496f0d0a5573707153686e6c454f553035515a6c6d444a4256614f615574326f6f5651524e59396151713268746c4b765559656f457a52316d6a6e4e67785a4a533657746f705854476d6758615064700d0a722b6830756848646c52354f6c39425830737670522b6958364150306477774e686857447834686e4b426d62474163595a786c33474b2b59544b595a3034735a783151774e7a48726d4f655a0d0a44356c765656677174697038465a484b4370564b6c53615647796f76564b6d7170717265716774563831584c56492b70586c4e39726b5a564d31506a71516e556c7174567170315136314d620d0a553265704f366948716d656f6231512f7048355a2f596b4757634e4d773039447046476773562f6a764d596743324d5a733367734957734e71345a31675458454a72484e325878324b7275590d0a2f523237697a327171614535517a4e4b4d31657a55764f555a6a3848343568782b4a783054676e6e4b4b65583833364b336854764b65497047365930544c6b785a567872717061586c6c69720d0a534b74527130667276546175376165647072314675316e376751354278306f6e584364485a342f4f425a336e55396c543361634b70785a4e5054723172693671613655626f627445643739750d0a702b36596e723565674a354d623666656562336e2b6878394c2f31552f573336702f5648444667477377776b4274734d7a68673878545678627a77644c3866623856464458634e41513656680d0a6c57475834595352756445386f3956476a5559506a476e47584f4d6b343233476263616a4a67596d49535a4c5465704e3770705354626d6d4b6159375444744d7838334d7a614c4e31706b310d0a6d7a3078317a4c6e6d2b6562313576667432426165466f73747169327547564a73755261706c6e757472787568566f355761565956567064733061746e61306c317275747536635270376c4f0d0a6b3036726e745a6e7737447874736d327162635a734f585942747575746d32326657466e5968646e74385775772b3654765a4e39756e324e2f5430484459665a447173645768312b633752790d0a464470574f7436617a707a7550333346394a62704c3264597a7844503244506a7468504c4b6352706e564f623030646e463265356334507a6949754a53344c4c4c70632b4c707362787433490d0a7665524b6450567858654636307657646d374f627775326f32362f754e753570376f66636e3877306e796d6557544e7a304d5049512b42523564452f43352b564d4776667248355051302b420d0a5a37586e4979396a4c3546587264657774365633717664683778632b396a35796e2b4d2b347a7733336a4c6557562f4d4e384333794c664c54384e766e6c2b4633304e2f492f396b2f33722f0d0a3051436e674355425a774f4a6755474257774c372b48703849622b4f507a72625a666179326531426a4b4335515256426a344b74677558427253466f794f79517253483335356a4f6b6335700d0a446f565166756a5730416468356d474c7733344d4a345748685665475034357769466761305447584e58665233454e7a33305436524a5a453370746e4d5538357279314b4e536f2b716935710d0a504e6f33756a5336503859755a6c6e4d3156696457456c73537877354c6971754e6d357376742f3837664f4834703369432b4e3746356776794631776561484f77765346707861704c6849730d0a4f705a415449684f4f4a5477515241717142614d4a6649546479574f436e6e4348634a6e49692f524e74474932454e634b68354f386b677154587153374a47384e586b6b78544f6c4c4f57350d0a684365706b4c784d44557a646d7a71654670703249473079505471394d594f536b5a427851716f68545a4f325a2b706e356d5a327936786c68624c2b7857364c747938656c51664a61374f510d0a7241565a4c5171325171626f56466f6f31796f48736d646c5632612f7a596e4b4f5a61726e69764e3763797a797475514e357a766e2f2f7445734953345a4b3270595a4c56793064574f61390d0a72476f35736a78786564734b347855464b345a5742717738754971324b6d335654367674563565756672306d656b317267563742796f4c4274514672367774564375574666657663312b31640d0a5431677657642b31596671476e52732b46596d4b72685462463563566639676f33486a6c4734647679722b5a334a533071617645755754505a744a6d366562654c5a3562447061716c2b61580d0a446d344e3264713044643957744f3331396b58624c35664e4b4e753767375a4475614f2f504c69385a61664a7a7330375031536b565052552b6c513237744c64745748582b473752376874370d0a76505930374e586257377a332f54374a767474564156564e315762565a66744a2b3750335036364a71756e346c7674745861314f6258487478775053412f30484977363231376e55315233530d0a505652536a3959723630634f78782b2b2f7033766479304e4e6731566a5a7a4734694e7752486e6b3666634a332f636544547261646f7837724f454830783932485763644c3270436d764b610d0a527074546d767462596c75365438772b30646271336e7238523973664435773050466c3553764e5579576e6136594c546b3266797a3479646c5a313966693735334744626f725a373532504f0d0a33326f50622b2b3645485468306b582f692b63377644764f58504b3464504b79322b5554563768586d71383658323371644f6f382f705054543865376e4c756172726c6361376e75657232310d0a65326233365275654e383764394c31353852622f317457654f54336476664e36622f6646392f58664674312b636966397a7375373258636e37713238543778663945447451646c44335966560d0a5031762b334e6a76334839717748656738394863522f6347685950502f7048316a77394442592b5a6a387547445962726e6a672b4f546e6950334c393666796e5138396b7a796165462f36690d0a2f7375754678597666766a563639664f305a6a526f5a66796c354f2f6258796c2f65724136786d76323862437868362b7958677a4d5637305676767477586663647833766f393850542b52380d0a4948386f2f326a3573665654304b66376b786d546b2f384541356a7a2f474d7a4c6473414141416759306853545141416569554141494344414144352f774141674f6b4141485577414144710d0a594141414f706741414264766b6c2f4652674141434a684a52454655654e72635747314d564e6b5a5075656565346635416f61426d554564566f703878745a516a51707231557846677749780d0a74626f4a336154393466356f593853304b544530616478433138516652724f74686931614e396c75706149726139477943416f4349734957465553526a2f4c6c4c41777a7a423047357574380d0a3963653146464442625a6f596658497a756665384f653939357a6e7663393733487367354a3454593766615a6d5a6c514b4154655867535a2f773839763154756f37575756505061393739660d0a4542356d67426a6a6e703665364f686f6f39476f55716e41573432434c3363767379514c45416b632b4879795178372b634d635a6358523031476730476f314751736a626e515541674f57360d0a2b4d65754c6769684a4568365657534533767a35507a385750523550636e4a794d42683852532f546f536d394b75494e7063436f6957574278344144796d695142694d6c7737326e4c534b6c0d0a6c4244434f56396b5a736659335a74442f3367792b64415464414d494141636d6a575674624f61757844316d3362493369414931306c44475a6838393344506a6e78493535327a4f36414a4d0d0a4265586a72622f723958524c6f6f6751306d6f31436756654c74654e566e30312b4f58656c4a2f7553586e2f5461456744476b5a6f374f506d44464b7163673566316b4b4d4d352b333354340d0a472f2b514a6b7753454249676a4e5774304571365565395169416152414a45414b78352f4367442f55664a2f575343456c4a6558333774337a2b667a425149426f3947346566506d6e4a77630d0a555252664f7755435235545475534f45734d556f614279754735377056367456584144664d53542f61754f484674317978686a6e76476579363378335762667a76675445767a3336644b4e310d0a797a4b4e565a6b56434151714b696f6b53544959444661727462753775362b7672373239766269342b4c56544d423263496d77654255746b516333414653674178686e42354e4436333061480d0a6d57565a566c5154692b494f72546c79704f585169486341434c7868754f596e61523951536745416e484f456b43694b71616d704a53556c70302b66727179736650446767537a4c5471657a0d0a6f6146686348445137585a624c4a5a7432375a6c5a6d613274625864766e3062414c4231363962303950546d35756232396e59415148352b2f766e7a352b66476b352b6672395070717175720d0a2b2f763752305a4764447064656e7236377432377838624748413748786f30626c36526763735a4a325949736f49767442613641677747474b5361594c412b506337766479702f38443642560d0a48543867393344415850344a6c556f314d7a4d444146445368464b4b4d63595953354c4547494d51756c79752b2f6676583770304353454549527761476d7075626934704b644671745455310d0a4e5242436c55713162743236717171717a73374f2b5068346a555a5455314d6a43494c79737069596d4d4f4844342b506a35383764303451424167683537797a73334e67594b4377734e42730d0a4e6f756975475252743775484b4a79664259514b2f4f5541414243474d63556867736538647156777a474936354c307a3268676947444f7930704367784b51415978774b6851676866722b2f0d0a70615746454949515772567156565a57566d6c7061564e5455323174625652556c43694b64585631613961734d5a764e434b472b766a35524648743765775642794d374f4e70764e4a302b650d0a4c436f715167676868486275334b6c57712f56362f64476a522b767136686f62472f667533597351616d707130756c3061725536464172787064446e654551596d5863524b69714c396b4c4f0d0a4969586a6d483855516b67774b32302b3855463667556f49553078335270722b335046484a335a4959574b596f4d354e335474625752554b4f4f654e6a5930326d7730684a456c53595747680d0a775743676c4c6131745a57566c5932506a7a756454676968302b6c55713957624e6d3271724b7a7337653374372b2b666e703547434e6c734e6b7070516b4a4364585731346a6b6e4a3866760d0a3933504f4e52724e69524d6e4f6a6f3658433458593477783576563652564663704b3470474845502b4b67664d476d654543686254416a664e613774636e59494175534d58583330525858580d0a333566723477414839756e5249504e4c616c4653695179445839754b4465716f69596b4a7851396a544b484162445a6e5a47516b4a5357745837382b49534842342f45634f584b6b7672342b0d0a4f6a70363337353931645856446f654455676f687a4d7a4d76484468416d5073367457726c4e4c342b506934754c694a69516d667a36645173487231616d586b31713162786358466e504e640d0a7533596c4a695a657633366445504b736669314677623252316a434e316b6544333049495739374a3467515351696e6a6767674a4367314f3977334f394745556c4e5149497267693470336a0d0a4f383973543837786544795530746d4a6842434d635878386646465255563565586b784d7a4d5445784e4451304930624e775242324c392f2f3445444231517146635a593256775345784e4e0d0a4a684e6a724c362b6e6843795938634f51676768704b476859587036476d4f636c35656e4b4f76697859754d7361536b704750486a71576c70536d442f4e567776627453306d6f595a584f760d0a5a39766879345267306c717933736d3939712f4c45414b6a7870526d587532636d56424d4b6161304c516c5a37385a7668527936584b36352f54566a54416c4c49554b57355765646d566f740d0a43414c6e764c6132316d713179724a4d434647576a6e4f656b5a4652555646687439735a59397533622f6635664a7a7a71716f715261654e6a5930744c53335a32646b47677745414d4477380d0a66506e79355364506e737a3173486944322b666f4870753271794d694b5748666f69494141483632357565506e412f3633552f454d48513839784f4d386577576a5447656b716343676343430d0a647a50476c4c5656666d656461375861676f4b43733266507472613274725330694b4949496252594c456f414e707574764c78634549536b704b53564b31654f6a59304e44773933644852490d0a6b73513576336274477156307734594e4277386548426b5a36652f764c79777346415142495751304769564a6d7558695a666a387a6966364b4b4d633944444f46676842584a79384d4448730d0a493976487831744c6d7676714c3337393130334c625576575871315757317061616a4b5a39487239416c4e4f546b35756271374c355a4a6c325767306d6b776d69385869382f6b41414371560d0a6968436931414b4d4d53456b49694b69724b77734a695a6d64727256616d574d6e546c7a5a6e4a793075313268346548713958716c4a515553716e443456676b704e377868772f484f2f52780d0a306353504635676f7066446d7a5a734a43516c4c394a5743384d44313965574f696857366c58752b393535424737556b4563725267794b4b425359496f53524a6769416f2b61496b79366c540d0a70796f714b694345396658313465486855314e54796e7358744e577a7179314a456b4a496b516e47654a4649434d572f2b4f773947436c4d4953396843344f522b385a466842436c644461390d0a5877684b365770442b767163642f563666536755386e71393833756b4678335276507a726d334f2b774f727a2b613563756349355430314e6a59754c652f7230715a4b62737751396a31632f0d0a327668547733476f67554556435161656d384b415671574458563164684a4449794d6a58324c7237664c37523056474c7852495445324d796d6578322b2f2f4c63336e72326161524f686f4f0d0a4844506a6a43396b453373435736772f684d4667384f3764753171744e6a77382f44562b7a416d4349456d5373764c50612b642f7778667466326b592b6f7270344c6a2f47307a6e695955520d0a4770727961344c714b37397068707a7a55436a553239737279334967454868727a7367612b717650747035346d54553261735550556d3146502f346f4e6d7246767763414a467a6b537951450d0a71496741414141415355564f524b35435949493d0d0a WHERE name='Payza'");

@mysql_query("UPDATE `".$prefix."ipn_merchants` SET buy_now='<form action=\"https://www.2checkout.com/2co/buyer/purchase\" method=\"post\">\r\n<input type=\"hidden\" name=\"sid\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"description\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"product_id\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"fixed\" value=\"Y\">\r\n<input type=\"hidden\" name=\"user_id\" value=\"[user_id]\">\r\n<input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy from 2CO\">\r\n</form>', subscribe='<form action=\"https://www.2checkout.com/2co/buyer/purchase\" method=\"post\">\r\n<input type=\"hidden\" name=\"sid\" value=\"[merchant_id]\">\r\n<input type=\"hidden\" name=\"quantity\" value=\"1\">\r\n<input type=\"hidden\" name=\"description\" value=\"[item_name]\">\r\n<input type=\"hidden\" name=\"product_id\" value=\"[item_number]\">\r\n<input type=\"hidden\" name=\"fixed\" value=\"Y\">\r\n<input type=\"hidden\" name=\"user_id\" value=\"[user_id]\">\r\n<input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy from 2CO\">\r\n</form>', defaultbutton=0x52306c474f446c685a514165415063414145684953466859574742675947686f61484277634868346549434167496949694a43516b4b43676f4c4377734c6934754d4441774d6a49794e44510d0a304e6a59324f4467344f6a6f3650447738502f2f2f3569516b4b69676f4e4441774f6a67344e6a49794741344f4d69676f4f4449794f4441774f4341674f69676f4b6767494f6749435041490d0a4350384943502f77385038594550386747503949514f686757504341654d687759503877434d6834615042675150417743503941434e4334734e6a41754e42594d50394943502f6f344f69410d0a574942494d4a42494b4e6a49774c4341614f6a59304f4441734f6a49755039344d502b6763502f5175502f67304a68514b5042774b5042674550396741502f49714d4351634f6a51774b42590d0a4b4f696f674d4267494f426f475043515550396f43502b6759502b6f634d4349594e6967654b6859494e686f4750397743502f6f324f43346d4f6a416f4d4267475043595550393445502b410d0a494e4359614d6941514f4377694f696759504334694d686f47504351514f6834454b6959694f4459304d4351594f6a51754f43595550434145502f77344f6a416b502f49694e4377694c69410d0a4f502b6f4f502b6f4d504359454f44496f504441634e6a5177504477364f6a6f324c4334754d4449794e4459324f446f3650442f2f3269496b4669516f47435971424267654843496b4869510d0a6d4b4334774669496d47695971454277674368776943426f6742686765486967734468346b4442776943426f694b6934774743416b4843516f4668346949436773474351714542776943686f0d0a69484359734d4449304d6a51324e44593449435975496967774a4367754a696f774b4377794542676b48434973444251694c4334794c6a4130496959754d6a51344668776f4a696f794368490d0a694342416748694973494351754568676d4542596b42673467424177654468516b454249594b6977794742776f4c43343047683471484341734241595747686f63474267614b4367714b696f0d0a734c437775484277674943416b474267634a43516f4d6a49304a695971484277694868346b4b696f7547686f674943416d4e6a5934466859634c69347947686f6945684959474267674d44410d0a304542415746425163474267694868346f446734554568496146685967454241594c6934304f6a6f3843676f59424151554267515544416f59434159554367675544676f5944676f554367590d0a51436751534567345744675953446751534e4449304e6a51324f44593445416f5145415151466841554667514f4d437775464177514e4334774767674f4841514d496777534d696f734e69770d0a754c4251594a41514b4e6967714d4341694c67674d4b4151494d4267614d4251574d6849554c6751474d4151474e4149454f41494543483542414141414141414c4141414141426c414234410d0a41416a2f41414d594f4543776f4d47444341744173485374466a6148324c4a4a66456778596a614b4471385a6d2f43676f30655045446f61432f6e526f774d4a4578536f584d6d79705575560d0a43513445713551706b7969624f472f717a496b7a314149496f786f2b4850634f487a3536367344567973624f7870456a4e63356c7131564c4938634835547a2b71755341713965755844316d0d0a52646c6767746d7a614e4f716e5141683136354e724f4c4b6e557558727165664468706d652b65506841692f4966713569354946545a6243536d7073773261315a4b564569694958556a52350d0a73694a484a636d7533627a32516134436d553631596a57364e4f6e54706b76664264727758516a416630574963434845734f33434e7259312f6e696e6b43452f76344d4439395049675669790d0a456877596b7741685a48493849333551675342427767506a79694e4171424173314b7859733069462f783876766a7a3557624e4b2f6278306264772f7637466c74354168704c4239773249790d0a37505a59715a41665034637338743976774431696e45646b51534141415167554d41414375424241775263394e44464741676b55494141774177696767476346574b5048424e59776b6b6b650d0a6c35525369697979654a4c4b4a586c6b4973736c456f7a3430796a6130464d4343535745384e64724c72546741684e6161484562476b6c6f6b3864562f43565379434b4652506c6b6c4973550d0a3978465a4478545151414332474544414141334d30415152583041787741414a4149414141414538414145757761426b4669656658454a4c4b703934736b6f716e467953436975654c476b570d0a5874336334304548484a786767676b6e6f45414443797a51454963615459534242525a49514b484d52735a345a4977446731415343522b4944444a49716162756357424857417051514147320d0a4950385151414547564943454530373438436f44416542797070747579546c424b4b6155516771646e4b7843796957586b4c496e576a643273303835474842517a7759626141444442764a770d0a30493454584d417742775a47774f42464f6874355a413445654641534b6957495141497648364b7132696d72457a51677751494d574f5041416774596f384143462f686778416834504241420d0a76773434304679473170775679696d6479494b4b4b74614d6f6977747a733479347141514f4e444e504f7459554b304638577851547a73563147505046315667344d414c4f75694168544c6d0d0a4d4e6d5241346b73516d3866706b62794c69444b495a697657722f6b736b41444379687452775736434476424346614d495177645a3430437144576475496a4a4b70326f77676b70704168370d0a6f7a6277374c4f5050766e6b6b30492b4a36535177676b78464c464645555745386351575848542f4134484f7876526e43422b6d77767375496f36732b6f426d61566e6a5a67515275416e420d0a53476a35674555514f4f7753775168706a4144424d4242456f4573354377786a42774f2f6a4d413035794f4d734e3431367277576e7767687143446b665965686755594e326e443655582b480d0a4b4a49495a4d4d58722b715652334f323167672b4e4448464545706b546f516262377a68427658585a352b393964645876345964433132544454342f2b6d552b665563616c7351332b3358550d0a4834414143726a49495145616948785a79714d314178464c5044384539452f5968525867514d4143477643414235544436326f42446e7a344b445968414d4548784a432b4b37436a4b6844510d0a772b3859555967412b554641682f6a67663679456c63556c54336c7038494554654d43452f2f3176436d4c496e42586541416361317043414e4d7a68445850596869346f67445654416363370d0a2f2f6a686a332f3434775071714d5535674141474d55676843545867786b50614277704743454953574d796946762b7771704f634d43306a6f414952656841474a7254516866364c4867486d0d0a45416372784f474e6349796a484f453468317945544274556f557037784d474e626454436a397667686a6569345565715a4f4d615379716853527241794559366f4a454e4549734a475143350d0a537635674230685951684345304549682f45384a4c67536c46484477693545303535536f544b55716f534f425554446a6c62434d7053786e4f517155564f6557754c526c64536151793175610d0a705a4b517530414f6a4f41444d786a5444447334706a4b4e61515149574f4f5a304c5347424a34357a576861383572597a4b593274536d425045417a417453304a6a69783263317469684d610d0a3645796e4f74664a7a6e6136383533776a4b633835776e505a7954446e766938707a377a79662f5066656f7a4766384571442f744b644343457253674344556f51412f617a345975644b41510d0a66595a454a307252696c72556f744f596145597a696c474a426b4f69475532474d4361616a46676b5936505051476c4b515872526c6e4b3070525364786a476d49564f617a72536d4f4c32700d0a546d314b55353732394b632f5451597a4c50474c59457a444638793478432b454d59784c6b4b4d61526757715648507130353332314b70576e59597a74737256726e723171324231686c62440d0a796c566649494d59767742474d6934426a4755455178655743455979644d474d59354431726c34644b3136374b6731704f4d4f766750327259414e4c324d45437471392f505778692f33714d0d0a597954444163487778532b342b67746639445559786e6a475968484c32634a3674724f44446531685879454e30707132744b6739725770547939725672705961783243414c3134684441657a0d0a48494d61796d41414c6b6f726a4163637737577444533577683876615a6c434447735939626e4b5265317a6d4b7265357934337563354e7233475a4951786351414559776e4d4541586644430d0a4638427767432b4b515135674f4665367a30327663382b72587651694e7872776a613938353076662b74715876723059686936384777316e2b474959774841474d58435243324530343734490d0a5472434346387a67426b634474395477717a496d5447486a55746a42474d36776868744d4457636f6f374566397243486d2b48685979686a7779684f7359716a635744357468692b4c3136780d0a6a47644d34786f764f4341414f773d3d0d0a WHERE name='2CheckOut'");

$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."templates` where template_name='comms_header'"), 0);
if ($checkexists == 0) {

@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `template_data`, `template_type`, `template_title` ) 
VALUES (
'comms_header', '<p align=\"left\"><font size=\"3\">You can earn commissions by promoting #SITENAME# using the tools on our Affiliate Toolbox page.  Be sure to enter your PayPal address on your Profile page.</font></p>', 'page', 'Commissions Page Header'
) ;");

@mysql_query("INSERT INTO `".$prefix."templates_defaults` (`template_name`, `template_data` )
VALUES (
'comms_header', '<p align=\"left\"><font size=\"3\">You can earn commissions by promoting #SITENAME# using the tools on our Affiliate Toolbox page.  Be sure to enter your PayPal address on your Profile page.</font></p>'
) ;");

}

// Pay With Commissions Mod
$checkinstalled = mysql_query("Select COUNT(*) from `".$prefix."ipn_merchants` where name='Comms'");
if (mysql_result($checkinstalled, 0) > 0) {
	// Reset existing values
	
	@mysql_query("Update `".$prefix."ipn_merchants` set buy_now='<form action=\"/paywithcommissions.php\" method=\"post\"><input type=\"hidden\" name=\"itemnumber\" value=\"[item_number]\"><input type=\"hidden\" name=\"itemname\" value=\"[item_name]\"><input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy With Your Commissions\"></form>', subscribe='<form action=\"/paywithcommissions.php\" method=\"post\"><input type=\"hidden\" name=\"itemnumber\" value=\"[item_number]\"><input type=\"hidden\" name=\"itemname\" value=\"[item_name]\"><input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy With Your Commissions\"></form>', defaultbutton=0x52306c474f446c6853774166414d5141414f372b414f2f76415044664150485041504b2f41504f76415053664150575041505a2f4150687741506c67415070514150744141507777415030670d0a415034514150384141502f2f2f7741414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141434835424145410d0a414245414c4141414141424c414238414141582f4943534f5a476d65614b7175624f752b63437a50644930655248347763494749436f4a4368436730434937486f51454a506d7970426942410d0a4146683572716c494142434972417441677748344951425971496c4d45425541427767446b5741754671494834676b5a694b5657446d7750435951414267316e43776b4a446d6f6b624349470d0a5a573957414173454155384861434b636a41425643676c6c5a365257424765576d6f38695a46526341413648454745466e454d4241534e68426d396b4262396e4447526d4141554f666d6c710d0a727a6b46654134486671477a7a33416a443130424178414341567751773858686e634f746371456b73774630365656766a695054635a7a70346d586b504f61746b53504144704f344b6244430d0a6a5551395047487733535041674a4f2b547132797463453262594166455145414449476b42634b446a4578494d66686f5a5a4964694b30612f2f4342784f524253313072527a5349423845420d0a45316435474469364359486e4f525943754d54355362524641674d4a69697064797253705578454f4669324c694b654641706f7a5642474d71576166696c6b4761704135684f6e597a7763620d0a575679745557586f6736433159686c776553776a6758704442466a7367734375495a75784344787755415841674a6e34466d5345512f65413377654642544468736d79774c675256444977310d0a774957644651696f43682b67504d7a50676b6c4841594453484f6f56416a3849786f347577366c646d304459444d584a687359657669716741547949644162427344634445436851307558410d0a56545a6e6873775351435a4f6d485943446a547163326c45304f686265736342442f777a38564a6f486e43324d675242724232684a6d4735676d2f633635495153456c6d586d4267675a35540d0a324e644a6551436730305a7870595646796d78646251444342696c68446352516662514e51466542334b334368422b4c4c534167442b58744167773543517854785637474766594f4d4139770d0a654d5534785a7768514255544e584741442f45515973414f486d6e576b77474f6e4a5a6655684434434a424e632b6e6f484241484943554a44345434774d535254397149464664505a616e6c0d0a6c6c783236535555495141414f773d3d0d0a where name='Comms' limit 1");
	
} else {
	// New install
	
	@mysql_query("INSERT INTO `".$prefix."ipn_merchants` VALUES (NULL, 'Comms', '', '', '<form action=\"/paywithcommissions.php\" method=\"post\"><input type=\"hidden\" name=\"itemnumber\" value=\"[item_number]\"><input type=\"hidden\" name=\"itemname\" value=\"[item_name]\"><input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy With Your Commissions\"></form>', '<form action=\"/paywithcommissions.php\" method=\"post\"><input type=\"hidden\" name=\"itemnumber\" value=\"[item_number]\"><input type=\"hidden\" name=\"itemname\" value=\"[item_name]\"><input type=\"image\" src=\"[merchant_button]\" border=\"0\" name=\"submit\" type=\"submit\" alt=\"Buy With Your Commissions\"></form>', 0x52306c474f446c6853774166414d5141414f372b414f2f76415044664150485041504b2f41504f76415053664150575041505a2f4150687741506c67415070514150744141507777415030670d0a415034514150384141502f2f2f7741414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141434835424145410d0a414245414c4141414141424c414238414141582f4943534f5a476d65614b7175624f752b63437a50644930655248347763494749436f4a4368436730434937486f51454a506d7970426942410d0a4146683572716c494142434972417441677748344951425971496c4d45425541427767446b5741754671494834676b5a694b5657446d7750435951414267316e43776b4a446d6f6b624349470d0a5a573957414173454155384861434b636a41425643676c6c5a365257424765576d6f38695a46526341413648454745466e454d4241534e68426d396b4262396e4447526d4141554f666d6c710d0a727a6b46654134486671477a7a33416a443130424178414341567751773858686e634f746371456b73774630365656766a695054635a7a70346d586b504f61746b53504144704f344b6244430d0a6a5551395047487733535041674a4f2b547132797463453262594166455145414449476b42634b446a4578494d66686f5a5a4964694b30612f2f4342784f524253313072527a5349423845420d0a45316435474469364359486e4f525943754d54355362524641674d4a69697064797253705578454f4669324c694b654641706f7a5642474d71576166696c6b4761704135684f6e597a7763620d0a575679745557586f6736433159686c776553776a6758704442466a7367734375495a75784344787755415841674a6e34466d5345512f65413377654642544468736d79774c675256444977310d0a774957644651696f43682b67504d7a50676b6c4841594453484f6f56416a3849786f347577366c646d304459444d584a687359657669716741547949644162427344634445436851307558410d0a56545a6e6873775351435a4f6d485943446a547163326c45304f686265736342442f777a38564a6f486e43324d675242724232684a6d4735676d2f633635495153456c6d586d4267675a35540d0a324e644a6551436730305a7870595646796d78646251444342696c68446352516662514e51466542334b334368422b4c4c534167442b58744167773543517854785637474766594f4d4139770d0a654d5534785a7768514255544e584741442f45515973414f486d6e576b77474f6e4a5a6655684434434a424e632b6e6f484241484943554a44345434774d535254397149464664505a616e6c0d0a6c6c783236535555495141414f773d3d0d0a)");
	
}
// End Pay With Commissions Mod

// Add Admin Menu Item
$checkmenuid = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."adminmenu` where id=1535"), 0);
if ($checkmenuid == 0) {
	$menuidnum = 1535;
} else {
	// Try alternate ID
	$menuidnum = 99999 - 1535;
}

$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."adminmenu` where menu_label='Purchased Pages'"), 0);
if ($checkexists == 0) {
mysql_query("INSERT INTO `".$prefix."adminmenu` (
`id` ,
`menu_label` ,
`menu_url` ,
`menu_target` ,
`menu_parent` ,
`menu_order` ,
`f` ,
`filename` 
)
VALUES (
'".$menuidnum."', 'Purchased Pages', 'admin.php?f=purchasedpages', '_top', '1500', '7', 'purchasedpages', 'purchasedpages.php'
);");

mysql_query("ALTER TABLE `".$prefix."adminmenu` ORDER BY id ;");
}
// End Add Admin Menu Item

// End v2.14 Updates


// v2.16 Updates

@mysql_query("INSERT INTO `".$prefix."surf_admin_prefs` (`field`, `value`) VALUES ('surficontype', '-1');");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."barmods_ajaxin` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."barmods_ajaxout` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."barmods_body` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."barmods_framesite` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."barmods_header` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

// End v2.16 Updates


// v2.17 Updates

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."loginmods` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."signupmods` (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `enabled` INT(11) NOT NULL DEFAULT '1',
 `filename` VARCHAR(50) NOT NULL DEFAULT 'file.php'
) ENGINE=MYISAM ;");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `m_spon_did` INT(11) NOT NULL DEFAULT '0'");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `trwvid` VARCHAR(100) NOT NULL DEFAULT ''");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `trwvseries` VARCHAR(100) NOT NULL DEFAULT ''");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."customfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT 'None',
  `type` int(11) NOT NULL DEFAULT '1',
  `options` varchar(255) NOT NULL DEFAULT 'None',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `onsignup` tinyint(1) NOT NULL DEFAULT '1',
  `onprofile` tinyint(1) NOT NULL DEFAULT '1',
  `searchable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rank` (`rank`),
  KEY `required` (`required`),
  KEY `onsignup` (`onsignup`),
  KEY `onprofile` (`onprofile`),
  KEY `searchable` (`searchable`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."customvals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `fieldvalue` varchar(255) NOT NULL DEFAULT 'None',
  PRIMARY KEY (`id`),
  KEY `fieldid` (`fieldid`),
  KEY `userid` (`userid`),
  KEY `fieldvalue` (`fieldvalue`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

// Extra function for custom field macros
$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."extra_funcs` where include_path='custom_macros.php'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."extra_funcs` (`type`, `include_path`) VALUES ('user', 'custom_macros.php')");
}
// End extra function for the custom field macros

// Add Admin Menu
$checkdata = mysql_query("Select COUNT(*) from `".$prefix."adminmenu` where id='822' or menu_label='Custom Fields'");
if (mysql_result($checkdata, 0) == 0) {
	@mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES (822, 'Custom Fields', 'admin.php?f=cfields', '_top', 300, 3, 'cfields', 'customfields.php');");
}

// End v2.17 Updates


// v2.20 Updates
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `cantransfer` TINYINT(1) NOT NULL DEFAULT '1' ;");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `mintransfer` INT(11) NOT NULL DEFAULT '10' ;");
@mysql_query("ALTER TABLE `".$prefix."membertypes` ADD `maxtransfer` INT(11) NOT NULL DEFAULT '0' ;");
// End v2.20 Updates


// v2.21 Updates
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `reqrever` INT(11) NOT NULL DEFAULT '1' AFTER `reqemailver`");
// End v2.21 Updates


// v2.22 Updates
@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."autotrack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timehit` int(11) NOT NULL DEFAULT '0',
  `ipaddress` varchar(40) NOT NULL,
  `srtrkdm` varchar(200) NOT NULL,
  `srtrkid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timehit` (`timehit`),
  KEY `ipaddress` (`ipaddress`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
// End v2.22 Updates


// v2.23 Updates

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."ipn_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

// Add Category To Products
@mysql_query("ALTER TABLE `".$prefix."ipn_products` ADD `catid` INT(11) NOT NULL DEFAULT '0' AFTER `id`");
@mysql_query("ALTER TABLE `".$prefix."ipn_products` ADD INDEX(`catid`)");

// Add Custom Return URL and Email To Products
@mysql_query("ALTER TABLE `".$prefix."ipn_products` ADD `return_url` VARCHAR(150) NOT NULL, ADD `email_subject` VARCHAR(150) NOT NULL, ADD `email_body` TEXT NOT NULL");

$defaultreturn = mysql_result(mysql_query("SELECT value FROM `".$prefix."ipn_vals` WHERE field='return'"), 0);
$defaultsubject = mysql_result(mysql_query("SELECT value FROM `".$prefix."ipn_vals` WHERE field='memsubj'"), 0);
$defaultbody = mysql_result(mysql_query("SELECT value FROM `".$prefix."ipn_vals` WHERE field='membody'"), 0);
mysql_query("UPDATE `".$prefix."ipn_products` SET return_url='".$defaultreturn."', email_subject='".$defaultsubject."', email_body='".$defaultbody."'");

// End v2.23 Updates


// v2.24 Updates

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."products_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catname` varchar(50) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

// Add Category To Products
@mysql_query("ALTER TABLE `".$prefix."products` ADD `catid` INT(11) NOT NULL DEFAULT '0' AFTER `productid`");
@mysql_query("ALTER TABLE `".$prefix."products` ADD INDEX(`catid`)");

@mysql_query("ALTER TABLE `".$prefix."settings` ADD `delsusp` INT( 11 ) NOT NULL DEFAULT '365' ;");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `nodelpaid` INT( 11 ) NOT NULL DEFAULT '1' ;");

// End v2.24 Updates


// v2.27 Updates

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."barmods_topbar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  `modtype` int(11) DEFAULT '0',
  `modname` varchar(200) NOT NULL,
  `moddescr` text NOT NULL,
  `filename` varchar(200) NOT NULL DEFAULT 'None',
  `html` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `state` (`state`),
  KEY `modtype` (`modtype`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."barmods_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  `modtype` int(11) DEFAULT '0',
  `modname` varchar(200) NOT NULL,
  `moddescr` text NOT NULL,
  `filename` varchar(200) NOT NULL DEFAULT 'None',
  `html` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `state` (`state`),
  KEY `modtype` (`modtype`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."barmods_topbar` WHERE modname='Surf Icons'"), 0);
if ($checkexists < 1) {
	mysql_query("INSERT INTO `".$prefix."barmods_topbar` (`id`, `state`, `rank`, `modtype`, `modname`, `moddescr`, `filename`, `html`) VALUES
			(1, 1, 1, 1, 'Surf Icons', 'The icons members click to get credit', 'surficons.php', ''),
			(2, 1, 2, 1, 'Surf Timer', 'The surfbar timer', 'surftimer.php', ''),
			(3, 0, 3, 2, 'Classic Layout', 'Aligns the surfbar in a classic layout', 'classiclayout.php', ''),
			(4, 1, 4, 2, 'Banner and Text', 'Displays a member banner and text ad', 'bannertext.php', '');");
			
	mysql_query("INSERT INTO `".$prefix."barmods_extensions` (`id`, `state`, `rank`, `modtype`, `modname`, `moddescr`, `filename`, `html`) VALUES
			(1, 1, 1, 2, 'Status and Menu', 'Shows the status text, plus links to report a site, open in new window, etc.', 'surfmenu.php', '');");
}

@mysql_query("ALTER TABLE `".$prefix."surf_admin_prefs` CHANGE `value` `value` VARCHAR( 50 ) NOT NULL DEFAULT '0';");

@mysql_query("TRUNCATE TABLE `".$prefix."surf_user_prefs`");
@mysql_query("TRUNCATE TABLE `".$prefix."surf_admin_prefs`");

@mysql_query("INSERT INTO ".$prefix."surf_admin_prefs (field, value) VALUES
	('surfbarstyle', '2'),
	('preloadsites', '-1'),
	('timertype', '1'),
	('timercolor', '0000FF');");

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."extra_funcs` WHERE include_path='macros_extended.php'"), 0);
if ($checkexists < 1) {
	mysql_query("INSERT INTO `".$prefix."extra_funcs` (`type`, `include_path`) VALUES ('user', 'macros_extended.php');");
}

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."adminmenu` WHERE id='2102' OR filename='macros_list.php'"), 0);
if ($checkexists < 1) {
	mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES ('2102', 'Macros List', 'admin.php?f=macros', '_top', '2100', '1', 'macros', 'macros_list.php');");
}

@mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."upsells` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openkey` varchar(50) NOT NULL,
  `offername` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `openkey` (`openkey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."adminmenu` WHERE id='1152' OR filename='upsells.php'"), 0);
if ($checkexists < 1) {
	mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES (1152, 'Upsell Offers', 'admin.php?f=upsells', '_top', 900, 7, 'upsells', 'upsells.php');");
}

// End v2.27 Updates


// v2.29 Update

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."loginmods` WHERE filename='bounceemail.php'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."loginmods` (`enabled`, `filename`) VALUES ('1', 'bounceemail.php');");
}

$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."templates` where template_name='bouncetesteml'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `template_title`, `template_data`, `sflag`, `template_type`) VALUES ('bouncetesteml', 'Bounce Test Email', 'Hello #FIRSTNAME#, This email is a system generated email sent to you as a test. You are receiving it because recently an email you''d been sent by #SITENAME# bounced when delivered to you. If you have received this email then your account will automatically be updated to working. However, you may need to take further actions to ensure you continue receiving updates from #SITENAME#. 1.) Ensure that your email inbox is not full. 2.) Ensure that #SITENAME# is added to your email''s contact list. 3.) Ensure that your email account is not set to vacation mode or auto-reply.', '1', 'mail');");
}

$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."templates` where template_name='bounce_page'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."templates` (`template_name`, `template_data`, `template_type`, `template_title` ) VALUES ('bounce_page', '<h2>Attention: Your email address requires attention</h2><p>Your email address at #BOUNCEADDRESS# has been bouncing our messages.  You must update your email address to continue using #SITENAME#.</p>', 'page', 'Bounced Email Page') ;");
	@mysql_query("INSERT INTO `".$prefix."templates_defaults` (`template_name`, `template_data` ) VALUES ('bounce_page', '<h2>Attention: Your email address requires attention</h2><p>Your email address at #BOUNCEADDRESS# has been bouncing our messages.  You must update your email address to continue using #SITENAME#.</p>') ;");
}

@mysql_query("ALTER TABLE `".$prefix."members` ADD `lastbounce` INT NOT NULL AFTER `mailbounce` ;");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `bouncetest1` INT NOT NULL DEFAULT '1' ;");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `bouncetest2` INT NOT NULL DEFAULT '5';");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `lastbouncecron` INT NOT NULL DEFAULT '0';");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `bouncemode` INT NOT NULL AFTER `lastbounce` ;");
@mysql_query("ALTER TABLE `".$prefix."members` CHANGE `bouncemode` `bouncemode` INT( 11 ) NOT NULL DEFAULT '0';");
@mysql_query("ALTER TABLE `".$prefix."members` ADD `bouncetp` INT NOT NULL AFTER `bouncemode` ;");
@mysql_query("UPDATE ".$prefix."members SET bouncemode=0;");
@mysql_query("UPDATE ".$prefix."members SET bouncetp=0;");
@mysql_query("ALTER TABLE `".$prefix."settings` ADD `bouncelimit` INT NOT NULL DEFAULT '0';");

$checkexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."adminmenu` where f='bouncesettings'"), 0);
if ($checkexists == 0) {
	@mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES ('332', 'Bounce Settings', 'admin.php?f=bouncesettings', '_top', '300', '6', 'bouncesettings', 'bounce_settings.php');");
}

// End v2.29 Update


// v2.31 Update

mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."memgrid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(30) NOT NULL,
  `value` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `field` (`field`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;");

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."memgrid`"), 0);
if ($checkexists < 1) {
	mysql_query("INSERT INTO `".$prefix."memgrid` (`field`, `value`) VALUES
	('showgrav', '1'),
	('showid', '1'),
	('showfirst', '1'),
	('showlast', '1'),
	('showusername', '1'),
	('showemail', '0'),
	('showpaypal', '0'),
	('showmtype', '1'),
	('showjoined', '0'),
	('showlogin', '1'),
	('showcredits', '1'),
	('showbanners', '0'),
	('showtexts', '0'),
	('showcomm', '1'),
	('showcommp', '0'),
	('showsales', '0'),
	('showrefs', '1'),
	('showclicks', '1'),
	('showclicksyester', '0'),
	('showstatus', '1'),
	('showcustom', '');");
}

@mysql_query("ALTER TABLE `".$prefix."members` ADD `tempcommp` float(6,2) NOT NULL default '0.00' ;");

mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."customipnvals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldid` int(11) NOT NULL DEFAULT '0',
  `ipnid` int(11) NOT NULL DEFAULT '0',
  `fieldvalue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fieldid` (`fieldid`),
  KEY `ipnid` (`ipnid`),
  KEY `fieldvalue` (`fieldvalue`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;");

@mysql_query("ALTER TABLE `".$prefix."customfields` ADD `onipn` TINYINT(1) NOT NULL DEFAULT '0' ;");

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."ipnmods` WHERE filename='ipn_cfields.php'"), 0);
if ($checkexists < 1) {
	mysql_query("INSERT INTO `".$prefix."ipnmods` (`enabled`, `filename`) VALUES ('1', 'ipn_cfields.php');");
}

@mysql_query("ALTER TABLE `".$prefix."ipn_products` CHANGE `productid` `productid` VARCHAR(255) NOT NULL DEFAULT '0';");

mysql_query("DELETE FROM `".$prefix."adminmenu` WHERE filename='groups.php';");
mysql_query("UPDATE `".$prefix."adminmenu` SET menu_label='Content Pages' WHERE filename='contentdelivery.php';");
mysql_query("UPDATE `".$prefix."adminmenu` SET menu_label='Payment Settings' WHERE menu_label='Payment/IPN Settings';");
mysql_query("UPDATE `".$prefix."adminmenu` SET menu_label='Leaderboards' WHERE menu_label='Member Stats';");
mysql_query("UPDATE `".$prefix."adminmenu` SET menu_label='Ads' WHERE menu_label='Sites/Ads';");
mysql_query("UPDATE `".$prefix."adminmenu` SET menu_label='Downloads' WHERE menu_label='Products';");
mysql_query("UPDATE `".$prefix."adminmenu` SET menu_label='Media Library' WHERE menu_label='File Library';");

mysql_query("UPDATE `".$prefix."adminmenu` SET menu_order='10' WHERE menu_label='Purchased Pages';");

$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."adminmenu` WHERE id=1536"), 0);
if ($checkexists < 1) {
	mysql_query("INSERT INTO `".$prefix."adminmenu` (id, menu_label, menu_url, menu_target, menu_parent, menu_order, f, filename) VALUES (1536, 'Purchased Downloads', 'admin.php?f=purchaseddls', '_top', '1500', '11', 'purchaseddls', 'purchaseddownloads.php');");
}

mysql_query("UPDATE `".$prefix."adminmenu` SET menu_label='One Time Offers' WHERE menu_label='OTO';");

// End v2.31 Update


// v2.32 Update

@mysql_query("ALTER TABLE `".$prefix."oto_groups` CHANGE `groupsales` `groupsales` DECIMAL(10,2) NOT NULL DEFAULT '0.00';");
@mysql_query("ALTER TABLE `".$prefix."oto_offers` CHANGE `sales` `sales` DECIMAL(10,2) NOT NULL DEFAULT '0.00';");

@mysql_query("ALTER TABLE `".$prefix."bonuspages` ADD `starttime` INT(11) NOT NULL DEFAULT '0' ;");
@mysql_query("ALTER TABLE `".$prefix."bonuspages` ADD `endtime` INT(11) NOT NULL DEFAULT '0' ;");
@mysql_query("ALTER TABLE `".$prefix."bonuspages` ADD INDEX(`starttime`);");
@mysql_query("ALTER TABLE `".$prefix."bonuspages` ADD INDEX(`endtime`);");

// End v2.32 Update


// v2.34 Updates
	
	@mysql_query("ALTER TABLE `".$prefix."members` CHANGE `password` `password` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '';");
	
	@mysql_query("ALTER TABLE `".$prefix."admin` CHANGE `password` `password` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'password';");
	@mysql_query("ALTER TABLE `".$prefix."admin` ADD `ipaddresses` VARCHAR(255) NOT NULL DEFAULT '';");
	
	@mysql_query("ALTER TABLE `".$prefix."settings` ADD `max_fail_name` INT( 11 ) NOT NULL DEFAULT '10';");
	@mysql_query("ALTER TABLE `".$prefix."settings` ADD `max_fail_ip` INT( 11 ) NOT NULL DEFAULT '15';");
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."admins` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `username` varchar(255) NOT NULL,
	  `password` varchar(255) NOT NULL,
	  `ipaddresses` varchar(255) NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `username` (`username`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."admins_log` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `logtime` int(11) NOT NULL DEFAULT '0',
	  `adminidnum` int(11) NOT NULL,
	  `ipaddress` varchar(50) NOT NULL,
	  `invalidlogin` tinyint(1) NOT NULL DEFAULT '0',
	  `message` varchar(255) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `logtime` (`logtime`),
	  KEY `adminidnum` (`adminidnum`),
	  KEY `ipaddress` (`ipaddress`),
	  KEY `invalidlogin` (`invalidlogin`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."admins_noperms` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `adminidnum` int(11) NOT NULL DEFAULT '0',
	  `permission` varchar(100) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `adminidnum` (`adminidnum`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."admins_permsadded` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `permission` varchar(100) NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `permission` (`permission`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."login_log` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `logtime` int(11) NOT NULL DEFAULT '0',
	  `userid` int(11) NOT NULL,
	  `ipaddress` varchar(50) NOT NULL,
	  `invalidlogin` tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`id`),
	  KEY `logtime` (`logtime`),
	  KEY `userid` (`userid`),
	  KEY `ipaddress` (`ipaddress`),
	  KEY `invalidlogin` (`invalidlogin`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
	
	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."adminmenu` WHERE id=334"), 0);
	if ($checkexists < 1) {
		mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES ('334', 'Administrators', 'admin.php?f=admins', '_top', '300', '3', 'admins', 'administrators.php');");
	}
	
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."ipn_salesglance` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `field` varchar(30) NOT NULL,
	  `value` text NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `field` (`field`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
	
	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."ipn_salesglance`"), 0);
	if ($checkexists < 1) {
		mysql_query("INSERT INTO `".$prefix."ipn_salesglance` (`field`, `value`) VALUES ('salespacks', '');");
		mysql_query("INSERT INTO `".$prefix."ipn_salesglance` (`field`, `value`) VALUES ('showpaypal', '1');");
		mysql_query("INSERT INTO `".$prefix."ipn_salesglance` (`field`, `value`) VALUES ('showpayza', '1');");
		mysql_query("INSERT INTO `".$prefix."ipn_salesglance` (`field`, `value`) VALUES ('show2co', '1');");
		mysql_query("INSERT INTO `".$prefix."ipn_salesglance` (`field`, `value`) VALUES ('showclickbank', '1');");
		mysql_query("INSERT INTO `".$prefix."ipn_salesglance` (`field`, `value`) VALUES ('showcomms', '1');");
	}
	
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."widgets` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `name` varchar(100) NOT NULL,
	  `descr` text NOT NULL,
	  `widgetfile` varchar(100) NOT NULL,
	  `settingsfile` varchar(100) NOT NULL,
	  `candelete` tinyint(1) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`id`)
	) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;");
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."widgetpositions` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `widgetid` int(11) NOT NULL,
	  `adminid` int(11) NOT NULL,
	  `row` int(11) NOT NULL,
	  `col` varchar(20) NOT NULL DEFAULT 'left',
	  `enabled` tinyint(1) NOT NULL DEFAULT '1',
	  PRIMARY KEY (`id`),
	  KEY `widgetid` (`widgetid`),
	  KEY `adminid` (`adminid`),
	  KEY `col` (`col`),
	  KEY `enabled` (`enabled`)
	) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;");
	
	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."widgets`"), 0);
	if ($checkexists < 1) {
		mysql_query("INSERT INTO `".$prefix."widgets` (`id`, `name`, `descr`, `widgetfile`, `settingsfile`, `candelete`) VALUES
		(1, 'Member Dashboard', 'Lists the number of members in your site.', 'memberdashboard.php', '', 0),
		(2, 'New From LFMTE', 'Delivers important information about your software, including updates, security alerts, and new plugins.', 'newfromlfm.php', '', 0),
		(3, 'Sales Dashboard', 'Shows the sales and profit stats of your site.', 'salesdashboard.php', '', 0),
		(4, 'Split Test', 'If you have enabled split testing in your System Settings, the stats will be shown here.  This widget will not appear if split testing is not enabled.', 'splittest.php', '', 0),
		(5, 'Affiliate Leaderboard', 'Shows a list of which affiliates are making the most sales at your site.', 'salesleaderboard.php', '', 0),
		(6, 'TE Dashboard', 'Displays information and stats about your traffic exchange.', 'tedash.php', 'tedash_settings.php', 0);");
		
		mysql_query("INSERT INTO `".$prefix."widgetpositions` (`id`, `widgetid`, `adminid`, `row`, `col`, `enabled`) VALUES
		(1, 1, 0, 1, 'left', 1),
		(2, 2, 0, 1, 'center', 1),
		(3, 3, 0, 2, 'left', 1),
		(4, 4, 0, 3, 'left', 1),
		(5, 5, 0, 4, 'left', 1),
		(6, 6, 0, 2, 'center', 1);");
	}
	
	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."adminmenu` WHERE id=336"), 0);
	if ($checkexists < 1) {
		mysql_query("INSERT INTO `".$prefix."adminmenu` (`id`, `menu_label`, `menu_url`, `menu_target`, `menu_parent`, `menu_order`, `f`, `filename`) VALUES ('336', 'Widgets', 'admin.php?f=widgets', '_top', '300', '7', 'widgets', 'widgets.php');");
	}
	
	
	mysql_query("CREATE TABLE IF NOT EXISTS `".$prefix."tedash_hidden` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `adminidnum` int(11) NOT NULL DEFAULT '0',
	  `listitem` varchar(100) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `adminidnum` (`adminidnum`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");
	
// End v2.34 Updates


// Set the current version
@mysql_query("UPDATE `".$prefix."settings` SET `ver` = '2.35'");


// SurfingGuard Tables

@mysql_query("ALTER TABLE `".$prefix."f4checks` ADD `date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `id` ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_ban_exclude` (
  `id` int(11) NOT NULL auto_increment,
  `domain` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_ban_types` (
  `id` int(11) NOT NULL auto_increment,
  `type` int(11) default 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_mban_types` (
  `id` int(11) NOT NULL auto_increment,
  `type` int(11) default 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_mban_list` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(100) NOT NULL,
  `ipaddress` varchar(30) NOT NULL,
  `type` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_checker_actions` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 100 ) NOT NULL ,
`action` INT( 11 ) NOT NULL DEFAULT '0',
`mailadmin` INT( 11 ) NOT NULL DEFAULT '0'
) ENGINE = MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_monitor_actions` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 100 ) NOT NULL ,
`action` INT( 11 ) NOT NULL DEFAULT '0',
`mailmem` INT( 11 ) NOT NULL DEFAULT '0',
`mailadmin` INT( 11 ) NOT NULL DEFAULT '0'
) ENGINE = MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_mban_actions` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 100 ) NOT NULL ,
`action` INT( 11 ) NOT NULL DEFAULT '0',
`mailadmin` INT( 11 ) NOT NULL DEFAULT '0'
) ENGINE = MYISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_mail_templates` (
  `id` int(11) NOT NULL auto_increment,
  `subject` varchar(100) NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM ;");

@mysql_query("CREATE TABLE IF NOT EXISTS `sg_settings` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;");


@mysql_query("INSERT INTO `sg_ban_types` (`id`, `type`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6);");

@mysql_query("INSERT INTO `sg_checker_actions` (`id`, `name`, `action`, `mailadmin`) VALUES
(1, 'Not Found', 1, 0),
(2, 'Too Many Redirects', 1, 0),
(3, 'Hidden IFrame', 1, 0),
(4, 'Encoded JavaScript', 1, 0),
(5, 'Frame Breaking Code', 2, 0),
(6, 'Site Banned By You', 2, 0),
(7, 'Site In Central Ban', 2, 0),
(8, 'Virus', 2, 0),
(9, 'Google - Suspected phishing page', 2, 0),
(10, 'Google - Suspected malicious code', 2, 0);");

@mysql_query("INSERT INTO `sg_monitor_actions` (`id`, `name`, `action`, `mailmem`, `mailadmin`) VALUES
(1, 'Not Found', 1, 1, 0),
(2, 'Too Many Redirects', 1, 1, 0),
(3, 'Hidden IFrame', 2, 1, 0),
(4, 'Encoded JavaScript', 2, 1, 0),
(5, 'Frame Breaking Code', 2, 1, 0),
(6, 'Site Banned By You', 2, 1, 0),
(7, 'Site In Central Ban', 2, 1, 0),
(8, 'Virus', 2, 1, 1),
(9, 'Google - Suspected phishing page', 2, 1, 1),
(10, 'Google - Suspected malicious code', 2, 1, 1);");

@mysql_query("INSERT INTO `sg_mban_actions` (`id`, `name`, `action`, `mailadmin`) VALUES
(1, 'Chargeback/Reversal', 1, 1),
(2, 'Cheater', 1, 1),
(3, 'Unreasonable Dispute', 0, 1),
(4, 'Other', 0, 1);");

@mysql_query("INSERT INTO `sg_mail_templates` (`id`, `subject`, `body`) VALUES
(1, 'Problem Found On Your Site', 'Hello,\n\nWe have detected a problem with one of your sites.\n\nThe error(s) found were:\n\n[errors]\n\nThe site has been temporarily removed from rotation.\nPlease correct the problem and then login to your account\nat ".$domainurl." to verify your site again.\n\nThank you,\n".$domainurl." Automated Approval System'),
(2, 'Your Site Was Suspended', 'Hello,\n\nWe have detected a problem with one of your sites.\n\nThe error(s) found were:\n\n[errors]\n\nPlease contact our support if you have any questions.\n\nThank you,\n".$domainurl." Automated Approval System');");

// Get License Key
include("sglicense.php");
$sglicense = sglicense();
// End Get License Key

@mysql_query("INSERT INTO `sg_settings` (`id`, `name`, `value`) VALUES
(1, 'timer', '5'),
(2, 'exchange', '".$_SERVER["SERVER_NAME"]."'),
(3, 'key', '".$sglicense."'),
(4, 'hitsconnectid', '7296'),
(5, 'adminemail', 'support@surfingguard.com'),
(6, 'sgusername', ''),
(7, 'sgpassword', ''),
(8, 'keeplogs', '30'),
(9, 'serverrequired', '0'),
(10, 'version', '2'),
(11, 'lastcbmon', '0'),
(12, 'adminreply', 'support@surfingguard.com');");

?>