<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
//    Surf Logs Originally Developed By:    //
//       Chris Houg, LFMTE-Host.com         //
//////////////////////////////////////////////

$confirm = $_GET[confirm];

if ($_POST['cronchange'] == 1) cronchange($_POST['cron'],$prefix);

echo "<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"surfstats.css\">";
if($confirm == 0)
{
	$number = get_num($time,$prefix);

	$cronsetting = get_cronsetting($prefix);

	echo "<p>&nbsp</p><span class=\"s8black\"><center>Delete Stats from Database</span><br /><br />";

	echo get_menu($action);

	echo "<table class=\"statsbox\" width=100% bgcolor=#FFFFFF border=0 cellpadding=10 cellspacing=0>";
	echo "<tr bgcolor=#FFFFCC><td>";

	$form = "<form id=\"cron\" name=\"cron\" method=\"post\" action=\"/admin/admin.php?f=sstats&action=sdelete\">
	<span class=\"s9\"><b>You can delete the stats manually or use a daily cron to delete the URL's in the database.
	If left 			undeleted, a large number of rows will decrease performance.</b></span><br /><br />
	Delete Stats older than:&nbsp;&nbsp;
	  <input name=\"cron\" type=\"text\" id=\"cron\" value=\"$cronsetting\" style=\"width:20px;text-align:center\" maxlength=\"2\"/>&nbsp;Days
	  <input name=\"cronchange\" type=\"hidden\" value=\"1\" />
	  <input type=\"submit\" name=\"submit\" id=\"submit\" value=\"Change Cron\" />
	  &nbsp;&nbsp;A sensible value would be 7 Days for new exchanges, 2 Days for busy exchanges.
	</form>";

	echo $form;

	echo "<br><br><span class=\"s8\">&nbsp;&nbsp;<b>$number</b> rows were found in database table 'stats'<br></span>";

	echo "<span class=\"s8\">&nbsp;&nbsp;You should make a habit of deleting rows now and then. A lot of rows will
	slow down server performance.<br><br><br></span>";

	echo "<span class=\"s8\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Delete ALL the ".get_num($time,$prefix)." rows then click
	<a href=\"/admin/admin.php?f=sstats&action=sdelete&confirm=3\"><b>[DELETE ALL]</b></a></center></span><br>";

	echo "<span class=\"s8\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Delete the ".get_num($time-3600,$prefix)." rows OLDER THAN 1
	 hour then click <a href=\"/admin/admin.php?f=sstats&action=sdelete&confirm=1\"><b>[DELETE ALL OLDER THAN 1 HOUR]
	 </b></a></center>	</span><br>";

	echo "<span class=\"s8\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Delete the ".get_num($time-86400,$prefix)." rows OLDER THAN
	24 hours then click <a href=\"/admin/admin.php?f=sstats&action=sdelete&confirm=24\">
	<b>[DELETE ALL OLDER THAN 24 HOURS]</b></a></center></span><br>";

	echo "<span class=\"s8\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Delete the ".get_num($time-172800,$prefix)." rows OLDER THAN 48
	 hours then click <a href=\"/admin/admin.php?f=sstats&action=sdelete&confirm=48\"><b>[DELETE ALL OLDER THAN 48
	  HOURS]</b></a>	</center></span><br>";

	echo "</td></tr></table>";
	exit;
}

if($confirm == 3)
{
	$number = get_num($time,$prefix);

	del_stats(time(),$prefix);

	echo "<br><span class=\"s9\"><center>$number rows were removed from database table 'sstats' and table was optimized.<br><br><a href=\"/admin/admin.php?f=sstats&action=sdelete\">Return</a></center></span>";
	exit;
}
if($confirm == 24)
{
	$opt24 = $time-86400;
	$number = get_num($opt24,$prefix);

	del_stats($opt24,$prefix);

	echo "<br><span class=\"s9\"><center>$number rows older than 24 hours were removed from database table 'sstats' and table was optimized.<br><br><a href=\"/admin/admin.php?f=sstats&action=sdelete\">Return</a></center></span>";
	exit;
}
if($confirm == 1)
{
	$opt1 = $time-3600;
	$number = get_num($opt1,$prefix);

	del_stats($opt1,$prefix);

	echo "<br><span class=\"s9\"><center>$number rows older than 1 hour were removed from database table 'sstats' and table was optimized<br><br><a href=\"/admin/admin.php?f=sstats&action=sdelete\">Return</a>.</center></span>";
	exit;
}
if($confirm == 48)
{
	$opt48 = $time-172800;
	$number = get_num($opt48,$prefix);

	del_stats($opt48,$prefix);

	echo "<br><br><span class=\"s9\"><center>$number rows older than 48 hours were removed from database table 'sstats' and table was optimized.<br><br><a href=\"/admin/admin.php?f=sstats&action=sdelete\">Return</a></center></span>";
	exit;
}

?>