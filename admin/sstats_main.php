<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
//    Surf Logs Originally Developed By:    //
//       Chris Houg, LFMTE-Host.com         //
//////////////////////////////////////////////

require_once "../inc/filter.php";
require_once "../inc/sstat_func.php";

// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

$userid = $_REQUEST['userid'];
$sterm = "1";

// check to see how many URLs to display
$action = $_REQUEST['action'];

// if no sort order default it else grab the sort order
if ($_REQUEST['sort'] == "") {
	$sort= 1;
	$nextsort = $sort;
}else $nextsort = $_REQUEST['sort'];

$orderby = $_GET['orderby'] == "userid" ? "userid" : "adate";

	switch ($action) {
		case "1all":
			$sdate = 3600;  // seconds in one hour
			$sterm = "adate >= ";  // used with where clause in query
			break;
		case "24all":
			$sdate = 86400;  // seconds in 24 hours
			$sterm = "adate >= ";  // used with where clause in query
			break;
		case "48all":
			$sdate = 172800; // seconds in 48 hours
			$sterm = "adate >= ";  // used with where clause in query
			break;
		case "sdelete":
			include "sstats_delete.php";
		exit;
}

if (is_numeric($userid)) {
	$usersterm = "userid = $userid AND";  // User ID used in query
} else {
	$usersterm = "id > 0 AND";  // User ID used in query
}

// Limit the number URL's to display per page
$limit=20;

// Pagination
$Prev = "Previous";
$Next = "Next";

// Column sort logic in table
$ob=$_GET['ob'];
$field=$_GET['field'];
$offset = $_GET['offset'];
$sortby = $_REQUEST['sort'] == 1 ? "DESC" : "ASC";
$sort = $_REQUEST['sort'] == true? 0: 1;
$suid = $orderby != "userid" ? 0: $sort;
$duid = $orderby != "adate" ? 1: $sort;

if ($sterm != "1") {
	$etime = time();
	$eetime = $etime - $sdate;
} else {
	$sterm = "adate >= ";
	$sdate = "";
	$eetime = "0";
}


// check for rows in the db
$numresults=mysql_query("SELECT * FROM ".$prefix."sstats WHERE $usersterm $sterm $eetime");
$number=mysql_num_rows($numresults);


// Handle Lookups From Abuse Reports
if (isset($_GET['reportedsiteid']) && is_numeric($_GET['reportedsiteid']) && is_numeric($_GET['reportedtime'])) {

	echo("<br><center><a href=\"javascript:javascript:history.go(-1)\"><input type=\"button\" name=\"backbutton\" value=\"<< Back To Abuse Reports\"></a></center><br>");

	// Find the report's entry in the log
	$getentryid = mysql_query("SELECT id FROM ".$prefix."sstats WHERE websiteid=".$_GET['reportedsiteid']." AND adate <= ".$_GET['reportedtime']." ORDER BY adate DESC LIMIT 1");
	if (mysql_num_rows($getentryid) == 1) {
		
		$highlightid = mysql_result($getentryid, 0, "id");
		
		// Calculate offset to go directly to the page with the entry
		$getpriorentries = mysql_query("SELECT COUNT(*) FROM ".$prefix."sstats WHERE adate > ".$_GET['reportedtime']." AND $usersterm $sterm $eetime");
		$priorentries = mysql_result($getpriorentries, 0);
		$calpagenum = floor($priorentries/$limit);
		$offset = $calpagenum*$limit;
		
	} else {
		// The surf log must have been deleted
		$cronsetting = get_cronsetting($prefix);
		echo("<center><p><b>We could not find the reported entry in the surf logs.  Surf logs are deleted automatically after ".$cronsetting." days</b></p></center>");
		exit;
	}

} elseif (isset($_GET['highlightid']) && is_numeric($_GET['highlightid'])) {
	$highlightid = $_GET['highlightid'];
} else {
	$highlightid = 0;
}


if (empty($offset)) {
$offset=0;
}

echo "<LINK REL=\"stylesheet\" TYPE=\"text/css\" HREF=\"surfstats.css\">";

// Get table title
echo get_title($action);

echo "<form action=\"/admin/admin.php?f=sstats&sort=1\" method=\"post\">
<span class=\"s10\">&nbsp;Enter UserID to Search for</span><br />
<input name=\"userid\" type=\"text\" size=\"10\" />
<input name=\"submit\" type=\"submit\" value=\"Search\" />
</form>";

// Get the menu
echo get_menu($action);

echo "<table class=\"statsbox\" width=100% bgcolor=#FFFFFF border=0 cellpadding=5 cellspacing=0>";

echo "<tr bgcolor=#FFFFCC><td><span class=\"s8\"><b>id</span></td><td><span class=\"s8\"><b>[<a href=\"/admin/admin.php?f=sstats&action=$action&sort=$suid&orderby=userid&userid=$userid&highlightid=$highlightid\">USERID</a>]</span></td><td><span class=\"s8\"><b>URL</span></td><td><span class=\"s8\"><b>Time</b></span></td><td><span class=\"s8\"><b>[<a href=\"/admin/admin.php?f=sstats&action=$action&sort=$duid&userid=$userid&highlightid=$highlightid\">Date</a>]</b></span></td><td><span class=\"s8\"><b>Website ID</b></span></td></tr>";

// Get the URLs for displaying the table
echo build_table($prefix,$usersterm,$sterm,$eetime,$orderby,$sortby,$offset,$limit,$highlightid);

echo "</table>";

// number of pages logic
$pages=intval($number/$limit);

if ($number%$limit) {
$pages++;
}

for ($i=1;$i<=$pages;$i++) {
$newoffset=$limit*($i-1);
print "<a href=\"/admin/admin.php?f=sstats&userid=$userid&highlightid=$highlightid&offset=$newoffset&field=$field&ob=$ob&action=$action&sort=$nextsort\">$i</a> \n";
}

if ($offset>1) {
$prevoffset=$offset-$limit;
print "<a href=\"/admin/admin.php?f=sstats&userid=$userid&highlightid=$highlightid&offset=$prevoffset&field=$field&ob=$ob&action=$action&sort=$nextsort\">$Prev</a> \n";
}

if ($number>($offset+$limit)) {
$nextoffset=$offset+$limit;
print "<a href=\"/admin/admin.php?f=sstats&userid=$userid&highlightid=$highlightid&offset=$nextoffset&field=$field&ob=$ob&action=$action&sort=$nextsort\">$Next</a><p>\n";
}

$coffset = $nextoffset-20;

echo "<br><span class=\"s8\">Showing record number: $coffset - $nextoffset</span>";
echo "<br><span class=\"s8\">$number records  found in database</span>";
exit;
?>