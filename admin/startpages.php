<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////
// AUTOMATED START PAGE SYSTEM v1.21		         //
// (c) 2007-2008 Simon B Kelly. All rights reserved.     //
// http://replytosimon.com                               //
//                                                       //
// Not for resale. Sold and installed exclusively by:    //
// Simon B Kelly and Josh Abbott, http://trafficmods.com //
///////////////////////////////////////////////////////////

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

extract($_GET);
extract($_POST);

echo("<center><br><br>");

$t=time();

$action=$_GET[action];
$id=$_GET[id];
$submit=$_POST[submit];

if($submit=="Update") {
	extract($_POST);
	lfmsql_query("UPDATE `".$prefix."startpage_settings` SET `enable`=$enable, `type`='$type', `default`='$default', `showbar`=$showbar, `alwaysframe`=$alwaysframe, `bartext`='$bartext';") or die(lfmsql_error());
	
	if($merchant1=="on") { $merchant1=1; } else { $merchant1=0; }
	if($merchant2=="on") { $merchant2=1; } else { $merchant2=0; }
	if($merchant3=="on") { $merchant3=1; } else { $merchant3=0; }
	if($merchant4=="on") { $merchant4=1; } else { $merchant4=0; }
	if($merchant5=="on") { $merchant5=1; } else { $merchant5=0; }  
	if($merchant6=="on") { $merchant6=1; } else { $merchant6=0; }
	
	lfmsql_query("Update ".$prefix."ipn_products set amount=$amount, description='$description', merchant1='$merchant1', merchant2='$merchant2', merchant3='$merchant3', merchant4='$merchant4', merchant5='$merchant5', merchant6='$merchant6' where id=1 limit 1");
	
	echo '<em>Settings updated!</em><p>';
	}

if($action=="delete") {
	lfmsql_query("DELETE FROM `".$prefix."startpage` WHERE id='$id';") or die(lfmsql_error());
	lfmsql_query("DELETE FROM `".$prefix."startpage_stats` WHERE sid='$id';") or die(lfmsql_error());
	echo '<em>Booking deleted!</em><p>';
	}

if($submit=="Add"||$submit=="Edit") {
	$from=mktime(0,0,0,substr($_POST[sdate],5,2),substr($_POST[sdate],8),substr($_POST[sdate],0,4));
	$to=mktime(0,0,0,substr($_POST[edate],5,2),substr($_POST[edate],8),substr($_POST[edate],0,4))+86399;
	if($_POST[id]) {
		lfmsql_query("UPDATE `".$prefix."startpage` SET `from`='$from', `to`='$to', usrid='$usrid', url='$url', cost='$cost' WHERE id='$_POST[id]';") or die(lfmsql_error());
		echo '<em>The booking details have been updated in the database.</em><p>';
		} else {
		lfmsql_query("INSERT INTO `".$prefix."startpage` (`from`, `to`, `usrid`, `url`, `purchased`, `cost`) VALUES ('$from', '$to', '$usrid', '$url', NOW(), '$cost');") or die(lfmsql_error());
		echo '<em>The booking details have been added to the database.</em><p>';
		}
	}

if($action=="add"||$action=="edit") {
	echo '<script type="text/javascript" src="datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol.css">
<p>
<table align="center" cellpadding=2 cellspacing=0>
<tr><td colspan=2 align="right"><a href="admin.php?f=startp">List all bookings</a></td></tr>
<tr><th colspan=2>';
	if($id) {
		echo "Edit booking ID $id";
		$r=lfmsql_query("SELECT * FROM ".$prefix."startpage WHERE id='$id';") or die(lfmsql_error());
		$row=lfmsql_fetch_array($r);
		extract($row);
		} else {
		echo "Add a new booking";
		$from=''; $to='';
		}
	if($from) { $from=date("Y-m-d",$from); }
	if($to) { $to=date("Y-m-d",$to); }
	echo '</th></tr>
<FORM action="admin.php?f=startp" method="POST">
<INPUT type="hidden" name="id" value="'.$id.'">
<tr><td align="right">From:</td><td><input name="sdate" type id="DPC_sdate_YYYY-MM-DD" value="'.$from.'" width="80" text></td></tr>
<tr><td align="right">To:</td><td><input name="edate" type id="DPC_edate_YYYY-MM-DD" value="'.$to.'" width="80" text></td></tr>
<tr><td align="right">User ID:</td><td><INPUT type="text" name="usrid" value="'.$usrid.'" maxlength=6 size=5></td></tr>
<tr><td align="right">URL:</td><td><INPUT type="text" name="url" value="'.$url.'" maxlength=100 size=40></td></tr>
<tr><td align="right">Cost:</td><td><INPUT type="text" name="cost" value="'.$cost.'" maxlength=6 size=5> USD</td></tr>
<tr><td colspan=2 align="center"><INPUT type="submit" name="submit" value="'.ucwords($action).'"></td></tr>
</FORM>
</table>
</center>';
	echo '
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>
 </body>
</html>';
	lfmsql_close;
	exit;
	}

$result = lfmsql_query("SELECT SUM(cost) FROM `".$prefix."startpage`;");
$gross = lfmsql_result($result,0);
if(!$gross) { $gross='0.00'; }

$result = lfmsql_query("SELECT * FROM `".$prefix."startpage_settings`;");
$row = lfmsql_fetch_array($result);
extract($row);
if(!$default) { $default=$config[75]; }

echo 'Here you can view stats and change the settings of your Startpage mod.
<p>
<b>Stats</b>
<br>So far your startpages have grossed $'.$gross;

echo '<p>
<b>BOOKINGS PENDING:</b><br>
<table border=1 cellpadding=2 cellspacing=0>
<tr bgcolor="#F8F8F8"><td colspan=8 height=22 align="right"><a href="admin.php?f=startp&action=add">Add a booking</a>&nbsp;</td></tr>
<tr bgcolor="#F8F8F8"><th>ID</th><th>From</th><th>To</th><th>User ID</th><th>URL</th><th>Purchased</th><th colspan=2>Action</th></tr>';

$result = lfmsql_query("SELECT * FROM `".$prefix."startpage` WHERE `from`>$t ORDER BY `from`;");
if(!lfmsql_num_rows($result)) { echo "<tr><th colspan=8 height=22>There are no bookings pending!</th></tr>"; }
while($row=lfmsql_fetch_array($result)) {
	$from=date("Y-m-d",$row[from]);
	$to=date("Y-m-d",$row[to]);
	$url=substr($row[url],strpos($row[url],":")+3);
	if(strlen($url)>32) { $url=substr($url,0,30)."..."; }
	echo "<tr align=\"center\"><td>$row[id]</td><td>$from</td><td>$to</td><td><a href=\"admin.php?f=mm&sf=browse&searchfield=id&searchtext=$row[usrid]\">$row[usrid]</a></td><td>&nbsp;<a href=\"$row[url]\" target=\"new\">$url</a>&nbsp;</td><td>$row[purchased]</td><td>&nbsp;<a href=\"admin.php?f=startp&action=edit&id=$row[id]\">Edit</a>&nbsp;</td><td title=\"Click here to delete this program\">&nbsp;<a href=\"admin.php?f=startp&action=delete&id=$row[id]\" Onclick=\"return confirm('Are you sure you want to delete?')\">Delete</a>&nbsp;</td></tr>";
	}

echo '</table>
<p>
<b>CURRENT STARTPAGE:</b><br>
<table border=1 cellpadding=2 cellspacing=0>
<tr bgcolor="#F8F8F8"><th>ID</th><th>From</th><th>To</th><th>User ID</th><th>URL</th><th>Views<br>Today</th><th>Unique<br>Today</th><th>Total<br>Views</th><th>Total<br>Unique</th><th>Last<br>Viewed</th><th>Last<br>User</th><th colspan=2>Action</th></tr>';

$result = lfmsql_query("SELECT * FROM `".$prefix."startpage` WHERE `from`<$t AND `to`>$t;");
if(!lfmsql_num_rows($result)) { echo "<tr><th colspan=13 height=22>There is no current booking!</th></tr>"; }
while($row=lfmsql_fetch_array($result)) {
	$from=date("Y-m-d",$row[from]);
	$to=date("Y-m-d",$row[to]);
	$url=substr($row[url],strpos($row[url],":")+3);
	if(strlen($url)>32) { $url=substr($url,0,30)."..."; }
	$lastviewed="n/a";
	if($row[lastview]) { $lastviewed=$t-$row[lastview]." secs ago"; }
	echo "<tr align=\"center\"><td>$row[id]</td><td>$from</td><td>$to</td><td><a href=\"admin.php?f=mm&sf=browse&searchfield=id&searchtext=$row[usrid]\">$row[usrid]</a></td><td><a href=\"$row[url]\" target=\"new\">$url</a></td><td>$row[viewstoday]</td><td>$row[uniquetoday]</td><td>$row[views]</td><td>$row[totalunique]</td><td>$lastviewed</td> <td><a href=\"admin.php?f=mm&sf=browse&searchfield=id&searchtext=$row[lastusrid]\">$row[lastusrid]</a></td><td>&nbsp;<a href=\"admin.php?f=startp&action=edit&id=$row[id]\">Edit</a>&nbsp;</td><td title=\"Click here to delete this program\">&nbsp;<a href=\"admin.php?f=startp&action=delete&id=$row[id]\" Onclick=\"return confirm('Are you sure you want to delete?')\">Delete</a>&nbsp;</td></tr>";
	}

echo '</table>
<p>
<b>RECENTLY COMPLETED BOOKINGS:</b><br>
<table border=1 cellpadding=2 cellspacing=0>
<tr bgcolor="#F8F8F8"><th>ID</th><th>From</th><th>To</th><th>User ID</th><th>URL</th><th>Total<br>Views</th><th>Total<br>Unique</th><th colspan=2>Action</th></tr>';

$result = lfmsql_query("SELECT * FROM ".$prefix."startpage WHERE `to`<$t ORDER BY `to` DESC LIMIT 10;");
if(!lfmsql_num_rows($result)) { echo "<tr><th colspan=9 height=22>There are no completed bookings yet!</th></tr>"; }
while($row=lfmsql_fetch_array($result)) {
	$from=date("Y-m-d",$row[from]);
	$to=date("Y-m-d",$row[to]);
	$url=substr($row[url],strpos($row[url],":")+3);
	if(strlen($url)>32) { $url=substr($url,0,30)."..."; }
	echo "<tr align=\"center\"><td>$row[id]</td><td>$from</td><td>$to</td><td><a href=\"admin.php?f=mm&sf=browse&searchfield=id&searchtext=$row[usrid]\">$row[usrid]</a></td><td><a href=\"$row[url]\" target=\"new\">$url</a></td><td>$row[views]</td><td>$row[totalunique]</td><td>&nbsp;<a href=\"admin.php?f=startp&action=edit&id=$row[id]\">Edit</a>&nbsp;</td><td title=\"Click here to delete this program\">&nbsp;<a href=\"admin.php?f=startp&action=delete&id=$row[id]\" Onclick=\"return confirm('Are you sure you want to delete?')\">Delete</a>&nbsp;</td></tr>";
	}

// Get Merchants
$res=lfmsql_query("SELECT * FROM ".$prefix."ipn_merchants ORDER BY id LIMIT 6;");
$tot=lfmsql_num_rows($res);
$merchant=array();
for($f=1;$f<=$tot;$f++) {
	$row=lfmsql_fetch_array($res);
	$merchant[$f]=$row[name];
}
// End Get Merchants

$getitem = lfmsql_query("Select * from ".$prefix."ipn_products where id=1 limit 1");
$amount = lfmsql_result($getitem, 0, "amount");
$description = lfmsql_result($getitem, 0, "description");

$merchant1 = lfmsql_result($getitem, 0, "merchant1");
$merchant2 = lfmsql_result($getitem, 0, "merchant2");
$merchant3 = lfmsql_result($getitem, 0, "merchant3");
$merchant4 = lfmsql_result($getitem, 0, "merchant4");
$merchant5 = lfmsql_result($getitem, 0, "merchant5");
$merchant6 = lfmsql_result($getitem, 0, "merchant6");

echo '</table>
<p>
<b>Settings</b>
<table>
<FORM action="admin.php?f=startp" method="POST">
<tr><th align="right">Startpage Enable:</th>
<td align="left"><SELECT name="enable">
<OPTION value="0">No</OPTION>
<OPTION value="1"';
if($enable) { echo ' SELECTED'; }
echo '>Yes</OPTION>
</SELECT></td></tr>
<tr><th align="right">Startpage Type:</th>
<td align="left">Site Of The <SELECT name="type">
<OPTION value="D"';
if($type=="D") { echo ' SELECTED'; }
echo '>Day</OPTION>
<OPTION value="W"';
if($type=="W") { echo ' SELECTED'; }
echo '>Week</OPTION>
<OPTION value="M"';
if($type=="M") { echo ' SELECTED'; }
echo '>Month</OPTION>
</SELECT></td></tr>
<tr><th align="right">Price:</th>
<td align="left"><INPUT type="text" name="amount" value="'.$amount.'" size=2></td></tr>

<tr><th align="right">Payment Options:</th>
<td align="left">

<table border="0" cellpadding="2" cellspacing="0"><tr>';

for($f=1;$f<=$tot;$f++) {
	$name="merchant$f";
	echo '
<td><input type=checkbox name='.$name;
	if($$name) { echo ' checked'; }
		echo '><span style="cursor:pointer; color:blue; size:8px;" onClick="window.open(\'/admin/ipn_editimg.php?merch='.$f.'&id=1\',\'edit_img\',\'scrollbars=yes, menubars=no, width=550, height=550\');">'.$merchant[$f].'</span></td>';
}

echo'</tr></table>

</td></tr>

<tr><th align="right">Instructions:</th>
<td align="left"><textarea rows=5 cols=40 name=description>'.$description.'</textarea></td></tr>

<tr><th align="right">Default URL:</th>
<td align="left"><INPUT type="text" name="default" value="'.$default.'" size=50></td></tr>

<tr><th align="right">Always Frame Startpage:</th>
<td align="left"><SELECT name="alwaysframe">
<OPTION value="0">No</OPTION>
<OPTION value="1"';
if($alwaysframe) { echo ' SELECTED'; }
echo '>Yes</OPTION>
</SELECT></td></tr>

<tr><th align="right">Show Startpage Bar:</th>
<td align="left"><SELECT name="showbar">
<OPTION value="0">No</OPTION>
<OPTION value="1"';
if($showbar) { echo ' SELECTED'; }
echo '>Yes</OPTION>
</SELECT></td></tr>

<tr><th align="right">Startpage Bar Text:</th>
<td align="left"><textarea rows=5 cols=40 name=bartext>'.$bartext.'</textarea></td></tr>

<tr><td></td><td align="left"><INPUT type="submit" name="submit" value="Update"></td></tr>

</FORM>
</table>
</center>';

?>