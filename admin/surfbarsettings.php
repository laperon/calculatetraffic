<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

function checkModExtension($file) {
	$fileExp = explode('.', $file);
	return $fileExp[count($fileExp) -1];
}

// Add/Edit a module or extension
if (isset($_GET['addedit']) && isset($_GET['addedittype'])) {
	
	if ($_GET['addedittype'] == "mod") {
		$actionobj = "Module";
		$tablename = "`".$prefix."barmods_topbar`";
		$directoryname = "surf_modules";
	} else {
		$actionobj = "Extension";
		$tablename = "`".$prefix."barmods_extensions`";
		$directoryname = "surf_extensions";
	}
	
	if ($_POST['Submit'] == "Add" || $_POST['Submit'] == "Edit") {
		
		if ($_POST['filename'] == "None" || file_exists("../".$directoryname."/".$_POST['filename'])) {
			
			if (is_numeric($_GET['addedit'])) {
				// Edit existing entry
				lfmsql_query("UPDATE ".$tablename." SET modname='".$_POST['modname']."', moddescr='".$_POST['moddescr']."', filename='".$_POST['filename']."', html='".$_POST['html']."' WHERE id='".$_GET['addedit']."' AND modtype >= 4") or die(lfmsql_error());
			} else {
				// Add new entry
				$getlastrank = lfmsql_query("SELECT rank FROM ".$tablename." ORDER BY rank DESC LIMIT 1");
				if (lfmsql_num_rows($getlastrank) > 0) {
					$lastrank = lfmsql_result($getlastrank, 0, "rank");
				} else {
					$lastrank = 0;
				}
				$newrank = $lastrank + 1;
				lfmsql_query("INSERT INTO ".$tablename." (state, rank, modtype, modname, moddescr, filename, html) VALUES ('0', '".$newrank."', '4', '".$_POST['modname']."', '".$_POST['moddescr']."', '".$_POST['filename']."', '".$_POST['html']."')") or die(lfmsql_error());
			}			
		}
		
	} else {
		
		if (is_numeric($_GET['addedit'])) {
			$actiontype = "Edit";
			$getentry = lfmsql_query("SELECT modname, moddescr, filename, html FROM ".$tablename." WHERE id='".$_GET['addedit']."' AND modtype >= 4") or die(lfmsql_error());
			if (lfmsql_num_rows($getentry) > 0) {
				$modnameval = lfmsql_result($getentry, 0, "modname");
				$moddescrval = lfmsql_result($getentry, 0, "moddescr");
				$filenameval = lfmsql_result($getentry, 0, "filename");
				$htmlval = lfmsql_result($getentry, 0, "html");
			} else {
				echo("Module or Extension ID not found.");
				exit;
			}
		} else {
			$actiontype = "Add";
			$modnameval = "";
			$moddescrval = "";
			$filenameval = "None";
			$htmlval = "";
		}
		
		?>
		<link href="styles.css" rel="stylesheet" type="text/css" />
		<table width="100%" border="0" cellspacing="0" cellpadding="4">
		  <tr>
		    <td><p>&nbsp;</p></td>
		  </tr>
		  <tr>
		    <td>

		<center>
		<p><font size="5"><b><? echo($actiontype." ".$actionobj); ?></b></font></p>
		
		<form action="admin.php?f=sbsettings&addedit=<? echo($_GET['addedit']); ?>&addedittype=<? echo($_GET['addedittype']); ?>" method="post" name="sbsettings" id="sbsettings">
		<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
		
		      <tr>
        		<td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><? echo($actionobj); ?> Name</strong></font></td>
		        <td colspan="2" class="formfield" align="left">
		        <input type="text" name="modname" size="20" value="<? echo($modnameval); ?>">
		        </td>
		        <td align="center"><a href="#" title="A quick title for your own reference.">
				<img src="../images/question.jpg" width="15" height="15" border="0" />
				</a></td>
		      </tr>
		      
		      <tr>
        		<td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Description</strong></font></td>
		        <td colspan="2" class="formfield" align="left">
		        <textarea name="moddescr" cols="25" rows="4"><? echo($moddescrval); ?></textarea>
		        </td>
		        <td align="center"><a href="#" title="A description for your own reference.">
				<img src="../images/question.jpg" width="15" height="15" border="0" />
				</a></td>
		      </tr>
		      
    		  <tr>
		        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">PHP File</font> </strong></td>
		        <td colspan="2" class="formfield" align="left">
		        <select name="filename">
		        <option value="None"<? if($filenameval == "None"){echo(" selected");} ?>>None</option>
		        	<?php
				$handle = opendir("../".$directoryname);
				while(false != $handle and $fName = readdir($handle)) {
				  if (is_numeric($_GET['addedit'])) {
				   $checkadded = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$tablename." WHERE filename='".$fName."' AND id != '".$_GET['addedit']."'"), 0);
				  } else {
				    $checkadded = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$tablename." WHERE filename='".$fName."'"), 0);
				  }
				   if($checkadded == 0) {
				      if (checkModExtension($fName) == "php") {
				      	echo("<option value=\"".$fName."\""); if($filenameval == $fName) { echo(" selected"); } echo(">".$fName."</option>");
				      }
				   }
				}
		        	?>
		        </select>
		        </td>
		        <td align="center"><a href="#" title="To use a PHP file for this customization, upload the file to your <? echo($directoryname); ?> directory and select it from the list."><img src="../images/question.jpg" width="15" height="15" border="0" />
			</a></td>
      		</tr>
		      
		      <tr>
        		<td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>HTML</strong></font></td>
		        <td colspan="2" class="formfield" align="left">
		        <textarea name="html" cols="25" rows="8"><? echo($htmlval); ?></textarea>
		        </td>
		        <td align="center"><a href="#" title="The HTML code for your customization, if not using a PHP file.">
				<img src="../images/question.jpg" width="15" height="15" border="0" />
				</a></td>
		      </tr>
		      
		      <tr>
		        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="<? echo($actiontype); ?>" /></td>
		        </tr>
		    </table>
		</form>
		
		<br><br>
		<table border="0" cellpadding="0" cellspacing="0" width="450">
		<tr><td align="left">
			<p><font size="2"><b>Note:</b> This feature is intended for advanced users with programming experience who want to customize their exchanges.  To install a plugin purchased from LFMTE, just run the installation wizard and any necessary modules or extensions will be added automatically.  LFMTE does not provide technical support for customizations added by owners or 3rd party developers.</font></p>
		</td></tr>
		</table>
		
		<?
	exit;
	
	}
	
}


// Update system settings
if($_POST["Submit"] == "Update Surfbar Settings")
{

		if ((!isset($_POST['surfbarstyle'])) || (!is_numeric($_POST['surfbarstyle'])) || ($_POST['surfbarstyle'] < 1) || ($_POST['surfbarstyle'] > 2)) {
			$_POST['surfbarstyle'] = 2;
		}
		
		if ((!isset($_POST['preloadsites'])) || (!is_numeric($_POST['preloadsites'])) || ($_POST['preloadsites'] < -1) || ($_POST['preloadsites'] > 1)) {
			$_POST['preloadsites'] = -1;
		}
		
		if ((!isset($_POST['timertype'])) || (!is_numeric($_POST['timertype'])) || ($_POST['timertype'] < 0) || ($_POST['timertype'] > 1)) {
			$_POST['timertype'] = 1;
		}
		
		if (!isset($_POST['timercolor'])) {
			$_POST['timercolor'] = "0000FF";
		} else {
			$_POST['timercolor'] = str_ireplace("#", "", $_POST['timercolor']);
			if (strlen($_POST['timercolor']) != 6) {
				$_POST['timercolor'] = "0000FF";
			}
		}
		
		// Update settings
		@lfmsql_query("TRUNCATE TABLE ".$prefix."surf_admin_prefs");
		
		@lfmsql_query("Insert into ".$prefix."surf_admin_prefs (field, value) VALUES
		('surfbarstyle', '".$_POST['surfbarstyle']."'),
		('preloadsites', '".$_POST['preloadsites']."'),
		('timertype', '".$_POST['timertype']."'),
		('timercolor', '".$_POST['timercolor']."');");
		
		$msg = "Settings Updated";

}

// Change a module rank
$rank = $_GET['modrank'];
if ($rank == "left" || $rank == "right") {
	
	$runchange = "yes";
	$rankid = $_GET['rankid'];
	
	$checkfirstrow = lfmsql_query("Select rank from `".$prefix."barmods_topbar` order by rank asc");
	$firstrow = lfmsql_result($checkfirstrow, 0, "rank");
	
	$checklastrow = lfmsql_query("Select rank from `".$prefix."barmods_topbar` order by rank desc");
	$lastrow = lfmsql_result($checklastrow, 0, "rank");
	
	if (($rank=="left") && ($rankid > $firstrow)) {
		$change=$rankid-1;
	} elseif (($rank=="right") && ($rankid < $lastrow)) {
		$change=$rankid+1;
	} else {
		$runchange = "no";
	}
	
	if ($runchange == "yes") {
		@lfmsql_query("Update `".$prefix."barmods_topbar` set rank=0 where rank=$rankid");
		@lfmsql_query("Update `".$prefix."barmods_topbar` set rank=$rankid where rank=$change");
		@lfmsql_query("Update `".$prefix."barmods_topbar` set rank=$change where rank=0");
		@lfmsql_query("ALTER TABLE `".$prefix."barmods_topbar` ORDER BY rank;");
	}
	
}

// Change an extension rank
$rank = $_GET['extrank'];
if ($rank == "up" || $rank == "down") {
	
	$runchange = "yes";
	$rankid = $_GET['rankid'];
	
	$checkfirstrow = lfmsql_query("Select rank from `".$prefix."barmods_extensions` order by rank asc");
	$firstrow = lfmsql_result($checkfirstrow, 0, "rank");
	
	$checklastrow = lfmsql_query("Select rank from `".$prefix."barmods_extensions` order by rank desc");
	$lastrow = lfmsql_result($checklastrow, 0, "rank");
	
	if (($rank=="up") && ($rankid > $firstrow)) {
		$change=$rankid-1;
	} elseif (($rank=="down") && ($rankid < $lastrow)) {
		$change=$rankid+1;
	} else {
		$runchange = "no";
	}
	
	if ($runchange == "yes") {
		@lfmsql_query("Update `".$prefix."barmods_extensions` set rank=0 where rank=$rankid");
		@lfmsql_query("Update `".$prefix."barmods_extensions` set rank=$rankid where rank=$change");
		@lfmsql_query("Update `".$prefix."barmods_extensions` set rank=$change where rank=0");
		@lfmsql_query("ALTER TABLE `".$prefix."barmods_extensions` ORDER BY rank;");
	}
}

// Change a module state
if (isset($_GET['modstate']) && is_numeric($_GET['modstate']) && isset($_GET['modid']) && is_numeric($_GET['modid'])) {
	if ($_GET['modstate'] == 1) {
		lfmsql_query("UPDATE `".$prefix."barmods_topbar` SET state='1' WHERE id='".$_GET['modid']."' AND modtype > 1") or die(lfmsql_error());
	} elseif ($_GET['modstate'] == 0) {
		lfmsql_query("UPDATE `".$prefix."barmods_topbar` SET state='0' WHERE id='".$_GET['modid']."' AND modtype > 1") or die(lfmsql_error());
	}
}

// Change an extension state
if (isset($_GET['extstate']) && is_numeric($_GET['extstate']) && isset($_GET['extid']) && is_numeric($_GET['extid'])) {
	if ($_GET['extstate'] == 1) {
		lfmsql_query("UPDATE `".$prefix."barmods_extensions` SET state='1' WHERE id='".$_GET['extid']."' AND modtype > 1") or die(lfmsql_error());
	} elseif ($_GET['extstate'] == 0) {
		lfmsql_query("UPDATE `".$prefix."barmods_extensions` SET state='0' WHERE id='".$_GET['extid']."' AND modtype > 1") or die(lfmsql_error());
	}
}

// Delete a module
if (isset($_GET['deletemod']) && is_numeric($_GET['deletemod'])) {
	$getmodule = lfmsql_query("SELECT rank, modtype, modname FROM `oto_barmods_topbar` WHERE id='".$_GET['deletemod']."'") or die(lfmsql_error());
	if (lfmsql_num_rows($getmodule) > 0) {
		$modrank = lfmsql_result($getmodule, 0, "rank");
		$modtype = lfmsql_result($getmodule, 0, "modtype");
		$modname = lfmsql_result($getmodule, 0, "modname");
		if ($modtype > 2) {
			if(isset($_GET['confirmdel']) && $_GET['confirmdel'] == "yes") {
				lfmsql_query("DELETE FROM `oto_barmods_topbar` WHERE id='".$_GET['deletemod']."'") or die(lfmsql_error());
				lfmsql_query("UPDATE `oto_barmods_topbar` SET rank=rank-1 WHERE rank > '".$modrank."'");
			} else {
				echo("<center>
				<p><font size=3>Are you sure you want to delete this module from your surfbar?</font></p>
				<p><font size=4><b>".$modname."</b></p>
				<p><font size=3><a href=\"admin.php?f=sbsettings&deletemod=".$_GET['deletemod']."&confirmdel=yes\">Yes</a></font></p>
				<p><font size=3><a href=\"admin.php?f=sbsettings\">No</a></font></p>
				</center>");
				exit;
			}
		}
	}
}

// Delete an extension
if (isset($_GET['deleteext']) && is_numeric($_GET['deleteext'])) {
	$getmodule = lfmsql_query("SELECT rank, modtype, modname FROM `oto_barmods_extensions` WHERE id='".$_GET['deleteext']."'") or die(lfmsql_error());
	if (lfmsql_num_rows($getmodule) > 0) {
		$modrank = lfmsql_result($getmodule, 0, "rank");
		$modtype = lfmsql_result($getmodule, 0, "modtype");
		$modname = lfmsql_result($getmodule, 0, "modname");
		if ($modtype > 2) {
			if(isset($_GET['confirmdel']) && $_GET['confirmdel'] == "yes") {
				lfmsql_query("DELETE FROM `oto_barmods_extensions` WHERE id='".$_GET['deleteext']."'") or die(lfmsql_error());
				lfmsql_query("UPDATE `oto_barmods_extensions` SET rank=rank-1 WHERE rank > '".$modrank."'");
			} else {
				echo("<center>
				<p><font size=3>Are you sure you want to delete this extension from your surfbar?</font></p>
				<p><font size=4><b>".$modname."</b></p>
				<p><font size=3><a href=\"admin.php?f=sbsettings&deleteext=".$_GET['deleteext']."&confirmdel=yes\">Yes</a></font></p>
				<p><font size=3><a href=\"admin.php?f=sbsettings\">No</a></font></p>
				</center>");
				exit;
			}
		}
	}
}


include("../surf_prefs.php");
$adminprefs = get_admin_prefs();

if($adminprefs["surfbarstyle"] < 1) {
	// Switch to the default surfbar
	@lfmsql_query("TRUNCATE TABLE ".$prefix."surf_admin_prefs");
	
	if (!isset($adminprefs['preloadsites']) || !is_numeric($adminprefs['preloadsites'])) {
		$adminprefs['preloadsites'] = -1;
	}
	
	@lfmsql_query("Insert into ".$prefix."surf_admin_prefs (field, value) VALUES
	('surfbarstyle', '2'),
	('preloadsites', '".$adminprefs['preloadsites']."'),
	('timertype', '1'),
	('timercolor', '0000FF');");
	
	$adminprefs = get_admin_prefs();
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td>

<center>
<p><font size="5"><b>Main Settings</b></font></p>
    
    
	<form action="admin.php?f=sbsettings" method="post" name="sbsettings" id="sbsettings">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
      <?
      // Update system settings
      if($_POST["Submit"] == "Update Surfbar Settings")
      { ?>
        <tr>
          <td  colspan="4" align="center">
            <font color="red"><strong><?=$msg;?></strong></font></td>
        </tr>
    <?
      }    
    ?>
    
    <?
    if(file_exists("../surf_frame_classic.php")) {
    ?>
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Surfbar Type</font> </strong></td>
        <td colspan="2" class="formfield" align="left">
        
        <select name="surfbarstyle">
        <option value="2"<? if($adminprefs["surfbarstyle"] == 2){echo(" selected");} ?>>LFMTE Default (Recommended)</option>
        <option value="1"<? if($adminprefs["surfbarstyle"] == 1){echo(" selected");} ?>>Deprecated</option>
        </select>
        
        </td>
        <td align="center"><a href="#" title="The deprecated surfbar is available so that any customizations and plugins can continue working while you implement them into the updated default surfbar."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
    <?
    	if($adminprefs["surfbarstyle"] == 1) {
    		echo("<tr><td colspan=\"4\" align=\"center\" nowrap=\"nowrap\"><input name=\"Submit\" type=\"submit\" class=\"footer-text\" value=\"Update Surfbar Settings\" /></td></tr>
    		<tr><td colspan=\"4\" align=\"center\"><p><font size=\"2\">Additional settings are available<br>with the LFMTE Default surfbar.</font></p></td></tr>");
    		exit;
    	}
      }
    ?>
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Preload Sites</strong></font></td>
        <td colspan="2" class="formfield" align="left">
        
        <select name="preloadsites">
        <option value="1"<? if($adminprefs["preloadsites"] == 1){echo(" selected");} ?>>Yes</option>
        <option value="0"<? if($adminprefs["preloadsites"] == 0){echo(" selected");} ?>>No</option>
        <option value="-1"<? if($adminprefs["preloadsites"] == -1){echo(" selected");} ?>>Members Choice</option>
        </select>
        
        </td>
        <td align="center"><a href="#" title="Preloads the next site in rotation while the timer is counting down.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Timer Type</strong></font></td>
        <td colspan="2" class="formfield" align="left">
        <select name="timertype">
        <option value="1"<? if($adminprefs["timertype"] == 1){echo(" selected");} ?>>Animated Image</option>
        <option value="0"<? if($adminprefs["timertype"] == 0){echo(" selected");} ?>>Plain Text</option>
        </select>
        </td>
        <td align="center"><a href="#" title="Choose whether to use an animated timer or a basic text countdown.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <?
      if($adminprefs["timertype"] == 1) {
      ?>
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Animated Timer Color</strong></font></td>
        <td colspan="2" class="formfield" align="left">
        # <input type="text" name="timercolor" size="6" value="<? echo($adminprefs["timercolor"]); ?>">
        </td>
        <td align="center"><a href="#" title="The six digit hex color code to use for the animated timer.  Example: 0000FF is blue.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <?
      }
      ?>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Update Surfbar Settings" /></td>
        </tr>
    </table>
	</form>
	<br>
<hr>

<p><font size="5"><b>Surfbar Modules</b></font>
<br><font size="3">The surf modules go across the top row of the surfbar.</font>
<br><font size="3">Click the arrow icons to move a module left or right.</font></p>
	
	<table border="1" bordercolor="black" cellpadding="5" cellspacing="0">
		<tr>
		<?
		
		$getmodules = lfmsql_query("SELECT id, state, rank, modtype, modname, moddescr FROM `".$prefix."barmods_topbar` ORDER BY rank ASC") or die(lfmsql_error());
		while ($modlist = lfmsql_fetch_array($getmodules)) {
			
			$modid = $modlist['id'];
			$modstate = $modlist['state'];
			$modrank = $modlist['rank'];
			$modtype = $modlist['modtype'];
			$modname = $modlist['modname'];
			$moddescr = $modlist['moddescr'];
			
			if ($modstate == 1) {
				$tdbg = "#73fb76";
			} else {
				$tdbg = "#DDDDDD";
			}
			
			$modreorder = "<a href=admin.php?f=sbsettings&modrank=left&rankid=".$modrank."><img height=\"20\" width=\"20\" border=\"0\" src=\"module_left.jpg\"></a> <a href=admin.php?f=sbsettings&modrank=right&rankid=".$modrank."><img height=\"20\" width=\"20\" border=\"0\" src=\"module_right.jpg\"></a><br>";
			
			if ($modtype == 2) {
				// Can reorder or disable but not delete or edit module
				if ($modstate == 1) {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&modstate=0&modid=".$modid."><font size=2><b>Click To Disable</b></font></a>";
				} else {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&modstate=1&modid=".$modid."><font size=2><b>Click To Enable</b></font></a>";
				}
			} elseif ($modtype == 3) {
				// Can reorder, disable, or delete, but not edit module
				if ($modstate == 1) {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&modstate=0&modid=".$modid."><font size=2><b>Click To Disable</b></font></a>";
				} else {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&modstate=1&modid=".$modid."><font size=2><b>Click To Enable</b></font></a>";
				}
				$modoptions .= "<br><br><a href=admin.php?f=sbsettings&deletemod=".$modid."><font size=2 color=darkred><b>Click To DELETE</b></font></a>";
			} elseif ($modtype >= 4) {
				// Can reorder, disable, edit or delete module
				if ($modstate == 1) {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&modstate=0&modid=".$modid."><font size=2><b>Click To Disable</b></font></a>";
				} else {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&modstate=1&modid=".$modid."><font size=2><b>Click To Enable</b></font></a>";
				}
				$modoptions .= "<br><br><a href=admin.php?f=sbsettings&addedit=".$modid."&addedittype=mod><font size=2><b>Edit</b></font></a>";
				$modoptions .= "<br><br><a href=admin.php?f=sbsettings&deletemod=".$modid."><font size=2 color=darkred><b>Click To DELETE</b></font></a>";
			} else {
				// Can only reorder
				$modoptions = "";
			}
			
			echo("<td width=\"150\" align=\"left\" valign=\"top\" bgcolor=\"".$tdbg."\">
			<center>
			".$modreorder."
			<br>
			<font size=3><b>".$modname."</b></font>
			<br>
			</center>
			<font size=2>".$moddescr."</font>
			<center>
			".$modoptions."
			</center>
			</td>");
			
		}
		?>
		</tr>
	</table>
	<br>
	<p><a href="admin.php?f=sbsettings&addedit=new&addedittype=mod"><font size="4">Add A New Module</font></a></p>
	<br>
	
<hr>

<p><font size="5"><b>Surfbar Extensions</b></font>
<br><font size="3">The surf extensions go below the main section of the surfbar.</font>
<br><font size="3">Click the arrow icons to move an extension up or down.</font></p>

	<table border="1" bordercolor="black" cellpadding="5" cellspacing="0">
		<tr>
		<?
		
		$getmodules = lfmsql_query("SELECT id, state, rank, modtype, modname, moddescr FROM `".$prefix."barmods_extensions` ORDER BY rank ASC") or die(lfmsql_error());
		while ($modlist = lfmsql_fetch_array($getmodules)) {
			
			$modid = $modlist['id'];
			$modstate = $modlist['state'];
			$modrank = $modlist['rank'];
			$modtype = $modlist['modtype'];
			$modname = $modlist['modname'];
			$moddescr = $modlist['moddescr'];
			
			if ($modstate == 1) {
				$tdbg = "#73fb76";
			} else {
				$tdbg = "#DDDDDD";
			}
			
			$modreorder = "<a href=admin.php?f=sbsettings&extrank=up&rankid=".$modrank."><img height=\"20\" width=\"20\" border=\"0\" src=\"module_up.jpg\"></a> <a href=admin.php?f=sbsettings&extrank=down&rankid=".$modrank."><img height=\"20\" width=\"20\" border=\"0\" src=\"module_down.jpg\"></a><br>";
			
			if ($modtype == 2) {
				// Can reorder or disable but not delete or edit module
				if ($modstate == 1) {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&extstate=0&extid=".$modid."><font size=2><b>Click To Disable</b></font></a>";
				} else {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&extstate=1&extid=".$modid."><font size=2><b>Click To Enable</b></font></a>";
				}
			} elseif ($modtype == 3) {
				// Can reorder, disable, or delete, but not edit module
				if ($modstate == 1) {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&extstate=0&extid=".$modid."><font size=2><b>Click To Disable</b></font></a>";
				} else {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&extstate=1&extid=".$modid."><font size=2><b>Click To Enable</b></font></a>";
				}
				$modoptions .= "<br><br><a href=admin.php?f=sbsettings&deleteext=".$modid."><font size=2 color=darkred><b>Click To DELETE</b></font></a>";
			} elseif ($modtype >= 4) {
				// Can reorder, disable, edit or delete module
				if ($modstate == 1) {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&extstate=0&extid=".$modid."><font size=2><b>Click To Disable</b></font></a>";
				} else {
					$modoptions = "<br><br><a href=admin.php?f=sbsettings&extstate=1&extid=".$modid."><font size=2><b>Click To Enable</b></font></a>";
				}
				$modoptions .= "<br><br><a href=admin.php?f=sbsettings&addedit=".$modid."&addedittype=ext><font size=2><b>Edit</b></font></a>";
				$modoptions .= "<br><br><a href=admin.php?f=sbsettings&deleteext=".$modid."><font size=2 color=darkred><b>Click To DELETE</b></font></a>";
			} else {
				// Can only reorder
				$modoptions = "";
			}
			
			echo("<tr>
			<td width=\"450\" align=\"left\" valign=\"top\" bgcolor=\"".$tdbg."\">
			<center>
			".$modreorder."
			<br>
			<font size=3><b>".$modname."</b></font>
			<br>
			<font size=2>".$moddescr."</font>
			".$modoptions."
			</center>
			</td>
			</tr>");
			
		}
		?>
		</tr>
	</table>
	<br>
	<p><a href="admin.php?f=sbsettings&addedit=new&addedittype=ext"><font size="4">Add A New Extension</font></a></p>
	<br>
	
	</td>
  </tr>

</table>
