<?php

// VisuGraph 1.0
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

$errormess = "";
$graphurl = "";
$canzoomin = "no";
$canzoomout = "no";

?>

<link href="styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="datepickercontrol.js"></script>
<link type="text/css" rel="stylesheet" href="datepickercontrol.css">

<center><br><br>
<p><a href="admin.php?f=mstats">Click Here To View Individual Member Stats</a></p>

<?

// Show New Graph
if ($_GET['drawgraph'] == "yes") {

	$starttime = mktime(0,0,0,substr($_POST['sdate'],5,2),substr($_POST['sdate'],8),substr($_POST['sdate'],0,4));
	$endtime = mktime(0,0,0,substr($_POST['edate'],5,2),substr($_POST['edate'],8),substr($_POST['edate'],0,4))+86399;
	
	if ($endtime > $starttime) {
	
		$datatype = $_POST['datatype'];
		$graphtype = $_POST['graphtype'];

		$checknumdays = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$_POST['sdate']."' and date<='".$_POST['edate']."' order by date asc");
		$numdays = mysql_result($checknumdays, 0);
		
		$gettotaldays = mysql_query("Select COUNT(*) from ".$prefix."datestats");
		$totaldays = mysql_result($gettotaldays, 0);
		
		if ($numdays > 0) {
		
			//Generate Image URL
			if ($graphtype == 1) {
				$graphurl = "drawlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype;
			} elseif ($graphtype == 2) {
				$graphurl = "drawbargraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype;
			} else {
				//Draw Text Chart
				include "surfstatschart.php"; 		
			}
			
			//Check Zoom Levels
			if ($numdays > 7) {
				$canzoomin = "yes";
			} else {
				$canzoomin = "no";
			}
			if ($numdays == $totaldays) {
				$canzoomout = "no";
			} else {
				$canzoomout = "yes";
			}
		
		} else {
			$errormess = "There is no data for this date range.";
		}
	} else {
		$errormess = "The 'Start' date must be before the 'End' date.";
	}
} elseif (($_GET['clickzoom'] == "yes") && is_numeric($_GET['starttime']) && is_numeric($_GET['endtime']) && is_numeric($_POST['x'])) {

// Zoom In Current Graph

	$starttime = $_GET['starttime'];
	$endtime = $_GET['endtime'];
	$datatype = $_GET['datatype'];
	$graphtype = $_GET['graphtype'];
	$clickedpixel = $_POST['x'] - 60;
	
	$timedistance = $endtime - $starttime;
	$distancefourths = round($timedistance/4);
	
	// Zoom The Start And End Times
	
	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
	$checknumdays = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."' order by date asc");
	$daysingraph = mysql_result($checknumdays, 0);
	
	$width = 660;
	$pixelsperday = $width/$daysingraph;
	
	$daystomove = ($clickedpixel/$pixelsperday)/2;
	
	$secondstomove = $daystomove*86400;
	
	$starttime = $starttime + $secondstomove;
	$endtime = $starttime + ($distancefourths*2);

	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
	
	$checknumdays = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."' order by date asc");
	$numdays = mysql_result($checknumdays, 0);
	
	$gettotaldays = mysql_query("Select COUNT(*) from ".$prefix."datestats");
	$totaldays = mysql_result($gettotaldays, 0);
	
	if ($numdays > 0) {
	
		//Generate Image URL
		if ($graphtype == 1) {
			$graphurl = "drawlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype;
		} else {
			$graphurl = "drawbargraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype;
		}
		
		//Check Zoom Levels
		if ($numdays > 7) {
			$canzoomin = "yes";
		} else {
			$canzoomin = "no";
		}
		if ($numdays == $totaldays) {
			$canzoomout = "no";
		} else {
			$canzoomout = "yes";
		}
	
	} else {
	//	$errormess = "There is no data for this date range.";
	}
} elseif ((($_GET['changezoom'] == "in") || ($_GET['changezoom'] == "out")) && is_numeric($_GET['starttime']) && is_numeric($_GET['endtime'])) {

// Change Zoom

	$starttime = $_GET['starttime'];
	$endtime = $_GET['endtime'];
	$datatype = $_GET['datatype'];
	$graphtype = $_GET['graphtype'];
	
	$timedistance = $endtime - $starttime;
	$distancefourths = round($timedistance/4);
	
	// Zoom The Start And End Times
	if ($_GET['changezoom'] == "in") {
		$starttime = $starttime + $distancefourths;
		$endtime = $starttime + ($distancefourths*2);
	} else {
		$checkdate = date("Y-m-d",$starttime - $distancefourths);
		$checknumdays = mysql_query("Select COUNT(*) from ".$prefix."datestats where date<='".$checkdate."'");
		$numdays = mysql_result($checknumdays, 0);
		if ($numdays > 0) {
			$starttime = $starttime - ($distancefourths*2);
		} else {
			$getfirst = mysql_query("Select date from ".$prefix."datestats order by date asc limit 1");
			$firstdate = mysql_result($getfirst, 0, "date");
			$starttime = mktime(0,0,0,substr($firstdate,5,2),substr($firstdate,8),substr($firstdate,0,4));
		}
		
		$checkdate = date("Y-m-d",$starttime + ($distancefourths*8));
		$checknumdays = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$checkdate."'");
		$numdays = mysql_result($checknumdays, 0);
		if ($numdays > 0) {
			$endtime = $starttime + ($distancefourths*8);
		} else {
			$getlast = mysql_query("Select date from ".$prefix."datestats order by date desc limit 1");
			$lastdate = mysql_result($getlast, 0, "date");
			$endtime = mktime(0,0,0,substr($lastdate,5,2),substr($lastdate,8),substr($lastdate,0,4));
		}
	}
	
	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
	
	$checknumdays = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."'");
	$numdays = mysql_result($checknumdays, 0);
	
	$gettotaldays = mysql_query("Select COUNT(*) from ".$prefix."datestats");
	$totaldays = mysql_result($gettotaldays, 0);
	
	if ($numdays > 0) {
	
		//Generate Image URL
		if ($graphtype == 1) {
			$graphurl = "drawlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype;
		} else {
			$graphurl = "drawbargraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype;
		}
		
		//Check Zoom Levels
		if ($numdays > 7) {
			$canzoomin = "yes";
		} else {
			$canzoomin = "no";
		}
		if ($numdays == $totaldays) {
			$canzoomout = "no";
		} else {
			$canzoomout = "yes";
		}
	
	} else {
		$errormess = "There is no data for this date range.";
	}

} else {
	//Show the weekly hits graph
	$currentdate = date("Y-m-d");
	$startdate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));
	$enddate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
	$datatype = 1;
	$graphtype = 1;
	
	$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
	$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;
	
	$checknumdays = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."' order by date asc");
	$numdays = mysql_result($checknumdays, 0);
	
	$gettotaldays = mysql_query("Select COUNT(*) from ".$prefix."datestats");
	$totaldays = mysql_result($gettotaldays, 0);
	
	if ($numdays > 0) {
	
		//Generate Image URL
		$graphurl = "drawlinegraph.php?starttime=".$starttime."&endtime=".$endtime."&charttype=".$datatype;
		
		//Check Zoom Levels
		if ($numdays > 7) {
			$canzoomin = "yes";
		} else {
			$canzoomin = "no";
		}
		if ($numdays == $totaldays) {
			$canzoomout = "no";
		} else {
			$canzoomout = "yes";
		}
	}
}

if ($starttime>0 && $endtime>0) {
	$startdate = date("Y-m-d",$starttime);
	$enddate = date("Y-m-d",$endtime);
} else {
	$currentdate = date("Y-m-d");
	$startdate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));
	$enddate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
}

if ($errormess != "") {
echo("<p>".$errormess."</p>");
}

?>
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td>
	<form action="admin.php?f=surfstats&drawgraph=yes" method="post">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Start Date: </strong></font></td>
        <td colspan="2" class="formfield">
        <input name="sdate" type id="DPC_sdate_YYYY-MM-DD" value="<? echo($startdate); ?>" width="80" text>
        </td>
        <td align="center"><a href="#" title="The starting date that will be shown in the graph.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
 
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>End Date: </strong></font></td>
        <td colspan="2" class="formfield">
        <input name="edate" type id="DPC_edate_YYYY-MM-DD" value="<? echo($enddate); ?>" width="80" text>
        </td>
        <td align="center"><a href="#" title="The ending date that will be shown in the graph.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Data Type</font> </strong></td>
        <td colspan="2" class="formfield">
<select name="datatype">
<option value=1<? if ($datatype == 1) { echo(" selected"); } ?>>Hits Delivered</option>
<option value=2<? if ($datatype == 2) { echo(" selected"); } ?>>Credits Earned</option>
<option value=3<? if ($datatype == 3) { echo(" selected"); } ?>>Members Surfed</option>
<option value=4<? if ($datatype == 4) { echo(" selected"); } ?>>Hits Per URL</option>
<option value=5<? if ($datatype == 5) { echo(" selected"); } ?>>Imps Per Banner</option>
<option value=6<? if ($datatype == 6) { echo(" selected"); } ?>>Imps Per Text Ad</option>
<option value=7<? if ($datatype == 7) { echo(" selected"); } ?>>New Signups</option>
</select>
        </td>
        <td align="center"><a href="#" title="The type of data you want to view."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Graph Type</font> </strong></td>
        <td colspan="2" class="formfield">
<select name="graphtype">
<option value=1<? if ($graphtype == 1) { echo(" selected"); } ?>>Line Graph</option>
<option value=2<? if ($graphtype == 2) { echo(" selected"); } ?>>Bar Graph</option>
<option value=3<? if ($graphtype == 3) { echo(" selected"); } ?>>Text Chart</option>
</select>
        </td>
        <td align="center"><a href="#" title="The type of graph that will be displayed."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Show Stats" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>

<BR>

<?

if ($graphurl != "") {

	if ($canzoomin == "yes") {
		echo("<table border=0 cellpadding=0 cellspacing=0 width=760 height=500 id=\"graphimg\"><tr><td><form style=\"margin:0px\" action=\"admin.php?f=surfstats&clickzoom=yes&starttime=".$starttime."&endtime=".$endtime."&datatype=".$datatype."&graphtype=".$graphtype."#graphimg\" method=\"post\">
		<input type=\"image\" src=\"".$graphurl."\">
		</form></td></tr></table>");
		
		echo("<a href=\"admin.php?f=surfstats&changezoom=in&starttime=".$starttime."&endtime=".$endtime."&datatype=".$datatype."&graphtype=".$graphtype."#graphimg\">Zoom In</a><br><br>");
		
	} else {
		echo("<table border=0 cellpadding=0 cellspacing=0 width=760 height=500 id=\"graphimg\"><tr><td><img border=\"0\" src=\"".$graphurl."\"></td></tr></table>");
	}
	
	if ($canzoomout == "yes") {
		echo("<a href=\"admin.php?f=surfstats&changezoom=out&starttime=".$starttime."&endtime=".$endtime."&datatype=".$datatype."&graphtype=".$graphtype."#graphimg\">Zoom Out</a><br>");
	}

} elseif ($charthtml != "") {
	echo($charthtml);
}

?>
<br><br><br><br><br>
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  
  <tr>
    <td align="left" valign="top" bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">If the date range is larger than 40 days, the graph will be shown in monthly view.  For the 'Hits Delivered', 'Credits Earned', and 'New Signups' data types, the monthly totals will be shown in monthly view.  For the other data types, the daily average for that month will be shown.</font></p>
      
      </font></p>

      <p>&nbsp;</p></td>
  </tr>
</table>
</body>
</html>

<?
exit;
?>