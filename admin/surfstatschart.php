<?php

// VisuGraph 1.0
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

if(!isset($_SESSION["adminid"])) { exit; };
if(!isset($datatype)) { exit; };

//Draw Text Chart

if ($datatype == 1) {
	$chartheading = "Hits Delivered";
	$searchvalue = "hitsdelivered";
} elseif ($datatype == 2) {
	$chartheading = "Credits Earned";
	$searchvalue = "crdsearned";
} elseif ($datatype == 3) {
	$chartheading = "Members Surfed";
	$searchvalue = "memsurfed";
} elseif ($datatype == 4) {
	$chartheading = "Hits Per URL";
	$searchvalue = "perurl";
} elseif ($datatype == 5) {
	$chartheading = "Imps Per Banner";
	$searchvalue = "perbanner";
} elseif ($datatype == 6) {
	$chartheading = "Imps Per Text Ad";
	$searchvalue = "pertext";
} elseif ($datatype == 7) {
	$chartheading = "New Signups";
	$searchvalue = "signups";
}

$startdate = $_POST['sdate'];
$enddate = $_POST['edate'];

$charthtml = "<table width=\"300\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\" class=\"lfmtable\" style=\"border: 1px solid #999;\">
  <tr>
    <td align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Date</font> </strong></td>
    <td align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$chartheading."</font> </strong></td>
  </tr>";
  
$getdays = mysql_query("Select ".$searchvalue.", date from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."' order by date asc");
$numdays = mysql_num_rows($getdays);
$numelems = 0;

if ($numdays > 40) {
	//Switch to monthly view
	$getmonths = mysql_query("Select date from ".$prefix."datestats where date>='".$startdate."' and date<='".$enddate."' order by date asc");
	
		for ($i = 0; $i < mysql_num_rows($getmonths); $i++) {
						
		$selectdate = mysql_result($getmonths, $i, "date");
		$selecteddate = mktime(0,0,0,substr($selectdate,5,2),substr($selectdate,8),substr($selectdate,0,4));
		$selectedday = date("d", $selecteddate);
		
		if ($selectedday == 28) {
		$selectedmonth = date("m", $selecteddate);
		$selectedyear = date("Y", $selecteddate);
		$daya = $selectedyear."-".$selectedmonth."-"."01";
		$dayb = $selectedyear."-".$selectedmonth."-"."31";
		$monthname = date("M", $selecteddate);
		
		if ($monthname == "Jan") {
			$monthname = "Jan ".$selectedyear;
		}
	
		$getmonth = mysql_query("Select SUM(".$searchvalue.") from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
		$currentsum = mysql_result($getmonth, 0);
		
		if ($datatype > 2 && $datatype < 7) {
			// Take the daily average for the month
			$countmonth = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
			$monthcount = mysql_result($countmonth, 0);
			if ($monthcount > 0) {
				$currentsum = round($currentsum/$monthcount);
			}
		}
	
		$statsum[$numelems] = $currentsum;
		$xname[$numelems] = $monthname;
		$numelems = $numelems+1;
		}
	
		}
	
		// Add The Current Month
		$currentdate = date("Y-m-d",time());
		$currentday = date("d", time());
		$currentmonth = date("m", time());
		$currentyear = date("Y", time());
		$daya = $currentyear."-".$currentmonth."-"."01";
		$dayb = $currentyear."-".$currentmonth."-"."31";
		if (($currentday <= 28) && ($daya <= $enddate)) {
			$monthname = date("M", time());
				
			if ($monthname == "Jan") {
				$monthname = "Jan ".$currentyear;
			}
	
			$countmonth = mysql_query("Select COUNT(*) from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
			$monthcount = mysql_result($countmonth, 0);
		
			if ($monthcount > 0) {
		
			$getmonth = mysql_query("Select SUM(".$searchvalue.") from ".$prefix."datestats where date>='".$daya."' and date<='".$dayb."'");
			$currentsum = mysql_result($getmonth, 0);
		
			if ($datatype > 2) {
				// Take the daily average for the month
				$currentsum = round($currentsum/$monthcount);
			}

			$statsum[$numelems] = $currentsum;
			$xname[$numelems] = $monthname;
			$numelems = $numelems+1;
			}
		}
					
} else {
				
	for ($i = 0; $i < mysql_num_rows($getdays); $i++) {
	
		$thedate = mysql_result($getdays, $i, "date");
		$statsum[$i] = mysql_result($getdays, $i, $searchvalue);
		$xname[$i] = $thedate;
		$numelems = $numelems+1;

	}
				
}

if ($numelems == 0) {
	$charthtml .= "<tr><td colspan=2 align=\"center\"><b>There is no data for this date range.</b></td></tr>";
} else {
				
	for ($i = 0; $i < mysql_num_rows($getdays); $i++) {
		$charthtml .= "<tr><td align=\"center\">".$xname[$i]."</td><td align=\"center\">".$statsum[$i]."</td></tr>";
	}
}
	
$charthtml .= "</table>";

?>