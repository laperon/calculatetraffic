<?php

// LFMTE Surfing tikets
// �2012 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if (!isset($_SESSION["adminid"])) {
    exit;
};

$errormess = "";
?>


<ul>
    <li>
        Display list of users with tickets won.
    </li>
    <li>
        Display history of winners with prizes won.
    </li>
    <li>
        Ability to control page display frequency (base is one ticket won for every 75 clicks).
    </li>
    <li>
        Ability to change draw value variables, both base prize and per ticket values.
    </li>
    <li>
        Random draw feature – either manually or automatically (preferred option) – similar to Promo Code draw feature.
    </li>
    <li>
        Automatic awarding of prize and emailing winner with details (Admin can create/modify the email).
    </li>
</ul>


<center><h1>Setting page</h1></center>
<!--<link href="styles.css" rel="stylesheet" type="text/css" />-->
<!--<table width="100%" border="0" cellspacing="0" cellpadding="4">-->
<!--    <tr>-->
<!--        <td>-->
<!--            <form action="admin.php?f=socialsb&updatesettings=yes" method="post">-->
<!--                <table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">-->
<!--                    <tr>-->
<!--                        <td>title</td>-->
<!--                        <td>Credits</td>-->
<!--                        <td>Banners</td>-->
<!--                        <td>Texts</td>-->
<!--                        <td>Cash</td>-->
<!--                        <td>action</td>-->
<!--                    </tr>-->
<!---->
<!--                    <tr>-->
<!--                        <td>Base prise</td>-->
<!--                        <td>100</td>-->
<!--                        <td>500</td>-->
<!--                        <td>500</td>-->
<!--                        <td>100$</td>-->
<!--                        <td nowrap="nowrap"><a href="javascript:editTicketSetting(--><? //=$prodrow["id"];?><!--);"><img src="../images/edit.png" width="16" height="16" border="0" /></a></td>-->
<!--                    </tr>-->
<!--                </table>-->
<!--            </form>-->
<!--        </td>-->
<!--    </tr>-->
<!---->
<!--</table>-->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td>

            <center><br><br>
                <link href="styles.css" rel="stylesheet" type="text/css">
                <table style="border:thin solid #000 ; border-width: 1px;" border="0" align="center" cellpadding="4"
                       cellspacing="0" width="400">
                    <tbody>
                        <tr>
                            <td align="center" class="admintd">Name</td>
                            <td align="center" class="admintd">Credits</td>
                            <td align="center" class="admintd">Banners</td>
                            <td align="center" class="admintd">Text Imps</td>
                            <td align="center" class="admintd">Cash</td>
                            <td align="center" class="admintd">Frequency</td>
                            <td align="center" class="admintd">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                Base Prize
                            </td>
                            <td>
                                <input type="text" name="credits" value="100">
                            </td>
                            <td>
                                <input type="text" name="banners" value="100">
                            </td>
                            <td>
                                <input type="text" name="text" value="100">
                            </td>
                            <td>
                                <input type="text" name="cash" value="100">
                            </td>
                            <td>
                                <input type="text" name="frequency" value="75">
                            </td>
                            <td>
                                <input type="submit" value="save">
                            </td>
                        </tr>
                    </tbody>
                </table>
        </td>
    </tr>
    </tbody>
</table>