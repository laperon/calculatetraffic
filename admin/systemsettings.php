<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

		// Get path for affiliate URL
		$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
		$aff_path=substr($fullurl,0,strlen($fullurl)-15);


	// Get path to PHP
	$php_path = @exec( 'which php');
	if(isset($_SERVER['DOCUMENT_ROOT']))
	{
		$fullapp_path = $_SERVER['DOCUMENT_ROOT'];
	}
	else
	if(isset($_ENV['DOCUMENT_ROOT']))
	{
		$fullapp_path = $_ENV['DOCUMENT_ROOT'];
	}
	else
	if(isset($_SERVER['PATH_TRANSLATED']))
	{
		$fullapp_path = $_SERVER['PATH_TRANSLATED'];
	}
	else
	{
		$fullapp_path = $_SERVER['SCRIPT_FILENAME'];
	}

	// Strip filename from $fullapp_path
	$app_path=substr($fullapp_path,0,strlen($fullapp_path)-15);

// Update system settings
if($_POST["Submit"] == "Update General Settings")
{
	if(strlen($_POST["sitename"]) < 1) { $msg="Error: Sitename cannot be empty!"; }
	else
	if(strlen($_POST["admin_name"]) < 1) { $msg="Error: Admin Name cannot be empty!"; }
	else
	if(strlen($_POST["admin_email"]) < 1) { $msg="Error: Admin Email cannot be empty!"; }
	else
	{
		if(!is_numeric($_POST["hitsconnectid"])){ $_POST["hitsconnectid"]=1; }
		
		if(isset($_POST["bgemail"])){ $bgemail=1; } else { $bgemail=0; }
		if(isset($_POST["pptestmode"])){ $pptestmode=1; } else { $pptestmode=0; }
		if(isset($_POST["paypal_enable"])){ $paypal_enable=1; } else { $paypal_enable=0; }
		if(isset($_POST["_2co_enable"])){ $_2co_enable=1; } else { $_2co_enable=0; }
		if(isset($_POST["authnet_enable"])){ $authnet_enable=1; } else { $authnet_enable=0; }
		if(isset($_POST["addtophpbb"])){ $addtophpbb=1; } else { $addtophpbb=0; }
		if(isset($_POST["fileprot"])){ $fileprot=1; } else { $fileprot=0; }
		$verifyemail=0;
		if(isset($_POST["notifysale"])){ $notifysale=1; } else { $notifysale=0; }
		if(isset($_POST["notifydownline"])){ $notifydownline=1; } else { $notifydownline=0; }
		if(isset($_POST["firstcookie"])){ $firstcookie=1; } else { $firstcookie=0; }
		if(isset($_POST["bannerurls"])){ $bannerurls=1; } else { $bannerurls=0; }
		if(isset($_POST["flow"])){ $flow=1; } else { $flow=0; }
		if(isset($_POST["cb_enable"])){ $cb_enable=1; } else { $cb_enable=0; }
		
        if(isset($_POST["enablemenu_home"])){ $enablemenu_home=$_POST["enablemenu_home"]; } else { $enablemenu_home=0; }
        if(isset($_POST["enablemenu_ref"])){ $enablemenu_ref=$_POST["enablemenu_ref"]; } else { $enablemenu_ref=0; }
        if(isset($_POST["enablemenu_profile"])){ $enablemenu_profile=$_POST["enablemenu_profile"]; } else { $enablemenu_profile=0; }
        if(isset($_POST["enablemenu_promote"])){ $enablemenu_promote=$_POST["enablemenu_promote"]; } else { $enablemenu_promote=0; }
        if(isset($_POST["enablemenu_makemoney"])){ $enablemenu_makemoney=$_POST["enablemenu_makemoney"]; } else { $enablemenu_makemoney=0; }
        if(isset($_POST["enablemenu_downloads"])){ $enablemenu_downloads=$_POST["enablemenu_downloads"]; } else { $enablemenu_downloads=0; }
        if(isset($_POST["enablemenu_dlb"])){ $enablemenu_dlb=$_POST["enablemenu_dlb"]; } else { $enablemenu_dlb=0; }
        if(isset($_POST["enablemenu_upgrade"])){ $enablemenu_upgrade=$_POST["enablemenu_upgrade"]; } else { $enablemenu_upgrade=0; }
        if(isset($_POST["enablemenu_forum"])){ $enablemenu_forum=$_POST["enablemenu_forum"]; } else { $enablemenu_forum=0; }
        if(isset($_POST["enablemenu_logout"])){ $enablemenu_logout=$_POST["enablemenu_logout"]; } else { $enablemenu_logout=0; }
        
        if(isset($_POST["indextheme"])){ $indextheme=$_POST["indextheme"]; } else { $indextheme=0; }
        
        if (!isset($_POST["delsusp"]) || !is_numeric($_POST["delsusp"]) || $_POST["delsusp"] < 0) {
        	$_POST["delsusp"] = 0;
        }
        
        if (!isset($_POST["max_fail_name"]) || !is_numeric($_POST["max_fail_name"]) || $_POST["max_fail_name"] < 0) {
        	$_POST["max_fail_name"] = 0;
        }
        
        if (!isset($_POST["max_fail_ip"]) || !is_numeric($_POST["max_fail_ip"]) || $_POST["max_fail_ip"] < 0) {
        	$_POST["max_fail_ip"] = 0;
        }
        
        if(isset($_POST["nodelpaid"])){ $nodelpaid=$_POST["nodelpaid"]; } else { $nodelpaid=0; }
        
        if (!isset($_POST["m_spon_did"]) || !is_numeric($_POST["m_spon_did"]) || $_POST["m_spon_did"] < 0) {
        	$_POST["m_spon_did"] = 0;
        }
		
		if(isset($_POST["reqemailver"])){ $reqemailver=1; } else { $reqemailver=0; }
		
		if(isset($_POST["reqrever"])){ $reqrever=1; } else { $reqrever=0; }
		
		if(isset($_POST["allowduplicates"])){ $allowduplicates=1; } else { $allowduplicates=0; }
		
		if(isset($_POST["framebreakblock"])){ $framebreakblock=1; } else { $framebreakblock=0; }
		
		if(isset($_POST["showrefmail"])){ $showrefmail=1; } else { $showrefmail=0; }
		
		$splittest=$_POST["splittest"];
		
		if (isset($_POST['storeimps'])) {
			$storeimps = 1;
		} else {
			$storeimps = 0;
		}

					
		// Update site settings
		$qry="UPDATE ".$prefix."settings SET sitename='".$_POST["sitename"]."',sitedesc='".$_POST["sitedesc"]."',meta_description='".$_POST["meta_description"]."',
        meta_keywords='".$_POST["meta_keywords"]."',hitsconnectid=".$_POST["hitsconnectid"].",paypal_email='".$_POST["paypal_email"]."',affurl='".$_POST["affurl"]."',
        php_path='".$php_path."',documentroot='".$app_path."',smtp_server='".$_POST["smtp_server"]."',
        replyaddress='".$_POST["replyaddress"]."', bounceaddress='".$_POST["bounceaddress"]."', auto_email='".$_POST["auto_email"]."', bgemail=$bgemail,
        pptestmode=$pptestmode,pptestemail='".$_POST["pptestemail"]."',_2co_id='".$_POST["_2co_id"]."', _2co_secret='".$_POST["_2co_secret"]."',
		authnet_login='".$_POST["authnet_login"]."', authnet_key='".$_POST["authnet_key"]."', paypal_enable=$paypal_enable,a_meta_web_id='".$_POST["a_meta_web_id"]."',
		a_unit='".$_POST["a_unit"]."',a_req_fields='".$_POST["a_req_fields"]."',autoresponder=".$_POST["autoresp"].",
		phpbb_path='".$_POST["phpbb_path"]."',phpbb_host='".$_POST["phpbb_host"]."',phpbb_db='".$_POST["phpbb_db"]."',phpbb_dbuser='".$_POST["phpbb_dbuser"]."',
		phpbb_dbpass='".$_POST["phpbb_dbpass"]."',phpbb_prefix='".$_POST["phpbb_prefix"]."',
		phpbb_group='".$_POST["phpbb_group"]."', addtophpbb=$addtophpbb, _2co_enable=$_2co_enable, authnet_enable=$authnet_enable,
		filelibpath='".$_POST["filelibpath"]."', fileprot=$fileprot, pp_curr='".$_POST["pp_curr"]."',firstcookie=$firstcookie,
		bannerurls=$bannerurls,flow=$flow,splittest=$splittest,cb_enable=$cb_enable,cb_publisherid='".$_POST["cb_publisherid"]."',cb_secret='".$_POST["cb_secret"]."', 
		cb_hoplink='".$_POST["cb_hoplink"]."',phpbbver=".$_POST["phpbbver"].",verifyemail=$verifyemail,notifysale=$notifysale,notifydownline=$notifydownline";
		
	@lfmsql_query("UPDATE ".$prefix."settings SET enablemenu_home=$enablemenu_home,enablemenu_ref=$enablemenu_ref,enablemenu_profile=$enablemenu_profile,enablemenu_promote=$enablemenu_promote,enablemenu_makemoney=$enablemenu_makemoney,enablemenu_downloads=$enablemenu_downloads,enablemenu_dlb=$enablemenu_dlb,enablemenu_upgrade=$enablemenu_upgrade,enablemenu_forum=$enablemenu_forum,enablemenu_logout=$enablemenu_logout,indextheme='".$_POST["indextheme"]."',delsusp='".$_POST["delsusp"]."',nodelpaid='".$nodelpaid."',max_fail_name='".$_POST["max_fail_name"]."',max_fail_ip='".$_POST["max_fail_ip"]."',min_comm=".$_POST["min_comm"].",arp_email='".$_POST["arp_email"]."',gvo_affiliate_name='".$_POST["gvo_affiliate_name"]."',gvo_campaign='".$_POST["gvo_campaign"]."',gvo_form_id='".$_POST["gvo_form_id"]."',m_spon_did='".$_POST["m_spon_did"]."',trwvid='".$_POST["trwvid"]."',trwvseries='".$_POST["trwvseries"]."'");

        lfmsql_query($qry) or die("<br><br>Error updating site settings: ".lfmsql_error());
        
        //Update TE settings
        lfmsql_query("UPDATE ".$prefix."settings SET storeimps=".$storeimps.", defurl='".$_POST['defurl']."', defbanimg='".$_POST['defbanimg']."', defbantar='".$_POST['defbantar']."', deftextad='".$_POST['deftextad']."', deftexttar='".$_POST['deftexttar']."', rotationtype='".$_POST['rotationtype']."', allowduplicates='".$allowduplicates."', surflogdays='".$_POST['surflogdays']."', framebreakblock='".$framebreakblock."', showrefmail='".$showrefmail."', unveridel='".$_POST['unveridel']."', unverirem='".$_POST['unverirem']."'") or die("<br><br>Error updating TE settings: ".lfmsql_error());
        
        lfmsql_query("UPDATE ".$prefix."settings SET reqemailver='".$reqemailver."'") or die("<br><br>Error updating settings: ".lfmsql_error());
        
        lfmsql_query("UPDATE ".$prefix."settings SET reqrever='".$reqrever."'") or die("<br><br>Error updating settings: ".lfmsql_error());
		
		// Update admin settings
		$aqry="UPDATE ".$prefix."admin SET admin_name='".$_POST["admin_name"]."',email='".$_POST["admin_email"]."',remotekey='0'";
		lfmsql_query($aqry) or die("Error updating admin settings: ".lfmsql_error());
		$msg="Settings were updated";
		
		// Enable file protection
		if($fileprot==1)
		{
			if ( ! isset($_SERVER['DOCUMENT_ROOT'] ) )
		  	$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr(
    		$_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF']) ) );
		
			$serverpath=$_SERVER["DOCUMENT_ROOT"];
			$filelibpath=trim($_POST["filelibpath"]);
			
			$protfile=$serverpath.$filelibpath.".htaccess";
			if(!$fp=fopen($protfile,"w"))
			{
				$msg="Error: Unable to create .htaccess ($protfile)!";
			}
			else
			{
                // Get our local variables
                $fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
                $admin_path=substr($fullurl,0,strrpos($fullurl, "/"));
                $root_path=substr($admin_path,0,strrpos($admin_path, "/"));
                $loginurl=$root_path."/login.php";

				$htaccess="RewriteEngine on\n";
				$htaccess.="RewriteCond %{HTTP_COOKIE} !(lfmpr3s5gy) [NC]\n";
				$htaccess.="RewriteRule ^(.*)$ $loginurl [R,L]\n";
				fwrite($fp,$htaccess);
			}
		}
	}	
}

$res=@lfmsql_query("SELECT * FROM ".$prefix."settings");
$row=@lfmsql_fetch_array($res);

$ares=@lfmsql_query("SELECT * FROM ".$prefix."admin");
$arow=@lfmsql_fetch_array($ares);
?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><p>&nbsp;</p><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td>
	<form action="admin.php?f=ss" method="post" name="ss" id="ss">
	<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
      <?
      // Update system settings
      if($_POST["Submit"] == "Update General Settings")
      { ?>
        <tr>
          <td  colspan="4" align="center">
            <font color="red"><strong><?=$msg;?></strong></font></td>
        </tr>
    <?
      }    
    ?>
    <tr>
        <td colspan="4" nowrap="nowrap" class="admintd"><font face="Verdana, Arial, Helvetica, sans-serif"><strong>General Settings</strong></font> </td>
        </tr>

      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Sitename</strong></font></td>
        <td colspan="2" class="formfield"><input name="sitename" type="text" class="formfield" id="sitename" value="<?=$row["sitename"];?>" size="32" /></td>
        <td align="center"><a href="#" title="This is the name of your site that will appear in the title of the web browser.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Site Description</strong></font></td>
        <td colspan="2" class="formfield"><input name="sitedesc" type="text" class="formfield" id="sitedesc" value="<?=$row["sitedesc"];?>" size="40" maxlength="40"/></td>
        <td align="center"><a href="#" title="This is the desctiion, slogan etc of your site that appears on some themes.">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>      
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Meta Description </strong></font></td>
        <td colspan="2" class="formfield"><input name="meta_description" type="text" class="formfield" id="meta_description" value="<?=$row["meta_description"];?>" size="40" maxlength="254" /></td>
        <td align="center"><a href="#" title="This is the description of your site that some search engines will use."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Meta keywords</strong></font></td>
        <td colspan="2" class="formfield"><textarea name="meta_keywords" cols="40" rows="4" class="formfield" id="meta_keywords"><?=$row["meta_keywords"];?></textarea></td>
        <td align="center"><a href="#" title="The keywords for your site that some search engines will use."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <input name="hitsconnectid" type="hidden" id="hitsconnectid" value="0"/>

      <tr>
        <td nowrap="nowrap" class="button"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td colspan="4" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Admin Configuration </font></strong></td>
        </tr>

      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Name</font> </strong></td>
        <td colspan="2" class="formfield"><input name="admin_name" type="text" class="formfield" id="admin_name" value="<?=$arow["admin_name"];?>" /></td>
        <td align="center"><a href="#" title="The administrator's real name (e.g. John Citizen)."><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>

      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Admin Email </font></strong></td>
        <td colspan="2" class="formfield"><input name="admin_email" type="text" class="formfield" id="admin_email" value="<?=$arow["email"];?>" size="32" /></td>
        <td align="center"><a title="The administrator email address. Payment notifications from LFM will be sent to this address." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>

      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Mail List 'From' Address:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="replyaddress" type="text" class="formfield" id="replyaddress" value="<?=$row["replyaddress"];?>" /></td>
        <td align="center"><a title="The address that will appear on emails sent to members when they sign up and on the emails you send from within the LFM admin area." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Bounce Notification Address:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="bounceaddress" type="text" class="formfield" id="bounceaddress" value="<?=$row["bounceaddress"];?>" /></td>
        <td align="center"><a title="Bounce notifications will be sent to this address." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>

      
      <tr>
        <td colspan="4" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Brute Booter</font></strong>
        <br>
        <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Password Brute Force Protection</font></td>
        </tr>

      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Invalid Logins Per Account</font>: </strong></td>
        <td colspan="2" class="formfield"><input name="max_fail_name" type="text" class="formfield" id="max_fail_name" value="<?=$row["max_fail_name"];?>" /></td>
        <td align="center"><a href="#" title="An account will be temporarily locked after this number of login attempts using an invalid password."><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>

      <tr>
        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Max Invalid Logins Per IP: </font></strong></td>
        <td colspan="2" class="formfield"><input name="max_fail_ip" type="text" class="formfield" id="max_fail_ip" value="<?=$row["max_fail_ip"];?>" /></td>
        <td align="center"><a title="An IP Address will be temporarily blocked after this number of login attempts using an invalid username or password." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      

      <tr>
        <td colspan="4" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Traffic Exchange Configuration </font></strong></td>
        </tr>
        
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Rotation Type: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
          <select name="rotationtype" class="formfield" id="rotationtype">
            <option value="0"<? if($row["rotationtype"] == 0) { ?> selected="selected"<? } ?>>Per Member (Recommended)</option>
            <option value="1"<? if($row["rotationtype"] == 1) { ?> selected="selected"<? } ?>>Per Site</option>
          </select>
        </td>
        <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a title="Per Member means that all members who have credits assigned will receive an equal number of hits.  These hits will be evenly distributed among their sites.  Per Member is the default and recommended setting." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></font></td>
      </tr>
        
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Allow Duplicate Sites: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
		  <? if($row["allowduplicates"] == 1) { ?>
		<input name="allowduplicates" type="checkbox" id="allowduplicates" value="1" checked="checked" />
		  <? } else { ?>
		<input name="allowduplicates" type="checkbox" id="allowduplicates" value="1" />
		  <? } ?></td>
        <td align="center"><a title="If checked, a member can submit the same site, banner, or text ad multiple times.  This is not recommended unless you are using the Per Site rotation type." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Enable Frame Break Blocker:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="framebreakblock" type="checkbox" <? if($row["framebreakblock"] == 1) { echo(" checked"); } ?>></td>
        <td align="center"><a title="Helps block frame breakers and makes it easier for members to report them." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
        
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Use Banner/Text Imps:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="storeimps" type="checkbox" <? if($row["storeimps"] == 1) { echo(" checked"); } ?>></td>
        <td align="center"><a title="Keeps banner and text impression balances, so members can buy or earn impressions for banners and text ads." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
        
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Default URL:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="defurl" type="text" class="formfield" size="40" id="defurl" value="<?=$row["defurl"];?>" /></td>
        <td align="center"><a title="The site shown if no other sites are in rotation." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Default Banner Image:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="defbanimg" type="text" class="formfield" size="40" id="defbanimg" value="<?=$row["defbanimg"];?>" /></td>
        <td align="center"><a title="The banner shown if no other banners are in rotation." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Default Banner Target:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="defbantar" type="text" class="formfield" size="40" id="defbantar" value="<?=$row["defbantar"];?>" /></td>
        <td align="center"><a title="The banner shown if no other banners are in rotation." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Default Text Ad:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="deftextad" type="text" class="formfield" size="40" id="deftextad" value="<?=$row["deftextad"];?>" /></td>
        <td align="center"><a title="The text ad shown if no other text ads are in rotation." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Default Text Ad Target:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="deftexttar" type="text" class="formfield" size="40" id="deftexttar" value="<?=$row["deftexttar"];?>" /></td>
        <td align="center"><a title="The text ad shown if no other text ads are in rotation." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Keep Surf Logs:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="surflogdays" type="text" class="formfield" size="10" id="surflogdays" value="<?=$row["surflogdays"];?>" /> days</td>
        <td align="center"><a title="The number of days before a member surf log is deleted.  This will affect lifetime stats and any active leader boards within the deleted range.  Set to 0 to never delete surf logs." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Remind Unverified Members:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="unverirem" type="text" class="formfield" size="10" id="unverirem" value="<?=$row["unverirem"];?>" /> days</td>
        <td align="center"><a title="The number of days before an unverified member is sent a verification reminder e-mail.  Set to 0 to disable verification reminders." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Delete Unverified Members:</font> </strong></td>
        <td colspan="2" align="left" nowrap="nowrap" class="formfield"><input name="unveridel" type="text" class="formfield" size="10" id="unveridel" value="<?=$row["unveridel"];?>" /> days</td>
        <td align="center"><a title="The number of days before an unverified account is deleted.  Set to 0 to never delete unverified members." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Delete Suspended Members:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="delsusp" type="text" class="formfield" id="delsusp" value="<?=$row["delsusp"];?>" size="20" /> 
          days</td>
        <td align="center" nowrap="nowrap"><a title="Suspended members who haven't logged in after this number of days will be deleted.  Enter 0 to disable auto delete." href="#"><img src="../images/question.jpg" alt="" width="15" height="15" border="0" /></a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Exclude Paid Suspended Members:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><? if($row["nodelpaid"] == 1) { ?>
        <input name="nodelpaid" type="checkbox" id="nodelpaid" value="1" checked="checked" />
          <? } else { ?>
        <input name="nodelpaid" type="checkbox" id="nodelpaid" value="1" />
          <? } ?></td>
        <td align="center" nowrap="nowrap"><a title="If checked, suspended members who have made a purchase at your site will never be auto deleted." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
      </tr>
      
            <tr>
        <td colspan="4" align="left" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Member Menu Configuration </font></strong></td>
        </tr>
      <tr>
        <td colspan="4" align="left">You can enable or disable each of the options on the member area menu by checking or unchecking the boxes below.</td>
      </tr>
        <tr>
          <td colspan="4" align="left" nowrap="nowrap"><table border="0" align="center" cellpadding="4">
            <tr>
              <td><strong>Home</strong></td>
              <td align="center"><input name="enablemenu_home" type="checkbox" id="enablemenu_home" value="1" <? if(get_setting("enablemenu_home") == "1") { ?> checked="checked"<? } ?>></td>
              <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td><strong>Downloads</strong></td>
              <td align="center"><input name="enablemenu_downloads" type="checkbox" id="enablemenu_downloads" value="1" <? if(get_setting("enablemenu_downloads") == "1") { ?> checked="checked"<? } ?>></td>
            </tr>
            <tr>
              <td><strong>Referrals</strong></td>
              <td align="center"><input name="enablemenu_ref" type="checkbox" id="enablemenu_ref" value="1" <? if(get_setting("enablemenu_ref") == "1") { ?> checked="checked"<? } ?>></td>
              <td>&nbsp;</td>
              <td><strong>Downline Builder</strong></td>
              <td align="center"><input name="enablemenu_dlb" type="checkbox" id="enablemenu_dlb" value="1" <? if(get_setting("enablemenu_dlb") == "1") { ?> checked="checked"<? } ?>></td>
            </tr>
            <tr>
              <td><strong>Profile</strong></td>
              <td align="center"><input name="enablemenu_profile" type="checkbox" id="enablemenu_profile" value="1" <? if(get_setting("enablemenu_profile") == "1") { ?> checked="checked"<? } ?>></td>
              <td>&nbsp;</td>
              <td><strong>Upgrade</strong></td>
              <td align="center"><input name="enablemenu_upgrade" type="checkbox" id="enablemenu_upgrade" value="1" <? if(get_setting("enablemenu_upgrade") == "1") { ?> checked="checked"<? } ?>></td>
            </tr>
            <tr>
              <td><strong>Affiliate Toolbox</strong></td>
              <td align="center"><input name="enablemenu_promote" type="checkbox" id="enablemenu_promote" value="1" <? if(get_setting("enablemenu_promote") == "1") { ?> checked="checked"<? } ?>></td>
              <td>&nbsp;</td>
              <td><strong>Forum</strong></td>
              <td align="center"><input name="enablemenu_forum" type="checkbox" id="enablemenu_forum" value="1" <? if(get_setting("enablemenu_forum") == "1") { ?> checked="checked"<? } ?>></td>
            </tr>
            <tr>
              <td><strong>Commissions</strong></td>
              <td align="center"><input name="enablemenu_makemoney" type="checkbox" id="enablemenu_makemoney" value="1" <? if(get_setting("enablemenu_makemoney") == "1") { ?> checked="checked"<? } ?>></td>
              <td>&nbsp;</td>
              <td><strong>Logout</strong></td>
              <td align="center"><input name="enablemenu_logout" type="checkbox" id="enablemenu_logout" value="1" <? if(get_setting("enablemenu_logout") == "1") { ?> checked="checked"<? } ?>></td>
            </tr>
          </table></td>
        <td align="center">&nbsp;</td>
        </tr>
        
        
      <tr>
        <td align="center" nowrap="nowrap" class="button">&nbsp;</td>
        <td colspan="2" align="left" valign="top" nowrap="nowrap">&nbsp;</td>
        <td align="center" valign="top" nowrap="nowrap">&nbsp;</td>
      </tr>

      <tr>
        <td colspan="4" align="left" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Member &amp; Affiliate Configuration </font></strong></td>
        </tr>
        <tr>
          <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Site URL: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="affurl" type="text" class="formfield" id="affurl" value="<?=$row["affurl"];?>" size="32" /></td>
        <td align="center"><a title="The URL that your affiliates will use to refer new members." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
        </tr>
      <tr>
        <td align="center" nowrap="nowrap" class="button">&nbsp;</td>
        <td colspan="2" align="left" valign="top" nowrap="nowrap"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">eg. 
          <?=$aff_path;?>
        </font></td>
        <td align="center" valign="top" nowrap="nowrap">&nbsp;</td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Use Theme On Sales Page: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
<? if($row["indextheme"] == 0) { ?>
        <input name="indextheme" type="checkbox" id="indextheme" value="1">
<? } else { ?>
        <input name="indextheme" type="checkbox" id="indextheme" value="1" checked="checked" />
<? } ?>        </td>
        <td align="center" nowrap="nowrap"><a title="If this box is checked then the header and footer of the selected theme will be automatically added to your sales page template." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Show Referral E-mail: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
		  <? if($row["showrefmail"] == 1) { ?>
		<input name="showrefmail" type="checkbox" id="showrefmail" value="1" checked="checked" />
		  <? } else { ?>
		<input name="showrefmail" type="checkbox" id="showrefmail" value="1" />
		  <? } ?></td>
        <td align="center"><a title="If checked, then members will be able to see the e-mail addresses of their referrals." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Require Email Verification: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
		  <? if($row["reqemailver"] == 1) { ?>
		<input name="reqemailver" type="checkbox" id="reqemailver" value="1" checked="checked" />
		  <? } else { ?>
		<input name="reqemailver" type="checkbox" id="reqemailver" value="1" />
		  <? } ?></td>
        <td align="center"><a title="If checked, then new members must click the verification link in their e-mail before they can login." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Require Email Re-Verification: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
		  <? if($row["reqrever"] == 1) { ?>
		<input name="reqrever" type="checkbox" id="reqrever" value="1" checked="checked" />
		  <? } else { ?>
		<input name="reqrever" type="checkbox" id="reqrever" value="1" />
		  <? } ?></td>
        <td align="center"><a title="If checked, then members must click a new verification link if they change their email." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Send Downline Notifications: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
		  <? if($row["notifydownline"] == 1) { ?>
		<input name="notifydownline" type="checkbox" id="notifydownline" value="1" checked="checked" />
		  <? } else { ?>
		<input name="notifydownline" type="checkbox" id="notifydownline" value="1" />
		  <? } ?></td>
        <td align="center" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">First Cookie Gets Sale: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
		  <? if($row["firstcookie"] == 1) { ?>
		<input name="firstcookie" type="checkbox" id="firstcookie" value="1" checked="checked" />
		  <? } else { ?>
		<input name="firstcookie" type="checkbox" id="firstcookie" value="1" />
		  <? } ?></td>
        <td align="center" nowrap="nowrap"><a title="If this box is checked and there is an LFM cookie in the browser, the cookie will OVERRIDE the referrer ID on the end of the affiliate URL. If the box is unchecked, then the referral ID on the end of the URL will override any cookies that were previously set." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Provide Banner URLs:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><? if($row["bannerurls"] == 1) { ?>
		<input name="bannerurls" type="checkbox" id="bannerurls" value="1" checked="checked" />
		  <? } else { ?>
		<input name="bannerurls" type="checkbox" id="bannerurls" value="1" />
		  <? } ?></td>
        <td align="center" nowrap="nowrap"><a title="Check this box to have URLs displayed with your banners and images in the members promo area. This will mean that they will use the images (and bandwidth) from your site." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Member Levels Flow Through: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><? if($row["flow"] == 1) { ?>
		<input name="flow" type="checkbox" id="flow" value="1" checked="checked" />
		  <? } else { ?>
		<input name="flow" type="checkbox" id="flow" value="1" />
		  <? } ?></td>
        <td align="center" nowrap="nowrap"><a title="If this box is checked, members on a higher level will automatically be given access to content pages for lower levels. For example, if you had Free, Silver and Gold levels the Silver level would have access to Free content pages and the Gold level would have access to Silver and Free content pages. See the notes in the Member Levels area about setting 'ranks'" href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Minimum Comm. Payment:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="min_comm" type="text" class="formfield" id="min_comm" value="<?=$row["min_comm"];?>" size="32" /> 
          e.g 10.00</td>
        <td align="center" nowrap="nowrap"><a title="This is the minimum commission required for affiliates to be paid when generating a PayPal payment file. Affiliates who have earned less than this amount will not be paid." href="#"><img src="../images/question.jpg" alt="" width="15" height="15" border="0" /></a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button">&nbsp;</td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="center" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" align="left" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Statistics</font></strong></td>
        </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Split Testing </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Split test 
          <select name="splittest" class="formfield" id="splittest">
            <option value="1"<? if($row["splittest"] == 1) { ?> selected="selected"<? } ?>>1</option>
            <option value="2"<? if($row["splittest"] == 2) { ?> selected="selected"<? } ?>>2</option>
            <option value="3"<? if($row["splittest"] == 3) { ?> selected="selected"<? } ?>>3</option>
          </select>
        sales page(s) </font></td>
        <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a title="You can split test two or three sales pages. If you set this option to '1', conversion stats will be displayed for your sales page." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /></a></font></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button">&nbsp;</td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
		<tr>
        <td colspan="4" align="left" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">File Library</font></strong></td>
        </tr>      
		<tr>
		  <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Enable File Protection:</font></strong> </td>
		  <td colspan="2" align="left" nowrap="nowrap">
		  <? if($row["fileprot"] == 1) { ?>
		  <input name="fileprot" type="checkbox" id="fileprot" value="1" checked="checked" />
		  <? } else { ?>
		  <input name="fileprot" type="checkbox" id="fileprot" value="1" />
		  <? } ?>		  </td>
		  <td align="center" nowrap="nowrap"><a title="Enable .htaccess file protection." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
		</tr>
		<tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">File Library Folder: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="filelibpath" type="text" id="filelibpath" size="20" value="<?=$row["filelibpath"];?>" /></td>
        <td align="center"><a title="This is the folder where additions to the 'File Library' will be stored." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
		</tr>
        <tr>
        <td align="left" nowrap="nowrap"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">e.g.: /myfiles1q33e/ </td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
        </tr>
      <tr>
        <td colspan="4" align="left"><strong><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif">Note: If you change the file library folder you must create the directory on your server first and set its permissions to 777. You should include leading and trailing slashes.</font></strong></td>
      </tr>
        
      
      <tr>
        <td colspan="4" align="left" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">phpBB Integration</font></strong></td>
        </tr>
      <tr>
        <td colspan="4" align="left" nowrap="nowrap" bgcolor="#D6E3FE">
<?		if($row["addtophpbb"] == 1)
		{ ?>
		<input name="addtophpbb" type="checkbox" checked="checked" id="addtophpbb" value="1" />
<?        }
		else
		{ ?>
		<input name="addtophpbb" type="checkbox" id="addtophpbb" value="1" />
<?		} 

		// Get the PHPBB settings
        $phpbbpath=$row["phpbb_path"];
        $phpbbconf=$phpbbpath."config.php";
		
		if(file_exists($phpbbconf))
		{
			include $phpbbconf;
	
			$phpbb_host=$dbhost;
			$phpbb_db=$dbname;
			$phpbb_user=$dbuser;
			$phpbb_pass=$dbpasswd;
			$phpbb_prefix=$table_prefix;
			$phpbb_group=$row["phpbb_group"];
			$phpbbver=$row["phpbbver"];
	
			$pconn=@lfmsql_connect($phpbb_host,$phpbb_user,$phpbb_pass);
			$pdb=@lfmsql_select_db($phpbb_db,$pconn);
	
			// Update phpBB
			if($_POST["Submit"] == "Sync phpBB <-> LFM" && (strlen(trim($phpbb_group)) > 2))
			{
				include "../inc/phpbbupdate.php";
			}
		}
?>
		  <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Add New Members To phpBB2</strong></font> </td>
        </tr>
      <tr>
        <td colspan="4" align="left">Note: You only need to add the phpBB path. The database details will be automatically inserted after you enter the path and update the settings. </td>
        </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">phpBB Version: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
		<input name="phpbbver" type="radio" value="2" <? if($row["phpbbver"] == 2) { ?> checked="checked"<? } ?> />v2.x&nbsp;
        <input name="phpbbver" type="radio" value="3" <? if($row["phpbbver"] == 3) { ?> checked="checked"<? } ?> />v3.x</td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">phpBB Path: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="phpbb_path" type="text" class="formfield" id="phpbb_path" value="<?=$row["phpbb_path"];?>" size="40" /></td>
        <td align="center"><a title="The full server path to where phpBB is installed." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" /><span></span></a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">(e.g. /home/myserver/public_html/forum/ ) </font></td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Database Host: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input disabled="disabled" name="phpbb_host" type="text" class="formfield" id="phpbb_host" value="<?=$phpbb_host;?>" /></td>
        <td align="center"><a title="The hostname of the lfmsql database that your phpBB2 forum uses." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Database Name: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input disabled="disabled" name="phpbb_db" type="text" class="formfield" id="phpbb_db" value="<?=$phpbb_db;?>" /></td>
        <td align="center"><a title="The name of the database that your phpBB2 forum uses." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Database User: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input disabled="disabled" name="phpbb_dbuser" type="text" class="formfield" id="phpbb_dbuser" value="<?=$phpbb_user;?>" /></td>
        <td align="center"><a title="The username for connection to your phpBB2 database." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><p><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Database Password: </strong></font></p>          </td>
        <td colspan="2" align="left" nowrap="nowrap"><input disabled="disabled" name="phpbb_dbpass" type="text" class="formfield" id="phpbb_dbpass" value="<?=$phpbb_pass;?>" /></td>
        <td align="center"><a title="The password for connection to your phpBB2 database." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Table Prefix: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input disabled="disabled" name="phpbb_prefix" type="text" class="formfield" id="phpbb_prefix" value="<?=$phpbb_prefix;?>" /></td>
        <td align="center"><a title="The table prefix used by your phpBB2 database." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Forum Group: </font></strong></td>
        <td align="left" nowrap="nowrap">
		<select name="phpbb_group" class="formfield" id="phpbb_group">
		<option value="" selected>Select A Group</option>
<?
		$phpbbqry="SELECT group_id,group_name FROM ".$phpbb_prefix."groups";
echo "SELECT group_id,group_name FROM ".$phpbb_prefix."groups";

		$pres=@lfmsql_query("SELECT group_id,group_name FROM ".$phpbb_prefix."groups",$pconn);
		while($prow=@lfmsql_fetch_array($pres))
		{
			if($row["phpbb_group"] == $prow["group_name"] && (strlen(trim($prow["group_name"])) > 1))
			{
?>
		<option value="<?=$prow["group_name"];?>" selected><?=$prow["group_name"];?></option>
<?
			}
			else if(strlen(trim($prow["group_name"])) > 1)
			{
?>
		<option value="<?=$prow["group_name"];?>"><?=$prow["group_name"];?></option>
<?
			}
}
?>        </select></td>
        <td align="right" nowrap="nowrap"><div style="width:100; text-align:right;">
		<strong><font color="#FF0000">Important-></font></strong></div></td>
        <td align="center" nowrap="nowrap"><a title="You must create a phpBB group for your members (using the phpBB admin area of phpBB) and select the group here." href="#">
		<img src="../images/question.jpg" width="15" height="15" border="0" /></a></td>
      </tr>
      <tr>
        <td colspan="4" align="center" nowrap="nowrap">
<?
		if($pres)
		{
?>
		<input name="Submit" type="submit" class="footer-text" id="Submit" value="Sync phpBB &lt;-&gt; LFM" /></td>
<?
		}
?>
        </tr>
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><font color="#FF0000"><strong>Note: Don't forget to 'Update General Settings' after selecting a group! </strong></font></td>
        </tr>
      <tr>
        <td colspan="4" align="left" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Autoresponder Settings </font></strong></td>
        </tr>
      <tr valign="middle">
        <td colspan="4" align="left" nowrap="nowrap" bgcolor="#D6E3FE" class="button">
        <input name="autoresp" type="radio" value="0" <? if($row["autoresponder"] == 0){ echo "checked=\"checked\""; } ?> /> None 
        <input name="autoresp" type="radio" value="3" <? if($row["autoresponder"] == 3){ echo "checked=\"checked\""; } ?> />
        Auto Response Plus
<input name="autoresp" type="radio" value="1" <? if($row["autoresponder"] == 1){ echo "checked=\"checked\""; } ?> /> AWeber 
<input name="autoresp" type="radio" value="4" <? if($row["autoresponder"] == 4){ echo "checked=\"checked\""; } ?> /> GVO 
<input name="autoresp" type="radio" value="5" <? if($row["autoresponder"] == 5){ echo "checked=\"checked\""; } ?> /> Multisponder Elite 
<input name="autoresp" type="radio" value="6" <? if($row["autoresponder"] == 6){ echo "checked=\"checked\""; } ?> /> TrafficWave 
             <input name="autoresp" type="radio" value="2" <? if($row["autoresponder"] == 2){ echo "checked=\"checked\""; } ?> /> Other </td>
        </tr>
      <tr>
        <td align="left" nowrap="nowrap" bgcolor="#D6E3FE" class="button style1">Generic (Other)  Settings </td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Autoresponder Email:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="auto_email" type="text" class="formfield" id="auto_email" value="<?=$row["auto_email"];?>" size="32" /></td>
        <td align="center"><a title="The email address of your auto-responder." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" bgcolor="#D6E3FE" class="button style1">Aweber Settings </td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Aweber Meta Web Form ID: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="a_meta_web_id" type="text" class="formfield" value="<?=$row["a_meta_web_id"];?>" id="a_meta_web_id" /></td>
        <td align="center"><a title="The AWeber meta_web_id field value from your AWeber subscription form." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Aweber Unit: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="a_unit" type="text" class="formfield" value="<?=$row["a_unit"];?>" id="a_unit" /></td>
        <td align="center"><a title="The AWeber unit field value from your AWeber subscription form." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Aweber Meta Req. Fields: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="a_req_fields" type="text" class="formfield" value="<?=$row["a_req_fields"];?>" id="a_req_fields" /></td>
        <td align="center"><a title="The AWeber req_fields value from your AWeber subscription form." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" bgcolor="#D6E3FE" class="button style1">GVO settings </td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">GVO affiliate name: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="gvo_affiliate_name" type="text" class="formfield" value="<?=$row["gvo_affiliate_name"];?>" id="gvo_affiliate_name" /></td>
        <td align="center"><a title="your GVO user name." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">GVO Campaign: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="gvo_campaign" type="text" class="formfield" value="<?=$row["gvo_campaign"];?>" id="gvo_campaign" /></td>
        <td align="center"><a title="The GVO campaign list you wish members to be added to." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">GVO Form ID: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="gvo_form_id" type="text" class="formfield" value="<?=$row["gvo_form_id"];?>" id="gvo_form_id" /></td>
        <td align="center"><a title="The ID of your GVO subscription form." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
        </a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" bgcolor="#D6E3FE" class="button style1">Auto Response Plus Settings </td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">ARP Subscription Email:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="arp_email" type="text" class="formfield" id="arp_email" value="<?=$row["arp_email"];?>" size="40" /></td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap">&nbsp;</td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" bgcolor="#D6E3FE" class="button style1">Multisponder Elite Settings </td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Form ID Number:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="m_spon_did" type="text" class="formfield" id="m_spon_did" value="<?=$row["m_spon_did"];?>" size="40" /></td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap">&nbsp;</td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" bgcolor="#D6E3FE" class="button style1">TrafficWave Settings </td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">TrafficWave Username:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="trwvid" type="text" class="formfield" id="trwvid" value="<?=$row["trwvid"];?>" size="40" /></td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Campaign Name:</font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="trwvseries" type="text" class="formfield" id="trwvseries" value="<?=$row["trwvseries"];?>" size="40" /></td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap">&nbsp;</td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left" nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" align="left" nowrap="nowrap" class="admintd"><strong><font face="Verdana, Arial, Helvetica, sans-serif">Misc Configuration </font></strong></td>
        </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Background Mail Despatch: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap">
<? if($row["bgemail"] == 0) { ?>
		<input name="bgemail" type="checkbox" id="bgemail" value="1">
<? } else { ?>
		<input name="bgemail" type="checkbox" id="bgemail" value="1" checked="checked" />
<? } ?>
          <font size="1" face="Verdana, Arial, Helvetica, sans-serif">(Read docs before turning this feature on!) </font></td>
        <td align="center"><a title="Enable background mail sending. Requires the web server to have command line access to PHP." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
      <tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">SMTP Server: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><input name="smtp_server" type="text" id="smtp_server" value="<?=$row["smtp_server"];?>" /></td>
        <td align="center"><a title="The location of your SMTP (outgoing) mail server." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>
<tr>
        <td align="left" nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">PHP Path: </font></strong></td>
        <td colspan="2" align="left" nowrap="nowrap"><?=$row["php_path"];?></td>
        <td align="center"><a title="This is the path that was found to your PHP binary." href="#"><img src="../images/question.jpg" width="15" height="15" border="0" />
		</a></td>
      </tr>	  
      <tr>
        <td align="left" nowrap="nowrap" class="button"><font size="2" face="Arial, Helvetica, sans-serif">&nbsp;</font></td>
        <td colspan="2" align="left" nowrap="nowrap">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="Update General Settings" /></td>
        </tr>
    </table>
	</form>
	</td>
  </tr>

</table>

<center><p><a target="_blank" href="view_phpinfo.php"><b>View Advanced Server/PHP Info</b></a></p></center>
