<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.32
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_SESSION["adminid"])) { exit; };

$currenttime = time();

$nowtime = $currenttime-120;
$hourtime = $currenttime-3600;
//$daytime = $currenttime-86400;
$daytime_from = strtotime(date('Y-m-d') . " 00:00:00");
$daytime_to = strtotime(date('Y-m-d') . " 23:59:59");
//$weektime = $currenttime-604800;
$weektime_from = firstDayOf('week')->format('U');
$weektime_to = lastDayOf('week')->format('U');

$currentdate = date("Y-m-d");
$yesterdate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
$weekdate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));

$surfingnow = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where lastclick >= ".$nowtime),0);
$surfinghour = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where lastclick >= ".$hourtime),0);
//$surfingtoday = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where clickstoday > 0"),0);
$surfingtoday = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where lastclick >= " . $daytime_from . " and lastclick <= " . $daytime_to),0);
//$surfingweek = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where lastclick >= ".$weektime),0);
$surfingweek = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where lastclick >= ".$weektime_from . " and lastclick <= " . $weektime_to ),0);

$sitesrotating = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."msites where state=1 and credits >= 1"),0);
$bannersrotating = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."mbanners where state=1 and imps >= 1"),0);
$textsrotating = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."mtexts where state=1 and imps >= 1"),0);

$numbermembers = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."members"), 0);
if ($numbermembers > 0) {
    $surfTodayFrom = date('Y-m-d') . ' 00:00:00';
    $surfTodayTo = date('Y-m-d') . ' 59:59:59';
//	$creditstoday = lfmsql_result(lfmsql_query("Select SUM(creditstoday) from ".$prefix."members"),0);
    $creditstoday = lfmsql_result(lfmsql_query("Select SUM(amount) surfing_ratio from ".$prefix."surf_amount_logs where created_at >= '$surfTodayFrom' and created_at <= '$surfTodayTo'"),0);
    
    $yesterday = new DateTime('Yesterday');
    $surfYesterdayFrom = $yesterday->format('Y-m-d') . ' 00:00:00';
    $surfYesterdayTo = $yesterday->format('Y-m-d') . ' 59:59:59';
//	$creditsyesterday = lfmsql_result(lfmsql_query("Select SUM(creditsyesterday) from ".$prefix."members"),0);
    $creditsyesterday = lfmsql_result(lfmsql_query("Select SUM(amount) surfing_ratio from ".$prefix."surf_amount_logs where created_at >= '$surfYesterdayFrom' and created_at <= '$surfYesterdayTo'"),0);
    
	$creditstoday = round($creditstoday, 1);
	$creditsyesterday = round($creditsyesterday, 1);
} else {
	$creditstoday = 0;
	$creditsyesterday = 0;
}

$numbersites = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."msites"), 0);
if ($numbersites > 0) {
	$hitstoday = lfmsql_result(lfmsql_query("Select SUM(hitstoday) from ".$prefix."msites"),0);
	$hitsyesterday = lfmsql_result(lfmsql_query("Select SUM(hitsyesterday) from ".$prefix."msites"),0);
	$persitetoday = lfmsql_result(lfmsql_query("Select hitstoday from ".$prefix."msites order by hitstoday desc limit 1"),0);
	$persiteyesterday = lfmsql_result(lfmsql_query("Select hitsyesterday from ".$prefix."msites order by hitsyesterday desc limit 1"),0);
} else {
	$hitstoday = 0;
	$hitsyesterday = 0;
	$persitetoday = 0;
	$persiteyesterday = 0;
}

$numberbanners = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."mbanners"), 0);
if ($numberbanners > 0) {
	$perbantoday = lfmsql_result(lfmsql_query("Select hitstoday from ".$prefix."mbanners order by hitstoday desc limit 1"),0);
	$perbanyesterday = lfmsql_result(lfmsql_query("Select hitsyesterday from ".$prefix."mbanners order by hitsyesterday desc limit 1"),0);
} else {
	$perbantoday = 0;
	$perbanyesterday = 0;
}

$numbertexts = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."mtexts"), 0);
if ($numbertexts > 0) {
	$pertexttoday = lfmsql_result(lfmsql_query("Select hitstoday from ".$prefix."mtexts order by hitstoday desc limit 1"),0);
	$pertextyesterday = lfmsql_result(lfmsql_query("Select hitsyesterday from ".$prefix."mtexts order by hitsyesterday desc limit 1"),0);
} else {
	$pertexttoday = 0;
	$pertextyesterday = 0;
}

$abusereports = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."reports where action=0"),0);

$newmemstoday = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where joindate <= '".$currentdate." 23:59:59' and joindate >= '".$currentdate."'"), 0);
$newmemsyesterday = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where joindate <= '".$yesterdate." 23:59:59' and joindate >= '".$yesterdate."'"), 0);
$newmemsweek = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where joindate <= '".$currentdate." 23:59:59' and joindate >= '".$weekdate."'"), 0);

?>

<div align="center">

<!-- Start TE Dashboard -->
<div class="lfm_infobox" style="width: 400px;">
<table width="400" border="0" cellpadding="1">
  <tr>
    <td colspan="2" align="center"><div class="lfm_infobox_heading">TE Dashboard</div><br></td>
  </tr>
  
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Surfing Now (<a target=_self href=admin.php?f=mm&sf=browse&show=surfing>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$surfingnow;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Surfed This Hour (<a target=_self href=admin.php?f=mm&sf=browse&show=surfhour>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$surfinghour;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Surfed Today (<a target=_self href=admin.php?f=mm&sf=browse&show=surftoday>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$surfingtoday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Surfed This Week (<a target=_self href=admin.php?f=mm&sf=browse&show=surfweek>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$surfingweek;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_sectbreak"><hr></td></tr>
    
    
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Open Abuse Reports (<a target=_self href=admin.php?f=areports&show=&searchfield=action&searchtext=0&sf=browse>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$abusereports;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
    
    
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Sites In Rotation (<a target=_self href=admin.php?f=smm&sf=browse&show=rotsites>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$sitesrotating;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Banners In Rotation (<a target=_self href=admin.php?f=bmm&sf=browse&show=rotbanners>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$bannersrotating;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Text Ads In Rotation (<a target=_self href=admin.php?f=tmm&sf=browse&show=rottexts>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$textsrotating;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_sectbreak"><hr></td></tr>
    
    
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Signups Today (<a target=_self href=admin.php?f=mm&sf=browse&show=newtoday>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$newmemstoday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Signups Yesterday (<a target=_self href=admin.php?f=mm&sf=browse&show=newyester>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$newmemsyesterday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Signups This Week (<a target=_self href=admin.php?f=mm&sf=browse&show=newweek>View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$newmemsweek;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_sectbreak"><hr></td></tr>
    
    
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Credits Earned Today: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$creditstoday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Hits Delivered Today: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$hitstoday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Hits Delivered Per URL: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$persitetoday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Imps Delivered Per Banner: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$perbantoday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Imps Delivered Per Text Ad: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$pertexttoday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_sectbreak"><hr></td></tr>   
    
    
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Credits Earned Yesterday: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$creditsyesterday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
    
    
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Hits Delivered Yesterday: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$hitsyesterday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Hits Delivered Per URL: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$persiteyesterday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Imps Delivered Per Banner: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$perbanyesterday;?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
      
    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Imps Delivered Per Text Ad: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$pertextyesterday;?></td>
      </tr>
      
      
      <tr><td colspan="2"><br></td></tr>

    
    <tr>
      <td colspan="2" align="center"><a target=_self href=admin.php?f=surfstats><input type="button" value="View More Stats"></a></td>
    </tr>
    
</table>
</div>
<!-- End TE Dashboard -->