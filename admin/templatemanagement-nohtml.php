<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";



// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

// Update affiliate news
if($_POST["Submit"] == "Update News")
{
	if(isset($_POST["news"]))
	{
		// Update default or member level news
		if($_POST["ntpl"] == "0")
		{
			@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["news"]."' WHERE template_name='News'");
		}
		else
		{
			@mysql_query("UPDATE ".$prefix."membertypes SET template_data='".$_POST["news"]."' WHERE mtid=".$_POST["ntpl"]);
		}
	}
}

// Update upgrades page
if($_POST["Submit"] == "Update Upgrades")
{
	if(isset($_POST["upgrades"]))
	{
		// Update default or member level news
		if($_POST["utpl"] == "0")
		{
			@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["upgrades"]."' WHERE template_name='Upgrades'");
			if(mysql_affected_rows() < 1)
			{
				@mysql_query("INSERT INTO ".$prefix."templates(template_name,template_data) VALUES('Upgrades','".$_POST["upgrades"]."')");
			}
		}
		else
		{
			@mysql_query("UPDATE ".$prefix."membertypes SET utemplate_data='".$_POST["upgrades"]."' WHERE mtid=".$_POST["utpl"]);
		}
	}
}

// Update the site header
if($_POST["Submit"] == "Update Site Header")
{
	if(isset($_POST["siteheader"]))
	{
		@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["siteheader"]."' WHERE template_name='Site Header'");
	}
}

// Update the site footer
if($_POST["Submit"] == "Update Site Footer")
{
	if(isset($_POST["sitefooter"]))
	{
		@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["sitefooter"]."' WHERE template_name='Site Footer'");
	}
}

// Update the homepage
if($_POST["Submit"] == "Update Login Page")
{
	if(isset($_POST["homepage"]))
	{
		@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["homepage"]."' WHERE template_name='Homepage Body'");
	}
}

// Update the suspended members page
if($_POST["Submit"] == "Update Suspended Page")
{
	if(isset($_POST["suspended"]))
	{
		@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["suspended"]."' WHERE template_name='Suspended'");
	}
}
// Update the Sales Page
if($_POST["Submit"] == "Update Sales Page")
{
	if(isset($_POST["salespage"]))
	{
		@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["salespage"]."' WHERE template_name='Sales Page'");
	}
}

// Update the signup header
if($_POST["Submit"] == "Update Signup Header")
{
	if(isset($_POST["signup"]))
	{
		@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["signup"]."' WHERE template_name='Signup'");
	}
}

// Update the welcome email
if($_POST["Submit"] == "Update Email")
{
	if(isset($_POST["welcomeemail"]))
	{
		@mysql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["welcomeemail"]."' WHERE template_name='Welcome Email'");
	}
}

?>
<form name="hphform" method="post" action="admin.php?f=tm">
<p>&nbsp;</p>
<table border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Member/Affiliate Main Page- news and updates shown in your members area </font></strong></td>
  </tr>
  <tr>
    <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Membership Level:</font></strong> 
	  <select name="ntpl" onChange="javascript:window.document.hphform.submit();">
	    <option value="0">Default</option>
<?
	// Get the list of member account types
	$mtres=@mysql_query("SELECT * FROM ".$prefix."membertypes ORDER BY acclevel");
	while($mtrow=@mysql_fetch_array($mtres))
	{
		if($_POST["ntpl"] == $mtrow["mtid"])
		{
?>
		<option value="<?=$mtrow["mtid"];?>" selected><?=$mtrow["accname"];?></option>
<?
		}
		else
		{
?>
		<option value="<?=$mtrow["mtid"];?>"><?=$mtrow["accname"];?></option>
<?
		}
}
?>		
	    </select>
	   <input name="Button" type="button" id="Button" OnClick="javascript:previewPage('news');" value="Preview" />
    <input type="submit" name="Submit" value="Update News" /></td>
  </tr>
  <tr>
    <td>
<?
	// Get template data from database
	if(!isset($_POST["ntpl"]) || $_POST["ntpl"] == "0")
	{
		$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='News'");
	}
	else
	{
		$hres=@mysql_query("SELECT template_data FROM ".$prefix."membertypes WHERE mtid=".$_POST["ntpl"]);
	}
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="news" cols="90" rows="12" id="news"><?=$hrow["template_data"];?></textarea>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Site Header - main header for login page and member area </font></strong></td>
  </tr>
  <tr>
    <td>
	<input name="Button" type="button" id="Button" OnClick="javascript:previewPage('siteheader');" value="Preview" />
    <input type="submit" name="Submit" value="Update Site Header" /></td>
  </tr>
  <tr>
    <td>
<?
	// Get template data from database
	$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Site Header'");
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="siteheader" cols="90" rows="12"><?=$hrow["template_data"];?></textarea>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Site Footer - footer for login page and member area </font></strong></td>
  </tr>
  <tr>
    <td>
	<input name="Button" type="button" id="Submit" OnClick="javascript:previewPage('sitefooter');" value="Preview" />
    <input name="Submit" type="submit" id="Submit" value="Update Site Footer" /></td>
  </tr>
  <tr>
    <td>
<?
	// Get template data from database
	$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Site Footer'");
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="sitefooter" cols="90" rows="12"><?=$hrow["template_data"];?></textarea>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Member  Login Page (login.php) </font></strong></td>
  </tr>
  <tr>
    <td><input name="Button" type="button" id="Button" OnClick="javascript:previewPage('homepage');" value="Preview" />
      <input name="Submit" type="submit" id="Submit" value="Update Login Page" /></td>
  </tr>
  <tr>
    <td>
<?
	// Get template data from database
	$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Homepage Body'");
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="homepage" cols="90" rows="24"><?=$hrow["template_data"];?></textarea>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Suspended Page - Displayed to suspended members</font></strong></td>
  </tr>
  <tr>
    <td><input name="Button" type="button" id="Button" OnClick="javascript:previewPage('suspended');" value="Preview" />
      <input name="Submit" type="submit" id="Submit" value="Update Suspended Page" /></td>
  </tr>
  <tr>
    <td>
	<?
	// Get template data from database
	$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Suspended'");
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="suspended" cols="90" rows="12"><?=$hrow["template_data"];?></textarea>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Sales Page - Your main sales page (index.php) </font></strong></td>
  </tr>
  <tr>
    <td><input name="Button" type="button" id="Button" OnClick="javascript:previewPage('salespage');" value="Preview" />
      <input name="Submit" type="submit" id="Submit" value="Update Sales Page" /></td>
  </tr>
  <tr>
    <td><?
	// Get template data from database
	$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Sales Page'");
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="salespage" cols="90" rows="12" id="salespage"><?=$hrow["template_data"];?></textarea></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
<td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Affiliate Upgrades Page- upgrade options shown in your members area </font></strong></td>
  </tr>
  <tr>
    <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Membership Level:</font></strong> 
	  <select name="utpl" id="utpl" onChange="javascript:window.document.hphform.submit();">
<?
	// Get the list of member account types
	$mtres=@mysql_query("SELECT * FROM ".$prefix."membertypes ORDER BY acclevel");
	while($mtrow=@mysql_fetch_array($mtres))
	{
		if($_POST["utpl"] == $mtrow["mtid"])
		{
?>
		<option value="<?=$mtrow["mtid"];?>" selected><?=$mtrow["accname"];?></option>
<?
		}
		else
		{
?>
		<option value="<?=$mtrow["mtid"];?>"><?=$mtrow["accname"];?></option>
<?
		}
}
?>		
	    </select>
	<input name="Button" type="button" id="Button" OnClick="javascript:previewPage('upgrades');" value="Preview" />
    <input type="submit" name="Submit" value="Update Upgrades" /></td>
  </tr>
  <tr>
    <td>
<?
	// Get template data from database
	if(!isset($_POST["utpl"]) || $_POST["utpl"] == "0")
	{
		$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Upgrades'");
	}
	else
	{
		$hres=@mysql_query("SELECT utemplate_data FROM ".$prefix."membertypes WHERE mtid=".$_POST["utpl"]);
	}
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="upgrades" cols="90" rows="12" id="upgrades"><?=$hrow["utemplate_data"];?></textarea>	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>  </tr>
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Signup Form Header - Information to show above the signup form </font></strong></td>
  </tr>
  <tr>
    <td><input name="Button" type="button" id="Button" OnClick="javascript:previewPage('signup');" value="Preview" />
	<input name="Submit" type="submit" id="Submit" value="Update Signup Header" /></td>
  </tr>
  <tr>
    <td><?
	// Get template data from database
	$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Signup'");
	$hrow=@mysql_fetch_array($hres);
?>
	<textarea name="signup" cols="90" rows="12" id="signup"><?=$hrow["template_data"];?></textarea>
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Welcome Email - Email automatically sent to all new members </font></strong></td>
  </tr>
  <tr>
    <td><input name="Submit" type="submit" id="Submit" value="Update Email" /></td>
  </tr>
  <tr>
    <td>	  <table width="100%" border="0">
        <tr>
          <td><?
	// Get template data from database
	$hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Welcome Email'");
	$hrow=@mysql_fetch_array($hres);
?>
	  <textarea name="welcomeemail" cols="50" rows="10"><?=$hrow["template_data"];?></textarea></td>
          <td><p><font size="2" face="Arial, Helvetica, sans-serif">The following tag substitutions may be used: </font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">#FIRSTNAME# - Member first name <br />
              #LASTNAME# - Member last name <br />
              #USERNAME# - Member login username <br />
              #PASSWORD# - Member password<br />
              #AFFILIATEID# - The members affiliate ID <br />
              #SITENAME# - The name of this site<br />
            #REFURL# - The referral link<br />
            #PROMOCODE# - Add a promo code entry field<br />
            #PROMOCODE_2# - Add a formatted promo <br />
            code entry field (see Help)
</font></p></td>
        </tr>
      </table>
	  <p>&nbsp;</p></td>
    </tr>
</table>
</form>
