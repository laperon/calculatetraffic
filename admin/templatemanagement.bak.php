<?php
// ////////////////////////////////////////////////////////////
// LFM, LFMTE, and LFMVM Templates Manager with CK Editor integration
// Copyright �2006-2014 LFM Wealth Systems. All Rights Reserved.
// ////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

require "templates_defaults.php";

// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };


// Number of previous template versions to backup
$keep_prev = 15;


// Save Templates

if($_POST["Submit"] == "Update Upgrades") {
	if(isset($_POST["upgrades"]))
	{
		// Update default or member level news
		if($_POST["utpl"] == "0")
		{
			$getaccid = lfmsql_query("SELECT mtid FROM ".$prefix."membertypes ORDER BY mtid DESC LIMIT 1");
			if (lfmsql_num_rows($getaccid) > 0) {
				$_POST["utpl"] = lfmsql_result($getaccid, 0, "mtid");
				@lfmsql_query("UPDATE ".$prefix."membertypes SET utemplate_data='".$_POST["upgrades"]."' WHERE mtid=".$_POST["utpl"]) or die(lfmsql_error());
			}
		}
		else
		{
			@lfmsql_query("UPDATE ".$prefix."membertypes SET utemplate_data='".$_POST["upgrades"]."' WHERE mtid=".$_POST["utpl"]) or die(lfmsql_error());
		}
	}
	
} elseif($_POST["Submit"] == "Apply To All Memberships") {

	// Update All Member Type Templates
	if(isset($_POST["news"]))
	{
		$getaccounts = lfmsql_query("Select mtid from `".$prefix."membertypes` order by mtid asc");
		for ($i = 0; $i < lfmsql_num_rows($getaccounts); $i++) {
			$accid = lfmsql_result($getaccounts, $i, "mtid");
			
			$getoldtemplate = lfmsql_query("SELECT template_data from ".$prefix."membertypes WHERE mtid=".$accid) or die(lfmsql_error());
			if (lfmsql_num_rows($getoldtemplate) > 0) {
				$oldtemplate = addslashes(lfmsql_result($getoldtemplate, 0, "template_data"));
				
				// Make sure the old template is already saved
				$backupexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."memtemplates_backups where mtid='".$accid."' and template_data='".$oldtemplate."'"), 0);
				if ($backupexists < 1 && (!isset($default_templates['Members Area']) || ($oldtemplate != $default_templates['Members Area']))) {
					// We don't know when this template was saved, so set 1 day ago
					$tempsavetime = date("Y-m-d H:i:s", time()-86400);
					lfmsql_query("INSERT INTO ".$prefix."memtemplates_backups (savetime, mtid, template_data) VALUES ('".$tempsavetime."', '".$accid."', '".$oldtemplate."')") or die(lfmsql_error());
				}
				
				// Save new template backup
				lfmsql_query("INSERT INTO ".$prefix."memtemplates_backups (savetime, mtid, template_data) VALUES (NOW(), '".$accid."', '".$_POST["news"]."')") or die(lfmsql_error());
				
				// Check and remove extra previous versions
				$countprev = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."memtemplates_backups where mtid='".$accid."'"), 0);
				if ($countprev > $keep_prev) {
					$num_to_del = $countprev - $keep_prev;
					lfmsql_query("DELETE FROM ".$prefix."memtemplates_backups WHERE mtid='".$accid."' ORDER BY savetime ASC LIMIT ".$num_to_del) or die(lfmsql_error());
				}
			}
			
		}
		
		// Save new template
		@lfmsql_query("UPDATE ".$prefix."membertypes SET template_data='".$_POST["news"]."' WHERE mtid>=0") or die(lfmsql_error());
	}
	
} elseif($_POST["Submit"] == "Update") {

	// Update Member Type Template
	if(isset($_POST["news"]))
	{
		$getoldtemplate = lfmsql_query("SELECT template_data from ".$prefix."membertypes WHERE mtid=".$_POST["ntpl"]) or die(lfmsql_error());
		if (lfmsql_num_rows($getoldtemplate) > 0) {
			$oldtemplate = addslashes(lfmsql_result($getoldtemplate, 0, "template_data"));
			
			// Make sure the old template is already saved
			$backupexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."memtemplates_backups where mtid='".$_POST["ntpl"]."' and template_data='".$oldtemplate."'"), 0);
			if ($backupexists < 1 && (!isset($default_templates['Members Area']) || ($oldtemplate != $default_templates['Members Area']))) {
				// We don't know when this template was saved, so set 1 day ago
				$tempsavetime = date("Y-m-d H:i:s", time()-86400);
				lfmsql_query("INSERT INTO ".$prefix."memtemplates_backups (savetime, mtid, template_data) VALUES ('".$tempsavetime."', '".$_POST["ntpl"]."', '".$oldtemplate."')") or die(lfmsql_error());
			}
			
			// Save new template backup
			lfmsql_query("INSERT INTO ".$prefix."memtemplates_backups (savetime, mtid, template_data) VALUES (NOW(), '".$_POST["ntpl"]."', '".$_POST["news"]."')") or die(lfmsql_error());
			
			// Save new template
			@lfmsql_query("UPDATE ".$prefix."membertypes SET template_data='".$_POST["news"]."' WHERE mtid=".$_POST["ntpl"]) or die(lfmsql_error());
			
			// Check and remove extra previous versions
			$countprev = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."memtemplates_backups where mtid='".$_POST["ntpl"]."'"), 0);
			if ($countprev > $keep_prev) {
				$num_to_del = $countprev - $keep_prev;
				lfmsql_query("DELETE FROM ".$prefix."memtemplates_backups WHERE mtid='".$_POST["ntpl"]."' ORDER BY savetime ASC LIMIT ".$num_to_del) or die(lfmsql_error());
			}
			
		}
	}
	
} elseif($_POST["Submit"] == "Update Email") {

	$get_templates = lfmsql_query("SELECT template_id, template_name FROM ".$prefix."templates WHERE template_type='mail'");
	for ($i = 0; $i < lfmsql_num_rows($get_templates); $i++) {
		$template_id = lfmsql_result($get_templates, $i, "template_id");
		$template_name = lfmsql_result($get_templates, $i, "template_name");
		if (isset($_POST['template'.$template_id]) && $_POST['template'.$template_id] != "") {
			@lfmsql_query("UPDATE ".$prefix."templates SET template_data='".$_POST['template'.$template_id]."' WHERE template_name='".$template_name."'") or die(lfmsql_error());
		}
	}
	
} elseif (isset($_POST["Submit"]) && $_POST["Submit"] != "") {

	// Update page template
	
	$getoldtemplate = lfmsql_query("SELECT template_data from ".$prefix."templates WHERE template_name='".$_POST["template_name"]."'") or die(lfmsql_error());
	if (lfmsql_num_rows($getoldtemplate) > 0) {
		$template_name = $_POST["template_name"];
		$oldtemplate = addslashes(lfmsql_result($getoldtemplate, 0, "template_data"));
		
		// Make sure the old template is already saved
		$backupexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."templates_backups where template_name='".$_POST["template_name"]."' and template_data='".$oldtemplate."'"), 0);
		if ($backupexists < 1 && (!isset($default_templates[$template_name]) || ($oldtemplate != $default_templates[$template_name]))) {
			// We don't know when this template was saved, so set 1 day ago
			$tempsavetime = date("Y-m-d H:i:s", time()-86400);
			lfmsql_query("INSERT INTO ".$prefix."templates_backups (savetime, template_name, template_data) VALUES ('".$tempsavetime."', '".$_POST["template_name"]."', '".$oldtemplate."')") or die(lfmsql_error());
		}
		
		// Save new template backup
		lfmsql_query("INSERT INTO ".$prefix."templates_backups (savetime, template_name, template_data) VALUES (NOW(), '".$_POST["template_name"]."', '".$_POST["template_data"]."')") or die(lfmsql_error());
		
		// Save new template
		lfmsql_query("UPDATE ".$prefix."templates SET template_data='".$_POST["template_data"]."' WHERE template_name='".$_POST["template_name"]."'") or die(lfmsql_error());
		
		// Check and remove extra previous versions
		$countprev = lfmsql_result(lfmsql_query("SELECT COUNT(*) from ".$prefix."templates_backups where template_name='".$_POST["template_name"]."'"), 0);
		if ($countprev > $keep_prev) {
			$num_to_del = $countprev - $keep_prev;
			lfmsql_query("DELETE FROM ".$prefix."templates_backups WHERE template_name='".$_POST["template_name"]."' ORDER BY savetime ASC LIMIT ".$num_to_del) or die(lfmsql_error());
		}
				
	}
}
// End Save Templates




if(isset($_POST["editpage"]))
{
	$editpage = trim($_POST["editpage"]);
}
else
{
	$editpage = "siteindex";
}

// Get/Set Editors Settings

$enablewysiwyg = lfmsql_result(lfmsql_query("Select enablewysiwyg from `".$prefix."settings`"), 0);
if (isset($_GET['seteditors']) && ($_GET['seteditors'] == 0 || $_GET['seteditors'] == 1)) {
	$enablewysiwyg = $_GET['seteditors'];
	@lfmsql_query("Update `".$prefix."settings` set enablewysiwyg=".$enablewysiwyg);
}
//End Get/Set Editors Settings

?>
<? if($enablewysiwyg == "1") { ?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<center><p><font size="4"><a href="admin.php?f=tm&seteditors=0">Click To Hide The HTML Editor</a></font></p></center>
<? } else { ?>
<center><p><font size="4"><a href="admin.php?f=tm&seteditors=1">Click To Show The HTML Editor</a></font></p></center>
<? } ?>
<form name="hphform" method="post" action="admin.php?f=tm">
<p>&nbsp;</p>
<table width="760" border="0" align="center" cellpadding="4" cellspacing="0">
  <tr>
    <td><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Edit Page:</font></strong>
    <select name="editpage" id="editpage" onChange="javascript:document.forms.hphform.submit();">
    
    <option value="memberarea" <? if($editpage=="memberarea") { echo "selected=selected"; } ?>>Member Area</option>
    
    
    <?php
    	// List all the page templates as choices
    	$get_templates = lfmsql_query("SELECT template_title FROM ".$prefix."templates WHERE template_type='page' ORDER BY template_title ASC");
    	for ($i = 0; $i < lfmsql_num_rows($get_templates); $i++) {
    		$template_title = lfmsql_result($get_templates, $i, "template_title");
    		echo("<option value=\"".$template_title."\""); if($editpage==$template_title) { echo "selected=selected"; } echo(">".$template_title."</option>");
    	}
    ?>
    
    <?php
    if (!file_exists('../upgrade.php')) {
    ?>
    <option value="upgrades" <? if($editpage=="upgrades") { echo "selected=selected"; } ?>>Upgrades Page</option>
    <?
    }
    ?>
    
    </select>    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
<?

$checktemplate = lfmsql_query("SELECT * FROM ".$prefix."templates where template_title='".$editpage."'") or die(lfmsql_error());
if (lfmsql_num_rows($checktemplate) > 0 && $editpage != "memberarea" && $editpage != "upgrades") {
	$template_name = lfmsql_result($checktemplate, 0, "template_name");
	$template_title = lfmsql_result($checktemplate, 0, "template_title");
	$template_data = lfmsql_result($checktemplate, 0, "template_data");

?>

  <tr>
    <td bgcolor="#9DBDFF" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif"><?=$template_title;?></font></strong></td>
  </tr>
  <tr>
    <td>
    <input name="Submit" type="submit" id="Submit" value="Update <?=$template_title;?>" />&nbsp; &nbsp;<span style="cursor:pointer; color:blue; size:10px;" onClick="window.open('templaterestore.php?template_name=<? echo(urlencode($template_name)); ?>','restore_previous','width=500, height=400');"><b>Restore Previous Version</b></span>
    <input type="hidden" name="template_name" value="<?=$template_name;?>" />
    </td>
  </tr>
  <tr>
    <td>
<?
    if (function_exists('html_entity_decode'))
    {
	    $html=html_entity_decode($template_data);
	}
    else
    {
        $html=unhtmlentities($template_data);
    }
?>  <textarea class="ckeditor" name="template_data" cols="80" rows="8" id="template_data"><?=$html;?></textarea>
<br />
	<a href="help.php#affmain" target="_blank">Click Here For Help And Tag Substitutions That Can Be Used</a>
	</td>
  </tr>

<?
} elseif($editpage == "upgrades") { ?>
  <tr>
<td bgcolor="#9DBDFF" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Affiliate Upgrades Page- upgrade options shown in your members area </font></strong></td>
  </tr>
  <tr>
    <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Membership Level:</font></strong>
	  <select name="utpl" id="utpl" onChange="javascript:window.document.hphform.submit();">
<?
	// Get the list of member account types
	$mtres=@lfmsql_query("SELECT * FROM ".$prefix."membertypes ORDER BY acclevel");
	while($mtrow=@lfmsql_fetch_array($mtres))
	{
		if($_POST["utpl"] == $mtrow["mtid"])
		{
?>
		<option value="<?=$mtrow["mtid"];?>" selected><?=$mtrow["accname"];?></option>
<?
		}
		else
		{
?>
		<option value="<?=$mtrow["mtid"];?>"><?=$mtrow["accname"];?></option>
<?
		}
}
?>
	    </select>
	  <input type="submit" name="Submit" value="Update Upgrades" />
	  </td>
  </tr>
  <tr>
    <td>
<?
	// Get template data from database
	if(!isset($_POST["utpl"]) || $_POST["utpl"] == "0")
	{
		$hres=@lfmsql_query("SELECT utemplate_data FROM ".$prefix."membertypes ORDER BY mtid DESC LIMIT 1");
	}
	else
	{
		$hres=@lfmsql_query("SELECT utemplate_data FROM ".$prefix."membertypes WHERE mtid=".$_POST["utpl"]);
	}
	$hrow=@lfmsql_fetch_array($hres);
	// Decode template data
    if (function_exists('html_entity_decode'))
    {
	    $html=html_entity_decode($hrow["utemplate_data"]);
	}
    else
    {
        $html=unhtmlentities($hrow["utemplate_data"]);
    }
?>  <textarea class="ckeditor" name="upgrades" cols="80" rows="8" id="upgrades"><?=$html;?></textarea>
<br />

	</td>
  </tr>
<?

} else { ?>
  <tr bgcolor="#9DBDFF">
    <td class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Member Area - news and updates shown in your member/affiliate area </font></strong></td>
  </tr>
  <tr>
    <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Membership Level:</font></strong>
	  <select name="ntpl" onChange="javascript:window.document.hphform.submit();">
<?

if(!isset($_POST["ntpl"]) || $_POST["ntpl"] == "0") {
	$_POST["ntpl"] = "1";
}

	// Get the list of member account types
	$mtres=@lfmsql_query("SELECT * FROM ".$prefix."membertypes ORDER BY acclevel");
	while($mtrow=@lfmsql_fetch_array($mtres))
	{
		if($_POST["ntpl"] == $mtrow["mtid"])
		{
?>
		<option value="<?=$mtrow["mtid"];?>" selected><?=$mtrow["accname"];?></option>
<?
		}
		else
		{
?>
		<option value="<?=$mtrow["mtid"];?>"><?=$mtrow["accname"];?></option>
<?
		}
}
?>
	    </select>
	  <input type="submit" name="Submit" value="Update" />
	  <input type="submit" name="Submit" value="Apply To All Memberships" />
	  &nbsp; &nbsp;<span style="cursor:pointer; color:blue; size:10px;" onClick="window.open('templatememrestore.php?mtid=<? echo($_POST["ntpl"]); ?>','restore_membership','width=500, height=400');"><b>Restore Previous Version</b></span>
	  </td>
  </tr>
  <tr>
    <td>
<?
	// Get template data from database

	$hres=@lfmsql_query("SELECT template_data FROM ".$prefix."membertypes WHERE mtid=".$_POST["ntpl"]);

	$hrow=@lfmsql_fetch_array($hres);
	// Decode template data
    if (function_exists('html_entity_decode'))
    {
	    $html=html_entity_decode($hrow["template_data"]);
	}
    else
    {
        $html=unhtmlentities($hrow["template_data"]);
    }

?>  <textarea class="ckeditor" name="news" cols="80" rows="8" id="news"><?=$html;?></textarea>
<br />
	<a href="help.php#affmain" target="_blank">Click Here For Help And Tag Substitutions That Can Be Used</a>
	</td>  </td>
  </tr>
<? }


?>
</form>

<form name="mailsform" method="post" action="admin.php?f=tm">

  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td bgcolor="#9DBDFF" class="admintd"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif">Email Messages </font></strong></td>
  </tr>
  <tr>
    <td><input name="Submit" type="submit" id="Submit" value="Update Email" /></td>
  </tr>
  <tr>
    <td>
    
    
    
    
<?

$get_templates = lfmsql_query("SELECT * FROM ".$prefix."templates WHERE template_type='mail' ORDER BY template_title ASC");
for ($i = 0; $i < lfmsql_num_rows($get_templates); $i++) {

		$template_id = lfmsql_result($get_templates, $i, "template_id");
		$template_name = lfmsql_result($get_templates, $i, "template_name");
    		$template_title = lfmsql_result($get_templates, $i, "template_title");
		$template_data = lfmsql_result($get_templates, $i, "template_data");

?>    
    
<table width="100%" border="0">
        <tr>
          <td colspan="2"><strong><font size="3" face="Verdana, Arial, Helvetica, sans-serif"><?=$template_title;?></font></strong></td>
          </tr>
        <tr>
          <td>
          
	  <textarea name="template<?=$template_id;?>" cols="50" rows="10"><?=$template_data;?></textarea></td>
          <td><p><font size="2" face="Arial, Helvetica, sans-serif">The following tag substitutions may be used: </font></p>
            <p><font size="2" face="Arial, Helvetica, sans-serif">#FIRSTNAME# - Member first name <br />
              #LASTNAME# - Member last name <br />
              #USERNAME# - Member login username <br />
              #AFFILIATEID# - The members affiliate ID <br />
              #SITENAME# - The name of this site<br />
            #REFURL# - The referral link<br />
            #SPONSORNAME# - The member's upline name <br />
            #SPONSORID# - The member's upline ID <br />
            #VERIFY# - The email address verification link </font><font size="2" face="Arial, Helvetica, sans-serif"><br />
            </font></p>
            
            </td>
        </tr>
      </table>
      

<?

}    

?>
      
	  <p>&nbsp;</p></td>
    </tr>
</table>
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  <tr>
    <td bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
        <ul>
          <li><font size="2" face="Verdana, Arial, Helvetica, sans-serif">The templates in this section are the main templates for your site. These include the site header and footer, mail index page and a template for each member level that you create.</font></li>
        </ul>
        <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</form>
