<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

require_once "../inc/theme.php";

if ( isset($_GET['action']) ) {

	if ('activate' == $_GET['action']) {
		switch_theme($_GET['template']);
		echo "<script language=\"javascript\">";
		echo "window.location.href=\"admin.php?f=th&activated=true\";";
		echo "</script>";
		exit;
	}
}

?>
<style>
.wrap {
	width: 760px;
	background-color: #FFFFFF;
   margin-left: auto ;
   margin-right: auto ;
}

.wrap h2 {
	margin: 20px 10px 0 0 ;
	padding-right: 0px;
	padding-left: 0px;
}

#current-theme {
	margin-top: 1em;
}

#current-theme a {
	border-bottom: none;
}

#current-theme h3 {
	font-size: 17px;
	font-weight: normal;
	margin: 0;
}

#current-theme .description {
	margin-top: 5px;
}

#current-theme img {
	float: left;
	border: 1px solid #666;
	margin-right: 1em;
	margin-bottom: 1.5em;
	width: 150px;
}
table #availablethemes {
	border-spacing: 0px;
	border: none;
	border-top: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	margin: 10px auto;
}
td.available-theme {
	vertical-align: top;
	width: 240px;
	margin: 0;
	padding: 20px;
	text-align: left;
}
table#availablethemes td {
	border: 1px solid #eee;
	border-top: none;
}
table#availablethemes td.top {
	border-top: none;
}
table#availablethemes td.right {
	border-right: none;
	border-left: none;
}
table#availablethemes td.bottom {
	border-bottom: none;
}
table#availablethemes td.left {
	border-right: none;
	border-left: none;
}

#message {
   font-family: Lucida Grande, Lucida Sans, Arial, sans-serif;
   font-size: 17px;
}
</style>
<div class="wrap">
<?php if ( isset($_GET['activated']) ) : ?>
<div id="message" ><p style="text-align: center">New theme activated.</p></div>
<?php endif; ?>

<?php
$themes = get_themes();
$ct = current_theme_info();

ksort( $themes );
$theme_total = count( $themes );
$per_page = 15;

if ( isset( $_GET['pagenum'] ) )
	$page = absint( $_GET['pagenum'] );

if ( empty($page) )
	$page = 1;

$start = $offset = ( $page - 1 ) * $per_page;

$themes = array_slice( $themes, $start, $per_page );

?>


<h2>Current Theme</h2>
<div id="current-theme">
<?php if ( $ct->screenshot ) : ?>
<img src="<?php echo "../". $ct->template . '/' . $ct->screenshot; ?>" alt="Current theme preview" />
<?php endif; ?>
<h3><?php printf('%1$s %2$s by %3$s', $ct->title, $ct->version, $ct->author) ; ?></h3>
<p class="description"><?php echo $ct->description; ?></p>
<p><?php echo 'All of this theme&#8217;s files are located in <code>'.$theme_dir.'</code>.'; ?></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
<hr>
<h2>Available Themes</h2>
<p class="description">For additional themes, please visit: <a href="http://lfmtethemes.com" target="_blank">http://lfmtethemes.com</a></p>
<br class="clear" />

<?php if ( $page_links ) : ?>
<div class="tablenav">
<?php echo "<div class='tablenav-pages'>$page_links</div>"; ?>
<br class="clear" />
</div>
<br class="clear" />
<?php endif; ?>

<?php if ( 1 < $theme_total ) { ?>
<table id="availablethemes" cellspacing="0" cellpadding="0">
<?php
$style = '';

$theme_names = array_keys($themes);
natcasesort($theme_names);

$rows = ceil(count($theme_names) / 2);
for ( $row = 1; $row <= $rows; $row++ )
	for ( $col = 1; $col <= 2; $col++ )
		$table[$row][$col] = array_shift($theme_names);

foreach ( $table as $row => $cols ) {
?>
<tr>
<?php
foreach ( $cols as $col => $theme_name ) {
	$class = array('available-theme');
	if ( $row == 1 ) $class[] = 'top';
	if ( $col == 1 ) $class[] = 'left';
	if ( $row == $rows ) $class[] = 'bottom';
	if ( $col == 3 ) $class[] = 'right';
?>
	<td class="<?php echo join(' ', $class); ?>">
<?php if ( !empty($theme_name) ) :
	$template = $themes[$theme_name]['Template'];
	$title = $themes[$theme_name]['Title'];
	$version = $themes[$theme_name]['Version'];
	$description = $themes[$theme_name]['Description'];
	$author = $themes[$theme_name]['Author'];
	$screenshot = $themes[$theme_name]['Screenshot'];
	$preview_link = "";
	$preview_link = '';
	$preview_text = sprintf( 'Preview of "%s"', $title );
	$tags = $themes[$theme_name]['Tags'];
	$thickbox_class = 'thickbox';
	$activate_link = "admin.php?f=th&action=activate&amp;template=".urlencode($template);
	$activate_text = sprintf( 'Activate "%s"', $title );
?>
		<a href="<?php echo $activate_link; ?>" class="<?php echo $thickbox_class; ?> screenshot">
<?php if ( $screenshot ) : ?>
			<img src="<?php echo "../" . $template . '/' . $screenshot; ?>" alt="" />
<?php endif; ?>
		</a>
		<h3><a class="<?php echo $thickbox_class; ?>" href="<?php echo $activate_link; ?>"><?php echo $title; ?></a></h3>
		<p><?php echo $description; ?></p>
<?php if ( $tags ) : ?>
		<p>Tags: <?php echo join(', ', $tags); ?></p>
		<noscript><p class="themeactions"><a href="<?php echo $preview_link; ?>" title="<?php echo $preview_text; ?>">Preview</a> <a href="<?php echo $activate_link; ?>" title="<?php echo $activate_text; ?>">Activate</a></p></noscript>
<?php endif; ?>
		<div style="display:none;"><a class="previewlink" href="<?php echo $preview_link; ?>"><?php echo $preview_text; ?></a> <a class="activatelink" href="<?php echo $activate_link; ?>"><?php echo $activate_text; ?></a></div>
<?php endif; // end if not empty theme_name ?>
	</td>
<?php } // end foreach $cols ?>
</tr>
<?php } // end foreach $table ?>
</table>
<?php } ?>

<br class="clear" />

<?php if ( $page_links ) : ?>
<div class="tablenav">
<?php echo "<div class='tablenav-pages'>$page_links</div>"; ?>
<br class="clear" />
</div>
<?php endif; ?>

</div>

