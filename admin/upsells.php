<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

$site_url = "http://".$_SERVER["SERVER_NAME"];

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style3 {font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold;

}
-->
</style>

<br><br><center>
<?

$add = $_GET['add'];
$update = $_GET['update'];
$delete = $_GET['delete'];

if ($update == "yes") {
	//Update An Offer
	$id = $_GET['upsellid'];
	$offername = $_POST['offername'];
	$text = $_POST['text'];
	lfmsql_query("Update ".$prefix."upsells set offername='$offername', content='$text' where id=$id limit 1") or die(lfmsql_error());
} elseif ($add == "yes") {
	//Add An Offer
	$openkey = md5(time().rand(0,9));
	$offername = $_POST['offername'];
	$text = $_POST['text'];
	lfmsql_query("Insert into ".$prefix."upsells (openkey, offername, content) values ('$openkey', '$offername', '$text')") or die(lfmsql_error());
} elseif ($delete == "yes") {
	//Delete An Offer
	$confirmdelete = $_GET['confirmdelete'];
	$id = $_GET['upsellid'];
	if ($confirmdelete == "yes") {
		lfmsql_query("Delete from ".$prefix."upsells where id=$id limit 1") or die(lfmsql_error());
		$resmessage = "Offer Deleted";
	} else{
		echo("<b>Are you sure you want to delete this offer?</b><br><br><a href=admin.php?f=upsells&delete=yes&upsellid=$id&confirmdelete=yes><b>Yes</b></a><br><br><a href=admin.php?f=upsells><b>No</b></a>");
		exit;
	}
}

//Begin Main Page

?>

<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  <tr>
    <td align="center"><div class="lfm_infobox_heading">Upsell Offers</div></td>
  </tr>
  
  <tr>
    <td align="left"><div class="lfm_descr">
    	
    	<br>You can show your customers an upsell offer whenever they purchase one of your products.  After creating an offer below, you can add it to a product by going to the Sales Packages page, clicking on the product's ID number, and setting the Return URL to the Upsell URL of the offer.
    	
    </div></td>
  </tr>
      
</table>
</div>

<br><br>

<?

if ($resmessage != "") {
	echo("<p><font face=$fontface size=2>$resmessage</font></p>");
}

//Show Offers

$getdata = lfmsql_query("Select * from ".$prefix."upsells order by id asc");

if (lfmsql_num_rows($getdata) != 0) {

for ($i = 0; $i < lfmsql_num_rows($getdata); $i++) {
	
	$id = lfmsql_result($getdata, $i, "id");
	$openkey = lfmsql_result($getdata, $i, "openkey");
	$offername = lfmsql_result($getdata, $i, "offername");
	$text = lfmsql_result($getdata, $i, "content");
	$showid = $i+1;
	
	echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
	<tr><td align=center><b>Upsell URL</b><br><a target=_blank href=\"$site_url/showupsell.php?upsellkey=$openkey\">$site_url/showupsell.php?upsellkey=$openkey</a></td></tr>
	");
	
	echo("
	<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=upsells&update=yes&upsellid=$id\">
	<tr>
	<td align=center><b>Offer Name:</b> <input type=text size=20 name=offername value=\"$offername\"></td>
	</tr>");
	
	echo("<tr><td align=center valign=center>
	<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center>
		<table border=0 cellpadding=3 cellspacing=0>
			<tr><td align=center colspan=2><b>Offer Macros</b></td></tr>
			<tr><td align=right><b>#IPNid#</b> </td><td align=left> Insert payment button</td></tr>
			<tr><td align=right><b>[themeheader]</b> </td><td align=left> Insert your design theme header</td></tr>
			<tr><td align=right><b>[themefooter]</b> </td><td align=left> Insert your design theme footer</td></tr>
			<tr><td align=right><b>#FIRSTNAME#</b> </td><td align=left> Member first name</td></tr>
			<tr><td align=right><b>#LASTNAME#</b> </td><td align=left> Member last name</td></tr>
			<tr><td align=right><b>#USERNAME#</b> </td><td align=left> Member login username</td></tr>
			<tr><td align=right><b>#AFFILIATEID#</b> </td><td align=left> The members affiliate ID</td></tr>
		</table>
	</td><td align=center valign=center><b>Offer HTML</b><br><textarea name=text cols=50 rows=7>$text</textarea></td></tr></table>
	</td></tr>
	<tr><td align=center><input type=submit value=Update><br><br><a href=\"admin.php?f=upsells&delete=yes&upsellid=$id\"><font size=2 color=\"darkred\">Delete Offer</font></a></td></tr>
	</table>
	</form><br><br>");

}

}

//Add New Offer

echo("<table bgcolor=#EEEEEE border=1 bordercolor=black cellpadding=3 cellspacing=0>
<tr><td align=center><b>Add A New Offer</b></td></tr>");

echo("
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=upsells&add=yes\">
<tr>
<td align=center><b>Offer Name:</b> <input type=text size=20 name=offername value=\"\"></td>");

echo("<tr><td align=center valign=center>
<table border=0 cellpadding=3 cellspacing=0><tr><td align=right valign=center>
	<table border=0 cellpadding=3 cellspacing=0>
		<tr><td align=center colspan=2><b>Offer Macros</b></td></tr>
		<tr><td align=right><b>#IPNid#</b> </td><td align=left> Insert payment button</td></tr>
		<tr><td align=right><b>[themeheader]</b> </td><td align=left> Insert your design theme header</td></tr>
		<tr><td align=right><b>[themefooter]</b> </td><td align=left> Insert your design theme footer</td></tr>
		<tr><td align=right><b>#FIRSTNAME#</b> </td><td align=left> Member first name</td></tr>
		<tr><td align=right><b>#LASTNAME#</b> </td><td align=left> Member last name</td></tr>
		<tr><td align=right><b>#USERNAME#</b> </td><td align=left> Member login username</td></tr>
		<tr><td align=right><b>#AFFILIATEID#</b> </td><td align=left> The members affiliate ID</td></tr>
	</table>
</td><td align=center valign=center><b>Offer HTML</b><br><textarea name=text cols=50 rows=7></textarea></td></tr></table>
</td></tr>
<tr><td align=center><input type=submit value=Add></td></tr>
</table>
</form><br><br>");

?>