<?php
## Prize Page ##

#########################################
##     LFMTE Prize Page Addon v2       ##
##                                     ##
##        �2011 LJ Fix LLC             ##
##           www.LJFix.com             ##
##                                     ##
#########################################

// Prize Page Settings
$pPrizePage = "http://".$_SERVER["SERVER_NAME"]."/prizepage.php"; // URL for Prize Page
$pTable = $prefix.'prizepage_prizes';
$pWinnersTable = $prefix.'prizepage_prize_winners';
$pSettingsTable = $prefix.'prizepage_prize_settings';
$form_action = "admin.php?f=prizep";

if (isset($_POST['formm'])) {
	switch($_POST['formm']) {
		case 'winners':
			$sql="update `$pWinnersTable` set `delivered`=1 where ";
			if (is_array($_POST['deliver']))  {
				$ids=array();
				foreach($_POST['deliver'] as $d) {
					$ids[]='`id`='.$d;
				}
				$sql .= implode(' or ',$ids);
			mysql_query($sql);
			echo '<h3>Changes Made</h3>';
			}
			break;
		case 'settings':
			if ($_POST['rnd_a'] <= $_POST['rnd_b'] and $_POST['rnd_a'] >= 0 and $_POST['max_win'] >= 0) {
				mysql_query("update `$pSettingsTable` set `value`={$_POST['rnd_a']} where `field`='rnd_a'");
				mysql_query("update `$pSettingsTable` set `value`={$_POST['rnd_b']} where `field`='rnd_b'");
				if ($_POST['per_mem'] == 0 || $_POST['per_mem'] == 1) {
					mysql_query("update `$pSettingsTable` set `value`={$_POST['per_mem']} where `field`='per_mem'");
				}
				mysql_query("update `$pSettingsTable` set `value`={$_POST['max_win']} where `field`='max_win'");
				echo '<h3>Changes Made</h3>';
			}
			break;
		case 'add':
			if ('' != $_POST['name']) {
				$name=trim($_POST['name']);
				if ($_POST['qty']>=0) {
					$qty=$_POST['qty'];
				} else {
					$qty=0;
				}
				if (1 == $_POST['address']) {
					$address=1;
				} else {
					$address=0;
				}
				if (1 == $_POST['available']) {
					$avl=1;
				} else {
					$avl=0;
				}
				if (is_numeric($_POST['cred']) and $_POST['cred']>0) {
					$credits=$_POST['cred'];
				} else {
					$credits=0;
				}
				if (is_numeric($_POST['cash']) and $_POST['cash']>0) {
					$cash=$_POST['cash'];
				} else {
					$cash=0;
				}
				if ($_POST['ban']>=0) {
					$ban=$_POST['ban'];
				} else {
					$ban=0;
				}
				if ($_POST['text']>=0) {
					$text=$_POST['text'];
				} else {
					$text=0;
				}
				mysql_query("insert into `$pTable` (`name`,`qty_in`,`address`,`available`,`credits`,`cash`,`ban_imps`,`text_imps`) values ('".mysql_real_escape_string($name)."',$qty,$address,$avl,$credits,$cash,$ban,$text)");
				echo '<h3>Saved</h3>';
			} else {
				echo '<h3 style="color:red">You must enter a name</h3>';
			}
			break;
		case 'update':
			if (isset($_POST['del'])) {
				foreach($_POST['del'] as $id) {
					mysql_query("delete from `$pTable` where `id`=$id limit 1");
					mysql_query("delete from `$pWinnersTable` where `prize_id`=$id");
				}
			}
			$cnx=mysql_query("select * from `$pTable`");
			while ($r=mysql_fetch_assoc($cnx)) {
				$sql=array();
				if (1 == $_POST['available'][$r['id']]) {
					$val=1;
				} else {
					$val=0;
				}
				if ($val != $r['available']) {
					$sql[]="`available`=$val";
				}
				if (1 == $_POST['address'][$r['id']]) {
					$val=1;
				} else {
					$val=0;
				}
				if ($val != $r['address']) {
					$sql[]="`address`=$val";
				}
				if ($_POST['more'][$r['id']] > 0) {
					$sql[]='`qty_in`=`qty_in`+'.$_POST['more'][$r['id']];
				}
				if ($_POST['name'][$r['id']] != $r['name']) {
					$sql[]='`name`="'.mysql_real_escape_string($_POST['name'][$r['id']]).'"';
				}
				if ($_POST['cred'][$r['id']] != $r['credits'] and $_POST['cred'][$r['id']]>=0) {
					$sql[]='`credits`='.$_POST['cred'][$r['id']];
				}
				if ($_POST['cash'][$r['id']] != $r['cash'] and $_POST['cash'][$r['id']]>=0) {
					$sql[]='`cash`='.$_POST['cash'][$r['id']];
				}
				if ($_POST['ban'][$r['id']] != $r['ban_imps'] and $_POST['ban'][$r['id']]>=0) {
					$sql[]='`ban_imps`='.$_POST['ban'][$r['id']];
				}
				if ($_POST['text'][$r['id']] != $r['text_imps'] and $_POST['text'][$r['id']]>=0) {
					$sql[]='`text_imps`='.$_POST['text'][$r['id']];
				}
				if (array() != $sql) {
					$sql=implode(',',$sql);
					mysql_query("update `$pTable` set $sql where `id`={$r['id']}");
				}
			}
			break;
	}
}



if (isset($_GET['view']) and 'winners'==$_GET['view'] and $_GET['p']>0) {
	$ord='delivered';
	$direction='asc';
	$ord_links=array(
		'user' => $form_action.'&amp;view=winners&amp;p='.$_GET['p'].'&amp;order=user',
		'date' => $form_action.'&amp;view=winners&amp;p='.$_GET['p'].'&amp;order=date',
		'delivered' => $form_action.'&amp;view=winners&amp;p='.$_GET['p'].'&amp;order=delivered'
	);
	// Display Winners
	if (isset($_GET['order'])) {
		$order = $_GET['order'];
	} else {
		$order=$ord;
	}
	if ('-' == substr($order,0,1)) { // negative order
		$direction='desc';
		$order = substr($order,1,strlen($order)-1);
	} else {
		$ord_links[$order]=str_replace("=$order","=-$order",$ord_links[$order]);
	}
	switch($order) {
		case 'user':
			$ord='user_id';
			break;
		case 'date':
			$ord='datetime';
			break;
		case 'delivered':
			$ord='delivered';
			break;
	}
	echo '<p><a href="'.$form_action.'">Back to Prize Listing</a></p>';
	$cnx=mysql_query("select `id`,`user_id`,`delivered`,`datetime` from `$pWinnersTable` where `prize_id`={$_GET['p']} order by `$ord` $direction");
	if (mysql_num_rows($cnx) > 0) {
		echo '<form action="'.$form_action.'&amp;p='.$_GET['p'].'&amp;view=winners" method="POST"><input type="hidden" name="formm" value="winners" />';
		echo '<p><input type="submit" name="submit" value="Save" /></p>';
		echo '<table><tr style="font-weight:bold;"><td><a href="'.$ord_links['user'].'">User ID</a></td><td><a href="'.$ord_links['date'].'">Time Won</a></td><td><a href="'.$ord_links['delivered'].'">Delivered</a></td></tr>';
		while ($r=mysql_fetch_assoc($cnx)) {
			echo '<tr><td><a target="_blank" href="admin.php?f=mm&searchfield=Id&searchtext='.$r['user_id'].'&sf=browse">'.$r['user_id'].'</td><td>'.$r['datetime'].'</td><td>';
			if (1==$r['delivered']) {
				echo 'yes';
			} else {
				echo '<input type="checkbox" name="deliver[]" value="'.$r['id'].'" />';
			}
			echo "</td></tr>\n";
		}
	} else {
		echo '<h3>none</h3>';
	}
} else {
	// Settings
	$max_win_arr = array('no limit','day','week','month','2x per day','4x per day','8x per day','10x per day','20x per day','50x per day');
	$settings=array();
	$cnx=mysql_query("select `field`,`value` from `$pSettingsTable`");
	while ($r=mysql_fetch_assoc($cnx)) {
		$settings[$r['field']] = $r['value'];
	}
	echo '<div><form action="'.$form_action.'" method="POST"><input type="hidden" name="formm" value="settings" />';
	echo '<p>Show prize page randomly between <input type="text" name="rnd_a" size="4" value="'.$settings['rnd_a'].'" /> and <input type="text" name="rnd_b" size="4" value="'.$settings['rnd_b'].'" /> 
	<select name="per_mem"><option value="0"'; if($settings['per_mem'] == 0) { echo' selected'; } echo'>site-wide</option><option value="1"'; if($settings['per_mem'] == 1) { echo' selected'; } echo'>per member</option></select>
	 page views</p>';
	echo '<p>Member can win max once each <select name="max_win">'.selectedOption($max_win_arr,$settings['max_win']).'</select></p>';
	echo '<p><input type="submit" name="submit" value="Save" /></p>';
	echo '</form></div>';
	
	// Display and Add Prizes
	$ord='name';
	$direction='asc';
	$ord_links=array(
		'name' => $form_action.'&amp;order=name',
		'qty_avail' => $form_action.'&amp;order=qty_avail',
		'given' => $form_action.'&amp;order=given'
	);
	if (isset($_GET['order'])) {
		$order = $_GET['order'];
	} else {
		$order=$ord;
	}
	if ('-' == substr($order,0,1)) { // negative order
		$direction='desc';
		$order = substr($order,1,strlen($order)-1);
	} else {
		$ord_links[$order]=str_replace("=$order","=-$order",$ord_links[$order]);
	}
	switch($order) {
		case 'name':
			$ord='name';
			break;
		case 'qty_avail':
			$ord='qty_avail';
			break;
		case 'given':
			$ord='qty_out';
			break;
	}
	
	// Add Prize
	echo '<form action="'.$form_action.'" method="POST"><input type="hidden" name="formm" value="add" />';
	?>
	<hr />
	<h3>Add a Prize</h3>
	<table>
	<tr><td>Name:</td><td><input type="text" name="name" size="35" /></td></tr>
	<tr><td>Qty:</td><td><input type="text" name="qty" size="4" /></td></tr>
	<tr><td>Request Address:</td><td><input type="checkbox" name="address" value="1" /></td></tr>
	<tr><td>Give Credits:</td><td><input type="text" name="cred" value="0" size="4" /></td></tr>
	<tr><td>Give Cash:</td><td><input type="text" name="cash" value="0" size="4" /></td></tr>
	<tr><td>Give Banner Impressions:</td><td><input type="text" name="ban" value="0" size="4" /></td></tr>
	<tr><td>Give Text Link Impressions:</td><td><input type="text" name="text" value="0" size="4" /></td></tr>
	<tr><td>Make Available:</td><td><input type="checkbox" name="available" value="1" /></td></tr>
	<tr><td colspan="2"><input type="submit" name="submit" value="Add a Prize" /></td></tr>
	</table>
	</form>
	<hr />
	<?php
	// List Prizes
	$cnx = mysql_query("select `id`,`name`,`qty_in`,`qty_out`,(`qty_in` - `qty_out`) as `qty_avail`,`available`,`address`,`credits`,`cash`,`ban_imps`,`text_imps` from `$pTable` order by $ord $direction");
	if (mysql_num_rows($cnx) > 0) {
		echo '<form action="'.$form_action.'" method="POST"><input type="hidden" name="formm" value="update" />';
		echo '<table border="1"><tr style="font-weight:bold;"><td>Make Available</td><td><a href="'.$ord_links['name'].'">Name</a></td><td><a href="'.$ord_links['qty_avail'].'">Qty Avail</a></td><td><a href="'.$ord_links['given'].'">Given</a></td><td>Add More</td><td>Req. Address</td><td>Credits</td><td>Cash</td><td>Ban. Imps</td><td>Text Imps</td><td>Delete</td><td>Winners</td></tr>';
		while ($r=mysql_fetch_assoc($cnx)) {
			$winners='<a href="'.$form_action.'&amp;p='.$r['id'].'&amp;view=winners">';
			$win_cnx=mysql_query("select count(`prize_id`) as `c` from $pWinnersTable where `prize_id`={$r['id']} and delivered=0");
			$num=mysql_result($win_cnx,0);
			if ($num>0) {
				$winners .= $num.' waiting</a>';
			} else {
				$winners .= 'view</a>';
			}
			echo '<tr><td>'.checkedCheckbox("available[{$r['id']}]",$r['available']).'</td><td><input type="text" name="name['.$r['id'].']" value="'.$r['name'].'" size="30" /></td><td>'.$r['qty_avail'].'</td><td>'.$r['qty_out'].'</td><td><input type="text" name="more['.$r['id'].']" size="4" /></td><td>'.checkedCheckbox("address[{$r['id']}]",$r['address']).'</td>';
			echo '<td><input type="text" name="cred['.$r['id'].']" value="'.$r['credits'].'" size="4" /></td><td><input type="text" name="cash['.$r['id'].']" value="'.$r['cash'].'" size="4" /></td><td><input type="text" name="ban['.$r['id'].']" value="'.$r['ban_imps'].'" size="4" /></td><td><input type="text" name="text['.$r['id'].']" value="'.$r['text_imps'].'" size="4" /></td><td>';
			if ($num==0) {
				echo '<input type="checkbox" name="del[]" value="'.$r['id'].'" />';
			}
			echo '</td><td>'.$winners."</td></tr>\n";
		}
		echo '</table>';
		echo '<input type="submit" name="submit" value="Save" /></form>';
	}
}

function checkedCheckbox($name,$data,$checkData=1,$val=1) {
	$out = '';
	if ($data==$checkData) {
		$out='<input type="checkbox" name="'.$name.'" value="'.$val.'" checked />';
	} else {
		$out='<input type="checkbox" name="'.$name.'" value="'.$val.'" />';
	}
	return $out;
}
function selectedOption($op,$data) {
	$out = '';
	foreach($op as $k=>$v) {
		if ($k==$data) {
			$out .= "<option value=\"$k\" selected>$v</option>";
		} else {
			$out .= "<option value=\"$k\">$v</option>";
		}
	}
	return $out;
}