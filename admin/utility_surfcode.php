<?php

// LFMTE Surf Promo Code Plugin v3
// �2014 LFM Wealth Systems
// http://thetrafficexchangescript.com

require_once "../inc/filter.php";
// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php"; 
if(!isset($_SESSION["adminid"])) { exit; };

$errormess = "";

echo("<center><br><br>");

// Add A Prize
if ($_GET['add'] == "prize" && isset($_POST['prizename'])) {
	$prizename = $_POST['prizename'];
	$download = $_POST['download'];
	$credits = $_POST['credits'];
	$banners = $_POST['banners'];
	$texts = $_POST['texts'];
	$cash = $_POST['cash'];
	$sendemail = $_POST['sendemail'];
	$emailsubj = $_POST['emailsubj'];
	$emailtext = $_POST['emailtext'];
	
	$peelimps = $_POST['peelimps'];
	$sqbanimps = $_POST['sqbanimps'];
	$porkyps = $_POST['porkyps'];
	
	if (isset($sendemail)) {
		$sendemail = 1;
	} else {
		$sendemail = 0;
	}
	
	mysql_query("Insert into ".$prefix."promo_prizes (prizename, download, credits, banners, texts, cash, sendemail, emailsubj, emailtext, peelimps, sqbanimps, porkyps) values ('".$prizename."', '".$download."', $credits, $banners, $texts, '$cash', '".$sendemail."', '".$emailsubj."', '".$emailtext."', '".$peelimps."', '".$sqbanimps."', '".$porkyps."')") or die(mysql_error());
}

// Edit A Prize
if ($_GET['edit'] == "prize" && is_numeric($_GET['prizeid']) && isset($_POST['prizename'])) {
	$prizename = $_POST['prizename'];
	$download = $_POST['download'];
	$credits = $_POST['credits'];
	$banners = $_POST['banners'];
	$texts = $_POST['texts'];
	$cash = $_POST['cash'];
	$sendemail = $_POST['sendemail'];
	$emailsubj = $_POST['emailsubj'];
	$emailtext = $_POST['emailtext'];
	
	$peelimps = $_POST['peelimps'];
	$sqbanimps = $_POST['sqbanimps'];
	$porkyps = $_POST['porkyps'];
	
	if (isset($sendemail)) {
		$sendemail = 1;
	} else {
		$sendemail = 0;
	}
	
	mysql_query("Update ".$prefix."promo_prizes set prizename='".$prizename."', download='".$download."', credits=$credits, banners=$banners, texts=$texts, cash='$cash', sendemail='".$sendemail."', emailsubj='".$emailsubj."', emailtext='".$emailtext."', peelimps='".$peelimps."', sqbanimps='".$sqbanimps."', porkyps='".$porkyps."' where id=".$_GET['prizeid']) or die(mysql_error());
}

// Add A Code
if ($_GET['add'] == "code" && isset($_POST['prizeid'])) {
	$clicks = $_POST['clicks'];
	$prizeid = $_POST['prizeid'];
	$refid = 0;
	$code = $_POST['code'];
	$sendadmin = $_POST['sendadmin'];
	$adminto = $_POST['adminto'];
	$emailsubj = $_POST['emailsubj'];
	$adminemail = $_POST['adminemail'];
	
	if (isset($sendadmin)) {
		$sendadmin = 1;
	} else {
		$sendadmin = 0;
	}
	
	mysql_query("Insert into ".$prefix."promo_codes (enabled, clicks, prizeid, refid, code, sendadmin, adminto, emailsubj, adminemail) values (1, ".$clicks.", ".$prizeid.", ".$refid.", '".$code."', ".$sendadmin.", '".$adminto."', '".$emailsubj."', '".$adminemail."')") or die(mysql_error());
}

// Edit A Code
if ($_GET['edit'] == "code" && is_numeric($_GET['codeid']) && isset($_POST['prizeid'])) {
	$clicks = $_POST['clicks'];
	$prizeid = $_POST['prizeid'];
	$refid = 0;
	$code = $_POST['code'];
	$sendadmin = $_POST['sendadmin'];
	$adminto = $_POST['adminto'];
	$emailsubj = $_POST['emailsubj'];
	$adminemail = $_POST['adminemail'];
	
	if (isset($sendadmin)) {
		$sendadmin = 1;
	} else {
		$sendadmin = 0;
	}
	
	mysql_query("Update ".$prefix."promo_codes set clicks=".$clicks.", prizeid=".$prizeid.", refid=".$refid.", code='".$code."', sendadmin=".$sendadmin.", adminto='".$adminto."', emailsubj='".$emailsubj."', adminemail='".$adminemail."' where id=".$_GET['codeid']) or die(mysql_error());
}

// Enable Code
if ($_GET['status'] == "enable" && is_numeric($_GET['codeid'])) {
	mysql_query("Update ".$prefix."promo_codes set enabled=1 where id=".$_GET['codeid']) or die(mysql_error());
}

// Disable Code
if ($_GET['status'] == "disable" && is_numeric($_GET['codeid'])) {
	mysql_query("Update ".$prefix."promo_codes set enabled=0 where id=".$_GET['codeid']) or die(mysql_error());
}

// Delete Code
if ($_GET['delcode'] == "yes" && is_numeric($_GET['codeid'])) {
	mysql_query("Delete from ".$prefix."promo_codes where id=".$_GET['codeid']) or die(mysql_error());
}

// Delete Prize
if ($_GET['delprize'] == "yes" && is_numeric($_GET['prizeid'])) {
	mysql_query("Delete from ".$prefix."promo_prizes where id=".$_GET['prizeid']) or die(mysql_error());
	mysql_query("Delete from ".$prefix."promo_codes where prizeid=".$_GET['prizeid']) or die(mysql_error());
}

// Start Main Page
if ($errormess != "") {
	echo("<p><b>".$errormess."</b></p>");
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<?

$getimpsetting = mysql_query("Select storeimps from ".$prefix."settings limit 1");
$impsetting = mysql_result($getimpsetting, 0, "storeimps");

echo("<table style=\"border:thin solid #000 ; border-width: 1px;\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\" width=\"400\">
  <tr>
    <td colspan=\"14\" align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Prizes</font> </strong></td>
  </tr>
  
  <tr>
    <td align=\"center\" class=\"admintd\">Prize Name</td><td align=\"center\" class=\"admintd\">Download</td><td align=\"center\" class=\"admintd\">Credits</td>");
    
	if ($impsetting == 1) {
		echo("<td align=\"center\" class=\"admintd\">Banners</td><td align=\"center\" class=\"admintd\">Text Imps</td>");
	}
	
	if (file_exists("peelads.php")) {
		echo("<td align=\"center\" class=\"admintd\">Peels</td>");
	}
	
	if (file_exists("sqbanners.php")) {
		echo("<td align=\"center\" class=\"admintd\">Sq Banners</td>");
	}
	
	if (file_exists("../inc/porkyp_funcs.php")) {
		echo("<td align=\"center\" class=\"admintd\">Porky Pnts</td>");
	}
	
	if (file_exists("jetfuel.php")) {
		echo("<td align=\"center\" class=\"admintd\">Jetfuel</td>");
	}
	
    echo("<td align=\"center\" class=\"admintd\">Cash</td><td align=\"center\" class=\"admintd\">Email Member</td><td align=\"center\" class=\"admintd\">Email Text</td><td align=\"center\" class=\"admintd\">&nbsp;</td><td align=\"center\" class=\"admintd\">&nbsp;</td>
  </tr>
  ");


// Get Prizes
$getprizes = mysql_query("Select * from ".$prefix."promo_prizes order by id asc");
for ($i = 0; $i < mysql_num_rows($getprizes); $i++) {
	$prizeid = mysql_result($getprizes, $i, "id");
	$prizename = mysql_result($getprizes, $i, "prizename");
	$download = mysql_result($getprizes, $i, "download");
	$credits = mysql_result($getprizes, $i, "credits");
	$banners = mysql_result($getprizes, $i, "banners");
	$texts = mysql_result($getprizes, $i, "texts");
	$cash = mysql_result($getprizes, $i, "cash");
	$sendemail = mysql_result($getprizes, $i, "sendemail");
	$emailsubj = mysql_result($getprizes, $i, "emailsubj");
	$emailtext = mysql_result($getprizes, $i, "emailtext");
	
	$peelimps = mysql_result($getprizes, $i, "peelimps");
	$sqbanimps = mysql_result($getprizes, $i, "sqbanimps");
	$porkyps = mysql_result($getprizes, $i, "porkyps");
	
	$jetfuel = mysql_result($getprizes, $i, "jetfuel");
	
	echo("<tr>
	<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=surfcode&edit=prize&prizeid=".$prizeid."\">
	<td valign=\"middle\"><input type=text size=20 name=prizename value=\"".$prizename."\"></td>
	<td valign=\"middle\"><select name=download>
	<option value=0>None</option>");

	$getdownloads = mysql_query("Select productid, productname from `".$prefix."products` where free=0 order by productname asc");
	for ($j = 0; $j < mysql_num_rows($getdownloads); $j++) {
		$productid = mysql_result($getdownloads, $j, "productid");
		$productname = mysql_result($getdownloads, $j, "productname");
		echo("<option value=$productid"); if($download==$productid){echo(" selected");} echo(">$productname</option>");
	}
	
	echo("</select></td>
	
	<td valign=\"middle\"><input type=text size=3 name=credits value=\"".$credits."\"></td>");
	
	if ($impsetting == 1) {
		echo("<td valign=\"middle\"><input type=text size=3 name=banners value=\"".$banners."\"></td>
		<td valign=\"middle\"><input type=text size=3 name=texts value=\"".$texts."\"></td>");
	} else {
		echo("<input type=hidden name=banners value=\"0\"><input type=hidden name=texts value=\"0\">");
	}
	
	if (file_exists("peelads.php")) {
		echo("<td valign=\"middle\"><input type=text size=3 name=peelimps value=\"".$peelimps."\"></td>");
	} else {
		echo("<input type=hidden name=peelimps value=\"0\">");
	}
	
	if (file_exists("sqbanners.php")) {
		echo("<td valign=\"middle\"><input type=text size=3 name=sqbanimps value=\"".$sqbanimps."\"></td>");
	} else {
		echo("<input type=hidden name=sqbanimps value=\"0\">");
	}
	
	if (file_exists("../inc/porkyp_funcs.php")) {
		echo("<td valign=\"middle\"><input type=text size=3 name=porkyps value=\"".$porkyps."\"></td>");
	} else {
		echo("<input type=hidden name=porkyps value=\"0\">");
	}
	
	if (file_exists("jetfuel.php")) {
		if ($jetfuel < 1) {
			$countjetfuel = "No";
		} else {
			$countjetfuel = "Yes";
		}
		echo ('<td valign="middle">'.$countjetfuel.'<br><span style="cursor:pointer; color:blue; size:8px;" onClick="window.open(\'/admin/promocode_jetfuel.php?id='.$prizeid.'\',\'set_jetfuel\',\'scrollbars=yes, menubars=no, width=550, height=550\');">Set</span></td>');
	}
	
	echo("<td valign=\"middle\"><input type=text size=3 name=cash value=\"".$cash."\"></td>
	<td valign=\"middle\"><input type=\"checkbox\" name=\"sendemail\""); if ($sendemail == 1) { echo(" checked"); } echo("></td>
	<td valign=\"middle\" align=\"left\">Subject: <input type=text size=20 name=emailsubj value=\"".$emailsubj."\"><br /><textarea name=\"emailtext\" wrap=\"soft\" rows=\"5\" cols=\"30\">".$emailtext."</textarea></td>
	<td valign=\"middle\"><input type=submit value=\"Edit\"></td>
	</form>
	<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=surfcode&delprize=yes&prizeid=".$prizeid."\">
	<td valign=\"middle\"><input type=submit value=\"Delete\"></td>
	</form>
	</tr>");
}

// Add A New Prize
echo("<tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=surfcode&add=prize\">
<td valign=\"middle\"><input type=text size=20 name=prizename value=\"\"></td>
<td valign=\"middle\"><select name=download>
<option value=0>None</option>");

$getdownloads = mysql_query("Select productid, productname from `".$prefix."products` where free=0 order by productname asc");
for ($j = 0; $j < mysql_num_rows($getdownloads); $j++) {
	$productid = mysql_result($getdownloads, $j, "productid");
	$productname = mysql_result($getdownloads, $j, "productname");
	echo("<option value=$productid>$productname</option>");
}

echo("</select></td>

	<td valign=\"middle\"><input type=text size=3 name=credits value=\"0\"></td>");
	
	if ($impsetting == 1) {
		echo("<td valign=\"middle\"><input type=text size=3 name=banners value=\"0\"></td>
		<td valign=\"middle\"><input type=text size=3 name=texts value=\"0\"></td>");
	} else {
		echo("<input type=hidden name=banners value=\"0\"><input type=hidden name=texts value=\"0\">");
	}
	
	if (file_exists("peelads.php")) {
		echo("<td valign=\"middle\"><input type=text size=3 name=peelimps value=\"0\"></td>");
	} else {
		echo("<input type=hidden name=peelimps value=\"0\">");
	}
	
	if (file_exists("sqbanners.php")) {
		echo("<td valign=\"middle\"><input type=text size=3 name=sqbanimps value=\"0\"></td>");
	} else {
		echo("<input type=hidden name=sqbanimps value=\"0\">");
	}
	
	if (file_exists("../inc/porkyp_funcs.php")) {
		echo("<td valign=\"middle\"><input type=text size=3 name=porkyps value=\"0\"></td>");
	} else {
		echo("<input type=hidden name=porkyps value=\"0\">");
	}
	
	if (file_exists("jetfuel.php")) {
		echo("<td valign=\"middle\">N/A</td>");
	}
	
	echo("<td valign=\"middle\"><input type=text size=3 name=cash value=\"0.00\"></td>

<td valign=\"middle\"><input type=\"checkbox\" name=\"sendemail\"></td>
<td valign=\"middle\" align=\"left\">Subject: <input type=text size=20 name=emailsubj value=\"Your Promo Code\"><br /><textarea name=\"emailtext\" wrap=\"soft\" rows=\"5\" cols=\"30\">You successfully completed the surf code.  You can download your prize here: example.com</textarea></td>
<td valign=\"middle\"><input type=submit value=\"Add\"></td>
</form>
<td valign=\"middle\">&nbsp;</td>
</tr>");

echo("</table>
<br /><br />");

$checkprizes = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."promo_prizes"), 0);
if ($checkprizes > 0) {

echo("<table style=\"border:thin solid #000 ; border-width: 1px;\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"0\" width=\"400\">
  <tr>
    <td colspan=\"9\" align=\"center\" class=\"admintd\"><strong><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">Surf Codes</font> </strong></td>
  </tr>
  
  <tr>
    <td align=\"center\" class=\"admintd\">Code</td><td align=\"center\" class=\"admintd\">Clicks</td><td align=\"center\" class=\"admintd\">Prize</td><td align=\"center\" class=\"admintd\">Notify</td><td align=\"center\" class=\"admintd\">Notify Email</td><td align=\"center\" class=\"admintd\">Notify Message</td><td align=\"center\" class=\"admintd\">Traffic Codex</td><td align=\"center\" class=\"admintd\">&nbsp;</td><td align=\"center\" class=\"admintd\">&nbsp;</td><td align=\"center\" class=\"admintd\">&nbsp;</td>
  </tr>
  ");

// Get Codes
$getcodes = mysql_query("Select * from ".$prefix."promo_codes order by id asc");
for ($i = 0; $i < mysql_num_rows($getcodes); $i++) {
	$codeid = mysql_result($getcodes, $i, "id");
	$clicks = mysql_result($getcodes, $i, "clicks");
	$prizeid = mysql_result($getcodes, $i, "prizeid");
	$code = mysql_result($getcodes, $i, "code");
	$sendadmin = mysql_result($getcodes, $i, "sendadmin");
	$adminto = mysql_result($getcodes, $i, "adminto");
	$emailsubj = mysql_result($getcodes, $i, "emailsubj");
	$adminemail = mysql_result($getcodes, $i, "adminemail");
	
	$tcodex = mysql_result($getcodes, $i, "tcodex");
	
	echo("<tr>
	<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=surfcode&edit=code&codeid=".$codeid."\">
	<td valign=\"middle\"><input type=text size=10 name=\"code\" value=\"".$code."\"></td>
	<td valign=\"middle\"><input type=text size=3 name=\"clicks\" value=\"".$clicks."\"></td>
	<td valign=\"middle\"><select name=prizeid>");

	$getprizes = mysql_query("Select id, prizename from ".$prefix."promo_prizes order by id asc");
	for ($j = 0; $j < mysql_num_rows($getprizes); $j++) {
		$getprizeid = mysql_result($getprizes, $j, "id");
		$prizename = mysql_result($getprizes, $j, "prizename");
		echo("<option value=$getprizeid"); if($getprizeid==$prizeid){echo(" selected");} echo(">$prizename</option>");
	}
	
	echo("</select></td>
	<td valign=\"middle\"><input type=\"checkbox\" name=\"sendadmin\""); if ($sendadmin == 1) { echo(" checked"); } echo("></td>
	<td valign=\"middle\"><input type=text size=15 name=\"adminto\" value=\"".$adminto."\"></td>
	<td valign=\"middle\" align=\"left\">Subject: <input type=text size=20 name=emailsubj value=\"".$emailsubj."\"><br /><textarea name=\"adminemail\" wrap=\"soft\" rows=\"5\" cols=\"30\">".$adminemail."</textarea></td>");
	
	if ($tcodex < 1) {
		$texttcodex = 'No<br><span style="cursor:pointer; color:blue; size:8px;" onClick="window.open(\'/admin/promocode_tcodex.php?id='.$codeid.'\',\'traffic_codex\',\'scrollbars=yes, menubars=no, width=550, height=550\');">Add</span>';
	} else {
		$texttcodex = 'Yes';
	}
	echo ('<td valign="middle">'.$texttcodex.'</td>');
	
	echo("<td valign=\"middle\"><input type=submit value=\"Edit\"></td>
	</form>
	<form style=\"margin:0px\" method=\"post\" target=\"_blank\" action=\"promocode_list.php?codeid=".$codeid."\">
	<td valign=\"middle\"><input type=submit value=\"View Members\"></td>
	</form>
	<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=surfcode&delcode=yes&codeid=".$codeid."\">
	<td valign=\"middle\"><input type=submit value=\"Delete\"></td>
	</form>
	</tr>");
}

// Add A Code
echo("<tr>
<form style=\"margin:0px\" method=\"post\" action=\"admin.php?f=surfcode&add=code\">
<td valign=\"middle\"><input type=text size=10 name=\"code\" value=\"\"></td>
<td valign=\"middle\"><input type=text size=3 name=\"clicks\" value=\"\"></td>
<td valign=\"middle\"><select name=prizeid>");

$getprizes = mysql_query("Select id, prizename from ".$prefix."promo_prizes order by id asc");
for ($j = 0; $j < mysql_num_rows($getprizes); $j++) {
	$getprizeid = mysql_result($getprizes, $j, "id");
	$prizename = mysql_result($getprizes, $j, "prizename");
	echo("<option value=$getprizeid>$prizename</option>");
}

echo("</select></td>
<td valign=\"middle\"><input type=\"checkbox\" name=\"sendadmin\"></td>
<td valign=\"middle\"><input type=text size=15 name=adminto value=\"\"></td>
<td valign=\"middle\" align=\"left\">Subject: <input type=text size=20 name=emailsubj value=\"Promo Code Notification\"><br /><textarea name=\"adminemail\" wrap=\"soft\" rows=\"5\" cols=\"30\">This is a notification that a member completed your surf code at #SITENAME#.  The member is:\n#FIRSTNAME#\n#LASTNAME#\n#USERNAME#</textarea></td>
<td valign=\"middle\">&nbsp;</td>
<td valign=\"middle\"><input type=submit value=\"Add\"></td>
</form>
<td valign=\"middle\">&nbsp;</td>
<td valign=\"middle\">&nbsp;</td>
</tr>");

echo("</table>
<br /><br />");
}

?>