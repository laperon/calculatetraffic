<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";


// Prevent anyone who isn't logged in from opening this page
include "../inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

if (!isset($_GET['userid']) || !is_numeric($_GET['userid'])) {
	echo("Invalid User ID");
	exit;
}

$userid = $_GET['userid'];

$starttime = $_GET['starttime'];
$endtime = $_GET['endtime'];

if (!is_numeric($starttime) || $starttime < 1) {
	$starttime = strtotime("2000-01-01");
	$endtime = strtotime(date("Y-m-d")) + 86399;
}

$startdate = date("Y-m-d",$starttime);
$enddate = date("Y-m-d",$endtime);

?>
<html>
<body>

<link href="styles.css" rel="stylesheet" type="text/css" />

<center>

<table width="600" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
	
  <tr>
    <td colspan="7" align="center" class="admintd"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Referrals of Member #<? echo($userid); ?> (<? echo($startdate); ?> - <? echo($enddate); ?>)</font> </strong></td>
  </tr>
  
  <tr>
    <td align="center"><b>ID</b></td><td align="center"><b>Username</b></td><td align="center"><b>First Name</b></td><td align="center"><b>Last Name</b></td><td align="center"><b>Email</b></td><td align="center"><b>Status</b></td><td align="center"><b>IP Address</b></td>
  </tr>
<?

$ipchecker[0] = "12345";
$ipindex = 1;

$getrefs = mysql_query("Select Id, username, firstname, lastname, email, status, signupip from ".$prefix."members where refid=".$userid." AND joindate <= '".$enddate." 23:59:59' AND joindate >= '".$startdate."'");

if (mysql_num_rows($getrefs) == 0) {
	echo("<tr><td align=\"center\" colspan=\"7\">No Referrals Found.</td></tr>
	</table>
	</center>
	</body>
	</html>");
	exit;
}

for ($i = 0; $i < mysql_num_rows($getrefs); $i++) {
	$memid = mysql_result($getrefs, $i, "Id");
	$username = mysql_result($getrefs, $i, "username");
	$firstname = mysql_result($getrefs, $i, "firstname");
	$lastname = mysql_result($getrefs, $i, "lastname");
	$email = mysql_result($getrefs, $i, "email");
	$status = mysql_result($getrefs, $i, "status");
	$ipadd = mysql_result($getrefs, $i, "signupip");
	$ipbg = "white";
	$stbg = "white";
	
	if ($ipadd != "") {
		
		$ipcheck = array_search($ipadd, $ipchecker);
		if ($ipcheck === false) {
			$ipbg = "white";
		} else {
			$ipbg = "red";
		}
		
		$ipchecker[$ipindex] = $ipadd;
		$ipindex++;
		
	}
	
	if ($status != "Active") {
		$stbg = "yellow";
	}
	
	echo("<tr>
	<td>$memid</td><td>$username</td><td>$firstname</td><td>$lastname</td><td>$email</td><td bgcolor=\"$stbg\">$status</td><td bgcolor=\"$ipbg\">$ipadd</td>
	</tr>");

}

echo("</table>");
?>
<br>
<table width="600" border="1" align="center" bordercolor="#FFFFFF">
  <tr>
    <td align="left" valign="top" bordercolor="#000000" bgcolor="#FFFF99"><p><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notes:</font></strong></p>
      <p><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Multiple referrals from the same IP Address will be shown in red.  Referrals that are unverified or suspended will be shown in yellow and are not counted in a member's stats.</font></p>
      
      </font></p>

      <p>&nbsp;</p></td>
  </tr>
</table>
<?
echo("</center>
</body>
</html>");

exit;

?>
