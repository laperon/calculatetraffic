<?php

// VisuGraph 1.0
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

/*

	##### Bar Chart Function Parameter Examples

	##### X AXIS LABELS
	$xaxis = array(

		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
	);

	##### EACH SET OF BARS PLOTTED IS IN AN ARRAY, AND EACH OF THESE ARRAYS ARE THEIR OWN INDIVIDUAL ITEM IN ANOTHER ARRAY
	$mybars = array(

		array('46','332','78','36','778','423'),
		array('189','439','1698','439','58','121'),
	);

	##### COLORS OF BAR SETS
	$colors = array(

		'#333333',
		'#666666'.
		'#999999',
	);

	##### TRANSPARENCY OF BAR SETS
	##### A NUMBER 0 THRU 127, 0 BEING 100% OPAQUE, 127 BEING 100% TRANSPARENT

	$transparency = 50;

	$w = 540; // WIDTH OF GRAPH IMAGE
	$h = 470; // HEIGHT OF GRAPH IMAGE
	$margin = 60; // TOP AND LEFT MARGIN IN PIXELS BETWEEN IMAGE EDGE AND BEGINNING OF GRAPH BARS
	$margin2 = 20; // TOP MARGIN IN PIXELS
	
	$showlabels = 1; // SET TO 0 TO HIDE LABELS
	
	$fontsize = 0; // SET TO 0 TO AUTOMATICALLY ADJUST

*/


##### BEGIN CODE
// VisuGraph 1.0
// �2009 Josh Abbott
// Not to be sold or distributed individually

function drawbargraph($xaxis, $mybars, $colors, $showlabels=1, $fontsize=0, $transparency=50, $w=540, $h=470, $margin=60, $margin2=20) {

##### CREATE OUR IMAGE, AND SET THE BASE COLORS WE WILL USE

$image = imagecreatetruecolor($w, $h);

$white = imagecolorallocate($image, 255, 255, 255);
$black = imagecolorallocate($image, 0, 0, 0);
$gray = imagecolorallocate($image, 185, 185, 185);
$lightgray = imagecolorallocate($image, 240, 240, 240);

imagefill($image, 0, 0, $white);

##### DRAW RECTANGLES ACROSS THE GRAPH IMAGE TO MAKE OUR 'VERTICAL' LINES IN THE GRAPH,
##### FILLED LIGHT GRAY ONES TO MAKE THE BACKGROUND COLOR AND HOLLOW DARK GRAY ONES TO MAKE THE LINES

$divider1 = round(($w - ($margin/3) - $margin2) / count($xaxis) - 4,0);
$startx = $margin;
$twidth = 1000;
$i = '0';
	
	if ($fontsize == 0) {
	$fsize = 10;
	while ($twidth > ($divider1 - 5)) {
		$bbox = imagettfbbox($fsize,'0',"Vera.ttf",$xaxis[$i]);
		$twidth = abs($bbox[2]-$bbox[0]);
		$fsize--;
	}
	} else {
	$fsize = $fontsize;
	}
		
for ($i=0;$i<(count($xaxis));$i++) {
	imagefilledrectangle($image, $startx, $margin2, $startx + $divider1, ($h - $margin), $lightgray);
	imagerectangle($image, $startx, $margin2, $startx + $divider1, ($h - $margin), $gray);
	imagettftext($image,$fsize,0,$startx,($h - $margin + 20),$black,"Vera.ttf",$xaxis[$i]);
	$startx += $divider1;
	$k = $i;
}

$k++;
imagettftext($image,$fsize,0,$startx,($h - $margin + 20),$black,"Vera.ttf",$xaxis[$k]);

$fsize = 10;
while ($twidth > ($divider1 - 5)) {
	$bbox = imagettfbbox($fsize,'0',"Vera.ttf",$xaxis[$i]);
	$twidth = abs($bbox[2]-$bbox[0]);
	$fsize--;
}

##### FIGURE OUT THE LARGEST VALUE OF ALL THE NUMBERS TO BE PLOTTED

foreach ($mybars as $ii) {
	foreach ($ii as $i) {
		if ($largest < $i) {
			$largest = $i;
		}
	}
}

##### TAKE THE HIGHEST VALUE AND UP IT TO THE NEXT '100' VALUE TO MAKE TOP BAR OF GRAPH
$top = ceilpow1000($largest);

if ($top == 0) {
	$top = 1;
}

if ($largest < 20) {
	$top = 20;
}

##### NOW DRAW RECTANGLES DOWN THE GRAPH IMAGE TO MAKE OUR 'HORIZONTAL' LINES IN THE GRAPH

$divider2 = (($h - $margin - $margin2) / 10);
$starty = $margin2;
$textnum = $top;
$twidth = 1000;

	while ($twidth > ($margin - 5)) {
		$bbox = imagettfbbox($fsize,'0',"Vera.ttf",$textnum);
		$twidth = abs($bbox[2]-$bbox[0]);
		$fsize--;
	}
	
for ($i=0;$i<10;$i++) {
	imagerectangle($image, $margin, $starty, $startx, $starty + $divider2, $gray);
	imagettftext($image,$fsize,0,($margin - $twidth - 10),$starty + ($fsize/2),$black,"Vera.ttf",$textnum);
	$starty += $divider2;
	$textnum -= $top / 10;
}
imagettftext($image,$fsize,0,($margin - $twidth - 10),$starty + ($fsize / 2),$black,"Vera.ttf",'0');

$texta = array();
$boxa = array();

##### TIME TO PLOT THE BARS ON THE GRAPH 

$l = 0;
foreach ($mybars as $bar1) {
	$lastx = '';
	$lasty = '';
	$g = $starty - $margin2;
	$pp = $g / $top;
	$startx = $margin + ($l * 8);
	$color = str_replace("#","",$colors[$l]);
	$c = hexrgb($color);
	$l++;
	for ($i=0;$i<(count($xaxis));$i++) {

		ImageFilledRectangle($image, $startx, ($starty - ($bar1[$i] * $pp)), ($startx + 16), $starty, imagecolorallocatealpha($image,$c['red'],$c['green'],$c['blue'],$transparency));

		$bbox = imagettfbbox($fsize - 1,'0',"Vera.ttf",$bar1[$i]);
		$twidth = abs($bbox[2]-$bbox[0]);
		$theight = abs($bbox[1]-$bbox[7]);

		$boxa[] = "ImageFilledRectangle(\$image, $startx + 11, ($starty - (".$bar1[$i]." * $pp) - 8), $startx + 11 + $twidth + 6, ($starty - (".$bar1[$i]." * $pp) - 3) + $theight + 3, $black);";
		$texta[] = "imagettftext(\$image, $fsize - 1, 0, $startx + 14, ($starty - (".$bar1[$i]." * $pp)) + ($theight / 2), $white,'Vera.ttf', ".$bar1[$i].");";
		$lastx = $startx;
		$lasty = ($starty - ($bar1[$i] * $pp));
		$startx += $divider1;
	}
}

if ($showlabels == 1) {

$c = 0;
foreach ($boxa as $i) {
	$j = $texta[$c];
//	echo $i."<br>";
//	echo $j."<br>";
	eval(stripslashes($i));
	eval(stripslashes($j));
	$c++;
}

}

##### OUTPUT THE IMAGE AS A PNG IMAGE

header('Content-type: image/png');
imagepng($image);
imagedestroy($image);

}


/*

	##### Line Chart Function Parameter Examples

	##### X AXIS LABELS

	$xaxis = array(

		'Jan',
		'Feb',
		'Mar',
		'Apr',
		'May',
		'Jun',
	);

	##### EACH LINE PLOTTED IS IN AN ARRAY, AND EACH OF THESE ARRAYS ARE THEIR OWN INDIVIDUAL ITEM IN ANOTHER ARRAY

	$mylines = array(

		array('46','332','78','36','778','423'),
		array('189','439','1698','439','58','121'),
	);

	##### COLORS OF LINES

	$colors = array(

		'#333333',
		'#666666'.
		'#999999',
	);


	$w = 540; // WIDTH OF GRAPH IMAGE
	$h = 470; // HEIGHT OF GRAPH IMAGE
	$margin = 60; // TOP AND LEFT MARGIN IN PIXELS BETWEEN IMAGE EDGE AND BEGINNING OF GRAPH LINES
	$margin2 = 20; // TOP MARGIN IN PIXELS
	
	$showlabels = 1; // SET TO 0 TO HIDE LABELS
	
	$fontsize = 0; // SET TO 0 TO AUTOMATICALLY ADJUST

*/


##### BEGIN CODE
// VisuGraph 1.0
// �2009 Josh Abbott
// Not to be sold or distributed individually

function drawlinegraph($xaxis, $mylines, $colors, $showlabels=1, $fontsize=0, $w=540, $h=470, $margin=60, $margin2=20) {

##### CREATE OUR IMAGE, AND SET THE BASE COLORS WE WILL USE

$image = imagecreatetruecolor($w, $h);

$white = imagecolorallocate($image, 255, 255, 255);
$black = imagecolorallocate($image, 0, 0, 0);
$gray = imagecolorallocate($image, 185, 185, 185);
$lightgray = imagecolorallocate($image, 240, 240, 240);

imagefill($image, 0, 0, $white);

##### DRAW RECTANGLES ACROSS THE GRAPH IMAGE TO MAKE OUR 'VERTICAL' LINES IN THE GRAPH,
##### FILLED LIGHT GRAY ONES TO MAKE THE BACKGROUND COLOR AND HOLLOW DARK GRAY ONES TO MAKE THE LINES

$divider1 = round(($w - ($margin/3) - $margin2) / count($xaxis),0);
$startx = $margin;
$twidth = 1000;
$i = '0';

	if ($fontsize == 0) {
	$fsize = 10;
	while ($twidth > ($divider1 - 5)) {
		$bbox = imagettfbbox($fsize,'0',"Vera.ttf",$xaxis[$i]);
		$twidth = abs($bbox[2]-$bbox[0]);
		$fsize--;
	}
	} else {
	$fsize = $fontsize;
	}
	
for ($i=0;$i<(count($xaxis)-1);$i++) {
	imagefilledrectangle($image, $startx, $margin2, $startx + $divider1, ($h - $margin), $lightgray);
	imagerectangle($image, $startx, $margin2, $startx + $divider1, ($h - $margin), $gray);
	imagettftext($image,$fsize,0,$startx,($h - $margin + 20),$black,"Vera.ttf",$xaxis[$i]);
	$startx += $divider1;
	$k = $i;
}

$k++;
imagettftext($image,$fsize,0,$startx,($h - $margin + 20),$black,"Vera.ttf",$xaxis[$k]);

$fsize = 10;
while ($twidth > ($divider1 - 5)) {
	$bbox = imagettfbbox($fsize,'0',"Vera.ttf",$xaxis[$i]);
	$twidth = abs($bbox[2]-$bbox[0]);
	$fsize--;
}

##### FIGURE OUT THE LARGEST VALUE OF ALL THE NUMBERS TO BE PLOTTED IN ALL THE LINES

foreach ($mylines as $ii) {
	foreach ($ii as $i) {
		if ($largest < $i) {
			$largest = $i;
		}
	}
}

##### TAKE THE HIGHEST VALUE AND UP IT TO THE NEXT '100' VALUE TO MAKE TOP BAR OF GRAPH
$top = ceilpow1000($largest);

if ($top == 0) {
	$top = 1;
}

if ($largest < 20) {
	$top = 20;
}

##### NOW DRAW RECTANGLES DOWN THE GRAPH IMAGE TO MAKE OUR 'HORIZONTAL' LINES IN THE GRAPH

$divider2 = (($h - $margin - $margin2) / 10);
$starty = $margin2;
$textnum = $top;
$twidth = 1000;

	$fsize = 9;
	while ($twidth > ($margin - 5)) {
		$bbox = imagettfbbox($fsize,'0',"Vera.ttf",$textnum);
		$twidth = abs($bbox[2]-$bbox[0]);
		$fsize--;
	}
		
for ($i=0;$i<10;$i++) {
	imagerectangle($image, $margin, $starty, $startx, $starty + $divider2, $gray);
	imagettftext($image,$fsize,0,($margin - $twidth - 10),$starty + ($fsize/2),$black,"Vera.ttf",$textnum);
	$starty += $divider2;
	$textnum -= $top / 10;
}
imagettftext($image,$fsize,0,($margin - $twidth - 10),$starty + ($fsize / 2),$black,"Vera.ttf",'0');

##### TIME TO PLOT THE LINES ON THE GRAPH 

$l = 0;
foreach ($mylines as $line1) {
	$lastx = '';
	$lasty = '';
	$g = $starty - $margin2;
	$pp = $g / $top;
	$startx = $margin;
	$color = str_replace("#","",$colors[$l]);
	$c = hexrgb($color);
	$l++;
	for ($i=0;$i<(count($xaxis));$i++) {
	
	if ($showlabels == 1) {
		ImageFilledArc($image, $startx, ($starty - ($line1[$i] * $pp)), 10, 10, 0, 360, imagecolorallocate($image,$c['red'],$c['green'],$c['blue']), IMG_ARC_PIE);
	}
		
		if ($lastx && $lasty) ImageLine($image, $lastx, $lasty, $startx, ($starty - ($line1[$i] * $pp)), imagecolorallocate($image,$c['red'],$c['green'],$c['blue']));
	
	if ($showlabels == 1) {
		imagettftext($image, $fsize, 0, $startx + 10, (($starty - ($line1[$i] * $pp)) - ($fsize / 2)), imagecolorallocate($image,$c['red'],$c['green'],$c['blue']),"Vera.ttf", $line1[$i]);
	}
		
		$lastx = $startx;
		$lasty = ($starty - ($line1[$i] * $pp));
		$startx += $divider1;
	}

}

##### OUTPUT THE IMAGE AS A PNG IMAGE

header('Content-type: image/png');
imagepng($image);
imagedestroy($image);

}


############################
// VisuGraph 1.0
// �2009 Josh Abbott
// Not to be sold or distributed individually

function ceilpow1000($val) {
   return ceil(intval($val)/100)*100;
} 

function hexrgb($hexstr) {
    $int = hexdec($hexstr);
    return array("red" => 0xFF & ($int >> 0x10), "green" => 0xFF & ($int >> 0x8), "blue" => 0xFF & $int);
}

function countdays($firstdate, $lastdate) {
	$currdate = $firstdate;
	$daycount = 0;
	while ($currdate <= $lastdate) {
		$daycount++;
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
	}
	return $daycount;
}

?>