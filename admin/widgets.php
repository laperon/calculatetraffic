<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "../inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
if(!isset($_SESSION["adminid"])) { exit; };

function checkModExtension($file) {
	$fileExp = explode('.', $file);
	return $fileExp[count($fileExp) -1];
}

// Add/Edit a Widget
if (isset($_GET['addedit']) && isset($_GET['addedittype'])) {
	
	if (!isset($_SESSION["adminidnum"]) || !is_numeric($_SESSION["adminidnum"]) || $_SESSION["adminidnum"] != 0) {
		echo("You must be logged into the main Admin account to add, delete, or edit a Widget");
		exit;
	}
	
	$actionobj = "Widget";
	$tablename = "`".$prefix."widgets`";
	$directoryname = "widgets";
	
	if ($_POST['Submit'] == "Add" || $_POST['Submit'] == "Edit") {
		
		if (strlen($_POST['widgetfile']) > 0 && file_exists($directoryname."/".$_POST['widgetfile'])) {
			
			$_POST['col'] = "left";
			
			if (!isset($_POST['settingsfile']) || strlen($_POST['settingsfile']) < 1 || !file_exists($directoryname."/".$_POST['settingsfile'])) {
				$_POST['settingsfile'] = "None";
			}
			
			if (is_numeric($_GET['addedit'])) {
				// Edit existing entry
				lfmsql_query("UPDATE ".$tablename." SET name='".$_POST['name']."', descr='".$_POST['descr']."', widgetfile='".$_POST['widgetfile']."', settingsfile='".$_POST['settingsfile']."' WHERE id='".$_GET['addedit']."' AND candelete=1") or die(lfmsql_error());
			} else {
				// Add new entry
				$newrow = 0;
				lfmsql_query("INSERT INTO ".$tablename." (name, descr, widgetfile, settingsfile, candelete) VALUES ('".$_POST['name']."', '".$_POST['descr']."', '".$_POST['widgetfile']."', '".$_POST['settingsfile']."', 1)") or die(lfmsql_error());
				$newid = lfmsql_insert_id();
				lfmsql_query("INSERT INTO ".$prefix."widgetpositions (widgetid, adminid, row, col, enabled) VALUES ('".$newid."', '0', '".$newrow."', '".$_POST['col']."', 1)") or die(lfmsql_error());
				
				// Add to secondary admins
				$adminres = lfmsql_query("SELECT id FROM ".$prefix."admins");
				while ($adminrow=lfmsql_fetch_array($adminres)) {
					lfmsql_query("INSERT INTO ".$prefix."widgetpositions (widgetid, adminid, row, col, enabled) VALUES ('".$newid."', '".$adminrow["id"]."', '".$newrow."', '".$_POST['col']."', 1)") or die(lfmsql_error());
				}
				
				lfmsql_query("UPDATE ".$prefix."widgetpositions SET row=row+1 WHERE col='".$_POST['col']."'");
			}
		}
		
	} else {
		
		if (is_numeric($_GET['addedit'])) {
			$actiontype = "Edit";
			$getentry = lfmsql_query("SELECT name, descr, widgetfile, settingsfile FROM ".$tablename." WHERE id='".$_GET['addedit']."' AND candelete=1") or die(lfmsql_error());
			if (lfmsql_num_rows($getentry) > 0) {
				$modnameval = lfmsql_result($getentry, 0, "name");
				$moddescrval = lfmsql_result($getentry, 0, "descr");
				$widgetfileval = lfmsql_result($getentry, 0, "widgetfile");
				$settingsfileval = lfmsql_result($getentry, 0, "settingsfile");
			} else {
				echo("Module or Extension ID not found.");
				exit;
			}
		} else {
			$actiontype = "Add";
			$modnameval = "";
			$moddescrval = "";
			$widgetfileval = "None";
			$settingsfileval = "None";
		}
		
		?>
		<link href="styles.css" rel="stylesheet" type="text/css" />
		<table width="100%" border="0" cellspacing="0" cellpadding="4">
		  <tr>
		    <td><p>&nbsp;</p></td>
		  </tr>
		  <tr>
		    <td>

		<center>
		<p><font size="5"><b><? echo($actiontype." ".$actionobj); ?></b></font></p>
		
		<table border="0" cellpadding="0" cellspacing="0" width="450">
		<tr><td align="left">
			<p><font size="2"><b>Note:</b> This feature is intended for developers and advanced users with programming experience who want to create their own Widgets.  LFM does not provide technical support for custom Widgets.  Carefully read the guidelines below before adding a Widget.</font></p>
		</td></tr>
		</table>
		
		<form action="admin.php?f=widgets&addedit=<? echo($_GET['addedit']); ?>&addedittype=<? echo($_GET['addedittype']); ?>" method="post" name="widgets" id="widgets">
		<table width="300" align="center" cellpadding="4" cellspacing="0" class="lfmtable" style="border: 1px solid #999;">
		
		      <tr>
        		<td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><? echo($actionobj); ?> Name</strong></font></td>
		        <td colspan="2" class="formfield" align="left">
		        <input type="text" name="name" size="20" value="<? echo($modnameval); ?>">
		        </td>
		        <td align="center"><a href="#" title="A quick title for your own reference.">
				<img src="../images/question.jpg" width="15" height="15" border="0" />
				</a></td>
		      </tr>
		      
		      <tr>
        		<td nowrap="nowrap" class="button"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Description</strong></font></td>
		        <td colspan="2" class="formfield" align="left">
		        <textarea name="descr" cols="25" rows="4"><? echo($moddescrval); ?></textarea>
		        </td>
		        <td align="center"><a href="#" title="A description for your own reference.">
				<img src="../images/question.jpg" width="15" height="15" border="0" />
				</a></td>
		      </tr>
		      
    		  <tr>
		        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Main PHP File</font> </strong></td>
		        <td colspan="2" class="formfield" align="left">
		        <select name="widgetfile">
		        <option value="None"<? if($widgetfileval == "None"){echo(" selected");} ?>>-- Select File --</option>
		        	<?php
				$handle = opendir($directoryname);
				while(false != $handle and $fName = readdir($handle)) {
				  if (is_numeric($_GET['addedit'])) {
				   $checkadded = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$tablename." WHERE (widgetfile='".$fName."' OR settingsfile='".$fName."') AND id != '".$_GET['addedit']."'"), 0);
				  } else {
				    $checkadded = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$tablename." WHERE widgetfile='".$fName."' OR settingsfile='".$fName."'"), 0);
				  }
				   if($checkadded == 0) {
				      if ($fName != "index.php" && checkModExtension($fName) == "php") {
				      	echo("<option value=\"".$fName."\""); if($widgetfileval == $fName) { echo(" selected"); } echo(">".$fName."</option>");
				      }
				   }
				}
		        	?>
		        </select>
		        </td>
		        <td align="center"><a href="#" title="Upload the main Widget file to your <? echo($directoryname); ?> directory and then select it from the list."><img src="../images/question.jpg" width="15" height="15" border="0" />
			</a></td>
      		</tr>
		      
    		  <tr>
		        <td nowrap="nowrap" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Optional Settings File</font> </strong></td>
		        <td colspan="2" class="formfield" align="left">
		        <select name="settingsfile">
		        <option value="None"<? if($settingsfileval == "None"){echo(" selected");} ?>>None</option>
		        	<?php
				$handle = opendir($directoryname);
				while(false != $handle and $fName = readdir($handle)) {
				  if (is_numeric($_GET['addedit'])) {
				   $checkadded = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$tablename." WHERE (widgetfile='".$fName."' OR settingsfile='".$fName."') AND id != '".$_GET['addedit']."'"), 0);
				  } else {
				    $checkadded = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$tablename." WHERE widgetfile='".$fName."' OR settingsfile='".$fName."'"), 0);
				  }
				   if($checkadded == 0) {
				      if ($fName != "index.php" && checkModExtension($fName) == "php") {
				      	echo("<option value=\"".$fName."\""); if($settingsfileval == $fName) { echo(" selected"); } echo(">".$fName."</option>");
				      }
				   }
				}
		        	?>
		        </select>
		        </td>
		        <td align="center"><a href="#" title="An optional PHP file to allow users to change the settings of your Widget."><img src="../images/question.jpg" width="15" height="15" border="0" />
			</a></td>
      		</tr>
		      
		      <tr>
		        <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" value="<? echo($actiontype); ?>" /></td>
		        </tr>
		    </table>
		</form>
		
		<br><br>
		<table border="0" cellpadding="0" cellspacing="0" width="450">
		<tr><td align="left">
			<font size="2"><b>Widget Guidelines</b></font><br>
			<ul>
				<li><font size="2"><a href="widget_template.zip">Click Here</a> to download a sample Widget with a template for both the main PHP file and an optional settings file.<br></font></li>
				<li><font size="2">All Widget files should be uploaded to the /admin/widgets directory.  Use distinct file names such as johndoe_map_widget.php so your files are not accidentally overwritten by future updates or Widgets created by other developers.<br></font></li>
				<li><font size="2">All Widgets should be 400 pixels or less in width.<br></font></li>
				<li><font size="2">Widget output should be in the following format:</font> <xmp><div class="lfm_infobox" style="width: 400px;">
(Your Widget HTML)
</div>
<br><br></xmp>
					</li>
				<li><font size="2">Widgets should be self-contained and not alter the function or appearance of other widgets or other parts of the script.<br></font></li>
				<li><font size="2">Widgets should not create new database connections.  A connection will already be available when your main file or settings file is included by the script.  You should use the functions in /inc/lfmsql.php for making database queries.<br></font></li>
				<li><font size="2">Widgets with a settings file should post any form data to the URL admin.php?f=widgets&widgetsettings=settings.php replacing settings.php with the name of your file.  Your settings file will then be included by the script, and the data will be available in the $_POST array.<br></font></li>
			</ul>
		</td></tr>
		</table>
		
		<?
	exit;
	
	}
	
}


// Change a column
if (isset($_GET['widgetid']) && is_numeric($_GET['widgetid']) && isset($_GET['widgetcol']) && ($_GET['widgetcol'] == "left" || $_GET['widgetcol'] == "center" || $_GET['widgetcol'] == "right")) {
	
	$widgetid = $_GET['widgetid'];
	$col = $_GET['widgetcol'];
	
	$getoldrow = lfmsql_query("SELECT row, col FROM `".$prefix."widgetpositions` WHERE adminid='".$_SESSION["adminidnum"]."' AND widgetid=".$widgetid) or die(lfmsql_error());
	if (lfmsql_num_rows($getoldrow) > 0) {
		$oldrow = lfmsql_result($getoldrow, 0, "row");
		$oldcol = lfmsql_result($getoldrow, 0, "col");
		if ($oldcol != $col) {
			$gethighestrow = lfmsql_query("SELECT row FROM `".$prefix."widgetpositions` WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$col."' ORDER BY row DESC LIMIT 1") or die(lfmsql_error());
			if (lfmsql_num_rows($gethighestrow) > 0) {
				$highestrow = lfmsql_result($gethighestrow, 0, "row");
			} else {
				$highestrow = 0;
			}
			if ($oldrow > $highestrow) {
				// Insert the Widget at the end of the new column
				$newrow = $highestrow + 1;
			} else {
				// Keep the Widget in its current row and move everything else down in the new column
				$newrow = $oldrow;
				lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=row+1 WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$col."' AND row >= ".$newrow) or die(lfmsql_error());
			}
			
			// Move the Widget
			lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=".$newrow.", col='".$col."' WHERE adminid='".$_SESSION["adminidnum"]."' AND widgetid=".$widgetid) or die(lfmsql_error());
			
			// Move the Widgets up in the old column to fill the gap left by the one that was moved
			lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=row-1 WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$oldcol."' AND row > ".$oldrow) or die(lfmsql_error());
			
		}
	}
}

// Change a row
if (isset($_GET['widgetid']) && is_numeric($_GET['widgetid']) && isset($_GET['extrow']) && ($_GET['extrow'] == "up" || $_GET['extrow'] == "down")) {
	
	$widgetid = $_GET['widgetid'];
	$direction = $_GET['extrow'];
	
	$getwidget = lfmsql_query("SELECT row, col FROM `".$prefix."widgetpositions` WHERE adminid='".$_SESSION["adminidnum"]."' AND widgetid=".$widgetid) or die(lfmsql_error());
	if (lfmsql_num_rows($getwidget) > 0) {
		
		$oldrow = lfmsql_result($getwidget, 0, "row");
		$col = lfmsql_result($getwidget, 0, "col");
		
		$runchange = "yes";
		
		$checkfirstrow = lfmsql_query("SELECT row FROM `".$prefix."widgetpositions` WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$col."' ORDER BY row ASC") or die(lfmsql_error());
		$firstrow = lfmsql_result($checkfirstrow, 0, "row");
		
		$checklastrow = lfmsql_query("SELECT row FROM `".$prefix."widgetpositions` WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$col."' ORDER BY row DESC") or die(lfmsql_error());
		$lastrow = lfmsql_result($checklastrow, 0, "row");
		
		if (($direction=="up") && ($oldrow > $firstrow)) {
			$change = $oldrow-1;
		} elseif (($direction=="down") && ($oldrow < $lastrow)) {
			$change = $oldrow+1;
		} else {
			$runchange = "no";
		}
		
		if ($runchange == "yes") {
			lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=0 WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$col."' AND row=".$oldrow) or die(lfmsql_error());
			lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=$oldrow WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$col."' AND row=".$change) or die(lfmsql_error());
			lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=$change WHERE adminid='".$_SESSION["adminidnum"]."' AND col='".$col."' AND row=0") or die(lfmsql_error());
		}
	}
}

// Change a Widget state
if (isset($_GET['widgetstate']) && is_numeric($_GET['widgetstate']) && isset($_GET['widgetid']) && is_numeric($_GET['widgetid'])) {
	if ($_GET['widgetstate'] == 1) {
		lfmsql_query("UPDATE `".$prefix."widgetpositions` SET enabled='1' WHERE adminid='".$_SESSION["adminidnum"]."' AND widgetid='".$_GET['widgetid']."'") or die(lfmsql_error());
	} elseif ($_GET['widgetstate'] == 0) {
		lfmsql_query("UPDATE `".$prefix."widgetpositions` SET enabled='0' WHERE adminid='".$_SESSION["adminidnum"]."' AND widgetid='".$_GET['widgetid']."'") or die(lfmsql_error());
	}
}

// Delete a Widget
if (isset($_GET['deletewidget']) && is_numeric($_GET['deletewidget'])) {
	
	if (!isset($_SESSION["adminidnum"]) || !is_numeric($_SESSION["adminidnum"]) || $_SESSION["adminidnum"] != 0) {
		echo("You must be logged into the main Admin account to add, delete, or edit a Widget");
		exit;
	}
	
	$getwidget = lfmsql_query("SELECT name FROM `".$prefix."widgets` WHERE id='".$_GET['deletewidget']."' AND candelete=1") or die(lfmsql_error());
	if (lfmsql_num_rows($getwidget) > 0) {
		
		$widgetname = lfmsql_result($getwidget, 0, "name");
		
		if(isset($_GET['confirmdel']) && $_GET['confirmdel'] == "yes") {
			lfmsql_query("DELETE FROM `".$prefix."widgets` WHERE id='".$_GET['deletewidget']."' AND candelete=1") or die(lfmsql_error());
			
			// Delete main admin positions
			$getwidgetpos = lfmsql_query("SELECT row, col FROM `".$prefix."widgetpositions` WHERE adminid='0' AND widgetid='".$_GET['deletewidget']."'") or die(lfmsql_error());
			if (lfmsql_num_rows($getwidgetpos) > 0) {
				$widgetrow = lfmsql_result($getwidgetpos, 0, "row");
				$widgetcol = lfmsql_result($getwidgetpos, 0, "col");
				lfmsql_query("DELETE FROM `".$prefix."widgetpositions` WHERE adminid='0' AND widgetid='".$_GET['deletewidget']."'") or die(lfmsql_error());
				lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=row-1 WHERE adminid='0' AND col='".$widgetcol."' AND  row > '".$widgetrow."'");
			}
			
			// Delete secondary admin positions
			$adminres = lfmsql_query("SELECT id FROM ".$prefix."admins");
			while ($adminrow=lfmsql_fetch_array($adminres)) {
				$getwidgetpos = lfmsql_query("SELECT row, col FROM `".$prefix."widgetpositions` WHERE adminid='".$adminrow["id"]."' AND widgetid='".$_GET['deletewidget']."'") or die(lfmsql_error());
				if (lfmsql_num_rows($getwidgetpos) > 0) {
					$widgetrow = lfmsql_result($getwidgetpos, 0, "row");
					$widgetcol = lfmsql_result($getwidgetpos, 0, "col");
					lfmsql_query("DELETE FROM `".$prefix."widgetpositions` WHERE adminid='".$adminrow["id"]."' AND widgetid='".$_GET['deletewidget']."'") or die(lfmsql_error());
					lfmsql_query("UPDATE `".$prefix."widgetpositions` SET row=row-1 WHERE adminid='".$adminrow["id"]."' AND col='".$widgetcol."' AND  row > '".$widgetrow."'");
				}
				
			}
		} else {
			echo("<center>
			<p><font size=3>Are you sure you want to delete this Widget from your Admin Panel?</font></p>
			<p><font size=4><b>".$widgetname."</b></p>
			<p><font size=3><a href=\"admin.php?f=widgets&deletewidget=".$_GET['deletewidget']."&confirmdel=yes\">Yes</a></font></p>
			<p><font size=3><a href=\"admin.php?f=widgets\">No</a></font></p>
			</center>");
			exit;
		}

	}
}

// Widget Settings
if (isset($_GET['widgetsettings']) && strlen($_GET['widgetsettings']) > 0) {
	if (stripos($_GET['widgetsettings'], '..') !== false || stripos($_GET['widgetsettings'], '/') !== false) {
		echo("Widget files must be in the widgets directory");
		exit;
	}
	$checkwidget = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."widgets` WHERE settingsfile='".$_GET['widgetsettings']."'"), 0);
	if ($checkwidget > 0 && file_exists("widgets/".$_GET['widgetsettings'])) {
		include "widgets/".$_GET['widgetsettings'];
		exit;
	} else {
		echo("Could not validate settings file");
		exit;
	}
}

?>
<link href="styles.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><p>&nbsp;</p></td>
  </tr>
  <tr>
    <td>

<center>

<div class="lfm_infobox" style="width: 600px;">
<table width="600" border="0" cellpadding="2">
  <tr>
    <td align="center"><div class="lfm_infobox_heading">Widgets</div></td>
  </tr>
  
  <tr>
    <td align="left"><div class="lfm_descr">
    	
    	<br>Widgets are the mini-apps that appear on the homepage of your Admin Panel.  You can switch each Widget on or off, and move them to different positions.  Widgets are placed into two columns by default, but if you have a high-resolution monitor, you can move Widgets into a third column to take advantage of the extra space.
    	<br><br>
    	If logged into the main Admin account, you can also create and add your own Widgets.
    	
    </div></td>
  </tr>
      
</table>
</div>

<br><br>

<?

$getleftwidgets = lfmsql_query("SELECT a.id AS id, b.enabled AS enabled, a.name AS name, a.descr AS descr, a.candelete AS candelete, a.settingsfile AS settingsfile FROM ".$prefix."widgets a LEFT JOIN ".$prefix."widgetpositions b ON (a.id=b.widgetid) WHERE b.adminid='".$_SESSION["adminidnum"]."' AND b.col='left' ORDER BY b.row ASC");

$getcenterwidgets = lfmsql_query("SELECT a.id AS id, b.enabled AS enabled, a.name AS name, a.descr AS descr, a.candelete AS candelete, a.settingsfile AS settingsfile FROM ".$prefix."widgets a LEFT JOIN ".$prefix."widgetpositions b ON (a.id=b.widgetid) WHERE b.adminid='".$_SESSION["adminidnum"]."' AND b.col='center' ORDER BY b.row ASC");

$getrightwidgets = lfmsql_query("SELECT a.id AS id, b.enabled AS enabled, a.name AS name, a.descr AS descr, a.candelete AS candelete, a.settingsfile AS settingsfile FROM ".$prefix."widgets a LEFT JOIN ".$prefix."widgetpositions b ON (a.id=b.widgetid) WHERE b.adminid='".$_SESSION["adminidnum"]."' AND b.col='right' ORDER BY b.row ASC");

$widgetcounts = array();

$widgetcounts[] = $leftwidgetscount = lfmsql_num_rows($getleftwidgets);
$widgetcounts[] = $centerwidgetscount = lfmsql_num_rows($getcenterwidgets);
$widgetcounts[] = $rightwidgetscount = lfmsql_num_rows($getrightwidgets);

$widgetrows = max($widgetcounts);

echo('<table border="0" cellpadding="0" cellspacing="20">');

for ($i = 0; $i < $widgetrows; $i++) {
	
	echo('<tr>');
	
	// List left column widgets
	if ($leftwidgetscount > $i) {
		$widgetid = lfmsql_result($getleftwidgets, $i, "id");
		$widgetenabled = lfmsql_result($getleftwidgets, $i, "enabled");
		$widgetname = lfmsql_result($getleftwidgets, $i, "name");
		$widgetdescr = lfmsql_result($getleftwidgets, $i, "descr");
		$widgetcandelete = lfmsql_result($getleftwidgets, $i, "candelete");
		$widgetsettingsfile = lfmsql_result($getleftwidgets, $i, "settingsfile");
		
		if ($widgetenabled == 1) {
			$tdbg = "#73fb76";
		} else {
			$tdbg = "#DDDDDD";
		}
		
		if ($widgetenabled == 1) {
			$widgetoptions = "<br><br><a href=admin.php?f=widgets&widgetstate=0&widgetid=".$widgetid."><font size=3><b>Click To Disable</b></font></a>";
		} else {
			$widgetoptions = "<br><br><a href=admin.php?f=widgets&widgetstate=1&widgetid=".$widgetid."><font size=3><b>Click To Enable</b></font></a>";
		}
		
		if (strlen($widgetsettingsfile) > 0 && $widgetsettingsfile != "None") {
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&widgetsettings=".$widgetsettingsfile."><font size=3><b>Widget Settings</b></font></a>";
		}
		
		if ($widgetcandelete == 1 && isset($_SESSION["adminidnum"]) && is_numeric($_SESSION["adminidnum"]) && $_SESSION["adminidnum"] == 0) {
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&addedit=".$widgetid."&addedittype=widget><font size=3><b>Edit</b></font></a>";
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&deletewidget=".$widgetid."><font size=3 color=darkred><b>Click To DELETE</b></font></a>";
		}
		
		echo("<td width=\"350\" align=\"left\" valign=\"top\">
			<table height=\"250\" style=\"border-style:solid; border-color:black; border-width:2px;\" cellpadding=\"5\" cellspacing=\"0\">
			<tr><td colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\"><a href=admin.php?f=widgets&extrow=up&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_up.jpg\"></a></td></tr>
			<tr>
			<td width=\"30\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				&nbsp;
			</td>
			<td width=\"290\" align=\"left\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				<center>
				<font size=3><b>".$widgetname."</b></font>
				<br>
				</center>
				<font size=2>".$widgetdescr."</font>
				<center>
				".$widgetoptions."
				</center>
			</td>
			<td width=\"30\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				<a href=admin.php?f=widgets&widgetcol=center&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_right.jpg\"></a>
			</td>
			</tr>
			<tr><td colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\"><a href=admin.php?f=widgets&extrow=down&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_down.jpg\"></a></td></tr>
			</table>
		</td>");
		
	} else {
		echo('<td>&nbsp;</td>');
	}
	
	// List center column widgets
	if ($centerwidgetscount > $i) {
		$widgetid = lfmsql_result($getcenterwidgets, $i, "id");
		$widgetenabled = lfmsql_result($getcenterwidgets, $i, "enabled");
		$widgetname = lfmsql_result($getcenterwidgets, $i, "name");
		$widgetdescr = lfmsql_result($getcenterwidgets, $i, "descr");
		$widgetcandelete = lfmsql_result($getcenterwidgets, $i, "candelete");
		$widgetsettingsfile = lfmsql_result($getcenterwidgets, $i, "settingsfile");
		
		if ($widgetenabled == 1) {
			$tdbg = "#73fb76";
		} else {
			$tdbg = "#DDDDDD";
		}
		
		if ($widgetenabled == 1) {
			$widgetoptions = "<br><br><a href=admin.php?f=widgets&widgetstate=0&widgetid=".$widgetid."><font size=3><b>Click To Disable</b></font></a>";
		} else {
			$widgetoptions = "<br><br><a href=admin.php?f=widgets&widgetstate=1&widgetid=".$widgetid."><font size=3><b>Click To Enable</b></font></a>";
		}
		
		if (strlen($widgetsettingsfile) > 0 && $widgetsettingsfile != "None") {
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&widgetsettings=".$widgetsettingsfile."><font size=3><b>Widget Settings</b></font></a>";
		}
		
		if ($widgetcandelete == 1 && isset($_SESSION["adminidnum"]) && is_numeric($_SESSION["adminidnum"]) && $_SESSION["adminidnum"] == 0) {
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&addedit=".$widgetid."&addedittype=widget><font size=3><b>Edit</b></font></a>";
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&deletewidget=".$widgetid."><font size=3 color=darkred><b>Click To DELETE</b></font></a>";
		}
		
		echo("<td width=\"350\" align=\"left\" valign=\"top\">
			<table height=\"250\" style=\"border-style:solid; border-color:black; border-width:2px;\" cellpadding=\"5\" cellspacing=\"0\">
			<tr><td colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\"><a href=admin.php?f=widgets&extrow=up&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_up.jpg\"></a></td></tr>
			<tr>
			<td width=\"30\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				<a href=admin.php?f=widgets&widgetcol=left&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_left.jpg\"></a>
			</td>
			<td width=\"290\" align=\"left\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				<center>
				<font size=3><b>".$widgetname."</b></font>
				<br>
				</center>
				<font size=2>".$widgetdescr."</font>
				<center>
				".$widgetoptions."
				</center>
			</td>
			<td width=\"30\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				<a href=admin.php?f=widgets&widgetcol=right&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_right.jpg\"></a>
			</td>
			</tr>
			<tr><td colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\"><a href=admin.php?f=widgets&extrow=down&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_down.jpg\"></a></td></tr>
			</table>
		</td>");
		
	} else {
		echo('<td>&nbsp;</td>');
	}
	
	// List right column widgets
	if ($rightwidgetscount > $i) {
		$widgetid = lfmsql_result($getrightwidgets, $i, "id");
		$widgetenabled = lfmsql_result($getrightwidgets, $i, "enabled");
		$widgetname = lfmsql_result($getrightwidgets, $i, "name");
		$widgetdescr = lfmsql_result($getrightwidgets, $i, "descr");
		$widgetcandelete = lfmsql_result($getrightwidgets, $i, "candelete");
		$widgetsettingsfile = lfmsql_result($getrightwidgets, $i, "settingsfile");
		
		if ($widgetenabled == 1) {
			$tdbg = "#73fb76";
		} else {
			$tdbg = "#DDDDDD";
		}
		
		if ($widgetenabled == 1) {
			$widgetoptions = "<br><br><a href=admin.php?f=widgets&widgetstate=0&widgetid=".$widgetid."><font size=3><b>Click To Disable</b></font></a>";
		} else {
			$widgetoptions = "<br><br><a href=admin.php?f=widgets&widgetstate=1&widgetid=".$widgetid."><font size=3><b>Click To Enable</b></font></a>";
		}
		
		if (strlen($widgetsettingsfile) > 0 && $widgetsettingsfile != "None") {
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&widgetsettings=".$widgetsettingsfile."><font size=3><b>Widget Settings</b></font></a>";
		}
		
		if ($widgetcandelete == 1 && isset($_SESSION["adminidnum"]) && is_numeric($_SESSION["adminidnum"]) && $_SESSION["adminidnum"] == 0) {
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&addedit=".$widgetid."&addedittype=widget><font size=3><b>Edit</b></font></a>";
			$widgetoptions .= "<br><br><a href=admin.php?f=widgets&deletewidget=".$widgetid."><font size=3 color=darkred><b>Click To DELETE</b></font></a>";
		}
		
		echo("<td width=\"350\" align=\"left\" valign=\"top\">
			<table height=\"250\" style=\"border-style:solid; border-color:black; border-width:2px;\" cellpadding=\"5\" cellspacing=\"0\">
			<tr><td colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\"><a href=admin.php?f=widgets&extrow=up&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_up.jpg\"></a></td></tr>
			<tr>
			<td width=\"30\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				<a href=admin.php?f=widgets&widgetcol=center&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_left.jpg\"></a>
			</td>
			<td width=\"290\" align=\"left\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				<center>
				<font size=3><b>".$widgetname."</b></font>
				<br>
				</center>
				<font size=2>".$widgetdescr."</font>
				<center>
				".$widgetoptions."
				</center>
			</td>
			<td width=\"30\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\">
				&nbsp;
			</td>
			</tr>
			<tr><td colspan=\"3\" align=\"center\" valign=\"middle\" bgcolor=\"".$tdbg."\"><a href=admin.php?f=widgets&extrow=down&widgetid=".$widgetid."><img height=\"30\" width=\"30\" border=\"0\" src=\"widget_down.jpg\"></a></td></tr>
			</table>
		</td>");
		
	} else {
		echo('<td>&nbsp;</td>');
	}
	
	echo('</tr>');
}

echo('</table>');

if (isset($_SESSION["adminidnum"]) && is_numeric($_SESSION["adminidnum"]) && $_SESSION["adminidnum"] == 0) {
echo('<br>
<p><a href="admin.php?f=widgets&addedit=new&addedittype=widget"><font size="4">Add A New Widget</font></a></p>
<br>');
}

?>