<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright ©2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_SESSION["adminid"])) { exit; };

    // Get some stats
    $mres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members");
    $mrow=@lfmsql_fetch_array($mres);
    
    // Count members who are verified and subscribed
    $mactres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Active' AND newsletter=1");
    $mactrow=@lfmsql_fetch_array($mactres);
    
    // Count members who are not yet verified
    $munvres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Unverified'");
    $munvrow=@lfmsql_fetch_array($munvres);
    
     // Count members who were verified but have unsubscribed
    $munsres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Active' AND newsletter=0");
    $munsrow=@lfmsql_fetch_array($munsres);
    
     // Count suspended members
    $msusres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Suspended'");
    $msusrow=@lfmsql_fetch_array($msusres);
    
    // Count members who have a bouncing email address
    $mbounceres=@lfmsql_query("SELECT count(*) as mcount FROM ".$prefix."members WHERE status='Active' AND bouncemode=3");
    $mbouncerow=@lfmsql_fetch_array($mbounceres);

?>

<!-- Start Members Dashboard -->
<div class="lfm_infobox" style="width: 400px;">
<table width="400" border="0" cellpadding="1">
  <tr>
    <td colspan="2" align="center"><div class="lfm_infobox_heading">Member Dashboard</div><br></td>
  </tr>

    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Total Members (<a target="_self" href="admin.php?f=mm&sf=browse">View</a>): </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=$mrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>

    <tr>
      <td align="right" class="lfm_infobox_item">Subscribed Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=subscribed">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$mactrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Unverified Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=unverified">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$munvrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Suspended Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=suspended">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$msusrow["mcount"];?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Unsubscribed Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=unsubscribed">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$munsrow["mcount"];?></td>
    </tr>
    
    <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
    
    <tr>
      <td align="right" class="lfm_infobox_item">Bounced Mail Members (<a target="_self" href="admin.php?f=mm&sf=browse&show=bounced">View</a>): </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=$mbouncerow["mcount"];?></td>
    </tr>

</table>
</div>
<!-- End Members Dashboard -->

<br><br>