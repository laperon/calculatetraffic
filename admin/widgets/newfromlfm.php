<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright ©2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_SESSION["adminid"])) { exit; };

function getNewFromLFM($processurl, $getdata = null) {
	
	$processurl = $processurl."?widget=1";
	
	foreach ($getdata as $key=>$value) {
		$processurl = $processurl."&".$key."=".$value;
	}
	
	$urlinfo = parse_url($processurl);
	$hostname = $urlinfo['host'];
	if ($hostname == "" || gethostbyname($hostname) == $hostname) {
		return "";
	} else {
		$port = 80;
		$timeout = 5;
		$headers = "GET ".$urlinfo['path'].'?'.$urlinfo['query']." HTTP/1.1\r\n";
		$headers .= "Host: ".$hostname."\r\n";
		$headers .= "Referer: http://".$_SERVER["SERVER_NAME"]."\r\n";
		$headers .= "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)\r\n";
		$headers .= "Connection: close\r\n";
		$headers .= "Accept: */*\r\n";
		$headers .= "\r\n";
		$checkreturn = "";
		$attempts = 0;
		while((strpos($checkreturn, "200 OK") === false) && (strpos($checkreturn, "302 Found") === false) && ($attempts<2)) {
			$connection = fsockopen($hostname, $port, $errno, $errstr, $timeout);
			if ($connection) {
				stream_set_timeout($connection, $timeout);
				fwrite($connection,$headers);
				//Read the reply
				$checkreturn = "";
				$readruns = 0;
				while (!feof($connection) && !$checktimeout['timed_out'] && $readruns < 50) {
					$checkreturn .= fread($connection, 10000);
					$checktimeout = stream_get_meta_data($connection);
					$readruns++;
				}
				fclose($connection);
			}
			$attempts = $attempts+1;
		}
		
		if((strpos($checkreturn, "200 OK") === false) && (strpos($checkreturn, "302 Found") === false)) {
			return "";
		} elseif (strpos($checkreturn, "NEWLFM:") === false) {
			return "";
		} else {
			$colonpos = strpos($checkreturn, "NEWLFM:");
			$colonpos = $colonpos+7;
			$newresult = trim(substr($checkreturn, $colonpos));
			
			$packressplit = explode(':ENDNEWLFM', $newresult);
			$newresult = trim($packressplit[0]);
			
			return $newresult;
		}
	}
}

$keyres = lfmsql_query("SELECT license FROM ".$prefix."admin");
$lfmkey = lfmsql_result($keyres, 0);
$lfmvers = get_script_vers();

$data = array
(
	'lfmkey' => $lfmkey,
	'lfmvers' => $lfmvers
);

$news_info = getNewFromLFM('http://thetrafficexchangescript.com/newfromlfm.php', $data);

if (strlen($news_info) > 0) {
	// Blocks script injection attempts
	$news_info_check = preg_replace('/\s+/', '', html_entity_decode($news_info));
	if (stripos($news_info_check, '<script') !== false || stripos($news_info_check, '<object') !== false || stripos($news_info_check, '<embed') !== false || stripos($news_info_check, '<iframe') !== false || stripos($news_info_check, '<form') !== false || stripos($news_info_check, 'javascript:') !== false || stripos($news_info_check, 'http-equiv') !== false || stripos($news_info_check, 'eval(') !== false || stripos($news_info_check, 'unescape(') !== false || stripos($news_info_check, 'decodeURI') !== false || stripos($news_info_check, 'onload') !== false || stripos($news_info_check, 'onunload') !== false || stripos($news_info_check, 'getElement') !== false || stripos($news_info_check, '.navigate') !== false || stripos($news_info_check, '.href') !== false || stripos($news_info_check, '.submit') !== false || stripos($news_info_check, '.location') !== false || stripos($news_info_check, '.innerhtml') !== false || stripos($news_info_check, '.write') !== false || stripos($news_info_check, 'document.') !== false || stripos($news_info_check, 'window.') !== false || stripos($news_info_check, 'self.') !== false || stripos($news_info_check, 'top.') !== false || stripos($news_info_check, 'parent.') !== false) {
		$news_info = "";
	}
}

if (strlen($news_info) > 0) {

echo('
<!-- Start New From LFM -->
<div class="lfm_infobox" style="width: 400px;">

'.$news_info.'

</div>
<!-- End New From LFM -->

<br><br>');

}

?>