<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright ©2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_SESSION["adminid"])) { exit; };

    // Get some stats
    $sres=@lfmsql_query("SELECT sum(itemamount) as tsales FROM ".$prefix."sales where salestier=1");
    $srow=@lfmsql_fetch_array($sres);
    $rres=@lfmsql_query("SELECT sum(itemamount) as trefunds FROM ".$prefix."sales where salestier=1 and status = 'R'");
    $rrow=@lfmsql_fetch_array($rres);
    $pres=@lfmsql_query("SELECT sum(commission) as tcommspaid FROM ".$prefix."sales WHERE status = 'P'");
    $prow=@lfmsql_fetch_array($pres);
    $cres=@lfmsql_query("SELECT sum(commission) as tcomms FROM ".$prefix."sales WHERE status IS NULL");
    $crow=@lfmsql_fetch_array($cres);
    
    $profit = $srow["tsales"]-($rrow["trefunds"]+$prow["tcommspaid"]+$crow["tcomms"]);

?>

<!-- Start Sales Dashboard -->
<div class="lfm_infobox" style="width: 400px;">
<table width="400" border="0" cellpadding="1">
  <tr>
    <td colspan="2" align="center"><div class="lfm_infobox_heading">Sales Dashboard</div><div class="lfm_descr"><a href="admin.php?f=ipnsales&processor=salesglance">View SalesGlance</a></div><br></td>
  </tr>

    <tr>
      <td align="right" width="75%" class="lfm_infobox_item">Total Sales: </td>
      <td align="left" width="25%" class="lfm_infobox_itemvalue"><?=number_format($srow["tsales"],2);?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Total Refunds: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($rrow["trefunds"],2);?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Comm. Paid: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($prow["tcommspaid"],2);?></td>
      </tr>
      
      <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
      
    <tr>
      <td align="right" class="lfm_infobox_item">Comm. Owing: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($crow["tcomms"],2);?></td>
    </tr>
    
    <tr><td colspan="2" class="lfm_infobox_linebreak"><hr></td></tr>
    
    <tr>
      <td align="right" class="lfm_infobox_item">Profit: </td>
      <td align="left" class="lfm_infobox_itemvalue"><?=number_format($profit,2);?></td>
    </tr>

</table>
</div>
<!-- End Sales Dashboard -->

<br><br>