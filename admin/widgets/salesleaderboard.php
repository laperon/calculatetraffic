<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright ©2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_SESSION["adminid"])) { exit; };

?>

<!-- Start Sales Leaderboard -->
<div class="lfm_infobox" style="width: 400px;">
<form method="post" action="admin.php">
  <table border="0" cellpadding="0" cellspacing="0">
  
  <tr>
    <td colspan="4" align="center"><div class="lfm_infobox_heading">Affiliate Leaderboard</div><br><br></td>
  </tr>
  
    <tr>
      <td colspan="4" align="center" nowrap="nowrap"><table width="100%" border="0" class="lfmtable">
<?    if(isset($_POST["sdate"]) && isset($_POST["edate"]))
    {
?>
        <tr>
          <td nowrap="nowrap">From:
            <input name="sdate" type id="DPC_sdate_YYYY-MM-DD" value="<?=$_POST['sdate'];?>" width="80" text>            </td>
          <td nowrap="nowrap">To:
            <input name="edate" type id="DPC_edate_YYYY-MM-DD" value="<?=$_POST['edate'];?>" width="80" text>            </td>
        </tr>
<?
    }
    else
    {
?>
        <tr>
          <td nowrap="nowrap">From:
            <input name="sdate" type="text" id="DPC_sdate_YYYY-MM-DD" value="<?=date("Y-m-d",mktime(0,0,0,date("m")-1,date("d"),date("Y")));?>" width="80">            </td>
          <td nowrap="nowrap">To:
            <input name="edate" type="text" id="DPC_edate_YYYY-MM-DD" value="<?=date("Y-m-d");?>" width="80">            </td>
        </tr>

<?
    }
    
    if (is_numeric($_POST['showtop']) && $_POST['showtop'] > 0) {
    	$showtop = $_POST['showtop'];
    } else {
    	$showtop = 10;
    }
    
?>
        <tr>
          <td colspan="2" align="center">Show the top <input name="showtop" type="text" size="3" value="<?=$showtop?>"> sellers.
          </td>
        </tr>
        
      </table></td>
      </tr>
    <tr>
      <td colspan="4" align="center" nowrap="nowrap"><input name="Submit" type="submit" class="footer-text" id="Submit" value="Update Leaderboard" /></td>
      </tr>
    <tr class="button">
      <td align="center" nowrap="nowrap"><strong>#</strong></td>
      <td nowrap="nowrap"><strong>Name</strong></td>
      <td align="center" nowrap="nowrap"><strong>Number of  Sales </strong></td>
      <td align="center" nowrap="nowrap"><strong> Commission </strong></td>
      </tr>
<?
// Get sales data for all affiliates
if(isset($_POST["sdate"]))
{
    $sdate = $_POST["sdate"]." 00:00:00";
    $edate = $_POST["edate"]." 23:59:59";
}
else
{
    $sdate = date("Y-m-d",mktime(0,0,0,date("m")-1,date("d"),date("Y")))." 00:00:00";
    $edate = date("Y-m-d",mktime(23,59,59,date("m"),date("d"),date("Y")))." 23:59:59";
}
$lbqry="SELECT firstname,lastname,affid,count(*) as salecount,sum(s.commission) as comm FROM ".$prefix."sales s LEFT JOIN ".$prefix."members m on m.id=s.affid WHERE saledate > '$sdate' AND saledate < '$edate' AND prize=0 GROUP BY affid ORDER BY comm DESC LIMIT ".$showtop;
$lbres=@lfmsql_query($lbqry);
$affpos=0;                    // Position in the leaderboard
$totalsales=0;                // Overall total sales ($)
$totalnumsales=0;            // Overall total number of sales

while($lbrow=@lfmsql_fetch_array($lbres))
{
    $totalsales=$totalsales+$lbrow["comm"];
    $totalnumsales=$totalnumsales+$lbrow["salecount"];
    $affpos=$affpos+1;
	$affid=$lbrow["affid"];
?>
    <tr>
      <td align="center"><strong><?=$affpos;?></strong></td>
      <td><?=$lbrow["firstname"];?>&nbsp;<?=$lbrow["lastname"];?></td>
      <td align="center"><?=$lbrow["salecount"];?></td>
      <td align="center"><?=$lbrow["comm"];?></td>
      </tr>
<?
}
?>
    <tr>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap"><strong>Total Sales: <?=$totalnumsales;?></strong></td>
      <td nowrap="nowrap"><strong>Total Commissions: $<?=$totalsales;?></strong></td>
      </tr>
  </table>
</form>
</div>
<!-- End Sales Leaderboard -->

<br><br>