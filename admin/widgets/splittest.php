<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright ©2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_SESSION["adminid"])) { exit; };

$sres=@lfmsql_query("SELECT splittest FROM ".$prefix."settings");
$srow=@lfmsql_fetch_array($sres);
$splittest = $srow["splittest"];

if ($splittest > 1) {

?>

<!-- Start Split Test Stats -->
<div class="lfm_infobox" style="width: 400px;">
<table width="400" border="0" align="center" cellpadding="0" cellspacing="0">
	
	<tr>
		<td colspan="4" align="center"><div class="lfm_infobox_heading">Split Test Stats</div><br><br></td>
	</tr>
	
        <tr>
          <td align="left" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Sales Page </font></strong></td>
          <td align="center" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Hits</font></strong></td>
          <td align="center" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Joins</font></strong></td>
          <td align="center" class="button"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">C/R</font></strong></td>
        </tr>
<?
	$sph=@lfmsql_query("SELECT count(salespage) AS salescount, salespage FROM ".$prefix."splithits GROUP BY salespage ORDER BY salespage");
	while($splithits=@lfmsql_fetch_object($sph))
	{
		// Get joins for this page
		$spr=@lfmsql_query("SELECT count(salespage) as salescount FROM ".$prefix."splitresults WHERE salespage='$splithits->salespage'");

		$splitresult=@lfmsql_fetch_object($spr);
		$crate=($splitresult->salescount/$splithits->salescount)*100;
		$crpc=number_format($crate,2);
?>
	     <tr>
          <td align="left"><?=$splithits->salespage;?></td>
          <td align="center"><?=$splithits->salescount;?></td>
          <td align="center"><?=$splitresult->salescount;?></td>
          <td align="center"><?=$crpc;?>%</td>
        </tr>
<? } ?>
      </table>
</div>
<!-- End Split Test Stats -->

<br><br>

<?
}
?>