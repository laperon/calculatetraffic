<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_SESSION["adminid"])) { exit; };

$itemslist = array();

$itemslist[] = "Surfing Now";
$itemslist[] = "Surfed This Hour";
$itemslist[] = "Surfed Today";
$itemslist[] = "Surfed This Week";
$itemslist[] = "Open Abuse Reports";
$itemslist[] = "Sites In Rotation";
$itemslist[] = "Banners In Rotation";
$itemslist[] = "Texts Ads In Rotation";
$itemslist[] = "Signups Today";
$itemslist[] = "Signups Yesterday";
$itemslist[] = "Signups This Week";
$itemslist[] = "Credits Earned Today";
$itemslist[] = "Hits Delivered Today";
$itemslist[] = "Hits Delivered Per URL Today";
$itemslist[] = "Imps Delivered Per Banner Today";
$itemslist[] = "Imps Delivered Per Text Ad Today";
$itemslist[] = "Credits Earned Yesterday";
$itemslist[] = "Hits Delivered Yesterday";
$itemslist[] = "Hits Delivered Per URL Yesterday";
$itemslist[] = "Imps Delivered Per Banner Yesterday";
$itemslist[] = "Imps Delivered Per Text Ad Yesterday";

?>

<center>

<?

if (isset($_POST['Submit']) && $_POST['Submit'] == "Update Settings") {
	
	lfmsql_query("DELETE FROM `".$prefix."tedash_hidden` WHERE adminidnum='".$_SESSION["adminidnum"]."'") or die(lfmsql_error());
	
	foreach ($itemslist as $value) {
		$checkboxname = str_replace(' ', '', $value);
		if ($_POST[$checkboxname] != "1") {
			lfmsql_query("INSERT INTO `".$prefix."tedash_hidden` (adminidnum, listitem) VALUES ('".$_SESSION["adminidnum"]."', '".$value."')") or die(lfmsql_error());
		}
	}
	
	echo("<br><div class=\"lfm_title\">Updated Successfully</div><br>");
	echo("<br><div class=\"lfm_descr\"><a href=\"admin.php?f=widgets\">Go back to Widgets</a></div><br>");
	exit;	
}

####################

//Begin main page

####################

?>

	<br><div class="lfm_title">TE Dashboard Settings</div>
	
	<br><div class="lfm_descr">Check the box for each item that you want to show on the TE Dashboard.</div>
	
	<form action="admin.php?f=widgets&widgetsettings=tedash_settings.php" method="post">
	<table width="500" cellpadding="5" cellspacing="0" border="0">
	
	<tr><td align="left">
	
	<?
	
	foreach ($itemslist as $value) {
		$valuename = $value;
		$itemhidden = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."tedash_hidden` WHERE adminidnum='".$_SESSION["adminidnum"]."' AND listitem='".$value."'"), 0);
		echo("<input type=\"checkbox\" name=\"".str_replace(' ', '', $value)."\" value=\"1\""); if($itemhidden == 0) { echo(" checked"); } echo("> <font size=\"2\">".$valuename."</font><br>");
	}
	
	?>
	
	</td></tr>
	
	<tr><td>
	<INPUT type="submit" name="Submit" value="Update Settings">
	</td></tr>
	
	</table>
	</form>
	