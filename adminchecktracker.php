<?php

// HitsConnect SurfingGuard Checker v2.1
// �2011 Josh Abbott, http://surfingguard.com
// LFMTE Rotator and Tracker Checker

require_once "inc/filter.php";

// Prevent anyone who isn't logged in from opening this page
include "inc/checkauth.php";
if(!isset($_SESSION["adminid"])) { exit; };

$siteid = $_GET['siteid'];
$blocksite = 0;

if (isset($_GET['showsite']) && is_numeric($_GET['showsite'])) {
	$showsite = $_GET['showsite'];
} else {
	$showsite = 0;
}

if (!is_numeric($siteid)) {
	echo("Site ID not found.");
	exit;
}

$getsite = mysql_query("Select url from `tracker_urls` where id=$siteid limit 1");

if (mysql_num_rows($getsite) == 0) {
	echo("Site not found.");
	exit;
}

$testurl = mysql_result($getsite, 0, "url");

echo("<html><body bgcolor=\"#8FBAE7\">");

if (file_exists("f4checker.php")) {

  include "f4checker.php";

   echo("<center><table border=0 align=center width=\"100%\"><tr><td align=center width=\"15%\"><a target=\"_blank\" href=\"http://surfingguard.com\"><img src=\"http://surfingguard.com/hcsgshield125.png\" border=\"0\" align=\"left\"></a></td>");
   
   // Run the site checker 
   $resultsHTML = ""; 
   $f4warnings = "";
   $F4CHECK = f4check($testurl);

   if ($F4CHECK['PAGE_STATUS'] == "FAILED") {
     $resultsHTML .= "<td valign=\"middle\" align=\"center\" width=\"25%\">";
     $resultsHTML .= "<font color=\"darkred\"><b>PAGE FAILED CHECK<br><a target=_blank href=\"$testurl\">Open Site In New Window</a></b></font>";
     $resultsHTML .= "</td>";
     $resultsHTML .= "<td align=\"center\" width=\"30%\"><font size=\"2\">";
     $resultsHTML .= "The following errors were generated:<br>";
	   reset($F4CHECK);
	   foreach($F4CHECK AS $F4KEY=>$F4VAL) {
		   if (substr($F4KEY,0,3) == 'ERR') {
			   $resultsHTML .= "<font color=\"darkred\">$F4VAL</font><br>";
			   
			   if (strpos($F4VAL, "Virus Found") !== false || $F4VAL == "Google - Suspected malicious code") {
			   	$blocksite = 1;
			   }
		   }
	   }
	   $resultsHTML .= "</td>";
   } else {
   
     $checkerkey = rand(100, 1000);
     $checkerkey = md5($checkerkey);
     $_SESSION['checkerkey'] = $checkerkey;
   
     $resultsHTML .= "<td align=\"center\" width=\"25%\">";
     $resultsHTML .= "<b>PAGE PASSED CHECK<br><a target=_blank href=\"$testurl\">Open Site In New Window</a></b>";
     $resultsHTML .= "</td>";
	   foreach($F4CHECK AS $F4KEY => $F4VAL) {
		   if (substr($F4KEY,0,3) == 'WAR') {
			   $f4warnings .= "<font color=\"darkred\">$F4VAL</font><br>";
		   }
	   }
	   if($f4warnings != "") {
	    $resultsHTML .= "<td align=\"center\" width=\"40%\"><font size=\"2\">";
      $resultsHTML .= "The following Warnings were generated:<br>";
      $resultsHTML .= $f4warnings;
      $resultsHTML .= "</td>";
     }
   }
   
   echo($resultsHTML);
   echo("</tr></table></center>");
  
} else {
	echo("The HitsConnect SurfingGuard checker is not installed correctly.");
	exit;
}

if ($blocksite == 1 && $showsite != 1) {

echo("<hr><center>
<p><font size=\"5\" color=\"darkred\"><b>WARNING</b></font></p>

<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\">
<tr><td align=\"left\">
<p><b>The HitsConnect SurfingGuard Checker has detected that opening this page could potentially infect your computer with a virus.  Please see the error above for more details.</b></p>
</td></tr>
</table>

<p><b><a href=\"adminchecksite.php?siteid=".$siteid."&showsite=1\">Click Here</a> if you still wish to open the page</a></b></p>

</center>
</body></html>");

} else {

echo("<hr><center><iframe src=\"$testurl\" width=800 height=500 scrolling=yes></iframe></center>
</body></html>");

}

exit;

?>