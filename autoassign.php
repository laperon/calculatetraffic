<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.04
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

$getaccdata = mysql_query("Select minauto from ".$prefix."membertypes where mtid=$acctype limit 1");
$minauto = mysql_result($getaccdata, 0, "minauto");

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";
load_template ($theme_dir."/header.php");
//load_template ($theme_dir."/mmenu.php");
?>
<div class="wfull">
    <div class="grid w960">
        <div class="header-banner">&nbsp;</div>
    </div>
</div>
<?php
if ($_GET['assigncredits'] == "yes") {

	$geturls = mysql_query("Select id from ".$prefix."msites where memid=$userid order by url asc");
	@mysql_query("Update ".$prefix."msites set assign=0 where memid=$userid");
	$totalassigned = 0;
	
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$siteid = mysql_result($geturls, $i, "id");
		
		$getcreditpost = 'assignamount'.$siteid.'';
		$toassign = $_POST[$getcreditpost];
		$testtotal = $totalassigned+$toassign;
		if ((is_numeric($toassign)) && ($toassign >= 0) && ($toassign <= 100) && ($testtotal <= 100)) {
		$totalassigned = $totalassigned+$toassign;
		@mysql_query("Update ".$prefix."msites set assign=$toassign where memid=$userid and id=$siteid");
		}
	}

}

####################

//Begin main page

####################

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>");
/*Load Leftside Infobox*/
load_template ($theme_dir."/mmenu.php");

echo("<div class=c9>
<div class=banner-content>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
    <tbody><tr>
    <td align=center valign=top><h4><b>Auto Assign</b></h4>");

if ($minauto > 0) {
echo("<p><b>You must auto-assign at least ".$minauto."% of your credits.</b></p>");
}

$countsites = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."msites where memid=$userid"),0);
	if ($countsites >= 1) {
	$totalassigned = mysql_result(mysql_query("Select SUM(assign) from ".$prefix."msites where memid=$userid"),0);
	}

echo("<p><b>".$totalassigned."% of your credits are currently auto-assigned</b></p>");

if ($_GET['surfreturn'] == 1) {
echo("<p><b><font color=red size=3>You do not have enough credits auto-assigned.</font></b></p>");
}

$getsites = @mysql_query("Select * from ".$prefix."msites where memid=$userid order by url asc");

$numsites = mysql_num_rows($getsites);
echo("</td></tr>");
echo("<tr>
<td align=left valign=top class=table-structure>
<div style=margin-bottom:5%;>
<form style=\"margin:0px\" action=\"autoassign.php?assigncredits=yes\" method=\"post\">
<table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tbody><tr>
                <td align=left valign=top class=step-title>URL</td>
                <td align=left valign=top class=step-title>Auto Assign</td>
               </tr>");

for ($i = 0; $i < mysql_num_rows($getsites); $i++) {
	$siteid = mysql_result($getsites, $i, "id");
	$siteurl = mysql_result($getsites, $i, "url");
	$nowassigned = mysql_result($getsites, $i, "assign");
	echo("<tr><td align=left valign=top class=Image>
        <div><input type=text size=25 name=url value=\"$siteurl\" disabled></div>
	</td><td><div><input type=text size=3 name=assignamount$siteid value=$nowassigned>%</div></td></tr>");
}

if (mysql_num_rows($getsites) > 0) {
echo("<tr bgcolor=#EEEEEE height=35><td colspan=2 align=center><input type=submit value=Update></td></tr>");
}

echo("</table></form>
</div></td></tr></table></div></div>
<br><br>");

include $theme_dir."/footer.php";

exit;

?>