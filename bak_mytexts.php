<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.09
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$getduplicates = mysql_query("Select allowduplicates from ".$prefix."settings limit 1");
$allowduplicates = mysql_result($getduplicates, 0, "allowduplicates");

$countcredits = mysql_query("select credits, textimps, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$usertexts = mysql_result($countcredits, 0, "textimps");
$acctype = mysql_result($countcredits, 0, "mtype");

$getaccdata = mysql_query("Select textvalue, maxtexts, mantexts from ".$prefix."membertypes where mtid=$acctype limit 1");
$impvalue = mysql_result($getaccdata, 0, "textvalue");
$maxtexts = mysql_result($getaccdata, 0, "maxtexts");
$mantexts = mysql_result($getaccdata, 0, "mantexts");

if ($mantexts == 0) {
	$newstate = 1;
} else {
	$newstate = 0;
}

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";
load_template ($theme_dir."/header.php");
?>
<div class="wfull">
    <div class="grid w960">
        <div class="header-banner">&nbsp;</div>
    </div>
</div>
<?php
load_template ($theme_dir."/mmenu.php");

if ($_GET['addtext'] == "yes" && $_POST['newtext'] != "" && $_POST['newtarget'] != "") {

	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mtexts where memid=$userid and text='".$_POST['newtext']."' and target='".$_POST['newtarget']."'"),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {

	$numtexts = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mtexts where memid=$userid"),0);
	if ($numtexts < $maxtexts) {
	
	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  $bannedurl = "no";
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['newtarget']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }

	if ($bannedurl == "no") {
		@mysql_query("Insert into ".$prefix."mtexts (state, memid, text, target) values ($newstate, $userid, '".$_POST['newtext']."', '".$_POST['newtarget']."')");
	} else {
		$errormess = "This site is banned.";
	}
	
	}
	
	} else {
		$errormess = "You have already added this text ad to your account.";
	}
}

if ($_GET['edittext'] == "yes" && $_POST['edittext'] != ""&& $_POST['edittarget'] != "" && is_numeric($_GET['textid'])) {

	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mtexts where memid=$userid and text='".$_POST['edittext']."' and target='".$_POST['edittarget']."'"),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {

	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  $bannedurl = "no";
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['edittarget']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }
	  
	if ($bannedurl == "no") {
		@mysql_query("Update ".$prefix."mtexts set state=$newstate, text='".$_POST['edittext']."', target='".$_POST['edittarget']."' where id=".$_GET['textid']." and memid=$userid and state!=3 limit 1");
	} else {
		$errormess = "This site is banned.";
	}
	
	} else {
		$errormess = "You have already added this text ad to your account.";
	}	
}

if ($_GET['deletetext'] == "yes" && is_numeric($_GET['textid'])) {
	$confirmdelete = $_GET['confirmdelete'];
	$textid = $_GET['textid'];
	if ($confirmdelete == "yes") {
		$getimps = mysql_query("Select imps from ".$prefix."mtexts where id=$textid and memid=$userid limit 1");
		if (mysql_num_rows($getimps) > 0) {
			$refundimps = mysql_result($getimps, 0, "imps");
			$refundcredits = $refundimps/$impvalue;
			if ($refundcredits > 0) {
				@mysql_query("Update ".$prefix."members set credits=credits+$refundcredits where Id=$userid limit 1");
				$usercredits = $usercredits+$refundcredits;
			}
			@mysql_query("Delete from ".$prefix."mtexts where id=$textid and memid=$userid limit 1");
		}
	} else{
		echo("<center><h4><b>Delete Text Ad</b></h4>
		<font size=2><b>Are you sure you want to delete this text ad?</b><br><br><a href=mytexts.php?deletetext=yes&textid=$textid&confirmdelete=yes><b>Yes</b></a><br><br><a href=mytexts.php><b>No</b></a></font><br><br>");
		include $theme_dir."/footer.php";
		exit;
	}
}

if ($_GET['resethits'] == "yes" && is_numeric($_GET['textid'])) {
	@mysql_query("Update ".$prefix."mtexts set hits=0, clicks=0 where id=".$_GET['textid']." and memid=$userid limit 1");
}

if ($_GET['stateop'] == 1 && is_numeric($_GET['textid'])) {
	@mysql_query("Update ".$prefix."mtexts set state=1 where id=".$_GET['textid']." and memid=$userid and state=2 limit 1");
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");
}

if ($_GET['stateop'] == 2 && is_numeric($_GET['textid'])) {
	@mysql_query("Update ".$prefix."mtexts set state=2 where id=".$_GET['textid']." and memid=$userid and state=1 limit 1");
}

if ($_GET['assigncredits'] == "yes") {

	$geturls = mysql_query("Select id from ".$prefix."mtexts where memid=$userid");
	
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$textid = mysql_result($geturls, $i, "id");
		
		$getcreditpost = 'assignamount'.$textid.'';
		$toassign = $_POST[$getcreditpost];
		
		if (!check_number($toassign)) {
			echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
		}
		
		$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
		$membercredits = mysql_result($countcredits, 0, "credits");
		
		if ($toassign >= 1 && $toassign <= $membercredits) {
		$usercredits = $usercredits-$toassign;
		$imptoassign = $toassign*$impvalue;
		@mysql_query("Update ".$prefix."mtexts set imps=imps+$imptoassign where memid=$userid and id=$textid");
		@mysql_query("Update ".$prefix."members set credits=credits-$toassign where Id=$userid");
		}
	}
	
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");

}

if ($_GET['assignimps'] == "yes") {

	$geturls = mysql_query("Select id from ".$prefix."mtexts where memid=$userid");
	
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$textid = mysql_result($geturls, $i, "id");
		
		$getcreditpost = 'assignimps'.$textid.'';
		$toassign = $_POST[$getcreditpost];
		
		if (!check_number($toassign)) {
			echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
		}
		
		$countcredits = mysql_query("select textimps from ".$prefix."members where Id=$userid");
		$membercredits = mysql_result($countcredits, 0, "textimps");
		
		if ($toassign >= 1 && $toassign <= $membercredits) {
		$usertexts = $usertexts-$toassign;
		$imptoassign = $toassign;
		@mysql_query("Update ".$prefix."mtexts set imps=imps+$imptoassign where memid=$userid and id=$textid");
		@mysql_query("Update ".$prefix."members set textimps=textimps-$toassign where Id=$userid");
		}
	}
	
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");

}

if ($_GET['quickassign'] == "yes") {

	$quickcrds = $_POST['quickcrds'];
	
	if (!check_number($quickcrds)) {
		echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
	}
	
	$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
	$membercredits = mysql_result($countcredits, 0, "credits");
	if (is_numeric($quickcrds) && $quickcrds >= 1 && $quickcrds <= $membercredits) {
	
		$getactive = mysql_query("Select id from ".$prefix."mtexts where memid=$userid and state=1");
		if ((mysql_num_rows($getactive) > 0) && ($quickcrds >= mysql_num_rows($getactive))) {
		
		$persite = floor($quickcrds/mysql_num_rows($getactive));
		$impspersite = floor(($quickcrds*$impvalue)/mysql_num_rows($getactive));
		
		for ($i = 0; $i < mysql_num_rows($getactive); $i++) {
		
			$updateid = mysql_result($getactive, $i, "id");
			$usercredits = $usercredits-$persite;
			@mysql_query("Update ".$prefix."mtexts set imps=imps+$impspersite where id=$updateid limit 1");
			@mysql_query("Update ".$prefix."members set credits=credits-$persite where Id=$userid limit 1");

		}
		}
		
	}
	
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");
}

####################

//Begin main page

####################

$usercredits = round($usercredits, 2);

$getimpsetting = mysql_query("Select storeimps from ".$prefix."settings limit 1");
$impsetting = mysql_result($getimpsetting, 0, "storeimps");

echo("<center><h4><b>My Texts Ads</b></h4>");

if ($errormess != "") {
echo("<p><b><font color=red>$errormess</font></b></p>");
}

echo("<br>
<p><b>You have ".$usercredits." credits in your account.</b></p>
<p><b>1 Credit = $impvalue Views</b></p>
");

if ($impsetting == 1) {
echo("<br>
<p><b>You have ".$usertexts." text impressions in your account.</b></p>
");
}

echo("
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=left>
<center><p><b>Quick Assign</b></p>
<form style=\"margin:0px\" action=\"mytexts.php?quickassign=yes\" method=\"post\">
<p>Evenly distribute <input type=text name=quickcrds value=0 size=3> credits to my active text ads.<br><br><input type=submit value=\"Assign\"></p>
</form>
</td></tr>
</table>
<br><br>");

echo("<table border=0 cellpadding=0 cellspacing=0>
<tr><td valign=top>

<table border=1 bordercolor=black cellpadding=5 cellspacing=0>");

echo("<tr height=30><td align=center>Text Ad</td><td align=center>Stats</td><td align=center>Views<br>Assigned</td></tr>");

$gettexts = @mysql_query("Select * from ".$prefix."mtexts where memid=$userid order by target asc");
$numtexts = mysql_num_rows($gettexts);

for ($i = 0; $i < mysql_num_rows($gettexts); $i++) {

	$textid = mysql_result($gettexts, $i, "id");
	$state = mysql_result($gettexts, $i, "state");
	$textcontent = mysql_result($gettexts, $i, "text");
	$targeturl = mysql_result($gettexts, $i, "target");
	$imps = mysql_result($gettexts, $i, "imps");
	$hits = mysql_result($gettexts, $i, "hits");
	$clicks = mysql_result($gettexts, $i, "clicks");
	
	if ($hits > 0) {
		$ctr = round(($clicks/$hits)*100);
	} else {
		$ctr = 0;
	}
	
	if ($state == 0) {
		$textstate = "Pending<br>Approval";
	} elseif ($state == 1) {
		$textstate = "Active<br><a href=mytexts.php?stateop=2&textid=$textid>Pause Ad</a>";
	} elseif ($state == 2) {
		$textstate = "Paused<br><a href=mytexts.php?stateop=1&textid=$textid>Enable Ad</a>";
	} elseif ($state == 3) {
		$textstate = "Suspended";
	} else {
		@mysql_query("Update ".$prefix."mtexts set state=0 where id=$textid limit 1");
		$textstate = "Pending Approval";
	}
	
	echo("<form style=\"margin:0px\" action=\"mytexts.php?edittext=yes&textid=$textid\" method=\"post\">
	<tr bgcolor=#EEEEEE height=157>
	<td align=center valign=center width=230><b><a target=_blank href=\"$targeturl\">$textcontent</a></b><br><br>
	Text: <input type=text size=25 maxlength=30 name=edittext value=\"$textcontent\"><br>Target: <input type=text size=25 name=edittarget value=\"$targeturl\"><br><input type=submit value=\"Save\"><br /><br /><center><font size=1><b>$textstate</b></font></center></td>
	
	<td align=left><font size=2>$hits Views<br>$clicks Clicks<br><br>$ctr% CTR<br><br><center><a href=mytexts.php?resethits=yes&textid=$textid>Reset</a></center></font></td>
	<td align=center><font size=2>$imps</font></td>
	</tr>
	</form>");

}

	if ($numtexts < $maxtexts) {

	echo("<form style=\"margin:0px\" action=\"mytexts.php?addtext=yes\" method=\"post\">
	<tr bgcolor=#EEEEEE>
	<td align=left colspan=4 height=157>Text: <input type=text size=25 maxlength=30 name=newtext value=\"Click Here\"><br>Target: <input type=text size=25 name=newtarget value=\"http://\"><input type=submit value=\"Add\"></td>
	</tr>
	</form>");
	
	} else {
	
	echo("<tr bgcolor=#EEEEEE><td align=left colspan=4 height=157>You must delete a text ad before adding another.</td></tr>");
	
	}

echo("</table>
</td>

<td valign=top>
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=center>Assign<br>Credits</td></tr>
<form style=\"margin:0px\" action=\"mytexts.php?assigncredits=yes\" method=\"post\">");

for ($i = 0; $i < mysql_num_rows($gettexts); $i++) {
	$textid = mysql_result($gettexts, $i, "id");
	echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=text size=3 name=assignamount$textid value=0></td></tr>");
}

if (mysql_num_rows($gettexts) > 0) {
echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=submit value=Assign></td></tr>");
}

echo("</form>
</table>
</td>");

if ($impsetting == 1) {

echo("<td valign=top>
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=center>Assign<br>Imps</td></tr>
<form style=\"margin:0px\" action=\"mytexts.php?assignimps=yes\" method=\"post\">");

for ($i = 0; $i < mysql_num_rows($gettexts); $i++) {
	$textid = mysql_result($gettexts, $i, "id");
	echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=text size=3 name=assignimps$textid value=0></td></tr>");
}

if (mysql_num_rows($gettexts) > 0) {
echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=submit value=Assign></td></tr>");
}

echo("</form>
</table>
</td>");

}

echo("<td valign=top>
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=center>Delete<br>Ad</td></tr>");

for ($i = 0; $i < mysql_num_rows($gettexts); $i++) {
	$textid = mysql_result($gettexts, $i, "id");
	echo("<form style=\"margin:0px\" action=\"mytexts.php?deletetext=yes&textid=$textid\" method=\"post\">
	<tr bgcolor=#EEEEEE height=157><td align=center><input type=submit value=\"Delete\"></td></tr>
	</form>");
}

echo("</form>
</table>
</td>");

echo("</table><br><br>");

include $theme_dir."/footer.php";

exit;

?>