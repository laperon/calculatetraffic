<?php

######################
/*

Banner Checker
Written by Josh Abbott
For LFM Traffic Exchange Scripts

Return Codes:

0 - Server not found
1 - Banner approved
2 - Page not found
3 - Invalid URL
4 - Https URL

*/
######################

function checkbanner($url) {

if (strpos($url, 'https://') !== false) {
	return 4;
}

if (strpos($url, 'http://') === false) {
	return 3;
}

if (strpos($url, '\"') !== false || strpos($url, '\'') !== false || $url == "http://") {
	return 3;
}

$urlinfo = parse_url($url);
$hostname = $urlinfo['host'];
if ($hostname == "") {
	return 3;
}

$ipaddress = gethostbyname($hostname);
if($ipaddress == $hostname) {
	return 0;
}

$port = 80;
$timeout = 20;
$headers = "GET ".$urlinfo['path'].'?'.$urlinfo['query']." HTTP/1.1\r\n";
$headers .= "Host: ".$hostname."\r\n";
$headers .= "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)\r\n";
$headers .= "Connection: close\r\n";
$headers .= "Accept: */*\r\n";
$headers .= "\r\n";

$headerdata = "";
$attempts = 0;

while((strpos($headerdata, '200 OK') === false) && (strpos($headerdata, '201 Created') === false) && (strpos($headerdata, '300 Multiple') === false) && (strpos($headerdata, '301 Moved') === false) && (strpos($headerdata, '302 Found') === false) && (strpos($headerdata, '303 See') === false) && (strpos($headerdata, '304 Not') === false) && (strpos($headerdata, '307 Temporary') === false) && (strpos($headerdata, '308 Permanent') === false) && ($attempts<2)) {
	
	$connection = fsockopen($hostname,$port,$timeout);
	fwrite($connection,$headers);
	//Read up to 30 bytes
	$headerdata = fread($connection, 30);
	fclose($connection);
	
	$attempts = $attempts+1;
}

if((strpos($headerdata, '200 OK') === false) && (strpos($headerdata, '201 Created') === false) && (strpos($headerdata, '300 Multiple') === false) && (strpos($headerdata, '301 Moved') === false) && (strpos($headerdata, '302 Found') === false) && (strpos($headerdata, '303 See') === false) && (strpos($headerdata, '304 Not') === false) && (strpos($headerdata, '307 Temporary') === false) && (strpos($headerdata, '308 Permanent') === false)) {
	return 2;
}

return 1;

}

?>