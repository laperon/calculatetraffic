<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.29
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

include "inc/config.php";
mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname) or die( "Unable to select database");

require_once "inc/funcs.php";

mysql_query("UPDATE ".$prefix."settings SET lastbouncecron='".time()."'");

$settings = mysql_fetch_array(mysql_query("SELECT sitename,replyaddress,bounceaddress,bouncetest1,bouncetest2 FROM ".$prefix."settings WHERE id=1"));

$sitename = $settings['sitename'];
$replyemail = $settings['replyaddress'];
$bounceaddress = $settings['bounceaddress'];

$bouncemode1_buffer = time()-(60*60*$settings['bouncetest1']);
$bouncemode2_buffer = time()-(60*60*$settings['bouncetest2']);
$bounceactivate = time()-(60*60*($settings['bouncetest2']+1));

// Manage Pending Modes 1 and 2
$getmem = mysql_query("SELECT email FROM ".$prefix."members WHERE bouncetp < bouncemode AND ((bouncemode=1 AND lastbounce <= $bouncemode1_buffer) OR (bouncemode=2 AND lastbounce <= $bouncemode2_buffer)) ");
if (mysql_num_rows($getmem)) {
	// Get the bounce email
	$eres=mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='bouncetesteml'");
	if (mysql_num_rows($eres) > 0) {
		$erow=@mysql_fetch_array($eres);
		for ($i = 0; $i < mysql_num_rows($getmem); $i++) {
			$member_email = mysql_result($getmem, $i, "email");
			$msgbody=translate_site_tags($erow["template_data"]);
			$msgbody=translate_user_tags($msgbody,$member_email);
			mysql_query("UPDATE ".$prefix."members SET bouncetp=bouncetp+1 WHERE email='$member_email' ");
		
			// Send message
			$now = date("ymdHis").rand(1000,9999);
			$headers = "From: ".$sitename." <".$replyemail.">"."\r\n";
			$headers .= "Return-Path: ".$sitename." <".$replyemail.">"."\r\n";
			$headers .= "Message-ID: <".$now."TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
			$mailFrom = "-f$bounceaddress";
			$subject = $sitename." Bounce Test";
			mail($member_email,$subject,$msgbody,$headers,$mailFrom);
		}
	}
}
mysql_query("UPDATE ".$prefix."members SET mailbounce=0,bouncemode=0,bouncetp=0 WHERE bouncemode < 3 AND lastbounce<='$bounceactivate' ");

?>