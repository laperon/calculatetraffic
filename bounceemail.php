<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.29
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if ($_REQUEST['act'] == 'updbncemail') {
	// Validate Email Address
	
	$_POST["myemail"] = $_REQUEST["myemail"] = strtolower($_REQUEST["myemail"]);
	
    if(!isset($_REQUEST["myemail"]) || strlen(trim($_REQUEST["myemail"])) < 2) {
	$errmsg.="Email address is required<br>";
	
    } elseif (check_email($_REQUEST["myemail"]) !== true) {
    	$errmsg.="Email address is invalid<br>";
    	
    } elseif (bannedEmail($_REQUEST["myemail"]) !== false) {
    	$errmsg.="Email address is banned<br>";    	
    }
      
	// Validate Confirm Email Address
	if(trim($_REQUEST["myemailconf"]) != trim($_REQUEST["myemail"])) {
		$errmsg.="Please confirm your email address<br>";
	}
	// Check that username and email are unique
	$uqry="SELECT count(*) AS ucount FROM ".$prefix."members WHERE email='".$_REQUEST["myemail"]."' AND Id != '".$drow['Id']."' ";
	$ures=mysql_query($uqry) or die(mysql_error());
	$urow=@mysql_fetch_array($ures);
	if($urow["ucount"] > 0) {
		// The email address is already in the database - send this member to the password recovery page
		$errmsg .= "The email address '".$_REQUEST["myemail"]."' is already registered<br>";
	}
	
	$vericode = md5(rand(100,10000));
		
    if($errmsg == "") {
	    $mynewsletter = mysql_result(mysql_query("SELECT newsletter FROM ".$prefix."members WHERE Id=".$drow['Id'] ),0);
	    switch ($mynewsletter) {
		    case 3:
		      $newnletter = 1;
		      break;
		    case 2:
		      $newnletter = 0;
		      break;
		    default:
		      $newnletter = 0;
		      break;
	    }
	    
	    $getreqver = mysql_query("Select reqemailver from ".$prefix."settings");
		$reqemailver = mysql_result($getreqver, 0, "reqemailver");
		if ($reqemailver == 0) {
			$memberstatus="Active";
		} else {
			$memberstatus="Unverified";
		}
	    
	    mysql_query("UPDATE ".$prefix."members SET email='${_REQUEST["myemail"]}',mailbounce=0,bouncemode=0,bouncetp=0,vericode='$vericode',status='$memberstatus',newsletter='$newnletter' WHERE Id=".$drow['Id'] ) or die(mysql_error());
		SendWelcomeEmail($_REQUEST["myemail"]);
		session_destroy();
		header("location:login.php");
		mysql_close();
		exit;
    } else {
	    $page_content .= $errmsg;
    }
}

if ($drow['bouncemode'] == 3) {
	
	$bouncetemplate = mysql_result(mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='bounce_page'"), 0);
	
	if (function_exists('html_entity_decode')) {
	   $bouncetemplate = html_entity_decode($bouncetemplate);
	} else {
	   $bouncetemplate = unhtmlentities($bouncetemplate);
	}
	
	$bouncetemplate = str_replace('#BOUNCEADDRESS#', $useremail, $bouncetemplate);
	
	$bouncetemplate = translate_site_tags($bouncetemplate);
	$bouncetemplate = translate_user_tags($bouncetemplate, $useremail);
	
	$page_content .= "<div style=\"width:80%;margin: 0 auto;\">
	  ".$bouncetemplate."
	  <form action=\"members.php\" method=\"post\">
	  <p><b>Your Email Address:</b><br><input type=\"text\" name=\"myemail\" value=\"\" size=\"40\"></p>
	  <p><b>Confirm Email Address:</b><br><input type=\"text\" name=\"myemailconf\" value=\"\" size=\"40\"></p>
	  <p><input type=\"hidden\" name=\"act\" value=\"updbncemail\"><input type=\"submit\" name=\"submit\" value=\"Save Email Address\"></p>
	</form></div>";
	
	load_template ($theme_dir."/header.php");
	load_template ($theme_dir."/content.php");
	load_template ($theme_dir."/footer.php");
	
	mysql_close();
	exit;
}
?>