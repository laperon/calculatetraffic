#!/usr/bin/php -q
<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.29
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/filter.php";
	include "inc/config.php";
	
	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");
	
	$settings = mysql_fetch_array(mysql_query("SELECT replyaddress,bounceaddress,auto_email,bouncetest1,bouncetest2,bouncelimit FROM ".$prefix."settings WHERE id=1"));
	
	$fd = fopen("php://stdin", "r");
	$email = "";
	while (!feof($fd)) {
	    $email .= fread($fd, 1024);
	}
	fclose($fd);
	
    // empty vars
    $from = "";
    $subject = "";
    $headers = "";
    $message = "";
    $splittingheaders = true;

	$getemails = extractEmail($email);
	foreach ($getemails[0] AS $k=>$v) {
		$email = trim(str_replace("\n","",str_replace("\r","",$v)));
		$getadminemails = mysql_query("SELECT * FROM ".$prefix."admin ");
		while ($adm = mysql_fetch_array($getadminemails)) {
			$admeml[] = $adm['email'];
		}
		if ($email != $settings['replyaddress'] AND $email != $settings['bounceaddress'] AND $email != $settings['auto_email'] AND !in_array($email,$admeml)) {
			$bema[] = $email;
		}
	}
	$bademails = array_unique($bema);
    
	foreach ($bademails AS $k=>$v) {
		$finduser = mysql_query("SELECT * FROM ".$prefix."members WHERE email='".$v."' ");
		if (mysql_num_rows($finduser) == 1) {
			$user = mysql_fetch_array($finduser);
			if ($user['bouncemode'] <= 2) {
				
				$user['mailbounce']++;
				
				if ($user['mailbounce'] > $settings['bouncelimit']) {
					if ($user['bouncemode'] == 0) {
						mysql_query("UPDATE ".$prefix."members SET mailbounce=mailbounce+1,lastbounce='".time()."',bouncemode=1 WHERE Id='${user['Id']}' ");
					} elseif ($user['bouncemode'] == 1 AND $user['lastbounce'] <= (time()-(60*60*$settings['bouncetest1']))) {
						mysql_query("UPDATE ".$prefix."members SET mailbounce=mailbounce+1,lastbounce='".time()."',bouncemode=2 WHERE Id='${user['Id']}' ");
					} elseif ($user['bouncemode'] == 2 AND $user['lastbounce'] <= (time()-(60*60*$settings['bouncetest2']))) {
						mysql_query("UPDATE ".$prefix."members SET mailbounce=mailbounce+1,lastbounce='".time()."',bouncemode=3,newsletter=newsletter+2 WHERE Id='${user['Id']}' ");
					}
				}
				
			}
		}
	}
	
	function extractEmail($string)
	{
	$regEx = "/([\s]*)[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i";
	preg_match_all($regEx, $string, $emails);
	
	return $emails;
	}  
?>