<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2011 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$countcredits = mysql_query("select credits, mtype, email from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");
$useremail = mysql_result($countcredits, 0, "email");

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";
load_template ($theme_dir."/header.php");
?>
<div class="wfull">
    <div class="grid w960">
        <div class="header-banner">&nbsp;</div>
    </div>
</div>
<?php
####################

//Begin main page

####################

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>
<link rel=\"stylesheet\" href=\"spstyle.css\" />");

load_template ($theme_dir."/mmenu.php");

echo("<div class=c9>
<div class=banner-content>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
    <tbody><tr>
    <td align=center valign=top><h2><b>Buy Credits</b></h2>
<br>");

// begin startpage mod

$r=mysql_query("SELECT * FROM ".$prefix."startpage_settings;");
$row=mysql_fetch_array($r);
extract($row);

$r=mysql_query("SELECT * FROM `".$prefix."ipn_products` WHERE id=1");
if(mysql_num_rows($r)) {

if($enable) {
// AUTOMATED START PAGE ENABLED

if($type=="D") { $type="Day"; }
if($type=="W") { $type="Week"; }
if($type=="M") { $type="Month"; }

include("activecalendar.php");

$r=mysql_query("SELECT * FROM `".$prefix."ipn_products` WHERE id=1;");
$amount=@mysql_result($r,0,"amount");

if($_GET[t]) {
	$packages = "";
	$from=$_GET[t];
	$date=date("j M Y",$from);
	echo '
<center>
<br>
<font face="Tahoma" size=4><b>'.mysql_result($r,0,"name").'</b></font>
<p>You clicked on '.$date;
	if($type=="Day") {
		$to=$from+86399;
		$extra=date("j M Y",$to);
		}
	if($type=="Week") {
		$d=date("w",$from);
		if($d<7) { $from=$from-($d*86400); }
		$to=$from+(7*86400)-1;
		$extra=date("j M Y",$from).' to '.date("j M Y",$to);
		}
	if($type=="Month") {
		$d=date("t",$from);
		$m=date("n",$from);
		$y=date("Y",$from);
		$from=mktime(0,0,0,$m,1,$y);
		$to=$from+($d*86400)-1;
		$extra=date("M Y",$to);
		}
	echo '<p>Your website will be our Site Of The '.$type.' from 
<br>'.date("r",$from).' GMT to '.date("r",$to).' GMT';
	// make sure no-one else has just booked them!
	$r=mysql_query("SELECT * FROM ".$prefix."startpage WHERE `from`=$from AND `to`=$to;") or die(mysql_error());
	if(mysql_num_rows($r)) {
		echo '<p>Sorry! That date has already been booked. Please choose another date...';
		} else {
		echo '<p>The total cost will be $'.$amount.'
<p>Please click below to pay for your Site Of The '.$type.'<br><br>'.show_button_code($id,6,$extra);
		}
	echo("</center><br><br>");
	include $theme_dir."/footer.php";
	exit;
	}

$spdescription = mysql_result($r,0,"description");

if ($spdescription != "") {
$spdescription = "<br><table width=500 border=0 cellpadding=0 cellspacing=0><tr><td><font size=2>".$spdescription."</font></td></tr></table>";
}

	echo '<center>
<br>
<font face="Tahoma" size=4><b>'.mysql_result($r,0,"name").' - $'.$amount.'</b></font>
<br>'.$spdescription.'

<table align="center" cellpadding=0 cellspacing=0><tr>';

	$y=date("Y");
	$m=date("n");
	$j=date("j");
	$t=date("t");

	$cal=new activeCalendar($y,$m);
	$cal->setFirstWeekDay(0);

	for($d=1;$d<=$t;$d++) {
	   if($d<=$j) {
		$cal->setEvent($y,$m,$d,"expired");
		} else {
		$u=mktime(0,0,0,$m,$d,$y);
		$r=mysql_query("SELECT id FROM ".$prefix."startpage WHERE `from`<=$u AND `to`>$u;");
		if(mysql_num_rows($r)) {
		   $cal->setEvent($y,$m,$d,"booked");
		   } else {	
		   $cal->setEvent($y,$m,$d,"available","buycredits.php?t=$u");
		   }
		}
   	   }

	echo '<td valign="top">'.$cal->showMonth(true).'</td>
<td width=27></td>
';

	$m++;
	if($m>12) { $m=1; $y++; }
	$cal=new activeCalendar($y,$m);
	$cal->setFirstWeekDay(0);
	$t=date("t",mktime(0,0,0,$m,1,$y));

	for($d=1;$d<=$t;$d++) {
	   $u=mktime(0,0,0,$m,$d,$y);
	   $r=mysql_query("SELECT id FROM ".$prefix."startpage WHERE `from`<=$u AND `to`>$u;");
	   if(mysql_num_rows($r)) {
		$cal->setEvent($y,$m,$d,"booked");
		} else {
		$cal->setEvent($y,$m,$d,"available","buycredits.php?t=$u");
		}
	   }

	echo '<td valign="top">'.$cal->showMonth(true).'</td>
</tr>
</table>
';
	}

echo '
<br>';
}

// end startpage mod


//Credit Boost
$t = time();
$getboost = mysql_query("SELECT buytext FROM ".$prefix."cboost WHERE `starttime`<$t AND `endtime`>$t AND buyboost>1 AND buytext!='' AND (acctype=$acctype OR acctype=0) LIMIT 1;");

if (mysql_num_rows($getboost) > 0) {
	$buytext = mysql_result($getboost, 0, "buytext");
	echo("<p><b><font color=\"red\" size=\"3\">".$buytext."</font></b></p>");
}
//End Credit Boost


echo "<table width=100% border=1><tr align=center><td>";
$res = mysql_query("SELECT id,name,amount,description FROM ".$prefix."ipn_products WHERE id>1 AND subscription!=2 AND upgrade=0 AND disable=0 AND (showaccount=0 OR showaccount=$acctype) ORDER BY rank asc;");
for ($i = 1; $i <= mysql_num_rows($res); $i++) {
	$row=mysql_fetch_array($res);
	echo '<table width=100%>
<tr>
<td><b><font face="'.$fontface.'" size=4>'.$row[name].' - $'.$row[amount].'</font></b></td>
<td>'.show_button_code($row[id]).'</td></tr>
<hr/>
<tr><td><b><font face="'.$fontface.'" size=2>'.$row[description].'</font></b></td></tr>
</table>';
	}
echo '</td></tr>
</table></td></tr></table></div></div></div></div><br/><br/>';

include $theme_dir."/footer.php";

exit;
?>
