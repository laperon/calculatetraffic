<?php

// SurfingGuard Checker v2.3
// �2011-2015 Josh Abbott, http://surfingguard.com
// LFMTE script version

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];
$siteid = $_GET["siteid"];

$getacctype = lfmsql_query("select mtype from ".$prefix."members where Id=$userid");
$acctype = lfmsql_result($getacctype, 0, "mtype");

$getaccdata = lfmsql_query("Select mansites from ".$prefix."membertypes where mtid=$acctype limit 1");
$mansites = lfmsql_result($getaccdata, 0, "mansites");

if ($mansites != 0) {
	header("Location: mysites.php");
	exit;
}

if (!is_numeric($userid) || !is_numeric($siteid)) {
	header("Location: mysites.php");
	exit;
}

$getsite = lfmsql_query("Select url from ".$prefix."msites where id=$siteid and state=0 and memid=$userid limit 1");

if (lfmsql_num_rows($getsite) == 0) {
	header("Location: mysites.php");
	exit;
}

//Check if site has already been approved
if (($_POST['checkkey'] != "") && ($_POST['checkkey'] == $_SESSION['checkerkey'])) {
	@lfmsql_query("Update ".$prefix."msites set state=1 where id=$siteid and memid=$userid limit 1");
	@lfmsql_query("Update ".$prefix."members set actsite=1 where Id=$userid limit 1");
	header("Location: mysites.php");
	exit;
}

$testurl = lfmsql_result($getsite, 0, "url");
if (strpos($testurl, '?') !== false && strpos($testurl, 'intellisplash=') === false) {
	// Tells IntelliSplash To Not Redirect To Another Site
	$testurl = $testurl."&intellisplash=9";
}

echo("<html><body bgcolor=\"#8FBAE7\">");

if (file_exists("f4checker.php")) {

  include "f4checker.php";
  
  $gethcid = lfmsql_query("Select value from sg_settings where name='hitsconnectid' limit 1");
  if (lfmsql_num_rows($gethcid) > 0) {
	  $hcid = lfmsql_result($gethcid, 0, "value");
  } else {
  	$hcid = 1;
  }

   echo("<center><table border=0 align=center width=\"100%\"><tr><td align=center width=\"10%\"><a target=\"_blank\" href=\"http://surfingguard.com\"><img border=\"0\" src=\"http://surfingguard.com/hcsgshield150.png\" align=\"left\"></a></font></b></td>");
   
   // Run the site checker 
   $resultsHTML = ""; 
   $f4warnings = "";
   $F4CHECK = f4check($testurl);
   
   // Show The Status
   reset($F4CHECK);
   $statusHTML = "";
   foreach($F4CHECK AS $F4KEY=>$F4VAL) {
   if (substr($F4KEY,0,3) == 'STA') {
          // Update Images
          $F4VAL = str_replace("SGCHECKPNG", "</td><td><img src=\"/sgcheck.png\">", $F4VAL);
          $F4VAL = str_replace("SGFAILPNG", "</td><td><img src=\"/sgfail.png\">", $F4VAL);
			   $statusHTML .= "<tr><td align=\"center\" valign=\"middle\"><font size=\"2\"><b>$F4VAL</b></font></td></tr>";
		   }
	   }
   echo("<td align=center width=\"20%\">
   <table border=\"0\" cellpadding=\"1\" cellspacing=\"0\">
   ".$statusHTML."
   </table>
   </td>");
   // End Show The Status

   if ($F4CHECK['PAGE_STATUS'] == "FAILED") {
     $resultsHTML .= "<td valign=\"middle\" align=\"center\" width=\"35%\">";
     $resultsHTML .= "<font color=\"darkred\"><b>PAGE FAILED CHECK</b></font>";
     $resultsHTML .= "<br><a target=_self href=\"mysites.php\">Go Back to My Sites</a>";
     $resultsHTML .= "<br><br><font size=\"2\">";
     $resultsHTML .= "The following errors were generated:<br>";
	   reset($F4CHECK);
	   foreach($F4CHECK AS $F4KEY=>$F4VAL) {
		   if (substr($F4KEY,0,3) == 'ERR') {

		   	// Check The Error Action
		   	if ($F4VAL == "Host Not Found" || $F4VAL == "Page Not Found") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Not Found'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Not Found'"), 0);
		   	} elseif ($F4VAL == "Too Many Redirects") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Too Many Redirects'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Too Many Redirects'"), 0);
		   	} elseif ($F4VAL == "Has Hidden IFrame") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Hidden IFrame'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Hidden IFrame'"), 0);
		   	} elseif ($F4VAL == "Has Encoded JavaScript") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Encoded JavaScript'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Encoded JavaScript'"), 0);
		   	} elseif ($F4VAL == "Contains Frame Breaking Code") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Frame Breaking Code'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Frame Breaking Code'"), 0);
		   	} elseif (strpos($F4VAL, "Site In Banned List") !== false) {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Site Banned By You'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Site Banned By You'"), 0);
		   	} elseif (strpos($F4VAL, "Site banned") !== false) {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Site In Central Ban'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Site In Central Ban'"), 0);
		   	} elseif (strpos($F4VAL, "Virus Found") !== false) {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Virus'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Virus'"), 0);
		   	} elseif ($F4VAL == "Google - Suspected phishing page") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Google - Suspected phishing page'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Google - Suspected phishing page'"), 0);
		   	} elseif ($F4VAL == "Google - Suspected malicious code") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_checker_actions where name='Google - Suspected malicious code'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_checker_actions where name='Google - Suspected malicious code'"), 0);
		   	} else {
		   		$action = 1; // Unknown error - leave in unverified state
		   		$mailadmin = 0;
		   	}

		   	if ($action == 2) {
		   		// Suspend Site
		   		@lfmsql_query("Update ".$prefix."msites set state=3 where id=$siteid and memid=$userid limit 1");
		   	}

		           // Add Google Links
		           $F4VAL = str_replace("Google", "<a target=\"_blank\" href=\"http://code.google.com/apis/safebrowsing/safebrowsing_faq.html#whyAdvisory\">Advisory provided by Google</a>", $F4VAL);
		           $F4VAL = str_replace("Suspected phishing page", "<a target=\"_blank\" href=\"http://www.antiphishing.org\">Suspected phishing page</a>", $F4VAL);
		           $F4VAL = str_replace("Suspected malicious code", "<a target=\"_blank\" href=\"http://www.stopbadware.org\">Suspected malicious code</a>", $F4VAL);
		           
			   $resultsHTML .= "<font color=\"darkred\">$F4VAL</font><br>";
			   
			   if ($mailadmin == 1 && $action>0) {
			   	$adminmail = lfmsql_result(lfmsql_query("Select value from sg_settings where name='adminemail'"), 0);
			   	$emailheader = "MIME-Version: 1.0\nContent-type: text/plain; charset=iso-8859-1\nFrom: HitsConnect SurfingGuard Checker <support@surfingguard.com>\nReply-To: HitsConnect SurfingGuard Checker <support@surfingguard.com>\nX-Priority: 3\nX-Mailer: PHP 4\n";
			   	$message = "The HitsConnect SurfingGuard site checker\nhas detected a problem with a site that\nuser ID ".$userid." attempted to submit.\n\nSite: ".$testurl."\nError: ".$F4VAL;
			   	mail($adminmail, "New Site Error", $message, $emailheader);
			   }
		   }
	   }
	   $resultsHTML .= "</td>";
   } else {
   
     $checkerkey = rand(100, 1000);
     $checkerkey = md5($checkerkey);
     $_SESSION['checkerkey'] = $checkerkey;
   
     $resultsHTML .= "<td valign=\"middle\" align=\"center\" width=\"35%\">";
     $resultsHTML .= "<b>PAGE PASSED CHECK</b>";
     $resultsHTML .= "<div id=\"timer\" style=\"font-size: 12pt; margin: 4px; \"></div><br><div id=\"myform\" style=\"visibility: hidden;\"><form style=\"margin:2px\" action=\"checksite.php?siteid=$siteid\" method=\"POST\"><input type=hidden name=checkkey value=\"$checkerkey\"><input type=submit value=\"Confirm My Site\"></form></div></td>";
  
  $gettimer = lfmsql_query("Select value from sg_settings where name='timer' limit 1");
  if (lfmsql_num_rows($gettimer) > 0) {
	$timer = lfmsql_result($gettimer, 0, "value");
  } else {
  	$timer = 10;
  }
  
     $resultsHTML .= "
<script language=\"javascript\">
  var timer=".$timer.";
  function run () {
	  if (timer <= 0) {
		  document.getElementById('myform').style.visibility='visible';
		  document.getElementById('timer').innerHTML='';
		  
	  } else {
		  document.getElementById('timer').innerHTML=timer;
		  timer--;
		  setTimeout(run, 1000);
	  }
  }
  document.onLoad=run();
</script>";

   }
   lfmsql_query("INSERT INTO `".$prefix."f4checks` (`user`,`date`,`page_checked`,`errors`,`warnings`,`status`,`from`) VALUES ('$userid',NOW(),'${F4CHECK['PAGE_CHECKED']}','".$F4CHECK['ERROR_0']."','".$F4CHECK['WARNING_0']."','${F4CHECK['PAGE_STATUS']}','checksite.php')");
   
   echo($resultsHTML);
   
   echo("<td align=\"right\" width=\"30%\">
   <p>&nbsp;</p>
   </td>
   
   <td align=\"center\" width=\"5%\">&nbsp;</td>
   ");
   
   echo("</tr></table></center>");
  
} else {
echo("Checker not installed correctly.");
exit;
}

echo("<hr><center><iframe src=\"$testurl\" width=800 height=325 scrolling=yes></iframe></center>");

echo("</body></html>");

exit;

?>