<?php

// HitsConnect SurfingGuard Checker v2.1
// �2011 Josh Abbott, http://surfingguard.com
// LFMTE Rotator and Tracker Checker

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];
$siteid = $_GET["siteid"];

$getacctype = mysql_query("select mtype from ".$prefix."members where Id=$userid");
$acctype = mysql_result($getacctype, 0, "mtype");

$getaccdata = mysql_query("Select rotator_approve from ".$prefix."membertypes where mtid=$acctype limit 1");
$rotator_approve = mysql_result($getaccdata, 0, "rotator_approve");

if ($rotator_approve != 0) {
	header("Location: myrotators.php?rotpage=trackers");
	exit;
}

if (!is_numeric($userid) || !is_numeric($siteid)) {
	header("Location: myrotators.php?rotpage=trackers");
	exit;
}

$getsite = mysql_query("Select url from tracker_urls where id='".$siteid."' and state=1 limit 1");

if (mysql_num_rows($getsite) == 0) {
	header("Location: myrotators.php?rotpage=trackers");
	exit;
}

//Check if site has already been approved
if (($_POST['checkkey'] != "") && ($_POST['checkkey'] == $_SESSION['checkerkey'])) {
	@mysql_query("Update tracker_urls set state=0 where id='".$siteid."' limit 1");
	header("Location: myrotators.php?rotpage=trackers");
	exit;
}

$testurl = mysql_result($getsite, 0, "url");

echo("<html><body bgcolor=\"#8FBAE7\">");

if (file_exists("f4checker.php")) {

  include "f4checker.php";
  
  $gethcid = mysql_query("Select value from sg_settings where name='hitsconnectid' limit 1");
  if (mysql_num_rows($gethcid) > 0) {
	  $hcid = mysql_result($gethcid, 0, "value");
  } else {
  	$hcid = 1;
  }

   echo("<center><table border=0 align=center width=\"100%\"><tr><td align=center width=\"10%\"><a target=\"_blank\" href=\"http://surfingguard.com\"><img border=\"0\" src=\"http://surfingguard.com/hcsgshield150.png\" align=\"left\"></a></font></b></td>");
   
   // Run the site checker 
   $resultsHTML = ""; 
   $f4warnings = "";
   $F4CHECK = f4check($testurl);
   
   // Show The Status
   reset($F4CHECK);
   $statusHTML = "";
   foreach($F4CHECK AS $F4KEY=>$F4VAL) {
   if (substr($F4KEY,0,3) == 'STA') {
          // Update Images
          $F4VAL = str_replace("SGCHECKPNG", "</td><td><img src=\"/sgcheck.png\">", $F4VAL);
          $F4VAL = str_replace("SGFAILPNG", "</td><td><img src=\"/sgfail.png\">", $F4VAL);
			   $statusHTML .= "<tr><td align=\"center\" valign=\"middle\"><font size=\"2\"><b>$F4VAL</b></font></td></tr>";
		   }
	   }
   echo("<td align=center width=\"20%\">
   <table border=\"0\" cellpadding=\"1\" cellspacing=\"0\">
   ".$statusHTML."
   </table>
   </td>");
   // End Show The Status

   if ($F4CHECK['PAGE_STATUS'] == "FAILED") {
     $resultsHTML .= "<td valign=\"middle\" align=\"center\" width=\"35%\">";
     $resultsHTML .= "<font color=\"darkred\"><b>PAGE FAILED CHECK</b></font>";
     $resultsHTML .= "<br><a target=_self href=\"myrotators.php?rotpage=trackers\">Go Back to My Trackers</a>";
     $resultsHTML .= "<br><br><font size=\"2\">";
     $resultsHTML .= "The following errors were generated:<br>";
	   reset($F4CHECK);
	   foreach($F4CHECK AS $F4KEY=>$F4VAL) {
		   if (substr($F4KEY,0,3) == 'ERR') {

		   	// Check The Error Action
		   	if ($F4VAL == "Host Not Found" || $F4VAL == "Page Not Found") {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Not Found'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Not Found'"), 0);
		   	} elseif ($F4VAL == "Too Many Redirects") {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Too Many Redirects'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Too Many Redirects'"), 0);
		   	} elseif ($F4VAL == "Has Hidden IFrame") {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Hidden IFrame'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Hidden IFrame'"), 0);
		   	} elseif ($F4VAL == "Has Encoded JavaScript") {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Encoded JavaScript'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Encoded JavaScript'"), 0);
		   	} elseif ($F4VAL == "Contains Frame Breaking Code") {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Frame Breaking Code'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Frame Breaking Code'"), 0);
		   	} elseif (strpos($F4VAL, "Site In Banned List") !== false) {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Site Banned By You'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Site Banned By You'"), 0);
		   	} elseif (strpos($F4VAL, "Site banned") !== false) {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Site In Central Ban'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Site In Central Ban'"), 0);
		   	} elseif (strpos($F4VAL, "Virus Found") !== false) {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Virus'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Virus'"), 0);
		   	} elseif ($F4VAL == "Google - Suspected phishing page") {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Google - Suspected phishing page'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Google - Suspected phishing page'"), 0);
		   	} elseif ($F4VAL == "Google - Suspected malicious code") {
		   		$action = mysql_result(mysql_query("Select action from sg_checker_actions where name='Google - Suspected malicious code'"), 0);
		   		$mailadmin = mysql_result(mysql_query("Select mailadmin from sg_checker_actions where name='Google - Suspected malicious code'"), 0);
		   	} else {
		   		$action = 1; // Unknown error - leave in unverified state
		   		$mailadmin = 0;
		   	}

		   	if ($action == 2) {
		   		// Suspend Site
		   		@mysql_query("Update tracker_urls set state=1 where id='".$siteid."' limit 1");
		   	}

		           // Add Google Links
		           $F4VAL = str_replace("Google", "<a target=\"_blank\" href=\"http://code.google.com/apis/safebrowsing/safebrowsing_faq.html#whyAdvisory\">Advisory provided by Google</a>", $F4VAL);
		           $F4VAL = str_replace("Suspected phishing page", "<a target=\"_blank\" href=\"http://www.antiphishing.org\">Suspected phishing page</a>", $F4VAL);
		           $F4VAL = str_replace("Suspected malicious code", "<a target=\"_blank\" href=\"http://www.stopbadware.org\">Suspected malicious code</a>", $F4VAL);
		           
			   $resultsHTML .= "<font color=\"darkred\">$F4VAL</font><br>";
			   
			   if ($mailadmin == 1 && $action>0) {
			   	$adminmail = mysql_result(mysql_query("Select value from sg_settings where name='adminemail'"), 0);
			   	$emailheader = "MIME-Version: 1.0\nContent-type: text/plain; charset=iso-8859-1\nFrom: HitsConnect SurfingGuard Checker <support@surfingguard.com>\nReply-To: HitsConnect SurfingGuard Checker <support@surfingguard.com>\nX-Priority: 3\nX-Mailer: PHP 4\n";
			   	$message = "The HitsConnect SurfingGuard site checker\nhas detected a problem with a site that\nuser ID ".$userid." attempted to submit.\n\nSite: ".$testurl."\nError: ".$F4VAL;
			   	mail($adminmail, "New Site Error", $message, $emailheader);
			   }
		   }
	   }
	   $resultsHTML .= "</td>";
   } else {
   
     $checkerkey = rand(100, 1000);
     $checkerkey = md5($checkerkey);
     $_SESSION['checkerkey'] = $checkerkey;
   
     $resultsHTML .= "<td valign=\"middle\" align=\"center\" width=\"35%\">";
     $resultsHTML .= "<b>PAGE PASSED CHECK</b>";
     $resultsHTML .= "<div id=\"timer\" style=\"font-size: 12pt; margin: 4px; \"></div><br><div id=\"myform\" style=\"visibility: hidden;\"><form style=\"margin:2px\" action=\"checktracker.php?siteid=".$siteid."\" method=\"POST\"><input type=hidden name=checkkey value=\"$checkerkey\"><input type=submit value=\"Confirm My Site\"></form></div></td>";
  
  $gettimer = mysql_query("Select value from sg_settings where name='timer' limit 1");
  if (mysql_num_rows($gettimer) > 0) {
	$timer = mysql_result($gettimer, 0, "value");
  } else {
  	$timer = 10;
  }
  
     $resultsHTML .= "
<script language=\"javascript\">
  var timer=".$timer.";
  function run () {
	  if (timer <= 0) {
		  document.getElementById('myform').style.visibility='visible';
		  document.getElementById('timer').innerHTML='';
		  
	  } else {
		  document.getElementById('timer').innerHTML=timer;
		  timer--;
		  setTimeout(run, 1000);
	  }
  }
  document.onLoad=run();
</script>";

   }
   mysql_query("INSERT INTO `".$prefix."f4checks` (`user`,`date`,`page_checked`,`errors`,`warnings`,`status`,`from`) VALUES ('$userid',NOW(),'${F4CHECK['PAGE_CHECKED']}','".$F4CHECK['ERROR_0']."','".$F4CHECK['WARNING_0']."','${F4CHECK['PAGE_STATUS']}','checktracker.php')");
   
   echo($resultsHTML);
   
   echo("<td align=\"right\" width=\"30%\">
   <a target=\"_blank\" href=\"http://www.hitsconnect.com/index.php?ref_id=".$hcid."\"><img border=\"0\" src=\"http://www.hitsconnect.com/banners/125x125.gif\" width=\"125\" height=\"125\"></a>
   </td>
   
   <td align=\"center\" width=\"5%\">&nbsp;</td>
   ");
   
   echo("</tr></table></center>");
  
} else {
echo("Checker not installed correctly.");
exit;
}

echo("<hr><center><iframe src=\"$testurl\" width=800 height=325 scrolling=yes></iframe></center>");

echo("</body></html>");

exit;

?>