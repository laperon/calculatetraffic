<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.04
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

/*
This file confirms if a user exists under a specified referrer
and account level.  This is useful for an external site that gives
bonuses for joining a site.
*/

require_once "inc/filter.php";

include "inc/config.php";
mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname) or die("Unable to select database");

$userid = $_GET['userid'];
$username = $_GET['username'];

if (!isset($userid) || !is_numeric($userid) || !isset($username) || $username=="") {
	echo("Invalid Request");
	exit;
}

$ref = $_GET['refid'];
$mtype = $_GET['mtype'];
$checkref = "";
$checkmtype = "";

if (isset($ref) && is_numeric($ref)) {
	$checkref = " and refid=".$ref;
}

if (isset($mtype) && is_numeric($mtype)) {
	$checkmtype = " and mtype >= ".$mtype;
}

$checkuser = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."members where status='Active' and Id='".$userid."' and (username='".$username."' or email='".$username."')".$checkref.$checkmtype), 0);

if ($checkuser >= 1) {
	echo("Valid User");
} else {
	echo("Not Found");
}

exit;
?>