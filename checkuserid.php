<?php

// User ID Lookup API
// Included With LFM Based Scripts

// Looks up a user's affiliate ID based on an IP address and/or MD5 encrypted e-mail address

require_once "inc/filter.php";
require_once "inc/lfmsql.php";

include "inc/config.php";
lfmsql_connect($dbhost,$dbuser,$dbpass);
lfmsql_select_db($dbname) or die("Error: Unable to select database");

$usermail = isset($_GET['usermail'])?$_GET['usermail']:'';
$userip = isset($_GET['userip'])?$_GET['userip']:'';

if ($userip == '') {
	echo("Error: Invalid Request");
	exit;
}

if ($usermail != '') {
	// Try Email Search
	$sql = "SELECT Id FROM {$prefix}members WHERE status='Active' AND MD5(email)='{$usermail}'";
	$result = lfmsql_query($sql);
} else {
	$result = false;
}

if(($result == false) || lfmsql_num_rows($result) == 0) {
	// Try IP Search
	$sql = "SELECT Id FROM {$prefix}members WHERE status='Active' AND lastip='{$userip}' ORDER BY lastlogin DESC LIMIT 1";
	$result = lfmsql_query($sql);
}

if(($result != false) && lfmsql_num_rows($result) == 1) {
	// Output The User ID
	$row = lfmsql_fetch_assoc($result);
	$userid = $row['Id'];
	echo("UserID:".$userid.":End");
} else {
	// User Not Found
	echo("UserID:0:End");
}

?>