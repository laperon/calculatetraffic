<?php
////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

require_once "inc/filter.php";

@session_start();
include "inc/userauth.php";
require_once "inc/bandj/functions.php";

$userid = intval($_SESSION["userid"]);

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
$acctype = $mtype;
include "inc/theme.php";
load_template ($theme_dir."/header.php");

echo("<div class=wfull>
    <div class=\"grid w960\">
        <div class=header-banner>&nbsp;</div>
    </div>
</div>");

load_template ($theme_dir."/mmenu.php");

$today = date("Y-m-d");
$time = time();

$claim = '<br><br>';

// get stats
$res = mysql_query("SELECT mtype, upgend, clickstoday FROM {$prefix}members WHERE Id=$userid");
$mtype = @mysql_result($res, 0, "mtype");
$upgend = @mysql_result($res, 0, "upgend");
//$clicks = @mysql_result($res, 0, "clickstoday");
$clicks = bandjSurfedToday($prefix, $userid);

// has member already claimed today?
$res = mysql_query("SELECT id FROM {$prefix}active_surfers WHERE usrid=$userid AND promoid=0 AND claimed='$today'");
if(mysql_num_rows($res)) { $alreadyclaimed = true; } else { $alreadyclaimed = false; }

// minimum number required for award?
$res = mysql_query("SELECT surf FROM {$prefix}active_surfer_rewards WHERE active>0 ORDER BY surf LIMIT 1");
if(mysql_num_rows($res)) { $min = @mysql_result($res,0); } else { $min = 0; }

// get info for best prize available...
$res = mysql_query("SELECT * FROM {$prefix}active_surfer_rewards WHERE active>0 AND surf<=$clicks ORDER BY surf DESC LIMIT 1");
$row = mysql_fetch_array($res);

$prize = $_POST[prize];
if($prize=="upgrade") { $amount = $row[upgrade]; }
if($prize=="cash") { $amount = $row[cash]; }
if($prize=="credits") { $amount = $row[credits]; }
if($prize=="bannerimps") { $amount = $row[bannerimps]; }
if($prize=="textimps") { $amount = $row[textimps]; }

if($_GET['promoid'] == 0 && $amount) {

    if($alreadyclaimed == true) {
        $claim .=  '<font color="red">You have already submitted a claim today!</font>';
    } else {
        mysql_query("INSERT INTO {$prefix}active_surfers (usrid,promoid,$prize,views,claimed,prizetype) VALUES ($userid,0,$amount,$clicks,'$today','$prize')") or die(mysql_error());
        // should prize be awarded automatically?
        if($row[active] == 2) {
            $ffrm = mysql_insert_id();
            $amount = $row["$prize"];
            if($prize=="upgrade") {
                $upgend = date("Y-m-d",strtotime($upgend)+86400); // add 24 hours to existing expiry date
                if($upgend <= date("Y-m-d")) { $upgend = date("Y-m-d",time()+129600); } // add 36 hours to current PHP time
                if($acctype > $amount) { $amount = $acctype; } // don't downgrade existing members!
                mysql_query("UPDATE {$prefix}members SET mtype=$amount, upgend='$upgend' WHERE Id=$userid LIMIT 1") or die(mysql_error());
            } elseif($prize=="cash") {
                mysql_query("INSERT INTO {$prefix}sales (affid,saledate,itemamount,itemname,commission,txn_id,prize) VALUES ($userid,NOW(),'$amount','Active Surfer Reward','$amount','0',1)") or die(mysql_error());
            } else {
                mysql_query("UPDATE {$prefix}members SET $prize=$prize+$amount WHERE Id=$userid LIMIT 1") or die(mysql_error());
            }
            mysql_query("UPDATE `{$prefix}active_surfers` SET awarded=$time WHERE id=$ffrm") or die(mysql_error());
            // Query settings table for sitename
            $res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
            $row=@mysql_fetch_array($res);
            $sitename=$row["sitename"];
            $replyaddress=$row["replyaddress"];
            $subject="Your Active Surfer Reward";
            $headers = 'From: '.$sitename.' <'.$replyaddress.'>';
            $res = mysql_query("SELECT * FROM {$prefix}members WHERE Id=$userid");
            $row = mysql_fetch_array($res);
            $emailbody = mysql_result(mysql_query("SELECT template_data FROM {$prefix}templates WHERE template_name='Active Surfer Reward'"), 0);
            if($prize=="upgrade") {
                $accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$amount"),0);
                $reward = '1 day '.$accname.' Upgrade';
                }
            elseif($prize=="commission") { $reward = '$'.$amount.' USD cash'; }
            elseif($prize=="credits") { $reward = $amount.' Credits'; }
            elseif($prize=="bannerimps") { $reward = $amount.' Banner Impressions'; }
            elseif($prize=="textimps") { $reward = $amount.' Text Link Impressions'; }
            /*
            $emailbody = str_replace("#FIRSTNAME#",$row[firstname],$emailbody);
            $emailbody = str_replace("#CLICKS#",$row[clickstoday],$emailbody);
            $emailbody = str_replace("#REWARD#",$reward,$emailbody);
            $emailbody = str_replace("#SITENAME#",$sitename,$emailbody);
            mail($row[email], $subject, $emailbody, $headers);
            */

            if($prize == 'credits') {
                $date = date("Y-m-d H:i:s");
                $reward_id = mysql_query("Select cvalue from ".$prefix."members AS m JOIN ".$prefix."wswords as w WHERE m.wsword=w.id AND m.id = $userid");
                $cvalue = mysql_result($reward_id, 0, "cvalue");
            }


            $claim .= "<em>Your reward has been added to your account.</em>";

            } else {
            $claim .=  '<p>Your claim has been received and should be processed shortly.';
            }
    }
    $claim .=  '<p>Surf '.$min.' or more sites tomorrow and you can claim another reward...';

} else {	

    if(!$min) {
	// No Rewards
	} else {
        $claim .=  '<font color="red">';
        if(!$clicks) {
            $claim .=  'You have not surfed any pages today!';
            } elseif($clicks==1) {
            $claim .=  'You have only surfed one page today!';
            } else {
            $claim .=  'Today you have surfed '.$clicks.' pages.';
            }
        if($min>$clicks) {
            $claim .=  '<br>You need to surf a minimum of '.$min.' pages<br>before you can claim a reward!';
            } else {
            if($alreadyclaimed == false) {
                $claim .=  '</b></font><p>Choose your prize and click the button to submit your claim:
    <form action="claim.php?promoid=0" method="POST">
    <select name="prize">
    <option value="none">PLEASE SELECT:</option>';
                if($row[upgrade]>0 && ($mtype==1 || $mtype==$row[upgrade])) {
                    $accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$row[upgrade]"),0);
                    $claim .=  '<option value="upgrade">1-day '.$accname.' Upgrade</option>';
                    }
                if($row[cash]>0) { $claim .=  '<option value="cash">$'.$row[cash].' USD cash</option>'; }
                if($row[credits]>0) { $claim .=  '<option value="credits">'.$row[credits].' Credits</option>'; }
                if($row[bannerimps]>0) { $claim .=  '<option value="bannerimps">'.$row[bannerimps].' Banner Imps</option>'; }
                if($row[textimps]>0) { $claim .=  '<option value="textimps">'.$row[textimps].' Text Link Imps</option>'; }
                $claim .=  '
    </select>
    <p><input type="submit" value="Submit Claim">
    </form>
    <b>';
                } else {
                $claim .=  '<p>You have already claimed a reward today.<p>Surf '.$min.' or more sites tomorrow and you can claim another reward...';
                }
            }
        $claim .= '</b></font>';
	}
}

echo '<br><br><br>
<table border="1" bordercolor="black" cellpadding="5" cellspacing="0">
<tr><td align="center">

<font face="Tahoma" size="4"><b>Daily Active Surfer Rewards</b></font>
<p>
<b>You can claim only one active surfer reward per day</b><br>
<br>Claims must be submitted before midnight on the same day the pages were surfed<br>
<br>The current server time and date is:<b> ' . date('l jS \of F Y h:i:s A') . '</b>
<br>You have ' . (23 - date("H")) . ' hours and ' . (59 - date("i")). ' minutes left to submit a claim for today.
<br>' . $claim . '<br><br>';

$res = mysql_query("select * FROM `{$prefix}active_surfer_rewards` WHERE active ORDER BY surf");
if(mysql_num_rows($res)) {
    while($row=mysql_fetch_array($res)) {
		$award = '';
		if($row[upgrade]>0 && $mtype==1) {
			$accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$row[upgrade]"),0);
			$award.= ' or 1 day '.$accname.' Upgrade';
			}
		if($row[cash]>0) { $award.=' or $'.$row[cash].' USD cash'; }
		if($row[credits]>0) { $award.=' or '.$row[credits].' Credits'; }
		if($row[bannerimps]>0) { $award.=' or '.$row[bannerimps].' Banner Imps'; }
		if($row[textimps]>0) { $award.=' or '.$row[textimps].' Text Link Imps'; }
		if($award == '') { $award = 'a prize'; } else { $award = substr($award, 4); }
		echo '<br><br><br><u><font face="Arial" size="3" color="#0000FF">Surf '.$row[surf].' Pages</u></font><br><br><font face="Arial" size="2">Receive '.$award;
		}
	} else {
	echo '<p>There are no active surfer rewards available at the moment.';
	}
	
	echo '</font></td></tr></table>';
  
include("claim_promos.php");

echo '<br><br><br>
</center>
';	

load_template ($theme_dir."/footer.php");
exit;
?>