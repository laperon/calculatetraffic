<?php
////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

// Cross Promo Addition
// �2011 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

/*
This file checks to see how many pages a member surfed for cross promo rewards.
*/

require_once "inc/filter.php";

include "inc/config.php";
mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname) or die("Error: Unable to select database");

$usermail = $_GET['usermail'];
$userip = $_GET['userip'];

$promokey = $_GET['promokey'];

if (!isset($usermail) || !isset($userip) || $userip=="" || !isset($promokey) || $promokey=="") {
	echo("Error: Invalid Request");
	exit;
}

$getpromoinfo = mysql_query("Select createdby, timediff from ".$prefix."active_surfer_promos where promokey='".$promokey."'");
if (mysql_num_rows($getpromoinfo) == 0) {
	echo("Error: Invalid Promo ID");
	exit;
}

$createdby = mysql_result($getpromoinfo, 0, "createdby");
$timediff = mysql_result($getpromoinfo, 0, "timediff");

$siteurl = mysql_result(mysql_query("Select affurl from `".$prefix."settings` where id=1 limit 1"), 0);
$spliturl = explode("/", $siteurl);
$thisdomain = $spliturl[2];
$thisdomain = strtolower(str_replace("www.", "", $thisdomain));

if ($createdby == $thisdomain) {
	$promoday = date("Y-m-d");
} else {
	$time = time();
	$promotime = $time+$timediff;
	$promoday = date("Y-m-d", $promotime);
}

// Try Email Search
$getuserinfo = mysql_query("Select Id from ".$prefix."members where status='Active' and MD5(email)='".$usermail."'");

if (mysql_num_rows($getuserinfo) == 0) {
	// Try IP Search
	$getuserinfo = mysql_query("Select Id from ".$prefix."members where status='Active' and lastip='".$userip."'");
}

if (mysql_num_rows($getuserinfo) >= 1) {
	$userid = mysql_result($getuserinfo, 0, "Id");
	$getpromoclicks = mysql_query("Select clickstoday from `".$prefix."active_surfer_promo_clicks` where userid=$userid and promokey='".$promokey."' and date='".$promoday."'");
	if (mysql_num_rows($getpromoclicks) == 0) {
		$promoclicks = 0;
	} else {
		$promoclicks = mysql_result($getpromoclicks, 0, "clickstoday");
	}
	echo("Count:".$promoclicks.":End");
} else {
	echo("User Not Found");
}

exit;
?>