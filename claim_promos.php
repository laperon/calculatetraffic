<?php
////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

// Cross Promo Addition
// �2011 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

require_once "inc/filter.php";

if (!isset($userid) || ($userid != intval($_SESSION["userid"]))) {
	header("Location: claim.php");
	exit;
}

$userip = $_SERVER['REMOTE_ADDR'];
if ($userip == "") {
	$userip = "INVALID";
}

$siteurl = mysql_result(mysql_query("Select affurl from `".$prefix."settings` where id=1 limit 1"), 0);
$spliturl = explode("/", $siteurl);
$thisdomain = $spliturl[2];
$thisdomain = strtolower(str_replace("www.", "", $thisdomain));

$getpromos = mysql_query("Select * from `".$prefix."active_surfer_promos` where active>0 AND (starttime+timediff)<$time AND (endtime+timediff)>$time");

if (mysql_num_rows($getpromos) > 0) {
for ($i = 0; $i < mysql_num_rows($getpromos); $i++) {

	$claim = "";

	$promoid = mysql_result($getpromos, $i, "id");
	$starttime = mysql_result($getpromos, $i, "starttime");
	$endtime = mysql_result($getpromos, $i, "endtime");
	$createdby = mysql_result($getpromos, $i, "createdby");
	$timediff = mysql_result($getpromos, $i, "timediff");
	$promokey = mysql_result($getpromos, $i, "promokey");
	
	$needsurfed = mysql_result($getpromos, $i, "surf");
	
	$promotime = $time+$timediff;
	$promoday = date("Y-m-d", $promotime);
	
	$getpromoclicks = mysql_query("Select clickstoday from `".$prefix."active_surfer_promo_clicks` where userid=$userid and promoid=$promoid and date='".$promoday."'");
	if (mysql_num_rows($getpromoclicks) == 0) {
		$promoclicks = 0;
	} else {
		$promoclicks = mysql_result($getpromoclicks, 0, "clickstoday");
	}
	
	$completedpromo = 1;
	
	if ($promoclicks < $needsurfed) {
		$completedpromo = 0;
		$thisstatus = "Surfed ".$promoclicks." Pages";
	} else {
		$thisstatus = "Complete";
	}

	$promoexchanges = array();	
	$getexchanges = mysql_query("Select domain, refid from `".$prefix."active_surfer_sites` where promoid=".$promoid." and domain!='".$thisdomain."' order by domain asc");
	for ($j = 0; $j < mysql_num_rows($getexchanges); $j++) {
	
		$promoexchanges[$j]['domain'] = mysql_result($getexchanges, $j, "domain");
		$promoexchanges[$j]['refid'] = mysql_result($getexchanges, $j, "refid");
		
		// Connect To Other Exchange
		$checkurl = "http://".mysql_result($getexchanges, $j, "domain")."/claim_checkuser.php?promokey=".$promokey."&usermail=".md5($useremail)."&userip=".$userip;
		
		$urlinfo = parse_url($checkurl);
		$hostname = $urlinfo['host'];
		if ($hostname == "" || gethostbyname($hostname) == $hostname) {
			$promoexchanges[$j]['status'] = "Could Not Connect";
			$completedpromo = 0;
		} else {
			$port = 80;
			$timeout = 5;
			$headers = "GET ".$checkurl." HTTP/1.1\r\n";
			$headers .= "Host: ".$hostname."\r\n";
			$headers .= "Referer: http://".$_SERVER["SERVER_NAME"]."\r\n";
			$headers .= "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
			$headers .= "Connection: close\r\n";
			$headers .= "Accept: */*\r\n";
			$headers .= "\r\n";
			$checkreturn = "";
			$attempts = 0;
			while((strpos($checkreturn, "200 OK") === false) && (strpos($checkreturn, "302 Found") === false) && ($attempts<2)) {
				$connection = fsockopen($hostname, $port, $errno, $errstr, $timeout);
				if ($connection) {
					stream_set_timeout($connection, $timeout);
					fwrite($connection,$headers);
					//Read the reply
					$checkreturn = "";
					$readruns = 0;
					while (!feof($connection) && !$checktimeout['timed_out'] && $readruns < 50) {
						$checkreturn .= fread($connection, 10000);
						$checktimeout = stream_get_meta_data($connection);
						$readruns++;
					}
					fclose($connection);
				}
				$attempts = $attempts+1;
			}
			
			if((strpos($checkreturn, "200 OK") === false) && (strpos($checkreturn, "302 Found") === false)) {
				$promoexchanges[$j]['status'] = "Could Not Connect";
				$completedpromo = 0;
			} elseif (strpos($checkreturn, "User Not Found") !== false) {
				$promoexchanges[$j]['status'] = "<- Click To Join";
				$completedpromo = 0;
			} elseif (strpos($checkreturn, "Count:") === false) {
				$promoexchanges[$j]['status'] = "Could Not Connect";
				$completedpromo = 0;
			} else {
				$colonpos = strpos($checkreturn, "Count:");
				$colonpos = $colonpos+6;
				$surfcount = trim(substr($checkreturn, $colonpos));
				
				$surfcountsplit = explode(':', $surfcount);
				$surfcount = trim($surfcountsplit[0]);
				
				if ($surfcount < $needsurfed) {
					$completedpromo = 0;
					$promoexchanges[$j]['status'] = "Surfed ".$surfcount." Pages";
				} else {
					$promoexchanges[$j]['status'] = "Complete";
				}
			}
		}
		
		// End Connect To Other Exchange
		
	}


	// has member already claimed today?
	$res = mysql_query("SELECT id FROM {$prefix}active_surfers WHERE usrid=$userid AND promoid=".$promoid." AND claimed='$promoday'");
	if(mysql_num_rows($res)) { $alreadyclaimed = true; } else { $alreadyclaimed = false; }
	
	// minimum number required for award?
	$res = mysql_query("SELECT surf FROM {$prefix}active_surfer_promos WHERE active>0 AND id=".$promoid." LIMIT 1");
	if(mysql_num_rows($res)) { $min = @mysql_result($res,0); } else { $min = 0; }
	
	// get info for best prize available...
	$res = mysql_query("SELECT * FROM {$prefix}active_surfer_promos WHERE active>0 AND id=".$promoid." AND surf<=$promoclicks LIMIT 1");
	$row = mysql_fetch_array($res);
	
	$prize = $_POST[prize];
	if($prize=="upgrade") { $amount = $row[upgrade]; }
	if($prize=="cash") { $amount = $row[cash]; }
	if($prize=="credits") { $amount = $row[credits]; }
	if($prize=="bannerimps") { $amount = $row[bannerimps]; }
	if($prize=="textimps") { $amount = $row[textimps]; }
	
	if($_GET['promoid'] == $promoid && $completedpromo==1 && $amount>0) {
	
		if($alreadyclaimed == true) {
			$claim .=  '<font color="red">You have already submitted a claim today!</font>';
		} else {
			mysql_query("INSERT INTO {$prefix}active_surfers (usrid,promoid,$prize,views,claimed,prizetype) VALUES ($userid,$promoid,$amount,$promoclicks,'$promoday','$prize')");
			// should prize be awarded automatically?
			if($row[active] == 2) {
				$ffrm = mysql_insert_id();
				$amount = $row["$prize"];
				if($prize=="upgrade") {
					$upgend = date("Y-m-d",strtotime($upgend)+86400); // add 24 hours to existing expiry date
					if($upgend <= date("Y-m-d")) { $upgend = date("Y-m-d",time()+129600); } // add 36 hours to current PHP time
					if($acctype > $amount) { $amount = $acctype; } // don't downgrade existing members!
					mysql_query("UPDATE {$prefix}members SET mtype=$amount, upgend='$upgend' WHERE Id=$userid LIMIT 1") or die(mysql_error());
				} elseif($prize=="cash") {
					mysql_query("INSERT INTO {$prefix}sales (affid,saledate,itemamount,itemname,commission,txn_id,prize) VALUES ($userid,NOW(),'$amount','Active Surfer Reward','$amount','0',1)") or die(mysql_error());
				} else {
					mysql_query("UPDATE {$prefix}members SET $prize=$prize+$amount WHERE Id=$userid LIMIT 1") or die(mysql_error());
				}
				mysql_query("UPDATE `{$prefix}active_surfers` SET awarded=$time WHERE id=$ffrm") or die(mysql_error());
				// Query settings table for sitename
				$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
				$row=@mysql_fetch_array($res);
				$sitename=$row["sitename"];
				$replyaddress=$row["replyaddress"];
				$subject="Your Active Surfer Reward";
				$headers = 'From: '.$sitename.' <'.$replyaddress.'>';
				$res = mysql_query("SELECT * FROM {$prefix}members WHERE Id=$userid");
				$row = mysql_fetch_array($res);
				$emailbody = mysql_result(mysql_query("SELECT template_data FROM {$prefix}templates WHERE template_name='Active Surfer Reward'"), 0);
				if($prize=="upgrade") {
					$accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$amount"),0);
					$reward = '1 day '.$accname.' Upgrade';
				}
				elseif($prize=="commission") { $reward = '$'.$amount.' USD cash'; }
				elseif($prize=="credits") { $reward = $amount.' Credits'; }
				elseif($prize=="bannerimps") { $reward = $amount.' Banner Impressions'; }
				elseif($prize=="textimps") { $reward = $amount.' Text Link Impressions'; }
				/*
				$emailbody = str_replace("#FIRSTNAME#",$row[firstname],$emailbody);
				$emailbody = str_replace("#CLICKS#",$row[clickstoday],$emailbody);
				$emailbody = str_replace("#REWARD#",$reward,$emailbody);
				$emailbody = str_replace("#SITENAME#",$sitename,$emailbody);
				mail($row[email], $subject, $emailbody, $headers);
				*/
				$claim .= "<em>Your reward has been added to your account.</em>";
			} else {
				$claim .=  '<p>Your claim has been received and should be processed shortly.';
			}
		}

	} else {

		if(!$min) {
			$claim .=  'There are no surfer rewards for this promo at the moment.';
		} else {
			if ($completedpromo==1) {
				if($alreadyclaimed == false) {
				$claim .=  '</b></font><p>Choose your prize and click the button to submit your claim:
<form action="claim.php?promoid='.$promoid.'" method="POST">
<select name="prize">
<option value="none">PLEASE SELECT:</option>';
				if($row[upgrade]>0 && ($mtype==1 || $mtype==$row[upgrade])) {
					$accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$row[upgrade]"),0);
					$claim .=  '<option value="upgrade">1-day '.$accname.' Upgrade</option>';
				}
				if($row[cash]>0) { $claim .=  '<option value="cash">$'.$row[cash].' USD cash</option>'; }
				if($row[credits]>0) { $claim .=  '<option value="credits">'.$row[credits].' Credits</option>'; }
				if($row[bannerimps]>0) { $claim .=  '<option value="bannerimps">'.$row[bannerimps].' Banner Imps</option>'; }
				if($row[textimps]>0) { $claim .=  '<option value="textimps">'.$row[textimps].' Text Link Imps</option>'; }
				$claim .=  '
</select>
<p><input type="submit" value="Submit Claim">
</form>
<b>';
				} else {
					$claim .=  '<p>You have already claimed a reward for this promo today.</p>';
				}
			}
			$claim .= '</b></font>';
		}
	}

	echo '<br><br><br>
	<table border="1" bordercolor="black" cellpadding="5" cellspacing="0" width="500">
	<tr><td align="center">
	<font face="Tahoma" size="4"><b>Special Promo ';
	if ($i > 0) {
		echo '#'.$i+2;
	}
	echo'</b></font>
	<p>
	<b>You can claim only one reward per day for each promo</b><br>
	<br>The current time and date for this promo is:<b> ' . date('l jS \of F Y h:i:s A', $promotime) . '</b>
	<br>You have ' . (23 - date("H", $promotime)) . ' hours and ' . (59 - date("i", $promotime)). ' minutes left to submit a claim today for this promo.
	<br>' . $claim . '<br><br>';
	
	$res = mysql_query("select * FROM `{$prefix}active_surfer_promos` WHERE active>0 AND id=".$promoid." LIMIT 1");
	if(mysql_num_rows($res)) {
	    while($row=mysql_fetch_array($res)) {
		$award = '';
		if($row[upgrade]>0 && $mtype==1) {
			$accname = @mysql_result(mysql_query("SELECT accname FROM {$prefix}membertypes WHERE mtid=$row[upgrade]"),0);
			$award.= ' or 1 day '.$accname.' Upgrade';
		}
		if($row[cash]>0) { $award.=' or $'.$row[cash].' USD cash'; }
		if($row[credits]>0) { $award.=' or '.$row[credits].' Credits'; }
		if($row[bannerimps]>0) { $award.=' or '.$row[bannerimps].' Banner Imps'; }
		if($row[textimps]>0) { $award.=' or '.$row[textimps].' Text Link Imps'; }
		if($award == '') { $award = 'a prize'; } else { $award = substr($award, 4); }
		echo '<br><br><br><u><font face="Arial" size="3" color="#0000FF">Surf '.$row[surf].' Pages At Each Of These Exchanges:</u></font><br><br>';
		
		echo '<b><a target="_blank" href="index.php">'.$thisdomain.'</a> ... '.$thisstatus.'</b><br><br>';
		
		for ($j = 0; $j < count($promoexchanges); $j++) {
			echo '<b><a target="_blank" href="http://'.$promoexchanges[$j]['domain'].'/index.php?rid='.$promoexchanges[$j]['refid'].'">'.$promoexchanges[$j]['domain'].'</a> ... '.$promoexchanges[$j]['status'].'</b><br><br>';
		}
		
		echo '<font face="Arial" size="2">Receive '.$award;
	    }
	} else {
		echo '<p>There are no surfer rewards for this promo at the moment.';
	}

	echo '</font></td></tr></table>';
}
}

?>