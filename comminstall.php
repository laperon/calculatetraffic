<?php

// Pay With Commissions Mod
// For The LFMTE Script
// http://thetrafficexchangescript.com

require_once "inc/filter.php";
include "inc/config.php";

$mconn=@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname,$mconn) or die( "Unable to select database");

echo("<html><body><p><center><b>Starting Installation...</b></p>");

$checkver = mysql_result(mysql_query("SELECT ver FROM ".$prefix."settings"), 0);
if ($checkver < "2.09") {
	echo("<br><br><font size=\"2\">You are running an old version of the LFMTE. <a target=\"_blank\" href=\"http://thetrafficexchangescript.com/updatecheck.php?ver=".$checkver."\"><b>Click Here To Update</b></a> then please run this installer again.</font><br><br>");
	exit;
}

if(!file_exists("commdb.php")) {
	echo("<br><br>The mod is either already installed, or installation files are missing.<br><br>");
	exit;
}

echo("<br><br>Updating database...<br><br>");

if(file_exists("commdb.php")) {
	include("commdb.php");
	@unlink("commdb.php");
}

echo("<br><br>Removing temporary installation files...<br><br>");

echo("<br><br><b>Installation Complete.</b></center></body></html>");

exit;
?>