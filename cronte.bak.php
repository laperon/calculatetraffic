<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

//TE Cron Job

if (!isset($prefix)) {
	echo("You should only run the admin/maint.php cron job instead of cronte.php");
	exit;
}

$currentdate = date("Y-m-d");
$yesterdate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));

$getlastcron = mysql_query("Select tecron from ".$prefix."settings");

if (mysql_num_rows($getlastcron) == 0) {
mysql_close;
exit;
}

$lastcron = mysql_result($getlastcron, 0, "tecron");

if ($lastcron == $currentdate) {
mysql_close;
exit;
}

@mysql_query("Update ".$prefix."settings set tecron='$currentdate'");

//Downgrade Expired Membership
@mysql_query("Update ".$prefix."members set mtype=1, upgend='0000-00-00' where upgend='".$currentdate."'");

//Record Stats
$creditstoday = mysql_result(mysql_query("Select SUM(creditstoday) from ".$prefix."members"),0);
$creditstoday = round($creditstoday);
$hitstoday = mysql_result(mysql_query("Select SUM(hitstoday) from ".$prefix."msites"),0);
$persitetoday = mysql_result(mysql_query("Select hitstoday from ".$prefix."msites order by hitstoday desc limit 1"),0);
$perbantoday = mysql_result(mysql_query("Select hitstoday from ".$prefix."mbanners order by hitstoday desc limit 1"),0);
$pertexttoday = mysql_result(mysql_query("Select hitstoday from ".$prefix."mtexts order by hitstoday desc limit 1"),0);
$memberssurfed = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."members where clickstoday>0"),0);
$newsignups = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."members where joindate <= '".$yesterdate." 23:59:59' and joindate >= '".$yesterdate."'"), 0);
@mysql_query("Insert into ".$prefix."datestats (date, crdsearned, hitsdelivered, perurl, perbanner, pertext, memsurfed, signups) VALUES ('$yesterdate', $creditstoday, $hitstoday, $persitetoday, $perbantoday, $pertexttoday, $memberssurfed, $newsignups)");

// Get surflogdays Value
$getlogdays = mysql_query("Select surflogdays from ".$prefix."settings");
$logdays = mysql_result($getlogdays, 0, "surflogdays");
if ($logdays > 0) {
	$logdate = strftime("%Y-%m-%d", strtotime("$currentdate + ".$logdays." days ago"));
	@mysql_query("Delete from ".$prefix."surflogs where date < '".$logdate."'");
}

//Update Surf Logs
$getmembers = mysql_query("Select Id, timetoday, clickstoday from ".$prefix."members");
if (mysql_num_rows($getmembers) > 0) {
for ($i = 0; $i < mysql_num_rows($getmembers); $i++) {
	$userid = mysql_result($getmembers, $i, "Id");
	$timetoday = mysql_result($getmembers, $i, "timetoday");
	$clickstoday = mysql_result($getmembers, $i, "clickstoday");
	$hitsdeliv = mysql_result(mysql_query("Select SUM(hitstoday) from ".$prefix."msites where memid=$userid"), 0);
	if ($hitsdeliv == NULL) { $hitsdeliv=0; }
	$bandeliv = mysql_result(mysql_query("Select SUM(hitstoday) from ".$prefix."mbanners where memid=$userid"), 0);
	if ($bandeliv == NULL) { $bandeliv=0; }
	$textdeliv = mysql_result(mysql_query("Select SUM(hitstoday) from ".$prefix."mtexts where memid=$userid"), 0);
	if ($textdeliv == NULL) { $textdeliv=0; }
	$newrefs = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."members where status='Active' and refid=$userid and joindate <= '".$yesterdate." 23:59:59' and joindate >= '".$yesterdate."'"), 0);
	
	if ($clickstoday>0 || $hitsdeliv>0 || $bandeliv>0 || $textdeliv>0 || $newrefs>0) {
		@mysql_query("Insert into ".$prefix."surflogs (date, userid, clicks, time, hitsdeliver, bandeliver, textdeliver, newrefs) VALUES ('$yesterdate', $userid, $clickstoday, $timetoday, $hitsdeliv, $bandeliv, $textdeliv, $newrefs)");
	}
}
}

//Update Stats
@mysql_query("Update ".$prefix."members set creditsyesterday=creditstoday, creditstoday=0");
@mysql_query("Update ".$prefix."members set clicksyesterday=clickstoday, clickstoday=0");
@mysql_query("Update ".$prefix."msites set hitsyesterday=hitstoday, hitstoday=0");
@mysql_query("Update ".$prefix."mbanners set hitsyesterday=hitstoday, hitstoday=0");
@mysql_query("Update ".$prefix."mtexts set hitsyesterday=hitstoday, hitstoday=0");

//Reset Daily Limits
@mysql_query("Update ".$prefix."members set timetoday=0 where timetoday>0");

//Update the Word Search Game
$getresetdays = mysql_query("Select value from ".$prefix."wssettings where field='daysreset' limit 1");
$resetdays = mysql_result($getresetdays, 0, "value");
$checklastreset = mysql_query("Select wsreset from ".$prefix."settings");
$lastreset = mysql_result($checklastreset, 0, "wsreset");
$doreset = strftime("%Y-%m-%d", strtotime("$lastreset + $resetdays days"));
if (($currentdate >= $doreset) && ($resetdays>0)) {
	@mysql_query("Update ".$prefix."settings set wsreset='$currentdate'");
	@mysql_query("UPDATE ".$prefix."members SET wsreset=1 where wsword != 1");
}

//Fix Any Frozen Sites
$getsites = mysql_query("Select memid from ".$prefix."msites where state=1 and credits>=1");
for ($i = 0; $i < mysql_num_rows($getsites); $i++) {
	$memid = mysql_result($getsites, $i, "memid");
	@mysql_query("Update ".$prefix."members set actsite=1 where Id=$memid and status='Active' limit 1");
}

//Fix Any Frozen Banners
$getbanners = mysql_query("Select memid from ".$prefix."mbanners where state=1 and imps>=1");
for ($i = 0; $i < mysql_num_rows($getbanners); $i++) {
	$memid = mysql_result($getbanners, $i, "memid");
	@mysql_query("Update ".$prefix."members set actbanner=1 where Id=$memid and status='Active' limit 1");
}

//Fix Any Frozen Text Ads
$gettexts = mysql_query("Select memid from ".$prefix."mtexts where state=1 and imps>=1");
for ($i = 0; $i < mysql_num_rows($gettexts); $i++) {
	$memid = mysql_result($gettexts, $i, "memid");
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$memid and status='Active' limit 1");
}

//Delete Old Surf Logs
$cronsetting = mysql_result(mysql_query("SELECT sstats from ".$prefix."settings where 1"), 0);
if (is_numeric($cronsetting)) {
	$delete = time() - $cronsetting*24*3600;
	@mysql_query("DELETE FROM ".$prefix."sstats WHERE adate < '$delete'");
	@mysql_query("OPTIMIZE TABLE ".$prefix."sstats");
}

//Delete Old Credit Boosts
$t = time();
@mysql_query("Delete from ".$prefix."cboost where endtime<$t");

//Delete Old Auto Track Records
$tenminago = time() - 600;
@mysql_query("DELETE FROM `".$prefix."autotrack` WHERE timehit < '".$tenminago."'");

//Delete Unverified Accounts
$getdeldays = mysql_query("Select unveridel from ".$prefix."settings");
$deldays = mysql_result($getdeldays, 0, "unveridel");
if ($deldays > 0) {
	$deldate = strftime("%Y-%m-%d", strtotime("$currentdate + ".$deldays." days ago"));
	@mysql_query("Delete from ".$prefix."members where status='Unverified' AND joindate < '".$deldate."'");
}

// Remind Unverified Accounts
$getremdays = mysql_query("Select unverirem from ".$prefix."settings");
$remdays = mysql_result($getremdays, 0, "unverirem");
if ($remdays > 0) {

	// Query settings table for sitename etc
	$res=@mysql_query("SELECT sitename,replyaddress FROM ".$prefix."settings") or die("1040: Unable to find ".$prefix."settings!");
	$row=@mysql_fetch_array($res);
	$sitename=$row["sitename"];
	$replyemail=$row["replyaddress"];
	
	// Get the reminder email
	$eres=mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Verify Reminder'");
	if (mysql_num_rows($eres) > 0) {
		$erow=@mysql_fetch_array($eres);
		
		$remdate = strftime("%Y-%m-%d", strtotime("$currentdate + ".$remdays." days ago"));
		$getmem = mysql_query("Select email from ".$prefix."members where status='Unverified' AND DATE(joindate) = '".$remdate."'");
			
			if (mysql_num_rows($getmem) > 0) {
			for ($i = 0; $i < mysql_num_rows($getmem); $i++) {
				$member_email = mysql_result($getmem, $i, "email");
				$msgbody=translate_site_tags($erow["template_data"]);
				$msgbody=translate_user_tags($msgbody,$member_email);
				
				// Send message
				$headers = "From: ".$sitename." <".$replyemail.">"."\r\n";
				$headers .= "Return-Path: ".$sitename." <".$replyemail.">"."\r\n";
				$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
				$subject = $sitename." Verification Reminder";
				mail($member_email,$subject,$msgbody,$headers);
			}
			}
	}
}
// End Remind Unverified Accounts

mysql_close;
exit;
?>