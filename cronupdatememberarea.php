<?php


require_once "inc/filter.php";

require "admin/templates_defaults.php";

include "inc/config.php";
mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname);

$current_time = date("Y-m-d H:i");

$cron_check = mysql_query("SELECT * FROM cron_jobs WHERE `type`='update_member_area' AND end_time LIKE '" . $current_time . "%' AND status='start'" ) or die(mysql_error());
$cron_check = mysql_fetch_array($cron_check);

function unhtmlentities($string)
{
    // replace numeric entities
    $string = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $string);
    $string = preg_replace('~&#([0-9]+);~e', 'chr(\\1)', $string);
    // replace literal entities
    $trans_tbl = get_html_translation_table(HTML_ENTITIES);
    $trans_tbl = array_flip($trans_tbl);
    return strtr($string, $trans_tbl);
}

$hres=@mysql_query("SELECT template_data FROM ".$prefix."membertypes WHERE mtid=5");

$hrow=mysql_fetch_array($hres);
// Decode template data
if (function_exists('html_entity_decode'))
{
    $html=html_entity_decode($hrow["template_data"]);
}
else
{
    $html=unhtmlentities($hrow["template_data"]);
}

// Update All Member Type Templates

if(!empty($html) && $cron_check) {
    echo 'works';
    $getaccounts = @mysql_query("Select mtid from `" . $prefix . "membertypes` order by mtid asc");
    for ($i = 0; $i < mysql_num_rows($getaccounts); $i++) {
        $accid = mysql_result($getaccounts, $i, "mtid");

        $getoldtemplate = @mysql_query("SELECT template_data from " . $prefix . "membertypes WHERE mtid=" . $accid) or die(mysql_error());
        if (mysql_num_rows($getoldtemplate) > 0) {
            $oldtemplate = addslashes (mysql_result($getoldtemplate, 0, "template_data"));

            // Make sure the old template is already saved
            $backupexists = mysql_result(mysql_query("SELECT COUNT(*) from " . $prefix . "memtemplates_backups where mtid='" . $accid . "' and template_data='" . $oldtemplate . "'"), 0);

            if ($backupexists < 1 && (!isset($default_templates['Members Area']) || ($oldtemplate != $default_templates['Members Area']))) {
                // We don't know when this template was saved, so set 1 day ago
                $tempsavetime = date("Y-m-d H:i:s", time() - 86400);

                @mysql_query("INSERT INTO " . $prefix . "memtemplates_backups (savetime, mtid, template_data) VALUES ('" . $tempsavetime . "', '" . $accid . "', '" . $oldtemplate . "')") or die(mysql_error());
            }

            // Save new template backup
            mysql_query("INSERT INTO " . $prefix . "memtemplates_backups (savetime, mtid, template_data) VALUES (NOW(), '" . $accid . "', '" . $_POST["news"] . "')") or die(mysql_error());

            // Check and remove extra previous versions
            $countprev = mysql_result(@mysql_query("SELECT COUNT(*) from " . $prefix . "memtemplates_backups where mtid='" . $accid . "'"), 0);

            if ($countprev > $keep_prev) {
                $num_to_del = $countprev - $keep_prev;

               // mysql_query("DELETE FROM " . $prefix . "memtemplates_backups WHERE mtid='" . $accid . "' ORDER BY savetime ASC LIMIT " . $num_to_del) or die(mysql_error());
            }
        }

    }
    // Save new template
    mysql_query("UPDATE " . $prefix . "membertypes SET template_data='" . addslashes($html). "' WHERE mtid>=0") or die(mysql_error());

    //Change status cron task
    $cron_status = mysql_query("UPDATE cron_jobs SET status='completed' WHERE `type`='update_member_area' AND end_time LIKE '" . $current_time . "%'") or die(mysql_error());
    if(!$cron_status) {
        mysql_query("UPDATE cron_jobs SET status='error' WHERE `type`='update_member_area' AND end_time LIKE '" . $current_time . "%'") or die(mysql_error());
    }
} else {
    echo "Error";
}
