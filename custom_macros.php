<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.17
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////


// Replace Custom Field Name Macros

$customfieldrow[] = array();
$res = array();

$res=@mysql_query("SELECT * FROM ".$prefix."customfields") or die("Custom Field macro error: ".mysql_error());
while($customfieldrow=@mysql_fetch_array($res)) {
	
	$fieldmacro="#".$customfieldrow["name"]."#";
	if(stristr($textstring,$fieldmacro)) {
		
		$getfieldvalue = mysql_query("SELECT fieldvalue FROM `".$prefix."customvals` WHERE fieldid='".$customfieldrow["id"]."' AND userid='".$row["Id"]."'");
		if (mysql_num_rows($getfieldvalue) > 0) {
			$fieldvalue = mysql_result($getfieldvalue, 0, "fieldvalue");
		} else {
			$fieldvalue = "";
		}
		
		$textstring = str_ireplace($fieldmacro, $fieldvalue, $textstring);
		
	}
}

// End Replace Custom Field Name Macros

?>