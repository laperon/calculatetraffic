<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.04
// Copyright ©2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();

if(isset($_SESSION["uid"]) && $_SESSION["uid"] != "") {
	include "inc/userauth.php";
	$userid = $_SESSION["userid"];
	
	$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
	$useremail = mysql_result($getuserdata, 0, "email");
	$mtype = mysql_result($getuserdata, 0, "mtype");
	$joindate = mysql_result($getuserdata, 0, "joindate");

	include "inc/theme.php";
	
	load_template ($theme_dir."/header.php");
	//load_template ($theme_dir."/mmenu.php");
	
	// Get homepage body
	$bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='faqs'") or die("Unable to find FAQs template");
	$brow=@mysql_fetch_array($bres);
	if (function_exists('html_entity_decode'))
	{
	   $page_content = html_entity_decode($brow["template_data"]);
	}
	else
	{
	   $page_content = unhtmlentities($brow["template_data"]);
	}
	
	
	$page_content = translate_user_tags($page_content,$useremail);
	
	$page_content = translate_site_tags($page_content);
	
	//load_template ($theme_dir."/content.php");
        load_template ($theme_dir."/content_disclaimer.php");
	load_template ($theme_dir."/footer.php");
	
} else {

	include "inc/config.php";
	include "inc/funcs.php";
	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");
	
	include "inc/theme.php";
	
	load_template ($theme_dir."/header.php");
	
	// Get homepage body
	$bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='about_us'") or die("Unable to find FAQs homepage template");
	$brow=@mysql_fetch_array($bres);
	if (function_exists('html_entity_decode'))
	{
	   $page_content = html_entity_decode($brow["template_data"]);
	}
	else
	{
	   $page_content = unhtmlentities($brow["template_data"]);
	}
	
	$page_content = translate_site_tags($page_content);

	load_template ($theme_dir."/content_disclaimer.php");
	load_template ($theme_dir."/footer.php");
}

?>
</body>
</html>