<?php ob_start(); ?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100' rel='stylesheet' type='text/css'>
    <title>Responsive email template</title>
  </head>

  <body bgcolor="#d9dfe2" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0"
        style="-webkit-font-smoothing:antialiased;width:100% !important;background-color:#d9dfe2;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;-webkit-text-size-adjust:none;">

  <span style="display: none;font-size: 0px; color:#ECF0F5; line-height: 0;">Engaging preheader text goes here!</span>

  <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#d9dfe2"
         style="font-family: 'Roboto',sans-serif !important;font-size:16px;line-height:20px;color:#555;">
    <tr>
      <td bgcolor="#ffffff" width="100%">


        <table width="600" align="center">
          <tr>
            <td>


              <!--Content wrapper-->


              <table cellpadding="0" cellspacing="0" width="100%" cellpadding="0" cellspacing="0" border="0"
                     align="center"
                     style="border-top: solid 5px #cdcdd5;border-left: solid 1px #cdcdd5;border-right: solid 1px #cdcdd5;border-bottom: solid 1px #cdcdd5;">
                <tr>
                  <td width="150" valign="top" style="padding: 10px 0;border-right: solid 1px #cdcdd5;">

                    <img
                      style="position:relative; left:50%;transform:translateX(-50%);-webkit-transform:translateX(-50%)"
                      src="http://calculatingtraffic.com/email_adv/images/man_03.png" width="100" alt="logo"
                      border="0"/>
                    <p align="center" style="margin: 5px 0 0 0;">Darrel Dean</p>

                  </td>
                  <td width="" valign="middle">
                    <a href="http://calculatingtraffic.com">
                      <img style="margin-left: 20px" src="http://calculatingtraffic.com/email_adv/images/logo_03.png"
                           width="250" alt="logo" border="0"/>
                    </a>

                  </td>
                </tr>
              </table>


              <table
                style="border-left: solid 1px #cdcdd5;border-right: solid 1px #cdcdd5;border-botom: solid 1px #cdcdd5; "
                cellpadding="0" cellspacing="0" width="100%" cellpadding="0" cellspacing="0" border="0" align="center">

                <tr>
                  <td width="150" valign="top" style="border-right: solid 1px #cdcdd5;">
                    <h2 style="font-size: 16px;font-weight: 300;" align="center" style="">Recommended<br/> resourses
                    </h2>
                  </td>
                  <td width="60" valign="middle" style="padding-left: 20px;">
                    <a href="" style="text-decoration: none">
                      <img src="http://calculatingtraffic.com/email_adv/images/fb_icon_03.png" width="35" height="35"
                           alt="logo" border="0"/>
                    </a>
                  </td>
                  <td width="60" valign="middle">
                    <a href="#" style="text-decoration: none">
                      <img src="http://calculatingtraffic.com/email_adv/images/tw_icon_03.png" width="35" height="35"
                           alt="logo" border="0"/>
                    </a>
                  </td>
                  <td width="" valign="middle">
                    <a href="#" style="text-decoration: none">
                      <img src="http://calculatingtraffic.com/email_adv/images/ln_03.png" width="35" height="35"
                           alt="logo" border="0"/>
                    </a>
                  </td>
                </tr>
              </table>


              <table style="border: solid 1px #cdcdd5;" cellpadding="0" cellspacing="0" width="100%" align="center">

                <tr>
                  <td>
                    <table cellpadding="0" cellspacing="0" width="100%" align="center"
                           style="border-bottom: solid 1px #cdcdd5;">
                      <tr>
                        <td rowspan=5 style="padding: 15px 15px 15px 20px ; position: relative;">
                          <table width="100%" align="center" style="/*position: absolute; top: 0;*/">
                            <tr>
                              <td align="left">
                                <h3 style="color: #92bf2c;font-size: 20px;margin: 0 0 10px 0; font-weight: 300">Dear
                                  Friend,</h3>
                                <p style="color: #000;font-size: 14px">
                                  Recently you visited my website:<br>
                                  <a href="http://www.calculatingtraffic.com">www.calculatingtraffic.com</a> and I
                                  noticed that you didn't finish
                                  adding your website to be advertized and be seen by many users who are already members
                                  of
                                  <a href="http://www.calculatingtraffic.com">www.calculatingtraffic.com.</a>
                                  This email is just a friendly reminder for you to come back to my website, register
                                  and get all the benefits of the services:
                                  <br>
                                  Website advertizing <br>
                                  Banner ad advertizing <br>
                                  Text advertizing <br>
                                  Mobile friendly <br>
                                  Automatic commission payments <br>
                                  Professional tracking link service <br>
                                  Please click here to register: <br>

                                  Thank you for visiting <a href="http://www.calculatingtraffic.com">www.calculatingtraffic.com</a>
                                  <br>
                                  Be the best! <br>

                                  Darrell Dean <br>
                                  Owner/Operator <br>
                                  CalculatingTraffic.com <br>


                                  P.S. - Get your Exclusive Promo codes for a TRUCK LOAD <br>
                                  of Ads here! <br>

                                  => <a href="http://www.CalculatingTraffic.com/truckload">www.CalculatingTraffic.com/truckload</a>
                                  <br>
                                </p>
                              </td>
                              <td align="center">
                                <a style="text-decoration: none;" href="http://calculatingtraffic.com"><input
                                    style="background: #02225e;cursor: pointer;color: #ffffff;border: none;border-radius: 5px;padding: 5px 10px;"
                                    type="button" value="Surf Now"/></a>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr></tr>
                      <tr></tr>
                    </table>


                    <table cellpadding="0" cellspacing="0" width="100%" align="center">
                      <tr>
                        <td rowspan=5 style="padding: 15px 15px 15px 20px; position: relative;">

                          <table width="100%" align="center" style="/*position: absolute;top: 0; padding-top: 15px;*/">
                            <tr>
                              <td align="left" width="50%">
                                <h3 style="color: #92bf2c;font-size: 20px;margin: 0 0 10px 0; font-weight: 300"> Refer
                                  others and earn cash</h3>
                                <p style="color: #000;font-size: 14px">Earn up to 50% commissions on referrals`
                                  upgrade<br>Import your friends and earn commission</p>
                              </td>

                              <td align="center" width="50%">
                                <img width="120" src="http://calculatingtraffic.com/email_adv/images/money_03.png"
                                     alt=""/>
                              </td>
                            </tr>
                            <tr>
                              <td align="left" style="padding-top: 15px;">
                                <table>
                                  <td width="60" valign="middle" style="">
                                    <a href="#" style="text-decoration: none">
                                      <img src="http://calculatingtraffic.com/email_adv/images/fb_icon_03.png"
                                           width="35" height="35" alt="logo" border="0"/>
                                    </a>
                                  </td>
                                  <td width="60" valign="middle">
                                    <a href="#" style="text-decoration: none">
                                      <img src="http://calculatingtraffic.com/email_adv/images/tw_icon_03.png"
                                           width="35" height="35" alt="logo" border="0"/>
                                    </a>
                                  </td>
                                  <td width="" valign="middle">
                                    <a href="#" style="text-decoration: none">
                                      <img src="http://calculatingtraffic.com/email_adv/images/g_icon_07.png" width="35"
                                           height="35" alt="logo" border="0"/>
                                    </a>
                                  </td>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td style="position: relative;right:-140px; margin-top: 20px;padding-top: 15px;">
                                <table>
                                  <tr>
                                    <td><a
                                        href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//calculatingtraffic.com&title=&summary=&source="><img
                                          alt=""
                                          src="http://calculatingtraffic.com/email_adv/images/in_share_03.png"></a></td>
                                    <td><a
                                        href="https://www.facebook.com/sharer/sharer.php?u=http%3A//calculatingtraffic.com/aboutus.php"><img
                                          alt=""
                                          src="http://calculatingtraffic.com/email_adv/images/f_share_03.png"></a></td>
                                    <td><a href="https://twitter.com/home?status=http%3A//calculatingtraffic.com"><img
                                          alt=""
                                          src="http://calculatingtraffic.com/email_adv/images/tw_share_03.png"></a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr></tr>
                      <tr></tr>
                    </table>
                  </td>
                </tr>
              </table>

              <table cellpadding="0" cellspacing="0" width="100%" align="center"
                     style="border-left: solid 1px #cdcdd5;border-right: solid 1px #cdcdd5;">
                <tr>
                  <td width="151" height="75" style="background:#d0e39e;"></td>
                  <td width="" valign="top" style="background:#d0e39e;">
                    <table width="100%" style="background:#fff;padding-left: 25px;">
                      <td width="50%" height="35"
                          style="color: #92bf2c;font-size: 22px;margin: 0 0 10px 0; font-weight: 300">In the news
                      </td>
                      <td width="50%" height="35">
                        <table>
                          <tr>
                            <td style="position: absolute; width: 200px">
                              <a style="text-decoration: none;" href="http://www.calculatingtraffic.com">
                                <input
                                  style="background: #02225e;color: #ffffff;border: none;width: 100%;cursor: pointer;border-radius: 5px;padding: 5px 10px; text-align: center;"
                                  type="button" value="Go to the Member`s Area"/>
                              </a>


                            </td>
                            <td style="height: 30px"></td>
                            <td style="height: 30px"></td>
                            <td style="height: 30px"></td>
                          </tr>
                        </table>
                      </td>
                    </table>
                  </td>
                </tr>
              </table>


              <table
                style="border-left: solid 1px #cdcdcf;border-right: solid 1px #cdcdcf;  border-bottom: 5px solid #cdcdcf"
                cellpadding="0" cellspacing="0" width="100%" align="center">
                <tr>
                  <td rowspan=5 style="padding: 15px 15px 15px 20px ;">
                    <h3 style="color: #92bf2c;font-size: 18px;margin: 0 0 10px 0; font-weight: 300">Synchronized
                      surfing: step-by-step
                      instructions</h3>
                    <table width="100%" align="center">
                      <tr>
                        <td align="left">
                          <iframe width="160" height="88" src="https://www.youtube.com/embed/yDCX2XsTnCo"
                                  frameborder="0" allowfullscreen></iframe>
                        </td>
                        <td align="center"><a style="text-decoration: none;"
                                              href="http://calculatingtraffic.com/how_it_works.php"><input
                              style="background: #02225e;cursor: pointer;color: #ffffff;border: none;border-radius: 5px;padding: 5px 10px;"
                              type="button" value="Link to instructions page"/></a></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr></tr>
              </table>
              <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="#78ac00">
                <tr>
                  <td width="100%" align="center">
                    <ul style="padding: 0;">
                      <li style="list-style-type: none; display: inline-block;"><a
                          href="http://calculatingtraffic.com/contactus.php"
                          style="color: #ffffff;font-size: 14px; text-decoration: none;padding: 0 10px;">Contact Us</a>
                      </li>
                      <li style="list-style-type: none; display: inline-block;"><a
                          href="http://calculatingtraffic.com/terms.php"
                          style="color: #ffffff;font-size: 14px; text-decoration: none;padding: 0 10px;">Terms of
                          Services</a></li>
                      <li style="list-style-type: none; display: inline-block;"><a
                          href="http://calculatingtraffic.com/earningsdisclaimer.php"
                          style="color: #ffffff;font-size: 14px; text-decoration: none;padding: 0 10px;">Earnings
                          Disclaimer </a></li>
                      <li style="list-style-type: none; display: inline-block;"><a
                          href="http://calculatingtraffic.com/privacypolicy.php"
                          style="color: #ffffff;font-size: 14px; text-decoration: none;padding: 0 10px;">Privacy
                          Policy</a></li>
                      <li style="list-style-type: none; display: inline-block;"><a
                          href="http://calculatingtraffic.com/aboutus.php"
                          style="color: #ffffff;font-size: 14px; text-decoration: none;padding: 0 10px;">About Us</a>
                      </li>
                    </ul>
                  </td>
                </tr>
                <tr>
                  <td>
                    <table width="100%   ">
                      <tr>
                        <td align="center">
                          <a href="https://play.google.com/store/apps/details?id=com.wCalculatingTraffic"><img
                              style="margin-right: 5px;"
                              src="http://calculatingtraffic.com/email_adv/images/footer_icon2_22.png" alt=""/></a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td width="100%" align="center">
                    <hr width="90%" align="center"/>
                    <span style="position: relative; top: -10px; color: #282828;"> Designed & Developed :</span>
                    <a href="http://bandjcanada.ca/" style="position: relative; top: -10px; color: #282828;"> B&J
                      Canada</a>
                    <img src="http://calculatingtraffic.com/email_adv/images/foooter_logo_26.png" alt=""/>
                  </td>
                </tr>
              </table>


            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </body>
  </html>

<?php
$email_template_adv = ob_get_contents();
ob_end_clean();

?>