<?php

// SurfingGuard Checker v2.2
// �2011-2014 Josh Abbott, http://surfingguard.com
// LFMTE script version

require_once('tepc/pc.php');

function f4check ($URL) {
	global $F4CHECKER;
	
	// Create the checker class
  $hcChecker = new HCChecker();
  
	// INITIATE XML PARSING
	$xml_parser = xml_parser_create();
		
	xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
	xml_set_element_handler($xml_parser, "starting", "ending");
	xml_set_character_data_handler($xml_parser, "RESULTS");

  // Check the URL	
	$data = $hcChecker->checkURL($URL);
		
	$parser = xml_parse($xml_parser, $data, true);
	if (!$parser) {
			$F4CHECKER = array("PAGE_CHECKED"=>"$URL","HTTP_VERSION"=>"BAD XML","HTTP_STATUS"=>"500","WARNING_0"=>"UNABLE TO PARSE CHECKER RESULTS","PAGE_STATUS"=>"PENDING");
	}

  xml_parser_free($xml_parser);
 
	return $F4CHECKER;
}

function starting($parser, $name, $attrs)
{
	global $curname;
	$curname = "$name";
}

function ending($parser, $name)
{
	// IGNORE ENDING
}

function RESULTS($parser, $data)
{
	global $curname,$F4CHECKER;
    $F4CHECKER[$curname] .= htmlentities($data);
}
?>