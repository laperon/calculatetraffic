<?php

// SurfingGuard Checker v2.3
// �2011-2015 Josh Abbott, http://surfingguard.com
// LFMTE script version

// Connect To Database
require_once "inc/filter.php";
include "inc/config.php";
include "inc/funcs.php";
@lfmsql_connect($dbhost,$dbuser,$dbpass);
@lfmsql_select_db($dbname) or die( "Unable to select database");
// End Connect To Database

include "f4monitor.php";

// Delete Old Logs
$keeplogs = lfmsql_result(lfmsql_query("Select value from sg_settings where name='keeplogs'"), 0);
if ($keeplogs > 0) {
	$currentdate = date("Y-m-d");
	$deletedate = strftime("%Y-%m-%d", strtotime("$currentdate + ".$keeplogs." days ago"));
	lfmsql_query("Delete from `".$prefix."f4checks` where date!='0000-00-00' and date < '".$deletedate."'");
}
// End Delete Old Logs

$getsitequery = "SELECT * FROM ".$prefix."msites WHERE state=1 ORDER BY f4check LIMIT 3 ";
$getsites = lfmsql_query($getsitequery);

echo "<b>Preparing to check ".lfmsql_num_rows($getsites)." webpages:</b><hr><br><br><div style=\"margin: 20px;\">";
if (lfmsql_num_rows($getsites) > 0) {
	while ($site = lfmsql_fetch_array($getsites)) {
		
		if (strpos($site['url'], '?') !== false && strpos($site['url'], 'intellisplash=') === false) {
			// Tells IntelliSplash To Not Redirect To Another Site
			$site['url'] = $site['url']."&intellisplash=9";
		}
		
		$F4CHECK = f4check($site['url']);
		
		$getuser = lfmsql_query("SELECT * FROM ".$prefix."members WHERE Id = '${site['memid']}' ");
		$user = lfmsql_fetch_array($getuser);
		    
		echo "<font color=\"blue\"><b>${F4CHECK['PAGE_CHECKED']}</b></font> -- ${F4CHECK['PAGE_STATUS']}<br>";
		if ($F4CHECK['PAGE_STATUS'] == "FAILED") {
			reset($F4CHECK);
			$f4errors = "${F4CHECK['PAGE_CHECKED']}\n";
		    foreach($F4CHECK AS $F4KEY=>$F4VAL) {
			    if (substr($F4KEY,0,3) == 'ERR') {
				    $f4errors .= "    $F4VAL\n\n";

			// Check The Error Action
			
			if (strpos($F4VAL, "Please try later") !== false) {
			// Don't Fail A Problem With SG Checker
				$action = 0;
				$mailmem = 0;
				$mailadmin = 0;
				$F4CHECK['PAGE_STATUS'] = "PASSED";
				$f4errors = "";
			
		   	}  elseif ($F4VAL == "Host Not Found" || $F4VAL == "Page Not Found") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Not Found'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Not Found'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Not Found'"), 0);
		   	} elseif ($F4VAL == "Too Many Redirects") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Too Many Redirects'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Too Many Redirects'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Too Many Redirects'"), 0);
		   	} elseif ($F4VAL == "Has Hidden IFrame") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Hidden IFrame'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Hidden IFrame'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Hidden IFrame'"), 0);
		   	} elseif ($F4VAL == "Has Encoded JavaScript") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Encoded JavaScript'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Encoded JavaScript'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Encoded JavaScript'"), 0);
		   		
		   	} elseif ($F4VAL == "Contains Frame Breaking Code") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Frame Breaking Code'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Frame Breaking Code'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Frame Breaking Code'"), 0);
		   	} elseif (strpos($F4VAL, "Site In Banned List") !== false) {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Site Banned By You'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Site Banned By You'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Site Banned By You'"), 0);
		   	} elseif (strpos($F4VAL, "Site banned") !== false) {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Site In Central Ban'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Site In Central Ban'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Site In Central Ban'"), 0);
		   	} elseif (strpos($F4VAL, "Virus Found") !== false) {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Virus'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Virus'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Virus'"), 0);
		   	} elseif ($F4VAL == "Google - Suspected phishing page") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Google - Suspected phishing page'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Google - Suspected phishing page'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Google - Suspected phishing page'"), 0);
		   	} elseif ($F4VAL == "Google - Suspected malicious code") {
		   		$action = lfmsql_result(lfmsql_query("Select action from sg_monitor_actions where name='Google - Suspected malicious code'"), 0);
		   		$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_monitor_actions where name='Google - Suspected malicious code'"), 0);
		   		$mailmem = lfmsql_result(lfmsql_query("Select mailmem from sg_monitor_actions where name='Google - Suspected malicious code'"), 0);
		   	} else {
		   		$action = 1; // Unknown error - put in unverified state
		   		$mailadmin = 1;
		   		$mailmem = 1;
		   	}
		   	// End Check The Error Action
			
				    
			    }
		    }
		    		    
		    if ($action == 2) {
		   	// Suspend Site
		   	@lfmsql_query("UPDATE ".$prefix."msites SET state=3 WHERE id='${site['id']}' ");
		   	$messagetype = 2;
		    } elseif ($action == 1) {
		    	// Remove Site From Rotation
		    	@lfmsql_query("UPDATE ".$prefix."msites SET state=0 WHERE id='${site['id']}' ");
		    	$messagetype = 1;
		    }
		    
		    // Mail The Member
			if ($mailmem == 1 && $action>0) {
				$getmessage = lfmsql_query("Select subject, body from sg_mail_templates where id=".$messagetype);
				if (lfmsql_num_rows($getmessage) > 0) {
					$msubject = lfmsql_result($getmessage, 0, "subject");
					$mbody = lfmsql_result($getmessage, 0, "body");
					$adminreply = lfmsql_result(lfmsql_query("Select value from sg_settings where name='adminreply'"), 0);
					if ($msubject != "" && $mbody != "") {
						$emailheader = "MIME-Version: 1.0\nContent-type: text/plain; charset=iso-8859-1\nFrom: Traffic Exchange Monitor <".$adminreply.">\nReply-To: Traffic Exchange Monitor <".$adminreply.">\nX-Priority: 3\nX-Mailer: PHP 4\n";
						$mbody = str_replace("[errors]",$f4errors, $mbody);
						mail($user['email'],$msubject,$mbody,$emailheader);
					}
				}
			}
		    // End Mail The Member
		    
		    
		    // Mail The Admin
			if ($mailadmin == 1 && $action>0) {
				$adminmail = lfmsql_result(lfmsql_query("Select value from sg_settings where name='adminemail'"), 0);
		   		$emailheader = "MIME-Version: 1.0\nContent-type: text/plain; charset=iso-8859-1\nFrom: HitsConnect SurfingGuard Monitor <support@surfingguard.com>\nReply-To: HitsConnect SurfingGuard Monitor <support@surfingguard.com>\nX-Priority: 3\nX-Mailer: PHP 4\n";
		   		$message = "The HitsConnect SurfingGuard site monitor\nhas detected a problem with a site that\nuser ID ".$user['Id']." had in rotation.\n\n".$f4errors;
		   		mail($adminmail, "Site Error Detected", $message, $emailheader);
			}
		    //End Mail The Admin
		    
		    
	    } else {
		    $f4errors = "";
	    }
	    
	    lfmsql_query("UPDATE ".$prefix."msites SET f4check='".time()."' WHERE id='${site['id']}' ");

	    $f4warnings = "";
	    reset($F4CHECK);
	    foreach($F4CHECK AS $F4KEY => $F4VAL) {
		    if (substr($F4KEY,0,3) == 'WAR') {
			    $f4warnings .= $F4VAL."\n";
		    }
	    }
	    
	    lfmsql_query("INSERT INTO `".$prefix."f4checks` (`date`,`user`,`page_checked`,`errors`,`warnings`,`status`,`from`) VALUES (NOW(),'${user['Id']}','${F4CHECK['PAGE_CHECKED']}','".$f4errors."','".$f4warnings."','${F4CHECK['PAGE_STATUS']}','f4cron.php')");
	    
		unset($F4CHECK);
		unset($F4CHECKER);
		unset($data);
		unset($curname);
		unset($dmessage);
	}
}
echo "</div>"
?>