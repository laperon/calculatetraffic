<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

// Query settings table for sitename
$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
$row=@mysql_fetch_array($res);

	$sitereported = "currentview";
	$todaysdate = date("Y-m-d");
	
	$getsiteid = mysql_query("Select ".$sitereported." from ".$prefix."members where Id=$userid limit 1");
	$siteid = mysql_result($getsiteid, 0, $sitereported);

	$checkexisting = mysql_query("Select id from ".$prefix."reports where siteid=".$siteid." and action=0");

	if ($siteid < 1 || mysql_num_rows($checkexisting) > 0) {
		echo("<center><h4><b>Thank You</b></h4>");
		echo("<br><p><b>Your report will be reviewed shortly.  You can now continue surfing.</b></p>");
		flush();
	} else {
	
		// Prepare Message
		$reportmess = "Possible Frame Breaker Detection.";
	
		//Submit report
		@mysql_query("Insert into ".$prefix."reports (date, userfrom, siteid, text, action, autosuspended) values (".time().", $userid, $siteid, '$reportmess', 0, 0)");
		$reportid = mysql_insert_id();
		
		echo("<center><h4><b>Thank You</b></h4>");
		echo("<br><p><b>Your report will be reviewed shortly.  You can now continue surfing.</b></p>");
		flush();

		//HitsConnect Autocheck
		if (file_exists("f4checker.php")) {
			include "f4checker.php";
			$gettesturl = mysql_query("Select url from ".$prefix."msites where id=".$siteid." limit 1");
			$testurl = mysql_result($gettesturl, 0, "url");
			$F4CHECK = f4check($testurl);
			if ($F4CHECK['PAGE_STATUS'] == "FAILED") {
				//Automatically suspend the site
			           $FAILEDREASON = "Automatic Frame Breaker Detection

Automatically Suspended By HitsConnect Checker:";
				   reset($F4CHECK);
				   foreach($F4CHECK AS $F4KEY=>$F4VAL) {
					   if (substr($F4KEY,0,3) == 'ERR') {
						   $FAILEDREASON .= " ".$F4VAL;
					   }
				   }
				   @mysql_query("Update ".$prefix."msites set state=3 where id=".$siteid." limit 1");
				   @mysql_query("Update ".$prefix."reports set text='".$FAILEDREASON."', autosuspended=1, action=2 where id=$reportid limit 1");
			}
		}
		//End HitsConnect Autocheck

		exit;
	}
exit;

?>