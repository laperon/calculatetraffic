<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.07
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

?>
<html>

<head>
	<title>Frame Site Checkbox</title>
</head>

<body>

<center>

<table border="0" cellpadding="10" cellspacing="0" width="600">

<tr><td align="center"><p><b>Frame Site Checkbox</b></p></td></tr>

<tr><td align="left"><p>By default our surfbar can float on top of the web site being displayed, which allows a larger portion of the site to be visible.  Alternatively, you can check the "Frame Site" checkbox to box your site inside a frame.  This prevents the surfbar from overlapping the top or bottom of your site, but slightly reduces the size of the space in which your site is shown.</p></td></tr>

</table>

</center>

</body>

</html>