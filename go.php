<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.29
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

    session_start();
    include "inc/userauth.php";
    
	// Get the file library path from the system settings
	$res=@mysql_query("SELECT filelibpath FROM ".$prefix."settings") or die("Unable to find settings!");
	$row=@mysql_fetch_array($res);
	
	/////////////////////////
	if ( ! isset($_SERVER['DOCUMENT_ROOT'] ) ) {
	  	$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr($_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF']) ) ); 
	}
	$download_path = $_SERVER["DOCUMENT_ROOT"].trim($row["filelibpath"]);
	/////////////////////////
	
	if (!isset($_GET['f']) || strlen($_GET['f']) < 1) {
		echo("Invalid File");
		exit;
	}
	
	// Get filename from URL	
	$filename = $_GET['f'];
	
	// Handle External Downloads
	if(stripos($filename, "http://") !== false) {
		header("Location:".$filename);
		exit;
	}
	
	// Make sure we can't download files above the current directory location.
	if(stripos($filename, '..') !== false) {
		echo("Invalid File");
		exit;
	}
	$file = str_replace("..", "", $filename);
	
	// Make sure we can't download .ht control files.
	if(stripos($filename, '.htacc') !== false) {
		echo("Invalid File");
		exit;
	}
	
	// Combine the download path and the filename to create the full path to the file.
	$file = $download_path.$file;
	
	// Test to ensure that the file exists.
	if(!file_exists($file)) die("I'm sorry, the file $filename doesn't seem to exist.");
	
	// Extract the type of file which will be sent to the browser as a header
	$type = filetype($file);
	
	// Get a date and timestamp
	$today = date("F j, Y, g:i a");
	$time = time();

	if (!function_exists('mime_content_type')) 
	{
   		function mime_content_type($filename)
		{
       		$idx = @strtolower(@end( @explode( '.', $filename )) );
       		$mimet = array(    'ai' =>'application/postscript',
           	'aif' =>'audio/x-aiff',
            'aifc' =>'audio/x-aiff',
			'htm' =>'text/html',
			'html' =>'text/html',
			'mp3' =>'audio/mpeg',
			'swf' =>'application/x-shockwave-flash',
            'xyz' =>'chemical/x-xyz',
            'zip' =>'application/zip'
        	);

       		if (isset( $mimet[$idx] )) {
           		return $mimet[$idx];
       		} else {
           		return 'application/octet-stream';
       		}
   		}
	}

	// Send file headers
	if($_GET["type"] == "s" || mime_content_type($file)=="text/html" || mime_content_type($file)=="application/x-shockwave-flash")
	{
		header("Content-type: ".mime_content_type($file)."\n");
	}
	else
	{
		header("Content-type: $type");
		header("Content-Disposition: attachment;filename=$filename");
	}

	header('Pragma: no-cache');
	header('Expires: 0');

	// Send the file contents.
	readfile($file);

?>
