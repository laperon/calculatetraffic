<html>
<head>
    <meta name="google-signin-client_id" content="462296554906-rp14u6u852k1p3bu7jh478kv7gojj1gv.apps.googleusercontent.com">
</head>
<body>
<div id="my-signin2"></div>
<script>
    function onSuccess(googleUser) {
        console.log(googleUser.getBasicProfile());
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log("Name: " + profile.getName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);

        console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
    }
    function onFailure(error) {
        console.log(error);
    }
    function renderButton() {
        gapi.signin2.render('my-signin2', {
            'scope': 'https://www.googleapis.com/auth/plus.login',
            'width': 200,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });
    }
</script>

<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
</body>
</html>


<?php


// Create a state token to prevent request forgery.
// Store it in the session for later validation.
$state = sha1(openssl_random_pseudo_bytes(1024));
$app['session']->set('state', $state);
// Set the client ID, token state, and application name in the HTML while
// serving it.
return $app['twig']->render('index.html', array(
    'CLIENT_ID' => 462296554906-rp14u6u852k1p3bu7jh478kv7gojj1gv,
    'STATE' => $state,
    'APPLICATION_NAME' => 'Traffic'
));

?>