<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying
// materials will void any responsibility that AKH Media Group
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions
// of use of the script. The terms and conditions of use are
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

include "inc/config.php";
@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die( "Unable to select database");


include "inc/theme.php";

load_template ($theme_dir."/header.php");

// Get homepage body
$bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='how_it_works'") or die("Unable to find How it works!");
$brow=@mysql_fetch_array($bres);
if (function_exists('html_entity_decode'))
{
  $page_content = html_entity_decode($brow["template_data"]);
}
else
{
  $page_content = unhtmlentities($brow["template_data"]);
}
?>

<div class="container">
  <div class="row">
    <?php print $page_content; ?>
  </div>
</div>

<?php
load_template ($theme_dir."/footer.php");
?>
</body>
</html>