<?php
    /**
     * Surfed Today
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */
    function bandjSurfedToday($prefix, $userid) {
        $surfFrom = date('Y-m-d') . ' 00:00:00';
        $surfTo = date('Y-m-d') . ' 59:59:59';
        $query = mysql_query("Select COUNT(id) surfed from ".$prefix."surf_amount_logs where userid = $userid and created_at >= '$surfFrom' and created_at <= '$surfTo'");
        $surfed = mysql_result($query, 0, "surfed");
        
        return $surfed;
    }
    
    /**
     * Surfed All Time
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */
    function bandjSurfedAllTime($prefix, $userid) {
        $query = mysql_query("Select COUNT(id) surfed from ".$prefix."surf_amount_logs where userid = $userid");
        $surfed = mysql_result($query, 0, "surfed");
        
        return $surfed;
    }
    
//    /**
//     * Surf + Surfer Rewards today
//     * 
//     * @param string $prefix
//     * @param integer $userid
//     * @return float
//     */
//    function bandjSurfingRatioToday($prefix, $userid){
//        $surfFrom = date('Y-m-d') . ' 00:00:00';
//        $surfTo = date('Y-m-d') . ' 23:59:59';
//        $query = mysql_query("Select COUNT(id) count, SUM(amount) surfing_ratio from ".$prefix."surf_amount_logs where userid = $userid and created_at >= '$surfFrom' and created_at <= '$surfTo'");
//        $surfingratio = mysql_result($query, 0, "surfing_ratio");
//        $count = mysql_result($query, 0, "count");
//        return number_format($surfingratio, 2);
//    }
    
    /**
     * Surf + Surfer Rewards today
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */
    /*
    function bandjSurfingRatioToday($prefix, $userid){
        $surfFrom = date('Y-m-d') . ' 00:00:00';
        $surfTo = date('Y-m-d') . ' 59:59:59';
        $query = mysql_query("Select COUNT(id) count, SUM(amount) surfing_ratio from ".$prefix."surf_amount_logs where userid = $userid and created_at >= '$surfFrom' and created_at <= '$surfTo'");
        $surfingratio = mysql_result($query, 0, "surfing_ratio");
        $count = mysql_result($query, 0, "count");
        
        $count = $count ? $count : 1;

        $surfingratio = number_format($surfingratio, 2);
        $surferrewards = bandjSurferRewardsToday($prefix, $userid);
        $promocredits = bandjPromoCreditsToday($prefix, $userid);

        $bonus = bandjBonusToday($prefix , $userid);
        $prize = bandjPrizeToday($prefix , $userid);
        $wsword = bandjWsWordToday($prefix, $userid);

        return number_format(($surfingratio+$surferrewards+$promocredits+$bonus+$prize+$wsword)/$count, 2);
    }
    */

    function bandjSurfingRatioToday($prefix , $userid) {
        $surfFrom = date('Y-m-d') . ' 00:00:00';
        $surfTo = date('Y-m-d') . ' 59:59:59';
        $surferrewards = bandjSurferRewardsToday($prefix, $userid);
        $bonus = bandjBonusToday($prefix , $userid);
        $prize = bandjPrizeToday($prefix , $userid);
        $wsword = bandjWsWordToday($prefix, $userid);
        $promocredits = bandjPromoCreditsToday($prefix, $userid);

        $query = mysql_query("Select COUNT(id) count, SUM(amount) surfing_ratio from ".$prefix."surf_amount_logs where userid = $userid and created_at >= '$surfFrom' and created_at <= '$surfTo'");
        $val   = mysql_fetch_array($query);
        $count = $val['count'];
        $surfingearned  = number_format($val['surfing_ratio'], 2);

        $total = $surfingearned+$surferrewards+$promocredits+$bonus+$prize+$wsword;

        if($count){
            return number_format(($total)/$count, 2);
        } else {
            return 0;
        }
    }
    
//    /**
//     * Surf + Surfer Rewards all time
//     * 
//     * @param string $prefix
//     * @param integer $userid
//     * @return float
//     */
//    function bandjSurfingRatioAllTime($prefix, $userid){
//        $query = mysql_query("Select COUNT(id) count, SUM(amount) surfing_ratio from ".$prefix."surf_amount_logs where userid = $userid");
//        $surfingratio = mysql_result($query, 0, "surfing_ratio");
//        $count = mysql_result($query, 0, "count");
//        
//        return number_format($surfingratio, 2, ',', ' ');
//    }
    
    /**
     * Surf + Surfer Rewards all time
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */

    function bandjSurfingRatioAllTime($prefix, $userid){
        $surferrewards = bandjSurferRewardsAllTime($prefix, $userid);
        $bonus = bandjBonusAllTime($prefix , $userid);
        $prize = bandjPrizeAllTime($prefix , $userid);
        $wsword = bandjWsWordAllTime($prefix, $userid);
        $promocredits = bandjPromoCreditsAllTime($prefix, $userid);

        $query = mysql_query("Select COUNT(id) count, SUM(amount) surfing_ratio from ".$prefix."surf_amount_logs where userid = $userid");
        $val   = mysql_fetch_array($query);
        $count = $val['count'];
        $surfingearned  = number_format($val['surfing_ratio'], 2);
        
        $total = $surfingearned+$surferrewards+$promocredits+$bonus+$prize+$wsword;

       if($count){
            return number_format(($total)/$count, 2);
        } else {
            return 0;
        }
    }

    function bandjBonusToday($prefix, $userid){
        $date = date('Y-m-d');
        $query = mysql_query(sprintf("Select SUM(amount) from winners where userid = %d and type='credits' and date= '%s'", $userid, $date ));
        $result = mysql_fetch_array($query);

        return  (int)$result[0];
    }

    function bandjPrizeToday($prefix, $userid) {
        $date = date('Y-m-d');
        $query = mysql_query(sprintf("
          Select SUM(credits) from %sprizepage_prize_winners AS w
          JOIN %sprizepage_prizes AS pp ON w.prize_id = pp.id where w.user_id = %d AND w.datetime LIKE '%s%s'", $prefix, $prefix, $userid, $date, '%')
        );


        $result = mysql_fetch_array($query);

        return  (int)$result[0];
    }

    function bandjWsWordToday($prefix, $userid) {
        
        $query = mysql_query(sprintf("Select SUM(credits) from %smembers_calculations where user_id = %d and type='wsword' and datetime LIKE '%s%s'", $prefix, $userid, date('Y-m-d'), '%'));
        $result = mysql_fetch_array($query);

        return  (int)$result[0];
    }
    
    /**
     * Surfer Rewards Today
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */
    function bandjSurferRewardsToday($prefix, $userid){
        $query = mysql_query(sprintf("Select SUM(credits) credits_amount from %sactive_surfers where usrid = %d and prizetype='credits' and claimed = '%s'", $prefix, $userid, date('Y-m-d')));
        $creditsamount = mysql_result($query, 0, "credits_amount");

        return number_format($creditsamount, 2);
    }




/**
     * Surfer Rewards All Time
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */

    function bandjSurferRewardsAllTime($prefix, $userid){
        $query = mysql_query(sprintf("Select SUM(credits) credits_amount from %sactive_surfers where usrid = %d and prizetype='credits'", $prefix, $userid));
        $creditsamount = mysql_result($query, 0, "credits_amount");
       
        return number_format($creditsamount, 2);
    }

    /*
    function bandjSurferRewardsAllTime($prefix, $userid){
        $query = mysql_query(
            sprintf("Select SUM(credits) surfer_rewards from %smembers_calculations where user_id = %d and type='rewards'", $prefix, $userid)
        );
        $credits= mysql_result($query, 0, "surfer_rewards");

        return number_format($credits, 2);

    }
    */


    function bandjBonusAllTime($prefix, $userid){
        $query = mysql_query(sprintf("Select SUM(amount) from winners where userid = %d and type='credits'", $userid ));
        $result = mysql_fetch_array($query);
        
        return  (int)$result[0];
    }

    function bandjPrizeAllTime($prefix, $userid){
        $query = mysql_query(sprintf("
          Select SUM(credits) from %sprizepage_prize_winners AS w
          JOIN %sprizepage_prizes AS pp ON w.prize_id = pp.id where w.user_id = %d", $prefix, $prefix, $userid)
        );

        $result = mysql_fetch_array($query);
        return  (int)$result[0];
    }

    function bandjWsWordAllTime($prefix, $userid){
        $query = mysql_query(sprintf("Select SUM(credits) from %smembers_calculations where user_id = %d and type='wsword'", $prefix, $userid));
        $result = mysql_fetch_array($query);

        return  (int)$result[0];
    }

    /**
     * Promo Credits Today
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */
    function bandjPromoCreditsToday($prefix, $userid){
        $query = mysql_query(sprintf("
            select
                SUM(pp.credits) credits_amount,
                pc.clicks clicks,
                pu.date date
            from
                %spromo_users pu
            left join
                %spromo_codes pc on pc.id = pu.codeid
            left join
                %spromo_prizes pp on pp.id = pc.prizeid
            where
                pu.userid = %d
                and pu.date = '%s'
                and pu.state = 1
                and not pu.id = 149
        ", $prefix, $prefix, $prefix, $userid, date('Y-m-d')));

        $amount = mysql_result($query, 0, "credits_amount");

        return number_format($amount, 2);
    }
    
    /**
     * Promo Credits All Time
     * 
     * @param string $prefix
     * @param integer $userid
     * @return float
     */
    function bandjPromoCreditsAllTime($prefix, $userid){
        $query = mysql_query(sprintf("
            select
                SUM(pp.credits) credits_amount,
                pc.clicks clicks,
                pu.date date
            from
                %spromo_users pu
            left join
                %spromo_codes pc on pc.id = pu.codeid
            left join
                %spromo_prizes pp on pp.id = pc.prizeid
            where
                pu.userid = %d
        ", $prefix, $prefix, $prefix, $userid));

        $amount = mysql_result($query, 0, "credits_amount");

        return number_format($amount, 2);
    }
    
    /**
     * Return the first day of the Week/Month/Quarter/Year that the
     * current/provided date falls within
     *
     * @param string   $period The period to find the first day of. ('year', 'quarter', 'month', 'week')
     * @param DateTime $date   The date to use instead of the current date
     *
     * @return DateTime
     * @throws InvalidArgumentException
     */
    function firstDayOf($period, DateTime $date = null)
    {
        $period = strtolower($period);
        $validPeriods = array('year', 'quarter', 'month', 'week');

        if ( ! in_array($period, $validPeriods))
            throw new InvalidArgumentException('Period must be one of: ' . implode(', ', $validPeriods));

        $newDate = ($date === null) ? new DateTime() : clone $date;

        switch ($period) {
            case 'year':
                $newDate->modify('first day of january ' . $newDate->format('Y'));
                break;
            case 'quarter':
                $month = $newDate->format('n') ;

                if ($month < 4) {
                    $newDate->modify('first day of january ' . $newDate->format('Y'));
                } elseif ($month > 3 && $month < 7) {
                    $newDate->modify('first day of april ' . $newDate->format('Y'));
                } elseif ($month > 6 && $month < 10) {
                    $newDate->modify('first day of july ' . $newDate->format('Y'));
                } elseif ($month > 9) {
                    $newDate->modify('first day of october ' . $newDate->format('Y'));
                }
                break;
            case 'month':
                $newDate->modify('first day of this month');
                break;
            case 'week':
                $newDate->modify(($newDate->format('w') === '0') ? 'monday last week' : 'monday this week');
                break;
        }

        return $newDate;
    }

    /**
     * Return the last day of the Week/Month/Quarter/Year that the
     * current/provided date falls within
     *
     * @param string   $period The period to find the last day of. ('year', 'quarter', 'month', 'week')
     * @param DateTime $date   The date to use instead of the current date
     *
     * @return DateTime
     * @throws InvalidArgumentException
     */
    function lastDayOf($period, DateTime $date = null)
    {
        $period = strtolower($period);
        $validPeriods = array('year', 'quarter', 'month', 'week');

        if ( ! in_array($period, $validPeriods))
            throw new InvalidArgumentException('Period must be one of: ' . implode(', ', $validPeriods));

        $newDate = ($date === null) ? new DateTime() : clone $date;

        switch ($period)
        {
            case 'year':
                $newDate->modify('last day of december ' . $newDate->format('Y'));
                break;
            case 'quarter':
                $month = $newDate->format('n') ;

                if ($month < 4) {
                    $newDate->modify('last day of march ' . $newDate->format('Y'));
                } elseif ($month > 3 && $month < 7) {
                    $newDate->modify('last day of june ' . $newDate->format('Y'));
                } elseif ($month > 6 && $month < 10) {
                    $newDate->modify('last day of september ' . $newDate->format('Y'));
                } elseif ($month > 9) {
                    $newDate->modify('last day of december ' . $newDate->format('Y'));
                }
                break;
            case 'month':
                $newDate->modify('last day of this month');
                break;
            case 'week':
                $newDate->modify(($newDate->format('w') === '0') ? 'now' : 'sunday this week');
                $newDate->add(new DateInterval('PT23H59M59S'));
                break;
        }

        return $newDate;
    }
?>