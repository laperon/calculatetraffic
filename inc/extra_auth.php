<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if ($GLOBALS[authenticated] != 1 || !isset($_SESSION["uid"]) || !isset($_SESSION["user_password"]) || !isset($_SESSION["userid"])){
echo "<script language=\"javascript\">";
			echo "window.location.href=\"login.php?s=noauth\";";
			echo "</script>";
		exit;
	}
?>