<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.19
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////
	
	// Checks that referral ID is a number
	if (isset($_GET["rid"])) {
		$_GET["rid"] = trim($_GET["rid"]);
		if (!is_numeric($_GET["rid"]) || $_GET["rid"] < 1) {
			$_GET["rid"] = 0;
			$_REQUEST["rid"] = 0;
		}
	}

	// Check for magic quotes and clean if necessary
	if (!get_magic_quotes_gpc()) {
		if (isset($_POST)) {
			foreach ($_POST as $key => $value) {
				if (!is_array($_POST[$key])) {
					$_POST[$key] =  trim(addslashes($value));
				}
			}
		}
		if (isset($_REQUEST)) {
			foreach ($_REQUEST as $key => $value) {
				if (!is_array($_REQUEST[$key])) {
					$_REQUEST[$key] =  trim(addslashes($value));
				}
			}
		}
		
		if (isset($_GET)) {
			foreach ($_GET as $key => $value) {
				if (!is_array($_GET[$key])) {
					$_GET[$key] = trim(addslashes($value));
				}
			}
		}
		
		// Ensures these variables aren't vulnerable to SQL injection
		if (isset($_SERVER['REMOTE_ADDR'])) {
			$_SERVER['REMOTE_ADDR'] = trim(addslashes($_SERVER['REMOTE_ADDR']));
		}
		if (isset($_SERVER["HTTP_REFERER"])) {
			$_SERVER["HTTP_REFERER"] = trim(addslashes($_SERVER["HTTP_REFERER"]));
		}
	}

	// Prevent over-writing of globals
    if (ini_get('register_globals')) {
        
        if (isset($_REQUEST['GLOBALS'])) {
            die('GLOBALS overwrite attempt logged');
        }

        // Variables that shouldn't be unset
        $noUnset = array('GLOBALS',  '_GET',
                         '_POST',    '_COOKIE',
                         '_REQUEST', '_SERVER',
                         '_ENV',     '_FILES');

        $input = array_merge($_GET,    $_POST,
                             $_COOKIE, $_SERVER,
                             $_ENV,    $_FILES,
                             isset($_SESSION) ? (array)$_SESSION : array());

        foreach ($input as $k => $v) {
            if (!in_array($k, $noUnset) AND isset($GLOBALS[$k])) {
                unset($GLOBALS[$k]);
            }
        }
    }
?>