<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "filter.php";
require_once "lfmsql.php";
require_once "lfmvers.php";

	include "config.php";

function get_setting($setting_name)
{
	include "config.php";
	$res=@lfmsql_query("SELECT ".$setting_name." FROM ".$prefix."settings");
	$setting=@lfmsql_result($res,0);
	return $setting;
}

function get_member_setting($setting_name,$member_id)
{
	include "config.php";
	$res=@lfmsql_query("SELECT ".$setting_name." FROM ".$prefix."members WHERE Id=".$member_id);
	$setting=@lfmsql_result($res,0);
	return $setting;
}

function unhtmlentities($string)
{
   // replace numeric entities
   $string = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $string);
   $string = preg_replace('~&#([0-9]+);~e', 'chr(\\1)', $string);
   // replace literal entities
   $trans_tbl = get_html_translation_table(HTML_ENTITIES);
   $trans_tbl = array_flip($trans_tbl);
   return strtr($string, $trans_tbl);
}

function pageNav($allEntries, $limitStart=0, $limitBy=40,$page="mm")
    {
    $lastPageEntries = ($allEntries % $limitBy);
    $allPages = round(($allEntries - $lastPageEntries) / $limitBy)+1;
    if($lastPageEntries>0 and $allPages==0) {$allPages=$allPages+1;}
    $thisPage = round(($limitStart+$limitBy) / $limitBy);

    if($allEntries > $limitBy) {
    
        print "<font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"2\"><b>Pages:</b>&nbsp;";
        
        if ($allPages > 15) {
        	
	        $firstpage = 1;
	        $lastpage = $allPages;
	        
	        if ($thisPage < $firstpage+6) {
	        	$listarray = array($firstpage, $firstpage+2, $firstpage+3, $firstpage+4, $firstpage+5, $firstpage+6, $firstpage+7, $firstpage+8, $firstpage+9, $firstpage+10, $firstpage+11, $lastpage);
	        } elseif ($thisPage > $lastpage-6) {
	        	$listarray = array($firstpage, $lastpage-11, $lastpage-10, $lastpage-9, $lastpage-8, $lastpage-7, $lastpage-6, $lastpage-5, $lastpage-4, $lastpage-3, $lastpage-2, $lastpage-1, $lastpage);
	        } else {
			$listarray = array($firstpage, $lastpage);
		}
	        
	        if (!in_array($thisPage-5, $listarray)) {
	        	$listarray[] = $thisPage-5;
	        }
	        
	        if (!in_array($thisPage-4, $listarray)) {
	        	$listarray[] = $thisPage-4;
	        }
	        
	        if (!in_array($thisPage-3, $listarray)) {
	        	$listarray[] = $thisPage-3;
	        }
	        
	        if (!in_array($thisPage-2, $listarray)) {
	        	$listarray[] = $thisPage-2;
	        }
	        
	        if (!in_array($thisPage-1, $listarray)) {
	        	$listarray[] = $thisPage-1;
	        }
	        
	        if (!in_array($thisPage, $listarray)) {
	        	$listarray[] = $thisPage;
	        }
	        
	        if (!in_array($thisPage+1, $listarray)) {
	        	$listarray[] = $thisPage+1;
	        }
	        
	        if (!in_array($thisPage+2, $listarray)) {
	        	$listarray[] = $thisPage+2;
	        }
	        
	        if (!in_array($thisPage+3, $listarray)) {
	        	$listarray[] = $thisPage+3;
	        }
	        
	        if (!in_array($thisPage+4, $listarray)) {
	        	$listarray[] = $thisPage+4;
	        }
	        
	        if (!in_array($thisPage+5, $listarray)) {
	        	$listarray[] = $thisPage+5;
	        }        
	        	        
	} else {
		
		$listarray = array();
		for($entryNr=1; $entryNr<=$allPages; $entryNr++) {
			$listarray[] = $entryNr;
		}
		
	}
        
        $lastentry = 0;
        for($entryNr=1; $entryNr<=$allPages; $entryNr++) {
            	$startPage = ($entryNr * $limitBy) - $limitBy;
            	if($thisPage!=$entryNr) {
            		if (!in_array($entryNr, $listarray)) {
            			if ($lastentry != " ... ") {
            				print " ... ";
            				$lastentry = " ... ";
            			}
            		} else {
            			
            			$lastentry = $entryNr;
				print "<a href=\"".$PHP_SELF."?f=".$page."&sf=browse&limitStart=".$startPage."\"><input type=\"button\" onClick=\"window.location.href='".$PHP_SELF."?f=".$page."&sf=browse&limitStart=".$startPage."'\" value=\"".$entryNr."\"></a>&nbsp;";
				
			}
			
		} else {
			
			$lastentry = $entryNr;
			print "<a href=\"".$PHP_SELF."?f=".$page."&sf=browse&limitStart=".$startPage."\"><input style=\"background-color:#FFFF00; color:#000000;\" type=\"button\" onClick=\"window.location.href='".$PHP_SELF."?f=".$page."&sf=browse&limitStart=".$startPage."'\" value=\"".$entryNr."\"></a>&nbsp;";
			
			}
          }
        }
    $startNext = $thisPage * $limitBy;
}

// generate a random string of numbers/letters
function GetRandomString($length) {
	settype($template, "string");
	$template = "1234567890abcdefghijklmnopqrstuvwxyz";

    global $template;

    settype($length, "integer");
    settype($rndstring, "string");
    settype($a, "integer");
    settype($b, "integer");

    for ($a = 0; $a <= $length; $a++) {
         $b = rand(0, strlen($template) - 1);
         $rndstring .= $template[$b];
    }

    return $rndstring;
}

function select_member($smember = "0")
{
	include "config.php";
	$mres=@lfmsql_query("SELECT Id,firstname,lastname FROM ".$prefix."members");
	while($member=lfmsql_fetch_object($mres))
	{
		// Get member list
		if($member->Id == $smember)
		{
			echo "<option value=\"$member->Id\" selected=\"selected\">".$member->firstname."&nbsp;".$member->lastname."</option>\n";
		}
		else
		{
			echo "<option value=\"$member->Id\">".$member->firstname."&nbsp;".$member->lastname."</option>\n";
		}
	}

}

function select_country( $country = "US" ) {
  $countries = array(
    "AU" => "Australia",
    "CA" => "Canada",
    "GB" => "United Kingdom",
    "US" => "United States",
    "AF" => "Afghanistan",
    "AL" => "Albania",
    "DZ" => "Algeria",
    "AS" => "American Samoa",
    "AD" => "Andorra",
    "AO" => "Angola",
    "AI" => "Anguilla",
    "AQ" => "Antarctica",
    "AG" => "Antigua and Barbuda",
    "AR" => "Argentina",
    "AM" => "Armenia",
    "AW" => "Aruba",
    "AT" => "Austria",
    "AZ" => "Azerbaijan",
    "BS" => "Bahamas",
    "BH" => "Bahrain",
    "BD" => "Bangladesh",
    "BB" => "Barbados",
    "BY" => "Belarus",
    "BE" => "Belgium",
    "BZ" => "Belize",
    "BJ" => "Benin",
    "BM" => "Bermuda",
    "BT" => "Bhutan",
    "BO" => "Bolivia",
    "BA" => "Bosnia and Herzegowina",
    "BW" => "Botswana",
    "BV" => "Bouvet Island",
    "BR" => "Brazil",
    "IO" => "British Indian Ocean Territory",
    "BN" => "Brunei Darussalam",
    "BG" => "Bulgaria",
    "BF" => "Burkina Faso",
    "BI" => "Burundi",
    "KH" => "Cambodia",
    "CM" => "Cameroon",
    "CV" => "Cape Verde",
    "KY" => "Cayman Islands",
    "CF" => "Central African Republic",
    "TD" => "Chad",
    "CL" => "Chile",
    "CN" => "China",
    "CX" => "Christmas Island",
    "CC" => "Cocos (Keeling) Islands",
    "CO" => "Colombia",
    "KM" => "Comoros",
    "CG" => "Congo",
    "CD" => "Congo, the Democratic Republic of the",
    "CK" => "Cook Islands",
    "CR" => "Costa Rica",
    "CI" => "Cote d'Ivoire",
    "HR" => "Croatia (Hrvatska)",
    "CU" => "Cuba",
    "CY" => "Cyprus",
    "CZ" => "Czech Republic",
    "DK" => "Denmark",
    "DJ" => "Djibouti",
    "DM" => "Dominica",
    "DO" => "Dominican Republic",
    "TP" => "East Timor",
    "EC" => "Ecuador",
    "EG" => "Egypt",
    "SV" => "El Salvador",
    "GQ" => "Equatorial Guinea",
    "ER" => "Eritrea",
    "EE" => "Estonia",
    "ET" => "Ethiopia",
    "FK" => "Falkland Islands (Malvinas)",
    "FO" => "Faroe Islands",
    "FJ" => "Fiji",
    "FI" => "Finland",
    "FR" => "France",
    "FX" => "France, Metropolitan",
    "GF" => "French Guiana",
    "PF" => "French Polynesia",
    "TF" => "French Southern Territories",
    "GA" => "Gabon",
    "GM" => "Gambia",
    "GE" => "Georgia",
    "DE" => "Germany",
    "GH" => "Ghana",
    "GI" => "Gibraltar",
    "GR" => "Greece",
    "GL" => "Greenland",
    "GD" => "Grenada",
    "GP" => "Guadeloupe",
    "GU" => "Guam",
    "GT" => "Guatemala",
    "GN" => "Guinea",
    "GW" => "Guinea-Bissau",
    "GY" => "Guyana",
    "HT" => "Haiti",
    "HM" => "Heard and Mc Donald Islands",
    "VA" => "Holy See (Vatican City State)",
    "HN" => "Honduras",
    "HK" => "Hong Kong",
    "HU" => "Hungary",
    "IS" => "Iceland",
    "IN" => "India",
    "ID" => "Indonesia",
    "IR" => "Iran (Islamic Republic of)",
    "IQ" => "Iraq",
    "IE" => "Ireland",
    "IL" => "Israel",
    "IT" => "Italy",
    "JM" => "Jamaica",
    "JP" => "Japan",
    "JO" => "Jordan",
    "KZ" => "Kazakhstan",
    "KE" => "Kenya",
    "KI" => "Kiribati",
    "KP" => "Korea, Democratic People's Republic of",
    "KR" => "Korea, Republic of",
    "KW" => "Kuwait",
    "KG" => "Kyrgyzstan",
    "LA" => "Lao People's Democratic Republic",
    "LV" => "Latvia",
    "LB" => "Lebanon",
    "LS" => "Lesotho",
    "LR" => "Liberia",
    "LY" => "Libyan Arab Jamahiriya",
    "LI" => "Liechtenstein",
    "LT" => "Lithuania",
    "LU" => "Luxembourg",
    "MO" => "Macau",
    "MK" => "Macedonia, The Former Yugoslav Republic of",
    "MG" => "Madagascar",
    "MW" => "Malawi",
    "MY" => "Malaysia",
    "MV" => "Maldives",
    "ML" => "Mali",
    "MT" => "Malta",
    "MH" => "Marshall Islands",
    "MQ" => "Martinique",
    "MR" => "Mauritania",
    "MU" => "Mauritius",
    "YT" => "Mayotte",
    "MX" => "Mexico",
    "FM" => "Micronesia, Federated States of",
    "MD" => "Moldova, Republic of",
    "MC" => "Monaco",
    "MN" => "Mongolia",
    "MS" => "Montserrat",
    "MA" => "Morocco",
    "MZ" => "Mozambique",
    "MM" => "Myanmar",
    "NA" => "Namibia",
    "NR" => "Nauru",
    "NP" => "Nepal",
    "NL" => "Netherlands",
    "AN" => "Netherlands Antilles",
    "NC" => "New Caledonia",
    "NZ" => "New Zealand",
    "NI" => "Nicaragua",
    "NE" => "Niger",
    "NG" => "Nigeria",
    "NU" => "Niue",
    "NF" => "Norfolk Island",
    "MP" => "Northern Mariana Islands",
    "NO" => "Norway",
    "OM" => "Oman",
    "PK" => "Pakistan",
    "PW" => "Palau",
    "PA" => "Panama",
    "PG" => "Papua New Guinea",
    "PY" => "Paraguay",
    "PE" => "Peru",
    "PH" => "Philippines",
    "PN" => "Pitcairn",
    "PL" => "Poland",
    "PT" => "Portugal",
    "PR" => "Puerto Rico",
    "QA" => "Qatar",
    "RE" => "Reunion",
    "RO" => "Romania",
    "RU" => "Russian Federation",
    "RW" => "Rwanda",
    "KN" => "Saint Kitts and Nevis",
    "LC" => "Saint LUCIA",
    "VC" => "Saint Vincent and the Grenadines",
    "WS" => "Samoa",
    "SM" => "San Marino",
    "ST" => "Sao Tome and Principe",
    "SA" => "Saudi Arabia",
    "SN" => "Senegal",
    "SC" => "Seychelles",
    "SL" => "Sierra Leone",
    "SG" => "Singapore",
    "SK" => "Slovakia (Slovak Republic)",
    "SI" => "Slovenia",
    "SB" => "Solomon Islands",
    "SO" => "Somalia",
    "ZA" => "South Africa",
    "GS" => "South Georgia and the South Sandwich Islands",
    "ES" => "Spain",
    "LK" => "Sri Lanka",
    "SH" => "St. Helena",
    "PM" => "St. Pierre and Miquelon",
    "SD" => "Sudan",
    "SR" => "Suriname",
    "SJ" => "Svalbard and Jan Mayen Islands",
    "SZ" => "Swaziland",
    "SE" => "Sweden",
    "CH" => "Switzerland",
    "SY" => "Syrian Arab Republic",
    "TW" => "Taiwan",
    "TJ" => "Tajikistan",
    "TZ" => "Tanzania, United Republic of",
    "TH" => "Thailand",
    "TG" => "Togo",
    "TK" => "Tokelau",
    "TO" => "Tonga",
    "TT" => "Trinidad and Tobago",
    "TN" => "Tunisia",
    "TR" => "Turkey",
    "TM" => "Turkmenistan",
    "TC" => "Turks and Caicos Islands",
    "TV" => "Tuvalu",
    "UG" => "Uganda",
    "UA" => "Ukraine",
    "AE" => "United Arab Emirates",
    "UY" => "Uruguay",
    "UZ" => "Uzbekistan",
    "VU" => "Vanuatu",
    "VE" => "Venezuela",
    "VN" => "Vietnam",
    "VG" => "Virgin Islands (British)",
    "VI" => "Virgin Islands (U.S.)",
    "WF" => "Wallis and Futuna Islands",
    "EH" => "Western Sahara",
    "YE" => "Yemen",
    "YU" => "Yugoslavia",
    "ZM" => "Zambia",
    "ZW" => "Zimbabwe"
  );

  $select_attr = ' selected="selected"';
   $selectCountry = "";
  foreach( $countries as $cAbbr => $cName ) {
    $selected = ($country == $cAbbr) ? $select_attr : "";

    $selectCountry .= <<<HTML
<option value="$cAbbr"$selected>$cName</option>
HTML;
}

return $selectCountry;
}

function GetGroup($grp)
{
	include "config.php";
	$select_attr = ' selected="selected"';

	$grpres=@lfmsql_query("SELECT * FROM ".$prefix."groups") or die(lfmsql_error());
	while($grprow=@lfmsql_fetch_array($grpres))
	{
    	$selected = ($grp == $grprow["groupid"]) ? $select_attr : "";
		$groupid=$grprow["groupid"];
		$groupname=$grprow["groupname"];

    	echo <<<HTML
<option value="$groupid"$selected>$groupname</option>
HTML;

	}
}

function GetMtype($mtype)
{
	include "config.php";
	$select_attr = ' selected="selected"';

	$grpres=@lfmsql_query("SELECT * FROM ".$prefix."membertypes");
	while($grprow=@lfmsql_fetch_array($grpres))
	{
    	$selected = ($mtype == $grprow["mtid"]) ? $select_attr : "";
		$mtid=$grprow["mtid"];
		$accname=$grprow["accname"];

    	echo <<<HTML
<option value="$mtid"$selected>$accname</option>
HTML;

	}
}

function GetPages($pageid)
{
	include "config.php";
	$select_attr = ' selected="selected"';

	$grpres=@lfmsql_query("SELECT * FROM ".$prefix."memberpages");

	while($grprow=@lfmsql_fetch_array($grpres))
	{
		if($pageid == $grprow["pageid"])
		{
			$selected = $select_attr;
		}
		else
		{
			$selected="";
		}
		$npageid=$grprow["pageid"];
		$pagename=$grprow["pagename"];

    	echo <<<HTML
<option value="$npageid"$selected>$pagename</option>
HTML;

	}
}

function GetProducts($productid)
{
	include "config.php";
	$select_attr = ' selected="selected"';

	$prres=@lfmsql_query("SELECT productid,productname FROM ".$prefix."products");

	while($prrow=@lfmsql_fetch_array($prres))
	{
		if($productid == $prrow["productid"])
		{
			$selected = $select_attr;
		}
		else
		{
			$selected="";
		}
		$nproductid=$prrow["productid"];
		$productname=$prrow["productname"];

    	echo <<<HTML
<option value="$nproductid"$selected>$productname</option>
HTML;

	}
}

// Return the position of the nth instance of the needle
function strpos2($haystack, $needle, $nth = 1)
{
   //Fixes a null return if the position is at the beginning of input
   //It also changes all input to that of a string
   $haystack = ' '.$haystack;
   if (!strpos($haystack, $needle))
       return false;
   $offset=0;
   for($i = 1; $i < $nth; $i++)
       $offset = strpos($haystack, $needle, $offset) + 1;
   return strpos($haystack, $needle, $offset) - 1;
}

function translate_site_tags($textstring)
{
	include "config.php";
    $row[] = array();
    $res = array();

	// This function will take any text with site related tags embedded in it and return the translated version
	// First we query the db for all the site related settings
	// ...then run a preg_replace for each one

	// Get the sitename and affiliate url
	$res=@lfmsql_query("SELECT sitename,affurl FROM ".$prefix."settings") or die("translate site: ".lfmsql_error());
	$row=@lfmsql_fetch_array($res) or die("translate site: ".lfmsql_error());

	// Replace authnet tags
	if(strstr($textstring,"[authnet_fields]"))
	{
		include "authnet_simlib.php";
		srand(time());
		$sequence = rand(1, 1000);
		// Insert the form elements required for SIM by calling InsertFP
		$authnet_fields=GetFP ($row["authnet_login"], $row["authnet_key"], $item_price, $sequence);
		$textstring=str_replace("[authnet_fields]", $authnet_fields, $textstring);
	}

	$textstring=str_replace("#SITENAME#", $row["sitename"], $textstring);
	$textstring=str_replace("#REFURL#", $row["affurl"]."?rid=#AFFILIATEID#", $textstring);
	
	// Get any extra replace functions
	$extra_res=@lfmsql_query("SELECT * FROM ".$prefix."extra_funcs WHERE type = 'site'");
	if (lfmsql_num_rows ($extra_res) != 0 ){
		while ($extra_row = @lfmsql_fetch_array($extra_res)){
			@include ($extra_row['include_path']);
			}		
		}

	return $textstring;
}

function translate_comm_tags($textstring,$refid)
{
	include "config.php";
	// Get most recent sale for this member
	$qry="SELECT MAX(salesid) FROM ".$prefix."sales WHERE affid=".$refid;
	$res=@lfmsql_query($qry);
	$saleinfo=@lfmsql_fetch_object($res);
	
	$itemamount=$saleinfo->itemamount;
	$commission=$saleinfo->commission;
	
	$textstring=str_replace("#ITEMCOST#", $itemamount, $textstring);
	$textstring=str_replace("#AFFEARN#", $commission, $textstring);

	// Get total commissions currently owed
	$qry="SELECT SUM(commission) as totalcomm FROM ".$prefix."sales WHERE affid=".$refid." AND status IS NULL";
	$res=@lfmsql_query($qry);
	$comminfo=@lfmsql_result($res,0,"totalcomm");

	$textstring=str_replace("#CASH#", $comminfo, $textstring);
	
	// Get any extra replace functions
	$extra_res=@lfmsql_query("SELECT * FROM ".$prefix."extra_funcs WHERE type = 'comm'");
	if (lfmsql_num_rows ($extra_res) != 0 ){
		while ($extra_row = @lfmsql_fetch_array($extra_res)){
			@include ($extra_row['include_path']);
			}		
		}

	
	return $textstring;
}

function translate_user_tags($textstring,$email)
{
	include "config.php";
    $row[] = array();
    $res = array();
    $mres = array();
    $mrow[] = array();
    $qry='';

	$sres=@lfmsql_query("SELECT affurl FROM ".$prefix."settings");
	$affurl=@lfmsql_result($sres,0);

	// This function will take any text with user related tags embedded in it and return the translated version
	$qry="SELECT Id, mtype, refid, firstname, lastname, username, vericode, credits, bannerimps, textimps, clickstoday, creditstoday, clicksyesterday, creditsyesterday FROM ".$prefix."members WHERE email='$email'";
    $res=@lfmsql_query($qry) or die("translate user: ".lfmsql_error());
    $row=@lfmsql_fetch_array($res) or die("translate user: ".lfmsql_error());

	// Get membership type
	$mres=@lfmsql_query("SELECT accname FROM ".$prefix."membertypes WHERE mtid=".$row["mtype"]);
	$mrow=@lfmsql_fetch_array($mres);

		// Get the commission for this member
		$commissionbal=0;
		$commres=@lfmsql_query("SELECT SUM(commission) as ctotal,COUNT(*) as tsales FROM ".$prefix."sales WHERE affid=".$row["Id"]." AND ".$prefix."sales.status IS NULL");
		if($commres)
		{
			$commrow=@lfmsql_fetch_array($commres);
			$commissionbal=$commrow["ctotal"];
			if ($commissionbal < 1) { $commissionbal = "0.00"; }
		}
		else
		{
			$commissionbal="0.00";
		}


	// Check the string for tags and update
	$textstring=str_replace("#FIRSTNAME#", $row["firstname"], $textstring);
	$textstring=str_replace("#LASTNAME#", $row["lastname"], $textstring);
	$textstring=str_replace("#AFFILIATEID#", $row["Id"], $textstring);
	$textstring=str_replace("#USERNAME#", $row["username"], $textstring);
	$textstring=str_replace("#BALANCE#", $commissionbal, $textstring);
	$textstring=str_replace("#MEMBERSHIP#", $mrow["accname"], $textstring);
	$textstring=str_replace("#VERIFY#", $affurl."verify.php?id=".$row["Id"]."&vcode=".$row["vericode"], $textstring);
	$textstring=str_replace("#CREDITS#", $row["credits"], $textstring);
	$textstring=str_replace("#PASSWORD#", "(hidden)", $textstring);
	
	if($row["refid"] > 0)
	{
		// Get referrer name
		$rres=@lfmsql_query("SELECT firstname,lastname FROM ".$prefix."members WHERE Id=".$row["refid"]);
		if (lfmsql_num_rows($rres) > 0) {
			$rrow=@lfmsql_fetch_array($rres);
			$textstring=str_replace("#SPONSORID#", $row["refid"], $textstring);
			$textstring=str_replace("#SPONSORNAME#", $rrow["firstname"]." ".$rrow["lastname"], $textstring);
		} else {
			$textstring=str_replace("#SPONSORID#", "0", $textstring);
			$textstring=str_replace("#SPONSORNAME#", "N/A", $textstring);
		}
	}
	else
	{
		$textstring=str_replace("#SPONSORID#", "0", $textstring);
		$textstring=str_replace("#SPONSORNAME#", "N/A", $textstring);
	}
	
// Get any extra replace functions
	$extra_res=@lfmsql_query("SELECT * FROM ".$prefix."extra_funcs WHERE type = 'user'");
	if (lfmsql_num_rows ($extra_res) != 0 ){
		while ($extra_row = @lfmsql_fetch_array($extra_res)){
			@include ($extra_row['include_path']);
			}		
		}

	return $textstring;
}

function translate_upgrade_tags($textstring)
{
    // Replaced By IPN
    return $textstring;
}

function translate_file_tags($textstring)
{
	include "config.php";
    $row[] = array();
    $res = array();

	// This function will take any text with file tags embedded in it and return the translated version

	// Get the filelib directory
	$flres=@lfmsql_query("SELECT filelibpath FROM ".$prefix."settings") or die("translate files: ".lfmsql_error());
	$flpath=@lfmsql_fetch_object($flres);

	// Get the sitename and affiliate url
	$res=@lfmsql_query("SELECT * FROM ".$prefix."filelib") or die("translate files: ".lfmsql_error());
	while($filelib=@lfmsql_fetch_object($res))
	{
		$filetag="~~".$filelib->filetag."~~";
		$filelink="<a href=\"".$flpath->filelibpath.$filelib->filename."\">$filelib->filetitle</a>";
		$textstring=str_replace($filetag, $filelink, $textstring);
	}
	
	// Get any extra replace functions
	$extra_res=@lfmsql_query("SELECT * FROM ".$prefix."extra_funcs WHERE type = 'file'");
	if (lfmsql_num_rows ($extra_res) != 0 ){
		while ($extra_row = @lfmsql_fetch_array($extra_res)){
			@include ($extra_row['include_path']);
			}		
		}

	return $textstring;
}

function AddLog($entry)
{
	include "config.php";
	$ts=date("Y-m-d H:i:s");
	@lfmsql_query("INSERT INTO ".$prefix."log(logtime,logentry) VALUES('$ts','".$entry."')");
}

function my_eval($arr) {
return ('echo stripslashes("'.addslashes($arr[0]).'");');
}

function eval_html3($string) {
$string = '<?php ?>'.$string.'<?php ?>';
$string = str_replace( '?>', '', str_replace( array( '<?php', '<?' ), '', preg_replace_callback( "/\?>(.*?)(<\?php|<\?)/", "my_eval", $string ) ) );
return eval($string);
}

function addsingleusertophpbb($username,$password,$email,$phpbb_host,$phpbb_db,$phpbb_user,$phpbb_pass,$phpbb_prefix,$phpbb_group,$phpbbver)
{
		$pconn2=@lfmsql_connect($phpbb_host,$phpbb_user,$phpbb_pass);
		$pdb=@lfmsql_select_db($phpbb_db,$pconn2);

        $user_fields['user_regdate'] = time();
        $user_fields['user_from'] = '';
        $user_fields['user_occ'] = '';
        $user_fields['user_interests'] = '';
        $user_fields['user_website'] = '';
        $user_fields['user_icq'] = '';
        $user_fields['user_aim'] = '';
        $user_fields['user_yim'] = '';
        $user_fields['user_msnm'] = '';
        $user_fields['user_sig'] = '';
        $user_fields['user_avatar'] = '';
        $user_fields['user_avatar_type'] = 0;
        $user_fields['user_viewemail'] = 0;
        $user_fields['user_attachsig'] = 1;
        $user_fields['user_allowsmile'] = 1;
        $user_fields['user_allowhtml'] = 0;
        $user_fields['user_allowbbcode'] = 1;
        $user_fields['user_allow_viewonline'] = 1;
        $user_fields['user_notify'] = 0;
        $user_fields['user_notify_pm'] = 1;
        $user_fields['user_popup_pm'] = 1;
        $user_fields['user_timezone'] = 0.00;
        $user_fields['user_dateformat'] = 'D M d, Y g:i a';
        $user_fields['user_lang'] = 'english';
        $user_fields['user_style'] = 1;
        $user_fields['user_level'] = 0;
        $user_fields['user_posts'] = 0;

		$user_id=0;
		$group_id=0;
		$personalgroup=0;

		// Check whether this member is already in phpBB
		$sql="SELECT user_id FROM ".$phpbb_prefix."users WHERE username='".$username."' AND user_email='".$email."'";
		$res=@lfmsql_query($sql,$pconn2);

		if(lfmsql_num_rows($res) == 0)
		{
			// Get the max user id from the users table
			$sql = "SELECT MAX(user_id) AS total FROM ".$phpbb_prefix."users";
			$res = lfmsql_query($sql,$pconn2) or die(lfmsql_error());
			$row=@lfmsql_fetch_array($res);
			$user_id=$row["total"]+1;

			// Build the main SQL query for phpBB ver 2.x
			if($phpbbver == 2)
			{
				$bbqry = "INSERT INTO ".$phpbb_prefix."users(user_id, username, user_regdate, user_password, user_email, user_icq,
				user_website, user_occ, user_from, user_interests, user_sig, user_sig_bbcode_uid, user_avatar, user_avatar_type,
				user_viewemail, user_aim, user_yim, user_msnm, user_attachsig, user_allowsmile, user_allowhtml, user_allowbbcode,
				user_allow_viewonline, user_notify, user_notify_pm, user_popup_pm, user_timezone, user_dateformat, user_lang,
				user_style, user_level, user_allow_pm, user_active, user_actkey, user_posts) ";

				$bbqry .= "VALUES (" . $user_id . ", '" . $username . "', '" . $user_fields['user_regdate'] . "',
				 MD5('" . $password . "'), '" . $email . "', '" . $user_fields['user_icq'] . "',
				 '" . $user_fields['user_website'] . "', '" . $user_fields['user_occ'] . "', '" . $user_fields['user_from'] . "',
				 '" . $user_fields['user_interests'] . "', '" . $user_fields['user_sig'] . "',
				 '" . $user_fields['user_sig_bbcode_uid'] . "', '" . $user_fields['user_avatar'] . "',
				 '" . $user_fields['user_avatar_type'] . "', " . $user_fields['user_viewemail'] . ",
				 '" . str_replace(' ', '+', $user_fields['user_aim']) . "', '" . $user_fields['user_yim'] . "',
				 '" . $user_fields['user_msnm'] . "', " . $user_fields['user_attachsig'] . ", " . $user_fields['user_allowsmile'] . ",
				 " . $user_fields['user_allowhtml'] . ", " . $user_fields['user_allowbbcode'] . ",
				 " . $user_fields['user_allow_viewonline'] . ", " . $user_fields['user_notify'] . ", " . $user_fields['user_notify_pm'] . ",
				 " . $user_fields['user_popup_pm'] . ", " . $user_fields['user_timezone'] . ", '" . $user_fields['user_dateformat'] . "',
				 '" . $user_fields['user_lang'] . "', " . $user_fields['user_style'] . ", " . $user_fields['user_level'] . ", 1, 1, '',
				 '" . $user_fields['user_posts'] . "')";
			}

			// Build the main SQL query for phpBB ver 3.x
			if($phpbbver == 3)
			{
				$bbqry = "INSERT INTO ".$phpbb_prefix."users(user_id,user_type,group_id,username,username_clean,user_password,user_regdate,user_style,user_lang,user_email,user_passchg,user_dst,user_ip) ";
				$bbqry.="VALUES (".$user_id.",0,2,'".$username."','".$username."',MD5('".$password."'),". time() .",1,'en','".$email."',0,1,'127.0.0.1')";
			}

			$res=@lfmsql_query($bbqry,$pconn2) or die(lfmsql_error());

			// Insert the personal group
			if($phpbbver == 2)
			{
				$sql = "INSERT INTO ".$phpbb_prefix."groups (group_name, group_description, group_single_user, group_moderator)
					VALUES ('', 'Personal User', 1, 0)";
				$res=@lfmsql_query($sql,$pconn2);

				// Add this group entry to the user_group table
				$res=@lfmsql_query("SELECT MAX(group_id) as personalgroup FROM ".$phpbb_prefix."groups",$pconn2) or die(lfmsql_error());
				$row=@lfmsql_fetch_array($res);
				$personalgroup=$row["personalgroup"]+1;

				$sql = "INSERT INTO ".$phpbb_prefix."user_group(user_id, group_id, user_pending)
					VALUES ($user_id, $personalgroup, 0)";
				$res=@lfmsql_query($sql,$pconn2);

				// Add user to defined group
				$sql = "SELECT group_id FROM ".$phpbb_prefix."groups WHERE group_name='".trim($phpbb_group)."'";
				$res=@lfmsql_query($sql,$pconn2) or die(lfmsql_error());
				$row=@lfmsql_fetch_array($res);
				$group_id=$row["group_id"];

				// Insert the user_group entry
				$sql = "INSERT INTO ".$phpbb_prefix."user_group(user_id, group_id, user_pending)
					VALUES ($user_id, $group_id, 0)";
				$res=@lfmsql_query($sql,$pconn2) or die(lfmsql_error());
			}

			if($phpbbver == 3)
			{
				// Add user to default group
				$sql = "INSERT INTO ".$phpbb_prefix."user_group (group_id,user_id,group_leader,user_pending)
            		VALUES (2,$user_id,0,0)";
				$res=@lfmsql_query($sql,$pconn2) or die(lfmsql_error());

				// Add user to defined group
				$sql = "SELECT group_id FROM ".$phpbb_prefix."groups WHERE group_name='".trim($phpbb_group)."'";
				$res=@lfmsql_query($sql,$pconn2) or die(lfmsql_error());
				$row=@lfmsql_fetch_array($res);
				$group_id=$row["group_id"];

				// Insert the user_group entry
				$sql = "INSERT INTO ".$phpbb_prefix."user_group(group_id,user_id,group_leader,user_pending)
					VALUES ($group_id,$user_id,0,0)";
				$res=@lfmsql_query($sql,$pconn2) or die(lfmsql_error());

				// $sql = "UPDATE ".$prefix."config SET config_value='".$username."' WHERE config_name='newest_username'";
				// $res=@lfmsql_query($sql,$pconn2) or die(lfmsql_error());
			}
		}

// Reconnect to Main DB
include "inc/config.php";
@lfmsql_connect($dbhost,$dbuser,$dbpass) or die("funcs user connect: ".lfmsql_error());
@lfmsql_select_db($dbname) or die("funcs user select: ". lfmsql_error());

}

function date_diff2($str_start, $str_end)
{
	$str_start = strtotime($str_start); // The start date becomes a timestamp
	$str_end = strtotime($str_end); // The end date becomes a timestamp

	$nseconds = $str_end - $str_start; // Number of seconds between the two dates
	$ndays = round($nseconds / 86400); // One day has 86400 seconds
	$nseconds = $nseconds % 86400; // The remainder from the operation
	$nhours = round($nseconds / 3600); // One hour has 3600 seconds
	$nseconds = $nseconds % 3600;
	$nminutes = round($nseconds / 60); // One minute has 60 seconds, duh!
	$nseconds = $nseconds % 60;

	//echo $ndays." days, ".$nhours." hours, ".$nminutes." minutes, ".$nseconds."
	// echo �seconds<br>\n";
	return $ndays;
}

// LFMv4 Function For date_diff
function date_diff_fix($str_start, $str_end) {
	return date_diff2($str_start, $str_end);
}

function BuildPagesMenu($daysjoined,$mtype,$joindate,$userid=0) {
	include "config.php";
	if (!is_numeric($userid) || $userid < 1) {
		return "";
	}
	
	// Check if publish date is enabled
	$enablepdate=0;
	$rank=0;
	$pres=@lfmsql_query("SELECT enablepdate,rank FROM ".$prefix."membertypes WHERE mtid=$mtype");
	$prow=@lfmsql_fetch_object($pres);
	$enablepdate=$prow->enablepdate;
	$rank=$prow->rank;
	
	// Get flow setting
	$flow=0;
	$fres=@lfmsql_query("SELECT flow FROM ".$prefix."settings");
	$frow=@lfmsql_fetch_object($fres);
	$flow=$frow->flow;
	
	// Get all the pages not included with the member level
	if($flow == 1)
	{
		
		$getacctypes = @lfmsql_query("SELECT mtid FROM ".$prefix."membertypes WHERE rank>$rank");
		while($gotacctypes=@lfmsql_fetch_object($getacctypes)) {
			$notacctypes[] = $gotacctypes->mtid;
		}
		$notacctypes[] = 9999;
		
		$getpages=@lfmsql_query("SELECT * FROM ".$prefix."memberpages WHERE menu=1 AND mtype IN (".implode(",", $notacctypes).") ORDER BY pageindex");
		
	}
	else
	{
		$getpages=@lfmsql_query("SELECT * FROM ".$prefix."memberpages WHERE menu=1 AND mtype!=$mtype ORDER BY pageindex");
	}

  if(lfmsql_num_rows($getpages) == 0)
      return "";

   $userMenu = "";

	while($menu=@lfmsql_fetch_object($getpages))
	{
		$checkpurchased = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."purchasedpages WHERE userid=".$userid." AND pageid=".$menu->pageid), 0);
		if ($checkpurchased > 0) {
			$userMenu .="<li><a href=\"members.php?page=$menu->pagetag\"><span>".$menu->pagename."</span></a></li>";
		}
	}

   return $userMenu;
	
}

function BuildUserMenu($daysjoined,$mtype,$joindate)
{
	include "config.php";
	// Check if publish date is enabled
	$enablepdate=0;
	$rank=0;
	$pres=@lfmsql_query("SELECT enablepdate,rank FROM ".$prefix."membertypes WHERE mtid=$mtype");
	$prow=@lfmsql_fetch_object($pres);
	$enablepdate=$prow->enablepdate;
	$rank=$prow->rank;

	// Get flow setting
	$flow=0;
	$fres=@lfmsql_query("SELECT flow FROM ".$prefix."settings");
	$frow=@lfmsql_fetch_object($fres);
	$flow=$frow->flow;

	// Display the menu based on days of membership and publish date
	$published="";
	if($enablepdate == 1) { $published="AND publishdate > '$joindate' "; }

	// If flow is enabled, this user will have access to their level and below
	if($flow == 1)
	{
		$res=@lfmsql_query("SELECT pagetag,pagename FROM ".$prefix."memberpages p LEFT JOIN ".$prefix."membertypes m ON p.mtype = m.mtid WHERE menu=1 AND rank<=$rank ORDER BY pageindex");
	}
	else
	{
		$res=@lfmsql_query("SELECT * FROM ".$prefix."memberpages WHERE menu=1 AND mtype=$mtype ORDER BY pageindex");
	}

  if(lfmsql_num_rows($res) == 0)
      return "";

   $userMenu = '';

	while($menu=@lfmsql_fetch_object($res))
	{
		$userMenu .="<li><a href=\"members.php?page=$menu->pagetag\"><span>".$menu->pagename."</span></a></li>";
	}

   return $userMenu;
}


function getip()
{
	if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
		$ip = getenv("HTTP_CLIENT_IP");
	else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");
	else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
		$ip = getenv("REMOTE_ADDR");
	else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
		$ip = $_SERVER['REMOTE_ADDR'];
	else
		$ip = "unknown";

	return($ip);
}

function parseUrl($url)
{
    $r  = "^(?:(?P<scheme>\w+)://)?";
    $r .= "(?:(?P<login>\w+):(?P<pass>\w+)@)?";
    $r .= "(?P<host>(?:(?P<subdomain>[\w\.]+)\.)?" . "(?P<domain>\w+\.(?P<extension>\w+)))";
    $r .= "(?::(?P<port>\d+))?";
    $r .= "(?P<path>[\w/]*/(?P<file>\w+(?:\.\w+)?)?)?";
    $r .= "(?:\?(?P<arg>[\w=&]+))?";
    $r .= "(?:#(?P<anchor>\w+))?";
    $r = "!$r!";                                                // Delimiters

    preg_match ( $r, $url, $out );

    return $out;
}

function SendWelcomeEmail($member_email)
{
	include "config.php";
	// Query settings table for sitename etc
	$res=@lfmsql_query("SELECT sitename,replyaddress FROM ".$prefix."settings") or die("1040: Unable to find ".$prefix."settings!");
	$row=@lfmsql_fetch_array($res);
	$sitename=$row["sitename"];
	$replyemail=$row["replyaddress"];

	// Get the welcome email
	$eres=@lfmsql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Welcome Email'");
	$erow=@lfmsql_fetch_array($eres);

	$msgbody=translate_site_tags($erow["template_data"]);
	$msgbody=translate_user_tags($msgbody,$member_email);

	// Send a welcome email
	$now = date("ymdHis");
	$headers = "From: ".$sitename." <".$replyemail.">"."\r\n";
	$headers .= "Return-Path: ".$sitename." <".$replyemail.">"."\r\n";
	$headers .= "Message-ID: <".$now."TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";

	$subject = "Welcome to ".$sitename;
	mail($member_email,$subject,$msgbody,$headers);
}

function SendNotifyDownline($r_id)
{
	include "config.php";
	// Query settings table for sitename etc
	$res=@lfmsql_query("SELECT sitename,replyaddress FROM ".$prefix."settings") or die("1068: Unable to find settings!");
	$row=@lfmsql_fetch_array($res);
	$sitename=$row["sitename"];
	$replyemail=$row["replyaddress"];

	// Get the referral email
	$refres=@lfmsql_query("SELECT email FROM ".$prefix."members WHERE Id=".$r_id);
	$refemail=@lfmsql_result($refres,0);

	$eres=@lfmsql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Downline Email'") or die(lfmsql_error());
	$erow=@lfmsql_fetch_array($eres);

	$msgbody=translate_site_tags($erow["template_data"]);
	$msgbody=translate_user_tags($msgbody,$refemail);

	// Send a referral email
	$now = date("ymdHis");
	$headers = "From: ".$sitename." <".$replyemail.">"."\r\n";
	$headers .= "Return-Path: ".$sitename." <".$replyemail.">"."\r\n";
	$headers .= "Message-ID: <".$now."TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";

	$subject = "New referral at ".$sitename;
	mail($refemail,$subject,$msgbody,$headers);

}

function getContentPages($prefix,$parent)
{
	include "config.php";
    $cpres=lfmsql_query("SELECT * FROM ".$prefix."memberpages");

    while($contentpage=lfmsql_fetch_object($cpres))
    {
			$selected = "";

	if ($parent == $contentpage->pageid){
		$selected = "selected = 'selected'";
		}
        $cpret.="\n<option value=\"$contentpage->pageid\" $selected  >".stripslashes($contentpage->pagename)."</option>";
    }
    return $cpret;
}

function block_html_chars($filterstring) {
	$filterstring = str_ireplace('<', '', $filterstring);
	$filterstring = str_ireplace('>', '', $filterstring);
	$filterstring = str_ireplace(';', '', $filterstring);
	$filterstring = str_ireplace('"', '', $filterstring);
	$filterstring = str_ireplace('&quot', '', $filterstring);
	$filterstring = str_ireplace('&nbsp', '', $filterstring);
	$filterstring = str_ireplace('&lt', '', $filterstring);
	$filterstring = str_ireplace('&gt', '', $filterstring);
	$filterstring = str_ireplace('&#34', '', $filterstring);
	$filterstring = str_ireplace('&#160', '', $filterstring);
	$filterstring = str_ireplace('&#60', '', $filterstring);
	$filterstring = str_ireplace('&#62', '', $filterstring);
	return $filterstring;
}

function text_filter($filterstring, $type=1) {
	if ($type == 1) {
		// Letters and numbers only
		$filterstring = preg_replace('/[^A-Za-z0-9]/', '', $filterstring);
	} elseif ($type == 2) {
		// Block HTML chars
		$filterstring = block_html_chars($filterstring);
	}
	return $filterstring;
}

function check_email($emailstr) {
	if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,30})$/', $emailstr)) {
		return true;
	} else {
		return false;
	}
}

function check_number($number) {
	if (!is_numeric($number)) { return false; }
	$teststring = (string)$number;
	$stringlength = strlen($teststring);
	if ($stringlength == 0) { return false; }
	for ($i = 0; $i < $stringlength; $i++) {
		$testchar = ord($teststring{$i});
		if (($testchar<48) || ($testchar>57)) {
			return false;
		}
	}
	return true;
}

function bannedIp($ipaddress)
{
	include "config.php";
	$splitip = preg_split("/\./", $ipaddress);
	$checkip1 = $splitip[0]."***";
	$checkip2 = $splitip[0].$splitip[1]."**";
	$checkip3 = $splitip[0].$splitip[1].$splitip[2]."*";
	$checkip4 = $splitip[0].$splitip[1].$splitip[2].$splitip[3];
	
	$checkip1 = str_replace(".", "", $checkip1);
	$checkip2 = str_replace(".", "", $checkip2);
	$checkip3 = str_replace(".", "", $checkip3);
	$checkip4 = str_replace(".", "", $checkip4);
	
	$getbannedip = lfmsql_query("Select banned FROM ".$prefix."banips");
	for ($i = 0; $i < lfmsql_num_rows($getbannedip); $i++) {
		$bannedip = lfmsql_result($getbannedip, $i, "banned");
		$bannedip = str_replace(".", "", $bannedip);
		if(($bannedip == $checkip1) || ($bannedip == $checkip2) || ($bannedip == $checkip3) || ($bannedip == $checkip4)) {
			return true;
		}
	}
	
	return false;
}

function bannedEmail($email)
{
	include "config.php";
	$getbannedemail = lfmsql_query("Select banned FROM ".$prefix."banemails");
	for ($i = 0; $i < lfmsql_num_rows($getbannedemail); $i++) {
		$bannedemail = lfmsql_result($getbannedemail, $i, "banned");
		if (strpos($email, $bannedemail) !== false) {
			return true;
		}
	}
	
	return false;
}

function fileExtension($file) {
    $fileExp = explode('.', $file);
	return $fileExp[count($fileExp) -1];
}


?>