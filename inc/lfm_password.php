<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

function lfmMakePassword($passwordstr) {
	if (strlen($passwordstr) < 1) {
		return false;
	}
	$salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
	$salt = base64_encode($salt);
	$salt = str_replace('+', '.', $salt);
	$hash = crypt($passwordstr, '$2y$10$'.$salt.'$');
	return $hash;
}

function lfmCheckPassword($passwordstr, $hash) {
	if (strlen($passwordstr) < 1 || strlen($hash) < 1) {
		return false;
	}
	if (crypt($passwordstr, $hash) == $hash) {
		return true;
	} else {
		return false;
	}
}

?>