<?php
// /////////////////////////////////////////////////////////////////////
// LFM, LFMTE, and LFMVM SQL Functions
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// /////////////////////////////////////////////////////////////////////

// These functions provide a layer between the script and PHP's mysql extension,
// to make it easier to eventually transition to an alternate extension such as
// mysqli.  Note that many parts of the script and plugins still contain direct
// mysql functions at the current time, and some files will need to be updated
// to include this functions file (always use require_once to prevent multiple
// includes of a function file).

// This variable sets the PHP database extension to use.
// Only mysql is fully supported at this time.
$GLOBALS['lfmsqlext'] = "mysql";

// This variable holds the database connection link identifier.
// This is optional with mysql but required with mysqli.
$GLOBALS['lfmsqllink'] = NULL;

function lfmsql_get_ext_type() {
	return $GLOBALS['lfmsqlext'];
}

function lfmsql_connect($sqlserver, $sqluser, $sqlpassword, $globallink=1) {
	$link = mysql_connect($sqlserver, $sqluser, $sqlpassword);
	if ($globallink == 1) {
		$GLOBALS['lfmsqllink'] = $link;
	}
	return $link;
}

function lfmsql_select_db($sqldb, $sqllink=0) {
	if ($sqllink == 0 && $GLOBALS['lfmsqllink'] === NULL) {
		$result = mysql_select_db($sqldb);
	} elseif ($sqllink == 0 && $GLOBALS['lfmsqllink'] !== NULL) {
		$result = mysql_select_db($sqldb, $GLOBALS['lfmsqllink']);
	} else {
		$result = mysql_select_db($sqldb, $sqllink);
	}
	return $result;
}

function lfmsql_close($sqllink=0) {
	if ($sqllink == 0 && $GLOBALS['lfmsqllink'] === NULL) {
		$result = mysql_close();
	} elseif ($sqllink == 0 && $GLOBALS['lfmsqllink'] !== NULL) {
		$result = mysql_close($GLOBALS['lfmsqllink']);
		$GLOBALS['lfmsqllink'] = NULL;
	} else {
		$result = mysql_close($sqllink);
		if ($sqllink == $GLOBALS['lfmsqllink']) {
			$GLOBALS['lfmsqllink'] = NULL;
		}
	}
	return $result;
}

function lfmsql_error($sqllink=0) {
	if ($sqllink == 0 && $GLOBALS['lfmsqllink'] === NULL) {
		$result = mysql_error();
	} elseif ($sqllink == 0 && $GLOBALS['lfmsqllink'] !== NULL) {
		$result = mysql_error($GLOBALS['lfmsqllink']);
	} else {
		$result = mysql_error($sqllink);
	}
	return $result;
}

function lfmsql_query($sqlquery, $sqllink=0) {
	if ($sqllink == 0 && $GLOBALS['lfmsqllink'] === NULL) {
		$result = mysql_query($sqlquery);
	} elseif ($sqllink == 0 && $GLOBALS['lfmsqllink'] !== NULL) {
		$result = mysql_query($sqlquery, $GLOBALS['lfmsqllink']);
	} else {
		$result = mysql_query($sqlquery, $sqllink);
	}
	return $result;
}

function lfmsql_result($sqlresource, $sqlrow=0, $sqlfield="0") {
	if ($sqlfield == "0") {
		$result = @mysql_result($sqlresource, $sqlrow);
	} else {
		$result = @mysql_result($sqlresource, $sqlrow, $sqlfield);
	}
	return $result;
}

function lfmsql_num_rows($sqlresource) {
	$result = mysql_num_rows($sqlresource);
	return $result;
}

function lfmsql_fetch_object($sqlresource) {
	$result = mysql_fetch_object($sqlresource);
	return $result;
}

function lfmsql_fetch_array($sqlresource) {
	$result = mysql_fetch_array($sqlresource);
	return $result;
}

function lfmsql_fetch_assoc($sqlresource) {
	$result = mysql_fetch_assoc($sqlresource);
	return $result;
}

function lfmsql_insert_id($sqllink=0) {
	if ($sqllink == 0 && $GLOBALS['lfmsqllink'] === NULL) {
		$result = mysql_insert_id();
	} elseif ($sqllink == 0 && $GLOBALS['lfmsqllink'] !== NULL) {
		$result = mysql_insert_id($GLOBALS['lfmsqllink']);
	} else {
		$result = mysql_insert_id($sqllink);
	}
	return $result;
}

?>