<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// ///////////////////////////////////////////////////////////////////// 
 
    require_once "../inc/filter.php";

    // Prevent anyone who isn't logged in from opening this page
    include "../inc/checkauth.php"; 
    if(!isset($_SESSION["adminid"])) { exit; };
    
    function addusertophpBB($username,$password,$email,$phpbb_host,$phpbb_db,$phpbb_user,$phpbb_pass,$phpbb_prefix,$phpbb_group)
	{
		$pconn2=@mysql_connect($phpbb_host,$phpbb_user,$phpbb_pass);
		$pdb=@mysql_select_db($phpbb_db,$pconn2);

        $user_fields['user_regdate'] = time();
        $user_fields['user_from'] = '';
        $user_fields['user_occ'] = '';
        $user_fields['user_interests'] = '';
        $user_fields['user_website'] = '';
        $user_fields['user_icq'] = '';
        $user_fields['user_aim'] = '';
        $user_fields['user_yim'] = '';
        $user_fields['user_msnm'] = '';
        $user_fields['user_sig'] = '';
        $user_fields['user_avatar'] = '';
        $user_fields['user_avatar_type'] = 0;
        $user_fields['user_viewemail'] = 0;
        $user_fields['user_attachsig'] = 1;
        $user_fields['user_allowsmile'] = 1;
        $user_fields['user_allowhtml'] = 0;
        $user_fields['user_allowbbcode'] = 1;
        $user_fields['user_allow_viewonline'] = 1;
        $user_fields['user_notify'] = 0;
        $user_fields['user_notify_pm'] = 1;
        $user_fields['user_popup_pm'] = 1;
        $user_fields['user_timezone'] = 0.00;
        $user_fields['user_dateformat'] = 'D M d, Y g:i a';
        $user_fields['user_lang'] = 'english';
        $user_fields['user_style'] = 1;
        $user_fields['user_level'] = 0;
        $user_fields['user_posts'] = 0;

		$user_id=0;
		$group_id=0;
		$personalgroup=0;
		
		// Check whether this member is already in phpBB
		$sql="SELECT user_id FROM ".$phpbb_prefix."users WHERE username='".$username."' AND user_email='".$email."'";
		$res=@mysql_query($sql);
		
		if(mysql_num_rows($res) == 0)
		{
			// Get the max user id from the users table
			$sql = "SELECT MAX(user_id) AS total FROM ".$phpbb_prefix."users";
			$res = mysql_query($sql,$pconn2) or die(mysql_error());
			$row=@mysql_fetch_array($res);
			$user_id=$row["total"]+1;
			
			// Build the main SQL query for phpBB ver 2.x
			if($phpbbver == 2)
			{
				$bbqry = "INSERT INTO ".$phpbb_prefix."users(user_id, username, user_regdate, user_password, user_email, user_icq, 
				user_website, user_occ, user_from, user_interests, user_sig, user_sig_bbcode_uid, user_avatar, user_avatar_type, 
				user_viewemail, user_aim, user_yim, user_msnm, user_attachsig, user_allowsmile, user_allowhtml, user_allowbbcode, 
				user_allow_viewonline, user_notify, user_notify_pm, user_popup_pm, user_timezone, user_dateformat, user_lang, 
				user_style, user_level, user_allow_pm, user_active, user_actkey, user_posts) ";
				
				$bbqry .= "VALUES (" . $user_id . ", '" . $username . "', '" . $user_fields['user_regdate'] . "',
				 MD5('" . $password . "'), '" . $email . "', '" . $user_fields['user_icq'] . "',
				 '" . $user_fields['user_website'] . "', '" . $user_fields['user_occ'] . "', '" . $user_fields['user_from'] . "',
				 '" . $user_fields['user_interests'] . "', '" . $user_fields['user_sig'] . "',
				 '" . $user_fields['user_sig_bbcode_uid'] . "', '" . $user_fields['user_avatar'] . "',
				 '" . $user_fields['user_avatar_type'] . "', " . $user_fields['user_viewemail'] . ",
				 '" . str_replace(' ', '+', $user_fields['user_aim']) . "', '" . $user_fields['user_yim'] . "',
				 '" . $user_fields['user_msnm'] . "', " . $user_fields['user_attachsig'] . ", " . $user_fields['user_allowsmile'] . ",
				 " . $user_fields['user_allowhtml'] . ", " . $user_fields['user_allowbbcode'] . ",
				 " . $user_fields['user_allow_viewonline'] . ", " . $user_fields['user_notify'] . ", " . $user_fields['user_notify_pm'] . ",
				 " . $user_fields['user_popup_pm'] . ", " . $user_fields['user_timezone'] . ", '" . $user_fields['user_dateformat'] . "',
				 '" . $user_fields['user_lang'] . "', " . $user_fields['user_style'] . ", " . $user_fields['user_level'] . ", 1, 1, '',
				 '" . $user_fields['user_posts'] . "')";
			}

			// Build the main SQL query for phpBB ver 3.x
			if($phpbbver == 3)
			{
				$bbqry = "INSERT INTO ".$phpbb_prefix."users(user_id,user_type,group_id,username,username_clean,user_password,user_regdate,user_style,user_lang,user_email,user_passchg,user_dst,user_ip) ";
				$bbqry.="VALUES (".$user_id.",0,2,'".$username."','".$username."','".$password."',". time() .",1,'en','".$email."',0,1,'127.0.0.1')";
			}
			
			$res=@mysql_query($bbqry,$pconn2) or die(mysql_error());
	
			// Insert the personal group
			if($phpbbver == 2)
			{
				$sql = "INSERT INTO ".$phpbb_prefix."groups (group_name, group_description, group_single_user, group_moderator)
					VALUES ('', 'Personal User', 1, 0)";
				$res=@mysql_query($sql,$pconn2);
					
				// Add this group entry to the user_group table
				$res=@mysql_query("SELECT MAX(group_id) as personalgroup FROM ".$phpbb_prefix."groups",$pconn2) or die(mysql_error());
				$row=@mysql_fetch_array($res);
				$personalgroup=$row["personalgroup"]+1;
				
				$sql = "INSERT INTO ".$phpbb_prefix."user_group(user_id, group_id, user_pending)
					VALUES ($user_id, $personalgroup, 0)";
				$res=@mysql_query($sql,$pconn2);

				// Add user to defined group
				$sql = "SELECT group_id FROM ".$phpbb_prefix."groups WHERE group_name='".trim($phpbb_group)."'";
				$res=@mysql_query($sql,$pconn2) or die(mysql_error());
				$row=@mysql_fetch_array($res);
				$group_id=$row["group_id"];
				
				// Insert the user_group entry
				$sql = "INSERT INTO ".$phpbb_prefix."user_group(user_id, group_id, user_pending)
					VALUES ($user_id, $group_id, 0)";
				$res=@mysql_query($sql,$pconn2) or die(mysql_error());
			}

			if($phpbbver == 3)
			{
				// Add user to default group			
				$sql = "INSERT INTO ".$phpbb_prefix."user_group (group_id,user_id,group_leader,user_pending)
            		VALUES (2,$user_id,0,0)"; 			
				$res=@mysql_query($sql,$pconn2) or die(mysql_error());

				// Add user to defined group
				$sql = "SELECT group_id FROM ".$phpbb_prefix."groups WHERE group_name='".trim($phpbb_group)."'";
				$res=@mysql_query($sql,$pconn2) or die(mysql_error());
				$row=@mysql_fetch_array($res);
				$group_id=$row["group_id"];
				
				// Insert the user_group entry
				$sql = "INSERT INTO ".$phpbb_prefix."user_group(group_id,user_id,group_leader,user_pending)
					VALUES ($group_id,$user_id,0,0)";
				$res=@mysql_query($sql,$pconn2) or die(mysql_error());

				$sql = "UPDATE ".$prefix."config SET config_value='".$username."' WHERE config_name='newest_username'"; 
				$res=@mysql_query($sql,$pconn2) or die(mysql_error());
			}
		}
    }
	
	// Synchronise phpBB
	$username='';
	$password='';
	$email='';
	
	// Go through the members table and add any member who is not already in the phpBB user table
	$sql="SELECT username,password,email FROM ".$prefix."members";
	$ures=@mysql_query($sql) or die(mysql_error());
	while($urow=@mysql_fetch_array($ures))
	{
		addusertophpBB($urow["username"],$urow["password"],$urow["email"],$phpbb_host,$phpbb_db,$phpbb_user,$phpbb_pass,$phpbb_prefix,$phpbb_group);
	}
?>
