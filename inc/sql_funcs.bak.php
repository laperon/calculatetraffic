<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.14
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "filter.php";
require_once "snoopy.class.php";

	if(!function_exists('http_build_query')) {
		function http_build_query($data,$prefix=null,$sep='',$key='') {
			$ret    = array();
				foreach((array)$data as $k => $v) {
					$k    = urlencode($k);
					if(is_int($k) && $prefix != null) {
						$k    = $prefix.$k;
					};
					if(!empty($key)) {
						$k    = $key."[".$k."]";
					};

					if(is_array($v) || is_object($v)) {
						array_push($ret,http_build_query($v,"",$sep,$k));
					}
					else {
						array_push($ret,$k."=".urlencode($v));
					};
				};

			if(empty($sep)) {
				$sep = ini_get("arg_separator.output");
			};

			return    implode($sep, $ret);
		};
	};

	function Post($url, $post = null)
	{
		/*$context = array();

		if (is_array($post))
		{
			ksort($post);
			$context['http'] = array
			(
				'method' => 'POST',
				'content' => http_build_query($post, '', '&'),
			);
		}
*/
	   $snoopy = new Snoopy;
      $snoopy->submit($url, $post);

		return $snoopy->results;


	}


	function sanitize_sql($sql_query)
	{
    	if( get_magic_quotes_gpc() )
	    {
    	      $sql_query = stripslashes( $sql_query );
	    }
    	//check if this function exists
	    if( function_exists( "mysql_real_escape_string" ) )
    	{
	          $sql_query = mysql_real_escape_string( $sql_query );
	    }
	    //for PHP version < 4.3.0 use addslashes
	    else
	    {
	          $sql_query = addslashes( $sql_query );
	    }
	    return $sql_query;
	}

/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2012 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////
function decimal($amount) {
	if (substr($amount,-2,1)==".") { $amount.="0"; }
	if (strpos($amount, '.') === false) { $amount.=".00"; }
	$y=strlen($amount)-1;
	for($x=0;$x<$y;$x++) {
		if(substr($amount,$x,1)==".") { $amount=substr($amount,0,($x+3)); }
		}
	if($amount[0]==".") { $amount="0".$amount; }
	return $amount;
}

function show_button_code($item,$a=6,$extra=0) {
 global $usrid;

if (isset($_SESSION["prefix"])) {
	$prefix = $_SESSION["prefix"];
} else {
	$prefix = "oto_";
}

if (isset($_SESSION["userid"]) && is_numeric($_SESSION["userid"])) {
	$usrid = $_SESSION["userid"];
} else {
	$usrid = 0;
	$code="<center><b>You must login to make this purchase</b><center>";
	return $code;
}

$res = @mysql_query("SELECT * FROM ".$prefix."ipn_merchants where name!='Comms';");
$merchant=array();
for ($i = 1; $i <= mysql_num_rows($res); $i++) {
	$row=mysql_fetch_array($res);
	$merchant[$i]['name']=$row['name'];
	$merchant[$i]['payee']=$row['payee'];
	$merchant[$i]['buy_now']=$row['buy_now'];
	$merchant[$i]['subscribe']=$row['subscribe'];
	}
$res = @mysql_query("SELECT * FROM ".$prefix."ipn_vals WHERE id>0 LIMIT 5;");
$adminvals=array();
while($row=mysql_fetch_array($res)) {
	$adminvals[$row['field']]=$row['value'];
	}
$res = @mysql_query("SELECT * FROM ".$prefix."ipn_products WHERE id='$item';");
$row=mysql_fetch_array($res);
if($row['type']!="N") {
	
	if ($usrid == 0) {
		$code="<center><b>You must login to create a subscription</b><center>";
		return $code;
	}

	$button="subscribe";
	$trial_amount = $row['trial_amount'];
	$trial_period = $row['trial_period'];
	$trial_type = $row['trial_type'];
	$trial_days = $trial_period;
	if($trial_type=="D") { $trial_unit="day"; }
		elseif($trial_type=="M") { $trial_unit="month"; $trial_days=$trial_days*30; }
		elseif($trial_type=="Y") { $trial_unit="year"; $trial_days=$trial_days*360; }
	$days=$row['period'];
	if($row[type]=="D") { $unit="day"; }
		elseif($row['type']=="M") { $unit="month"; $days=$days*30; }
		elseif($row['type']=="Y") { $unit="year"; $days=$days*360; }
	if($trial_type=="N") {
		$trial_amount=$row['amount'];
		$trial_period=$row['period'];
		$trial_type=$row['type'];
		$trial_days=$days;
		$trial_unit=$unit;
		}
	} else {
	$button="buy_now";
	}
	
	// Pay With Commissions Mod
	if ($usrid > 0) {
		$mcomm = mysql_result(mysql_query("SELECT SUM(commission) from ".$prefix."sales where affid=$usrid and status IS NULL"), 0);
		if (!is_numeric($mcomm) || $mcomm < 0) { $mcomm = "0.00"; }
		if ($row['amount'] <= $mcomm) {
			$comm_index = count($merchant)+1;
			$getcomm = mysql_query("SELECT * FROM ".$prefix."ipn_merchants where name='Comms';");
			$merchant[$comm_index]['name'] = "Comms";
			$merchant[$comm_index]['payee'] = "None";
			$merchant[$comm_index]['buy_now'] = mysql_result($getcomm, 0, "buy_now");
			$merchant[$comm_index]['subscribe'] = mysql_result($getcomm, 0, "subscribe");
		}
	}
	// End Pay With Commissions Mod

if($extra) { $row['name'].=" - ".$extra; }
$code='<table align="center"><tr><td width=5></td>';
	for($f = 1; $f <= count($merchant); $f++) {
if (($merchant[$f]['name'] == "2CheckOut") && ($row['twocoid'] != 0)) {
$showid = $row['twocoid'];
} else {
$showid = $row['id'];
}
	   $m="merchant$f";
	   if($row[$m]) {
		$buy=str_replace('[user_id]', $usrid, $merchant[$f][$button]);
		$buy=str_replace('[merchant_id]', $merchant[$f]['payee'], $buy);
		$buy=str_replace('[item_number]', $showid, $buy);
		$buy=str_replace('[item_name]', $row['name'], $buy);
		$buy=str_replace('[amount]', $row['amount'], $buy);
		$buy=str_replace('[period]', $row['period'], $buy);
		$buy=str_replace('[type]', $row['type'], $buy);
		$buy=str_replace('[days]', $days, $buy);
		$buy=str_replace('[unit]', $unit, $buy);
		$buy=str_replace('[trial_amount]', $trial_amount, $buy);
		$buy=str_replace('[trial_period]', $trial_period, $buy);
		$buy=str_replace('[trial_type]', $trial_type, $buy);
		$buy=str_replace('[trial_days]', $trial_days, $buy);
		$buy=str_replace('[trial_unit]', $trial_unit, $buy);
		$buy=str_replace('[notify]', $adminvals['notify'], $buy);
		$buy=str_replace('[return]', $adminvals['return'], $buy);
		$buy=str_replace('[cancel]', $adminvals['cancel'], $buy);
		$buy=str_replace('[images]', $adminvals['images'], $buy);
		$buy=str_replace('[merchant_button]', '/ipnimg.php?rnum='.rand(0,999999).'&merch='.$f.'&id='.$row['id'], $buy);
		$buy=str_replace('[sitename]', $adminvals['sitename'], $buy);
		$code.='<td align="center">'.$buy.'</td><td width=5></td>';
		if(round($f/$a)==$f/$a) { $code.='</tr><tr><td width=5></td>'; }
		}
	   }
$code.='</td></table>';
if($row['trial_type']<>"N" && $usrid>0) {
	$res2 = @mysql_query("SELECT * FROM ".$prefix."ipn_transactions WHERE user_id='$usrid' AND item_number='$item';");
	if(mysql_num_rows($res2)) { $code="<center><b>You have already signed up for this trial</b><center>"; }
}
return $code;
}
//End IPN Functions

?>