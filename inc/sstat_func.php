<?php

// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////
//    Surf Logs Originally Developed By:    //
//       Chris Houg, LFMTE-Host.com         //
//////////////////////////////////////////////

$time = time();
// Mysql connection
function db_conn() {
	mysql_connect($dbhost,$dbuser,$dbpass);
	mysql_select_db($dbname) or die("Query failed: " . mysql_error());;
}

//
/**
 * used in the sstats_delete.php file to indicate number of rows that will be deleted
 *
 * @param $time
 * @param $prefix
 * @return the number of rows
 */
function  get_num($time,$prefix){
	include_once "../inc/config.php";
	$aresult = mysql_query("SELECT id FROM ".$prefix."sstats WHERE adate < '$time'")
	or die("Query failed: " . mysql_error());
	$number = mysql_num_rows($aresult);
	return $number;
}


/**
 * Delete the surflog stats from the table
 *
 * @param $time delete older than this timestamp
 * @param $prefix
 */
function del_stats($time,$prefix) {
	mysql_query("DELETE FROM ".$prefix."sstats WHERE adate < '$time'")
	or die("Query failed: " . mysql_error());
	mysql_query("OPTIMIZE TABLE ".$prefix."sstats")
	or die("Query failed: " . mysql_error());
}

/**
 * Enter description here...
 *
 * @param $prefix
 * @param $usersterm user search term
 * @param $sterm date search term
 * @param $eetime Time
 * @param $orderby asc/desc
 * @param $sortby sort by column
 * @param  $offset to determine what page
 * @param  $limit number of url's per table
 * @param  $highlightid highlights a particular row id
 * @return the table
 */
function build_table($prefix,$usersterm,$sterm,$eetime,$orderby,$sortby,$offset,$limit,$highlightid=0) {
	$result = mysql_query("SELECT * FROM  ".$prefix."sstats WHERE $usersterm $sterm $eetime ORDER BY $orderby $sortby limit $offset,$limit")
	or die("Query failed: " . mysql_error());
	$i=1;

	while ($row = mysql_fetch_array($result)) {

		if ( $i % 2 != 0) $rowColor="EEEEEE"; else $rowColor="FFFFFF";
		
		if ($row[id] == $highlightid) { $rowColor="FBEC5D";  }

		$atime = date('H:i:s',$row[adate]);
		$adate=date("Y-m-d",$row[adate]);

		$rest = substr ("$row[url]", 0, 50);
		$table .= "<tr bgcolor=$rowColor><td><span class=\"s9\">$row[id]</b></span></td><td><span class=\"s9\"><b>
		<a href=\"/admin/admin.php?f=sstats&action=$action&sort=$sort&userid=$row[userid]&highlightid=$highlightid\">$row[userid]</span></td>
		<td><span class=\"s9\">";
		
		if ($row[websiteid] > 0) {
			$table .= "<a href=\"/adminchecksite.php?siteid=$row[websiteid]\" target=\"_blank\">$rest</a>";
		} else {
			$table .= $rest;
		}
		
		$table .= "</span></td>
		<td><span class=\"s81red\">$atime</b></span></td><td><span class=\"s81red\">$adate</b></span></td>
		<td>";
		
		if ($row[websiteid] > 0) {
			$table .= "<a href=\"/admin/admin.php?f=smm&show=&sortby=id&sortorder=ASC&sf=browse&searchfield=id&searchtext=$row[websiteid]\" target=\"_blank\"><span class=\"s9\">$row[websiteid]</span></a>";
		} else {
			$table .= "<span class=\"s9\">$row[websiteid]</span>";
		}
		
		$table .= "</td></tr>";
		$i++;
	}
	return $table;
}

/**
 * get cron setting from db
 */
function get_cronsetting($prefix) {
	$adminsetting = mysql_query("SELECT sstats from ".$prefix."settings where 1");
	$cronsetting = mysql_result($adminsetting, 0);

	return $cronsetting;
}

/**
 * Display the menu for navigation
 *
 * @param $action current page
 * @return
 */
function get_menu($action) {
	$menu = "<span class=\"s8\"><center>";
	
	if ($action != "1all") { $menu .= "[<a href=\"/admin/admin.php?f=sstats&action=1all&sort=1\">Traffic: Last Hour</a>]&nbsp;&nbsp;"; } else { $menu .= "[<b>Traffic: Last Hour</b>]&nbsp;&nbsp;"; }
	
	if ($action != "24all") { $menu .= "[<a href=\"/admin/admin.php?f=sstats&action=24all&sort=1\">Traffic: Last 24 hours</a>]&nbsp;&nbsp;"; } else { $menu .= "[<b>Traffic: Last 24 hours</b>]&nbsp;&nbsp;"; }
	 
	if ($action != "48all") { $menu .= "[<a href=\"/admin/admin.php?f=sstats&action=48all&sort=1\">Traffic: Last 48 hours</a>]&nbsp;&nbsp;"; } else { $menu .= "[<b>Traffic: Last 48 hours</b>]&nbsp;&nbsp;"; }
	 
	if ($action != "") { $menu .= "[<a href=\"/admin/admin.php?f=sstats&sort=1\">Traffic: All hours</a>]&nbsp;&nbsp;"; } else { $menu .= "[<b>Traffic: All hours</b>]&nbsp;&nbsp;"; }
	
	$menu .= "[<a href=\"/admin/admin.php?f=sstats&action=sdelete\">Delete rows</a>]</span>";

	return $menu;
}

/**
 * change the cron setting
 *
 * @param $setting
 * @param $prefix
 */
function cronchange($setting,$prefix) {
	mysql_query("UPDATE ".$prefix."settings SET sstats=$setting");
}

/*
	license check function and check removed

*/

/**
 * Get the title of the current page
 *
 * @param $setting current $action
 * @return unknown
 */
function get_title($setting) {
	switch ($setting) {
				case "1all":
			$title = "<p>&nbsp</p><span class=\"s8black\"><center>Traffic: Last Hour</span><br /><br />";
			break;
		case "24all":
			$title = "<p>&nbsp</p><span class=\"s8black\"><center>Traffic: Last 24 Hours</span><br /><br />";
			break;
		case "48all":
			$title = "<p>&nbsp</p><span class=\"s8black\"><center>Traffic: Last 48 Hours</span><br /><br />";
			break;
		default:
			$title = "<p>&nbsp</p><span class=\"s8black\"><center>Traffic: All Stats</span><br /><br />";
	}
	return $title;
}

?>