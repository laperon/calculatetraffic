<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.12
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////
//
//  Changelog
//  =========
///////////////////////////////////////////////////////////////

// Query settings table for sitename
$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
$row=@mysql_fetch_array($res);

// Get the site details
$sitename=$row["sitename"];
$sitedesc=$row["sitedesc"];
$theme_dir = $currentTheme = $row["theme"];

// Get paths and notification URL
$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$root_path=substr($fullurl,0,strrpos($fullurl, "/"));


function lfm_sitename()
{
   GLOBAL $sitename;

   echo $sitename;
}

function lfm_sitedescription() 
{
   GLOBAL $sitedesc;

   echo $sitedesc;
}

function lfm_head() {
   global $html_head;
   echo $html_head;

}

function lfm_page_content() {
   global $page_content;
   echo $page_content;
}

function lfm_HitsConnect_TrackingCode() {
   global $hcTrackingCode;
   
   if(isset($hcTrackingCode))
      echo "<img name=\"hc\" src=\"http://www.trker.com/trkconv.php?cid=$hcTrackingCode\" width=\"1\" height=\"1\" alt=\"\">";
}

function load_template($_template_file) {
   global $theme_dir;

	require_once($_template_file);
}

function lfm_login_url() {
   global $realscriptpath, $root_path;

   // Handle hosts with CGI PHP
   if(strlen($realscriptpath) > 10)
   {
      return $realscriptpath."/login.php?s=noauth";
   }
   else
   {
      return $root_path."/login.php?s=noauth";
   }
}

function lfm_show_user_menu() {
   global $joindate, $mtype;
   
   include "config.php";
   @mysql_connect($dbhost,$dbuser,$dbpass) or die("funcs user connect: ".mysql_error());
   @mysql_select_db($dbname) or die("funcs user select: ". mysql_error());
   if(isset($_SESSION["userid"]) && is_numeric($_SESSION["userid"]) && $_SESSION["userid"] > 0) {
   	$getuserdata = mysql_query("Select mtype, joindate from ".$prefix."members where Id=".$_SESSION["userid"]);
   	if (mysql_num_rows($getuserdata) > 0) {
		$mtype = mysql_result($getuserdata, 0, "mtype");
		$joindate = mysql_result($getuserdata, 0, "joindate");
	} else {
		$mtype = 1;
		$joindate = date("Y-m-d H:i:s");
	}
   }

   // Create the menu for 'virtual pages'
   $today=date("Y-m-d H:i:s");

   $daysjoined=date_diff2($joindate, $today);
   echo BuildUserMenu($daysjoined,$mtype,$joindate);
   
   // Get Individual Page Purchases
   if (function_exists('BuildPagesMenu')) {
   echo BuildPagesMenu($daysjoined,$mtype,$joindate,$_SESSION["userid"]);
   }

}

function get_theme_root() {
   global $root_path, $theme_dir;

   return dirname(dirname(__FILE__));
}

function get_theme_directory() {
   global $root_path, $theme_dir;

   return get_theme_root() . '/'.$theme_dir;
}

function get_theme_data( $theme_file ) {

	$theme_data = implode( '', file( $theme_file ) );
	$theme_data = str_replace ( '\r', '\n', $theme_data );
	preg_match( '|Theme Name:(.*)$|mi', $theme_data, $theme_name );
	preg_match( '|Theme URI:(.*)$|mi', $theme_data, $theme_uri );
	preg_match( '|Description:(.*)$|mi', $theme_data, $description );

	if ( preg_match( '|Version:(.*)|i', $theme_data, $version ) )
		$version = $version[1];
	else
		$version = '';

	$name = $theme = trim( $theme_name[1] );
	$theme_uri =  trim( $theme_uri[1] );
	$description = trim( $description[1] );

	if ( preg_match( '|Author:(.*)$|mi', $theme_data, $author_name ) ) {
		if ( empty( $author_uri ) ) {
			$author = trim( $author_name[1] );
		} else {
			$author = sprintf( '<a href="%1$s" title="%2$s">%3$s</a>', $author_uri, 'Visit author homepage', trim( $author_name[1] ));
		}
	} else {
		$author = 'Anonymous';
	}

	return array( 'Name' => $name, 'Title' => $theme, 'URI' => $theme_uri, 'Description' => $description, 'Author' => $author, 'Version' => $version, 'Template' => $template, 'Status' => $status, 'Tags' => $tags );
}

function get_themes() {
	global $lfm_themes, $lfm_broken_themes;

	if ( isset($lfm_themes) )
		return $lfm_themes;

	$themes = array();
	$lfm_broken_themes = array();
	$theme_loc = $theme_root = get_theme_root();

	// Files in themes directory and one subdir down
	$themes_dir = @ opendir($theme_root);
	if ( !$themes_dir )
		return false;

	while ( ($theme_dir = readdir($themes_dir)) !== false ) {
		if ( is_dir($theme_root . '/' . $theme_dir) && is_readable($theme_root . '/' . $theme_dir) ) {
			if ( $theme_dir{0} == '.' || $theme_dir == '..' || $theme_dir == 'CVS' )
				continue;

			$stylish_dir = @ opendir($theme_root . '/' . $theme_dir);
			$found_stylesheet = false;
			while ( ($theme_file = readdir($stylish_dir)) !== false ) {
				if ( $theme_file == 'style.css' ) {
					$theme_files[] = $theme_dir . '/' . $theme_file;
					$found_stylesheet = true;
					break;
				}
			}
			@closedir($stylish_dir);
			if ( !$found_stylesheet ) { // look for themes in that dir
				$subdir = "$theme_root/$theme_dir";
				$subdir_name = $theme_dir;
				$theme_subdir = @ opendir( $subdir );
				while ( ($theme_dir = readdir($theme_subdir)) !== false ) {
					if ( is_dir( $subdir . '/' . $theme_dir) && is_readable($subdir . '/' . $theme_dir) ) {
						if ( $theme_dir{0} == '.' || $theme_dir == '..' || $theme_dir == 'CVS' )
							continue;
						$stylish_dir = @ opendir($subdir . '/' . $theme_dir);
						$found_stylesheet = false;
						while ( ($theme_file = readdir($stylish_dir)) !== false ) {
							if ( $theme_file == 'style.css' ) {
								$theme_files[] = $subdir_name . '/' . $theme_dir . '/' . $theme_file;
								$found_stylesheet = true;
								break;
							}
						}
						@closedir($stylish_dir);
					}
				}
				@closedir($theme_subdir);
				$lfm_broken_themes[$theme_dir] = array('Name' => $theme_dir, 'Title' => $theme_dir, 'Description' => 'Stylesheet is missing.');
			}
		}
	}
	if ( is_dir( $theme_dir ) )
		@closedir( $theme_dir );

	if ( !$themes_dir || !$theme_files )
		return $themes;

	sort($theme_files);

	foreach ( (array) $theme_files as $theme_file ) {
		if ( !is_readable("$theme_root/$theme_file") ) {		   
			$lfm_broken_themes[$theme_file] = array('Name' => $theme_file, 'Title' => $theme_file, 'Description' => 'File not readable.');
			continue;
		}

		$theme_data = get_theme_data("$theme_root/$theme_file");

		$name        = $theme_data['Name'];
		$title       = $theme_data['Title'];
		$description = $theme_data['Description'];
		$version     = $theme_data['Version'];
		$author      = $theme_data['Author'];
		$template    = $theme_data['Template'];
		$stylesheet  = dirname($theme_file);

		$screenshot = false;
		foreach ( array('png', 'gif', 'jpg', 'jpeg') as $ext ) {
			if (file_exists("$theme_root/$stylesheet/screenshot.$ext")) {
				$screenshot = "screenshot.$ext";
				break;
			}
		}

		if ( empty($name) ) {
			$name = dirname($theme_file);
			$title = $name;
		}

		if ( empty($template) ) {
			if ( file_exists(dirname("$theme_root/$theme_file/content.php")) )
				$template = dirname($theme_file);
			else
				continue;
		}

		$template = trim($template);

		if ( !file_exists("$theme_root/$template/content.php") ) {
			$parent_dir = dirname(dirname($theme_file));
			if ( file_exists("$theme_root/$parent_dir/$template/content.php") ) {
				$template = "$parent_dir/$template";
			} else {
				$lfm_broken_themes[$name] = array('Name' => $name, 'Title' => $title, 'Description' => 'Template is missing.');
				continue;
			}
		}

		$themes[$name] = array('Name' => $name, 'Title' => $title, 'Description' => $description, 'Author' => $author, 'Version' => $version, 'Template' => $template, 'Template Files' => $template_files, 'Screenshot' => $screenshot);
	}

	$lfm_themes = $themes;

	return $themes;
}

function current_theme_info() {
	$themes = get_themes();
	$current_theme = get_current_theme();
	$ct->name = $current_theme;
	$ct->title = $themes[$current_theme]['Title'];
	$ct->version = $themes[$current_theme]['Version'];
	$ct->template = $themes[$current_theme]['Template'];
	$ct->screenshot = $themes[$current_theme]['Screenshot'];
	$ct->description = $themes[$current_theme]['Description'];
	$ct->author = $themes[$current_theme]['Author'];
	return $ct;
}

function get_current_theme() {
   global $theme_dir;

	$themes = get_themes();
	$theme_names = array_keys($themes);
	$current_theme = 'default';

	if ( $themes ) {
		foreach ( (array) $theme_names as $theme_name ) {
			if ( $themes[$theme_name]['Template'] == $theme_dir ) {
				$current_theme = $themes[$theme_name]['Name'];
				break;
			}
		}
	}

	//update_option('current_theme', $current_theme);

	return $current_theme;
}

function switch_theme($template) {
   GLOBAL $prefix;

   mysql_query("UPDATE ".$prefix."settings set theme='$template'") or die("Unable to update theme setting!");
}

?>
