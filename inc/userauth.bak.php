<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.26
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "filter.php";



	@session_start();
	// logout - kill this session
    if(isset($_GET["mf"]))
    {
	    if($_GET["mf"] == "lo")
	    {
		    $_SESSION["uid"] = "";
		    $_SESSION["user_password"] = "";
			if (!isset($_SESSION["adminid"])){
				if (isset($_COOKIE[session_name()])) {
					@setcookie(session_name(), '', time()-42000, '/');
				}
				if (isset($_COOKIE["lfmpr3s5gy"])) {
					@setcookie("lfmpr3s5gy", '', time()-42000, '/');
				}
				@session_destroy();
			}
		}
    }
	include "funcs.php";

	// Get the path to the root of the installation
	$slashpos=strrpos($_SERVER['REQUEST_URI'],"/");
	$memberpath=substr($_SERVER['REQUEST_URI'],0,$slashpos);
	$slashpos=strrpos($memberpath,"/");
	$indexpath=substr($memberpath,0,$slashpos);


	include "config.php";
	include_once "sql_funcs.php";
	
	$_SESSION["prefix"] = $prefix;

	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");

/* Check whether the userid is already set - if not, attempt to authenticate */
	/* In order to be considered authenticated, $uid must be set */

	if(isset($_SESSION["uid"]) && !isset($_POST["login"]))
	{
		 // Check that this is really who it says it is

		 // Sanitize input values
		 $s_login=sanitize_sql($_SESSION["uid"]);
		 $s_password=sanitize_sql($_SESSION["user_password"]);

		 $authquery="SELECT * FROM ".$prefix."members WHERE username='".$s_login."' AND password='".$s_password."'";
		 $authresult=@mysql_query($authquery);
		 $num=@mysql_num_rows($authresult);

		 // Not found - must be wrong/invalid
		 if($num==0)
		 {
			$_SESSION["uid"]="";
			$_SESSION["user_password"]="";
			$_SESSION["userid"]="";
			$authenticated=0;
			if (!isset($_SESSION["adminid"])){
				@session_destroy();
			}
			echo "<script language=\"javascript\">";
			echo "window.location.href=\"login.php?s=noauth\";";
			echo "</script>";
			exit;
		 }
		 else
		 {
		 	$authenticated=1;
		 	$_SESSION["userid"] = mysql_result($authresult, 0, "Id");
		 }
	}
	else
	{
		 // Sanitize input values
		 $s_login=sanitize_sql($_POST["login"]);
		 $s_password=sanitize_sql($_POST["password"]);
       $enc_s_password=md5($s_password);

		 /* Try to authenticate this user against db */
		 $authquery="SELECT * FROM ".$prefix."members WHERE username='".$s_login."' and password='".$enc_s_password."'";
		 $authresult=@mysql_query($authquery);
		 if(!$num=@mysql_num_rows($authresult))
		 {
			// Wrong login or password .. or user doesn't exist
			$authenticated=0;
			$_SESSION["uid"]="";
			$_SESSION["user_password"]="";
			$_SESSION["userid"]="";
			if (!isset($_SESSION["adminid"])){
				@session_destroy();
			}
			echo "<script language=\"javascript\">";
			echo "window.location.href=\"login.php?s=noauth\";";
			echo "</script>";
			exit;
		 }
		 else
		 {
			$authrow=@mysql_fetch_array($authresult);
			// Unverified
			if($authrow["status"] == "Unverified")
			{
				if (!isset($_SESSION["adminid"])){
					@session_destroy();
				}
				echo "<script language=\"javascript\">";
				echo "window.location.href=\"thank_you.php\";";
				echo "</script>";
				exit;
			}
			
			// Check Banned IP And Email
			if (bannedIp(getip()) !== false) { echo("Your IP Is Banned"); exit; }
			if (bannedEmail($authrow["email"]) !== false) { echo("Your Email Is Banned"); exit; }
			
			// Authenticated
			$logintime=date("Y-m-d H:i:s");
			$ip=getip();
			if (!isset($_SESSION["adminid"])){
				@mysql_query("UPDATE ".$prefix."members SET lastip='$ip', lastlogin='$logintime', numLogins=numLogins+1 WHERE Id=".$authrow["Id"]) or die(mysql_error());
			}
			$_SESSION["uid"] = $authrow["username"];
			$_SESSION["user_password"]=$authrow["password"];
			$_SESSION["userid"]=$authrow["Id"];
			setcookie("lfmpr3s5gy", session_name(), time()+3600);
			$authenticated=1;
		 }
	}

?>