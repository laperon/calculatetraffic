<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.34
// Copyright �2015 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "filter.php";

	@session_start();

	// logout - kill this session
    if(isset($_GET["mf"]))
    {
	    if($_GET["mf"] == "lo")
	    {
		    $_SESSION["uid"] = "";
		    $_SESSION["user_password"] = "";
			if (!isset($_SESSION["adminid"])){
				if (isset($_COOKIE[session_name()])) {
					@setcookie(session_name(), '', time()-42000, '/');
				}
				if (isset($_COOKIE["lfmpr3s5gy"])) {
					@setcookie("lfmpr3s5gy", '', time()-42000, '/');
				}
				@session_destroy();
			}
		}
    }
	include_once "funcs.php";
	include_once "lfm_password.php";

	// Get the path to the root of the installation
	$slashpos=strrpos($_SERVER['REQUEST_URI'],"/");
	$memberpath=substr($_SERVER['REQUEST_URI'],0,$slashpos);
	$slashpos=strrpos($memberpath,"/");
	$indexpath=substr($memberpath,0,$slashpos);


	include "config.php";
	include_once "sql_funcs.php";
	
	$_SESSION["prefix"] = $prefix;

	@lfmsql_connect($dbhost,$dbuser,$dbpass);
	@lfmsql_select_db($dbname) or die( "Unable to select database");

/* Check whether the userid is already set - if not, attempt to authenticate */
	/* In order to be considered authenticated, $uid must be set */
	if (isset($_SESSION["uid"]) && strlen($_SESSION["uid"]) > 0 && isset($_SESSION["user_password"]) && strlen($_SESSION["user_password"]) > 0 && isset($_SESSION["userid"]) && is_numeric($_SESSION["userid"]) && $_SESSION["userid"] > 0 && !isset($_POST["login"])) {
		 // Check that this is really who it says it is

		 // Sanitize input values
		 $s_login=sanitize_sql($_SESSION["uid"]);
		 $s_password=sanitize_sql($_SESSION["user_password"]);

		 $authquery="SELECT Id FROM ".$prefix."members WHERE username='".$s_login."' AND password='".$s_password."'";
		 $authresult=@lfmsql_query($authquery);
		 $num=@lfmsql_num_rows($authresult);
		 
		 if($num <= 0) {
		 	// Not found - must be wrong/invalid
			$_SESSION["uid"]="";
			$_SESSION["user_password"]="";
			$_SESSION["userid"]="";
			$authenticated=0;
			if (!isset($_SESSION["adminid"])){
				@session_destroy();
			}
			echo "<script language=\"javascript\">";
			echo "window.location.href=\"login.php?s=noauth\";";
			echo "</script>";
			exit;
		 } else {
		 	$authenticated=1;
		 	$_SESSION["userid"] = lfmsql_result($authresult, 0, "Id");
		 }
		 
	} elseif (isset($_POST["login"]) && strlen($_POST["login"]) > 0 && isset($_POST["password"]) && strlen($_POST["password"]) > 0) {

        // Check Banned IP
		if (bannedIp(getip()) !== false) { echo("Your IP Is Banned"); exit; }
		 
		 // Sanitize input values
		 $s_login=sanitize_sql($_POST["login"]);
		 $s_password=sanitize_sql($_POST["password"]);
		 
		$max_fail_name = lfmsql_result(lfmsql_query("SELECT max_fail_name FROM `".$prefix."settings`"), 0);
		$max_fail_ip = lfmsql_result(lfmsql_query("SELECT max_fail_ip FROM `".$prefix."settings`"), 0);
		$tenminago = time() - 600;
		
		// IP Address Brute Force Protection
		if (isset($max_fail_ip) && is_numeric($max_fail_ip) && $max_fail_ip > 0) {
			$countfailip = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."login_log` WHERE logtime >= ".$tenminago." AND ipaddress='".$_SERVER['REMOTE_ADDR']."' AND invalidlogin=1"), 0);
			if ($countfailip >= $max_fail_ip) {
				echo("As a security precaution, you have been temporary blocked due to too many invalid login attempts.  This block will be automatically lifted within the next hour.");
				exit;
			}
		}

		 /* Try to authenticate this user against db */
		 $authquery="SELECT Id, username, email, password, status FROM ".$prefix."members WHERE username='".$s_login."'";
		 $authresult=@lfmsql_query($authquery);
		 $num=@lfmsql_num_rows($authresult);
		 if($num <= 0) {
			// Username doesn't exist
			@lfmsql_query("INSERT INTO `".$prefix."login_log` (logtime, userid, ipaddress, invalidlogin) VALUES ('".time()."', -1, '".$_SERVER['REMOTE_ADDR']."', 1)");
			$authenticated=0;
			$_SESSION["uid"]="";
			$_SESSION["user_password"]="";
			$_SESSION["userid"]="";
			if (!isset($_SESSION["adminid"])){
				@session_destroy();
			}
			echo "<script language=\"javascript\">";
			echo "window.location.href=\"login.php?s=noauth\";";
			echo "</script>";
			exit;
		 } else	{
			$authrow=@lfmsql_fetch_array($authresult);
			
			// Validate Password
			$loginuserid = $authrow["Id"];
			$passwordhash = $authrow["password"];
			
			$verifiedip = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."login_log` WHERE userid=".$loginuserid." AND ipaddress='".$_SERVER['REMOTE_ADDR']."' AND invalidlogin=0"), 0);
			
			if ($verifiedip < 1) {
				// Username Brute Force Protection
				if (isset($max_fail_name) && is_numeric($max_fail_name) && $max_fail_name > 0) {
					$countfaillogins = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."login_log` WHERE logtime >= ".$tenminago." AND userid=".$loginuserid." AND invalidlogin=1"), 0);
					if ($countfaillogins >= $max_fail_name) {
						echo("As a security precaution, you have been temporary blocked due to too many invalid login attempts.  This block will be automatically lifted within the next hour.");
						exit;
					}
				}
			}
			
			if (strlen($passwordhash) == 32) {
				// Check Using Old MD5 Format
				if ($passwordhash == md5($s_password)) {
					// Correct Password
					@lfmsql_query("INSERT INTO `".$prefix."login_log` (logtime, userid, ipaddress, invalidlogin) VALUES ('".time()."', ".$loginuserid.", '".$_SERVER['REMOTE_ADDR']."', 0)");
					$passwordhash = lfmMakePassword($s_password);
					lfmsql_query("UPDATE ".$prefix."members SET password='".$passwordhash."' WHERE Id='".$loginuserid."'") or die(lfmsql_error());
				} else {
					// Wrong Password
					@lfmsql_query("INSERT INTO `".$prefix."login_log` (logtime, userid, ipaddress, invalidlogin) VALUES ('".time()."', ".$loginuserid.", '".$_SERVER['REMOTE_ADDR']."', 1)");
					$authenticated=0;
					$_SESSION["uid"]="";
					$_SESSION["user_password"]="";
					$_SESSION["userid"]="";
					if (!isset($_SESSION["adminid"])){
						@session_destroy();
					}
					echo "<script language=\"javascript\">";
					echo "window.location.href=\"login.php?s=noauth\";";
					echo "</script>";
					exit;
				}
			} else {
				// Check Using New Format
				if (lfmCheckPassword($s_password, $passwordhash)) {
					// Correct Password
					@lfmsql_query("INSERT INTO `".$prefix."login_log` (logtime, userid, ipaddress, invalidlogin) VALUES ('".time()."', ".$loginuserid.", '".$_SERVER['REMOTE_ADDR']."', 0)");
				} else {
					// Wrong Password
					@lfmsql_query("INSERT INTO `".$prefix."login_log` (logtime, userid, ipaddress, invalidlogin) VALUES ('".time()."', ".$loginuserid.", '".$_SERVER['REMOTE_ADDR']."', 1)");
					$authenticated=0;
					$_SESSION["uid"]="";
					$_SESSION["user_password"]="";
					$_SESSION["userid"]="";
					if (!isset($_SESSION["adminid"])){
						@session_destroy();
					}
					echo "<script language=\"javascript\">";
					echo "window.location.href=\"login.php?s=noauth\";";
					echo "</script>";
					exit;
				}
			}
			
			// Delete Old Log Entries
			$monthago = time() - 2678400;
			@lfmsql_query("DELETE FROM `".$prefix."login_log` WHERE logtime < ".$monthago);
			
			// Unverified
			if($authrow["status"] == "Unverified")
			{
				if (!isset($_SESSION["adminid"])){
					@session_destroy();
				}
				echo "<script language=\"javascript\">";
				echo "window.location.href=\"thank_you.php\";";
				echo "</script>";
				exit;
			}
			
			// Check Banned Email
			if (bannedEmail($authrow["email"]) !== false) { echo("Your Email Is Banned"); exit; }
			
			// Authenticated
			$logintime=date("Y-m-d H:i:s");
			$ip=getip();
			if (!isset($_SESSION["adminid"])){
				@lfmsql_query("UPDATE ".$prefix."members SET lastip='$ip', lastlogin='$logintime', numLogins=numLogins+1 WHERE Id=".$authrow["Id"]) or die(lfmsql_error());
			}
			$_SESSION["uid"] = $authrow["username"];
			$_SESSION["user_password"] = $passwordhash;
			$_SESSION["userid"] = $authrow["Id"];
			setcookie("lfmpr3s5gy", session_name(), time()+3600);
			$authenticated=1;
		 }
		 
	} else {
		
		// Not Logged In
		$authenticated=0;
		$_SESSION["uid"]="";
		$_SESSION["user_password"]="";
		$_SESSION["userid"]="";
		if (!isset($_SESSION["adminid"])){
			@session_destroy();
		}
		echo "<script language=\"javascript\">";
		echo "window.location.href=\"/\";";
		echo "</script>";
		exit;
		
	}

?>