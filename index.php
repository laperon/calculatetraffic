<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.21
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
require_once "inc/lfmsql.php";
include "inc/config.php";

session_start();
	
	@lfmsql_connect($dbhost,$dbuser,$dbpass);
	@lfmsql_select_db($dbname) or die( "Unable to select database");
	
include "inc/funcs.php";
include "inc/theme.php";

// Query settings table
$res=@lfmsql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
$row=@lfmsql_fetch_array($res);
$sitename=$row["sitename"];
$affurl=$row["affurl"];
$bannerurls=$row["bannerurls"];
$flow=$row["flow"];
$indextheme=$row["indextheme"];
	
	$sres=@lfmsql_query("SELECT sitename,meta_description,meta_keywords,firstcookie,splittest FROM ".$prefix."settings");
	$srow=@lfmsql_fetch_array($sres);
	
	if($srow["firstcookie"] == 0)
	{
		// rid request variable will override cookie
		if(isset($_GET["rid"]))
		{
			$rid=$_GET["rid"];
			setcookie("rid", $_GET["rid"],time()+2592000);
		}
	}
	else
	if(!isset($_COOKIE["rid"]))
	{
		setcookie("rid", $_GET["rid"],time()+2592000);
		$rid=$_GET["rid"];
	}
	else
	{
		$rid=$_COOKIE["rid"];
	}

	/* begin hitsconnect tracking code mod*/
   if(isset($_GET['jhcv']) && is_numeric($_GET['jhcv'])) {
 	   setcookie("jhcvc",$_GET['jhcv'],time()+86400);
	   $_SESSION['jhcvs']=$_GET['jhcv'];
   }   
   /* end hitsconnect tracking code mod */
  
	// Start Auto Conversion Tracking
  if (isset($_GET['srtrkck']) && $_GET['srtrkck'] == "1") {
  	echo("<!--srtrkvalid-->");
  }
  if (isset($_GET['srtrkdm']) && strlen($_GET['srtrkdm'])>3 && isset($_GET['srtrkid']) && is_numeric($_GET['srtrkid']) && $_GET['srtrkid']>0) {
  	@lfmsql_query("INSERT INTO `".$prefix."autotrack` (timehit, ipaddress, srtrkdm, srtrkid) VALUES ('".time()."', '".$_SERVER['REMOTE_ADDR']."', '".$_GET['srtrkdm']."', '".$_GET['srtrkid']."')");
  }
  // End Auto Conversion Tracking
  
	// Get some settings
	$sitename=$srow["sitename"];
	$meta_description=$srow["meta_description"];
	$meta_keywords=$srow["meta_keywords"];
	$splittest=$srow["splittest"];
		
	if($splittest == 1)
	{
		// Read the order template data from the settings table
		$tres=@lfmsql_query("SELECT * FROM ".$prefix."templates WHERE template_name='Sales Page'");
		$trow=@lfmsql_fetch_array($tres);
		$tname="Sales Page";
        $trowname=$trow["template_name"]; 
	}
	else
	{
		// Split testing 2 sales pages
		if($splittest == 2)
		{
			// Check if we have a page available
			$tchk=@lfmsql_query("SELECT COUNT(*) FROM ".$prefix."templates WHERE (template_name = 'Sales Page' OR template_name = 'Sales Page 2') AND sflag=0");
			$tchkres=@lfmsql_result($tchk,0);
			// None left ... need to reset
			if($tchkres == 0)
			{
				lfmsql_query("UPDATE ".$prefix."templates SET sflag=0 WHERE (template_name = 'Sales Page' OR template_name = 'Sales Page 2')"); 
			}
			
			$tres=@lfmsql_query("SELECT * FROM ".$prefix."templates WHERE (template_name = 'Sales Page' OR template_name = 'Sales Page 2') AND sflag=0 LIMIT 1");
			$trow=@lfmsql_fetch_array($tres);
			$trowid=$trow["template_id"];
			$trowname=$trow["template_name"];
			@lfmsql_query("UPDATE ".$prefix."templates SET sflag=1 WHERE template_id=$trowid");
			setcookie("sp", $trowname, time()+3600);
		}
		
		// Split testing 3 sales pages
		if($splittest == 3)
		{
			// Check if we have a page available
			$tchk=@lfmsql_query("SELECT COUNT(*) FROM ".$prefix."templates WHERE template_name LIKE 'Sales Page%' AND sflag=0");
			$tchkres=@lfmsql_result($tchk,0);
			// None left ... need to reset
			if($tchkres == 0)
			{
				lfmsql_query("UPDATE ".$prefix."templates SET sflag=0 WHERE template_name LIKE 'Sales Page%'"); 
			}
			
			$tres=@lfmsql_query("SELECT * FROM ".$prefix."templates WHERE template_name LIKE 'Sales Page%' AND sflag=0 LIMIT 1");
			$trow=@lfmsql_fetch_array($tres);
			$trowid=$trow["template_id"];
			$trowname=$trow["template_name"];
			lfmsql_query("UPDATE ".$prefix."templates SET sflag=1 WHERE template_id=$trowid");
			setcookie("sp", $trowname, time()+3600);
		}		
	}

	$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	$root_path=substr($fullurl,0,strrpos($fullurl, "/"));

	// Add hit stat for current sales page
	if($splittest > 1) {
		@lfmsql_query("INSERT INTO ".$prefix."splithits(remotehost,salespage) VALUES ('".$_SERVER['REMOTE_ADDR']."','$trowname')");
	}

// Display Sales template
if (function_exists('html_entity_decode'))
{
	$salespage=html_entity_decode($trow["template_data"]);
}
else
{
	$salespage=unhtmlentities($trow["template_data"]);
}

// Set the signup link to Clickbank or LFM signup
if(!isset($_REQUEST["hop"]))
{
	$signuplink="<center><a href=\"signup.php?rid=$rid\"><img src=\"images/signup.jpg\" border=\"0\"></a></center>";
	$salespage=str_replace("#SIGNUPLINK#",$signuplink,$salespage);
}

$salespage=str_replace("#AFFILIATEID#",$rid,$salespage);

// Check for promo code
if(strstr($salespage,"#PROMOCODE#"))
{

	$promocode="<form name=\"promo\" method=\"POST\" action=\"signup.php?rid=".$rid."\"><strong>CODE:</strong><input name=\"pc\" type=\"text\" size=\"4\"><input type=image name=\"Promo\" src=\"images/promo.jpg\" border=\"0\"></form>";
	$salespage=str_replace("#PROMOCODE#",$promocode,$salespage);
}

// Check for promo code
if(strstr($salespage,"#PROMOCODE_2#"))
{

$promocode="<form name=promo method=POST action=\"signup.php?rid=".$rid."\">
<table border=\"0\" align=\"center\"><tr><td align=center><input type=image name=Promo src=images/promo.jpg border=0></td></tr>
<tr><td align=center><strong>CODE:</strong><input name=pc type=text size=4></td></tr>
</table>
</form>";

	$salespage=str_replace("#PROMOCODE_2#",$promocode,$salespage);
}

$salespage=translate_site_tags($salespage);

// Ref's User Macros
if (isset($rid) && is_numeric($rid)) {
	$checkexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."members WHERE Id='".$rid."' AND status='Active'"), 0);
	if ($checkexists > 0) {
		$ridsearch = " WHERE Id='".$rid."'";
	} else {
		$ridsearch = "";
	}
} else {
	$ridsearch = "";
}
$getusermail = lfmsql_query("SELECT email FROM ".$prefix."members".$ridsearch." ORDER BY Id LIMIT 1");
if (lfmsql_num_rows($getusermail) > 0) {
	$useremail = lfmsql_result($getusermail, 0, "email");
	$salespage = translate_user_tags($salespage, $useremail);
}
// End Ref's User Macros

$salespage .= "<br><br><center><a target=_blank href=\"http://thetrafficexchangescript.com\"><font size=1 color=blue>Powered by LFMTE</font></a></center><br>";

$page_content = $salespage;

if ($indextheme > 0) {
	load_template ($theme_dir."/header.php");
	load_template ($theme_dir."/content_homepage.php");
	load_template ($theme_dir."/footer.php");
} else {
	echo $page_content;
}

?>
