<?php

// LFM, LFMTE, and LFMVM CK Editor integration

require_once "inc/filter.php";
require_once "inc/funcs.php";
include "inc/config.php";

$mconn=@lfmsql_connect($dbhost,$dbuser,$dbpass);
@lfmsql_select_db($dbname,$mconn) or die( "Unable to select database");

echo("<html><body><p><center><b>Starting Installation...</b></p>");

// Update Database
@lfmsql_query("ALTER TABLE `".$prefix."settings` ADD `enablewysiwyg` INT( 11 ) NOT NULL DEFAULT '0';");


// Update Content Delivery

if (file_exists("admin/contentdelivery.php")) {

$fileconnect = fopen("admin/contentdelivery.php",'r');
$oldfile = fread($fileconnect, 1000000);
fclose($fileconnect);

$checkexisting = strpos($oldfile, "seteditors");
if ($checkexisting === false) {

	$makebackup = fopen("admin/contentdelivery.bak.php",'w');
	fwrite($makebackup, $oldfile);
	fclose($makebackup);
	
	if (file_exists("admin/contentdelivery.bak.php")) {
		
		// Update 1
		$newcode = '<link href="styles.css" rel="stylesheet" type="text/css" />

<?
// Get/Set Editors Settings

$enablewysiwyg = lfmsql_result(lfmsql_query("Select enablewysiwyg from `".$prefix."settings`"), 0);
if (isset($_GET[\'seteditors\']) && ($_GET[\'seteditors\'] == 0 || $_GET[\'seteditors\'] == 1)) {
	$enablewysiwyg = $_GET[\'seteditors\'];
	@lfmsql_query("Update `".$prefix."settings` set enablewysiwyg=".$enablewysiwyg);
}
//End Get/Set Editors Settings

?>
<? if($enablewysiwyg == "1") { ?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<center><p><font size="4"><a href="admin.php?f=cd&seteditors=0">Click To Hide The HTML Editor</a></font></p></center>
<? } else { ?>
<center><p><font size="4"><a href="admin.php?f=cd&seteditors=1">Click To Show The HTML Editor</a></font></p></center>
<? } ?>';
		
		$replacetext = '<link href="styles.css" rel="stylesheet" type="text/css" />';
		
		$newfile = str_replace($replacetext, $newcode, $oldfile);
		
		// Write Updates
		$updatefile = fopen("admin/contentdelivery.php",'w');
		fwrite($updatefile, $newfile);
		fclose($updatefile);
		
	}
}

}

// End Update Content Delivery

echo("<br><br><b>Installation Complete.</b></center></body></html>");

exit;
?>