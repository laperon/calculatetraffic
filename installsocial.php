<?php

// LFMTE Social Surfbar
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
include "inc/config.php";
require_once "inc/funcs.php";
require_once "inc/sql_funcs.php";

$mconn=@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname,$mconn) or die( "Unable to select database");

echo("<html><body><p><center><b>Starting Installation...</b></p>");

$checkver = mysql_result(mysql_query("SELECT ver FROM ".$prefix."settings"), 0);
if ($checkver < "2.27") {
	echo("<br><br><font size=\"2\">You are running an old version of the LFMTE. <a target=\"_blank\" href=\"http://thetrafficexchangescript.com/updatecheck.php?ver=".$checkver."\"><b>Click Here To Update</b></a> then please run this installer again.</font><br><br>");
	exit;
}

if(!file_exists("installsocialfiles.php")) {
	echo("<br><br>The mod is either already installed, or installation files are missing.<br><br>");
	exit;
}

if(file_exists("installsocialdb.php")) {
	include("installsocialdb.php");
	@unlink("installsocialdb.php");
}

include("installsocialfiles.php");

echo("<br><br>Removing temporary installation files...<br><br>");

@unlink("installsocialfiles.php");

@unlink("surfbar_data.txt");
@unlink("surfbarfooter_data.txt");

echo("<br><br><b>Installation Complete.</b>
<br>
<p>To customize your Social Surfbar mod, click on the \"Social Surfbar\" link under the \"Surf Options\" menu in your Admin area.</p>
<p>Your members can now enter their social details by clicking on the Social Branding link on the members menu.</p>
</center></body></html>");

exit;
?>