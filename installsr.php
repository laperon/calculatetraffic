<?php

////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

require_once "inc/filter.php";
include "inc/config.php";
require_once "inc/funcs.php";
require_once "inc/sql_funcs.php";

$mconn=@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname,$mconn) or die( "Unable to select database");

echo("<html><body><p><center><b>Starting Installation...</b></p>");

$checkver = mysql_result(mysql_query("SELECT ver FROM ".$prefix."settings"), 0);
if ($checkver < "2.09") {
	echo("<br><br><font size=\"2\">You are running an old version of the LFMTE. <a target=\"_blank\" href=\"http://thetrafficexchangescript.com/updatecheck.php?ver=".$checkver."\"><b>Click Here To Update</b></a> then please run this installer again.</font><br><br>");
	exit;
}

if(!file_exists("installsrdatabase.php")) {
echo("<br><br>The mod is either already installed, or installation files are missing.<br><br>");
exit;
}

if(file_exists("installsrdatabase.php")) {
include("installsrdatabase.php");
unlink("installsrdatabase.php");
}

if(file_exists("installsrfiles.php")) {
include("installsrfiles.php");
unlink("installsrfiles.php");
}

echo("<br><br><b>Installation Complete.</b>
<br>
<p>To access your Active Surfer Rewards mod, click on the \"Surfer Rewards\" link under the \"Surf Options\" menu in your Admin area.</p>
</center></body></html>");

exit;
?>