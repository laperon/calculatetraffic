<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

//Connect To LFM Database
require_once "inc/filter.php";
include "inc/config.php";
include "inc/funcs.php";
@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die( "Unable to select database");

$getsiteinfo = mysql_query("Select sitename, affurl from `".$prefix."settings` where id=1 limit 1");
if (mysql_num_rows($getsiteinfo) > 0) {
$sitename = mysql_result($getsiteinfo, 0, "sitename");
$siteurl = mysql_result($getsiteinfo, 0, "affurl");
} else {
$sitename = "Traffic Exchange";
$siteurl = "http://thetrafficexchangescript.com";
}

$getadmininfo = mysql_query("Select email from `".$prefix."admin` where id=1 limit 1");
if (mysql_num_rows($getadmininfo) > 0) {
$adminemail = mysql_result($getadmininfo, 0, "email");
} else {
$adminemail = "lfmteipn@trafficmods.com";
}

if ($siteurl != "") {
$spliturl = explode("/", $siteurl);
$sitedomain = $spliturl[2];
$sitedomain = str_replace("www.", "", $sitedomain);
} else {
$sitedomain = "thetrafficexchangescript.com";
}

//End Connect to LFM Database

define('SITE_NAME',$sitename);
define('VALID_DOMAIN',$sitedomain);
define('CONTACT_EMAIL',$adminemail);

extract($_POST);

$variables="POST\n";
foreach ($_POST as $key=>$value) {
	$variables.=$key.": " . $value . "\n";
	}
$variables.="\nGET\n";
foreach ($_GET as $key=>$value) {
	$variables.=$key.": " . $value . "\n";
	}

$variables.="root: $root\nserver time: ".date("Y:m:d H:i:s")."\n\n$_SERVER[REMOTE_ADDR]";
$headers='From: "'.SITE_NAME.'" <ipn@'.VALID_DOMAIN.'>';

//mail (CONTACT_EMAIL,'IPN Page Requested - '.SITE_NAME,$variables,$headers);

include("ipn/code.php");

?>