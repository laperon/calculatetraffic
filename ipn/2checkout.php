<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

// 2checkout.php

if ( !defined('SITE_NAME') ) { header("Location:../"); exit; }

if($demo=="Y") { $test_mode=1; $order_number=1; }

$r = mysql_query("SELECT payee,verify FROM ".$prefix."ipn_merchants WHERE name='2CheckOut';");
$id = mysql_result($r,0,"payee");
$hash = mysql_result($r,0,"verify").$id.$order_number.$total;
$md5_hash=md5($hash);
$key1=trim($_POST['key']);
if(!$key1) { header("Location:../"); }
$key2=strtoupper($md5_hash);
$status=$credit_card_processed;
if($status=="Y") { $status="Approved"; } elseif($status=="K") { $status="Pending"; }

if($key1==$key2) {
	if ($sid == $id) {
		$new = array();
		$new['processor']='2checkout';
		if(!$date) { $date=date("r"); }
		$new['date']=$date;
		$new['txn_type']=$pay_method;
		$new['txn_id']=$order_number;
		$new['item_number']=$product_id;
		$new['item_name']=$description;
		$new['name']=$card_holder_name;
		$new['email']=$email;
		$new['amount']=$total;
		$new['fee']=5.5*($new['amount']/100)+0.45;
		$new['user_id']=$user_id;
		if ($status=="Approved") {
			$new['payment_status']='OK';
			} else {
			$new['payment_status']=$status;
			}	
		processOrder ($type,$new);
		} else {
		$err = '2CheckOut - Invalid Seller ID';
		processError ($err,$_POST);
		}	
	} else {
	$err = '2CheckOut - Invalid Security Code';
	processError ($err,$_POST);
	}

?>