<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

if ( !defined('SITE_NAME') ) { header("Location:../"); exit; }

$r = mysql_query("SELECT payee,verify FROM ".$prefix."ipn_merchants WHERE name='Payza';");
$payee = mysql_result($r,0,"payee");
$md5_hash = mysql_result($r,0,"verify");

@header("Status: 200 OK");

if($md5_hash==""||$md5_hash==$ap_securitycode) {
	if ($ap_merchant == $payee) {
		$new = array();
		$new['processor']='payza';
		$new['date']=date("r");
		$new['amount']=$ap_totalamount;
		$new['txn_type']=$ap_purchasetype;
		if($ap_purchasetype=='subscription') {
			// is it a new subscription?
			$res = mysql_query("SELECT id FROM ".$prefix."ipn_transactions WHERE user_id='$apc_1' AND item_number='$apc_2'");
			if( mysql_num_rows($res)==0 && $ap_trialperiodlength>0 ) { $new['amount']=$ap_trialamount; }
			$new['txn_id']=$ap_subscriptionreferencenumber.'-'.$ap_nextrundate;
			} else {
			$new['txn_id']=$ap_referencenumber;
			}
		$new['item_name']=$ap_itemname;
		$new['item_number']=$apc_2;
		$new['name']=$ap_custfirstname." ".$ap_custlastname;
		$new['email']=$ap_custemailaddress;
		$new['fee']=2.5*($new['amount']/100)+0.25;
		$new['user_id']=$apc_1;
		if ( $ap_status=='Success' && $ap_purchasetype<>'subscription' ) {
			$new['payment_status']='OK';
			} elseif ( $ap_status=='Subscription-Payment-Success' ) {
			$new['payment_status']='OK';
			} else {
			$new['payment_status']=$ap_status;
			}
		if($ap_test) {
			$test_mode=$ap_test;
			$new['txn_id']="TEST";
			}
		processOrder ($ap_purchasetype,$new);
		} else {
		$err = 'Payza - Invalid Receiver Email';
		processError ($err,$_POST);
		}	
	} else {
	$err = 'Payza - Invalid Security Code';
	processError ($err,$_POST);
	}	

?>