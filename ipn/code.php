<?php
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2014 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

if ( !defined('SITE_NAME') ) { header("Location:../"); exit; }

if (isset($verify_sign)) {
	include("ipn/paypal.php");
} elseif (isset($product_id)) {
	include("ipn/2checkout.php");
	if(!$test_mode) { header("Location:http://".VALID_DOMAIN."/members.php"); }
	} elseif (isset($_ipn_act)) {
	include("ipn/safepay.php");
	} elseif (isset($ap_status)) {
	include("ipn/alertpay.php");
	} else {
	if (!empty($_POST)) { processError ('Unidentified IPN request:',$_POST); }
	header("Location:http://".VALID_DOMAIN);
	}

exit();

// the functions
function processOrder ($type,$data) {
	if (file_exists("inc/config.php")) {
		include "inc/config.php";
	} else {
		processError ('Could not find config.php file',$data);
	}

	global $test_mode,$headers;
	$res=lfmsql_query("SELECT field,value FROM ".$prefix."ipn_settings WHERE id>0 LIMIT 3;"); // notify and jv comm variables
	while($row=lfmsql_fetch_array($res)) { ${$row['field']}=$row['value']; }
	
	//Get admin email
	$getreplyinfo = lfmsql_query("Select replyaddress from `".$prefix."settings` where id=1 limit 1");
	if (lfmsql_num_rows($getreplyinfo) > 0) {
		$ademail = lfmsql_result($getreplyinfo, 0, "replyaddress");
	} else {
		$ademail = "noreply@noreplydomain.com";
	}
	
	$res=lfmsql_query("SELECT field,value FROM ".$prefix."ipn_vals WHERE id>4 LIMIT 6;");
	while($row=lfmsql_fetch_array($res)) { ${$row['field']}=$row['value']; }
	$check=lfmsql_query('SELECT fee FROM '.$prefix.'ipn_transactions WHERE txn_id="'.$data['txn_id'].'" LIMIT 1');
	if (lfmsql_num_rows($check)) {
		$fee=lfmsql_result($check,0);
		if ($fee<>$data['fee']) {
			$query='UPDATE '.$prefix.'ipn_transactions SET fee="'.$data['fee'].'" WHERE txn_id="'.$data['txn_id'].'" LIMIT 1';
			mail (CONTACT_EMAIL,'IPN Completed - '.SITE_NAME,"The following transaction ID: $data[txn_id]\nhas now Completed.\n\nThe Pending fee was $fee\nThe Completed fee is $data[fee]\n\n$query\n\n$REMOTE_ADDR",$headers);
			lfmsql_query($query);
			} else {
			processError('Duplicate transaction ID',$data);
			}
		} else {
		$sql="INSERT INTO ".$prefix."ipn_transactions ( processor, date, txn_id, txn_type, item_name, item_number, name, email, amount, fee, user_id, added ) VALUES ( '$data[processor]', '$data[date]', '$data[txn_id]', '$data[txn_type]', '$data[item_name]', '$data[item_number]', '$data[name]', '$data[email]', $data[amount], $data[fee], '$data[user_id]', NOW() )";
		if ($data['payment_status']=='OK') {
			if (!$test_mode) {
				// ADD TO TRANSACTIONS DATABASE TABLE
				lfmsql_query($sql);
				$transaction_id=lfmsql_insert_id();
				} else {
				// TESTING... OUTPUT TO SCREEN
				echo $sql.'<br>';
				$transaction_id='[test mode]';
				}	
			// FIND ITEM IN ipn_products TABLE
			if ($data[processor] == "2checkout") {
			$cnx=lfmsql_query("SELECT amount, credits, bimps, limps, productid, subscription, upgrade, period, type, return_url, email_subject, email_body FROM ".$prefix."ipn_products WHERE twocoid=$data[item_number] AND amount=$data[amount];");
			if (lfmsql_num_rows($cnx) == 0) {
			$cnx=lfmsql_query("SELECT amount, credits, bimps, limps, productid, subscription, upgrade, period, type, return_url, email_subject, email_body FROM ".$prefix."ipn_products WHERE id=$data[item_number] AND amount=$data[amount];");
			}
			} else {
			$cnx=lfmsql_query("SELECT amount, credits, bimps, limps, productid, subscription, upgrade, period, type, return_url, email_subject, email_body FROM ".$prefix."ipn_products WHERE id=$data[item_number] AND amount=$data[amount];");
			}
			if (!lfmsql_num_rows($cnx)) {
				$cnx=lfmsql_query("SELECT amount, credits, bimps, limps, productid, subscription, upgrade, period, type, trial_period, trial_type, return_url, email_subject, email_body FROM ".$prefix."ipn_products WHERE id=$data[item_number] AND trial_amount=$data[amount] AND trial_type!='N';");
				$freetrial = "yes";
				}
			if ($row=@lfmsql_fetch_assoc($cnx)) {
				// UPDATE USER
				$usr_cnx=@lfmsql_query("SELECT username, email, upgend, lastotoid, refid FROM ".$prefix."members WHERE Id=$data[user_id]");
				$usr=lfmsql_fetch_assoc($usr_cnx);
				

//Start OTO Split Testing
if (($row['subscription'] == 2) && ($usr['lastotoid'] != '0/0/0')) {

$splitoto = explode("/", $usr['lastotoid']);
$groupoto = $splitoto[0];
$offeroto = $splitoto[1];
$origgroup = $splitoto[2];

@lfmsql_query("insert into ".$prefix."oto_stats (userid, groupid) values ($data[user_id], $origgroup)");
@lfmsql_query("Update `".$prefix."oto_offers` set timesbought=timesbought+1, sales=sales+$data[amount] where groupid=$groupoto and rank=$offeroto limit 1");
@lfmsql_query("Update `".$prefix."oto_groups` set groupbought=groupbought+1, groupsales=groupsales+$data[amount] where id=$groupoto limit 1");
@lfmsql_query("Update `".$prefix."members` set lastotoid='0/0/0' where Id=$data[user_id] limit 1");
$usr['lastotoid'] = '0/0/0';

}
//End OTO Split Testing
				
				$username=$usr[username];

				$msg_a="Successful {$data['processor']} transaction.\nItem # {$data['item_number']} : {$data['item_name']}\nChanges made to User # {$data['user_id']}:\n\n";
				$msg='';
				
					
	//Credit Boost
	$getacctype = lfmsql_query("Select mtype from ".$prefix."members where Id=$data[user_id] limit 1");
	$acctype = lfmsql_result($getacctype, 0, "mtype");
	
	$t = time();
	$getboost = lfmsql_query("SELECT buyboost FROM ".$prefix."cboost WHERE `starttime`<$t AND `endtime`>$t AND buyboost>1 AND (acctype=$acctype OR acctype=0) LIMIT 1;");
	
	if ((lfmsql_num_rows($getboost) > 0) && ($row['subscription'] == 0)) {
		$buyboost = lfmsql_result($getboost, 0, "buyboost");
		$row['credits'] = $row['credits']*$buyboost;
	}
	//End Credit Boost

				
				if ($row['credits']>0) {
					lfmsql_query("UPDATE ".$prefix."members SET credits=credits+$row[credits] WHERE Id=$data[user_id] LIMIT 1");
					$msg.="$row[credits] credits added\n";
				}
				
				if ($row['bimps']>0) {
					lfmsql_query("UPDATE ".$prefix."members SET bannerimps=bannerimps+$row[bimps] WHERE Id=$data[user_id] LIMIT 1");
					$msg.="$row[bimps] banner impressions added\n";
				}
				
				if ($row['limps']>0) {
					lfmsql_query("UPDATE ".$prefix."members SET textimps=textimps+$row[limps] WHERE Id=$data[user_id] LIMIT 1");
					$msg.="$row[limps] text impressions added\n";
				}
				
				// LFMTE v2.31 Update
				if (strlen($row['productid']) > 0) {
					$getprodlist = lfmsql_query("SELECT productid, itemid, productname FROM ".$prefix."products ORDER BY productid");
					while($proddata = @lfmsql_fetch_object($getprodlist)) {
						if (array_search($proddata->productid, explode(",", $row['productid'])) !==false) {
							$checkexisting = lfmsql_query("Select * from ".$prefix."purchases where affid=".$data["user_id"]." and itemid=".$proddata->productid." limit 1");
							if (lfmsql_num_rows($checkexisting) == 0) {
								lfmsql_query("INSERT INTO ".$prefix."purchases(affid,itemid,txn_id) VALUES(".$data["user_id"].",'".$proddata->productid."','".$data["txn_id"]."')");
								$msg .= $proddata->productname." can be downloaded in the Members Area\n";
							}
						}
					}
				}
				// End LFMTE v2.31 Update

				if ($row['upgrade'] > 0) {
					$today=date("Y-m-d");
					$exp=$usr['upgend'];
					$period=$row['period'];
					$type=$row['type'];
					$unit="day";
					
					if($type=="N") {
					//Lifetime Upgrade
					$upg_query = 'update '.$prefix.'members set mtype='.$row['upgrade'].', upgend="0000-00-00" where Id='.$data['user_id'].' limit 1';
					lfmsql_query($upg_query);		
					$res=lfmsql_query('select accname from '.$prefix.'membertypes where mtid='.$row['upgrade']);
					$acc_row=lfmsql_fetch_assoc($res);
					$msg .= "Lifetime {$acc_row['accname']} \n";
					
					} else {
										
					if($type=="M") { $unit="month"; }
					  elseif($type=="Y") { $unit="year"; }
					if($period<>1) { $unit.="s"; }
					
					if ($freetrial == "yes") {
						$period=$row['trial_period'];
						$type=$row['trial_type'];
						$unit = "days";
						if($type=="M") { $unit="month"; }
						  elseif($type=="Y") { $unit="year"; }
						if($period<>1) { $unit.="s"; }
						$exp=strftime("%Y-%m-%d",strtotime("$today + 2 days"));
					}
					
					if ($exp<$today) {
						$exp=strftime("%Y-%m-%d",strtotime("$today + 2 days"));
						}	
					$exp=strftime("%Y-%m-%d",strtotime("$exp + $period $unit"));
					$res=lfmsql_query('select accname from '.$prefix.'membertypes where mtid='.$row['upgrade']);
					$acc_row=lfmsql_fetch_assoc($res);
					if ($data[processor] == "2checkout") {
					$upg_query = 'update '.$prefix.'members set mtype='.$row['upgrade'].', upgend="0000-00-00" where Id='.$data['user_id'].' limit 1';
					} else {
					$upg_query = 'update '.$prefix.'members set mtype='.$row['upgrade'].', upgend="'.$exp.'" where Id='.$data['user_id'].' limit 1';
					}
					lfmsql_query($upg_query);
					$msg .= "Account type of {$acc_row['accname']} expires $exp\n";
					
					}
					
					}
				
				// begin startpage update
				$r=lfmsql_query("SELECT type FROM ".$prefix."startpage_settings;");
				if($data['item_number']==1) {
					// get date from item name
					$date=substr($data['item_name'],-11);
					$y=substr($date,-4);
					$m=substr($date,3,3);
					$d=substr($date,0,2);
					if($m=="Jan") { $m=1; } elseif($m=="Feb") { $m=2; } elseif($m=="Mar") { $m=3; } elseif($m=="Apr") { $m=4; } elseif($m=="May") { $m=5; } elseif($m=="Jun") { $m=6; } elseif($m=="Jul") { $m=7; } elseif($m=="Aug") { $m=8; } elseif($m=="Sep") { $m=9; } elseif($m=="Oct") { $m=10; } elseif($m=="Nov") { $m=11; } elseif($m=="Dec") { $m=12; } else { processError('Date Error',$data); }
					$type=@lfmsql_result($r,0,"type");
					$from=mktime(0,0,0,$m,$d,$y);
					$to=$from+86399;
					if($type=="W") { $from=$from-(6*86400); }
					// check for offset difference
					$o1=date("O",$from)/100;
					$o2=date("O",$to)/100;
					$from=$from-3600*($o1-$o2);
					if($type=="M") { $from=mktime(0,0,0,$m,1,$y); }
					lfmsql_query("INSERT INTO ".$prefix."startpage (`from`, `to`, `usrid`, `purchased`, `cost`) VALUES ('$from', '$to', '$data[user_id]', NOW(), '$data[amount]');");
					$msg .= "\nStartpage booked from ".date("r",$from)." to ".date("r",$to);
					}
				// end startpage update
				
				// Run IPN Mods
				$getmods = lfmsql_query("Select filename from `".$prefix."ipnmods` where enabled=1");
				if (lfmsql_num_rows($getmods) > 0) {
				while ($modlist = lfmsql_fetch_array($getmods)) {
					$modfilename = trim($modlist['filename']);
					if (file_exists("ipn/".$modfilename)) {
						include("ipn/".$modfilename);
					}
				}
				}
				// End Run IPN Mods
				
				$msg = trim($msg);	
				$msg_a .= $msg;

				// commissions
				if ($usr['refid'] > 0) {
				
					$affid = $usr['refid'];

				//Process LFM Commission
				    $affqry="SELECT username, email, mtype FROM ".$prefix."members WHERE Id=$affid";
				    $affres=@lfmsql_query($affqry);
				    $affrow=@lfmsql_fetch_array($affres);
				
				if ($row['subscription'] == 2) {
				// commission level on OTO
				    $commqry="SELECT otocomm FROM ".$prefix."membertypes WHERE mtid=".$affrow["mtype"];
				    $commres=@lfmsql_query($commqry);
				    $commrow=@lfmsql_fetch_array($commres);
				    $comper = $commrow["otocomm"];
				} else {
				// commission level on purchases
				    $commqry="SELECT comm FROM ".$prefix."membertypes WHERE mtid=".$affrow["mtype"];
				    $commres=@lfmsql_query($commqry);
				    $commrow=@lfmsql_fetch_array($commres);
				    $comper = $commrow["comm"];
				}
				    
				    if($comper > 0) {
				    $affcom=round((($comper/100)*$data[amount]),2);

				    // Sales table keeps affiliate commission stats
				    $saleqry="INSERT INTO ".$prefix."sales (affid, purchaserid, saledate, itemid, itemname, itemamount, commission, txn_id, salestier) VALUES ($affid, ".$data['user_id'].", NOW(), '".$data['item_number']."', '".$data['item_name']."', ".$data['amount'].", $affcom, '".$data['txn_id']."', 1)";
				    lfmsql_query($saleqry);
				    
				    } else {
				    	// No Commission - Record Sale
					$saleqry="INSERT INTO ".$prefix."sales(affid,purchaserid,saledate,itemid,itemname,itemamount,commission,txn_id) VALUES(0,".$data['user_id'].",NOW(),'".$data['item_number']."','".$data['item_name']."',".$data['amount'].",0,'".$data['txn_id']."')";
	  					lfmsql_query($saleqry);
				    }

				    // Check for tier 2 commission
					// Get referrer so we can insert this sale into the database
				    $ul2qry="SELECT refid FROM ".$prefix."members WHERE Id=$affid";
				    $ul2res=@lfmsql_query($ul2qry);

				    if($ul2res)
				    {
					    $ul2row=@lfmsql_fetch_array($ul2res);
					    $aff2id=$ul2row["refid"];

						// Find out the affiliate's member type for commission
						$aff2qry="SELECT mtype FROM ".$prefix."members WHERE Id=$aff2id";
						$aff2res=@lfmsql_query($aff2qry);
						$aff2row=@lfmsql_fetch_array($aff2res);
					
						if ($row['subscription'] == 2) {
						// commission level on OTO
							$comm2qry="SELECT otocomm2 FROM ".$prefix."membertypes WHERE mtid=".$aff2row["mtype"];
							$comm2res=@lfmsql_query($comm2qry);
							$comm2row=@lfmsql_fetch_array($comm2res);
							$comper2 = $comm2row["otocomm2"];
						} else {
						// commission level on purchases
							$comm2qry="SELECT comm2 FROM ".$prefix."membertypes WHERE mtid=".$aff2row["mtype"];
							$comm2res=@lfmsql_query($comm2qry);
							$comm2row=@lfmsql_fetch_array($comm2res);
							$comper2 = $comm2row["comm2"];
						}
						
						if($comper2 > 0)
						{
							$aff2com=round((($comper2/100)*$data[amount]),2);
	
							// Sales table keeps affiliate commission stats
							$sale2qry="INSERT INTO ".$prefix."sales(affid, purchaserid, saledate, itemid, itemname, itemamount, commission, txn_id, salestier) VALUES ($aff2id, ".$data['user_id'].", NOW(), '".$data['item_number']."', '".$data['item_name']."', ".$data['amount'].", $aff2com, '".$data['txn_id']."', 2)";
							lfmsql_query($sale2qry);
						}
					
					}
				//End Process LFM Commission

						if($refnotify && ($affcom>0)) {
							// send email to referer
							$details = $username." has purchased $data[item_name] - $ $data[amount]\nYour commission is $ ".$affcom." (".$comper." %)";
							$subject=str_replace('[username]',$affrow["username"],$refsubj);
							$body=str_replace("\r\n","\n",$refbody);
							$body=str_replace('[details]',$details,$body);
							$body=str_replace('[username]',$affrow["username"],$body);
							$body=str_replace('[sitename]',$sitename,$body);
							mail($affrow["email"],$subject,$body,"From: $sitename <$ademail>\nBcc: $bcc");
							}
					} else {
						// No Commission - Record Sale
						$saleqry="INSERT INTO ".$prefix."sales(affid,purchaserid,saledate,itemid,itemname,itemamount,commission,txn_id) VALUES(0,".$data['user_id'].",NOW(),'".$data['item_number']."','".$data['item_name']."',".$data['amount'].",0,'".$data['txn_id']."')";
	  					lfmsql_query($saleqry);
					}
				//End commissions
				
					// LFMTE v2.05 Update
					$msg_log = preg_replace("/((\r(?!\n))|((?<!\r)\n)|(\r\n))/", " ", $msg_a);
					AddLog("IPN Success - ".$msg_log);

				if ($adnotify) {
					// send email to admin
					mail (CONTACT_EMAIL,'IPN Success - '.SITE_NAME,$msg_a,$headers);
					}	
				if ($memnotify) {
					// send email to member
					$subject=str_replace('[username]',$username,$row['email_subject']);
					$body=str_replace("\r\n","\n",$row['email_body']);
					$body=str_replace('[details]',$msg,$body);
					$body=str_replace('[username]',$username,$body);
					$body=str_replace('[sitename]',$sitename,$body);
					$body=str_replace('[room]',$room,$body);
					
					$useremail = lfmsql_result(lfmsql_query("SELECT email FROM ".$prefix."members WHERE Id='".$data['user_id']."'"), 0);
					
					$subject = translate_site_tags($subject);
					$subject = translate_user_tags($subject, $useremail);
					
					$body = translate_site_tags($body);
					$body = translate_user_tags($body, $useremail);
					
					mail($data['email'],$subject,$body,"From: $sitename <$ademail>\nBcc: $bcc");
					}	
				} else {
				processError ('Could not find sales package',$data);
				}	
			}	
		}	
	}
function processError ($msg,$data=array()) {
	global $headers;
	$msg = "$msg\n\n";
	foreach ($data as $k => $v) {
		$msg .= $k.' = '.$v."\n";
		}	
	mail (CONTACT_EMAIL,'IPN Message - '.SITE_NAME,"$msg\n$_SERVER[REMOTE_ADDR]",$headers);
	
	// LFMTE v2.05 Update
	$msg = preg_replace("/((\r(?!\n))|((?<!\r)\n)|(\r\n))/", " ", $msg);
	AddLog("IPN Error - ".$msg);
	
	}

?>