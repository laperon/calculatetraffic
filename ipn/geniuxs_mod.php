<?php

// /////////////////////////////////////////////////////////////////////
// GeniuXs For LFMTE
// Copyright �2012 LFM Wealth Systems. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that LFM Wealth Systems 
// has regarding the proper functioning of the script.
// /////////////////////////////////////////////////////////////////////

$geniuxs_checktime = time()+900;

$checktracking = @mysql_query("SELECT id, mailtype, mailid FROM `geniuxs_tracksales` WHERE userid=".$data['user_id']." AND time > ".$geniuxs_checktime." AND salespackage=".$data['item_number']." ORDER BY time DESC LIMIT 1");

if (mysql_num_rows($checktracking) > 0) {

	$trackid = mysql_result($checktracking, 0, "id");
	$mailtype = mysql_result($checktracking, 0, "mailtype");
	$mailid = mysql_result($checktracking, 0, "mailid");
	
	if (is_numeric($mailid) && $mailid > 0) {
	
		if ($mailtype == 1) {
			@mysql_query("UPDATE `geniuxs_groupmails` SET numsales=numsales+1, sales=sales+'".$data['amount']."' WHERE id=".$mailid." LIMIT 1");
			@mysql_query("UPDATE `geniuxs_maillog` SET numsales=numsales+1, sales=sales+'".$data['amount']."' WHERE mailid=".$mailid." ORDER BY starttime DESC LIMIT 1");
		} elseif ($mailtype == 2) {
			@mysql_query("UPDATE `geniuxs_newmem` SET numsales=numsales+1, sales=sales+'".$data['amount']."' WHERE id=".$mailid." LIMIT 1");
		} elseif ($mailtype == 3) {
			@mysql_query("UPDATE `geniuxs_schedsent` SET numsales=numsales+1, sales=sales+'".$data['amount']."' WHERE messageid=".$mailid." LIMIT 1");
		}
		
		@mysql_query("DELETE FROM `geniuxs_tracksales` WHERE id=".$trackid);
	}

}

?>