<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

$getipnfields = lfmsql_query("SELECT fieldid, fieldvalue FROM ".$prefix."customipnvals WHERE ipnid=".$data['item_number']);

if (lfmsql_num_rows($getipnfields) > 0) {

	while ($fieldlist = lfmsql_fetch_array($getipnfields)) {
		
		$fieldid = $fieldlist['fieldid'];
		$fieldvalue = $fieldlist['fieldvalue'];
		
		$getfieldname = lfmsql_query("SELECT name, type, options FROM `".$prefix."customfields` WHERE id=".$fieldid." AND onipn='1'");
		
		if ($fieldvalue > 0 && lfmsql_num_rows($getfieldname) > 0) {
			
			$fieldname = lfmsql_result($getfieldname, 0, "name");
			$fieldtype = lfmsql_result($getfieldname, 0, "type");
			$fieldoptions = lfmsql_result($getfieldname, 0, "options");
			
			if ($fieldtype == "1") {
				// A numerical value that should be incremented
				$existingval = lfmsql_query("SELECT fieldvalue FROM `".$prefix."customvals` WHERE fieldid=".$fieldid." AND userid='".$data['user_id']."'");
				if (lfmsql_num_rows($existingval) > 0) {
					$userfieldvalue = lfmsql_result($existingval, 0, "fieldvalue");
					if (is_numeric($userfieldvalue)) {
						$userfieldvalue = $userfieldvalue + $fieldvalue;
					} else {
						$userfieldvalue = $fieldvalue;
					}
					lfmsql_query("UPDATE `".$prefix."customvals` SET fieldvalue='".$userfieldvalue."' WHERE fieldid=".$fieldid." AND userid='".$data['user_id']."'");
				} else {
					lfmsql_query("INSERT INTO `".$prefix."customvals` (fieldvalue, fieldid, userid) VALUES ('".$fieldvalue."', ".$fieldid.", '".$data['user_id']."')");
				}
				$msg .= $fieldvalue." ".$fieldname." added to account\n";
			} elseif ($fieldtype == "2") {
				// A predefined value from a dropdown list
				$fieldoptionsarr = explode(',', $fieldoptions);
				$fieldoptionsindex = $fieldvalue - 1;
				$userfieldvalue = $fieldoptionsarr[$fieldoptionsindex];
				$existingval = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM `".$prefix."customvals` WHERE fieldid=".$fieldid." AND userid='".$data['user_id']."'"), 0);
				if ($existingval > 0) {
					lfmsql_query("UPDATE `".$prefix."customvals` SET fieldvalue='".$userfieldvalue."' WHERE fieldid=".$fieldid." AND userid='".$data['user_id']."'");
				} else {
					lfmsql_query("INSERT INTO `".$prefix."customvals` (fieldvalue, fieldid, userid) VALUES ('".$userfieldvalue."', ".$fieldid.", '".$data['user_id']."')");
				}
				$msg .= $fieldname." set to ".$userfieldvalue."\n";
			}
			
		}
	}
}

?>