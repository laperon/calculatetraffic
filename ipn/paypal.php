<?php
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2012 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

if ( !defined('SITE_NAME') ) { header("Location:../"); exit; }

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
foreach ($_POST as $key => $value) {
	$value = urlencode(stripslashes($value));
	$req .= "&$key=$value";
}

// post back to PayPal system to validate
$header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
$header .= "Host: www.paypal.com\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n";
$header .= "Connection: close\r\n\r\n";

$fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);
if ($fp) {
	fputs ($fp, $header . $req);
	$res = stream_get_contents($fp, 6144);
	$res2 = strip_tags($res);
	if (strpos($res2, "VERIFIED") !== false) {
		$email = mysql_query("SELECT payee FROM ".$prefix."ipn_merchants WHERE name='PayPal';");
		$email = strtolower(mysql_result($email,0));
		$new = array();
		$new['processor']='paypal';
		if ($payment_date>'') {
			$new['date']=$payment_date;
			$new['txn_id']=$txn_id;
			} else {
			$new['date']=$subscr_date;
			$new['txn_id']=$subscr_id;
			}
		$new['txn_type']=$txn_type;
		if ($txn_type=="subscr_signup" && $amount1=="0.00") {
			$payment_status='Completed';
			$payment_gross=$amount1;
			$new['trial_period']=$period1;
			}
		$new['item_name']=$item_name;
		if($item_number==4 && $payment_gross==6.95) { $item_number=5; }
		$new['item_number']=$item_number;
		$new['name']=$first_name.' '.$last_name;
		$new['email']=$payer_email;
		$new['amount']=$payment_gross;
		if(!$payment_fee) { $payment_fee="0.00"; }
		$new['fee']=$payment_fee;
		if(!$custom) {
			$r=mysql_query("SELECT user_id FROM ".$prefix."ipn_transactions WHERE email='$payer_email';");
			if(mysql_num_rows($r)) { $custom=mysql_result($r,0); }
			}
		$new['user_id']=$custom;
		if ($payment_status=='Completed'||$payment_status=='Pending') {
			$new['payment_status']='OK';
			processOrder ($txn_type,$new);
			} else {
			$new['payment_status']=$payment_status;
			}	
		} else {
		processError ('PayPal - post response "'.$res2.'" is not "VERIFIED"',$res);
		}
	} else {
	processError ('PayPal - SOCKET ERROR!',"$errno : $errstr");
	}

?>