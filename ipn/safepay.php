<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

if ( !defined('SITE_NAME') ) { header("Location:../"); exit; }

$r = mysql_query("SELECT payee,verify FROM ".$prefix."ipn_merchants WHERE name='SafePay';");
$payee = mysql_result($r,0,"payee");
$md5_hash = mysql_result($r,0,"verify");

// subscription? use subscrid instead of tid
if($subscrid) {
	$tid=$subscrid;
	// recurring subscription?
	if($subscriptionDate<>$submitdate) { $tid=$subscrid.date("/Ym");	}
	}

if($md5_hash==""||$md5_hash==$passPhrase){
	if ($ireceiver == $payee) {
		$type=substr($_ipn_act,5);
		$new = array();
		$new['processor']='safepay';
		$new['date']=$submitdate;
		$new['txn_type']=$payment_type;
		$new['txn_id']=$tid;
		$new['item_name']=$itemName;
		$new['item_number']=$itemNum;
		$new['name']=$ipayer;
		$new['email']=$ipayer_email;
		$new['amount']=$iamount;
		$new['fee']=2.99*($iamount/100)+0.59;
		$new['user_id']=$custom1;
		if ($result_full=="SUCCESS" || $result==1) {
			$new['payment_status']='OK';
			} else {
			$new['payment_status']=$result_full;
			}
		if ($result_full=="TEST") {
			$test_mode=$itestmode;
			}
		processOrder ($type,$new);
		} else {
		$err = 'SafePay - Invalid Receiver Email';
		processError ($err,$_POST);
		}	
	} else {
	$err = 'SafePay - Invalid Pass Phrase';
	processError ($err,$_POST);
	}
?>