<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.14
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////
require_once "inc/filter.php";

include "inc/config.php";
mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname) or die( "Unable to select database");

if (!isset($_GET['merch']) || !is_numeric($_GET['merch']) || !isset($_GET['id']) || !is_numeric($_GET['id'])) {
	echo ("Invalid GET vars");
	exit;
}

$getimgdata = mysql_query("SELECT merchant".$_GET['merch']."button FROM ".$prefix."ipn_prod_img WHERE productid=".$_GET['id']) or die(mysql_error());

$getdefaultimg = mysql_query("SELECT defaultbutton FROM ".$prefix."ipn_merchants WHERE id=".$_GET['merch']) or die(mysql_error());

if (mysql_num_rows($getimgdata) > 0) {
	$encodeddata = mysql_result($getimgdata, 0);
	
	if ($encodeddata == "") {
		if (mysql_num_rows($getdefaultimg) > 0) {
			$encodeddata = mysql_result($getdefaultimg, 0);
			Header( "Content-type: image/gif");
			echo base64_decode($encodeddata);
			exit;
		} else {
			echo("Could not find IPN ID");
			exit;
		}
	}
	
	Header( "Content-type: image/gif");
	echo base64_decode($encodeddata);
	exit;
} elseif (mysql_num_rows($getdefaultimg) > 0) {
	$encodeddata = mysql_result($getdefaultimg, 0);
	Header( "Content-type: image/gif");
	echo base64_decode($encodeddata);
	exit;
} else {
	echo("Could not find IPN ID");
	exit;
}

?>