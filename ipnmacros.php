<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if (stristr($textstring, '#IPN')) {
	// Replace IPN ID Macros
	
	$ipnrow[] = array();
	$res = array();
	
	if (isset($_SESSION["userid"]) && is_numeric($_SESSION["userid"])) {
		$ipnuserid = $_SESSION["userid"];
	} else {
		$ipnuserid = 0;
	}
	
	$res=@lfmsql_query("SELECT id FROM ".$prefix."ipn_products WHERE disable=0") or die("IPN macro error: ".lfmsql_error());
	while($ipnrow=@lfmsql_fetch_array($res)) {
		
		$searchipnmacro="#IPN".$ipnrow["id"]."#";
		if(stristr($textstring,$searchipnmacro)) {
			$paymentcode = show_button_code($ipnrow["id"]);
			$textstring = str_ireplace($searchipnmacro, $paymentcode, $textstring);
		}
	}
	
	// End Replace IPN ID Macros
}

?>