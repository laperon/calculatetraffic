<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.02
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

include "inc/config.php";
include "inc/funcs.php";
@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die( "Unable to select database");

if (isset($_GET['boardid']) && is_numeric($_GET['boardid'])) {

	$loadstats = mysql_query("Select * from `".$prefix."savedstats` where id=".$_GET['boardid']." limit 1");
	if (mysql_num_rows($loadstats) > 0) {
		$statstype = mysql_result($loadstats, 0, "statstype");
		$mtype = mysql_result($loadstats, 0, "mtype");
		
		$boardtype = mysql_result($loadstats, 0, "boardtype");
		$showid = mysql_result($loadstats, 0, "showid");
		$showfirst = mysql_result($loadstats, 0, "showfirst");
		$showlast = mysql_result($loadstats, 0, "showlast");
		$showuser = mysql_result($loadstats, 0, "showuser");
		$topnumber = mysql_result($loadstats, 0, "topnumber");
		
		$tablecolor = mysql_result($loadstats, 0, "tablecolor");
		$tablecolor2 = mysql_result($loadstats, 0, "tablecolor2");
		$bordercolor = mysql_result($loadstats, 0, "bordercolor");
		$textcolor = mysql_result($loadstats, 0, "textcolor");
		
		if ($tablecolor == "") { $tablecolor = "white"; }
		if ($tablecolor2 == "") { $tablecolor = "gray"; }
		if ($bordercolor == "") { $bordercolor = "black"; }
		if ($textcolor == "") { $textcolor = "black"; }
		
		if ($statstype == 1) {
			$thedate = date("Y-m-d");
			$startdate = $thedate;
			$enddate = $thedate;
		} else {
			$startdate = mysql_result($loadstats, 0, "startdate");
			$enddate = mysql_result($loadstats, 0, "enddate");
		}
		
		if ($boardtype == 1) {
			$sortby = "tempclicks";
			$statscolumn = "Clicks";
		} elseif ($boardtype == 2) {
			$sortby = "temprefs";
			$statscolumn = "Refs";
		} elseif ($boardtype == 3) {
			$sortby = "tempsales";
			$statscolumn = "Sales";
		} elseif ($boardtype == 4) {
			$sortby = "tempnumsales";
			$statscolumn = "# of Sales";
		} else {
			$sortby = "tempclicks";
			$statscolumn = "Clicks";
		}
		
		$searchfield = "mtype";
		$searchtext = mysql_result($loadstats, 0, "mtype");
		
		$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
		$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;
	} else {
		print "document.write('Leader Board Error');";
		flush();
		exit;
	}

} else {
	print "document.write('Leader Board Error');";
	flush();
	exit;
}

	// Show Weekly If Dates Are Invalid
	if ($startdate < "2000-01-01") {
		$startdate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")." + 7 days ago"));
	}
	if ($enddate < "2000-01-01") {
		$enddate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")));
	}

	// Update For One Day Stats
	if ($starttime > $endtime) {
		$endtime = $starttime;
		$enddate = $startdate;
	}
	// End Update For One Day Stats

	// Handle membertype search
	if($searchfield == "mtype" && $searchtext == 0) {
		$searchtext = "";
		$searchfield = "";
	}

    // Get the records
    $mqry="SELECT Id, firstname, lastname, username, tempclicks, temprefs, tempsales, tempnumsales, tempclicks FROM ".$prefix."members WHERE `status` = 'Active'";

    // Add search criteria if applicable
    if($searchtext != "") {
    
    		if ($timequery != "") {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' AND ".$timequery;
		} else {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."'";
		}
		
		$searchtext = str_replace("%", "", $searchtext);

    }

	$mres=@mysql_query($mqry);

	while($mrow=@mysql_fetch_array($mres))
	{
	
		if ($boardtype == 3 || $boardtype == 4) {
		// Get the sales for this member
		$tsales=0;
		$tnumsales=0;
		$commres=@mysql_query("SELECT SUM(itemamount) as tsales, COUNT(*) as tnumsales FROM ".$prefix."sales WHERE affid=".$mrow["Id"]." AND saledate <= '".$enddate." 23:59:59' AND saledate >= '".$startdate."' AND prize=0");
		if($commres)
		{
			$commrow=@mysql_fetch_array($commres);
			$tnumsales=$commrow["tnumsales"];
			$tsales=$commrow["tsales"];
			if($commission < 1) { $commission="0.00"; }
			if($tsales < 1) { $tsales="0.00"; }
		}
		else
		{
			$tsales = "0.00";
			$tnumsales = 0;
		}
		if ($tsales < 0 || $tsales == "") { $tsales = 0; }
		@mysql_query("Update ".$prefix."members set tempnumsales=".$tnumsales.", tempsales=".$tsales." where Id=".$mrow["Id"]." limit 1");
		}

		if ($boardtype == 2) {
		// Get the number of referrals for this member
		$refs=0;
		$refresquery = "SELECT COUNT(*) AS refs FROM ".$prefix."members WHERE status='Active' AND refid=".$mrow["Id"]." AND joindate <= '".$enddate." 23:59:59' AND joindate >= '".$startdate."'";
		$refres=@mysql_query($refresquery);
		$refrow=@mysql_fetch_array($refres);
		if(mysql_num_rows($refres) > 0)
		{
			$refs=$refrow["refs"];
		} else {
			$refs = 0;
		}
		@mysql_query("Update ".$prefix."members set temprefs=".$refs." where Id=".$mrow["Id"]." limit 1");
		}
		
		
		// Get the clicks for this member
		$clicks=0;
		$time=0;
		$accuracy=0;
		if ($statstype == 1) {
			// Today Stats
			$memres = mysql_query("Select clickstoday from ".$prefix."members where Id=".$mrow["Id"]." limit 1");
			$clicks = mysql_result($memres, 0, "clickstoday");
		} elseif ($statstype == 2) {
			// Lifetime Stats
			$memres = mysql_query("Select clickstoday from ".$prefix."members where Id=".$mrow["Id"]." limit 1");
			$clicks = mysql_result($memres, 0, "clickstoday");

			$histres = mysql_query("Select SUM(clicks) AS clicks from ".$prefix."surflogs where userid=".$mrow["Id"]);
			$clicks = $clicks + mysql_result($histres, 0, "clicks");
		} else {
			// Date Range Stats
			$histres = mysql_query("Select SUM(clicks) AS clicks from ".$prefix."surflogs where userid=".$mrow["Id"]." AND date <= '".$enddate."' AND date >= '".$startdate."'");
			$clicks = mysql_result($histres, 0, "clicks");
			
			if (date("Y-m-d") <= $enddate) {
			// Add Today's Clicks
			$memres = mysql_query("Select clickstoday from ".$prefix."members where Id=".$mrow["Id"]." limit 1");
			$clicks = $clicks + mysql_result($memres, 0, "clickstoday");
			}
			
		}
		if ($clicks < 0 || $clicks == "") { $clicks = 0; }
		@mysql_query("Update ".$prefix."members set tempclicks=".$clicks." where Id=".$mrow["Id"]." limit 1");
	}

// End Update The LFMTE Stats


	// Handle membertype search
	if($searchfield == "mtype" && $searchtext == 0) {
		$searchtext = "";
		$searchfield = "";
	}


	// Get the total member count for member browse
	$cqry="SELECT COUNT(*) as mcount FROM ".$prefix."members";
	

    $cres=@mysql_query($cqry);
    $crow=@mysql_fetch_array($cres);

    // Get the records
    $mqry="SELECT Id, firstname, lastname, username, tempclicks, temprefs, tempsales, tempnumsales, tempclicks FROM ".$prefix."members WHERE `status` = 'Active'";

    // Add search criteria if applicable
    if($searchtext != "") {
    
    		if ($timequery != "") {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' AND ".$timequery;
		} else {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."'";
		}
		
		$searchtext = str_replace("%", "", $searchtext);

    }

    $mqry.=" ORDER BY ".$sortby." desc LIMIT ".$topnumber;

    $mres=@mysql_query($mqry);

	// Prepare The Output
	
	$output = "<table border=1 bordercolor=\"".$bordercolor."\" cellpadding=3 cellspacing=0>
	<tr>
	<td align=\"center\"><font color=\"".$textcolor."\"><b>Rank</b></font></td>";

	if ($showid == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\"><b>User ID</b></font></td>"; }
	if ($showfirst == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\"><b>First Name</b></font></td>"; }
	if ($showlast == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\"><b>Last Name</b></font></td>"; }
	if ($showuser == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\"><b>User Name</b></font></td>"; }
		
	$output .= "<td align=\"center\"><font color=\"".$textcolor."\"><b>".$statscolumn."</b></font></td>
	</tr>";
	
	$rank = 1;

	while($mrow=@mysql_fetch_array($mres))
	{

		if($bgcolor == $tablecolor2)
		{
			$bgcolor = $tablecolor;
		}
		else
		{
			$bgcolor = $tablecolor2;
		}
		

      $output .= "<tr bgcolor=\"$bgcolor\">
      <td align=\"center\"><font color=\"".$textcolor."\">".$rank."</font></td>";
      
      $rank++;
      
      	if ($showid == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\">".$mrow["Id"]."</font></td>"; }
	if ($showfirst == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\">".$mrow["firstname"]."</font></td>"; }
	if ($showlast == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\">".$mrow["lastname"]."</font></td>"; }
	if ($showuser == 1) { $output .= "<td align=\"center\"><font color=\"".$textcolor."\">".$mrow["username"]."</font></td>"; }
	
		if ($boardtype == 1) {
			$statdata = $mrow["tempclicks"];
		} elseif ($boardtype == 2) {
			$statdata = $mrow["temprefs"];
		} elseif ($boardtype == 3) {
			$statdata = "$".$mrow["tempsales"];
		} elseif ($boardtype == 4) {
			$statdata = $mrow["tempnumsales"];
		} else {
			$statdata = $mrow["tempclicks"];
		}

         $output .= "<td align=\"center\"><font color=\"".$textcolor."\"><font size=\"2\" face=\"Verdana, Arial, Helvetica, sans-serif\">".$statdata."</font></font></td>";
         
         $output .= "</tr>";
        	
	}

$output .= "</table>";

// Output The Data
$output = str_replace("
", "", $output);
$output = str_replace("'", "\'", $output);
print "document.write('$output');";
flush();
exit;

?>
