<?php
// Word Search Game
// �2011 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

function addletter($userid=0) {
$prefix = $_SESSION["prefix"];
$bonusoutput = "";
if (!is_numeric($userid) || $userid < 1 || $_SESSION["userid"] != $userid) {
	//Invalid Request
	return "Word search error...";
} else {

	//Get Users Vars
	$getuser = mysql_query("Select wsword, wsletter, claimws, wsreset from ".$prefix."members where Id=$userid limit 1");
	$wsword = mysql_result($getuser, 0, "wsword");
	$wsletter = mysql_result($getuser, 0, "wsletter");
	$claimws = mysql_result($getuser, 0, "claimws");
	$wsreset = mysql_result($getuser, 0, "wsreset");
	
	@mysql_query("Update ".$prefix."members set wssecure=0 where Id=$userid limit 1");
	
	if ($claimws < 1) {
		return "Invalid access";
	}

	//Add Letter
	
	//Correct and reset invalid word
	$getword = mysql_query("Select * from ".$prefix."wswords where id=$wsword limit 1");
	if (mysql_num_rows($getword) != 1) {
	$bonusoutput = "Word search error...  We may be updating our phrase list.  Please contact support if you continue to see this error.";
	$newword = mysql_query("Select id from ".$prefix."wswords order by id limit 1");
	$wordid = mysql_result($newword, 0, "id");
	@mysql_query("Update ".$prefix."members set claimws=0, wsword=$wordid, wsletter=1 where Id=$userid limit 1");
	return $bonusoutput;
	}

	$wordid = mysql_result($getword, 0, "id");
	$phrase = mysql_result($getword, 0, "phrase");
	$bcredits = mysql_result($getword, 0, "cvalue");

	$nospaces = str_replace(" ", "", $phrase);
	$countphrase = strlen($nospaces);
	$lettersleft = $countphrase - $wsletter;

	if ($wsletter >= $countphrase) {

		//User found the last letter

		$i = 0;
		$j = 0;
		$showphrase = "";
		while ($j <= $countphrase) {
			$wscharacter = $phrase{$i};
			$i = $i+1;
			if ($wscharacter == " ") {
				$showphrase = $showphrase."&nbsp;";
			} else {
				$showphrase = $showphrase."<u>$wscharacter</u> ";
				$j = $j+1;
			}
		}

		$bonustext = "";
		if ($bcredits != 0) {
			$bonustext = $bonustext."$bcredits bonus credits have been added to your account.";
		}

		$bonusoutput = "You found all the letters: $showphrase<br>$bonustext";

		//Get next word or go back to first
		$getnextword = mysql_query("Select id from ".$prefix."wswords where id>$wordid order by id asc limit 1");
		if ((mysql_num_rows($getnextword) != 1) || ($wsreset == 1)) {
			$getfirstword = mysql_query("Select id from ".$prefix."wswords order by id limit 1");
			$nextword = mysql_result($getfirstword, 0, "id");
		} else {
			$nextword = mysql_result($getnextword, 0, "id");
		}

        $date = date("Y-m-d H:i:s");
        $wsword_id = mysql_query("Select cvalue from ".$prefix."members AS m JOIN ".$prefix."wswords as w WHERE m.wsword=w.id AND m.id = $userid");
        $cvalue = mysql_result($wsword_id, 0, "cvalue");
        mysql_query("INSERT INTO `".$prefix."members_calculations` SET  user_id = '".(int)$userid."', datetime = '".$date."', type='wsword' , credits='" . $cvalue . "'");

        //Award bonus
		@mysql_query("Update ".$prefix."members set credits=credits+$bcredits, wsword=$nextword, wsletter=1, wsreset=0, claimws=0 where Id=$userid limit 1");

	} else {

		//User found another letter

		@mysql_query("Update ".$prefix."members set wsletter=wsletter+1, claimws=0 where Id=$userid limit 1");

		if ($lettersleft > 1) {
			$bonusoutput = "You have collected another letter.<br>There are $lettersleft more letters to find. Keep surfing!";
		} else {
			$bonusoutput = "You have collected another letter.<br>There is only one more letter to find! Keep surfing!";
		}

	}
	
	return $bonusoutput;

}
}

function checkforletter($userid=0) {
$prefix = $_SESSION["prefix"];
if (!is_numeric($userid) || $userid < 1 || $_SESSION["userid"] != $userid) {
	//Invalid Request
} else {
	//Check For Letter
	$checkwsenabled = mysql_query("Select value from ".$prefix."wssettings where field='enabled' limit 1");
	$wsenabled = mysql_result($checkwsenabled, 0, "value");
	
	$checkclaimpage = mysql_query("Select value from ".$prefix."wssettings where field='claimpage' limit 1");
	$claimpage = mysql_result($checkclaimpage, 0, "value");

	if ($wsenabled == 1 && $claimpage == 0) {
	
	$checkrand = mysql_query("Select value from ".$prefix."wssettings where field='random' limit 1");
	$isrand = mysql_result($checkrand, 0, "value");

	$checkshowcount = mysql_query("Select value from ".$prefix."wssettings where field='showcount' limit 1");
	$showcount = mysql_result($checkshowcount, 0, "value");
	
	$findletter = "no";
	if ($isrand == 1) {
		$brandomnum = rand(1, 100);
		if ($brandomnum <= (100/$showcount)) {
			$findletter = "yes";
		}
	} else {
		$getclickstoday = mysql_query("Select clickstoday from ".$prefix."members where Id=$userid limit 1");
		$clickstoday = mysql_result($getclickstoday, 0, "clickstoday");
		if ($clickstoday%$showcount == 0) {
			$findletter = "yes";
		}
	}
	
	if ($findletter == "yes") {

		//Automatically Add The Letter
		@mysql_query("Update ".$prefix."members set claimws=1 where Id=$userid limit 1");
		$addresult = addletter($userid);
		return $addresult;

	} else {
		return displayletters($userid);
	}
	}
	
}
}

function checkforclaimpage($userid=0) {
$prefix = $_SESSION["prefix"];
if (!is_numeric($userid) || $userid < 1 || $_SESSION["userid"] != $userid) {
	//Invalid Request
} else {
	//Check For Letter
	$checkwsenabled = mysql_query("Select value from ".$prefix."wssettings where field='enabled' limit 1");
	$wsenabled = mysql_result($checkwsenabled, 0, "value");

	if ($wsenabled == 1) {
	
	$checkrand = mysql_query("Select value from ".$prefix."wssettings where field='random' limit 1");
	$isrand = mysql_result($checkrand, 0, "value");

	$checkshowcount = mysql_query("Select value from ".$prefix."wssettings where field='showcount' limit 1");
	$showcount = mysql_result($checkshowcount, 0, "value");
	
	$findletter = "no";
	if ($isrand == 1) {
		$brandomnum = rand(1, 100);
		if ($brandomnum <= (100/$showcount)) {
			$findletter = "yes";
		}
	} else {
		$getclickstoday = mysql_query("Select clickstoday from ".$prefix."members where Id=$userid limit 1");
		$clickstoday = mysql_result($getclickstoday, 0, "clickstoday");
		if ($clickstoday%$showcount == 0) {
			$findletter = "yes";
		}
	}
	
	if ($findletter == "yes") {

		//Show The Claim Page
		@mysql_query("Update ".$prefix."members set claimws=1 where Id=$userid limit 1");
		return 1;

	} else {
		return 0;
	}
	} else {
		return 0;
	}
	
}
}

function displayletters($userid=0) {
$prefix = $_SESSION["prefix"];

if (!is_numeric($userid) || $userid < 1 || $_SESSION["userid"] != $userid) {
	//Invalid Request
} else {
	$wsoutput = "";
	$i = 0;
	$j = 1;
	
	$getuserdata = mysql_query("Select wsword, wsletter from ".$prefix."members where Id=$userid limit 1");

	$wsword = mysql_result($getuserdata, 0, "wsword");
	$wsletter = mysql_result($getuserdata, 0, "wsletter");
	$wsletterpos = $wsletter-1;

	$getphrase = mysql_query("Select phrase from ".$prefix."wswords where id=$wsword limit 1");
	if (mysql_num_rows($getphrase) == 0) {
		// Reset Word
		$wsoutput = "Resetting...";
		$newword = mysql_query("Select id from ".$prefix."wswords order by id limit 1");
		$wordid = mysql_result($newword, 0, "id");
		@mysql_query("Update ".$prefix."members set claimws=0, wsword=$wordid, wsletter=1 where Id=$userid limit 1");
		return $wsoutput;
	}
	
	$phrase = mysql_result($getphrase, 0, "phrase");

	$countspaces = strlen($phrase);
	$nospaces = str_replace(" ", "", $phrase);
	$countnospaces = strlen($nospaces);
	$lettersleft = $countnospaces - $wsletterpos;

	while ($j < $wsletter) {
	$wscharacter = $phrase{$i};
	$i = $i+1;
	if ($wscharacter == " ") {
		$wsoutput = $wsoutput."&nbsp;";
	} else {
		$wsoutput = $wsoutput."<u>$wscharacter</u> ";
		$j = $j+1;
	}
	}

	$j = 0;

	while ($j < $lettersleft) {
	$wscharacter = $phrase{$i};
	$i = $i+1;
	if ($wscharacter == " ") {
		$wsoutput = $wsoutput."&nbsp;";
	} else {
		$wsoutput = $wsoutput." _";
		$j = $j+1;
	}
	}

	return $wsoutput;
	
}
}

?>