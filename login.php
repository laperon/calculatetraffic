<style type="text/css">
input.placeholder {
    text-align: center !important;
}
#loginfrm   {
max-width: 350px;
width: 100%;
float: left;
/* padding-left: 30px; */
padding-bottom: 25px;
}

.alert.alert-error {
    background: none repeat scroll 0 0 #E3A492;
    color: #852D1D;
    text-align: center;
}
.alert {
    border: 3px solid #FFFFFF;
    border-radius: 4px;
    box-shadow: 2px 2px 2px 2px #CCCCCC;
    cursor: pointer;
    font-size: 11px;
    margin-bottom: 10px;
    margin-top: 10px;
    padding: 8px 59px 8px 14px;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    width: 74%;
}
#loginfrm #submit
{
    background: none repeat scroll 0 0 #0d374f;
    border: medium none;
    color: #ffffff;
    cursor: pointer;
    float: left;
    font-family: "Open Sans";
    font-size: 13px;
    font-style: normal;
    text-align: center;
    text-transform: uppercase;
    width: 30%;
}

#loginfrm  legend {
    border: medium none;
    margin: 0 0 8%;
    padding: 0;
    width: 100%;
}
</style>
<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

if($_REQUEST["s"] == "noauth")
{
	include "inc/config.php";
	include "inc/funcs.php";
	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");
}
else
{
	include "inc/userauth.php";
}


include "inc/theme.php";

load_template ($theme_dir."/header.php");

// Get homepage body
$bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Homepage Body'") or die("Unable to find Homepage Body!");
$brow=@mysql_fetch_array($bres);
if (function_exists('html_entity_decode'))
{
   $page_content = html_entity_decode($brow["template_data"]);
}
else
{
   $page_content = unhtmlentities($brow["template_data"]);
}

load_template ($theme_dir."/content.php");
load_template ($theme_dir."/footer.php");
?>
</body>
</html>