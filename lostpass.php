<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";


	include "inc/config.php";
	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");

	// Query settings table for sitename
	$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
	$row=@mysql_fetch_array($res);
	$sitename=$row["sitename"];
	$replyaddress=$row["replyaddress"];

	function generatePassword ($length = 8)
	{
	  // start with a blank password
	  $password = "";
	
	  // define possible characters
	  $possible = "0123456789bcdfghjkmnpqrstvwxyz"; 
		
	  // set up a counter
	  $i = 0; 
		
	  // add random characters to $password until $length is reached
	  while ($i < $length) { 
		// pick a random character from the possible ones
		$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			
		// we don't want this character if it's already in the password
		if (!strstr($password, $char)) { 
		  $password .= $char;
		  $i++;
		}
	  }
	  // done!
	  return $password;
	}
	
	if($_POST["Submit"] == "Submit")
	{
		$lres=@mysql_query("SELECT username, email FROM ".$prefix."members WHERE email='".$_POST["email"]."'");
		$lrow=@mysql_fetch_array($lres);
		$username=$lrow["username"];
		$recipient=$lrow["email"];
		$password=generatePassword();
		$encpassword=md5($password);
				
		if(mysql_num_rows($lres) > 0)
		{
			// Update password
			@mysql_query("UPDATE ".$prefix."members SET `password` = '$encpassword' WHERE email='".$recipient."' AND username='".$username."'");
			// Get login URL
			$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
			$root_path=substr($fullurl,0,strrpos($fullurl, "/"));

			// Handle hosts with CGI PHP
			if(strlen($realscriptpath) > 10)
			{
                $loginurl=$realscriptpath."/login.php";
			}
			else
			{
                $loginurl=$root_path."/login.php";
            }


			$subject="Your login details for $sitename";
			$headers = 'From: '.$sitename.' <'.$replyaddress.'>';
			$msg="\nYou or someone else recently submitted a request to have your login details 
for $sitename sent to you. Your login details are as follows:
			
Username: $username
Password: $password

You can log in at: $loginurl

			
Regards,
			
The $sitename Password Retrieval Robot";

			mail($recipient,$subject,$msg,$headers);
			$msg="Your details have been emailed";

		}
		else
		{
			$msg="Sorry, we were unable to locate your email address.";
		}
	}

include "inc/theme.php";

load_template ($theme_dir."/header.php");

	if(isset($_POST["email"]))
	{
		$page_content =  "<center>$msg</center><br>Click <a href=\"login.php\">HERE</a> to return to the login page.";
	}
	else
	{
$page_content = <<<EOT
 <form method="post" action="lostpass.php">
 <br>
<table align="center" width="50%" border="0" cellspacing="0" cellpadding="0">
  <tr class="membertdbold">
    <td colspan="2" align="center">Lost your password? </td>
  </tr>
  <tr class="formlabel">
    <td colspan="2">Enter the email address you signed up with in the box below and we will send you a new password. </td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" nowrap="nowrap" class="formlabelbold">Email Address: </td>
    <td align="left"><input name="email" type="text" id="email" size="30" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" name="Submit" value="Submit" /></td>
  </tr>
</table>
</form>

<div align="center">
  <center>
  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="56%">
    <tr>
      <td width="100%">
      <h2 align="center"><font face="Georgia" color="#FF0000">Tired of losing passwords? <BR />Get RoboForm and never lose 
      your passwords again</font></h2>
      </td>
    </tr>

    <tr>
      <td width="100%"><p align="center"><a href='http://www.roboform.com/php/land.php?affid=hotbo&frm=frame33'>
<img src='http://www.roboform.com/affiliates/banners/457x300-nonanimated.jpg' width='457' height='300' alt='RoboForm: Learn more...' border=0>
</a></p>
</td>
    </tr>
  </table>
  </center>
</div>

EOT;
}
load_template ($theme_dir."/content.php");
load_template ($theme_dir."/footer.php");
?>