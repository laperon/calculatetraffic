<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.14
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/extra_auth.php";
	
	$headercontent = mysql_result(mysql_query("SELECT template_data FROM `".$prefix."templates` WHERE template_name='comms_header'"), 0);
	if (function_exists('html_entity_decode')) {
		$headercontent = html_entity_decode($headercontent);
	} else {
		$headercontent = unhtmlentities($headercontent);
	}
	$headercontent = translate_site_tags($headercontent);
	$headercontent = translate_user_tags($headercontent, $useremail);
	
	$page_content .= $headercontent;
	
	$page_content .= '<p>&nbsp;</p>
	 <table align="center" cellpadding="2" cellspacing="0">
	 <tr class="membertdbold"><td align="center">Date</td>
	 <td align="center">Buyer</td>
	 <td align="center">Product</td>
	 <td align="center">Commission</td>
	 <td align="center">Status</td>
	 </tr>';

  // Get sales details which are for this affiliate
  $cnt=0;
  $totalowing=0;
  $res=@mysql_query("SELECT * FROM ".$prefix."sales WHERE affid=$_SESSION[userid]") or die("Unable to get sales data: ".mysql_error());
  while($row=@mysql_fetch_array($res))
  {
	 $cnt++;
	 
	 if (is_numeric($row["purchaserid"]) && $row["purchaserid"] > 0) {
	 	$getuserinfo = mysql_query("SELECT firstname, lastname FROM ".$prefix."members WHERE Id=".$row["purchaserid"]." LIMIT 1") or die(mysql_error());
	 	if (mysql_num_rows($getuserinfo) > 0) {
	 		$buyername = mysql_result($getuserinfo, 0, "firstname")." ".mysql_result($getuserinfo, 0, "lastname");
	 	} else {
	 		$buyername = "User Not Found";
	 	}
	 } else {
	 	$buyername = "N/A";
	 }

	 if($row["status"]=="P")
	 {
		$status = "Paid";
	 }
	 else if($row["status"] == "R")
	 {
		$status="Refund";
		$totalowing+=$row["commission"];
	 }
	 else if($row["commission"] < 0)
	 {
	 	// Purchase subtracted from balance
		$status="";
		$totalowing = $totalowing+$row["commission"];
	 }
	 else
	 {
		$status="Unpaid";
		$totalowing+=$row["commission"];
	 }
	 $page_content .= '<tr>
		<td align="center">'.$row["saledate"].'</td>
		<td align="center">'.$buyername.'</td>
		<td align="center">'.$row["itemname"].'</td>
		<td align="center">'.$row["commission"].'</td>
		<td align="center">'.$status.'</td>
		</tr>';

  } // End of rows from query

  if($cnt==0){
	 $page_content .= '<tr class="formlabelbold"><td colspan="4" ><center>No sales found</center></td></tr>';
  }
  else {
	 $page_content .= '<tr class="formlabelbold">
		<td colspan="3" align="right">Total Owing:</td>
		<td align="right">$'.$totalowing.'</td>
		</tr>';
  }
  $page_content .= '<tr><td colspan="4" >&nbsp;</td></tr></table></div></div>';
?>