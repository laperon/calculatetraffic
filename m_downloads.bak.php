<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/extra_auth.php";

      $page_content .= '<p>&nbsp;</p><table align="center" cellpadding="2" cellspacing="0">
         <tr>
           <td colspan="2" align="center"><h3>Your Product Downloads</h3></td>
         </tr>
         <tr class="membertdbold">
         <td align="center">Product</td>
         <td align="center">Download</td>
         </tr>';

      $mdres=@mysql_query("SELECT productname,filename FROM ".$prefix."purchases p LEFT join ".$prefix."products r on p.itemid = r.productid where affid=".$_SESSION["userid"]);
      $pcnt=0;
      while($mdrow=@mysql_fetch_array($mdres))
      {
         $pcnt++;
         
         if(strpos($mdrow["filename"], "http://") !== false) {
		$page_content .= '<tr class="formlabel">
            <td align="center">'.$mdrow["productname"].'</td>
            <td align="center" nowrap="nowrap"><a target="_blank" href="'.$mdrow["filename"].'">Click here to download</a></td>
            </tr>';
            
	} else {
         
         $page_content .= '<tr class="formlabel">
            <td align="center">'.$mdrow["productname"].'</td>
            <td align="center" nowrap="nowrap"><a target="_blank" href="go.php?f='.$mdrow["filename"].'">Click here to download</a></td>
            </tr>';
            
         }

      }

      $page_content .= '</table>';

      if($pcnt==0)
      {
         $page_content .= '<center>No Products Found</center>';
      }

      $page_content .= '<table align="center" cellpadding="2" cellspacing="0">
         <tr>
            <td colspan="2" align="center"><h3>Free Product Downloads</h3></td>
         </tr>
         <tr class="membertdbold">
         <td align="center">Product</td>
         <td align="center">Download</td>
         </tr>';

      $mdres=@mysql_query("SELECT productname,filename FROM ".$prefix."products where free=1");
      $pcnt=0;
      while($mdrow=@mysql_fetch_array($mdres))
      {
         $pcnt++;
         $page_content .= '<tr class="formlabel">
            <td align="center">'.$mdrow["productname"].'</td>
            <td align="center" nowrap="nowrap"><a href="go.php?f='.$mdrow["filename"].'">Click here to download</a></td>
            </tr>';
      }

      $page_content .= '</table>';

      if($pcnt==0)
      {
         $page_content .= '<center>No Products Found</center>';
      }

      $page_content .= '<br>';
      $bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Download Page'") or die("Unable to find Download Page HTML!");
      $brow=@mysql_fetch_array($bres);
      $memberarea=translate_site_tags($brow["template_data"]);
      $memberarea=translate_user_tags($memberarea,$useremail);
      $memberarea=translate_file_tags($memberarea);
      if (function_exists('html_entity_decode'))
      {
         $page_content .= html_entity_decode($memberarea);
      }
      else
      {
         $page_content .= unhtmlentities($memberarea);
      }
?>