<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.24
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/extra_auth.php";
	
	// Get Categories
	$getcats = mysql_query("SELECT id, catname, description FROM `".$prefix."products_cats` ORDER BY catname ASC") or die(mysql_error());
	if (mysql_num_rows($getcats) > 0) {
		while ($catlist = mysql_fetch_array($getcats)) {
			
			$catid = $catlist['id'];
			$catname = $catlist['catname'];
			$catdesc = $catlist['description'];
			$catdownloads = array();
			
			$mdres=@mysql_query("SELECT productname,filename FROM ".$prefix."purchases p LEFT join ".$prefix."products r on p.itemid = r.productid where r.catid='".$catid."' AND affid=".$_SESSION["userid"]);
			$pcnt=0;
			      while($mdrow=@mysql_fetch_array($mdres))
			      {
			         $pcnt++;
			         
			         if(strpos($mdrow["filename"], "http://") !== false) {
					$catdownloads[] .= '<tr class="formlabel">
			            <td align="center">'.$mdrow["productname"].'</td>
			            <td align="center" nowrap="nowrap"><a target="_blank" href="'.$mdrow["filename"].'">Click here to download</a></td>
			            </tr>';
			            
				} else {
			         
			         $catdownloads[] .= '<tr class="formlabel">
			            <td align="center">'.$mdrow["productname"].'</td>
			            <td align="center" nowrap="nowrap"><a target="_blank" href="go.php?f='.$mdrow["filename"].'">Click here to download</a></td>
			            </tr>';
			            
			         }
			
			      }
			
			      $mdres=@mysql_query("SELECT productname,filename FROM ".$prefix."products where catid='".$catid."' AND free=1");
			      while($mdrow=@mysql_fetch_array($mdres))
			      {
			         $pcnt++;
			         $catdownloads[] .= '<tr class="formlabel">
			            <td align="center">'.$mdrow["productname"].'</td>
			            <td align="center" nowrap="nowrap"><a href="go.php?f='.$mdrow["filename"].'">Click here to download</a></td>
			            </tr>';
			      }
			      
			 if (count($catdownloads) > 0) {
					$page_content .= '<p>&nbsp;</p><table align="center" cellpadding="2" cellspacing="0">
								<tr><td colspan="2" align="center"><font size="3"><b>'.$catname.'</b></font></td></tr>
								<tr><td colspan="2" align="left">'.$catdesc.'</td></tr>
								<tr class="membertdbold">
								<td align="center">Product</td>
								<td align="center">Download</td>
								</tr>';
					
					foreach ($catdownloads as $dlvalue) {
						$page_content .= $dlvalue;
					}
					
					$page_content .= '</table>';
			}
		}
	}
	
      $page_content .= '<p>&nbsp;</p><table align="center" cellpadding="2" cellspacing="0">
         <tr>
           <td colspan="2" align="center"><font size="3"><b>Your Product Downloads</b></font></td>
         </tr>
         <tr class="membertdbold">
         <td align="center">Product</td>
         <td align="center">Download</td>
         </tr>';

      $mdres=@mysql_query("SELECT productname,filename FROM ".$prefix."purchases p LEFT join ".$prefix."products r on p.itemid = r.productid where r.catid='0' AND affid=".$_SESSION["userid"]);
      $pcnt=0;
      while($mdrow=@mysql_fetch_array($mdres))
      {
         $pcnt++;
         
         if(strpos($mdrow["filename"], "http://") !== false) {
		$page_content .= '<tr class="formlabel">
            <td align="center">'.$mdrow["productname"].'</td>
            <td align="center" nowrap="nowrap"><a target="_blank" href="'.$mdrow["filename"].'">Click here to download</a></td>
            </tr>';
            
	} else {
         
         $page_content .= '<tr class="formlabel">
            <td align="center">'.$mdrow["productname"].'</td>
            <td align="center" nowrap="nowrap"><a target="_blank" href="go.php?f='.$mdrow["filename"].'">Click here to download</a></td>
            </tr>';
            
         }

      }

      $mdres=@mysql_query("SELECT productname,filename FROM ".$prefix."products where catid='0' AND free=1");
      while($mdrow=@mysql_fetch_array($mdres))
      {
         $pcnt++;
         $page_content .= '<tr class="formlabel">
            <td align="center">'.$mdrow["productname"].'</td>
            <td align="center" nowrap="nowrap"><a href="go.php?f='.$mdrow["filename"].'">Click here to download</a></td>
            </tr>';
      }

      $page_content .= '</table></div></div>';

      if($pcnt==0)
      {
         $page_content .= '<center>No Products Found</center>';
      }

      $page_content .= '<br>';
      $bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Download Page'") or die("Unable to find Download Page HTML!");
      $brow=@mysql_fetch_array($bres);
      $memberarea=translate_site_tags($brow["template_data"]);
      $memberarea=translate_user_tags($memberarea,$useremail);
      $memberarea=translate_file_tags($memberarea);
      if (function_exists('html_entity_decode'))
      {
         $page_content .= html_entity_decode($memberarea);
      }
      else
      {
         $page_content .= unhtmlentities($memberarea);
      }
?>