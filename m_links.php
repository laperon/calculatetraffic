<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/extra_auth.php";

	//function to find out if the html headers is turned on, and output it
	function html_header ($type, $prefix) 
	{
		$header_enabled = "no";
		//check if the header is turned on
		$html_headers = mysql_query ("SELECT * FROM ".$prefix."promotion_headers WHERE type = '".$type."'");
		while ($html_header = mysql_fetch_array($html_headers)){
			$header_enabled = "yes";
			$template_data = $html_header[template_data];
		}
		if ($header_enabled == "yes"){
			return'<tr>
				<td align="center">&nbsp;</td>
				</tr>
				<td align="center">'.$template_data .'</td>
				</tr>
				<tr>
				<td align="center">&nbsp;</td>
				</tr>';
			} 
	}

	$snoopy = new Snoopy;


	function get_tiny_url($url) 
	{

		if (function_exists('curl_init')) {
	
	
  		  $ch = curl_init();
	
  		  $timeout = 20;
	
		  curl_setopt($ch,CURLOPT_URL,"http://u-click.it/shorten.php?exchange=".$_SERVER['SERVER_NAME']."&longurl=".$url);
	
		  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	
		  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);

		  $data = curl_exec($ch);
	
		  curl_close($ch);
		  return $data;
  
		} else {
		
		  return $url;
		
		}

	}

	
	  // We need the server URL for banner images
	  if(isset($_SERVER['DOCUMENT_ROOT'])) { $fullapp_path = $_SERVER['DOCUMENT_ROOT']; }
	  else if(isset($_ENV['DOCUMENT_ROOT'])) { $fullapp_path = $_ENV['DOCUMENT_ROOT']; }
	  else if(isset($_SERVER['PATH_TRANSLATED'])) { $fullapp_path = $_SERVER['PATH_TRANSLATED']; }
	  else { $fullapp_path = $_SERVER['SCRIPT_FILENAME']; }
	
	  // Strip filename from $fullapp_path
	  $app_path=substr($fullapp_path,0,strlen($fullapp_path)-15);
	
	  // Get the affiliate link details
	  $res=@mysql_query("SELECT affurl FROM ".$prefix."settings") or die("Unable to get affiliate link: ".mysql_error());
	  $row=@mysql_fetch_array($res);
	
	  $uqry="SELECT email,firstname,lastname FROM ".$prefix."members WHERE Id=".$_SESSION[userid];
	  $ures=@mysql_query($uqry) or die("Unable to get referrals: ".mysql_error());
	  $urow=@mysql_fetch_array($ures);
	
	
	  $step4="";
	  if($_POST["Submit"] == "Send" && strlen($_POST["friendemail"]) > 3 && strlen($_POST["youremail"]) > 3 && strlen($_POST["yourname"]) > 3)
	  {
		 $recipient=$_POST["friendemail"];
		 $headers  = "From: \"".$_POST["yourname"]."\"<".$_POST["youremail"].">\n";
		 $subject=trim($_POST["subject"]);
		 $from="-f".$_POST["youremail"];
		 $msg=trim($_POST["content"]);
	
		 $step4="Message Sent!";
		 mail($recipient,$subject,$msg,$headers,$from);
	  }
	  if($_POST["Submit"] == "Send" && (strlen($_POST["friendemail"])<3 || strlen($_POST["youremail"])<3 || strlen($_POST["yourname"])<3))
	  {
		 $step4="Message Not Sent - All Fields Must Be Filled!";
	  }
	
	  $step=0;
	
	  if($step4 > "")
		 $page_content .= '<center><font color="red"><strong>'.$step4.'</strong></font></center>';
		 
	  $page_content .= '<script>
	  function viewPromo(promo)
	  {
		  var windowprops = "location=no,scrollbars=no,menubars=no,toolbars=no,resizable=no" + ",left=100,top=100"; 
	
		  var URL = "viewpromo.php?promo="+promo; 
		  popup = window.open(URL,"PromoPopup",windowprops);	
	  }
	  </script>';

	  
	  $page_content .= '<h3 align="center">Your Affiliate Link</h3>
		<p align="center"><a href="'.$row["affurl"].'?rid='.$_SESSION[userid].'" target="_blank">'.$row["affurl"].'?rid='.$_SESSION[userid].'</a></p>
		<table width="75%" border="0" align="center" cellpadding="0" cellspacing="0">';

	  $emres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='email'");
	  if(mysql_num_rows($emres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Send an email to your subscribers</td>
			</tr>';
					 $page_content .= html_header('email', $prefix);
	
			   $page_content .= '
			<tr>
			<td align="center">&nbsp;</td>
			</tr>';
	
		 while($emrow=@mysql_fetch_array($emres))
		 {
			$subject=translate_site_tags($emrow["subject"]);
			$subject=translate_user_tags($subject,$urow["email"]);
			$content=translate_site_tags($emrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr class="formlabelbold">
			   <td align="left">Subject:</strong> '.$subject.'</td>
			   </tr>
			   <tr>
			   <td align="center">
			   <textarea name="textarea" cols="50" rows="8">'.$content.'</textarea></td>
			   </tr>
			   <tr>
			   <td align="center">&nbsp;</td>
			   </tr>';
		 }
	
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
	 
	  $emres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='tweet'");
	  if(mysql_num_rows($emres) > 0)
	  {
		$step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.':  Send a Tweet to your Twitter followers</td>
			</tr>';
					 $page_content .= html_header('tweet', $prefix);
	
			   $page_content .= '
			<tr>
			<td align="center">&nbsp;</td>
			</tr>';
		 while($emrow=@mysql_fetch_array($emres))
		 {
			$content=str_replace("#REFURL#", get_tiny_url($row[affurl].'?rid='.$_SESSION[userid]), $emrow["content"]);
			$content=translate_site_tags($content);
			$content=translate_user_tags($content,$urow["email"]);
	$page_content .= '   
		<tr>
		  <td align="center">
		  <textarea name="textarea" cols="50" rows="3">'.$content.'</textarea></td>
		</tr>
		<tr>
			<td align="center"><a href="http://twitter.com?status='.urlencode($content).'" target="_blank">Click Here To Send Tweet</a></td>
		</tr>        
		<tr>
		  <td align="center">&nbsp;</td>
		</tr>';
		 }
	
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
	  
	  
	  $emres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='facebooklike'");
	  if(mysql_num_rows($emres) > 0)
	  {
		$step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.':  Share this site on facebook</td>
			</tr>';
					 $page_content .= html_header('facebooklike', $prefix);
	
			   $page_content .= '
			<tr>
			<td align="center">&nbsp;</td>
			</tr>';
		 while($emrow=@mysql_fetch_array($emres))
		 {
			$content= $emrow["content"];
			$content=translate_site_tags($content);
			$content=translate_user_tags($content,$urow["email"]);
	$page_content .= '   
		<tr>
		  <td align="center">
		 <a name="fb_share" type="button_count" share_url="'.$content.'"  href="http://www.facebook.com/sharer.php&t=hello_world">Share</a><script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script></td>
		</tr>
		      
		<tr>
		  <td align="center">&nbsp;</td>
		</tr>';
		 }
	
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
				  
	  $revres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='review'");
	  if(mysql_num_rows($revres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Add  reviews to your site</td>
			</tr>';
					 $page_content .= html_header('review', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
		 while($revrow=@mysql_fetch_array($revres))
		 {
			$content=translate_site_tags($revrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr>
			   <td align="center"><textarea name="textarea2" cols="50" rows="8">'.$content.'</textarea></td>
			   </tr>
			   <tr>
			   <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }     
	
			
	$tafres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='tellafriend'");
	  if(mysql_num_rows($tafres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Tell A Friend</td>
			</tr><tr>';
					 $page_content .= html_header('tellafriend', $prefix);
	
			   $page_content .= '<td align="left">&nbsp;</td>
			</tr>';
		 while($tafrow=@mysql_fetch_array($tafres))
		 {
			$subject=translate_site_tags($tafrow["subject"]);
			$subject=translate_user_tags($subject,$urow["email"]);
			$content=translate_site_tags($tafrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
	
			$page_content .= '<tr><td>
			   <form method="post" action="members.php?mf=li" onSubmit="javascript:return validateTaf(this);">
			   <input name="subject" type="hidden" value="'.$subject.'" />
			   <input name="content" type="hidden" value="'.$content.'" />
			   <table width="100%" align="center">
				   <tr class="formlabelbold">
					 <td colspan="2" align="center">Note: All fields must be filled in</td>
					  </tr>
					<tr>
					 <td width="40%" align="right" class="formlabelbold">From:<br />         </td>
					  <td align="left"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
						<input name="yourname" type="text" class="footer-text" id="yourname" />
					  </font></strong></td>
					</tr>
					<tr>
					  <td align="right" class="formlabelbold">Your Email Address:</td>
					  <td align="left"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
						<input name="youremail" type="text" class="footer-text" id="youremail" />
					  </font></strong></td>
					</tr>
					<tr>
					  <td align="right" class="formlabelbold">Email address to send to:</td>
					  <td align="left"><input name="friendemail" type="text" class="footer-text" id="friendemail" size="30" /></td>
					</tr>
					<tr>
					  <td align="right" class="formlabelbold">
					  Subject: </td>
					  <td align="left">'.$subject.'</td>
					</tr>
					<tr>
					  <td colspan="2" align="left">&nbsp;</td>
					</tr>
					<tr class="formlabelbold">
					  <td colspan="2" align="center">Message:</td>
					</tr>
					<tr>
					  <td colspan="2" align="center"><textarea name="taftext" cols="50" rows="8" id="taftext">'.$content.'</textarea></td>
					</tr>
					<tr>
					  <td colspan="2" align="center"><input name="Submit" type="submit" id="Submit" value="Send" /></td>
					</tr>
					<tr>
					  <td colspan="2" align="center">&nbsp;</td>
					</tr>
				  </table>
				  </form>
				  </td></tr>';
	
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }        
	
	  $esres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='emailsig'");
	  if(mysql_num_rows($esres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Use this email signature</td>
			</tr>';
					 $page_content .= html_header('emailsig', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
	
		 while($esrow=@mysql_fetch_array($esres))
		 {
			$content=translate_site_tags($esrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr>
			   <td align="center"><textarea name="textarea3" cols="50" rows="8">'.$content.'</textarea></td>
			   </tr>
			   <tr>
				  <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	
	  }
	
	  $fsres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='forumsig'");
	  if(mysql_num_rows($fsres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Use this forum signature</td>
			</tr>';
					 $page_content .= html_header('forumsig', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
	
		 while($fsrow=@mysql_fetch_array($fsres))
		 {
			$content=translate_site_tags($fsrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr>
			   <td align="center"><textarea name="textarea4" cols="50" rows="8">'.$content.'
			   </textarea></td>
			   </tr>
			   <tr>
				  <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
	
	  $tsres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='topsponsor'");
	  if(mysql_num_rows($tsres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Use a top sponsor ad</td>
			</tr>';
					 $page_content .= html_header('topsponser', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
	
		 while($tsrow=@mysql_fetch_array($tsres))
		 {
			$content=translate_site_tags($tsrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr>
			   <td align="center"><textarea name="textarea5" cols="50" rows="8">'.$content.'</textarea></td>
			   </tr>
			   <tr>
				  <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
	
	  $spres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='squeezepage'");
	  if(mysql_num_rows($spres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Use a squeeze page to get<br />
			   affiliates to your URL</td>
			</tr>';
					 $page_content .= html_header('squeezepage', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
	
		 while($sprow=@mysql_fetch_array($spres))
		 {
			$subject=translate_site_tags($sprow["subject"]);
			$subject=translate_user_tags($subject,$urow["email"]);
			$content=translate_site_tags($sprow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr class="formlabelbold">
			   <td align="center">'.$subject.'</td>
			   </tr>
			   <tr><td align="center"><a href="javascript:viewPromo('.$sprow["id"].');">View HTML</a></td></tr>
			   <tr>
			   <td align="center"><textarea name="textarea6" cols="50" rows="8">'.$content.'</textarea></td>
			   </tr>
			   <tr>
				  <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
	  
      $sqpres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='splashpage'");
      if(mysql_num_rows($sqpres) > 0)
      {
         $domainurl = "http://".$_SERVER["SERVER_NAME"];
         $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Promote a splash page to get<br />
			visitors to your affiliate URL </td>
			</tr>';
					 $page_content .= html_header('splashpage', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';

         while($sqprow=@mysql_fetch_array($sqpres))
         {
            $splashid=$sqprow["id"];

            $page_content .= '<tr class="formlabelbold">
               <td align="center"><font size=2><a target="_blank" href="'.$domainurl.'/splashpage.php?splashid='.$splashid.'&rid='.$_SESSION["userid"].'">'.$domainurl.'/splashpage.php?splashid='.$splashid.'&rid='.$_SESSION["userid"].'</a></font></td>
               </tr>
               <tr>
                  <td align="center">&nbsp;</td>
               </tr>';
         }
         $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
      }
	
	  $slres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='safelist'");
	  if(mysql_num_rows($slres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Use a Safe list email to get visitors <br />
			   to your sqeeze page or affiliate URL</font></strong></td>
			</tr>';
					 $page_content .= html_header('safelist', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
	
		 while($slrow=@mysql_fetch_array($slres))
		 {
			$subject=translate_site_tags($slrow["subject"]);
			$subject=translate_user_tags($subject,$urow["email"]);
			$content=translate_site_tags($slrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr class="formlabelbold">
			   <td align="left">Subject:</strong> '.$subject.'</td>
			   </tr><tr>
			   <td align="center"><textarea name="textarea7" cols="50" rows="8">'.$content.'</textarea></td>
			   </tr>
			   <tr>
				  <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
	
	  $popres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='popup'");
	  if(mysql_num_rows($popres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Add a popup to your site </td>
			</tr>';
					 $page_content .= html_header('popup', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
	
		 while($poprow=@mysql_fetch_array($popres))
		 {
			$content=translate_site_tags($poprow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr>
			   <td align="center"><textarea name="textarea8" cols="50" rows="8">'.$content.'</textarea></td>
			   </tr>
			   <tr>
				  <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	
	  }
	
	  $tyres=@mysql_query("SELECT * FROM ".$prefix."promotion WHERE type='thankyou'");
	  if(mysql_num_rows($tyres) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Add a html to the thank you or<br />
			   logout page of your site </td>
			</tr>';
					 $page_content .= html_header('thankyou', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>';
	
		 while($tyrow=@mysql_fetch_array($tyres))
		 {
			$subject=translate_site_tags($tyrow["subject"]);
			$subject=translate_user_tags($subject,$urow["email"]);
			$content=translate_site_tags($tyrow["content"]);
			$content=translate_user_tags($content,$urow["email"]);
			$page_content .= '<tr class="formlabelbold">
			   <td align="center">'.$subject.'</td>
			   </tr>
			   <tr><td align="center"><a href="javascript:viewPromo('.$tyrow["id"].');">View HTML</a></td></tr>
			   <tr>
			   <td align="center"><textarea name="textarea9" cols="50" rows="8">'.$content.'
			   </textarea></td>
			   </tr>
			   <tr>
				  <td align="center">&nbsp;</td>
			   </tr>';
		 }
		 $page_content .= '<tr><td align="center">&nbsp;</td></tr>';
	  }
	
	  // Display images from links table
	  $ires=@mysql_query("SELECT * FROM ".$prefix."banners");
	  if(mysql_num_rows($ires) > 0)
	  {
		 $step++;
		 $page_content .= '<tr class="membertdbold">
			<td align="center">Step '.$step.': Add these banners to your site</td>
			</tr>';
					 $page_content .= html_header('banner', $prefix);
	
			   $page_content .= '
			<tr>
			   <td align="center">&nbsp;</td>
			</tr>
			<tr>
			   <td align="center">';
	
		 while($irow=@mysql_fetch_array($ires))
		 {
			$page_content .= '<tr><td align="center">
			   <table border=0>
			   <tr>
			   <td align="center">
			   <img src="getimg.php?id='.$irow["id"].'"><br /><br />  </td>
			   </tr>';
	
			if($bannerurls == 1) {
			   $page_content .= '<tr>
				  <td align="center" class="formlabel">Use this html code in your web page </td>
				  </tr>
				  <tr>
				  <td align="center"><textarea name="textarea10" cols="60" rows="3"><a href="'.$row["affurl"].'?rid='.$_SESSION[userid].'"><img src="'.$row['affurl'].'getimg.php?id='.$irow["id"].'"></a></textarea></td>
				  </tr>';
			}
			$page_content .= '<tr>
			   <td align="center">&nbsp;</td>
			   </tr>
			   </table>';
	
		 }
		 $page_content .= '</td></tr>
			<tr><td align="center">&nbsp;</td>';
	  }
	  $page_content .= '</tr><tr><td align="center">&nbsp;</td></tr></table></div></div>';
?>	  