<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.17
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/extra_auth.php";

	$pagetag=trim($_GET["page"]);
	$mpres=@mysql_query("SELECT pageid, mtype, pagedata, isfile, filename, delay FROM ".$prefix."memberpages WHERE pagetag='".$pagetag."'") or die(mysql_error());
	$mprow=@mysql_fetch_object($mpres);
	$p_mtype=$mprow->mtype;
	$delay = $mprow->delay;
	// Get the rank for the page
	$p_accres=@mysql_query("SELECT accname,rank FROM ".$prefix."membertypes WHERE mtid='".$p_mtype."'") or die(mysql_error());
	if (mysql_num_rows($p_accres) < 1) {
		$p_acctype = "";
		$p_rank = 9999;
	} else {
		$p_accrow=@mysql_fetch_object($p_accres);
		$p_acctype=$p_accrow->accname;
		$p_rank=$p_accrow->rank;
	}
	
	// Get the rank for the member
	$m_accres=@mysql_query("SELECT accname,rank FROM ".$prefix."membertypes WHERE mtid='".$mtype."'") or die(mysql_error());
	$m_accrow=@mysql_fetch_object($m_accres);
	$m_acctype=$m_accrow->accname;
	$rank=$m_accrow->rank;
	
$checkpurchased = mysql_result(mysql_query("SELECT COUNT(*) FROM ".$prefix."purchasedpages WHERE userid=".$_SESSION["userid"]." AND pageid=".$mprow->pageid), 0);

    $today = date("Y-m-d H:i:s"); 
    $daysjoined = date_diff_fix($joindate, $today); 
	if ($daysjoined < $delay && $checkpurchased == 0) 
	{ 
	  $page_content .= '<center> 
	  <font size="3" face="Arial, Helvetica, sans-serif">Sorry, this area is not available till you\'ve been a '.$p_acctype.' member for '.$delay.' days or longer.</font> 
	  </center>';
	} 
	elseif((($flow == 0 && ($mtype != $p_mtype)) || ($flow == 1 && ($rank < $p_rank))) && $checkpurchased == 0)
	{ 
	   $page_content .= '<center>
		  <font size="3" face="Arial, Helvetica, sans-serif">Sorry, your account is not authorized to access this page.</font>
		  </center>';
	} 
	else
	{	
		
		if ($mprow->isfile == 1){
			include $mprow->filename;
			}
		else {
			$memberarea=html_entity_decode($mprow->pagedata);
			}
		$memberarea=translate_file_tags($memberarea);
		$memberarea=translate_site_tags($memberarea);
		$memberarea=translate_user_tags($memberarea,$useremail);
		if (function_exists('html_entity_decode'))
		{
			$page_content .= html_entity_decode($memberarea);
		}
		else
		{
			$page_content .= unhtmlentities($memberarea);
		}
	}
?>