<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.17
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/extra_auth.php";

// /////////////////////////////////////
// Profile updated
// /////////////////////////////////////
if($_GET["mf"] == "upd")
{
   // Input Filters
	$_POST["firstname"] = $_REQUEST["firstname"] = text_filter($_REQUEST["firstname"], 1);
	$_POST["lastname"] = $_REQUEST["lastname"] = text_filter($_REQUEST["lastname"], 1);
	$_POST["username"] = $_REQUEST["username"] = text_filter($_REQUEST["username"], 2);
	$_POST["email"] = $_REQUEST["email"] = strtolower($_REQUEST["email"]);
	$_POST["paypal_email"] = $_REQUEST["paypal_email"] = strtolower($_REQUEST["paypal_email"]);
	
	
   $uqry="SELECT count(*) AS ucount FROM ".$prefix."members WHERE username='".$_POST["username"]."' AND Id <>".$_POST["id"];
   $ures=@mysql_query($uqry);
   $urow=@mysql_fetch_array($ures);
   if($urow["ucount"] > 0)
   {
      $page_content .= "<center><font color=\"red\"><strong>Username is already taken!</strong</font></center>";
   }
   else
   if(strlen($_POST["username"]) < 3 || strlen($_POST["username"]) < 3)
    {
      $page_content .= "<center><font color=\"red\"><strong>Username and Password must be at least 3 characters!</strong</font></center>";
    }
    else
    {
      // Do some validation
      $errmsg="";
      $formfields[] = Array();
      // Get the form settings into an array
      $frmres=@mysql_query("SELECT * FROM ".$prefix."signupform");
      while($frmrow=@mysql_fetch_array($frmres))
      {
         $fieldname=$frmrow["fieldname"];
         $formfields["$fieldname"] = $frmrow;
      }

      // Validate Firstname
      if($formfields["firstname"]["enable"] == 1 && $formfields["firstname"]["require"] == 1)
      {
         if(!isset($_REQUEST["firstname"]) || strlen(trim($_REQUEST["firstname"])) < 2)
         {
            $errmsg="First Name must be at least 2 characters<br>";
         }
      }

      // Validate Lastname
      if($formfields["lastname"]["enable"] == 1 && $formfields["lastname"]["require"] == 1)
      {
         if(!isset($_REQUEST["lastname"]) || strlen(trim($_REQUEST["lastname"])) < 2)
         {
            $errmsg.="Last Name must be at least 2 characters<br>";
         }
      }

      // Validate Email Address
      if(!isset($_REQUEST["email"]) || strlen(trim($_REQUEST["email"])) < 2)
      {
         $errmsg.="Email address is required<br>";
      }

       elseif (check_email($_REQUEST["email"]) !== true) {
      	$errmsg.="Email address is invalid<br>";
      }

      // Validate Address
      if($formfields["address"]["enable"] == 1 && $formfields["address"]["require"] == 1)
      {
         if(!isset($_REQUEST["address"]) || strlen(trim($_REQUEST["address"])) < 2)
         {
            $errmsg.="Please enter your address<br>";
         }
      }

      // Validate City
      if($formfields["city"]["enable"] == 1 && $formfields["city"]["require"] == 1)
      {
         if(!isset($_REQUEST["city"]) || strlen(trim($_REQUEST["city"])) < 2)
         {
            $errmsg.="Please enter your city<br>";
         }
      }

      // Validate State
      if($formfields["city"]["enable"] == 1 && $formfields["city"]["require"] == 1)
      {
         if(!isset($_REQUEST["state"]) || strlen(trim($_REQUEST["state"])) < 2)
         {
            $errmsg.="Please enter your state<br>";
         }
      }

      // Validate Postcode
      if($formfields["zip"]["enable"] == 1 && $formfields["zip"]["require"] == 1)
      {
         if(!isset($_REQUEST["postcode"]) || strlen(trim($_REQUEST["postcode"])) < 2)
         {
            $errmsg.="Please enter your postcode<br>";
         }
      }

      // Validate Telephone
      if($formfields["telephone"]["enable"] == 1 && $formfields["telephone"]["require"] == 1)
      {
         if(!isset($_REQUEST["telephone"]) || strlen(trim($_REQUEST["telephone"])) < 2)
         {
            $errmsg.="Please enter your telephone number<br>";
         }
      }

      // Validate Username
      if(!isset($_REQUEST["username"]) || strlen(trim($_REQUEST["username"])) < 4)
      {
         $errmsg.="Username must be at least 4 characters<br>";
      }

      // Validate Password
      if(strlen($_REQUEST["password"]) > 0 && strlen(trim($_REQUEST["password"])) < 4)
      {
         $errmsg.="Password must be at least 4 characters<br>";
      }

      // Validate Password
      if(strlen(trim($_REQUEST["password"])) >= 4 || strlen(trim($_REQUEST["password2"])) >= 4)
      {
         if(strcmp($_REQUEST["password"],$_REQUEST["password2"]) !=0)
         {
            $errmsg.="Confirm password does not match<br>";
         }
      }

      // Validate PayPal Email
      if($formfields["paypal"]["enable"] == 1 && $formfields["paypal"]["require"] == 1)
      {
         if(!isset($_REQUEST["paypal_email"]) || strlen(trim($_REQUEST["paypal_email"])) < 2)
         {
            $errmsg.="Please enter your PayPal email address<br>";
         }
      }

      if (isset($_REQUEST["paypal_email"]) && strlen(trim($_REQUEST["paypal_email"])) > 0) {
      	if (check_email($_REQUEST["paypal_email"]) !== true) {
      		$errmsg.="PayPal address is invalid<br>";
      	}
      }

      // Validate Clickbank ID
      if($formfields["cb_id"]["enable"] == 1 && $formfields["cb_id"]["require"] == 1)
      {
         if(!isset($_REQUEST["cb_id"]) || strlen(trim($_REQUEST["cb_id"])) < 2)
         {
            $errmsg.="Please enter your Clickbank ID<br>";
         }
      }
      
		// Validate Custom Fields
		$getfields = mysql_query("SELECT id, name FROM `".$prefix."customfields` WHERE required=1 AND onprofile=1 ORDER BY rank ASC");
		if (mysql_num_rows($getfields) > 0) {
			for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
				$fieldid = mysql_result($getfields, $i, "id");
				$fieldname = mysql_result($getfields, $i, "name");
				
				$fieldpostname = "custom_".$fieldid;
				
				if (!isset($_REQUEST[$fieldpostname]) || strlen(trim($_REQUEST[$fieldpostname])) <= 0) {
					$errmsg.=$fieldname." is required<br>";
				}
			}
		}
		// End Validate Custom Fields
		
	// Email Re-Verification
	$reqrever = mysql_result(mysql_query("SELECT reqrever FROM `".$prefix."settings` ORDER BY id DESC"), 0);
	if ($errmsg == "" && $reqrever == "1") {
		$oldemail = mysql_result(mysql_query("SELECT email FROM ".$prefix."members WHERE Id='".$_SESSION['userid']."'"), 0);
		if ($_POST["email"] != $oldemail) {
			// The email has been changed and re-verification is required
			$_POST["newsletter"] = 0;
			$vericode = md5(rand(100,10000));
			mysql_query("UPDATE ".$prefix."members SET email='".$_POST["email"]."', status='Unverified', vericode='".$vericode."' WHERE Id='".$_SESSION['userid']."'") or die(mysql_error());
			SendWelcomeEmail($_POST["email"], 0);
			$page_content .= '<p>&nbsp;</p><center><h3><font color="#FF0000">Your Email Has Been Changed.<br />Please click the link in the verification e-mail to re-activate your account.</font></h3></center>';
		}
	}
	// End Email Re-Verification

      if(isset($_POST["newsletter"]))
      {
         $newsletter=$_POST["newsletter"];
      }
      else
      {
         $newsletter=0;
      }
      if(isset($_POST["affnotify"]))
      {
         $affnotify=$_POST["affnotify"];
      }
      else
      {
         $affnotify=0;
      }
      // Validate Region
      // if($formfields["geo"]["enable"] == 1 && $formfields["geo"]["require"] == 1)
      // {
      // if(!isset($_REQUEST["geo"]))
      // {
      //    $errmsg.="Please select your region<br>";
      // }
      // }

      if($errmsg == "")
      {
          if(strlen($_POST["password"]) >=4)
          {
              $encpassword=md5(trim($_POST["password"]));
              $qry="UPDATE ".$prefix."members SET firstname='".$_POST["firstname"]."', lastname='".$_POST["lastname"]."', email='".$_POST["email"]."',
              telephone='".$_POST["telephone"]."', address='".$_POST["address"]."', city='".$_POST["city"]."', state='".$_POST["state"]."',
              postcode='".$_POST["postcode"]."', country='".$_POST["country"]."', username='".$_POST["username"]."', password='".$encpassword."',
              paypal_email='".$_POST["paypal_email"]."', cb_id='".$_POST["cb_id"]."',newsletter=".$newsletter.",affnotify=".$affnotify." WHERE Id=".$_SESSION[userid];
          }
          else
          {
              $qry="UPDATE ".$prefix."members SET firstname='".$_POST["firstname"]."', lastname='".$_POST["lastname"]."', email='".$_POST["email"]."',
              telephone='".$_POST["telephone"]."', address='".$_POST["address"]."', city='".$_POST["city"]."', state='".$_POST["state"]."',
              postcode='".$_POST["postcode"]."', country='".$_POST["country"]."', username='".$_POST["username"]."',
              paypal_email='".$_POST["paypal_email"]."', cb_id='".$_POST["cb_id"]."',newsletter=".$newsletter.",affnotify=".$affnotify." WHERE Id=".$_SESSION[userid];
          }
          mysql_query($qry);
          
      	// Update Custom Fields
	$getfields = mysql_query("SELECT id FROM `".$prefix."customfields` WHERE onprofile=1 ORDER BY rank ASC");
	if (mysql_num_rows($getfields) > 0) {
		for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
			$fieldid = mysql_result($getfields, $i, "id");
			
			$fieldpostname = "custom_".$fieldid;
			
			if (isset($_POST[$fieldpostname])) {
				mysql_query("DELETE FROM ".$prefix."customvals WHERE fieldid='".$fieldid."' AND userid='".$_SESSION[userid]."'") or die(mysql_error());
				mysql_query("INSERT INTO ".$prefix."customvals (fieldid, userid, fieldvalue) VALUES ('".$fieldid."', '".$_SESSION[userid]."', '".$_POST[$fieldpostname]."')") or die(mysql_error());
			}
		}
	}
	// End Update Custom Fields
          
      }
   }
}

$qry="SELECT * FROM ".$prefix."members WHERE Id=".$_SESSION[userid];
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);

	if($_POST["Submit"] == "Delete My Account")
	{
	   $page_content .= '<form name="updfrm" method="post" action="members.php?mf=ydc">
		  <center><p><strong>Are You Sure?</strong><br />
		  <strong><font color="#FF0000">You are about to permanently delete your account!</font></strong></p>';

	   // Get template data from database
	   $hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Delete Confirmation'");
	   $hrow=@mysql_fetch_array($hres);
	   // Decode template data
	   if (function_exists('html_entity_decode'))
	   {
		  $page_content .= html_entity_decode($hrow["template_data"]);
	   }
	   else
	   {
		  $page_content .= unhtmlentities($hrow["template_data"]);
	   }

	   $page_content=translate_site_tags($page_content);
	   $page_content=translate_user_tags($page_content,$useremail);
	   $page_content=translate_upgrade_tags($page_content);
	   $page_content=translate_file_tags($page_content);

	   $page_content .= '<p><input name="Submit" type="submit" id="Submit" value="Yes - Delete My Account" />
		  &nbsp;<input name="Cancel" type="button" value="No - I\'ve Changed My Mind" onClick="window.location.href=\'members.php?mf=pf\';" />
		  </p></center></form>';
	}
	else
	{
            $page_content .= '<center><h3><font color="#FF0000">'.$errmsg.'</font></h3></center>
			 <form name="updfrm" method="post" action="members.php?mf=upd">
			   <table width="250" border="0" align="center" cellpadding="4" cellspacing="0">
				 <tr class="membertdbold">
				   <td colspan="2" align="center">Edit Your Profile</td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">First Name:</td>
			 <td align="left"><input name="firstname" type="text" id="firstname" value="'.$mrow["firstname"].'" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Last Name:</td>
			 <td align="left"><input name="lastname" type="text" id="lastname" value="'.$mrow["lastname"].'" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Email Address:</strong></td>
			 <td align="left"><input name="email" type="text" id="email" value="'.$mrow["email"].'" size="30" /></td>
		   </tr>

				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Address:</td>
			 <td align="left"><input name="address" type="text" id="address" value="'.$mrow["address"].'" size="30" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">City:</td>
			 <td align="left"><input name="city" type="text" id="city" value="'.$mrow["city"].'" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">State/County:</td>
			 <td align="left"><input name="state" type="text" id="state" value="'.$mrow["state"].'" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Zip/Postcode:</td>
			 <td align="left"><input name="postcode" type="text" id="postcode" value="'.$mrow["postcode"].'" size="12" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Country:</td>
			 <td align="left"><select name="country">'.select_country($mrow["country"]).'</select></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Telephone:</td>
			 <td align="left"><input name="telephone" type="text" id="telephone" value="'.$mrow["telephone"].'" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Username:</td>
			 <td align="left"><input name="username" type="text" id="username" value="'.$mrow["username"].'" /></td>
		   </tr>
				 <tr>
				   <td colspan="2" align="left" nowrap="NOWRAP" bgcolor="#FFFFCC" class="formlabelbold"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">Note: Leave password field blank unless changing password </font></td>
				 </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Password:</td>
				   <td align="left"><input name="password" type="password" id="password" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Confirm Password:</td>
				   <td align="left"><input name="password2" type="password" id="password2" /></td>
				 </tr>

				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">&nbsp;</td>
				   <td align="left">&nbsp;</td>
				 </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Paypal Email:</td>
			 <td align="left"><input name="paypal_email" type="text" id="paypal_email" value="'.$mrow["paypal_email"].'" size="30" /></td>
		   </tr>
				 <tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Clickbank ID: </td>
				   <td align="left"><input name="cb_id" type="text" id="cb_id" value="'.$mrow["cb_id"].'" /></td>
				 </tr>';
				 
// Custom Fields
$getfields = mysql_query("SELECT id, name, type, options FROM `".$prefix."customfields` WHERE onprofile=1 ORDER BY rank ASC");
if (mysql_num_rows($getfields) > 0) {
	for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
		$fieldid = mysql_result($getfields, $i, "id");
		$fieldname = mysql_result($getfields, $i, "name");
		$fieldtype = mysql_result($getfields, $i, "type");
		$fieldoptions = mysql_result($getfields, $i, "options");
		
		$fieldpostname = "custom_".$fieldid;
		
		$getfieldvalue = mysql_query("SELECT fieldvalue FROM `".$prefix."customvals` WHERE fieldid='".$fieldid."' AND userid='".$mrow["Id"]."'");
		if (mysql_num_rows($getfieldvalue) > 0) {
			$fieldvalue = mysql_result($getfieldvalue, 0, "fieldvalue");
		} else {
			$fieldvalue = "";
		}
		
		$page_content .= '
		<tr>
    			<td align="left" nowrap="nowrap" class="formlabelbold">'.$fieldname.': </td>
    			<td align="left" valign="top">';
    			
    			if ($fieldtype == 1) {
				$page_content .= '<input name="'.$fieldpostname.'" type="text" maxlength="255" id="'.$fieldpostname.'" value="'.$fieldvalue.'" />';
			} else {
				$page_content .= '<select name="'.$fieldpostname.'" id="'.$fieldpostname.'">';
				
				$splitoptions = explode(",", $fieldoptions);
				for ($j = 0; $j < count($splitoptions); $j++) {
					if (strlen(trim($splitoptions[$j])) > 0) {
						$splitoptions[$j] = trim($splitoptions[$j]);
						$page_content .= '<OPTION VALUE="'.$splitoptions[$j].'"';
						if ($fieldvalue == $splitoptions[$j]) { $page_content .= ' selected="selected"'; }
						$page_content .= '>'.$splitoptions[$j].'</OPTION>';
					}
				}
				
				$page_content .= '</select>';
			}
			
			$page_content .= '</td>
		</tr>
		';
		
		
	}
}
// End Custom Fields
				 
				 $page_content .= '<tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Receive Newsletters: </td>
				   <td align="left"><input name="newsletter" type="checkbox" id="newsletter" value="1"'.($mrow["newsletter"] == 1 ? "checked=\"checked\"" : "") .'/></td>
				 </tr>';
if(get_setting("notifydownline") == "1")
{
				 $page_content.='<tr>
				   <td align="left" nowrap="NOWRAP" class="formlabelbold">Receive Referral Notifications: </td>
				   <td align="left"><input name="affnotify" type="checkbox" id="affnotify" value="1"'.($mrow["affnotify"] == 1 ? "checked=\"checked\"" : "") .'/></td>
				 </tr>';
}
		   $page_content.='<tr>
			 <td colspan="2" align="center">
		  <input type="hidden" name="id" value="'.$mrow["Id"].'" />
		 </td></tr>

		 <tr>
		 <td colspan="2" align="center">
		  <input type="submit" name="Submit" value="Update" /></td>
		   </tr>
		   <tr>
			 <td colspan="2" align="center">
		  <input type="submit" name="Submit" value="Delete My Account" /></td>
		   </tr>
		  </table>
		 <center>'.$msg.'</center>
			 </form></div></div>';

	}
?>