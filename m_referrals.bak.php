<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/extra_auth.php";
	
	$page_content .= '<h3 align="center">Direct Referrals</h3>
	<table border="0" align="center" cellpadding="2" cellspacing="0">
        <tr class="membertdbold">
          <td align="center">First Name</td>
          <td align="center">Last Name</td>
          <td align="center">Date Joined</td>
          <td align="center">Total Sales</td>';
          
          if($showrefmail == 1) {
	          $page_content .= '<td align="center">&nbsp;</td>';
	  }
          
        $page_content .= '</tr>';

  $uqry="SELECT firstname,lastname,joindate,commission,email FROM ".$prefix."members WHERE refid=".$_SESSION[userid];
  $ures=@mysql_query($uqry) or die("Unable to get referrals: ".mysql_error());
  while($urow=@mysql_fetch_array($ures))
  {
	 $page_content .= '<tr>
		 <td align="center">'.$urow["firstname"].'</td>
		 <td align="center">'.$urow["lastname"].'</td>
		 <td align="center">'.date("d-M-Y",strtotime($urow["joindate"])).'</td>
		 <td align="center">'.$urow["commission"].'</td>';
             if($showrefmail == 1) {
             	$page_content .= '<td align="center">
	            <a href="mailto:'.$urow["email"].'">
        	    <img src="images/email.jpg" alt="Send Email" border="0" width="16" height="13" /></a></td>';
            }
            
           $page_content .= '</tr>';
  }
  $page_content .= '</table><br>';
?>