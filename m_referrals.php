<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.20
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////
	
	$domainurl = "http://".$_SERVER["SERVER_NAME"];
	
	require_once "inc/extra_auth.php";
	
	$cantransfer = mysql_result(mysql_query("SELECT cantransfer FROM ".$prefix."membertypes WHERE mtid='".$mtype."'"), 0);
	
	$page_content .= '<h3 align="center">Direct Referrals</h3>';
	
	if (isset($_POST['sorttype']) && is_numeric($_POST['sorttype'])) {
		$_GET['sorttype'] = $_POST['sorttype'];
	}
	
	if (!isset($_GET['sorttype']) || !is_numeric($_GET['sorttype'])) {
		$_GET['sorttype'] = 0;
	}
	
	if ($_GET['sorttype'] == 1) {
		// Sort By Join Date
		$sortquery = " ORDER BY joindate DESC";
	} elseif ($_GET['sorttype'] == 2) {
		// Sort By Login Date
		$sortquery = " ORDER BY lastlogin DESC";
	} elseif ($_GET['sorttype'] == 3) {
		// Sort By Name
		$sortquery = " ORDER BY firstname ASC";
	} elseif ($_GET['sorttype'] == 4) {
		// Sort By Membership Level
		$sortquery = " ORDER BY mtype DESC";
	} elseif ($_GET['sorttype'] == 5) {
		// Sort By Pages Surfed
		$uqry="SELECT * FROM ".$prefix."members WHERE refid='".$_SESSION[userid]."'";
	  	$ures=@mysql_query($uqry) or die("Unable to get referrals: ".mysql_error());
	  	while($urow=@mysql_fetch_array($ures)) {
		  	$startdate = '2000-01-01';
  			$enddate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")));
			$memres = mysql_query("Select clickstoday from ".$prefix."members where Id=".$urow["Id"]." limit 1");
			$clicks = mysql_result($memres, 0, "clickstoday");
			$histres = mysql_query("Select SUM(clicks) AS clicks from ".$prefix."surflogs where userid=".$urow["Id"]);
			$clicks = $clicks + mysql_result($histres, 0, "clicks");
			mysql_query("UPDATE ".$prefix."members SET tempclicks='".$clicks."' WHERE Id=".$urow["Id"]." LIMIT 1");
	  	}
		$sortquery = " ORDER BY tempclicks DESC";
	} elseif ($_GET['sorttype'] == 6) {
		// Sort By Total Purchased
		$uqry="SELECT * FROM ".$prefix."members WHERE refid='".$_SESSION[userid]."'";
	  	$ures=@mysql_query($uqry) or die("Unable to get referrals: ".mysql_error());
	  	while($urow=@mysql_fetch_array($ures)) {
		  	$startdate = '2000-01-01';
  			$enddate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")));
			$commres = mysql_query("SELECT SUM(itemamount) FROM ".$prefix."sales WHERE purchaserid=".$urow["Id"]." AND saledate <= '".$enddate." 23:59:59' AND saledate >= '".$startdate."' AND prize=0");
			if($commres) {
				$purchased = mysql_result($commres, 0);
				if(!is_numeric($purchased) || $purchased <= 0) { $purchased = "0.00"; }
				$purchased = round($purchased, 2);
			} else {
				$purchased = "0.00";
			}
	  		mysql_query("UPDATE ".$prefix."members SET tempsales='".$purchased."' WHERE Id=".$urow["Id"]." LIMIT 1");
	  	}
		$sortquery = " ORDER BY tempsales DESC";
	} else {
		$sortquery = " ORDER BY joindate DESC";
	}
	
	$refcounter = mysql_result(mysql_query("SELECT COUNT(*) FROM ".$prefix."members WHERE refid='".$_SESSION[userid]."'"), 0);
	
	$refsperpage = "10";
	if (!isset($_GET['pageoffset']) || !is_numeric($_GET['pageoffset'])) {
		$pageoffset = "0";
	} else {
		$pageoffset = $_GET['pageoffset'];
	}
	
	$uqry="SELECT * FROM ".$prefix."members WHERE refid='".$_SESSION[userid]."'".$sortquery." LIMIT ".$pageoffset.", ".$refsperpage;
  	$ures=@mysql_query($uqry) or die("Unable to get referrals: ".mysql_error());
  	
	if ($refcounter < 1) {
		$page_content .= '<p align="center"><font size="3"><b>No Referrals Found</b></font></p>';
	} else {
		$page_content .= '<center>
		<p align="center"><font size="3"><b>You Have '.$refcounter.' Referrals</b></font></p>
		<form action="members.php?mf=mr" method="post">
		<font size="2">Sort By: </font>
		<select name="sorttype" class="membertype">
			<option value="1"'; if($_GET['sorttype'] == 1) { $page_content .= ' selected="selected"'; } $page_content .= '>Join Date</option>
			<option value="2"'; if($_GET['sorttype'] == 2) { $page_content .= ' selected="selected"'; } $page_content .= '>Last Login</option>
			<option value="3"'; if($_GET['sorttype'] == 3) { $page_content .= ' selected="selected"'; } $page_content .= '>Name</option>
			<option value="4"'; if($_GET['sorttype'] == 4) { $page_content .= ' selected="selected"'; } $page_content .= '>Membership Level</option>
			<option value="5"'; if($_GET['sorttype'] == 5) { $page_content .= ' selected="selected"'; } $page_content .= '>Pages Surfed</option>
			<option value="6"'; if($_GET['sorttype'] == 6) { $page_content .= ' selected="selected"'; } $page_content .= '>Total Purchased</option>
		</select>
		<input type="submit" value="Sort" class="membersort">
		</form> <br />
		';
	}
  	
  	while($urow=@mysql_fetch_array($ures)) {
  		
  		$getaccname = mysql_query("SELECT accname FROM ".$prefix."membertypes WHERE mtid=".$urow["mtype"]);
  		if (mysql_num_rows($getaccname) > 0) {
  			$accname = mysql_result($getaccname, 0, "accname");
  		} else {
  			$accname = "Member Level Unknown";
  		}
  		
  		// Get Stats
  		$startdate = '2000-01-01';
  		$enddate = strftime("%Y-%m-%d", strtotime(date("Y-m-d")));
		$memres = mysql_query("Select clickstoday from ".$prefix."members where Id=".$urow["Id"]." limit 1");
		$clicks = mysql_result($memres, 0, "clickstoday");
		$histres = mysql_query("Select SUM(clicks) AS clicks from ".$prefix."surflogs where userid=".$urow["Id"]);
		$clicks = $clicks + mysql_result($histres, 0, "clicks");
		
		$commres = mysql_query("SELECT SUM(itemamount) FROM ".$prefix."sales WHERE purchaserid=".$urow["Id"]." AND saledate <= '".$enddate." 23:59:59' AND saledate >= '".$startdate."' AND prize=0");
		if($commres) {
			$purchased = mysql_result($commres, 0);
			if(!is_numeric($purchased) || $purchased <= 0) { $purchased = "0.00"; }
			$purchased = round($purchased, 2);
			if (substr($purchased, -2, 1) == ".") { $purchased .= "0"; }
		} else {
			$purchased = "0.00";
		}
		// End Get Stats
		
		if ($urow["lastlogin"] <= $urow["joindate"]) {
			$logindate = "Never";
		} else {
			$logindate = date("M j, Y", strtotime($urow["lastlogin"]));
		}
		
		// Source Tracking Info - Integrates With Profit Tracker Mod If Installed
		$sourceinfo = "";
		$sitesource = "Unknown";
		$promosource = "Unknown";
		if (file_exists("ptcron.php")) {
			// Try To Get Source
			$getsourceinfo = mysql_query("SELECT source_id, promo_id FROM `pt_users` WHERE userid='".$urow["Id"]."' LIMIT 1");
			if (mysql_num_rows($getsourceinfo) > 0) {
				$source_id = mysql_result($getsourceinfo, 0, "source_id");
				$promo_id = mysql_result($getsourceinfo, 0, "promo_id");
				
				// Get Source Site
				$getsourcesite = mysql_query("SELECT url FROM `pt_sources` WHERE id='".$source_id."' LIMIT 1");
				if (mysql_num_rows($getsourcesite) > 0) {
					$sitesource = "<a target=\"_blank\" href=\"http://".mysql_result($getsourcesite, 0, "url")."\">".mysql_result($getsourcesite, 0, "url")."</a>";
				}
				
				// Get Source Ad
				if ($promo_id == 0) {
					$promosource = "<a target=\"_blank\" href=\"".$domainurl."/index.php?rid=".$_SESSION[userid]."\">Home Page</a>";
				} elseif (isset($promo_id) && is_numeric($promo_id) && $promo_id < 90000) {
					$getsourcepromo = mysql_query("SELECT * FROM ".$prefix."promotion WHERE id='".$promo_id."' LIMIT 1");
					if (mysql_num_rows($getsourcepromo) > 0) {
						$promotype = mysql_result($getsourcepromo, 0, "type");
						if ($promotype == "splashpage") {
							$promosource = "<a target=\"_blank\" href=\"".$domainurl."/splashpage.php?splashid=".$promo_id."\">Splash Page #".$promo_id."</a>";
						} else {
							$promosource = "<a target=\"_blank\" href=\"".$domainurl."/ptviewpromo.php?promoid=".$promo_id."\">Affiliate Toolbox Item #".$promo_id."</a>";
						}
					}
				} elseif (isset($promo_id) && is_numeric($promo_id) && $promo_id < 100000) {
					$promo_id = 100000-$promo_id;
					$promosource = "<a target=\"_blank\" href=\"".$domainurl."/getimg.php?id=".$promo_id."\">Banner #".$promo_id."</a>";
				}
				
				$sourceinfo = '<br />
				<font size="2">Joined From Site: '.$sitesource.'<br />
				<font size="2">Joined From Ad: '.$promosource.'<br />';
			}
		}
		// End Source Tracking Info
		
		// Social Media Info - Integrates With Social Surfbar Mod If Installed
		$socialinfo = "";
		if (file_exists("social_branding.php")) {
			$website_url = $urow['website'];
			$fb = $urow['facebook'];
			$ti = $urow['twitter'];
			$sk = $urow['skype'];
			
			if (strlen($website_url) > 4) { $socialinfo .= "<br /><font size=\"2\"><b>&nbsp;<a target=\"_blank\" href=\"".$website_url."\">".$website_url."</a></b></font>"; }
			
			if ($fb || $ti || $sk) { $socialinfo .= "<br />"; }
			if (strlen($fb) > 0) { $socialinfo .= "&nbsp;<a href=\"http://facebook.com/".$fb."\" target=\"_BLANK\"><img src=\"images/fb.png\" border=0 width=20 height=20></a>"; }
			if (strlen($ti) > 0) { $socialinfo .= "&nbsp;<a href=\"http://twitter.com/".$ti."\" target=\"_BLANK\"><img src=\"images/ti.png\" border=0 width=20 height=20></a>"; }
			if (strlen($sk) > 0) { $socialinfo .= "&nbsp;<a href=\"skype:".$sk."?chat\" target=\"_BLANK\"><img src=\"images/sk.png\" border=0 width=20 height=20></a>"; }
		}
		// End Social Media Info
  		
  		$page_content .= '<br /><table border="1" bordercolor="black" cellpadding="0" cellspacing="0" width="500">
  			<tr><td bgcolor="#DDDDDD" height="100">
  				<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
  					<tr>
  						<td bgcolor="#DDDDDD" width="100" valign="middle" align="left"><img width="100" height="100" border="0" src="referral_photo.php?rid='.$urow["Id"].'"></td>
  						<td bgcolor="#DDDDDD" width="100%" valign="middle" align="left"><font size="3"><b>&nbsp;'.$urow["firstname"].' '.$urow["lastname"].'</b></font>'.$socialinfo.'</td>
  					</tr>
  				</table>
  			</td></tr>
  			<tr><td bgcolor="#EEEEEE">
  				<table border="0" cellpadding="5" cellspacing="0" width="100%" height="100%">
  					<tr>
    						<td bgcolor="#EEEEEE" width="400" valign="middle" align="left">
  							<font size="2">Status: <b>'.$urow["status"].'</b><br />
  							<font size="2">Membership: <b>'.$accname.'</b><br /><br />
  							<font size="2">Date Joined: <b>'.date("M j, Y", strtotime($urow["joindate"])).'</b><br />
  							<font size="2">Last Login: <b>'.$logindate.'</b><br /><br />
  							<font size="2">Pages Surfed: <b>'.$clicks.'</b><br />
  							<font size="2">Total Purchased: <b><a style="cursor:pointer;" onClick="window.open(\'referral_sales.php?ref='.$urow["Id"].'\',\'view_purchases\',\'width=700, height=500\');">$'.$purchased.'</b></a><br />
  							'.$sourceinfo.'
  						</td>
  						<td bgcolor="#EEEEEE" width="100" valign="middle" align="left">';
  						
  							if ($cantransfer == "1") {
  								$page_content .= '
	  							<form action="referral_transfer.php?rid='.$urow["Id"].'" method="post">
  								<input type="submit" value="Transfer Credits">
  								</form>
  								<br />';
  							}
  							
  							if (file_exists("pcompose.php")) {
	  							$page_content .= '
  								<form action="pcompose.php?sendto='.$urow["Id"].'" method="post">
  								<input type="submit" value="Send Message">
  								</form>';
  							}
  							
  							$page_content .= '
  						</td>
  					</tr>
  				</table>
  			</td></tr>
  		</table>
  		<br />';
  	}
  	
  	$page_content .= '<br />';
  	
  	// Output Page Numbers
	$pages = intval($refcounter/$refsperpage);
	if ($pages >= 1 && $refcounter > $refsperpage) {
		$page_content .= "<font size=\"2\"><b>Page: </b>";
		for ($i=1; $i<$pages+2; $i++) {
			$newoffset = $refsperpage*($i-1);
			if ((($i-1)*$refsperpage) < $refcounter) {
			if($newoffset == $pageoffset) {
				$page_content .= " <b>$i</b> ";
			} else {
				$page_content .= " <a href=\"members.php?mf=mr&pageoffset=".$newoffset."&sorttype=".$_GET['sorttype']."\">$i</a> ";
			}
			}
		}
		$page_content .= "</font></div></div>";
	}
	// End Output Page Numbers
	
	$page_content .= '</div></div><br />';

?>