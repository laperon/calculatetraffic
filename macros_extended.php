<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////


	
        //$res=mysql_fetch_array(@mysql_query("SELECT ".$prefix."mbanners.mtype ".$prefix."membertypes.accname FROM ".$prefix."mbanners LEFT JOIN ".$prefix."membertypes ON ".$prefix."membertypes.id = ".$prefix."membertypes.mtid WHERE id='".$row["Id"]."'") or die("Unable to find settings!"));

        $query = mysql_query("
            select 
                mt.`accname`,
                mt.`mtid`
            from
                " . $prefix . "members m
            left join
                " . $prefix . "membertypes mt on mt.`mtid` = m.`mtype`
            where
                m.`id` = '" . $row["Id"] . "'
        ") or die("Unable to find settings!");

        $res = mysql_fetch_array($query);

        $textstring = str_ireplace("#MEMBERLEVEL#", $res['accname'], $textstring);



$textstring=str_ireplace("#BANNERIMPS#", $row["bannerimps"], $textstring);
$textstring=str_ireplace("#TEXTIMPS#", $row["textimps"], $textstring);

$textstring=str_ireplace("#CLICKSTODAY#", $row["clickstoday"], $textstring);
$textstring=str_ireplace("#CREDITSTODAY#", $row["creditstoday"], $textstring);

$textstring=str_ireplace("#CLICKSYESTERDAY#", $row["clicksyesterday"], $textstring);
$textstring=str_ireplace("#CREDITSYESTERDAY#", $row["creditsyesterday"], $textstring);

if (stristr($textstring, '#NUMREFS#')) {
	$numrefs = mysql_result(mysql_query("SELECT COUNT(*) FROM ".$prefix."members WHERE refid='".$row["Id"]."'"), 0);	
	$textstring = str_ireplace("#NUMREFS#", $numrefs, $textstring);
}

if (stristr($textstring, '#HITSTODAY#')) {
	$hitstoday = mysql_result(mysql_query("SELECT SUM(hitstoday) FROM ".$prefix."msites WHERE memid='".$row["Id"]."'"),0);	
	$textstring = str_ireplace("#HITSTODAY#", $hitstoday, $textstring);
}

if (stristr($textstring, '#HITSYESTERDAY#')) {
	$hitsyesterday = mysql_result(mysql_query("SELECT SUM(hitsyesterday) FROM ".$prefix."msites WHERE memid='".$row["Id"]."'"),0);	
	$textstring = str_ireplace("#HITSYESTERDAY#", $hitsyesterday, $textstring);
}

if (stristr($textstring, '#BANNERSTODAY#')) {
	$hitstoday = mysql_result(mysql_query("SELECT SUM(hitstoday) FROM ".$prefix."mbanners WHERE memid='".$row["Id"]."'"),0);	
	$textstring = str_ireplace("#BANNERSTODAY#", $hitstoday, $textstring);
}

if (stristr($textstring, '#BANNERSYESTERDAY#')) {
	$hitsyesterday = mysql_result(mysql_query("SELECT SUM(hitsyesterday) FROM ".$prefix."mbanners WHERE memid='".$row["Id"]."'"),0);	
	$textstring = str_ireplace("#BANNERSYESTERDAY#", $hitsyesterday, $textstring);
}

if (stristr($textstring, '#TEXTSTODAY#')) {
	$hitstoday = mysql_result(mysql_query("SELECT SUM(hitstoday) FROM ".$prefix."mtexts WHERE memid='".$row["Id"]."'"),0);	
	$textstring = str_ireplace("#TEXTSTODAY#", $hitstoday, $textstring);
}

if (stristr($textstring, '#TEXTSYESTERDAY#')) {
	$hitsyesterday = mysql_result(mysql_query("SELECT SUM(hitsyesterday) FROM ".$prefix."mtexts WHERE memid='".$row["Id"]."'"),0);	
	$textstring = str_ireplace("#TEXTSYESTERDAY#", $hitsyesterday, $textstring);
}

if (stristr($textstring, '#BANNERAD#')) {
	$getdefaults = mysql_query("Select defbanimg, defbantar from `".$prefix."settings` limit 1");
	$defbanimg = mysql_result($getdefaults, 0, "defbanimg");
	$defbantar = mysql_result($getdefaults, 0, "defbantar");
	require_once "sfunctions.php";
	$textstring = str_ireplace("#BANNERAD#", showbanner($row["Id"], $defbanimg, $defbantar), $textstring);
}

if (stristr($textstring, '#TEXTAD#')) {
	$getdefaults = mysql_query("Select deftextad, deftexttar from `".$prefix."settings` limit 1");
	$deftextad = mysql_result($getdefaults, 0, "deftextad");
	$deftexttar = mysql_result($getdefaults, 0, "deftexttar");
	require_once "sfunctions.php";
	include "surfbarColors.php";
	$textstring = str_ireplace("#TEXTAD#", showtext($row["Id"], 50, $deftextad, $deftexttar, $textImpColor), $textstring);
}

?>