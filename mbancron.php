<?php

// SurfingGuard Checker v2.2
// �2011-2014 Josh Abbott, http://surfingguard.com
// LFMTE script version

// Connect To Database
require_once "inc/filter.php";
include "inc/config.php";
include "inc/funcs.php";
$sqlconnect = lfmsql_connect($dbhost,$dbuser,$dbpass);
@lfmsql_select_db($dbname) or die( "Unable to select database");
// End Connect To Database

$tename = lfmsql_result(lfmsql_query("Select value from sg_settings where name='exchange'"), 0);
$sgkey = lfmsql_result(lfmsql_query("Select value from sg_settings where name='key'"), 0);

$getbanpath = "http://sgchecker2.com/mcheckban.php?exchange=".$tename."&key=".$sgkey."&ver=".$sgver;
$urlinfo = parse_url($getbanpath);
$hostname = $urlinfo['host'];

if ($hostname == "" || gethostbyname($hostname) == $hostname) {
	//Connection Error
} else {

$port = 80;
$timeout = 10;
$headers = "GET ".$getbanpath." HTTP/1.1\r\n";
$headers .= "Host: ".$hostname."\r\n";
$headers .= "Referer: http://".$_SERVER["SERVER_NAME"]."\r\n";
$headers .= "User-Agent: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)\r\n";
$headers .= "Connection: close\r\n";
$headers .= "Accept: */*\r\n";
$headers .= "\r\n";
$mbaninfo = "";

$connectmban = fsockopen($hostname, $port, $errno, $errstr, $timeout);
if ($connectmban) {
	stream_set_timeout($connectmban, 10);
	fwrite($connectmban,$headers);
	$readruns = 0;
	while (!feof($connectmban) && !$checktimeout['timed_out'] && $readruns < 500) {
		$mbaninfo .= fread($connectmban, 10000);
		$checktimeout = stream_get_meta_data($connectmban);
		$readruns++;
	}
	fclose($connectmban);
}

if((strpos($mbaninfo, '200 OK') === false) && (strpos($mbaninfo, '302 Found') === false)) {
	//Connection Error
} else {
	$colonpos = strpos($mbaninfo, "List");
	$colonpos = $colonpos+4;
	$mbaninfo = substr($mbaninfo, $colonpos);
	
	$splitinfo = explode("\n", $mbaninfo);
	
	if (is_array($splitinfo)) {
	foreach($splitinfo AS $mbkey=>$mbval) {
		$splitentry = explode(",", $mbval);
		$banemail = trim($splitentry[0]);
		$banip = trim($splitentry[1]);
		$bantype = trim($splitentry[2]);
		
		if ($banemail != "" && $banip !="" && is_numeric($bantype)) {

			if ($bantype == 1) {
				$reason = "Chargeback or Bank Reversal";
			} elseif ($bantype == 2) {
				$reason = "Cheater";
			} elseif ($bantype == 3) {
				$reason = "Unreasonable Payment Dispute";
			} else {
				$reason = "Other - Banned Code ".$bantype;
			}

			$checkentry = lfmsql_result(lfmsql_query("Select COUNT(*) from sg_mban_list where email LIKE '".$banemail."%' or ipaddress='".$banip."'"), 0);
			if ($checkentry == 0) {
				$action = lfmsql_result(lfmsql_query("Select action from sg_mban_actions where id=".$bantype), 0);
				$mailadmin = lfmsql_result(lfmsql_query("Select mailadmin from sg_mban_actions where id=".$bantype), 0);
				
				if ($action == 1) {
				// Ban And Suspend
					
					// Ban IP
					$checkbanned = lfmsql_query("Select id from ".$prefix."banips where banned='$banip'");
					if(lfmsql_num_rows($checkbanned) == 0) {
					
						$testip = str_replace("*", "", $banip);
						$testip = str_replace(".", "", $testip);
						if (is_numeric($testip) && $testip >= 1000) {
							@lfmsql_query("Update ".$prefix."members set status='Suspended' where lastip='".$banip."' OR signupip='".$banip."'");
							@lfmsql_query("INSERT INTO ".$prefix."banips (banned) VALUES ('".$banip."')");
						}
					
					}
					// End Ban IP
					
					// Ban Email
					$checkbanned = lfmsql_query("Select id from ".$prefix."banemails where banned LIKE '".$banemail."%'");
					if(lfmsql_num_rows($checkbanned) == 0) {
					
						$testemail = strpos($banemail, "@");
						if ($testemail !== false) {
							@lfmsql_query("Update ".$prefix."members set status='Suspended' where email LIKE '".$banemail."%'");
							@lfmsql_query("INSERT INTO ".$prefix."banemails (banned) VALUES ('".$banemail."')");
						}
					
					}
					// End Ban Email
					
				// End Ban And Suspend
				}
				
				// Notify Admin
				if ($mailadmin == 1) {
					$memfound = lfmsql_result(lfmsql_query("Select COUNT(*) from ".$prefix."members where email LIKE '".$banemail."%' OR lastip='".$banip."' OR signupip='".$banip."'"), 0);
					if ($memfound > 0) {

					$adminmail = lfmsql_result(lfmsql_query("Select value from sg_settings where name='adminemail'"), 0);
				   	$emailheader = "MIME-Version: 1.0\nContent-type: text/plain; charset=iso-8859-1\nFrom: HitsConnect SurfingGuard Checker <support@surfingguard.com>\nReply-To: HitsConnect SurfingGuard Checker <support@surfingguard.com>\nX-Priority: 3\nX-Mailer: PHP 4\n";
				   	$message = "A member at your exchange has been added\nto the central banned list.\n\nEmail: ".$banemail."\nIP Address: ".$banip."\nReason: ".$reason;

				   	mail($adminmail, "Member In Central Ban", $message, $emailheader);

			   		}
				}
				// End Notify Admin
				
				@lfmsql_query("Insert into sg_mban_list (email, ipaddress, type) values ('".$banemail."', '".$banip."', '".$bantype."')");
			}
		}
	}
	}
	
	@lfmsql_query("Update sg_settings set value='".time()."' where name='lastcbmon' limit 1");

}

}

lfmsql_close($sqlconnect);
exit;

?>