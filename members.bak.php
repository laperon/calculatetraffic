<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.05
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

session_start();
include "inc/userauth.php";

include "inc/theme.php";

$_SESSION['startpage'] = 0;

// Query settings table for sitename and One Time Offer
$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
$row=@mysql_fetch_array($res);
$sitename=$row["sitename"];
$affurl=$row["affurl"];
$bannerurls=$row["bannerurls"];
$flow=$row["flow"];

$showrefmail=$row["showrefmail"];

// Check OTO settings
// There are 2 fields in the database which determine whether a member
// will see the OTO. These are 'ototype' in the settings table and 'otoview'
// in the member table
// There are 3 possible options for displaying the one time offer
// with each triggering table changes when the 'Update' button  is
// clicked by the admin. This negates the need for a cookie.

// 1 = Display offer once to new members
// This is the default setting. When this option is selected
// the 'ototype' field will be set to '1' and the offer will be shown
// if ototype=1 and otoview=0 (the default for new members)
// Once the new member has seen the offer, the otoview field
// for his member account will be set to '1'

// 2 = Display offer once to existing members
// This option will set the ototype field to '2' and the 'otoview'
// field for all existing members to 2. Once the member has seen the
// offer, his 'otoview' field will be set to '1'

// 3 = Display offer once to existing members and new members
// This option will set the ototype field to '3' and all existing
// members 'otoview' fields will be set to '0' until they have seen
// the offer. Once the offer has been displayed, their otoview field
// will be set to '1'

// Check whether member account has been disabled
$dqry="SELECT * FROM ".$prefix."members WHERE username='".$_SESSION["uid"]."'";
$dres=@mysql_query($dqry) or die("Unable to find login");
$drow=@mysql_fetch_array($dres);
$useremail=$drow["email"];
$joindate=$drow["joindate"];
$mtype=$drow["mtype"];
$lastbonus = $drow["lastbonus"];

$page_content = "";

if($drow["status"] == "Suspended")
{
   $_SESSION["uid"] = "";
   $_SESSION["password"] = "";
   if (isset($_COOKIE[session_name()])) {
      setcookie(session_name(), '', time()-42000, '/');
   }
   @session_destroy();

   load_template ($theme_dir."/header.php");

   // Get suspended page
   $bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Suspended'") or die("Unable to find Homepage Body!");
   $brow=@mysql_fetch_array($bres);
   $memberarea=translate_site_tags($brow["template_data"]);
   $memberarea=translate_user_tags($memberarea,$useremail);
   $memberarea=translate_file_tags($memberarea);
   if (function_exists('html_entity_decode'))
   {
      $page_content = html_entity_decode($memberarea);
   }
   else
   {
      $page_content = unhtmlentities($memberarea);
   }

   load_template ($theme_dir."/content.php");
   load_template ($theme_dir."/footer.php");
   exit;
}

if($_POST["Submit"] == "Login")
{

   // Find out if it is pro-only
   $mtres=@mysql_query("SELECT enabled FROM ".$prefix."membertypes WHERE mtid=1");
   $mtrow=@mysql_fetch_array($mtres);
   $free_enabled = $mtrow["enabled"];

   // Check for active account in pro-only system
   if($free_enabled == 0 && $mtype <=1)
   {
      // Allow user to log in to 'Pending' area
      load_template ($theme_dir."/header.php");

      // Get suspended page
      $bres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='News'") or die("Unable to find Homepage Body!");
      $brow=@mysql_fetch_array($bres);
      $memberarea=translate_site_tags($brow["template_data"]);
      $memberarea=translate_user_tags($memberarea,$useremail);
      $memberarea=translate_upgrade_tags($memberarea);
      if (function_exists('html_entity_decode'))
      {
         $page_content = html_entity_decode($memberarea);
      }
      else
      {
         $page_content = unhtmlentities($memberarea);
      }

      load_template ($theme_dir."/mmenu.php");
      load_template ($theme_dir."/content.php");
      load_template ($theme_dir."/footer.php");
      exit;
   }
}

 //START OTO AND SPO

 if($_POST["Submit"] == "Login") {

 $getuserinfo = mysql_query("Select mtype, joindate from ".$prefix."members where Id=".$_SESSION["userid"]." limit 1");
 $usrid = $_SESSION["userid"];
 $acctype = mysql_result($getuserinfo, 0, "mtype");

 $joindate = mysql_result($getuserinfo, 0, "joindate");
 $jointime = strtotime($joindate);
 $timediff = time()-$jointime;
 $userdays = $timediff/86400;
 $userdays = round($userdays);

 $_SESSION['altgroup'] = 0;

 //Pick A Main Group
 $checkotogroups = mysql_query("Select * from ".$prefix."oto_groups where days<=$userdays AND (acctype=$acctype OR acctype=0) AND splitmirror=0 order by days asc");

 if (mysql_num_rows($checkotogroups) > 0) {
 for ($i = 0; $i < mysql_num_rows($checkotogroups); $i++) {

 $showoto = "yes";

 $groupid = mysql_result($checkotogroups, $i, "id");

 $checkstats = mysql_query("Select id from ".$prefix."oto_stats where userid=$usrid and groupid=$groupid limit 1");

 if (mysql_num_rows($checkstats) != 0) {
 $showoto = "no";
 }

 $checkoffers = mysql_query("Select id from ".$prefix."oto_offers where groupid=$groupid limit 1");
 if (mysql_num_rows($checkoffers) == 0) {
 $showoto = "no";
 }

 if ($showoto == "yes") {
 $_SESSION['offershown'] = 1;
 header("Location: showoto.php");
 mysql_close;
 exit;
 }

 }
 }
 //Begin SPO

 $getspos = mysql_query("Select * from ".$prefix."spo where paused=0 AND (acctype=$acctype OR acctype=0) AND mdays<=$userdays order by rank asc");

 if (mysql_num_rows($getspos) > 0) {
 //Begin Checking SPOs
 for ($i = 0; $i < mysql_num_rows($getspos); $i++) {
 $id = mysql_result($getspos, $i, "id");
 $memtype = mysql_result($getspos, $i, "memtype");
 $reptype = mysql_result($getspos, $i, "reptype");
 //Check Stats
 if ($memtype == 0) {
 //Use Spostats Table
 $getstats = mysql_query("Select lastshown from ".$prefix."spostats where spoid=$id and userid=$usrid limit 1");
 if (mysql_num_rows($getstats) > 0) {
 $lastshown = mysql_result($getstats, 0, "lastshown");
 } else {
 $lastshown = 0;
 }
 } else {
 //Use User Table
 $colname = "spo".$id;
 $getstats = mysql_query("Select $colname from ".$prefix."members where Id=$usrid limit 1");
 if (mysql_num_rows($getstats) > 0) {
 $lastshown = mysql_result($getstats, 0, $colname);
 } else {
 $lastshown = 0;
 }
 }
 if ($reptype != 0) {
 //Offer Is Shown Multiple Times
 if ($reptype == 1) {
 $delaytime = 0;
 }
 elseif ($reptype == 2) {
 $delaytime = 86400;
 }
 elseif ($reptype == 3) {
 $delaytime = 604800;
 }
 elseif ($reptype == 4) {
 $delaytime = 1209600;
 }
 elseif ($reptype == 5) {
 $delaytime = 2635200;
 }
 elseif ($reptype == 6) {
 $delaytime = 5270400;
 }
 elseif ($reptype == 7) {
 $delaytime = 31536000;
 }
 $thetime = time();
 $delaycheck = $thetime - $delaytime;
 if ($lastshown <= $delaycheck) {
 header("Location: showspo.php?spoid=$id");
 mysql_close;
 exit;
 }
 } else {
 //Offer Is Only Shown Once
 if ($lastshown == 0) {
 header("Location: showspo.php?spoid=$id");
 mysql_close;
 exit;
 }
 }
 }
 }

 //Start RTO

 $gettotal = mysql_query("Select totalpercent from ".$prefix."rto where id=1 limit 1");
 if (mysql_num_rows($gettotal) == 1) {
 $totalpercent = mysql_result($gettotal, 0, "totalpercent");
 $randomnum = rand(1, 100);
 if ($randomnum <= $totalpercent) {
 $randnum = rand(0, 100);
 $getad = mysql_query("Select * from ".$prefix."rtoads where highnum>=$randnum and lownum<=$randnum limit 1");
 if (mysql_num_rows($getad) != 0) {
 $id = mysql_result($getad, 0, "id");
 @mysql_query("Update ".$prefix."rtoads set shown=shown+1 where id=$id limit 1");
 header("Location: showrto.php?rtoid=$id");
 mysql_close;
 exit;
 }
 }
 }

 //End RTO

 }

 //END OTO AND SPO
 
 
 
 //Check For Monthly Bonus
 
 $monthyear = date("m")."/".date("Y");
 
 if ($lastbonus != $monthyear) {
 
 	$checkbonus = mysql_query("Select accname, monthcrds, monthbimps, monthtimps from ".$prefix."membertypes where mtid=$mtype limit 1");
 	$accname = mysql_result($checkbonus, 0, "accname");
 	$bonuscrds = mysql_result($checkbonus, 0, "monthcrds");
 	$bonusbimps = mysql_result($checkbonus, 0, "monthbimps");
 	$bonustimps = mysql_result($checkbonus, 0, "monthtimps");
 	
 	if ($bonuscrds > 0 || $bonusbimps > 0 || $bonustimps > 0) {
 		@mysql_query("Update ".$prefix."members set credits=credits+".$bonuscrds.", bannerimps=bannerimps+".$bonusbimps.", textimps=textimps+".$bonustimps.", lastbonus='".$monthyear."' where Id=".$_SESSION["userid"]." limit 1");
 		AddLog($monthyear." ".$accname." Monthly Bonus Given To User #".$_SESSION["userid"]);
 	}
 
 }
 
 //End Check For Monthly Bonus



$html_head = <<<EOT
<script language="javascript" src="inc/jsfuncs.js"></script>
<script type="text/javascript" src="inc/ajax.js"></script>
<script type="text/javascript">
var ajax = new sack();

function validateTaf(form) {
   var e = form.elements, m = '';
   if(!e['yourname'].value) {m += '- Your Name is required.\\n';}
   if(!e['youremail'].value) {m += '- Your Email is required.\\n';}
   if(!e['friendemail'].value) {m += '- Friend Email is required.\\n';}
   if(m) {
   alert('The following error(s) occurred:\\n\\n' + m);
   return false;
   }
return true;
}
</script>
EOT;

load_template ($theme_dir."/header.php");

load_template ($theme_dir."/mmenu.php");

// Delete User Account
if(trim($_POST["Submit"]) == "Yes - Delete My Account" && $_GET["mf"] == "ydc")
{
   // Get template data from database
   $hres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE TEMPLATE_NAME='Deleted'");
   $hrow=@mysql_fetch_array($hres);
   // Decode template data
   if (function_exists('html_entity_decode'))
   {
      $dhtml=html_entity_decode($hrow["template_data"]);
   }
   else
   {
      $dhtml=unhtmlentities($hrow["template_data"]);
   }

   $dcarea=translate_site_tags($dhtml);
   $dcarea=translate_user_tags($dcarea,$useremail);
   $dcarea=translate_upgrade_tags($dcarea);
   $dcarea=translate_file_tags($dcarea);

   $page_content .= $dcarea;

   if($_SESSION[userid] > 0)
   {
      $qry="INSERT INTO ".$prefix."membersd (SELECT * FROM ".$prefix."members WHERE Id=".$_SESSION[userid].")";
      mysql_query($qry);

      mysql_query("DELETE FROM ".$prefix."members WHERE Id = ".$_SESSION[userid]);
      
      	$getsites = mysql_query("Select id from ".$prefix."msites where memid=".$_SESSION[userid]);
	for ($i = 0; $i < mysql_num_rows($getsites); $i++) {
		$siteid = mysql_result($getsites, $i, "id");
		@mysql_query("Delete from ".$prefix."reports where siteid=$siteid");
	}
	@mysql_query("Delete from ".$prefix."msites where memid=".$_SESSION[userid]);
	@mysql_query("Delete from ".$prefix."mbanners where memid=".$_SESSION[userid]);
	@mysql_query("Delete from ".$prefix."mtexts where memid=".$_SESSION[userid]);
	@mysql_query("Delete from ".$prefix."oto_stats where userid=".$_SESSION[userid]);
	@mysql_query("Delete from ".$prefix."spostats where userid=".$_SESSION[userid]);
      
   }
}

   ////////////////////////////////////////////////
   //
   // MENU - DEFAULT MEMBER AREA
   //
   ////////////////////////////////////////////////

   if(!isset($_GET["mf"]) && !isset($_GET["page"]))
   {
		include "m_memberarea.php";
   }

   ////////////////////////////////////////////////
   //
   // MENU - Member pages
   //
   ////////////////////////////////////////////////
   if(isset($_GET["page"]))
   {
		include "m_page.php";
   }

   ////////////////////////////////////////////////
   //
   // MENU OPTION - My Referrals
   //
   ////////////////////////////////////////////////

   if($_GET["mf"] == "mr")
   {
		include "m_referrals.php";
   }

   ////////////////////////////////////////////////
   //
   // MENU OPTION - Upgrades
   //
   ////////////////////////////////////////////////
   if($_GET["mf"] == "ug")
   {
   		include("upgrade.php");
   }

   ////////////////////////////////////////////////
   //
   // MENU OPTION - My Links (Promo items)
   //
   ////////////////////////////////////////////////
	if($_GET["mf"] == "li")
    {
    	include "m_links.php";
    }


   // Edit Profile
   if($_GET["mf"] == "pf" || $_GET["mf"] == "upd")
   {
        ////////////////////////////////////////////////
        //
        // MENU OPTION - My Profile
        //
        ////////////////////////////////////////////////
		
        include "m_profile.php";
        
   }

   ////////////////////////////////////////////////
   //
   // MENU OPTION - My Commissions
   //
   ////////////////////////////////////////////////

   if($_GET["mf"] == "mc")
   {
   		include "m_comm.php";
   }

   ////////////////////////////////////////////////
   //
   // MENU OPTION - My Downloads
   //
   ////////////////////////////////////////////////

   if($_GET["mf"] == "md")
   {
		include "m_downloads.php";
   }

   ////////////////////////////////////////////////
   //
   // MENU OPTION - Downline Builder
   //
   ////////////////////////////////////////////////

   if($_GET["mf"] == "dlb")
   {
		include "m_dlb.php";
   }

load_template ($theme_dir."/content.php");
load_template ($theme_dir."/footer.php");
?>