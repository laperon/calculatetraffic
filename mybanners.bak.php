<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.08
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$getduplicates = mysql_query("Select allowduplicates from ".$prefix."settings limit 1");
$allowduplicates = mysql_result($getduplicates, 0, "allowduplicates");

include "bannercheck.php";

$countcredits = mysql_query("select credits, bannerimps, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$userbanners = mysql_result($countcredits, 0, "bannerimps");
$acctype = mysql_result($countcredits, 0, "mtype");

$getaccdata = mysql_query("Select bannervalue, maxbanners, manbanners from ".$prefix."membertypes where mtid=$acctype limit 1");
$impvalue = mysql_result($getaccdata, 0, "bannervalue");
$maxbanners = mysql_result($getaccdata, 0, "maxbanners");
$manbanners = mysql_result($getaccdata, 0, "manbanners");

if ($manbanners == 0) {
	$newstate = 1;
} else {
	$newstate = 0;
}

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";
load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");

if ($_GET['addbanner'] == "yes" && $_POST['newimg'] != "" && $_POST['newtarget'] != "") {

	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mbanners where memid=$userid and img='".$_POST['newimg']."' and target='".$_POST['newtarget']."'"),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {

	$numbanners = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mbanners where memid=$userid"),0);
	if ($numbanners < $maxbanners) {
	
	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  $bannedurl = "no";
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['newimg']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }
	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['newtarget']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }

	$bannercheck = checkbanner($_POST['newimg']);
	if ($bannedurl == "yes") {
		$errormess = "This site or domain is banned.";
	} elseif ($bannercheck == 1) {
		@mysql_query("Insert into ".$prefix."mbanners (state, memid, img, target) values ($newstate, $userid, '".$_POST['newimg']."', '".$_POST['newtarget']."')");
	} elseif ($bannercheck == 3) {
		$errormess = "Your banner image URL is invalid.";
	} elseif ($bannercheck == 4) {
		$errormess = "Https image URLs are not permitted.";
	} else {
		$errormess = "Your banner image URL could not be checked.  Please make sure the URL is valid.";
	}
	
	}
	
	} else {
		$errormess = "You have already added this banner to your account.";
	}
}

if ($_GET['editbanner'] == "yes" && $_POST['editimg'] != ""&& $_POST['edittarget'] != "" && is_numeric($_GET['bannerid'])) {

	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mbanners where memid=$userid and img='".$_POST['editimg']."' and target='".$_POST['edittarget']."'"),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {

	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  $bannedurl = "no";
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['editimg']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }
	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['edittarget']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }

	$bannercheck = checkbanner($_POST['editimg']);
	
	if ($bannedurl == "yes") {
		$errormess = "This site or domain is banned.";
	} elseif ($bannercheck == 1) {
	@mysql_query("Update ".$prefix."mbanners set state=$newstate, img='".$_POST['editimg']."', target='".$_POST['edittarget']."' where id=".$_GET['bannerid']." and memid=$userid and state!=3 limit 1");
	} elseif ($bannercheck == 3) {
		$errormess = "Your banner image URL is invalid.";
	} elseif ($bannercheck == 4) {
		$errormess = "Https image URLs are not permitted.";
	} else {
		$errormess = "Your banner image URL could not be checked.  Please make sure the URL is valid.";
	}
	
	} else {
		$errormess = "You have already added this banner to your account.";
	}
}

if ($_GET['deletebanner'] == "yes" && is_numeric($_GET['bannerid'])) {
	$confirmdelete = $_GET['confirmdelete'];
	$bannerid = $_GET['bannerid'];
	if ($confirmdelete == "yes") {
		$getimps = mysql_query("Select imps from ".$prefix."mbanners where id=$bannerid and memid=$userid limit 1");
		if (mysql_num_rows($getimps) > 0) {
			$refundimps = mysql_result($getimps, 0, "imps");
			$refundcredits = $refundimps/$impvalue;
			if ($refundcredits > 0) {
				@mysql_query("Update ".$prefix."members set credits=credits+$refundcredits where Id=$userid limit 1");
				$usercredits = $usercredits+$refundcredits;
			}
			@mysql_query("Delete from ".$prefix."mbanners where id=$bannerid and memid=$userid limit 1");
		}
	} else{
		echo("<center><h4><b>Delete Banner</b></h4>
		<font size=2><b>Are you sure you want to delete this banner?</b><br><br><a href=mybanners.php?deletebanner=yes&bannerid=$bannerid&confirmdelete=yes><b>Yes</b></a><br><br><a href=mybanners.php><b>No</b></a></font><br><br>");
		include $theme_dir."/footer.php";
		exit;
	}
}

if ($_GET['resethits'] == "yes" && is_numeric($_GET['bannerid'])) {
	@mysql_query("Update ".$prefix."mbanners set hits=0, clicks=0 where id=".$_GET['bannerid']." and memid=$userid limit 1");
}

if ($_GET['stateop'] == 1 && is_numeric($_GET['bannerid'])) {
	@mysql_query("Update ".$prefix."mbanners set state=1 where id=".$_GET['bannerid']." and memid=$userid and state=2 limit 1");
	@mysql_query("Update ".$prefix."members set actbanner=1 where Id=$userid limit 1");
}

if ($_GET['stateop'] == 2 && is_numeric($_GET['bannerid'])) {
	@mysql_query("Update ".$prefix."mbanners set state=2 where id=".$_GET['bannerid']." and memid=$userid and state=1 limit 1");
}

if ($_GET['assigncredits'] == "yes") {

	$geturls = mysql_query("Select id from ".$prefix."mbanners where memid=$userid");
	
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$bannerid = mysql_result($geturls, $i, "id");
		
		$getcreditpost = 'assignamount'.$bannerid.'';
		$toassign = $_POST[$getcreditpost];
		
		if (!check_number($toassign)) {
			echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
		}
		
		$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
		$membercredits = mysql_result($countcredits, 0, "credits");
		
		if ($toassign >= 1 && $toassign <= $membercredits) {
		$usercredits = $usercredits-$toassign;
		$imptoassign = $toassign*$impvalue;
		@mysql_query("Update ".$prefix."mbanners set imps=imps+$imptoassign where memid=$userid and id=$bannerid");
		@mysql_query("Update ".$prefix."members set credits=credits-$toassign where Id=$userid");
		}
	}
	
	@mysql_query("Update ".$prefix."members set actbanner=1 where Id=$userid limit 1");

}

if ($_GET['assignimps'] == "yes") {

	$geturls = mysql_query("Select id from ".$prefix."mbanners where memid=$userid");
	
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$bannerid = mysql_result($geturls, $i, "id");
		
		$getcreditpost = 'assignimps'.$bannerid.'';
		$toassign = $_POST[$getcreditpost];
		
		if (!check_number($toassign)) {
			echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
		}
		
		$countcredits = mysql_query("select bannerimps from ".$prefix."members where Id=$userid");
		$membercredits = mysql_result($countcredits, 0, "bannerimps");
		
		if ($toassign >= 1 && $toassign <= $membercredits) {
		$userbanners = $userbanners-$toassign;
		$imptoassign = $toassign;
		@mysql_query("Update ".$prefix."mbanners set imps=imps+$imptoassign where memid=$userid and id=$bannerid");
		@mysql_query("Update ".$prefix."members set bannerimps=bannerimps-$toassign where Id=$userid");
		}
	}
	
	@mysql_query("Update ".$prefix."members set actbanner=1 where Id=$userid limit 1");

}

if ($_GET['quickassign'] == "yes") {

	$quickcrds = $_POST['quickcrds'];
	
	if (!check_number($quickcrds)) {
		echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
	}
	
	$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
	$membercredits = mysql_result($countcredits, 0, "credits");
	if (is_numeric($quickcrds) && $quickcrds >= 1 && $quickcrds <= $membercredits) {
	
		$getactive = mysql_query("Select id from ".$prefix."mbanners where memid=$userid and state=1");
		if ((mysql_num_rows($getactive) > 0) && ($quickcrds >= mysql_num_rows($getactive))) {
		
		$persite = floor($quickcrds/mysql_num_rows($getactive));
		$impspersite = floor(($quickcrds*$impvalue)/mysql_num_rows($getactive));
		
		for ($i = 0; $i < mysql_num_rows($getactive); $i++) {
		
			$updateid = mysql_result($getactive, $i, "id");
			$usercredits = $usercredits-$persite;
			@mysql_query("Update ".$prefix."mbanners set imps=imps+$impspersite where id=$updateid limit 1");
			@mysql_query("Update ".$prefix."members set credits=credits-$persite where Id=$userid limit 1");

		}
		}
		
	}
	
	@mysql_query("Update ".$prefix."members set actbanner=1 where Id=$userid limit 1");
}

####################

//Begin main page

####################

$usercredits = round($usercredits, 2);

$getimpsetting = mysql_query("Select storeimps from ".$prefix."settings limit 1");
$impsetting = mysql_result($getimpsetting, 0, "storeimps");

echo("<center><h4><b>My Banners</b></h4>");

if ($errormess != "") {
echo("<p><b><font color=red>$errormess</font></b></p>");
}

echo("<br>
<p><b>You have ".$usercredits." credits in your account.</b></p>
<p><b>1 Credit = $impvalue Views</b></p>
");

if ($impsetting == 1) {
echo("<br>
<p><b>You have ".$userbanners." banner impressions in your account.</b></p>
");
}

echo("
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=left>
<center><p><b>Quick Assign</b></p>
<form style=\"margin:0px\" action=\"mybanners.php?quickassign=yes\" method=\"post\">
<p>Evenly distribute <input type=text name=quickcrds value=0 size=3> credits to my active banners.<br><br><input type=submit value=\"Assign\"></p>
</form>
</td></tr>
</table>
<br><br>");

echo("<table border=0 cellpadding=0 cellspacing=0>
<tr><td valign=top>

<table border=1 bordercolor=black cellpadding=5 cellspacing=0>");

echo("<tr height=30><td align=center>Banner</td><td align=center>Stats</td><td align=center>Views<br>Assigned</td></tr>");

$getbanners = @mysql_query("Select * from ".$prefix."mbanners where memid=$userid order by target asc");
$numbanners = mysql_num_rows($getbanners);

for ($i = 0; $i < mysql_num_rows($getbanners); $i++) {

	$bannerid = mysql_result($getbanners, $i, "id");
	$state = mysql_result($getbanners, $i, "state");
	$imgurl = mysql_result($getbanners, $i, "img");
	$targeturl = mysql_result($getbanners, $i, "target");
	$imps = mysql_result($getbanners, $i, "imps");
	$hits = mysql_result($getbanners, $i, "hits");
	$clicks = mysql_result($getbanners, $i, "clicks");
	
	if ($hits > 0) {
		$ctr = round(($clicks/$hits)*100);
	} else {
		$ctr = 0;
	}
	
	if ($state == 0) {
		$textstate = "Pending<br>Approval";
	} elseif ($state == 1) {
		$textstate = "Active<br><a href=mybanners.php?stateop=2&bannerid=$bannerid>Pause Ad</a>";
	} elseif ($state == 2) {
		$textstate = "Paused<br><a href=mybanners.php?stateop=1&bannerid=$bannerid>Enable Ad</a>";
	} elseif ($state == 3) {
		$textstate = "Suspended";
	} else {
		@mysql_query("Update ".$prefix."mbanners set state=0 where id=$bannerid limit 1");
		$textstate = "Pending Approval";
	}
	
	echo("<form style=\"margin:0px\" action=\"mybanners.php?editbanner=yes&bannerid=$bannerid\" method=\"post\">
	<tr bgcolor=#EEEEEE height=157>
	<td align=center valign=center><a target=_blank href=\"$targeturl\"><img border=0 width=225 height=29 src=\"$imgurl\"></a><br>
	Image: <input type=text size=25 name=editimg value=\"$imgurl\"><br>Target: <input type=text size=25 name=edittarget value=\"$targeturl\"><br><input type=submit value=\"Save\"><br /><br /><center><font size=1><b>$textstate</b></font></center></td>
	
	<td align=left><font size=2>$hits Views<br>$clicks Clicks<br><br>$ctr% CTR<br><br><center><a href=mybanners.php?resethits=yes&bannerid=$bannerid>Reset</a></center></font></td>
	<td align=center><font size=2>$imps</font></td>
	</tr>
	</form>");

}

	if ($numbanners < $maxbanners) {

	echo("<form style=\"margin:0px\" action=\"mybanners.php?addbanner=yes\" method=\"post\">
	<tr bgcolor=#EEEEEE>
	<td align=left colspan=4 height=157>Image: <input type=text size=25 name=newimg value=\"http://\"><br>Target: <input type=text size=25 name=newtarget value=\"http://\"><input type=submit value=\"Add\"></td>
	</tr>
	</form>");
	
	} else {
	
	echo("<tr bgcolor=#EEEEEE><td align=left colspan=4 height=157>You must delete a banner before adding another.</td></tr>");
	
	}

echo("</table>
</td>

<td valign=top>
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=center>Assign<br>Credits</td></tr>
<form style=\"margin:0px\" action=\"mybanners.php?assigncredits=yes\" method=\"post\">");

for ($i = 0; $i < mysql_num_rows($getbanners); $i++) {
	$bannerid = mysql_result($getbanners, $i, "id");
	echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=text size=3 name=assignamount$bannerid value=0></td></tr>");
}

if (mysql_num_rows($getbanners) > 0) {
echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=submit value=Assign></td></tr>");
}

echo("</form>
</table>
</td>");

if ($impsetting == 1) {

echo("<td valign=top>
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=center>Assign<br>Imps</td></tr>
<form style=\"margin:0px\" action=\"mybanners.php?assignimps=yes\" method=\"post\">");

for ($i = 0; $i < mysql_num_rows($getbanners); $i++) {
	$bannerid = mysql_result($getbanners, $i, "id");
	echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=text size=3 name=assignimps$bannerid value=0></td></tr>");
}

if (mysql_num_rows($getbanners) > 0) {
echo("<tr bgcolor=#EEEEEE height=157><td align=center><input type=submit value=Assign></td></tr>");
}

echo("</form>
</table>
</td>");

}

echo("<td valign=top>
<table border=1 bordercolor=black cellpadding=5 cellspacing=0>
<tr height=30><td align=center>Delete<br>Banner</td></tr>");

for ($i = 0; $i < mysql_num_rows($getbanners); $i++) {
	$bannerid = mysql_result($getbanners, $i, "id");
	echo("<form style=\"margin:0px\" action=\"mybanners.php?deletebanner=yes&bannerid=$bannerid\" method=\"post\">
	<tr bgcolor=#EEEEEE height=157><td align=center><input type=submit value=\"Delete\"></td></tr>
	</form>");
}

echo("</form>
</table>
</td>");

echo("</table><br><br>");

include $theme_dir."/footer.php";

exit;

?>