<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

function getrotmenu($currentpage="trackers") {
	if (!isset($currentpage) || strlen($currentpage) < 1) { $currentpage="trackers"; }
	$rotmenu = "<br /><table border=\"0\" cellpadding=\"2\" cellspacing=\"0\">
		<tr>";
		
		$rotmenu .= createmenubutton("menu_trackers", "trackers", $currentpage);
		$rotmenu .= createmenubutton("menu_rotators", "rotators", $currentpage);
		$rotmenu .= createmenubutton("menu_stats", "trackstats", $currentpage);
		
		$showhelptab = mysql_result(mysql_query("SELECT value FROM `tracker_settings` WHERE field='showhelptab'"), 0);
		if ($showhelptab == "1") {
			$rotmenu .= createmenubutton("menu_help", "showhelp", $currentpage);
		}
		
		$rotmenu .= "
		</tr>
	</table><br />";
	return $rotmenu;
}

function createmenubutton($imgname, $pagename, $currentpage) {
	
	$buttoncode = "
	<td><a href=\"myrotators.php?rotpage=".$pagename."\"><img border=\"0\" "; if ($pagename == $currentpage) { $buttoncode .= "src=\"".$imgname."_3.png\" onmouseover=\"this.src='".$imgname."_4.png'\" onmouseout=\"this.src='".$imgname."_3.png'\""; } else { $buttoncode .= "src=\"".$imgname."_1.png\" onmouseover=\"this.src='".$imgname."_2.png'\" onmouseout=\"this.src='".$imgname."_1.png'\""; } $buttoncode .= "></a></td>";
	
	return $buttoncode;
}

$rotpage = $_GET['rotpage'];

switch ($rotpage) {
	
	case "rotators":
		include "myrotators2.php";
		break;
	
	case "trackstats":
		include "trackstats.php";
		break;
		
	case "showhelp":
		$helpurl = mysql_result(mysql_query("SELECT value FROM `tracker_settings` WHERE field='helpurl'"), 0);
		
		$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
		$useremail = mysql_result($getuserdata, 0, "email");
		$mtype = mysql_result($getuserdata, 0, "mtype");
		$joindate = mysql_result($getuserdata, 0, "joindate");
		
		include "inc/theme.php";
		load_template ($theme_dir."/header.php");
                 echo("<div class=\"wfull\">
    <div class=\"grid w960\">
        <div class=\"header-banner\">&nbsp;</div>
    </div>
</div>");
		load_template ($theme_dir."/mmenu.php");
		
		echo(getrotmenu($rotpage));
		
		echo("<center>
		<br>
		<iframe src=\"$helpurl\" width=\"550\" height=\"715\" scrolling=\"yes\" frameBorder=\"0\"></iframe>
		<br>
		");
		
		include $theme_dir."/footer.php";
		
		break;
	
	default:
		include "mytrackers.php";
	
}

?>