<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

if (!isset($userid) || !is_numeric($userid)) {
	echo("Invalid Link");
	exit;
}

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");

$usrid = $userid;

$cnx=mysql_query("select rotator_limit, rot_noframe from ".$prefix."membertypes where mtid=$acctype");
$lim=mysql_result($cnx,0,'rotator_limit');
$noframe=mysql_result($cnx,0,'rot_noframe');

$limDisp=$lim;
if (0==$lim) {
	$lim=9999999999;
	$limDisp='an unlimited number of';
}
$cnx=mysql_query("SELECT COUNT(*) FROM rotators WHERE user_id='".$userid."'") or die(mysql_error());
$num=mysql_result($cnx,0);

$checktrackers = mysql_result(mysql_query("SELECT COUNT(*) FROM tracker_urls WHERE user_id='".$userid."'"), 0);
if ($checktrackers < 2) {
	include "inc/theme.php";
	load_template ($theme_dir."/header.php");
      echo("<div class=\"wfull\">
    <div class=\"grid w960\">
        <div class=\"header-banner\">&nbsp;</div>
    </div>
</div>");
	load_template ($theme_dir."/mmenu.php");
	
	echo(getrotmenu($rotpage));
	
	echo '<div><h3>Add A Tracker</h3>';
	
	echo '<table border="0" cellpadding="2" cellspacing="0" width="500">
	<tr><td align="left">
		<p><font size="2">In order to setup a rotator, you need to have two or more sites you want to track and rotate. You can add a site by entering the URL below, and entering a name for your tracker.</font></p>
	</td></tr>
	</table>
	
	<form action="myrotators.php?rotpage=trackers&addurl=yes" method="POST">
	<table border="0" cellpadding="2" cellspacing="0">
		<tr><td align="right">Tracker Name: </td><td align="left"><input type="text" name="name" size="20" maxlength="15" /></td></tr>
		<tr><td align="right">URL: </td><td align="left"><input type="text" name="url" size="20" value="http://" /></td></tr>
		<tr><td align="center" colspan="2"><input type="hidden" name="formm" value="new" /><input type="submit" name="submit" value="Add" /></td></tr>
	</table>
	</form>';
	
	include $theme_dir."/footer.php";
	exit;
}

if (isset($_POST['rotatorid']) && is_numeric($_POST['rotatorid'])) {
	$_GET['rotatorid'] = $_POST['rotatorid'];
}

if (!isset($_GET['rotatorid']) || !is_numeric($_GET['rotatorid'])) {
	$getfirstrot = mysql_query("SELECT id FROM rotators WHERE user_id='".$userid."' ORDER BY name ASC LIMIT 1");
	if (mysql_num_rows($getfirstrot) > 0) {
		$rotatorid = mysql_result($getfirstrot, 0, "id");
	} else {
		$rotatorid = 0;
	}
} else {
	$rotatorid = $_GET['rotatorid'];
}

// Add Rotator Process
if ($_GET['addrot'] == "yes" && $_POST['submit'] == "Create Rotator" && ($num < $lim)) {
	
	if (isset($_POST['name']) && $_POST['name'] != "") {
		$_POST['name'] = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $_POST['name']));
		// Check Name Is Unique
		$nametaken = mysql_result(mysql_query("SELECT COUNT(*) FROM rotators WHERE name='".$_POST['name']."'"), 0);
		if ($nametaken == 0 && stripos(substr($_POST['name'], 0, 7), "rotator") === false) {
			// Add Rotator
			mysql_query("INSERT INTO rotators (user_id, name) VALUES ('".$userid."', '".$_POST['name']."')") or die(mysql_error());
			$rotatorid = mysql_insert_id();
		} else {
			// Name Is Taken Or Reserved
			$addroterror = "<p><font color=\"darkred\" size=\"3\"><b>That rotator name is already taken</b></font></p>";
		}
	} else {
		// Use New ID For Name
		mysql_query("INSERT INTO rotators (user_id, name) VALUES ('".$userid."', 'rotatortemp".rand(0,999)."')") or die(mysql_error());
		$rotatorid = mysql_insert_id();
		$rotname = "rotator".$rotatorid;
		mysql_query("UPDATE `rotators` SET name='".$rotname."' WHERE id='".$rotatorid."'") or die(mysql_error());
	}
}
// End Add Rotator Process

// Delete Rotator Process
if ($_GET['delrot'] == "yes" && $_POST['submit'] == "Delete Rotator") {
	// Validate Rotator Belongs To User
	$checkrotator = mysql_result(mysql_query("SELECT COUNT(*) FROM rotators WHERE user_id='".$userid."' AND id='".$rotatorid."'"), 0);
	if ($checkrotator != 1) {
		echo("Invalid Rotator");
		exit;
	}
	if ($_GET['confirmdel'] == 1) {
	
		// Delete the rotator
		mysql_query("UPDATE `tracker_trackdata` SET rotator_id='0' WHERE rotator_id='".$rotatorid."' AND user_id='".$userid."'") or die(mysql_error());
		mysql_query("DELETE FROM rotator_sites WHERE rotator_id='".$rotatorid."'") or die(mysql_error());
		mysql_query("DELETE FROM rotators WHERE user_id='".$userid."' AND id='".$rotatorid."'") or die(mysql_error());
		
		// Pick the next rotator or go to the Create Rotator form
		$getfirstrot = mysql_query("SELECT id FROM rotators WHERE user_id='".$userid."' ORDER BY name ASC LIMIT 1");
		if (mysql_num_rows($getfirstrot) > 0) {
			$rotatorid = mysql_result($getfirstrot, 0, "id");
		} else {
			$rotatorid = 0;
		}
		
		$cnx=mysql_query("SELECT COUNT(*) FROM rotators WHERE user_id='".$userid."'") or die(mysql_error());
		$num=mysql_result($cnx,0);
		
	} else {
		include "inc/theme.php";
		load_template ($theme_dir."/header.php");
		load_template ($theme_dir."/mmenu.php");
		
		echo(getrotmenu($rotpage));
		
		echo '<div><h3>Confirm Rotator Deletion</h3>';
		
		echo '<p><font size="2">Are you sure you want to delete this rotator and all its stats?</font></p>
		<form action="myrotators.php?rotpage=rotators&rotatorid='.$rotatorid.'&delrot=yes&confirmdel=1" method="post">
		<input type="submit" name="submit" value="Delete Rotator">
		</form>
		<p><font size="2"><a href="myrotators.php?rotpage=rotators&rotatorid='.$rotatorid.'">Cancel - Go Back</a></font></p>';
		
		include $theme_dir."/footer.php";
		exit;
		
	}	
}
// End Delete Rotator Process


if ($rotatorid != 0) {
	// Validate Rotator Belongs To User
	$checkrotator = mysql_result(mysql_query("SELECT COUNT(*) FROM rotators WHERE user_id='".$userid."' AND id='".$rotatorid."'"), 0);
	if ($checkrotator != 1) {
		echo("Invalid Rotator");
		exit;
	}
} else {
	// Add Rotator Form
	
	include "inc/theme.php";
	load_template ($theme_dir."/header.php");
          echo("<div class=\"wfull\">
    <div class=\"grid w960\">
        <div class=\"header-banner\">&nbsp;</div>
    </div>
</div>");
	load_template ($theme_dir."/mmenu.php");
	
	echo(getrotmenu($rotpage));
	
	echo '<div><h3>Create A New Rotator</h3>';
	
	if ($num>=$lim) {
		echo '<table border="0" cellpadding="2" cellspacing="0" width="500">
		<tr><td align="left">
		<p><font size="2">You have reached the limit of <b>'.$lim.'</b> rotator(s) your account type can have. Before adding another, you will need to delete one.</font></p>
		<p><font size="2"><b><a href="myrotators.php?rotpage=rotators">Go Back</a></b></font></p>
		</td></tr>
		</table>';
		include $theme_dir."/footer.php";
		exit;
	}
	
	if (isset($addroterror) && $addroterror != "") { echo($addroterror); }
	
	echo '<p><font size="2">Your account type allows for <b>'.$limDisp.'</b> rotator(s) to be added.</font></p>
	<p><font size="2">Please enter a name for your rotator. This will be used for the Rotator URL.</font></p>';
	
	echo '<form action="myrotators.php?rotpage=rotators&rotatorid=0&addrot=yes" method="POST"><input type="text" name="name" size="20" maxlength="50" /><br /><input type="hidden" name="formm" value="new" /><input type="submit" name="submit" value="Create Rotator" /></form></div>';
	
	include $theme_dir."/footer.php";
	exit;
	
	
}


// Update Control Graph
$update = $_GET['update'];
if ($update == "controlgraph") {
	$rotatorid = $_GET['rotatorid'];
	$getrotsites = mysql_query("SELECT id FROM rotator_sites WHERE rotator_id=$rotatorid ORDER BY id ASC");
	if (mysql_num_rows($getrotsites) > 0 && mysql_num_rows($getrotsites) <= 99) {
	
		// Update The New Bar Values
		for ($i = 0; $i < mysql_num_rows($getrotsites); $i++) {
			if (isset($_POST['graph'][$i]) && is_numeric($_POST['graph'][$i])) {		
				$newbarval = $_POST['graph'][$i];
				$barid = mysql_result($getrotsites, $i, "id");
				
				if ($newbarval <= 1) {
					$newbarval = 0;
				}
				
				if ($newbarval > 99) {
					$newbarval = 99;
				}
				
				mysql_query("UPDATE rotator_sites SET barvalue='".$newbarval."' WHERE id='".$barid."'");
			}
		}
		// End Update The New Bar Values
		
		// Disable Sites Set To 0
		mysql_query("UPDATE rotator_sites SET highnum=0, lownum=0 WHERE barvalue<1 AND rotator_id=$rotatorid");
		
		// Update The Bar Values
		$getbarvals = mysql_query("SELECT id, barvalue FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$rotatorid."'");
		if (mysql_num_rows($getbarvals) > 0) {
			$cdownrand = 100;
			$totalvarval = mysql_result(mysql_query("SELECT SUM(barvalue) FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$rotatorid."'"), 0);
			for ($i = 0; $i < mysql_num_rows($getbarvals); $i++) {
				$rotsiteid = mysql_result($getbarvals, $i, "id");
				$barvalue = mysql_result($getbarvals, $i, "barvalue");
				$barpercent = round(($barvalue/$totalvarval) * 100);
				
				$highnum = $cdownrand;
				$cdownrand = $cdownrand-$barpercent;
				$lownum = $cdownrand;
				
				mysql_query("UPDATE rotator_sites SET highnum='".$highnum."', lownum='".$lownum."' WHERE id='".$rotsiteid."'");
			}
		}
		// End Update The Bar Values
	}
}
// End Update Control Graph

// Update Smart Filter Setting
if ($_GET['checksmartfilter'] == 1) {
	@mysql_query("UPDATE `rotators` SET smartfilter='1' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
} elseif (isset($_GET['checksmartfilter']) && $_GET['checksmartfilter'] == 0) {
	@mysql_query("UPDATE `rotators` SET smartfilter='0' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
}

// Update Show Frame Setting
if ($_GET['checkshowframe'] == 1 || $noframe == 0) {
	@mysql_query("UPDATE `rotators` SET showframe='1' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
} elseif ((isset($_GET['checkshowframe']) && $_GET['checkshowframe'] == 0) && $noframe == 1) {
	@mysql_query("UPDATE `rotators` SET showframe='0' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
}

// Update Show Photo Setting
if ($_GET['checkshowphoto'] == 1) {
	@mysql_query("UPDATE `rotators` SET showphoto='1' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
} elseif (isset($_GET['checkshowphoto']) && $_GET['checkshowphoto'] == 0) {
	@mysql_query("UPDATE `rotators` SET showphoto='0' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
}

// Update Social Branding Setting
if ($_GET['checkshowsocial'] == 1) {
	@mysql_query("UPDATE `rotators` SET showsocial='1' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
} elseif (isset($_GET['checkshowsocial']) && $_GET['checkshowsocial'] == 0) {
	@mysql_query("UPDATE `rotators` SET showsocial='0' WHERE user_id='".$userid."' AND id='".$rotatorid."'");
}

####################

//Begin main page

####################

include "inc/theme.php";
load_template ($theme_dir."/header.php");
?>
<div class="wfull">
    <div class="grid w960">
        <div class="header-banner">&nbsp;</div>
    </div>
</div>
<?php
load_template ($theme_dir."/mmenu.php");
echo("<div class=c9>");
echo(getrotmenu($rotpage));

if (isset($_GET['dailystats']) && is_numeric($_GET['dailystats'])) {
	echo("<center><br>");
	include "trackdailystats.php";
	exit;
}

?>

    <link rel="stylesheet" href="graph_styles/main.css" media="screen" />
    <link rel="stylesheet" href="graph_styles/jquery.graph.css" media="screen" />
    <link rel="stylesheet" href="graph_styles/jquery.ui.css" media="screen" />
    <!--[if IE]>
    <script src="graph_scripts/html5.js"></script>
    <link rel="stylesheet" href="graph_styles/ie.css" media="screen" />
    <![endif]-->

    <script src="graph_scripts/jquery.js"></script>
    <script src="graph_scripts/jquery.ui.js"></script>
    <script src="graph_scripts/jquery.graph.js"></script>
    
    <style type="text/css">
	hr {
	border: 0;
	clear:both;
	display:block;
	width: 100%;
	background-color: #BBBBBB;
	height: 1px;
	}
    </style>

<?php

// Start Bar Graph Header

$getrotsites = mysql_query("SELECT barvalue FROM rotator_sites WHERE rotator_id='".$rotatorid."' ORDER BY id ASC");

$numofrotsites = mysql_num_rows($getrotsites);

if ($numofrotsites > 0) {

	$colorsarray = array("#ff0000", "#00ff00", "#0000ff", "yellow", "grey", "brown", "magenta", "teal", "orange", "black");
	
	// Output Graph Heading Data
	
	if ($numofrotsites <= 10) {
	
		echo("<script>
	    $(function(){
	      $('#demo-div').graph({
	        graphSize : 250,
	        inputName : 'graph',
	        barSize : 27,
	        barSpacing : 17,
	        autoBalanceShow : true,
	        autoBalance : true,
	        data : [
	        ");
	        
	} elseif ($numofrotsites <= 20) {
		
		echo("<script>
	    $(function(){
	      $('#demo-div').graph({
	        graphSize : 250,
	        inputName : 'graph',
	        barSize : 14,
	        barSpacing : 8,
	        autoBalanceShow : true,
	        autoBalance : true,
	        data : [
	        ");
		
	} elseif ($numofrotsites <= 30) {
		
		echo("<style type=\"text/css\">		
		.graphContainer{
			position:relative;
			font-family:Arial, sans-serif;
			font-size:11px;
			line-height:12px;
		}
	 	</style>
		
		<script>
	    $(function(){
	      $('#demo-div').graph({
	        graphSize : 250,
	        inputName : 'graph',
	        barSize : 9,
	        barSpacing : 5,
	        autoBalanceShow : true,
	        autoBalance : true,
	        data : [
	        ");
		
	} elseif ($numofrotsites <= 40) {
		
		echo("<style type=\"text/css\">		
		.graphContainer{
			position:relative;
			font-family:Arial, sans-serif;
			font-size:10px;
			line-height:12px;
		}
	 	</style>
		
		<script>
	    $(function(){
	      $('#demo-div').graph({
	        graphSize : 250,
	        inputName : 'graph',
	        barSize : 7,
	        barSpacing : 4,
	        autoBalanceShow : true,
	        autoBalance : true,
	        data : [
	        ");
		
	} elseif ($numofrotsites <= 50) {
		
		echo("<style type=\"text/css\">		
		.graphContainer{
			position:relative;
			font-family:Arial, sans-serif;
			font-size:7px;
			line-height:12px;
		}
	 	</style>
		
		<script>
	    $(function(){
	      $('#demo-div').graph({
	        graphSize : 250,
	        inputName : 'graph',
	        barSize : 6,
	        barSpacing : 3,
	        autoBalanceShow : true,
	        autoBalance : true,
	        data : [
	        ");
	        
	} else {
		echo("Rotators are limited to 50 URLs");
		exit;
	}
        
        // Output Bars
	$colorcounter = 0;
	for ($i = 0; $i < mysql_num_rows($getrotsites); $i++) {
		if ($i > 0) {
			echo(",
          ");
          	}
		$barvalue = mysql_result($getrotsites, $i, "barvalue");
		if (!is_numeric($barvalue) || $barvalue < 1) { $barvalue = 1; }
		echo("{
            color : '".$colorsarray[$colorcounter]."',
            value : ".$barvalue."
          }");	
		
		$colorcounter = $colorcounter + 1;
		
		if ($colorcounter >= 10) {
			$colorcounter = 0;
		}
		
	}
	// End Output Bar
	
	// Output Graph Footer Data
	echo("
        ]
      });
    })
    </script>");

}

// End Bar Graph Header

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>");

$getrotators = mysql_query("SELECT id, name FROM rotators WHERE user_id='".$userid."' ORDER BY name ASC");
if (mysql_num_rows($getrotators) > 0) {
	echo("<form action=\"myrotators.php?rotpage=rotators\" method=\"post\">
	<select name=\"rotatorid\">");
	for ($i = 0; $i < mysql_num_rows($getrotators); $i++) {
		$rotlinkid = mysql_result($getrotators, $i, "id");
		$rotlinkname = mysql_result($getrotators, $i, "name");
		echo("<option value=\"".$rotlinkid."\""); if ($rotlinkid == $rotatorid) { echo(" selected=\"selected\""); } echo(">".$rotlinkname."</option>");
	}
	echo("
	<option value=\"0\">Create New Rotator</option>
	</select>
	<input type=\"submit\" value=\"View/Manage Rotator\">
	</form>");
} else {
	echo("
	<br />
	<font size=\"3\"><b><a href=\"myrotators.php?rotpage=rotators&rotatorid=0\">Create A New Rotator</a></b></font>
	<br>");
}

echo("
<hr>
<br>
");

// List Rotator Sites

$states=array(0=>'#0f0',1=>'#f00');

$cnx = mysql_query("SELECT id, name, showframe, smartfilter, showphoto, showsocial FROM rotators WHERE id='".$rotatorid."' LIMIT 1") or die(mysql_error());
$r = mysql_fetch_assoc($cnx);
	echo "<table width=500 border=1 bordercolor=gray cellpadding=5 cellspacing=0>
	<tr><td align=center bgcolor=#EEEEEE><font size=4><b>Your Rotator URL</b></font></td></tr>
	<tr><td align=center>
	<p><font size=\"4\"><b><a target=\"_blank\" href=\"http://".$_SERVER["SERVER_NAME"]."/r/{$r['name']}\">http://".$_SERVER["SERVER_NAME"]."/r/{$r['name']}</a></b></font></p>
	</td></tr>
	</table>";
	
	echo '<br /><br /><table>';
	
	$colorsarray = array("#ff0000", "#00ff00", "#0000ff", "yellow", "grey", "brown", "magenta", "teal", "orange", "black");
	$textsarray = array("black", "black", "white", "black", "black", "white", "black", "black", "black", "white");
	$colorcounter = 0;
	
	$getrotsites = mysql_query("SELECT id, tracker_id FROM rotator_sites WHERE rotator_id='".$rotatorid."' ORDER BY id ASC");
	
	for ($i = 0; $i < mysql_num_rows($getrotsites); $i++) {
		
		$siteid = mysql_result($getrotsites, $i, "id");
		$sitenum = $i+1;
		$tracker_id = mysql_result($getrotsites, $i, "tracker_id");
		
		$getsiteurl = mysql_query("SELECT url FROM `tracker_urls` WHERE id='".$tracker_id."'");
		if (mysql_num_rows($getsiteurl) > 0) {
			$siteurl = mysql_result($getsiteurl, 0, "url");
		} else {
			$siteurl = "ERROR";
			mysql_query("DELETE FROM rotator_sites WHERE id='".$siteid."'") or die(mysql_error());
		}
		
		echo '<tr>
			<td bgcolor="'.$colorsarray[$colorcounter].'"><font color="'.$textsarray[$colorcounter].'"><b>'.$sitenum.'</b></font></td>
			<td style="border: 1px solid #0f0"><a href="'.$siteurl.'" target="_blank">'.$siteurl.'</a></td>
			</tr>';
			
		$colorcounter = $colorcounter + 1;
		
		if ($colorcounter >= 10) {
			$colorcounter = 0;
		}
		
	}
	echo '</table>
	<p><img border="0" src="btn_addsites.png" alt="Add and Edit Sites" onClick="window.open(\'/myrotatorsites.php?rotatorid='.$rotatorid.'\',\'rotator_sites\',\'scrollbars=yes, menubars=no, width=550, height=550\');"></p>
	
	<p><a href="myrotators.php?rotpage=rotators&dailystats='.$rotatorid.'"><img border="0" src="btn_dailystats.png" alt="View Daily Stats"></a></p>
	
	<form action="myrotators.php?rotpage=rotators&rotatorid='.$rotatorid.'&delrot=yes" method="post">
		<input type="hidden" name="submit" value="Delete Rotator">
		<input type="image" src="btn_delrotator.png" name="submitimg" alt="Delete Rotator" style="width:211px;height:50px;margin-left:258px;">
	</form>
	
	<br>
	<hr>
	<br>';


?>
<table width=500 border=1 bordercolor=gray cellpadding=5 cellspacing=0>
<tr><td align=center bgcolor=#EEEEEE><font size=4><b>Traffic Control Graph</b></font></td></tr>

<tr><td align=left>

	<table border=0 width=480 cellpadding=0 cellspacing=0><tr><td><p><font size=2>This graph is an easy way to control how frequently each site will be shown relative to the other sites in your rotator.  Each bar and the number beneath it correspond to one of your sites above.  If you want one of your sites to be seen more frequently than another, then simply drag the bar for that site towards the top of the graph.  Be sure to click Update to save your changes.</font></p></td></tr></table>

</td></tr>

<tr><td align=center>
<form action="myrotators.php?rotpage=rotators&rotatorid=<? echo($rotatorid); ?>&update=controlgraph" method="post">
    <div id="content">
      <div id="demo-div"></div>
    </div>
<input type="submit" value="Update"><br><br>
</form>
</td></tr>
</table><br><br>


<table width=500 border=1 bordercolor=gray cellpadding=5 cellspacing=0>
<tr><td align=center bgcolor=#EEEEEE><font size=4><b>Advanced Settings</b></font></td></tr>

<tr><td align=center>

	<?php
	
	if ($r['smartfilter'] == "1") {
		$smartfilter = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checksmartfilter=0\"><img border=\"0\" src=\"framesite_enable.jpg\"></a>";
	} else {
		$smartfilter = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checksmartfilter=1\"><img border=\"0\" src=\"framesite_disable.jpg\"></a>";
	}
	echo("<table border=0 cellpadding=2 cellspacing=0>
	<tr><td width=\"180\" align=right valign=middle>".$smartfilter."</td><td width=\"300\" align=left valign=middle><font size=\"2\"><b>Enable Smart Filter</b></font></td></tr>
	<tr><td width=\"480\" align=left colspan=2><font size=\"2\">The Smart Filter reduces the chance of a site being shown in the same site that you're promoting it at.  For example, it would help prevent a FiveHits splash page from being shown in rotation at FiveHits if you promote your rotator there.</font></td></tr>
	</table>
	<br><br>");
	
	if ($noframe == "1") {
		if ($r['showframe'] == "1") {
			$framebox = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checkshowframe=0\"><img border=\"0\" src=\"framesite_enable.jpg\"></a>";
		} else {
			$framebox = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checkshowframe=1\"><img border=\"0\" src=\"framesite_disable.jpg\"></a>";
		}
		echo("<table border=0 cellpadding=2 cellspacing=0><tr><td align=right valign=middle>".$framebox."</td><td align=left valign=middle><font size=\"2\"><b>Show Top Rotator Frame</b></font></td></tr></table>
		<br><br>");
	}
	
	if ($r['showphoto'] == "1") {
		$showphoto = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checkshowphoto=0\"><img border=\"0\" src=\"framesite_enable.jpg\"></a>";
	} else {
		$showphoto = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checkshowphoto=1\"><img border=\"0\" src=\"framesite_disable.jpg\"></a>";
	}
	echo("<table border=0 cellpadding=2 cellspacing=0>
	<tr><td align=right valign=middle>".$showphoto."</td><td align=left valign=middle><font size=\"2\"><b>Show My <a target=\"_blank\" href=\"http://gravatar.com\">Gravatar</a>/Photo In The Top Rotator Frame</b></font></td></tr>
	</table>
	<br><br>");
	
	if (file_exists("social_branding.php")) {
		if ($r['showsocial'] == "1") {
			$showsocial = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checkshowsocial=0\"><img border=\"0\" src=\"framesite_enable.jpg\"></a>";
		} else {
			$showsocial = "<a href=\"myrotators.php?rotpage=rotators&rotatorid=$rotatorid&checkshowsocial=1\"><img border=\"0\" src=\"framesite_disable.jpg\"></a>";
		}
		echo("<table border=0 cellpadding=2 cellspacing=0>
		<tr><td align=right valign=middle>".$showsocial."</td><td align=left valign=middle><font size=\"2\"><b>Show My <a target=\"_blank\" href=\"social_branding.php\">Social Branding Info</a> In The Top Rotator Frame</b></font></td></tr>
		</table>
		<br><br>");
	}
	
	?>

</td></tr>
</table></div><br><br>

<?php

include $theme_dir."/footer.php";
exit;

?>