<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");

$usrid = $userid;

if (!isset($_GET['rotatorid']) || !is_numeric($_GET['rotatorid'])) {
	echo("Invalid Rotator ID");
	exit;
} else {
	$rotatorid = $_GET['rotatorid'];
}

// Validate Rotator Belongs To User
$checkrotator = mysql_result(mysql_query("SELECT COUNT(*) FROM rotators WHERE user_id='".$userid."' AND id='".$rotatorid."'"), 0);
if ($checkrotator != 1) {
	echo("Invalid Rotator");
	exit;
}


if ($_GET['update'] == "yes") {
	
	$geturllist = mysql_query("SELECT * FROM tracker_urls WHERE user_id='".$usrid."' AND state=0 ORDER BY name") or die(mysql_error());
	while($trkurls=@mysql_fetch_object($geturllist)) {
		if (isset($_POST['includeurl'.$trkurls->id])) {
			if (mysql_result(mysql_query("SELECT COUNT(*) FROM `rotator_sites` WHERE rotator_id=".$rotatorid." AND tracker_id=".$trkurls->id), 0) == 0) {
				mysql_query("INSERT INTO `rotator_sites` (user_id, rotator_id, tracker_id, barvalue) VALUES ('".$usrid."', '".$rotatorid."', '".$trkurls->id."', '50')") or die(mysql_error());
			}
		} else {
			mysql_query("DELETE FROM `rotator_sites` WHERE rotator_id='".$rotatorid."' AND tracker_id='".$trkurls->id."'") or die(mysql_error());
		}
	}
	
	// Update The Bar Values
	$getbarvals = mysql_query("SELECT id, barvalue FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$rotatorid."'");
	if (mysql_num_rows($getbarvals) > 0) {
		$cdownrand = 100;
		$totalvarval = mysql_result(mysql_query("SELECT SUM(barvalue) FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$rotatorid."'"), 0);
		for ($i = 0; $i < mysql_num_rows($getbarvals); $i++) {
			$rotsiteid = mysql_result($getbarvals, $i, "id");
			$barvalue = mysql_result($getbarvals, $i, "barvalue");
			$barpercent = round(($barvalue/$totalvarval) * 100);
			
			$highnum = $cdownrand;
			$cdownrand = $cdownrand-$barpercent;
			$lownum = $cdownrand;
			
			mysql_query("UPDATE rotator_sites SET highnum='".$highnum."', lownum='".$lownum."' WHERE id='".$rotsiteid."'");
		}
	}
	// End Update The Bar Values
	
	echo "<script language=\"JavaScript\">";
	echo "window.opener.location.href = 'myrotators.php?rotpage=rotators&rotatorid=".$rotatorid."';";
	echo "</script>";
}

####################

//Begin main page

####################

echo("<form action=\"/myrotatorsites.php?rotatorid=".$rotatorid."&update=yes\" method=\"POST\">
");

?>

<table width=500 border=1 bordercolor=gray cellpadding=5 cellspacing=0>
<tr><td align=center colspan=2 bgcolor="#EEEEEE"><font size=4><b>Rotator Sites</b></font></td></tr>

<tr><td align=left colspan=2>

	<table border=0 width=480 cellpadding=0 cellspacing=0><tr><td><p><font size=2>You can add and remove your tracker URLs from this rotator at any time by checking and unchecking the boxes below. Be sure to click "Update Rotator" to save your settings.</font></p></td></tr></table>

</td></tr>

	<?php
	
	$geturllist = mysql_query("SELECT * FROM tracker_urls WHERE user_id='".$usrid."' AND state=0 ORDER BY name") or die(mysql_error());
	while($trkurls=@mysql_fetch_object($geturllist)) {
		echo("<tr><td align=center width=10><input name=\"includeurl".$trkurls->id."\" type=\"checkbox\""); if (mysql_result(mysql_query("SELECT COUNT(*) FROM `rotator_sites` WHERE rotator_id=".$rotatorid." AND tracker_id=".$trkurls->id), 0) >= 1) { echo(" checked=\"checked\""); } echo("></td><td align=left><b>".$trkurls->name."</b><br><b>".$trkurls->url."</b></td></tr>");
	}
	
	echo("<tr><td align=left colspan=2>
	<input type=\"submit\" value=\"Update Rotator\">
	</td></tr>
	</table>
	</form>
	");

echo("
<br><br>

</body>
</html>");

exit;

?>