<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.08
// Copyright ©2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$getduplicates = mysql_query("Select allowduplicates from ".$prefix."settings limit 1");
$allowduplicates = mysql_result($getduplicates, 0, "allowduplicates");

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";

$getaccdata = mysql_query("Select hitvalue, maxsites, mansites from ".$prefix."membertypes where mtid=$acctype limit 1");
$hitvalue = mysql_result($getaccdata, 0, "hitvalue");
$maxsites = mysql_result($getaccdata, 0, "maxsites");
$mansites = mysql_result($getaccdata, 0, "mansites");


if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

/*Advertise*/

if(isset($_POST['advertise'])) {
    $sql = mysql_query("SELECT * FROM `advertising` WHERE `email` = '{$useremail}' AND `status` = 0");
    $row = mysql_fetch_array($sql);
    $site = htmlspecialchars($row['site']);
    print $site;
    exit();
}

if(!empty($_POST['advertise_site'])) {
  $username = $_SESSION['uid'];
  $sql = mysql_query("SELECT `Id` FROM `{$prefix}members` WHERE `username` = '{$username}'");

  if($row = mysql_fetch_array($sql)) {
    if($row['Id']) {
      $site = htmlspecialchars(trim(
        $_POST['advertise_site']
      ));

      @mysql_query("UPDATE `advertising` SET `status`=true WHERE `site` = '{$site}'") or die('error sql');
    }
  }

  exit();
}



if ($_GET['addsite'] == "yes" && $_POST['newurl'] != "") {
	//echo 'test1';
	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."msites where memid=$userid and url='".$_POST['newurl']."'"),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {
		//echo 'test2';
		$numsites = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."msites where memid=$userid"),0);
		if ($numsites < $maxsites) {
			//echo 'test3';
			$checkbanned = mysql_query("Select domain from `banned_sites`");
			$bannedurl = "no";
			while ($bannedlist = mysql_fetch_array($checkbanned)) {
				if (stristr(trim($_POST['newurl']), trim($bannedlist['domain']))) {
					$bannedurl = "yes";
				}
			}

			if ($bannedurl == "no") {

				if ($_POST['sitename'] == "") { $_POST['sitename']="Site Name"; }
				if($_POST['sync'] == "N")
				{
					//echo 'nosync';
					@mysql_query("Insert into ".$prefix."msites (state, memid, sitename, url) values (0, $userid, '".$_POST['sitename']."', '".$_POST['newurl']."')");
				} else {
					//echo 'sync';
					@mysql_query("Insert into ".$prefix."msites (state, memid, sitename, url, bannerId, textId) values (0, $userid, '".$_POST['sitename']."', '".$_POST['newurl']."', '".$_POST['banner_id']."', '".$_POST['text_id']."')");

				}

				$newsiteid = mysql_insert_id();
				if ($mansites == 0) {
					//echo 'test5';
					header("Location: checksite.php?siteid=$newsiteid");
					exit;
				}

			} else {
				$errormess = "This site is banned.";
			}

		}

	} else {
		$errormess = "You have already added this site to your account.";
	}
}

if ($_GET['editsite'] == "yes" && $_POST['editurl'] != "" && is_numeric($_GET['siteid'])) {

	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."msites where memid=$userid and url='".$_POST['editurl']."' and id!=".$_GET['siteid'].""),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {

		$checkbanned = mysql_query("Select domain from `banned_sites`");
		$bannedurl = "no";
		while ($bannedlist = mysql_fetch_array($checkbanned)) {
			if (stristr(trim($_POST['editurl']), trim($bannedlist['domain']))) {
				$bannedurl = "yes";
			}
		}

		if ($bannedurl == "no") {
			if ($_POST['sitename'] == "") { $_POST['sitename']="Site Name"; }
			@mysql_query("Update ".$prefix."msites set state=0, sitename='".$_POST['sitename']."', url='".$_POST['editurl']."' where id=".$_GET['siteid']." and memid=$userid and state!=3 limit 1");

			if ($mansites == 0) {
				header("Location: checksite.php?siteid=".$_GET['siteid']);
				exit;
			}

		} else {
			$errormess = "This site is banned.";
		}

	} else {
		$errormess = "You have already added this site to your account.";
	}
}

if($_POST['updatesite'] == "yes" && !empty($_POST['siteid'])) {
	$siteid = (int)$_POST['siteid'];
	$user_id = $_SESSION['userid'];
	$sitename = htmlspecialchars(mysql_real_escape_string($_POST['sitename']));
	$editurl  = htmlspecialchars(mysql_real_escape_string($_POST['editurl']));
	$banner_id = (int)($_POST['banner_id']);
	$textads_id = (int)($_POST['text_id']);


	if($_POST['sync'] == 'N') {
		mysql_query( "UPDATE ".$prefix."msites SET sitename='" . $sitename . "', url = '" . $editurl . "',  bannerId='0' , textId='0' WHERE id='" . $siteid . "' AND memid='" . $user_id . "'" ) or die(mysql_error());
	}
	if($_POST['sync'] == 'Select') {
		mysql_query( "UPDATE ".$prefix."msites SET sitename='" . $sitename . "', url = '" . $editurl . "' WHERE id='" . $siteid . "' AND memid='" . $user_id . "'" ) or die(mysql_error());
	}
	if($_POST['sync'] == 'E') {
		mysql_query( "UPDATE ".$prefix."msites SET sitename='" . $sitename . "', url = '" . $editurl . "',  bannerId='" . $banner_id . "' , textId='" . $textads_id . "' WHERE id='" . $siteid . "' AND memid='" . $user_id . "'" ) or die(mysql_error());
	}

	die();
}

if ($_GET['deletesite'] == "yes" && is_numeric($_GET['siteid'])) {
	$confirmdelete = $_GET['confirmdelete'];
	$siteid = $_GET['siteid'];
	if ($confirmdelete == "yes") {
		$getcredits = mysql_query("Select credits from ".$prefix."msites where id=$siteid and memid=$userid limit 1");
		if (mysql_num_rows($getcredits) > 0) {
			$refundcredits = mysql_result($getcredits, 0, "credits");
			if ($refundcredits > 0) {
				@mysql_query("Update ".$prefix."members set credits=credits+$refundcredits where Id=$userid limit 1");
				$usercredits = $usercredits+$refundcredits;
			}
			@mysql_query("Delete from ".$prefix."msites where id=$siteid and memid=$userid limit 1");
		}
	} else{
		include $theme_dir."/header.php";
		?>
		<script type="text/javascript">
			function formSubmitDetails(assignId)
			{
				//alert(assignId);
				document.getElementById(assignId).submit();
			}
		</script>
		<div class="wfull">
			<div class="grid w960">
				<div class="header-banner">&nbsp;</div>
			</div>
		</div>
		<?php
		//include $theme_dir."/mmenu.php";
		echo("<center><h4><b>Delete Site</b></h4>
		<font size=2><b>Are you sure you want to delete this site?</b><br><br><a href=mysites.php?deletesite=yes&siteid=$siteid&confirmdelete=yes><b>Yes</b></a><br><br><a href=mysites.php><b>No</b></a></font><br><br>");
		include $theme_dir."/footer.php";
		exit;
	}
}

if ($_GET['resethits'] == "yes" && is_numeric($_GET['siteid'])) {
	@mysql_query("Update ".$prefix."msites set hits=0 where id=".$_GET['siteid']." and memid=$userid limit 1");
}

if ($_GET['stateop'] == 1 && is_numeric($_GET['siteid'])) {
	@mysql_query("Update ".$prefix."msites set state=1 where id=".$_GET['siteid']." and memid=$userid and state=2 limit 1");
	@mysql_query("Update ".$prefix."members set actsite=1 where Id=$userid limit 1");
}

if ($_GET['stateop'] == 2 && is_numeric($_GET['siteid'])) {
	@mysql_query("Update ".$prefix."msites set state=2 where id=".$_GET['siteid']." and memid=$userid and state=1 limit 1");
}

if ($_GET['assigncredits'] == "yes") {

	$text = $_GET['cId'];
	if(preg_match_all('/\d+/', $text, $numbers))
		$lastnum = end($numbers[0]);

	$geturls = mysql_query("Select id from ".$prefix."msites where memid=$userid AND id=$lastnum");

	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$siteid = mysql_result($geturls, $i, "id");

		$getcreditpost = 'assignamount'.$siteid.'';

		$toassign = $_POST[$getcreditpost];



		if (!check_number($toassign)) {
			echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
		}

		$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
		$membercredits = mysql_result($countcredits, 0, "credits");

		if (is_numeric($toassign) && $toassign >= 1 && $toassign <= $membercredits) {
			$usercredits = $usercredits-$toassign;
			$assignamount = $toassign*$hitvalue;
			@mysql_query("Update ".$prefix."msites set credits=credits+$assignamount where memid=$userid and id=$siteid");
			@mysql_query("Update ".$prefix."members set credits=credits-$toassign where Id=$userid");
		}
	}

	@mysql_query("Update ".$prefix."members set actsite=1 where Id=$userid limit 1");

}

if ($_GET['quickassign'] == "yes") {

	$quickcrds = $_POST['quickcrds'];

	if (!check_number($quickcrds)) {
		echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
	}

	$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
	$membercredits = mysql_result($countcredits, 0, "credits");
	if (is_numeric($quickcrds) && $quickcrds >= 1 && $quickcrds <= $membercredits) {

		$getactive = mysql_query("Select id from ".$prefix."msites where memid=$userid and state=1");
		if ((mysql_num_rows($getactive) > 0) && ($quickcrds >= mysql_num_rows($getactive))) {

			$persite = floor($quickcrds/mysql_num_rows($getactive));

			for ($i = 0; $i < mysql_num_rows($getactive); $i++) {

				$updateid = mysql_result($getactive, $i, "id");
				$usercredits = $usercredits-$persite;
				@mysql_query("Update ".$prefix."msites set credits=credits+$persite where id=$updateid limit 1");
				@mysql_query("Update ".$prefix."members set credits=credits-$persite where Id=$userid limit 1");

			}
		}

	}

	@mysql_query("Update ".$prefix."members set actsite=1 where Id=$userid limit 1");
}

if (isset($_GET['framesite']) && is_numeric($_GET['framesite']) && is_numeric($_GET['siteid'])) {
	$siteid = $_GET['siteid'];
	if ($_GET['framesite'] == 1) {
		@mysql_query("Update ".$prefix."msites set framesite=1 where memid=$userid and id=$siteid");
	} else {
		@mysql_query("Update ".$prefix."msites set framesite=0 where memid=$userid and id=$siteid");
	}
}
include("surf_prefs.php");
$adminprefs = get_admin_prefs();

####################

//Begin main page

####################

load_template ($theme_dir."/header.php");
//load_template ($theme_dir."/mmenu.php");
?>
	<div class="wfull">
		<div class="grid w960">
			<div class="header-banner">&nbsp;</div>
		</div>
	</div>

<?php
$usercredits = round($usercredits, 2);
?>
	<script type="text/javascript">

		function synchronize(val)
		{
			if(val=='Y')
			{
				var userId = '<?php echo $userid;?>';
				$.ajax
				({
					type: "POST",
					url: "/ajax.php",
					data: {func:'getBannerByCustomerId', user_id:userId},
					success: function(msg)
					{
						var html = '<br/><b>My Banner : </b><br/><div>Select Banner  '+msg+'</div>';
						//$("#test").append(html);

						$.ajax
						({
							type: "POST",
							url: "/text.php",
							data: {text:'getTextByCustomerId', user_id:userId},
							success: function(msg)
							{
								html += '<br/><b>My Text : </b><br/><div>Select Text '+msg+'</div>';
								$("#test").append(html);
							}

						});

					}
				});

			}

	if(val=='E')
	{
		var userId = '<?php echo $userid;?>';

		/*
		var bannerId = $('input[name="banner_id"]').val();
		var textId = $('input[name="text_id"]').val()
		*/

		$.ajax({
			type: "POST",
			url: '/ajax.php',
			data: {func:'getBannerByCustomerId', user_id:userId},
			success: function(msg) {
				var html = '<div class="edit_row"> <br/><b>My Banner : </b><br/><div>Select Banner  <br/>'+msg+'</div>';
				//$("#test").append(html);

				$.ajax({
					type: "POST",
					url: " /text.php",
					data: {text:'getTextByCustomerId', user_id:userId},
					success: function(msg)
					{
						html += '<br/><b>My Text : </b><br/><div>Select Text  <br/>'+msg+'</div></div>';

						$("#edit_block").html(html);
					}

				});

			}
		});

	}

			if(val=='N' || val=='Select')
			{
				$("#test").html('');
			}
		}



		function loading_show()
		{
			$('#loading').html("<img src='images/loading.gif'/>").fadeIn('fast');
		}

		/* hide loading */
		function loading_hide()
		{
			$('#loading').fadeOut('fast');
		}

		function saveSite()
		{
			$("#success").hide();
			loading_show();
			var flag = 0;
			var site_name = $("#site_name").val();
			var site_url = $("#site_url").val();
			var sync = $("#sync").val();

			if(site_name.trim()=='')
			{
				loading_hide();
				$("#site_name").addClass("error_validation");
				flag = 1;
			}
			else
			{
				$("#site_name").removeClass("error_validation");
			}

			if(site_url.trim()=='')
			{
				loading_hide();
				$("#site_url").addClass("error_validation");
				flag = 1;
			}
			else
			{
				$("#site_url").removeClass("error_validation");
			}

			if(sync.trim()=='Select')
			{
				loading_hide();
				$("#sync").addClass("error_validation");
				flag = 1;
			}
			else
			{
				/*if(sync.trim()=='Y')
				 {
				 var banner_id = $("#banner_id").val();
				 if(banner_id.trim()=='')
				 {
				 loading_hide();
				 $("#banner_id").addClass("error_validation");
				 flag = 1;
				 }
				 else
				 {
				 $("#banner_id").removeClass("error_validation");
				 }

				 var text_id = $("#text_id").val();
				 if(text_id.trim()=='')
				 {
				 loading_hide();
				 $("#text_id").addClass("error_validation");
				 flag = 1;
				 }
				 else
				 {
				 $("#text_id").removeClass("error_validation");
				 }
				 }*/


				$("#sync").removeClass("error_validation");
			}


			if(flag==0)
			{


        $.post('/mysites.php' , {advertise_site:site_url} );

        setTimeout(function(){

					$( "#form_add" ).submit();

				},2000);

			}

    }
	$(document).ready(function(){

        $.post('/mysites.php' , {advertise:true} , function(data){
          if(data.length) {
             $('input[name="newurl"]').val(data);
           }
        });

	$('input[name="edit_form"]').on('click' , function(){
		$("#dialogFormEdit").dialog();

		var edit_name = $(this).siblings('input[name="edit_name"]').val();
		var edit_site = $(this).siblings('input[name="edit_site"]').val();
		var siteid = $(this).siblings('input[name="edit_siteid"]').val();


		$('#form_edit input[name="sitename"]').val(edit_name);
		$('#form_edit input[name="editurl"]').val(edit_site);
		$('#form_edit input[name="siteid"]').val(siteid);

		$('#form_edit').on('submit', function(e){
			e.preventDefault();

			var siteid = $(this).find('input[name="siteid"]').val();
			var sitename = $(this).find('input[name="sitename"]').val();
			var editurl = $(this).find('input[name="editurl"]').val();
			var sync = $(this).find('select[name="sync_edit"]').val();
			var text_id = $(this).find('select[name="text_id"]').val();
			var banner_id = $(this).find('select[name="banner_id"]').val();

			var flag = '';

			if(!sitename.length) {
				$(this).find('input[name="sitename"]').addClass('error_validation');
			} else {
				$(this).find('input[name="sitename"]').removeClass('error_validation');
			}

			if(!editurl.length) {
				$(this).find('input[name="editurl"]').addClass('error_validation');
			} else {
				$(this).find('input[name="editurl"]').removeClass('error_validation');
			}


			if(sitename.length && editurl.length) {
				$.ajax({
					type: "POST",
					url: " /mysites.php",
					data: {
						updatesite:'yes' ,
						siteid:siteid ,
						sitename:sitename,
						editurl:editurl,
						sync: sync,
						text_id: text_id,
						banner_id: banner_id
					},
					success: function(msg)
					{
						if(!msg.length) {
							window.location.href="/mysites.php";
						} else {
							alert('Error');
						}
					}

				});
			}
		});

	});
});

</script>
<?php
echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>");

load_template ($theme_dir."/mmenu.php");

echo("<div class=c9>
<div class=banner-content>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
    <tbody><tr>
    <td align=center valign=top><p><b>My Sites</b></p>");

if ($errormess != "") {
	echo("<p><b><font color=red>$errormess</font></b></p>");
}

echo("
<p><b>You have ".$usercredits." credits in your account.</b></p>
<p><b>1 Credit = ".$hitvalue." View</b></p>");

if ($_GET['surfreturn'] == 1) {
	echo("<p><b><font color=red size=3>You must add a site below before surfing</font></b></p>");
}

echo("<div class=quick-assign>
	<p><strong>Quick Assign</strong></p>
<form style=\"margin:0px\" action=\"mysites.php?quickassign=yes\" method=\"post\">
<p>Evenly distribute  <input type=text maxlength=5 value=0 id=credit name=quickcrds /> credits to my active sites.</p>
	<p><input type=submit class=assign-btn value=Assign name=Assign></p>
	<div class=clear></div>
</form>
</div>
</td></tr>
<tr>
<td align=left valign=top class=table-structure>
<div style=margin-bottom:5%;>
        <table cellspacing=0 cellpadding=0 border=0 width=100%>
            <tbody><tr>
                <td align=left valign=top class=step-title>URL </td>");
if ($adminprefs['framesites'] == -1) { echo("<td align=center><a target=\"_blank\" href=\"framesite_info.php\">Frame<br>Site</a></td>"); }
echo ("<td align=left valign=top class=step-title>Views Received</td>
                <td align=left valign=top class=step-title>Views Assigned</td>
                <td align=left valign=top class=step-title>Assign Credits</td>
                <td align=left valign=top class=step-title>Delete Sites</td>
            </tr>");

$getsites = @mysql_query("SELECT * FROM oto_msites AS om
                       LEFT JOIN oto_mbanners AS ob ON om.bannerId = ob.id
                       LEFT JOIN oto_mtexts AS ot ON om.textId = ot.id
                       WHERE om.memid =".$userid." order by url asc");

$numsites = mysql_num_rows($getsites);


for ($i = 0; $i < mysql_num_rows($getsites); $i++) {

	$siteid = mysql_result($getsites, $i, "id");
	$state = mysql_result($getsites, $i, "state");
	$sitename = mysql_result($getsites, $i, "sitename");
	$url = mysql_result($getsites, $i, "url");
	$credits = mysql_result($getsites, $i, "credits");
	$credits = round($credits, 2);
	$hits = mysql_result($getsites, $i, "hits");
	$framesite = mysql_result($getsites, $i, "framesite");
	$imgBanner = mysql_result($getsites, $i, "img");
	$text = mysql_result($getsites, $i, "text");

	if ($framesite == 1) {
		$framesitebox = "<a href=\"mysites.php?framesite=0&siteid=$siteid\"><img border=\"0\" src=\"framesite_enable.jpg\"></a>";
	} else {
		$framesitebox = "<a href=\"mysites.php?framesite=1&siteid=$siteid\"><img border=\"0\" src=\"framesite_disable.jpg\"></a>";
	}

	if ($state == 0) {

		if ($mansites == 0) {
			$textstate = "<a href=checksite.php?siteid=$siteid>Click to Activate</a>";
		} else {
			$textstate = "Pending Approval";
		}

	} elseif ($state == 1) {
		$textstate = "Active<br><a href=mysites.php?stateop=2&siteid=$siteid>Pause URL</a>";
	} elseif ($state == 2) {
		$textstate = "Paused<br><a href=mysites.php?stateop=1&siteid=$siteid>Enable URL</a>";
	} elseif ($state == 3) {
		$textstate = "Suspended";
	} else {
		@mysql_query("Update ".$prefix."msites set state=0 where id=$siteid limit 1");
		if ($mansites == 0) {
			$textstate = "<a href=checksite.php?siteid=$siteid>Click to Activate</a>";
		} else {
			$textstate = "Pending Approval";
		}
	}



	echo("<tr>
	<td align=left valign=top class=Image>");
	if($imgBanner != '' && $text != '')
	{
		echo ("<div><img src=".$imgBanner." width=210 height=150 /></div><br/>
               <div><center><strong><a href=".$url." target=\"_blank\">$text</a></strong></center></div>");
	}
	echo ("<div>Name:<input type=text size=25 name=sitename value=\"$sitename\"></div>
	<div>Site:<input type=text size=25 name=editurl value=\"$url\"></div>
        <br /><br /><center><font size=1><b>$textstate</b></font></center></td>
	");

	if ($adminprefs['framesites'] == -1) { echo("<td align=center><font size=2>$framesitebox</font></td>"); }

	echo("<td align=center valign=top><font size=2>$hits <a href=mysites.php?resethits=yes&siteid=$siteid>Reset</a></font></td>
	<td align=center valign=top><font size=2>$credits</font></td>
       <td align=center valign=top><form style=\"margin:0px\" id=assignamount$siteid action=\"mysites.php?assigncredits=yes&cId=assignamount$siteid\" method=\"post\"><input type=text size=3 name=assignamount$siteid value=0><br/><input type=submit name=Assign value=Assign class=del-btn onclick=\"formSubmitDetails('assignamount$siteid')\"></form></td>
       <td align=center valign=top>
       <input type=button onclick=\"window.location.href='mysites.php?deletesite=yes&siteid=$siteid'\" name=Delete value=Delete class=del-btn>
       <input type=button name=edit_form value=Edit class=del-btn>
       <input type='hidden' name='edit_name' value='$sitename'>
       <input type='hidden' name='edit_site' value='$url'>
       <input type='hidden' name='edit_siteid' value='$siteid'>

	   </td>
       </tr>"
	);

}
//end of forloop

echo ("</table></div>
<div style='display:none;' id='dialogFormEdit'>

<form action=\"mysites.php\" name=\"form_edit\" id=\"form_edit\" method=\"post\">
<div class='edit_row'>
	<div class='title'>
		<b>Name:</b>
	</div>
	<div class='edit_field'>
		<input type=text size=25 name=sitename>
	</div>
</div>
<div class='edit_row'>
	<div class='title'>
		<b>Site:</b>
	</div>
	<div class='edit_field'>
		<input type=text size=25 name=editurl>
	</div>
</div>
<hr />
<div class='edit_row'><b>Synchronize :</b><select style=width:100px; name=sync_edit id=sync_edit onchange=synchronize(this.value)><option value=\"Select\">Select</option><option value=\"E\">Yes</option><option value=\"N\">No</option></select></div>
<input type='hidden' name='siteid'>
<input type='hidden' name='updatesite' value='yes'>
<div id=\"edit_block\"></div>
<div><input type=submit class=del-btn edit-save value=Save style=\"width:80px!important;margin:10px;\"></div>
</form>
</div>

</div>

<div style=margin-bottom:5%;>
                        <table width=100% border=0 cellspacing=0 cellpadding=0>
                        <tr>
                        <td class=step-title align=left valign=top>Site</td>
                        </tr>
                        
                        <tr>
                        <td valign=top class=Image>
                        <div style=margin-left:212px;>
                        
                        <div id=loading></div>");
if ($numsites < $maxsites) {

	echo("<form action=\"mysites.php?addsite=yes\" name=\"form_add\" id=\"form_add\" method=\"post\">
                        <div>Name: <input type=text value=\"\" id=\"site_name\" name=\"sitename\"></div>
                        
                        <div>Site: &nbsp; <input type=text value=\"\" id=\"site_url\" name=\"newurl\"></div>
                        
                        
                         <div>Synchronize : <select style=width:100px; name=sync id=sync onchange=synchronize(this.value)><option value=\"Select\">Select</option><option value=\"Y\">Yes</option><option value=\"N\">No</option></select></div>
                        
                        
                        
                        
                        <div id=\"test\"></div>
                        <div id=\"test\"></div>
                        
                        <div style=\"margin-right:315px;\"><input type=button class=del-btn value=Save name=Save style=\"width:80px!important;margin:10px;margin-left:125px\" onclick=\"saveSite();\"></div>
                        
                        
                        </form>");
} else {
	echo("<tr bgcolor=#EEEEEE><td align=left colspan=4 height=140>You must delete a site before adding another.</td></tr>");

}
echo ("</div>
                        
                        </td>

                        </tr>
                        
                        </table>
                        </div>
</td></tr>
</table>
</div>
</div>
<br><br>");

include $theme_dir."/footer.php";

exit;

?>