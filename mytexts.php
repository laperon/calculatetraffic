<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.09
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$getduplicates = mysql_query("Select allowduplicates from ".$prefix."settings limit 1");
$allowduplicates = mysql_result($getduplicates, 0, "allowduplicates");

$countcredits = mysql_query("select credits, textimps, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$usertexts = mysql_result($countcredits, 0, "textimps");
$acctype = mysql_result($countcredits, 0, "mtype");

$getaccdata = mysql_query("Select textvalue, maxtexts, mantexts from ".$prefix."membertypes where mtid=$acctype limit 1");
$impvalue = mysql_result($getaccdata, 0, "textvalue");
$maxtexts = mysql_result($getaccdata, 0, "maxtexts");
$mantexts = mysql_result($getaccdata, 0, "mantexts");

if ($mantexts == 0) {
	$newstate = 1;
} else {
	$newstate = 0;
}

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";
load_template ($theme_dir."/header.php");
//load_template ($theme_dir."/mmenu.php");
?>
<script type="text/javascript">
function formSubmitDetails(assignId)
{
   //alert(assignId);
   document.getElementById(assignId).submit();
}
</script>
<div class="wfull">
    <div class="grid w960">
        <div class="header-banner">&nbsp;</div>
    </div>
</div>
<?php
if ($_GET['addtext'] == "yes" && $_POST['newtext'] != "" && $_POST['newtarget'] != "") {
	
	$_POST['newtext'] = block_html_chars($_POST['newtext']);
	$_POST['newtarget'] = block_html_chars($_POST['newtarget']);

	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mtexts where memid=$userid and text='".$_POST['newtext']."' and target='".$_POST['newtarget']."'"),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {

	$numtexts = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mtexts where memid=$userid"),0);
	if ($numtexts < $maxtexts) {
	
	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  $bannedurl = "no";
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['newtarget']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }

	if ($bannedurl == "no") {
		@mysql_query("Insert into ".$prefix."mtexts (state, memid, text, target) values ($newstate, $userid, '".$_POST['newtext']."', '".$_POST['newtarget']."')");
	} else {
		$errormess = "This site is banned.";
	}
	
	}
	
	} else {
		$errormess = "You have already added this text ad to your account.";
	}
}

if ($_GET['edittext'] == "yes" && $_POST['edittext'] != ""&& $_POST['edittarget'] != "" && is_numeric($_GET['textid'])) {
	
	$_POST['edittext'] = block_html_chars($_POST['edittext']);
	$_POST['edittarget'] = block_html_chars($_POST['edittarget']);

	$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."mtexts where memid=$userid and text='".$_POST['edittext']."' and target='".$_POST['edittarget']."'"),0);
	if (($checkexisting == 0) || ($allowduplicates >= 1)) {

	  $checkbanned = mysql_query("Select domain from `banned_sites`");
	  $bannedurl = "no";
	  while ($bannedlist = mysql_fetch_array($checkbanned)) {
	  	if (stristr(trim($_POST['edittarget']), trim($bannedlist['domain']))) {
	  		$bannedurl = "yes";
	  	}
	  }
	  
	if ($bannedurl == "no") {
		@mysql_query("Update ".$prefix."mtexts set state=$newstate, text='".$_POST['edittext']."', target='".$_POST['edittarget']."' where id=".$_GET['textid']." and memid=$userid and state!=3 limit 1");
	} else {
		$errormess = "This site is banned.";
	}
	
	} else {
		$errormess = "You have already added this text ad to your account.";
	}	
}

if ($_GET['deletetext'] == "yes" && is_numeric($_GET['textid'])) {
	$confirmdelete = $_GET['confirmdelete'];
	$textid = $_GET['textid'];
	if ($confirmdelete == "yes") {
		$getimps = mysql_query("Select imps from ".$prefix."mtexts where id=$textid and memid=$userid limit 1");
		if (mysql_num_rows($getimps) > 0) {
			$refundimps = mysql_result($getimps, 0, "imps");
			$refundcredits = $refundimps/$impvalue;
			if ($refundcredits > 0) {
				@mysql_query("Update ".$prefix."members set credits=credits+$refundcredits where Id=$userid limit 1");
				$usercredits = $usercredits+$refundcredits;
			}
			@mysql_query("Delete from ".$prefix."mtexts where id=$textid and memid=$userid limit 1");
		
                        
                        }
	} else{
		echo("<center><h4><b>Delete Text Ad</b></h4>
		<font size=2><b>Are you sure you want to delete this text ad?</b><br><br><a href=mytexts.php?deletetext=yes&textid=$textid&confirmdelete=yes><b>Yes</b></a><br><br><a href=mytexts.php><b>No</b></a></font><br><br>");
		include $theme_dir."/footer.php";
		exit;
	}
}

if ($_GET['resethits'] == "yes" && is_numeric($_GET['textid'])) {
	@mysql_query("Update ".$prefix."mtexts set hits=0, clicks=0 where id=".$_GET['textid']." and memid=$userid limit 1");
}

if ($_GET['stateop'] == 1 && is_numeric($_GET['textid'])) {
	@mysql_query("Update ".$prefix."mtexts set state=1 where id=".$_GET['textid']." and memid=$userid and state=2 limit 1");
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");
}

if ($_GET['stateop'] == 2 && is_numeric($_GET['textid'])) {
	@mysql_query("Update ".$prefix."mtexts set state=2 where id=".$_GET['textid']." and memid=$userid and state=1 limit 1");
}

if ($_GET['assigncredits'] == "yes") {

      $text = $_GET['tId'];
      if(preg_match_all('/\d+/', $text, $numbers))
      $lastnum = end($numbers[0]);

	$geturls = mysql_query("Select id from ".$prefix."mtexts where memid=$userid AND id=$lastnum");
	
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$textid = mysql_result($geturls, $i, "id");
		
		$getcreditpost = 'assignamount'.$textid.'';
		$toassign = $_POST[$getcreditpost];
                
		
		if (!check_number($toassign)) {
			echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
		}
		
		$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
		$membercredits = mysql_result($countcredits, 0, "credits");
		
		if ($toassign >= 1 && $toassign <= $membercredits) {
		$usercredits = $usercredits-$toassign;
		$imptoassign = $toassign*$impvalue;
		@mysql_query("Update ".$prefix."mtexts set imps=imps+$imptoassign where memid=$userid and id=$textid");
		@mysql_query("Update ".$prefix."members set credits=credits-$toassign where Id=$userid");
		}
	}
	
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");

}

if ($_GET['assignimps'] == "yes") {

      $text = $_GET['tId'];
      if(preg_match_all('/\d+/', $text, $numbers))
      $lastnum = end($numbers[0]);

	$geturls = mysql_query("Select id from ".$prefix."mtexts where memid=$userid AND id=$lastnum");
	
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$textid = mysql_result($geturls, $i, "id");
		
		$getcreditpost = 'assignimps'.$textid.'';
		$toassign = $_POST[$getcreditpost];
		
		if (!check_number($toassign)) {
			echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
		}
		
		$countcredits = mysql_query("select textimps from ".$prefix."members where Id=$userid");
		$membercredits = mysql_result($countcredits, 0, "textimps");
		
		if ($toassign >= 1 && $toassign <= $membercredits) {
		$usertexts = $usertexts-$toassign;
		$imptoassign = $toassign;
		@mysql_query("Update ".$prefix."mtexts set imps=imps+$imptoassign where memid=$userid and id=$textid");
		@mysql_query("Update ".$prefix."members set textimps=textimps-$toassign where Id=$userid");
		}
	}
	
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");

}

if ($_GET['quickassign'] == "yes") {

	$quickcrds = $_POST['quickcrds'];
	
	if (!check_number($quickcrds)) {
		echo("<p><b>Assigned credits must be a positive whole number.</b></p>"); exit;
	}
	
	$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
	$membercredits = mysql_result($countcredits, 0, "credits");
	if (is_numeric($quickcrds) && $quickcrds >= 1 && $quickcrds <= $membercredits) {
	
		$getactive = mysql_query("Select id from ".$prefix."mtexts where memid=$userid and state=1");
		if ((mysql_num_rows($getactive) > 0) && ($quickcrds >= mysql_num_rows($getactive))) {
		
		$persite = floor($quickcrds/mysql_num_rows($getactive));
		$impspersite = floor(($quickcrds*$impvalue)/mysql_num_rows($getactive));
		
		for ($i = 0; $i < mysql_num_rows($getactive); $i++) {
		
			$updateid = mysql_result($getactive, $i, "id");
			$usercredits = $usercredits-$persite;
			@mysql_query("Update ".$prefix."mtexts set imps=imps+$impspersite where id=$updateid limit 1");
			@mysql_query("Update ".$prefix."members set credits=credits-$persite where Id=$userid limit 1");

		}
		}
		
	}
	
	@mysql_query("Update ".$prefix."members set acttext=1 where Id=$userid limit 1");
}

####################

//Begin main page

####################

$usercredits = round($usercredits, 2);

$getimpsetting = mysql_query("Select storeimps from ".$prefix."settings limit 1");
$impsetting = mysql_result($getimpsetting, 0, "storeimps");

load_template ($theme_dir."/mmenu.php");

echo("<div class=c9>
<div>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
    <tbody><tr>
    <td align=center valign=top><h4><b>My Texts Ads</b></h4>");

if ($errormess != "") {
echo("<p><b><font color=red>$errormess</font></b></p>");
}

echo("
<p><b>You have ".$usercredits." credits in your account.</b></p>
<p><b>1 Credit = $impvalue Views</b></p>
");

if ($impsetting == 1) {
echo("
<p><b>You have ".$usertexts." text impressions in your account.</b></p>
");
}

echo("
<div class=quick-assign>
<form style=\"margin:0px\" action=\"mytexts.php?quickassign=yes\" method=\"post\">
<p><strong>Quick Assign</strong></p>
<p>Evenly distribute <input type=text name=quickcrds value=0 size=3> credits to my active text ads.</p>
<p><input type=submit class=assign-btn name=\"Assign\" value=\"Assign\"></p>
</form>
<div class=clear></div>
</div>
<br><br>");

echo("<tr><td class=table-structure align=left valign=top>
<div style=margin-bottom:5%;>");
$gettexts = @mysql_query("Select * from ".$prefix."mtexts where memid=$userid order by target asc");
$numtexts = mysql_num_rows($gettexts);

if($numtexts > 0)
{
  echo("<table width=100% border=0 cellspacing=0 cellpadding=0>
  <tr>
    <td class=step-title align=left valign=top>Text Ads </td>
    <td class=step-title align=left valign=top>Stats</td>
    <td class=step-title align=left valign=top>Views Assigned</td>
    <td class=step-title  align=left valign=top>Assign Credits</td>
	 <td class=step-title align=left valign=top>Assign Imps</td>
	 <td class=step-title align=left valign=top>Delete Ads</td>
  </tr>");

  for ($i = 0; $i < mysql_num_rows($gettexts); $i++) {

	$textid = mysql_result($gettexts, $i, "id");
	$state = mysql_result($gettexts, $i, "state");
	$textcontent = mysql_result($gettexts, $i, "text");
        //echo $textcontent."<br/>";
	$targeturl = mysql_result($gettexts, $i, "target");
	$imps = mysql_result($gettexts, $i, "imps");
	$hits = mysql_result($gettexts, $i, "hits");
	$clicks = mysql_result($gettexts, $i, "clicks");
	
	if ($hits > 0) {
		$ctr = round(($clicks/$hits)*100);
	} else {
		$ctr = 0;
	}
	
	if ($state == 0) {
		$textstate = "Pending<br>Approval";
	} elseif ($state == 1) {
		$textstate = "Active<br><a href=mytexts.php?stateop=2&textid=$textid>Pause Ad</a>";
	} elseif ($state == 2) {
		$textstate = "Paused<br><a href=mytexts.php?stateop=1&textid=$textid>Enable Ad</a>";
	} elseif ($state == 3) {
		$textstate = "Suspended";
	} else {
		@mysql_query("Update ".$prefix."mtexts set state=0 where id=$textid limit 1");
		$textstate = "Pending Approval";
	}

     echo("<tr><td align=left valign=top class=Image>
          <div style=padding-left:10px;>
            <center><strong><a style=font-weight:bolder; href=".$targeturl." target=\"_blank\">$textcontent</a></strong></center>
          </div>
          <form style=\"margin:0px\" action=\"mytexts.php?edittext=yes&textid=$textid\" method=\"post\">
          <div>Text: <input type=text size=25 value=".str_replace(" ","&nbsp;",($textcontent))." name=edittext></div>
          <div>Target:<input type=text size=25 value=".str_replace(" ","&nbsp;",($targeturl))." name=edittarget></div>
          <div style=\"margin-left:-87px;\">
            <input type=submit class=del-btn value=Save name=Save style=\"width:80px!important;margin:10px 30%;\" />
          </div></form><br/><br/><br/>
          <font size=1><b>$textstate</b></font>
            </td>

        <td align=center valign=top><table width=100% border=0 cellspacing=0 cellpadding=0>
        <tr>
         <td align=center valign=top style=border:none;><p>$hits Views</p><br/>
             <p>$clicks Clicks</p><br/>
             <p>$ctr% CTR</p></td>
        </tr>
        <tr>
        <td align=center valign=top style=border:none;><a href=mytexts.php?resethits=yes&textid=$textid>Reset</a></td>
        </tr></table></td>
        
        <td align=center valign=top><p>$imps</p></td>
        
         <td align=center valign=top><form style=\"margin:0px\" name=assignamount$textid id=assignamount$textid action=\"mytexts.php?assigncredits=yes&tId=assignamount$textid\" method=\"post\"><input type=text maxlength=5 name=assignamount$textid value=\"0\"><input type=button value=Assign class=del-btn onclick=\"formSubmitDetails('assignamount$textid')\"></form></td>
         <td align=center valign=top><form style=\"margin:0px\" name=assignimps$textid id=assignimps$textid action=\"mytexts.php?assignimps=yes&tId=assignimps$textid\" method=\"post\"><input type=text maxlength=5 value=\"0\" name=assignimps$textid><input type=button value=Assign class=del-btn onclick=\"formSubmitDetails('assignimps$textid')\"></form></td>
         <td align=center valign=top><input type=button class=del-btn value=Delete onclick=\"window.location.href='mytexts.php?deletetext=yes&textid=$textid'\" name=Delete></td></tr>");
   }
     echo("</tr></table></div>");

}
?>
<div style="margin-bottom:5%;margin-top:5%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td class="step-title" align="left" valign="top">Text Ads </td>
                        </tr>
                        
                        <tr>
                        <td valign="top" class="Image">
                        <div style="text-align: center;">
                        
                        <div id="loading"></div>

<?php
if ($numtexts < $maxtexts) {
	echo("<form style=\"margin:0px\" id=\"form_add\" action=\"mytexts.php?addtext=yes\" method=\"post\">
	<div>Text:<input type=text size=25 maxlength=30 name=newtext id=text value=\"\"></div>
        <div>Target:<input type=text size=25 name=newtarget id=target_url value=\"\"></div>
        <div><input type=submit class=del-btn value=Save name=Save style=\"width:80px!important;margin:10px 30%;\"></div>
	</form>");
	
	} else {
	
	echo("<tr bgcolor=#EEEEEE><td align=left colspan=4 height=157>You must delete a text ad before adding another.</td></tr>");
	
	}
        

echo("</div></td></tr></table></div></table></div></td></tr></td></tr></table></div></div>");

include $theme_dir."/footer.php";

exit;

?>