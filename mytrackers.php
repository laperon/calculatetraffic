<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

if (!isset($userid) || !is_numeric($userid)) {
	echo("Invalid Link");
	exit;
}

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");

$cnx = mysql_query("select tracker_limit, rot_convtrk, rotator_approve, rot_sr_enable from ".$prefix."membertypes where mtid=$acctype");
$lim = mysql_result($cnx,0,'tracker_limit');
$convtrk = mysql_result($cnx,0,'rot_convtrk');
$app = mysql_result($cnx,0,'rotator_approve');
$accsourceranks = mysql_result($cnx,0,'rot_sr_enable');

if ($lim < 1 || $lim > 50) {
	$lim = 50;
}

$limDisp=$lim;

$cnx=mysql_query("SELECT COUNT(*) FROM tracker_urls WHERE user_id='".$userid."'") or die(mysql_error());
$num=mysql_result($cnx,0);

// Add A New Tracker URL
if ($_GET['addurl'] == "yes" && ($num < $lim)) {
	
	if (isset($_POST['name']) && $_POST['name'] != "") {
		$_POST['name'] = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $_POST['name']));
		// Check Name Is Unique
		$nametaken = mysql_result(mysql_query("SELECT COUNT(*) FROM tracker_urls WHERE name='".$_POST['name']."'"), 0);
		if ($nametaken == 0 && stripos(substr($_POST['name'], 0, 4), "site") === false) {
			
			$url = trim($_POST['url']);
			
			// Make Sure It's A Valid URL
			$testurl = str_ireplace("http://", "", $url);
			$testurl = str_ireplace("www.", "", $testurl);
			
			if ($url == '' || strpos($url, 'http://') === false || strpos($testurl, '.') === false || strlen($testurl) < 3) {
				$addurlerror = "<p><font color=\"darkred\" size=\"3\"><b>Invalid URL</b></font></p>";
			} else {
				//Check Banned Site List
				$parsesite=parse_url($url);
				$tsite='http://'.$parsesite['host'];
				$bsites=mysql_query('select domain from banned_sites where domain like "'.$tsite.$parsesite['path'].'" or domain like "'.$tsite.'" or domain like "http://'.$tsite.$parsesite['path'].'" or domain like "http://'.$tsite.'" limit 1');
				
				if (mysql_num_rows($bsites) > 0) {
					$addurlerror = "<p><font color=\"darkred\" size=\"3\"><b>Site Is Banned</b></font></p>";
				} else {
					// Add Tracker
					mysql_query("INSERT INTO tracker_urls (user_id, name, state, url) VALUES ('".$userid."', '".$_POST['name']."', '1', '".$url."')") or die(mysql_error());
					$newsiteid = mysql_insert_id();
					
					if ($app == 0) {
						header("Location: checktracker.php?siteid=".$newsiteid);
						exit;
					}
					
				}
			}
		} else {
			// Name Is Taken Or Reserved
			$addurlerror = "<p><font color=\"darkred\" size=\"3\"><b>That tracker name is already taken</b></font></p>";
		}
		
	} else {
		// Use New ID For Name
		
		$url = trim($_POST['url']);
		
		// Make Sure It's A Valid URL
		$testurl = str_ireplace("http://", "", $url);
		$testurl = str_ireplace("www.", "", $testurl);
		
		if ($url == '' || strpos($url, 'http://') === false || strpos($testurl, '.') === false || strlen($testurl) < 3) {
			$addurlerror = "<p><font color=\"darkred\" size=\"3\"><b>Invalid URL</b></font></p>";
		} else {
			//Check Banned Site List
			$parsesite=parse_url($url);
			$tsite='http://'.$parsesite['host'];
			$bsites=mysql_query('select domain from banned_sites where domain like "'.$tsite.$parsesite['path'].'" or domain like "'.$tsite.'" or domain like "http://'.$tsite.$parsesite['path'].'" or domain like "http://'.$tsite.'" limit 1');
			
			if (mysql_num_rows($bsites) > 0) {
				$addurlerror = "<p><font color=\"darkred\" size=\"3\"><b>Site Is Banned</b></font></p>";
			} else {
				// Add Rotator
				mysql_query("INSERT INTO tracker_urls (user_id, name, state, url) VALUES ('".$userid."', 'sitetemp".rand(0,999)."', '1', '".$url."')") or die(mysql_error());
				$newsiteid = mysql_insert_id();
				$trackername = "site".$newsiteid;
				mysql_query("UPDATE `tracker_urls` SET name='".$trackername."' WHERE id='".$newsiteid."'") or die(mysql_error());
				
				if ($app == 0) {
					header("Location: checktracker.php?siteid=".$newsiteid);
					exit;
				}
				
			}
		}
		
	}
}

// Edit A Tracker URL
if ($_GET['editsite'] == "yes" && isset($_GET['siteid']) && is_numeric($_GET['siteid'])) {
	$getoldurl = mysql_query("SELECT url FROM tracker_urls WHERE id='".$_GET['siteid']."' AND user_id='".$userid."'") or die(mysql_error());
	if (mysql_num_rows($getoldurl) > 0) {
		$oldurl = mysql_result($getoldurl, 0, "url");
		
		$newurl = trim($_POST['siteurl']);
		// Make Sure It's A Valid URL
		$testurl = str_ireplace("http://", "", $newurl);
		$testurl = str_ireplace("www.", "", $testurl);
		
		if ($newurl == '' || strpos($newurl, 'http://') === false || strpos($testurl, '.') === false || strlen($testurl) < 3) {
			$addurlerror = "<p><font color=\"darkred\" size=\"3\"><b>Invalid URL</b></font></p>";
		} elseif ($_POST['siteurl'] != $oldurl) {
			mysql_query("UPDATE `tracker_urls` SET state='1', url='".$_POST['siteurl']."' WHERE id='".$_GET['siteid']."' AND user_id='".$userid."'") or die(mysql_error());
			header("Location: checktracker.php?siteid=".$_GET['siteid']);
			exit;
		}
	} else {
		$addurlerror = "<p><font color=\"darkred\" size=\"3\"><b>Invalid URL ID</b></font></p>";
	}
}

// Delete A Tracker URL
if (isset($_POST['formm'])) {
	switch ($_POST['formm']) {
		case 'del':
			if (isset($_POST['siteid']) and $_POST['siteid'] > 0 and is_numeric($_POST['siteid'])) {
				
				// Make Sure The Tracker Is Valid
				$checkvalid = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_urls` WHERE user_id='".$userid."' AND id='".$_POST['siteid']."'"), 0);
				if ($checkvalid != 1) {
					echo("Invalid Tracker");
					exit;
				}
				
				if ($_GET['confirmdel'] == 1) {
					
					// Delete The Tracker
					mysql_query("DELETE FROM `tracker_urls` WHERE user_id='".$userid."' AND id='".$_POST['siteid']."'") or die(mysql_error());
					
					// Delete The Stats
					mysql_query("DELETE FROM `tracker_trackdata` WHERE user_id='".$userid."' AND tracker_id='".$_POST['siteid']."'") or die(mysql_error());
					mysql_query("DELETE FROM `tracker_rates` WHERE tracker_id='".$_POST['siteid']."'") or die(mysql_error());
					
					// Get Rotators To Update
					$getrotators = mysql_query("SELECT DISTINCT(rotator_id) FROM rotator_sites WHERE user_id='".$userid."' AND tracker_id='".$_POST['siteid']."'") or die(mysql_error());
					
					// Delete The URL From Any Rotators
					mysql_query("DELETE FROM rotator_sites WHERE user_id='".$userid."' AND tracker_id='".$_POST['siteid']."'") or die(mysql_error());
					
					// Update The Rotators
					if (mysql_num_rows($getrotators) > 0) {
						for ($j = 0; $j < mysql_num_rows($getrotators); $j++) {
						
							$updaterotid = mysql_result($getrotators, $j, "rotator_id");
							
							// Update The Bar Values
							$getbarvals = mysql_query("SELECT id, barvalue FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$updaterotid."'");
							if (mysql_num_rows($getbarvals) > 0) {
								$cdownrand = 100;
								$totalvarval = mysql_result(mysql_query("SELECT SUM(barvalue) FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$updaterotid."'"), 0);
								for ($i = 0; $i < mysql_num_rows($getbarvals); $i++) {
									$rotsiteid = mysql_result($getbarvals, $i, "id");
									$barvalue = mysql_result($getbarvals, $i, "barvalue");
									$barpercent = round(($barvalue/$totalvarval) * 100);
										
									$highnum = $cdownrand;
									$cdownrand = $cdownrand-$barpercent;
									$lownum = $cdownrand;
									
									mysql_query("UPDATE rotator_sites SET highnum='".$highnum."', lownum='".$lownum."' WHERE id='".$rotsiteid."'");
								}
							}
							// End Update The Bar Values
							
						}
					}
					// End Update The Rotators
					
					$cnx=mysql_query("SELECT COUNT(*) FROM tracker_urls WHERE user_id='".$userid."'") or die(mysql_error());
					$num=mysql_result($cnx,0);
					
				} else {
					include "inc/theme.php";
					load_template ($theme_dir."/header.php");
					load_template ($theme_dir."/mmenu.php");
					
					echo(getrotmenu($rotpage));
					
					echo '<div><h3>Confirm Tracker Deletion</h3>';
					
					echo '<p><font size="2">Are you sure you want to delete this tracker and all its stats?</font></p>
					<form action="myrotators.php?rotpage=trackers&confirmdel=1" method="post">
					<input type="hidden" name="formm" value="del" />
					<input type="hidden" name="siteid" value="'.$_POST['siteid'].'" />
					<input type="submit" name="submit" value="Delete Tracker">
					</form>
					<p><font size="2"><a href="myrotators.php?rotpage=trackers">Cancel - Go Back</a></font></p>';
					
					include $theme_dir."/footer.php";
					exit;				
				}
				
			}
			
			break;
	}
}

if (isset($_GET['sourceranks']) && is_numeric($_GET['sourceranks']) && is_numeric($_GET['siteid'])) {

	$siteid = $_GET['siteid'];

	if ($_GET['sourceranks'] == 1) {
		@mysql_query("UPDATE `tracker_urls` SET sourceranks='1' WHERE user_id='".$userid."' AND id='".$siteid."'");
	} else {
		@mysql_query("UPDATE `tracker_urls` SET sourceranks='0' WHERE user_id='".$userid."' AND id='".$siteid."'");
	}
}

####################

//Begin main page

####################

include "inc/theme.php";
load_template ($theme_dir."/header.php");
?>
<div class="wfull">
    <div class="grid w960">
        <div class="header-banner">&nbsp;</div>
    </div>
</div>
<?php
load_template ($theme_dir."/mmenu.php");

echo(getrotmenu($rotpage));

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>

<center>
<br>
");

if (isset($_GET['dailystats']) && is_numeric($_GET['dailystats'])) {
	include "trackdailystats.php";
	exit;
}

if (isset($addurlerror) && $addurlerror != "") { echo($addurlerror); }

echo "<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"500\">
<tr><td align=\"left\">
<p><font size=\"2\">Your trackers are the sites you want to track and add to your rotators.  To add a tracker, just enter the URL for your site and enter a name for your tracker.</font></p><p><font size=\"2\">Your account type allows for <b>$limDisp</b> trackers to be added";
if ($app) {
	echo ' and each url will need to be approved by the Admin before you can use that tracker';
}
echo ".</font></p>";

if ($num>=$lim) {
	echo "<p>You have reached the limit of $lim trackers your account type can have. Before adding another, you will need to delete one.</p>";
}

echo "
</td></tr>
</table>
";

?>

<table border=1 bordercolor=black cellpadding=5 cellspacing=0>

<tr height=30>
	<td align=center bgcolor="#EEEEEE"><font size="1">Site URL</font></td>
	<td align=center bgcolor="#EEEEEE"><font size="1">Tracker URL</font></td>
	<? if ($accsourceranks == 1) { echo("<td align=center bgcolor=\"#EEEEEE\"><font size=\"1\">Source<br>Ranks</font></td>"); } ?>
	<td align=center bgcolor="#EEEEEE"><font size="1">Tracking</font></td>
	<td align=center bgcolor="#EEEEEE"><font size="1">Delete<br>Tracker</font></td>
</tr>

<?


// List Tracker URLs

	$getrotsites = mysql_query("SELECT id, name, sourceranks, url, state FROM tracker_urls WHERE user_id='".$userid."'");
	
	for ($i = 0; $i < mysql_num_rows($getrotsites); $i++) {
		
		$siteid = mysql_result($getrotsites, $i, "id");
		$sitenum = $i+1;
		$trackername = mysql_result($getrotsites, $i, "name");
		$sourceranks = mysql_result($getrotsites, $i, "sourceranks");
		$siteurl = mysql_result($getrotsites, $i, "url");
		$sitestate = mysql_result($getrotsites, $i, "state");
		
		if ($sitestate == 0) {
			$statetext = 'Active';
		} else {
			if ($app == 0) {
				$statetext = '<a href="checktracker.php?siteid='.$siteid.'">Click To Verify</a>';
			} else {
				$statetext = 'Pending Admin Approval';
			}
		}
		
		if ($sourceranks == 1) {
			$sourceranksbox = "<a href=\"myrotators.php?rotpage=trackers&sourceranks=0&siteid=$siteid\"><img border=\"0\" src=\"framesite_enable.jpg\"></a>";
		} else {
			$sourceranksbox = "<a href=\"myrotators.php?rotpage=trackers&sourceranks=1&siteid=$siteid\"><img border=\"0\" src=\"framesite_disable.jpg\"></a>";
		}
		
		echo '<tr>
			<td align="left"><form style="margin:0px" action="myrotators.php?rotpage=trackers&editsite=yes&siteid='.$siteid.'" method="post"><input type="text" size="25" name="siteurl" value="'.$siteurl.'"><br><input type="submit" value="Edit"></form><br><center><font size="1"><b>'.$statetext.'</b></font></center></td>
			<td align="center"><font size="1"><a target="_blank" href="http://'.$_SERVER["SERVER_NAME"].'/t/'.$trackername.'">http://'.$_SERVER["SERVER_NAME"].'/t/'.$trackername.'</a></font></td>
			';
			
			if ($accsourceranks == 1) { echo '<td align="center">'.$sourceranksbox.'</td>'; }
			
			echo '
			<td align="center"><font size="1"><a href="myrotators.php?rotpage=trackers&dailystats='.$siteid.'">Daily<br>Stats</a>';
			
			if ($convtrk == 1) { echo '<br><br><span style="cursor:pointer; color:blue;" onmouseover="style.textDecoration=\'underline\'" onmouseout="style.textDecoration=\'none\'" onClick="window.open(\'/trackerconversions.php?trackerid='.$siteid.'\',\'conversion_tracking\',\'scrollbars=yes, menubars=no, width=550, height=550\');"><font size="1">Conversion<br>Tracking</font></span>'; }
			
			echo '</td>
			
			<td align="center"><form action="myrotators.php?rotpage=trackers" method="POST"><input type="hidden" name="formm" value="del" /><input type="hidden" name="siteid" value="'.$siteid.'" /><input type="submit" name="submit" value="X" /></form></td>
			</tr>';		
	}
	
	if ($num < $lim) {
		echo '<tr>
		<td colspan="5">
			<form action="myrotators.php?rotpage=trackers&addurl=yes" method="POST">
			<table border="0" cellpadding="2" cellspacing="0">
				<tr><td align="right">Tracker Name: </td><td align="left"><input type="text" name="name" size="20" maxlength="15" /></td></tr>
				<tr><td align="right">URL: </td><td align="left"><input type="text" name="url" size="20" value="http://" /></td></tr>
				<tr><td align="center" colspan="2"><input type="hidden" name="formm" value="new" /><input type="submit" name="submit" value="Add" /></td></tr>
			</table>
			</form>
		</td>
		</tr>';
	}
	
	echo '</table>';

include $theme_dir."/footer.php";
exit;

?>