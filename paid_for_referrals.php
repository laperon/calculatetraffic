<?php
session_start();
$userid = $_SESSION["userid"];

include "inc/userauth.php";
include "inc/theme.php";
####################

//Begin main page

####################

load_template ($theme_dir."/header.php");
echo("<div class=wfull>
    <div class=\"grid w960\">
        <div class=header-banner>&nbsp;</div>
    </div>
</div>");
load_template ($theme_dir."/mmenu.php");
?>

<div class="table-structure">
 <h1>Paid for referrals</h1>
</div>


<p>
  There are numerous benefits for referring others to join CalculatingTraffic.com!  These benefits include:
  <p>&nbsp;</p>
  <ul>
    <li>
      - Earn cash commissions when one of your referrals upgrades their account.  Free members earn 10% commission; upgraded members can earn up to 50% commission.
    </li>
    <li>
      - Earn cash commissions when one your referrals purchases advertising.  Free members earn 10%, upgraded members can earn up to 30%.
    </li>
    <li>
      - Earn credits from your referrals' surfing activity.  Free members earn 3% of referrals' surfing activity; upgraded members earn up to 10%.
    </li>
  </ul>

  All cash commissions earned are paid to you automatically within 14 days after your cash balance reaches $10.00 or greater.
  <p>&nbsp;</p>
  Please go here to <a href="http://calculatingtraffic.com/members.php?page=compare">compare member levels.</a>
</p>


<div class="soc_buttons">
  <meta property="og:image" content="http://calculatingtraffic.com/themes/LFMTE_arun/images/logo_03.png" />

  <div class="facebook_icon">
    <a href="https://www.facebook.com/sharer/sharer.php?u=http://calculatingtraffic.com/?rid=<?php print $userid; ?>">
      Share on Facebook
    </a>

  </div>
  <div class="google_icon">
    <a href="https://plus.google.com/share?url=http://calculatingtraffic.com/?rid=<?php print $userid; ?>" onclick="javascript:window.open(this.href,
    '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">Share on Google+</a>
  </div>
</div>
<?php

include $theme_dir."/footer.php";
exit;

?>
