<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

session_start();

if(isset($_SESSION["adminid"])) {
	require "inc/checkauth.php";
	require_once "inc/funcs.php";
	$res = lfmsql_query("Select Id, firstname, lastname, username, email from `".$prefix."members` ORDER BY Id ASC LIMIT 1") or die(lfmsql_error());
	if (lfmsql_num_rows($res) < 1) {
		echo("No Members In Database");
		exit;
	}
	$_SESSION["userid"] = $usrid = lfmsql_result($res, 0, "Id");
	$firstname = lfmsql_result($res, 0, "firstname");
	$lastname = lfmsql_result($res, 0, "lastname");
	$username = lfmsql_result($res, 0, "username");
	$useremail = lfmsql_result($res, 0, "email");
} else {
	echo("You must login to your Admin Panel to access this page.");
	exit;
}

$_SESSION["prefix"] = $prefix;
require_once "inc/theme.php";

$otoid = $_GET['otoid'];
$spoid = $_GET['spoid'];
$rtoid = $_GET['rtoid'];

if ($otoid > 0) {
	
	$getoto = lfmsql_query("Select * from ".$prefix."oto_offers where id=$otoid limit 1");
	if (lfmsql_num_rows($getoto) > 0) {
		$otohtml = lfmsql_result($getoto, 0, "text");
		$otohtml = str_replace('[paymentcode]', "Payment code...", $otohtml);
		$otohtml = str_replace('[firstname]', "First...", $otohtml);
		$otohtml = str_replace('[lastname]', "Last...", $otohtml);
		$otohtml = str_replace('[username]', "Username...", $otohtml);
		$otohtml=translate_site_tags($otohtml);
		
		$otohtml=translate_user_tags($otohtml,$useremail);
		
		$usetheme = 0;
		
		if(stristr($otohtml, '[themeheader]') != FALSE) {
			load_template ($theme_dir."/header.php");
			$usetheme = 1;
		}
		$otohtml = str_ireplace('[themeheader]', '', $otohtml);
		$otohtml = str_ireplace('[themefooter]', '', $otohtml);
		
		// Output The HTML Content
		$page_content = $otohtml;
		
		if ($usetheme == 1) {
			load_template ($theme_dir."/content.php");
			load_template ($theme_dir."/footer.php");
		} else {
			echo($page_content);
		}
		
	} else {
		echo("Invalid offer");
	}
	
} elseif ($spoid > 0) {

	$getspo = lfmsql_query("Select * from ".$prefix."spo where id=$spoid limit 1");
	if (lfmsql_num_rows($getspo) > 0) {
		$spohtml = lfmsql_result($getspo, 0, "text");
		$spohtml = str_replace('[paymentcode]', "Payment code...", $spohtml);
		$spohtml = str_replace('[firstname]', "First...", $spohtml);
		$spohtml = str_replace('[lastname]', "Last...", $spohtml);
		$spohtml = str_replace('[username]', "Username...", $spohtml);
		$spohtml=translate_site_tags($spohtml);
		$spohtml=translate_user_tags($spohtml,$useremail);
		echo("$spohtml");
		echo("<br>");
	} else {
		echo("Invalid offer");
	}

} elseif ($rtoid > 0) {

	$getrto = lfmsql_query("Select * from ".$prefix."rtoads where id=$rtoid limit 1");
	if (lfmsql_num_rows($getrto) > 0) {
		$rtohtml = lfmsql_result($getrto, 0, "content");
		$rtohtml = str_replace('[paymentcode]', "Payment code...", $rtohtml);
		$rtohtml = str_replace('[firstname]', "First...", $rtohtml);
		$rtohtml = str_replace('[lastname]', "Last...", $rtohtml);
		$rtohtml = str_replace('[username]', "Username...", $rtohtml);
		$rtohtml=translate_site_tags($rtohtml);
		$rtohtml=translate_user_tags($rtohtml,$useremail);
		echo("$rtohtml");
		echo("<br>");
	} else {
		echo("Invalid offer");
	}

} else {
	echo("Invalid offer");
}

lfmsql_close();
exit;
?>