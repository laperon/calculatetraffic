<?php

#########################################
##     LFMTE Prize Page Addon v2       ##
##                                     ##
##        �2011 LJ Fix LLC             ##
##           www.LJFix.com             ##
##                                     ##
#########################################

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$usrid = $_SESSION["userid"];

$getemail = mysql_query("Select email from `".$prefix."members` where Id=$usrid limit 1");
$useremail = mysql_result($getemail, 0, "email");

if ($_SESSION['prizepage']['on'] != 'PrizeOn' and $_SESSION['prizepage']['on'] != 'PrizeGo') {
	header("Location: members.php");
	exit();
}


// Prize Page Settings
$pPrizePage = "http://".$_SERVER["SERVER_NAME"]."/prizepage.php"; // URL for Prize Page
$pTable = $prefix.'prizepage_prizes';
$pWinnersTable = $prefix.'prizepage_prize_winners';
$pSettingsTable = $prefix.'prizepage_prize_settings';

if (isset($_GET['get']) and md5('suht'.$_SESSION['prizepage']['id']) == $_GET['get']) {
	// Get Prize
	$cnx=mysql_query("select `id`,`name`,`credits`,`ban_imps`,`text_imps`,`cash` from `$pTable` where `id`={$_SESSION['prizepage']['id']} and `available`=1 and (`qty_in` - `qty_out`) > 0 limit 1");
	if (mysql_num_rows($cnx) > 0) {
		$prize=mysql_fetch_assoc($cnx);
		$datetime=date("Y-m-d H:i:s");
		mysql_query("insert into `$pWinnersTable` (`prize_id`,`user_id`,`datetime`) values ({$prize['id']},$usrid,'$datetime')");
		$winId=mysql_insert_id();
		mysql_query("update `$pTable` set `qty_out`=`qty_out` + 1 where `id`={$prize['id']}");
		$sql=array();
		$sql_admin=array();
		if ($prize['credits']>0) {
			$sql="Update `".$prefix."members` set credits=credits+".$prize['credits']." where Id=$usrid limit 1";
			mysql_query($sql);
			mysql_query("update `$pWinnersTable` set `delivered`=1 where `id`=$winId");
		}
		if ($prize['cash']>0) {
			$sql="Insert into `".$prefix."sales` (affid, saledate, itemid, itemname, itemamount, commission, txn_id, prize) values ($usrid, NOW(), 0, 'Surfing Prize', '".$prize['cash']."', '".$prize['cash']."', '0', 1)";
			mysql_query($sql);
			mysql_query("update `$pWinnersTable` set `delivered`=1 where `id`=$winId");
		}
		if ($prize['ban_imps']>0) {
			$sql="Update `".$prefix."members` set bannerimps=bannerimps+".$prize['ban_imps']." where Id=$usrid limit 1";
			mysql_query($sql);
			mysql_query("update `$pWinnersTable` set `delivered`=1 where `id`=$winId");
		}
		if ($prize['text_imps']>0) {
			$sql="Update `".$prefix."members` set textimps=textimps+".$prize['text_imps']." where Id=$usrid limit 1";
			mysql_query($sql);
			mysql_query("update `$pWinnersTable` set `delivered`=1 where `id`=$winId");
		}
		
		$content = mysql_result(mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='prize_page_claimed'"), 0);
		$content = str_replace('[prize_name]', $prize['name'], $content);
		
		$centerjs = "<script language=\"javascript\"> ";
		$centerjs .= "document.getElementById('prizepagecontent').style.top = 120 + 'px'; ";
		if (file_exists("chatbox_modern.php")) {
			$centerjs .= "document.getElementById('prizepagecontent').style.width = (document.body.clientWidth - 245) + 'px'; ";
		}
		$centerjs .= "</script>";
		$content = str_replace('[center_js]', $centerjs, $content);
		
		if (function_exists('html_entity_decode')) {
		   $content = html_entity_decode($content);
		} else {
		   $content = unhtmlentities($content);
		}
		
		$content=translate_site_tags($content);
		$content=translate_user_tags($content,$useremail);
		
		echo $content;
		
		unset($_SESSION['prizepage']);
		session_write_close();
		exit();
	}
} elseif ('PrizeOn' == $_SESSION['prizepage']['on']) {
	$_SESSION['prizepage']['on'] = 'PrizeGo';
	// Show page
	$cnx=mysql_query("select `id`, `name`, `address` from `$pTable` where `available`=1 and (`qty_in` - `qty_out`) > 0 order by rand() limit 1");
	if (mysql_num_rows($cnx) > 0) {
		$prize=mysql_fetch_assoc($cnx);
		$_SESSION['prizepage']['id']=$prize['id'];
		$content = mysql_result(mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='prize_page'"), 0);
		$content = str_replace('[prize_name]', $prize['name'], $content);
		$content = str_replace('[prize_page_url]', $pPrizePage.'?get='.md5('suht'.$prize['id']), $content);
		if (1==$prize['address']) {
			$content = str_replace('[if address]', '', $content);
			$content = str_replace('[/if address]', '', $content);
		} else {
			$content = str_replace('[if address]', '<!--', $content);
			$content = str_replace('[/if address]', '-->', $content);
		}
		
		$centerjs = "<script language=\"javascript\"> ";
		$centerjs .= "document.getElementById('prizepagecontent').style.top = 120 + 'px'; ";
		if (file_exists("chatbox_modern.php")) {
			$centerjs .= "document.getElementById('prizepagecontent').style.width = (document.body.clientWidth - 245) + 'px'; ";
		}
		$centerjs .= "</script>";
		$content = str_replace('[center_js]', $centerjs, $content);
		
		if (function_exists('html_entity_decode')) {
		   $content = html_entity_decode($content);
		} else {
		   $content = unhtmlentities($content);
		}
		
		$content=translate_site_tags($content);
		$content=translate_user_tags($content,$useremail);
		
		echo $content;
	}
}
?>