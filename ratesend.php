<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";

include "inc/config.php";
$dbconnectlink = mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die( "Unable to select database");

include "inc/funcs.php";
include "inc/theme.php";

// Get The IP Address And Table
$ipaddress = $_SERVER['REMOTE_ADDR'];
if (!preg_match('/^([0-9]{1,3})\.([0-9]{1,3})\.' . '([0-9]{1,3})\.([0-9]{1,3})$/', $ipaddress, $range)) {
	echo("Unable to verify your IP Address.");
	exit;
}

// Get Verification Data
if (!isset($_POST['verifycode']) || $_POST['verifycode'] == "") {
	echo("Invalid form data.");
	exit;
}

if (!isset($_POST['rate']) || !is_numeric($_POST['rate']) || $_POST['rate'] < 1 || $_POST['rate'] > 5) {
	load_template ($theme_dir."/header.php");
	$page_content = "<p><font size=\"2\"><b>Invalid rating.  Please close this tab or window and select a rating for the page from the dropdown menu.</b></font></p>";
	load_template ($theme_dir."/content.php");
	load_template ($theme_dir."/footer.php");
	exit;
}

if (!isset($_POST['comments']) || ($_POST['comments'] == "")) {
	$_POST['comments'] = "none";
}

if ((strpos($_POST['comments'], "http://") !== false) || (strpos($_POST['comments'], ".com") !== false) || (strpos($_POST['comments'], ".net") !== false) || (strpos($_POST['comments'], ".biz") !== false) || (strpos($_POST['comments'], ".ws") !== false)) {
	load_template ($theme_dir."/header.php");
	$page_content = "<p><font size=\"2\"><b>To prevent SPAM, comments can not contain any links or web site names.  Please close this tab or window and remove any links or domain names from your comment.</b></font></p>";
	load_template ($theme_dir."/content.php");
	load_template ($theme_dir."/footer.php");
	exit;
}

$comments = trim($_POST['comments']);
$comments = str_ireplace("<", "", $comments);
$comments = str_ireplace(">", "", $comments);
$comments = str_ireplace("'", "", $comments);
$comments = str_ireplace('"', '', $comments);
$comments = str_ireplace("&", "", $comments);

$getdata = mysql_query("SELECT user_id, rotator_id, tracker_id, source_id FROM `tracker_verify` WHERE verifycode='".$_POST['verifycode']."' AND ipaddress='".$ipaddress."' LIMIT 1");
if (mysql_num_rows($getdata) < 1) {
	echo("Could not verify form data.");
	exit;
}

$ownerid = mysql_result($getdata, 0, "user_id");
$rotatorid = mysql_result($getdata, 0, "rotator_id");
$trackerid = mysql_result($getdata, 0, "tracker_id");
$sourceid = mysql_result($getdata, 0, "source_id");

@mysql_query("DELETE FROM `tracker_verify` WHERE verifycode='".$_POST['verifycode']."' AND ipaddress='".$ipaddress."'") or die(mysql_error());

// Check If The User Already Rated This Site
$checkuser = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_rates` WHERE ipaddress='".$ipaddress."' AND tracker_id='".$trackerid."'"), 0);
if ($checkuser > 0) {
	echo("You already rated this page.");
	exit;
}

// Update Source Hits
@mysql_query("UPDATE `tracker_trackdata` SET nrates=nrates+1 WHERE user_id='".$ownerid."' AND rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."' LIMIT 1");

// Get Prize
$getuserprize = mysql_query("SELECT prizeid FROM `tracker_prizeswon` WHERE ipaddress='".$ipaddress."' ORDER BY prizeid DESC LIMIT 1");
if (mysql_num_rows($getuserprize) < 1) {
	$lastuserprize = 0;
} else {
	$lastuserprize = mysql_result($getuserprize, 0, "prizeid");
}
$getnextprize = mysql_query("SELECT id, image, title, descr FROM `tracker_prizes` WHERE id > '".$lastuserprize."' ORDER BY id ASC LIMIT 1");
if (mysql_num_rows($getnextprize) < 1) {
	echo("Error: Prize could not be found.");
	exit;
}
$prizeid = mysql_result($getnextprize, 0, "id");
$prizeimage = mysql_result($getnextprize, 0, "image");
$prizetitle = mysql_result($getnextprize, 0, "title");
$prizedescr = mysql_result($getnextprize, 0, "descr");

// Check If Ratings Are Private
$getowner = mysql_query("SELECT user_id FROM `tracker_urls` WHERE id='".$trackerid."'") or die(mysql_error());
if (mysql_num_rows($getowner) > 0) {
	$ownerid = mysql_result($getowner, 0, "user_id");
	mysql_query("UPDATE `".$prefix."members` SET rankstoday=rankstoday+1 where Id='".$ownerid."' LIMIT 1") or die(mysql_error());
}

// Insert Rating
$currentdate = date("Y-m-d");
@mysql_query("INSERT INTO `tracker_rates` (rotator_id, tracker_id, source_id, rate, date, ipaddress, comment) VALUES ('".$rotatorid."', '".$trackerid."', '".$sourceid."', '".$_POST['rate']."', '".$currentdate."', '".$ipaddress."', '".$comments."')");

// Average Rates
	$avgrate = mysql_result(mysql_query("SELECT AVG(rate) FROM `tracker_rates` WHERE rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."'"), 0);
	$avgrate = round($avgrate);
	@mysql_query("UPDATE `tracker_trackdata` SET avgrate='".$avgrate."' WHERE rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."'");

// Insert Prize Verification Data
@mysql_query("INSERT INTO `tracker_prizeswon` (date, prizeid, ipaddress) VALUES ('".$currentdate."', ".$prizeid.", '".$ipaddress."')");


$prizehost = "http://".$_SERVER["SERVER_NAME"];

// Get Source Name
if ($sourceid > 0) {
	$sourcename = mysql_result(mysql_query("SELECT url FROM `tracker_sources` WHERE id=".$sourceid." LIMIT 1"), 0);
} else {
	$sourcename = "";
}

// Output Prize Page

$gettemplate = mysql_query("SELECT template_data FROM `".$prefix."templates` WHERE template_name='sr_prizedl'") or die(mysql_error());
if (mysql_num_rows($gettemplate) < 1) {
	echo("Prize download page template is missing");
	exit;
}

$page_content = mysql_result($gettemplate, 0, "template_data");

$page_content = str_ireplace("#REFURL#", $prizehost."?rid=".$ownerid, $page_content);

if (function_exists('html_entity_decode')) {
	$page_content = html_entity_decode($page_content);
} else {
	$page_content = unhtmlentities($page_content);
}

$page_content = translate_site_tags($page_content);

$checkcontent = $page_content;

$page_content = str_replace('#THEMEHEADER#', "", $page_content);
$page_content = str_replace('#THEMEFOOTER#', "", $page_content);

if (strpos($checkcontent, '#THEMEHEADER#') !== false) {
	load_template ($theme_dir."/header.php");
}

$dlurl = $prizehost."/srprizedl.php?prizeid=".$prizeid;


$prizebox_content = "
<table style=\"border: 4px dashed ;\" border=0 bordercolor=black width=500 cellpadding=5 cellspacing=0>
<tr>
<td>
<table border=0 cellpadding=4 cellspacing=0>
<tr><td align=center colspan=2>
<p align=center><font size=5><b>Download Your Prize</b></font></p>
</td></tr>
<tr>";

if ($prizeimage != "none" && $prizeimage != "" && file_exists("srprizeimg/".$prizeimage)) {
$prizebox_content .= "
<td align=right valign=center>
<a target=_blank href=".$dlurl."><img src=".$prizehost."/srprizeimg/".$prizeimage." border=0 align=left></a>
</td>";
}

$prizebox_content .= "
<td align=left>
<p align=left><font size=5><b>".$prizetitle."</b></font><br /><br /><font face=Tahoma size=4><b>".$prizedescr."</b></font></p>
</td>
</tr>
<tr><td align=center colspan=2>
<p align=center><font size=6><b><a target=_blank href=".$dlurl.">Click To Download</a></b></font>
</td></tr>
</table>
</td>
</tr>
</table>
";

$page_content = str_replace('#PRIZEDLBOX#', $prizebox_content, $page_content);

if (strpos($checkcontent, '#THEMEHEADER#') !== false) {
	load_template ($theme_dir."/content.php");
} else {
	echo($page_content);
}

if (strpos($checkcontent, '#THEMEFOOTER#') !== false) {
	load_template ($theme_dir."/footer.php");
}

exit;

?>