<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.21
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

if (!isset($_GET['rid']) || !is_numeric($_GET['rid']) || $_GET['rid'] < 1) {
	echo("Invalid User ID");
	exit;
}

$rid = $_GET['rid'];
$userimg = "None";
$admindefault = "None";

if ($rid > 0 && file_exists("splash/personal/personal_head.php")) {
	// Use Uploaded Image
	require_once ('splash/personal/personal_head.php');
	$userimg = $info['img'];
	
	$getadmindefault = mysql_query("SELECT img FROM user_promo_info WHERE userid=0") or die(mysql_error());
	if (mysql_num_rows($getadmindefault) > 0) {
		$admindefault = mysql_result($getadmindefault, 0, "img");
	}
}

if ($userimg == "None" || strlen($userimg) < 5 || $userimg == $admindefault || !file_exists($_SERVER['DOCUMENT_ROOT'].$userimg)) {
	// Use Gravatar
	$getemail = mysql_query("SELECT email FROM ".$prefix."members WHERE Id='".$rid."' AND refid='".$userid."' LIMIT 1") or die(mysql_error());
	if (mysql_num_rows($getemail) < 1) {
		echo("User Not Found");
		exit;
	}
	$useremail = mysql_result($getemail, 0, "email");
	$grav = md5(strtolower(trim($useremail)));
	$userimg = "http://www.gravatar.com/avatar/".$grav."?d=mm&s=100";
}

header ('Location: '.$userimg);
exit;

?>