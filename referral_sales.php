<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.20
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	require_once "inc/filter.php";
	session_start();
	include "inc/userauth.php";
	$userid = $_SESSION["userid"];
	
	if (!isset($_GET['ref']) || !is_numeric($_GET['ref'])) {
		echo("Invalid Referral ID");
		exit;
	}
	
	$page_content = '<html>
	<body>
	<center>
	<h3 align="center">Referral Purchases</h3>';
	
	$page_content .= '<br />
	 <table align="center" border="1" bordercolor="black" cellpadding="3" cellspacing="0">
	 <tr class="membertdbold"><td align="center">Date</td>
	 <td align="center">Product</td>
	 <td align="center">Price</td>
	 <td align="center">Your Commission</td>
	 <td align="center">Commission Status</td>
	 </tr>';

  // Get sales details which are for this affiliate
  $cnt=0;
  $totalowing=0;
  $res=@mysql_query("SELECT * FROM ".$prefix."sales WHERE affid='".$_SESSION['userid']."' AND purchaserid='".$_GET['ref']."'") or die("Unable to get sales data: ".mysql_error());
  while($row=@mysql_fetch_array($res))
  {
	 $cnt++;

	 if($row["status"]=="P")
	 {
		$status = "Paid";
	 }
	 else if($row["status"] == "R")
	 {
		$status="Refund";
		$totalowing+=$row["commission"];
	 }
	 else if($row["commission"] < 0)
	 {
	 	// Purchase subtracted from balance
		$status="";
		$totalowing = $totalowing+$row["commission"];
	 }
	 else
	 {
		$status="Unpaid";
		$totalowing+=$row["commission"];
	 }
	 $page_content .= '<tr>
		<td align="center">'.$row["saledate"].'</td>
		<td align="center">'.$row["itemname"].'</td>
		<td align="center">'.$row["itemamount"].'</td>
		<td align="center">'.$row["commission"].'</td>
		<td align="center">'.$status.'</td>
		</tr>';

  } // End of rows from query

  if($cnt==0) {
	 $page_content .= '<tr class="formlabelbold"><td colspan="5" ><center>No purchases found</center></td></tr>';
  }
  
  $page_content .= '</table>
  </center>
  </body>
  </html>';
  
  echo($page_content);
  exit;
  
?>