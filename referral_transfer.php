<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.20
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

if (!isset($_GET['rid']) || !is_numeric($_GET['rid']) || $_GET['rid'] < 1) {
	echo("Invalid User ID");
	exit;
}

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";

$getaccdata = mysql_query("Select cantransfer, mintransfer, maxtransfer from ".$prefix."membertypes where mtid=$acctype limit 1");
$cantransfer = mysql_result($getaccdata, 0, "cantransfer");
$mintransfer = mysql_result($getaccdata, 0, "mintransfer");
$maxtransfer = mysql_result($getaccdata, 0, "maxtransfer");

if ($cantransfer != "1") {
	echo("Transfer Credits Is Not Enabled");
	exit;
}

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}


if ($_GET['transfercredits'] == "yes") {

	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM ".$prefix."members WHERE Id='".$_GET['rid']."' AND refid='".$_SESSION['userid']."'"), 0);
	if ($checkexists < 1) {
		echo("User #".$_GET['rid']." Not Found In Your Downline");
		exit;
	}
	
	$totransfer = $_POST['totransfer'];
	
	if (!check_number($totransfer)) {
		echo("<p><b>Credit amount must be a positive whole number.</b></p><p><b><a href=\"referral_transfer.php?rid=".$_GET['rid']."\">Go Back</a></b></p>"); exit;
	}
	
	if (($mintransfer > 0) && ($totransfer < $mintransfer)) {
		echo("<p><b>You must transfer a minimum of ".$mintransfer." credits.</b></p><p><b><a href=\"referral_transfer.php?rid=".$_GET['rid']."\">Go Back</a></b></p>"); exit;
	}
	
	if (($maxtransfer > 0) && ($totransfer > $maxtransfer)) {
		echo("<p><b>You can only transfer a maximum of ".$maxtransfer." credits.</b></p><p><b><a href=\"referral_transfer.php?rid=".$_GET['rid']."\">Go Back</a></b></p>"); exit;
	}
	
	$countcredits = mysql_query("select credits from ".$prefix."members where Id=$userid");
	$membercredits = mysql_result($countcredits, 0, "credits");
	
	if ($totransfer > $membercredits) {
		echo("<p><b>You tried to transfer ".$totransfer." credits but only have ".$membercredits." credits in your account.</b></p><p><b><a href=\"referral_transfer.php?rid=".$_GET['rid']."\">Go Back</a></b></p>"); exit;
	}
	
	mysql_query("Update ".$prefix."members set credits=credits-$totransfer where Id='".$userid."'") or die(mysql_error());
	mysql_query("Update ".$prefix."members set credits=credits+$totransfer where Id='".$_GET['rid']."'") or die(mysql_error());
	
	if (file_exists("ptransfer_send.php")) {
		include("ptransfer_send.php");
	}
	
	load_template ($theme_dir."/header.php");
	load_template ($theme_dir."/mmenu.php");
	echo("<p><b>You successfully transferred ".$totransfer." credits.</b></p><p><b><a href=\"members.php?mf=mr\">Go Back to Referrals</a></b></p>");
	load_template ($theme_dir."/footer.php");
	exit;
	
}

include("surf_prefs.php");
$adminprefs = get_admin_prefs();

####################

//Begin main page

####################

load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");

$usercredits = round($usercredits, 2);

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>

<center>");

if ($errormess != "") {
echo("<p><b><font color=red>$errormess</font></b></p>");
}

echo("<p><b>You have ".$usercredits." credits in your account.</b></p>");

if ($mintransfer > 0) {
	echo("<p><b>You must transfer a minimum of ".$mintransfer." credits</b></p>");
}

if ($maxtransfer > 0) {
	echo("<p><b>You can transfer a maximum of ".$maxtransfer." credits</b></p>");
}

echo("
<table border=1 bordercolor=black bgcolor=#EEEEEE cellpadding=5 cellspacing=0>
<tr><td align=center>
<form style=\"margin:0px\" action=\"referral_transfer.php?transfercredits=yes&rid=".$_GET['rid']."\" method=\"post\">
<p>Transfer <input type=text name=totransfer value=0 size=4> credits.<br><br><input type=submit value=\"Transfer\"></p>
");

if (file_exists("ptransfer.php")) {
	include("ptransfer.php");
}

echo("</form>
</td></tr>
</table>
<br><br>");

include $theme_dir."/footer.php";

exit;

?>