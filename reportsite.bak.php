<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$acctype = $mtype;
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";

load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");

if ($_GET['sendreport'] == "yes") {

	$sitereported = $_POST['reportsite'];
	$todaysdate = date("Y-m-d");
	
	if ($sitereported == "currentview" || $sitereported == "lastview") {
	
	$getsiteid = mysql_query("Select ".$sitereported." from ".$prefix."members where Id=$userid limit 1");
	$siteid = mysql_result($getsiteid, 0, $sitereported);

	$checkexisting = mysql_query("Select id from ".$prefix."reports where siteid=".$siteid." and action=0");

	if ($siteid < 1) {
		$errormess = "There is no site to report.";
	} elseif (mysql_num_rows($checkexisting) > 0) {
		$errormess = "Another abuse report has already been submitted for this web site.  This site is pending review, and the appropriate action will be taken if necessary.  Please close this window to continue surfing.";
	} elseif ($_POST['reasontext'] == "") {
		$errormess = "Please enter a reason for reporting this site.";
		
	} else {
	
		//Submit report
		@mysql_query("Insert into ".$prefix."reports (date, userfrom, siteid, text, action, autosuspended) values (".time().", $userid, $siteid, '".$_POST['reasontext']."', 0, 0)");
		$reportid = mysql_insert_id();
		
		echo("<center><h4><b>Report Abuse</b></h4>");		
		echo("<br><p><b>Thank you.  Your report will be reviewed shortly.  Please close this window to continue surfing.</b></p>");
		include $theme_dir."/footer.php";
		flush();

		//HitsConnect Autocheck
		if (file_exists("f4checker.php")) {
			include "f4checker.php";
			$gettesturl = mysql_query("Select url from ".$prefix."msites where id=".$siteid." limit 1");
			$testurl = mysql_result($gettesturl, 0, "url");
			$F4CHECK = f4check($testurl);
			if ($F4CHECK['PAGE_STATUS'] == "FAILED") {
				//Automatically suspend the site
			           $FAILEDREASON = $_POST['reasontext']."

Automatically Suspended By HitsConnect Checker:";
				   reset($F4CHECK);
				   foreach($F4CHECK AS $F4KEY=>$F4VAL) {
					   if (substr($F4KEY,0,3) == 'ERR') {
						   $FAILEDREASON .= " ".$F4VAL;
					   }
				   }
				   @mysql_query("Update ".$prefix."msites set state=3 where id=".$siteid." limit 1");
				   @mysql_query("Update ".$prefix."reports set text='".$FAILEDREASON."', autosuspended=1, action=2 where id=$reportid limit 1");
			}
		}
		//End HitsConnect Autocheck

		exit;
	}
	
	}
}

####################

//Begin main page

####################

echo("<center><h4><b>Report Abuse</b></h4>");

if ($errormess != "") {
echo("<p><b><font color=red>$errormess</font></b></p>");
}

echo("
<form action=\"reportsite.php?sendreport=yes\" method=post>
<br>
<INPUT type=radio name=\"reportsite\" value=\"currentview\" CHECKED>Report The Current Site
<br>
<INPUT type=radio name=\"reportsite\" value=\"lastview\">Report The Last Site Shown
<br>
<p><b>Please enter a brief description of the problem you are reporting:</b></p>
<textarea name=\"reasontext\" wrap=\"soft\" rows=\"4\" cols=\"30\"></textarea><br>
<input type=submit value=\"Send Report\">
</form>
");

include $theme_dir."/footer.php";

exit;

?>