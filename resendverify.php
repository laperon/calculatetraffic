<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.02
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

	include "inc/config.php";
	include "inc/funcs.php";
	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");

	// Query settings table for sitename
	$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
	$row=@mysql_fetch_array($res);
	$sitename=$row["sitename"];
	$replyaddress=$row["replyaddress"];

	if($_POST["Submit"] == "Submit")
	{
		$lres=@mysql_query("SELECT username, email FROM ".$prefix."members WHERE email='".$_POST["email"]."'");
		
		if(mysql_num_rows($lres) > 0)
		{
		
			$lrow=@mysql_fetch_array($lres);
			$username=$lrow["username"];
			$recipient=$lrow["email"];

			SendWelcomeEmail($recipient);

			$msg="Your details have been emailed";

		}
		else
		{
			$msg="Sorry, your email address is not in the database.  Please contact support or create a new account.";
		}
	}

include "inc/theme.php";

load_template ($theme_dir."/header.php");

	if(isset($_POST["email"]))
	{
		$page_content =  "<center>$msg</center><br>Click <a href=\"login.php\">HERE</a> to return to the login page.";
	}
	else
	{
	
$page_content = <<<EOT
 <form method="post" action="resendverify.php">
 <br>
<table align="center" width="50%" border="0" cellspacing="0" cellpadding="0">
  <tr class="membertdbold">
    <td colspan="2" align="center">Reverify Your Account</td>
  </tr>
  <tr class="formlabel">
    <td colspan="2">Enter the email address you signed up with in the box below and we will resend the verification link.</td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" nowrap="nowrap" class="formlabelbold">Email Address: </td>
    <td align="left"><input name="email" type="text" id="email" size="30" /></td>
  </tr>
  <tr>
    <td colspan="2" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" name="Submit" value="Submit" /></td>
  </tr>
</table>
</form>

EOT;
}
load_template ($theme_dir."/content.php");
load_template ($theme_dir."/footer.php");
?>