<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
include "inc/config.php";
require_once "inc/funcs.php";
$dbconnectlink = mysql_connect($dbhost,$dbuser,$dbpass);
mysql_select_db($dbname) or die( "Unable to select database");

$url="about:blank";
$user=0;

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
	$rotatorid = $_GET['id'];
} elseif (isset($_GET['rotname']) && strlen($_GET['rotname']) > 0) {
	$_GET['rotname'] = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $_GET['rotname']));
	$getrotid = mysql_query("SELECT id FROM `rotators` WHERE name='".$_GET['rotname']."' LIMIT 1") or die(mysql_error());
	if (mysql_num_rows($getrotid) < 1) {
		echo("Rotator Name Not Found");
		exit;
	}
	$rotatorid = mysql_result($getrotid, 0, "id");
} else {
	echo("Invalid Rotator ID.");
	exit;
}

if ($rotatorid>0 and is_numeric($rotatorid)) {
	
		$cnx = mysql_query("SELECT a.user_id AS user_id, a.name AS rotname, a.showframe AS showframe, a.smartfilter AS smartfilter, c.rot_noframe AS noframe FROM rotators a LEFT JOIN ".$prefix."members b ON (a.user_id=b.Id) LEFT JOIN ".$prefix."membertypes c ON (b.mtype=c.mtid) WHERE a.id='".$rotatorid."' AND b.status != 'Suspended'") or die(mysql_error());
	if (mysql_num_rows($cnx) < 1) {
		echo("This Member's Account Has Been Deleted Or Disabled");
		exit;
	} else {
		$user = mysql_result($cnx, 0, 'user_id');
		$rotname = mysql_result($cnx, 0, 'rotname');
		$showframe = mysql_result($cnx, 0, 'showframe');
		$smartfilter = mysql_result($cnx, 0, 'smartfilter');
		$noframe = mysql_result($cnx, 0, 'noframe');
	}
	
	if (isset($_GET['srcname']) && strlen($_GET['srcname']) > 0) {
		$_GET['srcname'] = preg_replace('/[^A-Za-z0-9]/', '', $_GET['srcname']);
		$source = $_GET['srcname'];
	} else {
		$sourceurl = $_SERVER['HTTP_REFERER'];
		$splitsource = explode("/", $sourceurl);
		$source = $splitsource[2];
		$source = str_ireplace("www.", "", $source);
	}
	
	if ($source != "") {
		$getsourceid = mysql_query("SELECT id FROM `tracker_sources` WHERE url='".$source."' LIMIT 1");
		if (mysql_num_rows($getsourceid) > 0) {
			$sourceid = mysql_result($getsourceid, 0, "id");
		} else {
			// Add New Source
			mysql_query("INSERT INTO `tracker_sources` (url) VALUES ('".$source."')") or die(mysql_error());
			$newsource = mysql_insert_id($dbconnectlink);
			$sourceid = $newsource;
		}
		
		$livestats = @mysql_result(@mysql_query("SELECT value FROM `tracker_settings` WHERE field='livestats'"), 0);
		if ($livestats == "1" && strlen($source) > 0) {
			@mysql_query("INSERT INTO `rotator_livestats` (stattime, user_id, rotator_id, statdata) VALUES ('".time()."', '".$user."', '".$rotatorid."', '".$source."')");
		}
		
	} else {
		$source = "";
		$sourceid = 0;
	}
	
	
	$randnum = rand(1, 100);
	$cnx=mysql_query("SELECT a.tracker_id AS tracker_id, b.url AS trackurl FROM rotator_sites a LEFT JOIN tracker_urls b ON (a.tracker_id=b.id) WHERE a.rotator_id='".$rotatorid."' and a.highnum>='$randnum' and a.lownum<='$randnum'") or die(mysql_error());
	if (mysql_num_rows($cnx) > 0) {
		
		if ($smartfilter == 1 && $sourceid > 0 && strlen($_SERVER['HTTP_REFERER']) > 0) {
			$numtries = 0;
			$sourceurl = $_SERVER['HTTP_REFERER'];
			$splitsource = explode("/", $sourceurl);
			$sourcedomain = $splitsource[2];
			$sourcedomain = str_ireplace("www.", "", $sourcedomain);
			while ($numtries < 3 && strpos(mysql_result($cnx, 0, "trackurl"), $sourcedomain) !== false) {
				$randnum = rand(1, 100);
				$cnx=mysql_query("SELECT a.tracker_id AS tracker_id, b.url AS trackurl FROM rotator_sites a LEFT JOIN tracker_urls b ON (a.tracker_id=b.id) WHERE a.rotator_id='".$rotatorid."' and a.highnum>='$randnum' and a.lownum<='$randnum'") or die(mysql_error());
				$numtries = $numtries+1;
			}
		}
		
		$tracker_id = mysql_result($cnx, 0, "tracker_id");
		
		$checkurl = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_urls` WHERE state=0 and id='".$tracker_id."'"), 0);
		if ($checkurl > 0) {
			$url = "/tracker.php?id=".$tracker_id."&rotatorid=".$rotatorid."&srcid=".$sourceid;
		} else {
			// Tracker URL Has Been Removed - Reset Rotator
			$url = "about:blank";
			mysql_query("DELETE FROM rotator_sites WHERE tracker_id='".$tracker_id."'") or die(mysql_error());
			
			// Update The Bar Values
			$getbarvals = mysql_query("SELECT id, barvalue FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$rotatorid."'");
			if (mysql_num_rows($getbarvals) > 0) {
				$cdownrand = 100;
				$totalvarval = mysql_result(mysql_query("SELECT SUM(barvalue) FROM rotator_sites WHERE barvalue>0 AND rotator_id='".$rotatorid."'"), 0);
				for ($i = 0; $i < mysql_num_rows($getbarvals); $i++) {
					$rotsiteid = mysql_result($getbarvals, $i, "id");
					$barvalue = mysql_result($getbarvals, $i, "barvalue");
					$barpercent = round(($barvalue/$totalvarval) * 100);
					
					$highnum = $cdownrand;
					$cdownrand = $cdownrand-$barpercent;
					$lownum = $cdownrand;
					
						mysql_query("UPDATE rotator_sites SET highnum='".$highnum."', lownum='".$lownum."' WHERE id='".$rotsiteid."'");
				}
			}
			// End Update The Bar Values
			
		}
	}
	
	if ($showframe == 0 && $noframe == 1) {
		header("Location:".$url);
		exit;
	}
	
	$getframeheight = mysql_query("SELECT value FROM `tracker_settings` WHERE field='frameheight'");
	if (mysql_num_rows($getframeheight) > 0) {
		$frameheight = mysql_result($getframeheight, 0, "value");
	} else {
		$frameheight = "50";
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<title><?php echo($rotname); ?></title>
</head>
<frameset rows="<?php echo($frameheight); ?>,*" border="0">
<frame marginheight="0" marginwidth="0" scrolling="no" noresize border="0" src="/rotator_top.php?mem=<?php echo $user;?>&rotatorid=<?php echo $rotatorid;?>" />
<frame marginheight="0" marginwidth="0" scrolling="auto" noresize border="0" src="<?php echo $url;?>" />
</frameset>
</html>
<?php
exit();
mysql_close();
?>
