<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
include "inc/config.php";
require_once "inc/funcs.php";
require_once "inc/sql_funcs.php";
$mconn=@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname,$mconn) or die( "Unable to select database");

if (!isset($_GET['rotatorid']) || !is_numeric($_GET['rotatorid'])) {
	echo("Invalid Rotator ID");
	exit;
}

// Get Rotator Settings
$rotatorid = $_GET['rotatorid'];
$getrotatorinfo = mysql_query("SELECT showphoto, showsocial FROM `rotators` WHERE id='".$rotatorid."' LIMIT 1") or die(mysql_error());
if (mysql_num_rows($getrotatorinfo) > 0) {
	$showphoto = mysql_result($getrotatorinfo, 0, "showphoto");
	$showsocial = mysql_result($getrotatorinfo, 0, "showsocial");
} else {
	echo("Rotator Not Found");
	exit;
}

// Get User Settings
if ($_GET['mem']>0 and is_numeric($_GET['mem'])) {
	$ref='rid='.$_GET['mem'];
	$rid=$_GET['mem'];
	
	if ($showsocial == "1" && file_exists("social_branding.php")) {
		$selectsocial = ", facebook, twitter, skype";
	} else {
		$selectsocial = "";
	}
	
	$getuserinfo = mysql_query("SELECT username, firstname, lastname, email".$selectsocial." FROM ".$prefix."members WHERE Id='".$rid."' AND status='Active' LIMIT 1") or die(mysql_error());
	if (mysql_num_rows($getuserinfo) > 0) {
		$username = mysql_result($getuserinfo, 0, "username");
		$firstname = mysql_result($getuserinfo, 0, "firstname");
		$lastname = mysql_result($getuserinfo, 0, "lastname");
		$useremail = mysql_result($getuserinfo, 0, "email");
	} else {
		$ref='';
		$rid=0;
		$username = "";
		$firstname = "";
		$lastname = "";
	}
} else {
	$ref='';
	$rid=0;
	$username = "";
	$firstname = "";
	$lastname = "";
}
$_GET['rid'] = $rid;

// Social Media Info - Integrates With Social Surfbar Mod If Installed
$socialinfo = "";
if ($rid > 0 && $showsocial == "1" && file_exists("social_branding.php")) {
	$fb = mysql_result($getuserinfo, 0, "facebook");
	$ti = mysql_result($getuserinfo, 0, "twitter");
	$sk = mysql_result($getuserinfo, 0, "skype");
	if (strlen($fb) > 0) { $socialinfo .= "&nbsp;<a href=\"http://facebook.com/".$fb."\" target=\"_BLANK\"><img src=\"images/fb.png\" border=0 width=20 height=20></a>"; }
	if (strlen($ti) > 0) { $socialinfo .= "&nbsp;<a href=\"http://twitter.com/".$ti."\" target=\"_BLANK\"><img src=\"images/ti.png\" border=0 width=20 height=20></a>"; }
	if (strlen($sk) > 0) { $socialinfo .= "&nbsp;<a href=\"skype:".$sk."?chat\" target=\"_BLANK\"><img src=\"images/sk.png\" border=0 width=20 height=20></a>"; }
}
// End Social Media Info

// Get User Image
if ($rid > 0 && $showphoto == "1") {
	
	$userimg = "None";
	$admindefault = "None";
	if (file_exists("splash/personal/personal_head.php")) {
		// Use Uploaded Image
		require_once ('splash/personal/personal_head.php');
		$userimg = $info['img'];
		$getadmindefault = mysql_query("SELECT img FROM user_promo_info WHERE userid=0") or die(mysql_error());
		if (mysql_num_rows($getadmindefault) > 0) {
			$admindefault = mysql_result($getadmindefault, 0, "img");
		}
	}
	
	if ($userimg == "None" || strlen($userimg) < 5 || $userimg == $admindefault || !file_exists($_SERVER['DOCUMENT_ROOT'].$userimg)) {
		// Use Gravatar
		$grav = md5(strtolower(trim($useremail)));
		$userimg = "http://www.gravatar.com/avatar/".$grav."?d=mm&s=60";
	}
	
	$imgtag = "<img src=\"".$userimg."\" height=\"60\" border=\"0\">";
	$imgsrc = $userimg;
} else {
	$imgtag = "";
	$imgsrc = "";
}
// End Get User Image

$content = mysql_result(mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='rotator_top'"), 0);

if (function_exists('html_entity_decode')) {
   $content = html_entity_decode($content);
} else {
   $content = unhtmlentities($content);
}

// LFMTE User Macros
$content = str_replace('#FIRSTNAME#', $firstname, $content);
$content = str_replace('#LASTNAME#', $lastname, $content);
$content = str_replace('#USERNAME#', $username, $content);
$content = str_replace('#AFFILIATEID#', $rid, $content);
$content = str_replace('#REFURL#', "http://".$_SERVER["SERVER_NAME"]."/index.php?rid=".$rid, $content);

// Rotator Plugin Macros
$content = str_replace('[user_img]', $imgtag, $content);
$content = str_replace('[user_img_src]', $imgsrc, $content);
$content = str_replace('[ref_id]', $rid, $content);
$content = str_replace('[social_info]', $socialinfo, $content);

$content = translate_site_tags($content);

if ($rid > 0) {
	$usrid = $rid;
	$getemail = mysql_query("Select email from `".$prefix."members` where Id=$usrid limit 1");
	$useremail = mysql_result($getemail, 0, "email");
	$content = translate_user_tags($content,$useremail);
}

echo $content;

exit;

?>