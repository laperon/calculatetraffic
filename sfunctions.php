<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.12
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

function getsite($userid=0, $defaultsite="about:blank") {
	$prefix = $_SESSION["prefix"];
	$numattempts = 0;

	$getrotation = mysql_query("Select rotationtype from ".$prefix."settings");
	$rotationtype = mysql_result($getrotation, 0, "rotationtype");

	if ($rotationtype == 0) {
	//Per Member Rotation

	while ($numattempts<3) {
		$getuser = mysql_query("Select Id from ".$prefix."members where Id!=$userid and status='Active' and actsite=1 order by lastsite asc limit 1");
		if (mysql_num_rows($getuser)<1) {
			return $defaultsite;
		} else {
			$gotuser = mysql_result($getuser, 0, "Id");
			@mysql_query("Update ".$prefix."members set lastsite=".time()." where Id=$gotuser limit 1");
			$geturl = mysql_query("Select id, url from ".$prefix."msites where state=1 and memid=$gotuser and credits>=1 order by lasthit asc limit 1");
			if (mysql_num_rows($geturl)<1) {
				@mysql_query("Update ".$prefix."members set actsite=0 where Id=$gotuser limit 1");
			} else {
				$siteid = mysql_result($geturl, 0, "id");
				$siteurl = mysql_result($geturl, 0, "url");
				$_SESSION["siteviewed"] = $siteid;
				@mysql_query("Update ".$prefix."msites set lasthit=".time()." where id=$siteid limit 1");
				return $siteurl;
			}
		}
		$numattempts = $numattempts+1;
	}

	} else {
	//Per Site Rotation

	$geturl = mysql_query("Select id, url from ".$prefix."msites where memid!=$userid and state=1 and credits>=1 order by lasthit asc limit 1");

	if (mysql_num_rows($geturl)>0) {
		$siteid = mysql_result($geturl, 0, "id");
		$siteurl = mysql_result($geturl, 0, "url");
		$_SESSION["siteviewed"] = $siteid;
		@mysql_query("Update ".$prefix."msites set lasthit=".time()." where id=$siteid limit 1");
		return $siteurl;
	}

	}

return $defaultsite;
}

function showbanner($userid=0, $defaultimage="about:blank", $defaulttarget="about:blank") {
	$prefix = $_SESSION["prefix"];
	$numattempts = 0;

	$getrotation = mysql_query("Select rotationtype from ".$prefix."settings");
	$rotationtype = mysql_result($getrotation, 0, "rotationtype");

	if ($rotationtype == 0) {
	//Per Member Rotation

	while ($numattempts<3) {
		$getuser = mysql_query("Select Id from ".$prefix."members where Id!=$userid and status='Active' and actbanner=1 order by lastbanner asc limit 1");
		if (mysql_num_rows($getuser)<1) {
			return "<a target=_blank href=\"$defaulttarget\"><img border=0 src=\"$defaultimage\" width=468 height=60></a>";
		} else {
			$gotuser = mysql_result($getuser, 0, "Id");
			@mysql_query("Update ".$prefix."members set lastbanner=".time()." where Id=$gotuser limit 1");
			$geturl = mysql_query("Select id, img, target from ".$prefix."mbanners where state=1 and memid=$gotuser and imps>=1 order by lasthit asc limit 1");
			if (mysql_num_rows($geturl)<1) {
				@mysql_query("Update ".$prefix."members set actbanner=0 where Id=$gotuser limit 1");
			} else {
				$bannerid = mysql_result($geturl, 0, "id");
				$bannerimage = mysql_result($geturl, 0, "img");
				$bannertarget = "clickbanner.php?bannerid=".$bannerid;
				@mysql_query("Update ".$prefix."mbanners set hits=hits+1, hitstoday=hitstoday+1, imps=imps-1, lasthit=".time()." where id=$bannerid limit 1");
				return "<a target=_blank href=\"$bannertarget\"><img border=0 src=\"$bannerimage\" width=468 height=60></a>";
			}
		}
		$numattempts = $numattempts+1;
	}

	} else {
	//Per Site Rotation

	$geturl = mysql_query("Select id, img, target from ".$prefix."mbanners where memid!=$userid and state=1 and imps>=1 order by lasthit asc limit 1");

	if (mysql_num_rows($geturl)>0) {
		$bannerid = mysql_result($geturl, 0, "id");
		$bannerimage = mysql_result($geturl, 0, "img");
		$bannertarget = "clickbanner.php?bannerid=".$bannerid;
		@mysql_query("Update ".$prefix."mbanners set hits=hits+1, hitstoday=hitstoday+1, imps=imps-1, lasthit=".time()." where id=$bannerid limit 1");
		return "<a target=_blank href=\"$bannertarget\"><img border=0 src=\"$bannerimage\" width=468 height=60></a>";
	}

	}

    return "<a target=_blank href=\"$defaulttarget\"><img border=0 src=\"$defaultimage\" width=468 height=60></a>";
}

function __showbanner($userid=0, $defaultimage="about:blank", $defaulttarget="about:blank") {
    $prefix = $_SESSION["prefix"];

    // Get website data
//	$getnewsiteid = mysql_query("Select id, bannerId, textId from `".$prefix."msites` where id = " . $_SESSION["newsiteid"] . " limit 1");
    $getnewsiteid = mysql_query("Select id, bannerId, textId from `".$prefix."msites` where id = " . get_current_site_id() . " limit 1");
	if (mysql_num_rows($getnewsiteid) < 1) {
//        echo "\n\n\n|||||||||||||||| \$getnewsiteid is empty ||||||||||||||||\n\n\n";
        return showbanner($userid, $defaultimage, $defaulttarget);
	}
    
    $newbannerid = mysql_result($getnewsiteid, 0, "bannerId");
    if($newbannerid != ''){
//        echo "\n\n\n######### \$newbannerid: " . $newbannerid . " #########\n\n\n";
$imps_sub = 1;
        $geturl = mysql_query("
            Select
                id,
                img,
                target
            from 
                ".$prefix."mbanners
            where
                id = $newbannerid
                and state = 1
                and imps >= $imps_sub
            limit 1
        ");
        
        if (mysql_num_rows($geturl)<1) {
            return showbanner($userid, $defaultimage, $defaulttarget);
        }
        
        $bannerid = mysql_result($geturl, 0, "id");
        $bannerimage = mysql_result($geturl, 0, "img");
        $imps_sub = 1;
        $bannertarget = "clickbanner.php?bannerid=".$bannerid;
        @mysql_query("
            Update
                ".$prefix."mbanners
            set
                hits = hits+1,
                hitstoday = hitstoday+1,
                imps = imps-$imps_sub,
                lasthit = " . time() . "
            where
                id = $bannerid
            limit 1
        ");
        return "<a target=_blank href=\"$bannertarget\"><img border=0 src=\"$bannerimage\" width=468 height=60></a>";
    }

//    echo "\n\n\n@@@@@@@@@ \$newbannerid is empty @@@@@@@@@\n\n\n";
    
    return showbanner($userid, $defaultimage, $defaulttarget);
}

function showtext($userid=0, $maxchars=30, $defaulttext="Text Ad", $defaulttarget="about:blank", $textImpColor) {
	$prefix = $_SESSION["prefix"];
	$numattempts = 0;

	$getrotation = mysql_query("Select rotationtype from ".$prefix."settings");
	$rotationtype = mysql_result($getrotation, 0, "rotationtype");

	if ($rotationtype == 0) {
	//Per Member Rotation

	while ($numattempts<3) {
		$getuser = mysql_query("Select Id from ".$prefix."members where Id!=$userid and status='Active' and acttext=1 order by lasttext asc limit 1");
		if (mysql_num_rows($getuser)<1) {
			return "<a target=_blank style=\"text-decoration:none;color:".$textImpColor."\" href=\"$defaulttarget\">".$defaulttext."</a>";
		} else {
			$gotuser = mysql_result($getuser, 0, "Id");
			@mysql_query("Update ".$prefix."members set lasttext=".time()." where Id=$gotuser limit 1");
			$geturl = mysql_query("Select id, text, target from ".$prefix."mtexts where state=1 and memid=$gotuser and imps>=1 and numchars<=$maxchars order by lasthit asc limit 1");
			if (mysql_num_rows($geturl)<1) {
				@mysql_query("Update ".$prefix."members set acttext=0 where Id=$gotuser limit 1");
			} else {
				$textid = mysql_result($geturl, 0, "id");
				$textad = mysql_result($geturl, 0, "text");
				$texttarget = "clicklink.php?linkid=".$textid;
				@mysql_query("Update ".$prefix."mtexts set hits=hits+1, hitstoday=hitstoday+1, imps=imps-1, lasthit=".time()." where id=$textid limit 1");
				return "<a target=_blank style=\"text-decoration:none;color:".$textImpColor."\" href=\"$texttarget\">".$textad."</a>";
			}
		}
		$numattempts = $numattempts+1;
	}

	} else {
	//Per Site Rotation

	$geturl = mysql_query("Select id, text, target from ".$prefix."mtexts where memid!=$userid and state=1 and imps>=1 and numchars<=$maxchars order by lasthit asc limit 1");
	if (mysql_num_rows($geturl)>0) {
		$textid = mysql_result($geturl, 0, "id");
		$textad = mysql_result($geturl, 0, "text");
		$texttarget = "clicklink.php?linkid=".$textid;
		@mysql_query("Update ".$prefix."mtexts set hits=hits+1, hitstoday=hitstoday+1, imps=imps-1, lasthit=".time()." where id=$textid limit 1");
		return "<a target=_blank style=\"text-decoration:none;color:".$textImpColor."\" href=\"$texttarget\">".$textad."</a>";
	}

	}

    return "<a target=_blank style=\"text-decoration:none;color:".$textImpColor."\" href=\"$defaulttarget\">".$defaulttext."</a>";
}

function __showtext($userid=0, $maxchars=30, $defaulttext="Text Ad", $defaulttarget="about:blank", $textImpColor) {
    $prefix = $_SESSION["prefix"];

    // Get website data
//	$getnewsiteid = mysql_query("Select id, bannerId, textId from `".$prefix."msites` where id = " . $_SESSION["newsiteid"] . " limit 1");
    $getnewsiteid = mysql_query("Select id, bannerId, textId from `".$prefix."msites` where id = " . get_current_site_id() . " limit 1");
	if (mysql_num_rows($getnewsiteid) < 1) {
//        echo "\n\n\n|||||||||||||||| \$getnewsiteid is empty ||||||||||||||||\n\n\n";
        return showtext($userid, $maxchars, $defaulttext, $defaulttarget, $textImpColor);
	}
    
    $newtextid = mysql_result($getnewsiteid, 0, "textId");
    if($newtextid != ''){
        
//        echo "\n\n\n######### \$newtextid: " . $newtextid . " #########\n\n\n";
        $imps_sub = 1; //
        $geturl = mysql_query("
            Select
                id,
                text,
                target
            from
                ".$prefix."mtexts
            where
                id = $newtextid
                and state = 1
                and imps >= $imps_sub
                and numchars <= $maxchars
            limit 1
        ");
        if (mysql_num_rows($geturl)<1) {
            return showtext($userid, $maxchars, $defaulttext, $defaulttarget, $textImpColor);
        }
        
        $textid = mysql_result($geturl, 0, "id");
        $textad = mysql_result($geturl, 0, "text");
        $imps_sub = 1;
        $texttarget = "clicklink.php?linkid=".$textid;
        @mysql_query("
            Update
                ".$prefix."mtexts
            set
                hits = hits+1,
                hitstoday = hitstoday+1,
                imps = imps-$imps_sub,
                lasthit = ".time()."
            where
                id = $textid
            limit 1
        ");
        return "<a target=_blank style=\"text-decoration:none;color:".$textImpColor."\" href=\"$texttarget\">".$textad."</a>";
    }

//    echo "\n\n\n@@@@@@@@@ \$newtextid is empty @@@@@@@@@\n\n\n";
    
    return showtext($userid, $maxchars, $defaulttext, $defaulttarget, $textImpColor);
}


function get_current_site_id() {
    $prefix = $_SESSION["prefix"];
    
    $site_url = null;
    
    if($_SESSION['switchframe1'] == 0){
        $site_url = $_SESSION['frame1site'];
    }elseif($_SESSION['switchframe2'] == 0){
        $site_url = $_SESSION['frame2site'];
    }
    
	$sitequery = mysql_query("SELECT * FROM `".$prefix."msites` WHERE `url` LIKE '" . $site_url . "' LIMIT 0 , 30");
	if (mysql_num_rows($sitequery) > 0) {
        return mysql_result($sitequery, 0, "id");
    }else{
        return null;
    }
}
?>