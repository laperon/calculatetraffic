<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.31
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

session_start();

include "inc/userauth.php";

$res = lfmsql_query("Select firstname, lastname, username, email, mtype, joindate from `".$prefix."members` where Id=".$_SESSION['userid']);
$usrid = $_SESSION["userid"];
$firstname = lfmsql_result($res, 0, "firstname");
$lastname = lfmsql_result($res, 0, "lastname");
$username = lfmsql_result($res, 0, "username");
$useremail = lfmsql_result($res, 0, "email");
$acctype = lfmsql_result($res, 0, "mtype");

$joindate = lfmsql_result($res, 0, "joindate");
$jointime = strtotime($joindate);
$timediff = time()-$jointime;
$userdays = $timediff/86400;
$userdays = round($userdays);

$_SESSION["prefix"] = $prefix;
require_once "inc/theme.php";

if (!isset($_SESSION['altgroup']) || !is_numeric($_SESSION['altgroup']) || $_SESSION['altgroup'] <= 0) {
	// Pick A Main Group
	$checkotogroups = lfmsql_query("Select * from ".$prefix."oto_groups where days<=$userdays AND (acctype=$acctype OR acctype=0) AND splitmirror=0 order by days asc");
} else {
	// Use The Session Group
	$checkotogroups = lfmsql_query("Select * from ".$prefix."oto_groups where days<=$userdays AND (acctype=$acctype OR acctype=0) AND id=".$_SESSION['altgroup']." limit 1");
}

if (lfmsql_num_rows($checkotogroups) == 0) {
	$_SESSION['offershown'] = 0;
	@lfmsql_query("Update ".$prefix."members set lastotoid='0/0/0' where Id=$usrid limit 1");
	header("Location: members.php");
	lfmsql_close();
	exit;
}

for ($i = 0; $i < lfmsql_num_rows($checkotogroups); $i++) {

$groupid = lfmsql_result($checkotogroups, $i, "id");
$splitmirrorval = lfmsql_result($checkotogroups, $i, "splitmirror");

if ($splitmirrorval == 0) {
	$originalgroup = $groupid;
} else {
	$originalgroup = $splitmirrorval;
}

//Check if the user has already seen the group
$checkstats = lfmsql_query("Select id from ".$prefix."oto_stats where userid=$usrid and groupid=$originalgroup limit 1");
if (lfmsql_num_rows($checkstats) == 0) {
	
	//See if the group has alternate split-test groups
	//if ($_SESSION['altgroup'] == 0) {
	//	$checkaltgroups = lfmsql_query("Select * from ".$prefix."oto_groups where splitmirror=$groupid and percent>0");
	//	if (lfmsql_num_rows($checkaltgroups) != 0) {
	//		//Get a alternate group
	//		$randnum = rand(0, 100);
	//		$getaltgroup = lfmsql_query("Select * from ".$prefix."oto_groups where (splitmirror=$groupid or id=$groupid) and highnum>=$randnum and lownum<=$randnum limit 1");
	//		if (lfmsql_num_rows($getaltgroup) > 0) {
	//			$groupid = lfmsql_result($getaltgroup, 0, "id");
	//		}
	//	}
	//}
	
	//Make sure the group has an offer
	$checkoffers = lfmsql_query("Select id from ".$prefix."oto_offers where groupid=$groupid");
	if (lfmsql_num_rows($checkoffers) > 0) {
		
		$firstoffer = lfmsql_result(lfmsql_query("Select rank from ".$prefix."oto_offers where groupid=$groupid ORDER BY rank ASC LIMIT 1"), 0);
		$lastoffer = lfmsql_result(lfmsql_query("Select rank from ".$prefix."oto_offers where groupid=$groupid ORDER BY rank DESC LIMIT 1"), 0);
		
		//Make sure the offer id is valid
		$offerid = $_SESSION['offershown'];
		if ((!is_numeric($offerid)) || ($offerid < 1)) {
			$_SESSION['offershown'] = 0;
			@lfmsql_query("Update ".$prefix."members set lastotoid='0/0/0' where Id=$usrid limit 1");
			header("Location: members.php");
			lfmsql_close();
			exit;
		}
		
		//See if all of the offers have been shown
		if ($offerid > $lastoffer) {
			$_SESSION['offershown'] = 0;
			@lfmsql_query("insert into ".$prefix."oto_stats (userid, groupid) values ($usrid, $originalgroup)");
			@lfmsql_query("Update ".$prefix."members set lastotoid='0/0/0' where Id=$usrid limit 1");
			header("Location: members.php");
			lfmsql_close();
			exit;
		}
		
		// Get the next offer
		$getoto = lfmsql_query("SELECT * FROM ".$prefix."oto_offers WHERE groupid=$groupid AND rank>=$offerid ORDER BY rank ASC LIMIT 1");
		if (lfmsql_num_rows($getoto) == 0) {
			$_SESSION['offershown'] = 0;
			@lfmsql_query("Update ".$prefix."members set lastotoid='0/0/0' where Id=$usrid limit 1");
			header("Location: members.php");
			lfmsql_close();
			exit;
		}
		
		$offerid = lfmsql_result($getoto, 0, "rank");
		
		$_SESSION['altgroup'] = $groupid;
		@lfmsql_query("Update ".$prefix."oto_offers set timesshown=timesshown+1 where groupid=$groupid and rank=$offerid limit 1");
		@lfmsql_query("Update ".$prefix."members set lastotoid='".$groupid."/".$offerid."/".$originalgroup."' where Id=$usrid limit 1");
		
		if ($offerid == $firstoffer) {
			@lfmsql_query("Update `".$prefix."oto_groups` set groupshown=groupshown+1 where id=$groupid limit 1");
		}
		
		$otoipn = lfmsql_result($getoto, 0, "ipn");
		$otohtml = lfmsql_result($getoto, 0, "text");
		
		if ($otoipn > 0) {
			$paymentcode = show_button_code($otoipn);
			$otohtml = str_replace('[paymentcode]', $paymentcode, $otohtml);
		}
		
		$otohtml = str_replace('[firstname]', $firstname, $otohtml);
		$otohtml = str_replace('[lastname]', $lastname, $otohtml);
		$otohtml = str_replace('[username]', $username, $otohtml);
		$otohtml=translate_site_tags($otohtml);
		$otohtml=translate_user_tags($otohtml,$useremail);
		
		$usetheme = 0;
		
		if(stristr($otohtml, '[themeheader]') != FALSE) {
			load_template ($theme_dir."/header.php");
			$usetheme = 1;
		}
		$otohtml = str_ireplace('[themeheader]', '', $otohtml);
		$otohtml = str_ireplace('[themefooter]', '', $otohtml);
		
		// Output The HTML Content
		$page_content = $otohtml;
		
		if ($usetheme == 1) {
			load_template ($theme_dir."/content.php");
			load_template ($theme_dir."/footer.php");
		} else {
			echo($page_content);
		}
		
		$_SESSION['offershown'] = $offerid + 1;
		
		lfmsql_close();
		exit;
		
	}
}

}

$_SESSION['offershown'] = 0;
@lfmsql_query("Update ".$prefix."members set lastotoid='0/0/0' where Id=$usrid limit 1");
header("Location: members.php");
lfmsql_close();
exit;

?>