<?php

// Grouping One-Time-Offer Mod
// �2009 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

require_once "inc/filter.php";

	session_start();

	include "inc/userauth.php";
	
	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");

$res = mysql_query("Select firstname, lastname, username, email, mtype, joindate from `".$prefix."members` where Id=".$_SESSION[userid]);
$usrid = $_SESSION["userid"];
$firstname = mysql_result($res, 0, "firstname");
$lastname = mysql_result($res, 0, "lastname");
$username = mysql_result($res, 0, "username");
$useremail = mysql_result($res, 0, "email");
$acctype = mysql_result($res, 0, "mtype");

$joindate = mysql_result($res, 0, "joindate");
$jointime = strtotime($joindate);
$timediff = time()-$jointime;
$userdays = $timediff/86400;
$userdays = round($userdays);

$spoid = $_GET['spoid'];
$thetime = time();

if (!is_numeric($spoid)) {
echo("Access Denied");
mysql_close;
exit;
}

$getspos = mysql_query("Select * from ".$prefix."spo where paused=0 AND (acctype=$acctype OR acctype=0) AND mdays<=$userdays AND id=$spoid limit 1");
if (mysql_num_rows($getspos) != 1) {
echo("Access Denied");
mysql_close;
exit;
}

$id = mysql_result($getspos, 0, "id");
$spoipn = mysql_result($getspos, 0, "ipn");
$memtype = mysql_result($getspos, 0, "memtype");
$reptype = mysql_result($getspos, 0, "reptype");
$spohtml = mysql_result($getspos, 0, "text");


//Recheck Stats
if ($memtype == 0) {
//Use Spostats Table
$getstats = mysql_query("Select lastshown from ".$prefix."spostats where spoid=$id and userid=$usrid limit 1");
if (mysql_num_rows($getstats) > 0) {
$lastshown = mysql_result($getstats, 0, "lastshown");
} else {
$lastshown = 0;
}
} else {
//Use User Table
$colname = "spo".$id;
$getstats = mysql_query("Select $colname from ".$prefix."members where Id=$usrid limit 1");
if (mysql_num_rows($getstats) > 0) {
$lastshown = mysql_result($getstats, 0, $colname);
} else {
$lastshown = 0;
}
}
if ($reptype != 0) {
//Offer Is Shown Multiple Times
if ($reptype == 1) {
$delaytime = 0;
}
elseif ($reptype == 2) {
$delaytime = 86400;
}
elseif ($reptype == 3) {
$delaytime = 604800;
}
elseif ($reptype == 4) {
$delaytime = 1209600;
}
elseif ($reptype == 5) {
$delaytime = 2635200;
}
elseif ($reptype == 6) {
$delaytime = 5270400;
}
elseif ($reptype == 7) {
$delaytime = 31536000;
}
$delaycheck = $thetime - $delaytime;
if ($lastshown > $delaycheck) {
echo("Access Denied");
mysql_close;
exit;
}
} else {
//Offer Is Only Shown Once
if ($lastshown != 0) {
echo("Access Denied");
mysql_close;
exit;
}
}

//Update Stats
if ($memtype == 0) {
//Use Spostats Table
if ($lastshown > 0) {
@mysql_query("Update ".$prefix."spostats set lastshown=$thetime where spoid=$id and userid=$usrid limit 1");
} else {
@mysql_query("Insert into ".$prefix."spostats (spoid, userid, lastshown) values ($id, $usrid, $thetime)");
}
} else {
//Use User Table
$colname = "spo".$id;
@mysql_query("Update ".$prefix."members set $colname=$thetime where Id=$usrid limit 1");
}

@mysql_query("Update ".$prefix."members set lastotoid='0/0/0' where Id=$usrid limit 1");

if ($spoipn > 0) {
$paymentcode = show_button_code($spoipn);
$spohtml = str_replace('[paymentcode]', $paymentcode, $spohtml);
}

$spohtml = str_replace('[firstname]', $firstname, $spohtml);
$spohtml = str_replace('[lastname]', $lastname, $spohtml);
$spohtml = str_replace('[username]', $username, $spohtml);
$spohtml=translate_site_tags($spohtml);
$spohtml=translate_user_tags($spohtml,$useremail);

echo("$spohtml");

mysql_close;
exit;

?>