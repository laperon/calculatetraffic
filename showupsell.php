<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

if (!isset($_GET['upsellkey']) || strlen($_GET['upsellkey']) < 1) {
	echo("Invalid Upsell URL");
	exit;
}

session_start();

if(isset($_SESSION["adminid"])) {
	require "inc/checkauth.php";
	require_once "inc/funcs.php";
	$res = mysql_query("Select Id, firstname, lastname, username, email from `".$prefix."members` ORDER BY Id ASC LIMIT 1") or die(mysql_error());
	if (mysql_num_rows($res) < 1) {
		echo("No Members In Database");
		exit;
	}
	$_SESSION["userid"] = $usrid = mysql_result($res, 0, "Id");
	$firstname = mysql_result($res, 0, "firstname");
	$lastname = mysql_result($res, 0, "lastname");
	$username = mysql_result($res, 0, "username");
	$useremail = mysql_result($res, 0, "email");
} else {
	require "inc/userauth.php";
	$res = mysql_query("Select firstname, lastname, username, email from `".$prefix."members` where Id=".$_SESSION['userid']) or die(mysql_error());
	if (mysql_num_rows($res) < 1) {
		echo("Session Error");
		exit;
	}
	$usrid = $_SESSION["userid"];
	$firstname = mysql_result($res, 0, "firstname");
	$lastname = mysql_result($res, 0, "lastname");
	$username = mysql_result($res, 0, "username");
	$useremail = mysql_result($res, 0, "email");
}

$_SESSION["prefix"] = $prefix;

require_once "inc/theme.php";

$getupsell = mysql_query("Select content from ".$prefix."upsells where openkey='".$_GET['upsellkey']."' limit 1");
if (mysql_num_rows($getupsell) != 1) {
	echo("Upsell Not Found");
	exit;
}

$upsellhtml = mysql_result($getupsell, 0, "content");

$upsellhtml = str_replace('[firstname]', $firstname, $upsellhtml);
$upsellhtml = str_replace('[lastname]', $lastname, $upsellhtml);
$upsellhtml = str_replace('[username]', $username, $upsellhtml);

$upsellhtml=translate_site_tags($upsellhtml);
$upsellhtml=translate_user_tags($upsellhtml,$useremail);

$usetheme = 0;

if(stristr($upsellhtml, '[themeheader]') != FALSE) {
	load_template ($theme_dir."/header.php");
	$usetheme = 1;
}
$upsellhtml = str_ireplace('[themeheader]', '', $upsellhtml);
$upsellhtml = str_ireplace('[themefooter]', '', $upsellhtml);

// Output The HTML Content
$page_content = $upsellhtml;

if ($usetheme == 1) {
	load_template ($theme_dir."/content.php");
	load_template ($theme_dir."/footer.php");
} else {
	echo($page_content);
}

exit;

?>