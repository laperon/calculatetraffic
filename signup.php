<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.17
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions
// of use of the script. The terms and conditions of use are
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

	session_start();
	require_once "inc/filter.php";
	include "inc/config.php";
	require_once "inc/funcs.php";
	require_once "inc/sql_funcs.php";


	$mconn=@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname,$mconn) or die( "Unable to select database");

	$formfields[] = Array();
	// Get the form settings into an array
	$frmres=@mysql_query("SELECT * FROM ".$prefix."signupform");
	while($frmrow=@mysql_fetch_array($frmres))
	{
		$fieldname=$frmrow["fieldname"];
		$formfields["$fieldname"] = $frmrow;
	}
	
	$s_res=@mysql_query("SELECT firstcookie, signup_checkbox_text FROM ".$prefix."settings");
	$s_row=@mysql_fetch_array($s_res);
	$firstcookie=$s_row["firstcookie"];
	$signup_checkbox_text=$s_row["signup_checkbox_text"];
	
	// Check for the hoplink before setting rid
	if(isset($_REQUEST["hop"]))
	{
		$cb=$_REQUEST["hop"];
		
		// Find out if this is a member
		$hopres=@mysql_query("SELECT Id FROM ".$prefix."members WHERE cb_id='$cb'");
		
		if(@mysql_num_rows($hopres) == 1)
		{
			// found the member
			$rid=@mysql_result($hopres,0);
			setcookie("rid", $_GET["rid"],time()+2592000);
			setcookie("cb", $_REQUEST["hop"],time()+2592000);
		}
	}
	else // Not clickbank, so do the usual RID/firstcookie check
	if(($firstcookie == 0 && isset($_GET["rid"]) && $_GET["rid"] != ""))
	{
		// rid request variable will override cookie
		if(isset($_GET["rid"]))
		{
			$rid=$_GET["rid"];
			setcookie("rid", $_GET["rid"],time()+2592000);
		}
	}
	else
	if(!isset($_COOKIE["rid"]))
	{
		setcookie("rid", $_GET["rid"],time()+2592000);
		$rid=$_GET["rid"];
	}
	else
	{
		$rid=$_COOKIE["rid"];
	}

	/////////////////////////////////////////////////////////////////
	//
	// Signup form posted. See sections below for enabling/disabling
	// validation on each field
	//
	// NOTE: Once you have made your changes, make a backup copy of 
	// this file or copy/paste this validation section into a text
	// file so that you can easily re-set it when future updates
	// become available.
	/////////////////////////////////////////////////////////////////
	if($_REQUEST["Submit"] == "Submit")
	{
        // Input Filters
	$_POST["firstname"] = $_REQUEST["firstname"] = text_filter($_REQUEST["firstname"], 1);
	$_POST["lastname"] = $_REQUEST["lastname"] = text_filter($_REQUEST["lastname"], 1);
	$_POST["username"] = $_REQUEST["username"] = text_filter($_REQUEST["username"], 2);
	$_POST["email"] = $_REQUEST["email"] = strtolower($_REQUEST["email"]);
	$_POST["email2"] = $_REQUEST["email2"] = strtolower($_REQUEST["email2"]);
	$_POST["paypal_email"] = $_REQUEST["paypal_email"] = strtolower($_REQUEST["paypal_email"]);
	
        $fee=0.0;
		$errmsg="";

		if($_SESSION["captcha"]!=$_REQUEST["captcha"])
		{
		    //CAPTCHA is invalid
			$errmsg="Security code is incorrect<br>";
		}
		//////////////////////////////////////////////////////////////////////
		// This is where the validation starts. You can comment out any field
		// that you don't want validated. See the example below
		//////////////////////////////////////////////////////////////////////
		
		// Example - if you did not want to validate First Name simply comment it out like this:
		
		// Validate Firstname
		// if(!isset($_REQUEST["firstname"]) || strlen(trim($_REQUEST["firstname"])) < 2)
		// {
		//	$errmsg="First Name must be at least 2 characters<br>";
		// }

		// Validate Firstname
		if($formfields["firstname"]["enable"] == 1 && $formfields["firstname"]["require"] == 1)
		{
			if(!isset($_REQUEST["firstname"]) || strlen(trim($_REQUEST["firstname"])) < 2)
			{
				$errmsg="First Name must be at least 2 characters<br>";
			}
		}
		
		// Validate Lastname
		if($formfields["lastname"]["enable"] == 1 && $formfields["lastname"]["require"] == 1)
		{
			if(!isset($_REQUEST["lastname"]) || strlen(trim($_REQUEST["lastname"])) < 2)
			{
				$errmsg.="Last Name must be at least 2 characters<br>";
			}
		}
		
		// Validate Email Address
		if(!isset($_REQUEST["email"]) || strlen(trim($_REQUEST["email"])) < 2)
		{
			$errmsg.="Please enter an email address<br>";
		}
		 elseif (check_email($_REQUEST["email"]) !== true) {
    		  	$errmsg.="Email address is invalid<br>";
      		}
      		
		if (bannedEmail($_REQUEST["email"]) !== false) { $errmsg.="Email address is banned<br>"; }
		
		// Validate Confirm Email Address
		if(trim($_REQUEST["email2"]) != trim($_REQUEST["email"]))
		{
			$errmsg.="Please confirm your email address<br>";
		}
		
		// Validate Address
		if($formfields["address"]["enable"] == 1 && $formfields["address"]["require"] == 1)
		{
			if(!isset($_REQUEST["address"]) || strlen(trim($_REQUEST["address"])) < 2)
			{
				$errmsg.="Please enter your address<br>";
			}
		}
			
		// Validate City
		if($formfields["city"]["enable"] == 1 && $formfields["city"]["require"] == 1)
		{
			if(!isset($_REQUEST["city"]) || strlen(trim($_REQUEST["city"])) < 2)
			{
				$errmsg.="Please enter your city<br>";
			}
		}
			
		// Validate State
		if($formfields["state"]["enable"] == 1 && $formfields["state"]["require"] == 1)
		{
			if(!isset($_REQUEST["state"]) || strlen(trim($_REQUEST["state"])) < 2)
			{
				$errmsg.="Please enter your state<br>";
			}
		}
			
		// Validate Postcode
		if($formfields["zip"]["enable"] == 1 && $formfields["zip"]["require"] == 1)
		{
			if(!isset($_REQUEST["postcode"]) || strlen(trim($_REQUEST["postcode"])) < 2)
			{
				$errmsg.="Please enter your postcode<br>";
			}
		}
		
		// Validate Telephone
		if($formfields["telephone"]["enable"] == 1 && $formfields["telephone"]["require"] == 1)
		{
			if(!isset($_REQUEST["telephone"]) || strlen(trim($_REQUEST["telephone"])) < 2)
			{
				$errmsg.="Please enter your telephone number<br>";
			}
		}
			
		// Validate Username
		if(!isset($_REQUEST["username"]) || strlen(trim($_REQUEST["username"])) < 4)
		{
			$errmsg.="Username must be at least 4 characters<br>";
		}
		
		// Validate Password
		if(!isset($_REQUEST["password"]) || strlen(trim($_REQUEST["password"])) < 4)
		{
			$errmsg.="Password must be at least 4 characters<br>";
		}
		
		// Validate Confirm Password
		if(trim($_REQUEST["password2"]) != trim($_REQUEST["password"]))
		{
			$errmsg.="Password/Confirm Password do not match<br>";
		}
		
		// Validate PayPal Email
		if($formfields["paypal"]["enable"] == 1 && $formfields["paypal"]["require"] == 1)
		{
			if(!isset($_REQUEST["paypal_email"]) || strlen(trim($_REQUEST["paypal_email"])) < 2)
			{
				$errmsg.="Please enter your PayPal email address<br>";
			}
		}
			
		if (isset($_REQUEST["paypal_email"]) && strlen(trim($_REQUEST["paypal_email"])) > 0) {
      			if (check_email($_REQUEST["paypal_email"]) !== true) {
				$errmsg.="PayPal address is invalid<br>";
			}
		}
			
		// Validate Clickbank ID
		if($formfields["clickbank"]["enable"] == 1 && $formfields["clickbank"]["require"] == 1)
		{
			if(!isset($_REQUEST["cb_id"]) || strlen(trim($_REQUEST["cb_id"])) < 2)
			{
				$errmsg.="Please enter your Clickbank ID<br>";
			}
		}

		// Validate Region
		if($formfields["geo"]["enable"] == 1 && $formfields["geo"]["require"] == 1)
		{
			if(!isset($_REQUEST["geo"]))
			{
				$errmsg.="Please select your region<br>";
			}
		}
		
		// Validate Membership Type
		if($_REQUEST["mtid"] == 0)
		{
			$errmsg.="Please select a membership type<br>";
		}
		
		// Validate Terms
		if(!isset($_REQUEST["termsagree"]))
		{
			$errmsg.="You must read and agree to our terms agreement<br>";
		}
		
		// Check Banned IP
		if (bannedIp($_SERVER['REMOTE_ADDR']) !== false) { $errmsg.="IP address is banned<br>"; }
		
		// Validate Custom Fields
		$getfields = mysql_query("SELECT id, name FROM `".$prefix."customfields` WHERE onsignup=1 AND required=1 ORDER BY rank ASC");
		if (mysql_num_rows($getfields) > 0) {
			for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
				$fieldid = mysql_result($getfields, $i, "id");
				$fieldname = mysql_result($getfields, $i, "name");
				
				$fieldpostname = "custom_".$fieldid;
				
				if (!isset($_REQUEST[$fieldpostname]) || strlen(trim($_REQUEST[$fieldpostname])) <= 0) {
					$errmsg.=$fieldname." is required<br>";
				}
			}
		}
		// End Validate Custom Fields
		
		//////////////////////////////////////////////////////////////////////
		// End of validation. You really shouldn't alter anything below this
		// line unless you know exactly what you are doing
		//////////////////////////////////////////////////////////////////////
		
		// Check if members should automatically opt-in to mailings
		$getreqver = mysql_query("Select reqemailver from ".$prefix."settings");
		$reqemailver = mysql_result($getreqver, 0, "reqemailver");
		if ($reqemailver == 0) {
			$getnewsletter = 1;
		} else {
			$getnewsletter = 0;
		}

		// Check that username and email are unique
		$uqry="SELECT count(*) AS ucount FROM ".$prefix."members WHERE email='".$_REQUEST["email"]."'";
		$ures=@mysql_query($uqry,$mconn);
		$urow=@mysql_fetch_array($ures);
		if($urow["ucount"] > 0)
		{
			// The email address is already in the database - send this member to the password recovery page
			$errmsg.="The email address '".$_REQUEST["email"]."' is already registered<br>";
		}
		
		$uqry="SELECT count(*) AS ucount FROM ".$prefix."members WHERE username='".$_REQUEST["username"]."'";
		$ures=@mysql_query($uqry,$mconn);
		$urow=@mysql_fetch_array($ures);
		if($urow["ucount"] > 0)
		{
			// The username is already in the database - send this member to the password recovery page
			$errmsg.="The username '".$_REQUEST["username"]."' is already registered<br>";
		}
		
		//Start Promo Code Check
		if ($_REQUEST["mtid"] > 1) {
		
		// Check whether membership requires promo code
		$res=@mysql_query("SELECT promocode FROM ".$prefix."ipn_products WHERE id=".$_REQUEST["mtid"]);
		$pcode=@mysql_result($res);
		if(strlen($pcode) > 0 && strcmp($pcode, $_REQUEST["pc"]) != 0)
		{
			exit;
		}
		
		// Get the fee for this account type
		$fqry="SELECT amount FROM ".$prefix."ipn_products WHERE id=".$_REQUEST["mtid"];
		$fres=@mysql_query($fqry,$mconn);
		$frow=@mysql_fetch_array($fres);
		$fee=$frow["accfee"];
		$useipn = 1;
		
		} else {
		$fee = 0;
		$useipn = 0;	
		}
		// End Promo Code Check

   	/* begin hitsconnect tracking code mod*/
      if(isset($_POST['jhcvp']) && is_numeric($_POST['jhcvp'])) {
    	   setcookie("jhcvc",$_GET['jhcv'],time()+86400);
   	   $_SESSION['jhcvs']=$_GET['jhcv'];
      }     
      /* end hitsconnect tracking code mod */   
      
		// FREE MEMBER ACCOUNT		
		// If it is a free account the member can be added and redirected 
		// to the login page

		if($useipn == 0 && $errmsg == "")
		{

			// Query settings table for sitename etc
			$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("290: Unable to find settings!");
			$row=@mysql_fetch_array($res);
			$sitename=$row["sitename"];

			if($row["autoresponder"] == 1 && !isset($_REQUEST["awdone"]))
    	    {
                $fullname=$_REQUEST["firstname"]." ".$_REQUEST["lastname"];
                $email=$_REQUEST["email"];
            ?>
                // Get our local variables
                $fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
                $root_path=substr($fullurl,0,strrpos($fullurl, "/"));
                $awreturnurl=$root_path."/signup.php";
                <html><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
                <title>Transferring...</title>
                </head><body>          
           
                <form name="aweber" action="http://www.aweber.com/scripts/addlead.pl?$SID" method="post">
                <input type="hidden" name="meta_web_form_id" value="<?=$row["a_meta_web_id"];?>">
                <input type="hidden" name="meta_split_id" value="">
                <input type="hidden" name="unit" value="<?=$row["a_unit"];?>">
				<input type="hidden" name="redirect" value="<?=$awreturnurl?>">
				<input type="hidden" name="meta_redirect_onlist" value="<?=$awreturnurl?>">
                <input type="hidden" name="meta_adtracking" value="Registration">
                <input type="hidden" name="meta_message" value="1">
                <input type="hidden" name="meta_required" value="name,from">
                <input type="hidden" name="meta_forward_vars" value="1">
                <input type="hidden" name="name" id="name" value="<?=$fullname;?>">
                <input type="hidden" name="from" id="from" value="<?=$email;?>">
                <input type="hidden" name="awdone" value="1">
                <?
                // Add the other fields
                foreach($_REQUEST as $key => $value)
                {
					if($key != "phpbb2mysql_data" && $key != "phpbb2mysql_sid" && $key != "phpbb2mysql_t" && $key != "PHPSESSID" && $key != "cprelogin" && $key != "cpsession")
					{
                    	echo "<input type=\"hidden\" name=\"$key\" id=\"$key\" value=\"$value\">\n";
					}
                }
                ?>
                </form>
                <script type="text/javascript">
                document.forms['aweber'].submit();
                </script>
            <?
                echo "</body></html>";
                exit;
            }

			
                // Get ID of Everyone group
                $qry="SELECT groupid FROM ".$prefix."groups WHERE groupname = 'Everyone'";
                $grpres=@mysql_query($qry) or die(mysql_error());
                $grprow=@mysql_fetch_array($grpres);
                $eogroup=$grprow["groupid"];
                
               	$getreqver = mysql_query("Select reqemailver from ".$prefix."settings");
		$reqemailver = mysql_result($getreqver, 0, "reqemailver");
		if ($reqemailver == 0) {
			$memberstatus="Active";
		} else {
			$memberstatus="Unverified";
		}
            if(!$_REQUEST['rid']) {
                $refid = 0;
            } else {
                $refid = $_REQUEST['rid'];
            }

						//
                // Add the new member
				$signupip=$_SERVER['REMOTE_ADDR'];
				$vericode = md5(rand(100,10000));
				$qry="INSERT INTO ".$prefix."members(firstname,lastname,email,address,city,state,postcode,country,telephone,username,password,refid,geo,paypal_email,joindate,mtype,groupid,cb_id,status,signupip,vericode,newsletter) 
			 	VALUES('".$_REQUEST["firstname"]."','".$_REQUEST["lastname"]."','".$_REQUEST["email"]."','".$_REQUEST["address"]."','".$_REQUEST["city"]."','".$_REQUEST["state"]."','".$_REQUEST["postcode"]."','".$_REQUEST["country"]."','".$_REQUEST["telephone"]."','".$_REQUEST["username"]."','".md5($_REQUEST["password"])."','".$refid."','".$_REQUEST["geo"]."','".$_REQUEST["paypal_email"]."',NOW(),1,$eogroup,'".$_REQUEST["cb_id"]."','$memberstatus','$signupip','".$vericode."',".$getnewsletter.")";
				@mysql_query($qry,$mconn) or die(mysql_error());
				
		// Get the new member's ID
		$nures=@mysql_query("SELECT Id FROM ".$prefix."members WHERE email='".$_REQUEST["email"]."'",$mconn) or die(mysql_error());
		$nurow=@mysql_fetch_array($nures);
		$id=$nurow["Id"];
		
		// Add Custom Fields
		$getfields = mysql_query("SELECT id FROM `".$prefix."customfields` WHERE onsignup=1 ORDER BY rank ASC");
		if (mysql_num_rows($getfields) > 0) {
			for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
				$fieldid = mysql_result($getfields, $i, "id");
				
				$fieldpostname = "custom_".$fieldid;
				
				mysql_query("DELETE FROM ".$prefix."customvals WHERE fieldid='".$fieldid."' AND userid='".$id."'") or die(mysql_error());
				
				if (isset($_REQUEST[$fieldpostname]) && strlen(trim($_REQUEST[$fieldpostname])) > 0) {
					mysql_query("INSERT INTO ".$prefix."customvals (fieldid, userid, fieldvalue) VALUES ('".$fieldid."', '".$id."', '".$_REQUEST[$fieldpostname]."')") or die(mysql_error());
				}
			}
		}
		// End Add Custom Fields
				
				
 			// Pay the fixed commission if it is > 0
			// First we get the referrer and his/her membership type
			if($rid > 0)
			{
				// We may have a referrer to pay
				$rpres=@mysql_query("SELECT mtype FROM ".$prefix."members WHERE Id=$rid") or die(mysql_error());
				$rprow=mysql_fetch_array($rpres);
				$payee_mtype=$rprow["mtype"];
		
				// Now get the amount to pay (if applicable)
				$rpres=@mysql_query("SELECT mtid,accname,commfixed FROM ".$prefix."membertypes WHERE mtid=$payee_mtype");
				$rprow=@mysql_fetch_array($rpres);
				$commfixed=$rprow["commfixed"];
		
				// Pay the referrer
				if($commfixed > 0)
				{
					$rpres=@mysql_query("SELECT accname FROM ".$prefix."membertypes WHERE mtid=$mtid");
					$rprow=@mysql_fetch_array($rpres);
					$accname=$rprow["accname"];
					AddLog("Free Signup Ref Paying $commfixed to $rid");
					$rpres=@mysql_query("INSERT INTO ".$prefix."sales(affid,saledate,itemid,itemname,itemamount,commission) VALUES($rid,NOW(),'Free Member Signup','".$accname."',0,$commfixed)");
				}
				
				// Check if there is a second tier commission to pay
				// To do this we need to go through the same process for the referrer's referrer
				$m2qry="SELECT refid FROM ".$prefix."members WHERE Id=$rid";
				AddLog("Free Signup Ref: Get tier 2 referrer - ".$m2qry);
				$m2res=@mysql_query($m2qry);
				$m2row=@mysql_fetch_array($m2res);
				if($m2row["refid"] > 0)
				{
					$payee2 = $m2row["refid"];
	
					// Find out the referrer's member type for commission
					$aff2qry="SELECT mtype FROM ".$prefix."members WHERE Id=$payee2";
					$aff2res=@mysql_query($aff2qry);
					$aff2row=@mysql_fetch_array($aff2res);
							
					$acc2type=$aff2row["mtype"];
	
					// Now get the amount to pay (if applicable)
					$rpres=@mysql_query("SELECT mtid,accname,commfixed2 FROM ".$prefix."membertypes WHERE mtid=$acc2type");
					$rprow=@mysql_fetch_array($rpres);
					$commfixed2=$rprow["commfixed2"];
		
					if($commfixed2 > 0)
					{
						AddLog("Free Signup Ref Paying $commfixed2 to $payee2");
						$rpres=@mysql_query("SELECT accname FROM ".$prefix."membertypes WHERE mtid=$mtid");
						$rprow=@mysql_fetch_array($rpres);
						$accname=$rprow["accname"];
						$rpres=@mysql_query("INSERT INTO ".$prefix."sales(affid,saledate,itemid,itemname,itemamount,commission) VALUES($payee2,NOW(),'Free Member Signup','".$accname."',0,$commfixed2)");
					}
					else
					{
						AddLog("Member IPN: No tier 2 commission to be paid");
					}
				}
				else
				{
					AddLog("Member IPN: No tier 2 referrer found");
				}

			}
				$fullname=$_REQUEST["firstname"]." ".$_REQUEST["lastname"];
				
				// Get some system settings
				$sres=@mysql_query("SELECT sitename,replyaddress,auto_email,_2co_id,autoresponder,addtophpbb,phpbb_path,phpbb_group,phpbbver,arp_email,gvo_affiliate_name,gvo_campaign,gvo_form_id,m_spon_did,trwvid,trwvseries FROM ".$prefix."settings",$mconn);
				$srow=@mysql_fetch_array($sres);
				$sitename=$srow["sitename"];
				$replyemail=$srow["replyaddress"];
				$auto_email=$srow["auto_email"];
				
				$arp_email=$srow["arp_email"];
				
				$gvo_form_id=$srow["gvo_form_id"];
				$gvo_campaign=$srow["gvo_campaign"];
				$gvo_affiliate_name=$srow["gvo_affiliate_name"];
				
				$m_spon_did=$srow["m_spon_did"];
				
				$trwvid=$srow["trwvid"];
				$trwvseries=$srow["trwvseries"];
				
				
                // Add new user to phpbb
                if($srow["addtophpbb"] == 1)
                {
					// Get the PHPBB settings
			        $phpbbpath=$srow["phpbb_path"];
			        $phpbbconf=$phpbbpath."config.php";
					$phpbbver=$srow["phpbbver"];
		
					if(file_exists($phpbbconf))
					{
						include $phpbbconf;

						$phpbb_host=$dbhost;
						$phpbb_db=$dbname;
						$phpbb_user=$dbuser;
						$phpbb_pass=$dbpasswd;
						$phpbb_prefix=$table_prefix;
	                    $phpbb_group=$srow["phpbb_group"];
                	
						$username=$_REQUEST["username"];
						$password=$_REQUEST["password"];
						$email=$_REQUEST["email"];
					
						addsingleusertophpbb($username,$password,$email,$phpbb_host,$phpbb_db,$phpbb_user,$phpbb_pass,$phpbb_prefix,$phpbb_group,$phpbbver);

 						include "inc/config.php"; 
	  	                $mconn=@mysql_connect($dbhost,$dbuser,$dbpass);
        	            @mysql_select_db($dbname,$mconn) or die( "Unable to select database");

					}

                }                

				// Send a welcome email
				SendWelcomeEmail($_REQUEST["email"],0);
				
				if($row["notifydownline"] == 1 && $rid > 0)
				{
					SendNotifyDownline($rid);
				}
				
				// Get the new member's ID
				$nures=@mysql_query("SELECT Id FROM ".$prefix."members WHERE email='".$_REQUEST["email"]."'",$mconn) or die(mysql_error());
				$nurow=@mysql_fetch_array($nures);
				$id=$nurow["Id"];

				if(isset($_COOKIE["sp"]))
				{
					$spc=$_COOKIE["sp"];
					mysql_query("INSERT INTO ".$prefix."splitresults(memberid,salespage) VALUES($id,'$spc')");
				}

				// Send an email to the autoresponder
				if($srow["autoresponder"] == 2 && strlen($auto_email) > 3)
				{
					$msgbody = "Request from ".$fullname;
					$headers = "From: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
					$headers .= "Return-Path: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
					$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
					$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
				
					$subject = "Auto request from ".$sitename;
					mail($auto_email,$subject,$msgbody,$headers);
				}
				
				if($srow["autoresponder"] == 3 && strlen($arp_email) > 3)
				{
					// ARP autoresponder
					$msgbody = "Request from ".$fullname;
					$headers = "From: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
					$headers .= "Return-Path: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
					$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
					$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
				
					$subject = "Auto request from ".$sitename;
					mail($arp_email,$subject,$msgbody,$headers);
				}
				
				if($srow["autoresponder"] == 4 )
				{
					// GVO autoresponder
					define('POSTURL', 'http://www.gogvo.com/subscribe.php');
					 define('POSTVARS', 'Campaign='.urlencode($gvo_campaign).'&FormId='.urlencode($gvo_form_id).'&AffiliateName='.urlencode($gvo_affiliate_name).'&FullName='.$_REQUEST["firstname"].'%20'.$_REQUEST["lastname"].'&Email='.$_REQUEST["email"]);  // POST VARIABLES TO BE SENT
				
					 $ch = curl_init(POSTURL);
					 curl_setopt($ch, CURLOPT_POST      ,1);
					 curl_setopt($ch, CURLOPT_POSTFIELDS    ,POSTVARS);
					 curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
					 curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
					 curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
					 $Rec_Data = curl_exec($ch);
					 
				}
				
			if($srow["autoresponder"] == 5 )
				{
					// Multisponder Elite autoresponder
					$responderposturl = "http://multisponderelite.com/elite/form.php?form=".$m_spon_did;
					define('POSTURL', $responderposturl);
					define('POSTVARS', 'CustomFields[2]='.urlencode($_REQUEST["firstname"]).'&email='.urlencode($_REQUEST["email"]).'&CustomFields[3]='.urlencode($_REQUEST["lastname"]).'&format=h');  // POST VARIABLES TO BE SENT
					
					 $ch = curl_init(POSTURL);
					 curl_setopt($ch, CURLOPT_POST      ,1);
					 curl_setopt($ch, CURLOPT_POSTFIELDS    ,POSTVARS);
					 curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
					 curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
					 curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
					 $Rec_Data = curl_exec($ch);
					 
				}
				
				// Run Signup Mods
				$getmods = mysql_query("Select filename from `".$prefix."signupmods` where enabled=1");
				if (mysql_num_rows($getmods) > 0) {
				while ($modlist = mysql_fetch_array($getmods)) {
					$modfilename = trim($modlist['filename']);
					if (file_exists($modfilename)) {
						include($modfilename);
					}
				}
				}
				// End Run Signup Mods
				
			if($srow["autoresponder"] == 6)
				{
					// TrafficWave autoresponder
			                $fullname=$_REQUEST["firstname"]." ".$_REQUEST["lastname"];
			                $email=$_REQUEST["email"];
					$domainurl = "http://".$_SERVER["SERVER_NAME"];
			                
			                ?><center>Processing...</center>
			                <form name="trwave" action="http://www.trafficwave.net/cgi-bin/autoresp/inforeq.cgi" method="post">
							<input type="hidden" name="trwvid" value="<? echo($trwvid); ?>">
							<input type="hidden" name="series" value="<? echo($trwvseries); ?>">
							<input type="hidden" name="subscrLandingURL" value="<? echo($domainurl); ?>/thank_you.php?memberid=<?=$id;?>&emailadd=<?=$email;?>">
							<input type="hidden" name="confirmLandingURL" value="<? echo($domainurl); ?>/verify.php?id=<?=$id;?>&vcode=<?=$vericode;?>">
					                <input type="hidden" id="da_name" name="da_name" value="<?=$fullname;?>">
					                <input type="hidden" id="da_email" name="da_email" value="<?=$email;?>">
					                <input type="hidden" name="awdone" value="1">
			                </form>
			                <script type="text/javascript">
			                document.forms['trwave'].submit();
			                </script>
	            			<?
	            			exit;
				}
				
				echo "<script language=\"javascript\">";
				echo "window.location.href=\"thank_you.php?memberid=".$id."&emailadd=".$_REQUEST["email"]."\";";
				echo "</script>";
				exit;

		}
		
		// PAID MEMBER ACCOUNT
		// Check payment method
		if($useipn > 0 && trim($_REQUEST["payment_method"]) == "none")
		{
			$errmsg.="Please select a payment method";
		}		

		// All okay - process payment
		if($useipn > 0 && $errmsg == "")
		{
			// Query settings table for sitename etc
			$res=@mysql_query("SELECT * FROM ".$prefix."settings",$mconn) or die("522: Unable to find settings!");
			$row=@mysql_fetch_array($res);
			$sitename=$row["sitename"];

			if($row["autoresponder"] == 1 && !isset($_REQUEST["awdone"]))
			{
				$fullname=$_REQUEST["firstname"]." ".$_REQUEST["lastname"];
				$email=$_REQUEST["email"];
				// Get our local variables
				$fullurl = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
				$root_path=substr($fullurl,0,strrpos($fullurl, "/"));
				$awreturnurl=$root_path."/signup.php";
				echo "<html><head>";
				echo "";
				echo "<title>Transferring...</title>"; 
				echo "</head><body>";			
				?>
				<form name="aweber" action="http://www.aweber.com/scripts/addlead.pl?$SID" method="post">
				<input type="hidden" name="meta_web_form_id" value="<?=$row["a_meta_web_id"];?>">
				<input type="hidden" name="meta_split_id" value="">
				<input type="hidden" name="unit" value="<?=$row["a_unit"];?>">
				<input type="hidden" name="redirect" value="<?=$awreturnurl?>">
				<input type="hidden" name="meta_redirect_onlist" value="<?=$awreturnurl?>">
				<input type="hidden" name="meta_adtracking" value="Registration">
				<input type="hidden" name="meta_message" value="1">
				<input type="hidden" name="meta_required" value="name,from">
				<input type="hidden" name="meta_forward_vars" value="1">
				<input type="hidden" name="name" id="name" value="<?=$fullname;?>">
				<input type="hidden" name="from" id="from" value="<?=$email;?>">
				<input type="hidden" name="awdone" value="1">
				<?
			    // Add the other fields
			    foreach($_REQUEST as $key => $value)
    			{
			        echo "<input type=\"hidden\" name=\"$key\" id=\"$key\" value=\"$value\">\n";
			    }
				?>
				</form>
				<script type="text/javascript">
				document.forms['aweber'].submit();
				</script>
			<?
				echo "</body></html>";
				exit;
			}

            // Get ID of Everyone group
            $qry="SELECT groupid FROM ".$prefix."groups WHERE groupname = 'Everyone'";
            $grpres=@mysql_query($qry) or die(mysql_error());
            $grprow=@mysql_fetch_array($grpres);
            $eogroup=$grprow["groupid"];

               	$getreqver = mysql_query("Select reqemailver from ".$prefix."settings");
		$reqemailver = mysql_result($getreqver, 0, "reqemailver");
		if ($reqemailver == 0) {
			$memberstatus="Active";
		} else {
			$memberstatus="Unverified";
		}

			// Insert the new member
			$signupip=$_SERVER['REMOTE_ADDR'];
			$vericode = md5(rand(100,10000));
			$qry="INSERT INTO ".$prefix."members(firstname,lastname,email,address,city,state,postcode,country,telephone,username,password,refid,geo,paypal_email,joindate,mtype,groupid,cb_id,status,signupip,vericode,newsletter) 
			 	VALUES('".$_REQUEST["firstname"]."','".$_REQUEST["lastname"]."','".$_REQUEST["email"]."','".$_REQUEST["address"]."','".$_REQUEST["city"]."','".$_REQUEST["state"]."','".$_REQUEST["postcode"]."','".$_REQUEST["country"]."','".$_REQUEST["telephone"]."','".$_REQUEST["username"]."','".md5($_REQUEST["password"])."','".$_REQUEST["rid"]."','".$_REQUEST["geo"]."','".$_REQUEST["paypal_email"]."',NOW(),1,$eogroup,'".$_REQUEST["cb_id"]."','$memberstatus','$signupip','".$vericode."',".$getnewsletter.")";
				@mysql_query($qry,$mconn) or die(mysql_error());
				
		// Get the new member's ID
		$nures=@mysql_query("SELECT Id FROM ".$prefix."members WHERE email='".$_REQUEST["email"]."'",$mconn) or die(mysql_error());
		$nurow=@mysql_fetch_array($nures);
		$id=$nurow["Id"];
		
		// Add Custom Fields
		$getfields = mysql_query("SELECT id FROM `".$prefix."customfields` WHERE onsignup=1 ORDER BY rank ASC");
		if (mysql_num_rows($getfields) > 0) {
			for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
				$fieldid = mysql_result($getfields, $i, "id");
				
				$fieldpostname = "custom_".$fieldid;
				
				mysql_query("DELETE FROM ".$prefix."customvals WHERE fieldid='".$fieldid."' AND userid='".$id."'") or die(mysql_error());
				
				if (isset($_REQUEST[$fieldpostname]) && strlen(trim($_REQUEST[$fieldpostname])) > 0) {
					mysql_query("INSERT INTO ".$prefix."customvals (fieldid, userid, fieldvalue) VALUES ('".$fieldid."', '".$id."', '".$_REQUEST[$fieldpostname]."')") or die(mysql_error());
				}
			}
		}
		// End Add Custom Fields

			$fullname=$_REQUEST["firstname"]." ".$_REQUEST["lastname"];
				
			// Get some system settings
			$sres=@mysql_query("SELECT sitename,replyaddress,auto_email,_2co_id,autoresponder,addtophpbb,phpbb_path,phpbb_group,phpbbver,arp_email,gvo_affiliate_name,gvo_campaign,gvo_form_id,m_spon_did,trwvid,trwvseries FROM ".$prefix."settings",$mconn);
			$srow=@mysql_fetch_array($sres);
			$sitename=$srow["sitename"];
			$replyemail=$srow["replyaddress"];
			$auto_email=$srow["auto_email"];
			
			$arp_email=$srow["arp_email"];
			
			$gvo_form_id=$srow["gvo_form_id"];
			$gvo_campaign=$srow["gvo_campaign"];
			$gvo_affiliate_name=$srow["gvo_affiliate_name"];
				
			$m_spon_did=$srow["m_spon_did"];
			
			$trwvid=$srow["trwvid"];
			$trwvseries=$srow["trwvseries"];

           // Add new user to phpbb
            if($srow["addtophpbb"] == 1)
            {
				// Get the PHPBB settings
		        $phpbbpath=$srow["phpbb_path"];
		        $phpbbconf=$phpbbpath."config.php";
				$phpbbver=$srow["phpbbver"];
		
				if(file_exists($phpbbconf))
				{
					include $phpbbconf;
					$phpbb_host=$dbhost;
					$phpbb_db=$dbname;
					$phpbb_user=$dbuser;
					$phpbb_pass=$dbpasswd;
					$phpbb_prefix=$table_prefix;
                    $phpbb_group=$srow["phpbb_group"];
                	
					$username=$_REQUEST["username"];
					$password=$_REQUEST["password"];
					$email=$_REQUEST["email"];
					
					addsingleusertophpbb($username,$password,$email,$phpbb_host,$phpbb_db,$phpbb_user,$phpbb_pass,$phpbb_prefix,$phpbb_group,$phpbbver);
   	                $mconn=@mysql_connect($dbhost,$dbuser,$dbpass);
       	            @mysql_select_db($dbname,$mconn) or die( "Unable to select database");
				}
             }                

			// Send a welcome email
			SendWelcomeEmail($_REQUEST["email"]);			

			if($row["notifydownline"] == 1 && $rid > 0)
			{
				SendNotifyDownline($rid);
			}

			// Send an email to the autoresponder
			if($srow["autoresponder"] == 2 && strlen($auto_email) > 3)
			{
				$msgbody = "Request from ".$fullname;
				$headers = "From: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
				$headers .= "Return-Path: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
				$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
				$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
			
				$subject = "Auto request from ".$sitename;
				mail($auto_email,$subject,$msgbody,$headers);
			}
			
			if($srow["autoresponder"] == 3 && strlen($arp_email) > 3)
				{
					// ARP autoresponder
					$msgbody = "Request from ".$fullname;
					$headers = "From: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
					$headers .= "Return-Path: ".$fullname." <".$_REQUEST["email"].">"."\r\n";
					$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">"."\r\n";
					$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
				
					$subject = "Auto request from ".$sitename;
					mail($arp_email,$subject,$msgbody,$headers);
				}

			if($srow["autoresponder"] == 4 )
				{
					// GVO autoresponder
					define('POSTURL', 'http://www.gogvo.com/subscribe.php');
					 define('POSTVARS', 'Campaign='.urlencode($gvo_campaign).'&FormId='.urlencode($gvo_form_id).'&AffiliateName='.urlencode($gvo_affiliate_name).'&FullName='.$_REQUEST["firstname"].'%20'.$_REQUEST["lastname"].'&Email='.$_REQUEST["email"]);  // POST VARIABLES TO BE SENT
				
					 $ch = curl_init(POSTURL);
					 curl_setopt($ch, CURLOPT_POST      ,1);
					 curl_setopt($ch, CURLOPT_POSTFIELDS    ,POSTVARS);
					 curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
					 curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
					 curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
					 $Rec_Data = curl_exec($ch);
					 
				}
				
			if($srow["autoresponder"] == 5 )
				{
					// Multisponder Elite autoresponder
					$responderposturl = "http://multisponderelite.com/elite/form.php?form=".$m_spon_did;
					define('POSTURL', $responderposturl);
					define('POSTVARS', 'CustomFields[2]='.urlencode($_REQUEST["firstname"]).'&email='.urlencode($_REQUEST["email"]).'&CustomFields[3]='.urlencode($_REQUEST["lastname"]).'&format=h');  // POST VARIABLES TO BE SENT
					
					 $ch = curl_init(POSTURL);
					 curl_setopt($ch, CURLOPT_POST      ,1);
					 curl_setopt($ch, CURLOPT_POSTFIELDS    ,POSTVARS);
					 curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1); 
					 curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS 
					 curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL
					 $Rec_Data = curl_exec($ch);
					 
				}

			// Get the new member's ID
			$nures=@mysql_query("SELECT Id FROM ".$prefix."members WHERE email='".$_REQUEST["email"]."'",$mconn) or die(mysql_error());
			$nurow=@mysql_fetch_array($nures);
			$id=$nurow["Id"];
			$userid = $id;
			$_SESSION["userid"] = $userid;
			$_SESSION["prefix"] = $prefix;
			
			if(isset($_COOKIE["sp"]))
			{
				$spc=$_COOKIE["sp"];
				@mysql_query("INSERT INTO ".$prefix."splitresults(memberid,salespage) VALUES($id,'$spc')");
			}
			
			// Run Signup Mods
			$getmods = mysql_query("Select filename from `".$prefix."signupmods` where enabled=1");
			if (mysql_num_rows($getmods) > 0) {
			while ($modlist = mysql_fetch_array($getmods)) {
				$modfilename = trim($modlist['filename']);
				if (file_exists($modfilename)) {
					include($modfilename);
				}
			}
			}
			// End Run Signup Mods
			
			if($srow["autoresponder"] == 6)
				{
					// TrafficWave autoresponder
			                $fullname=$_REQUEST["firstname"]." ".$_REQUEST["lastname"];
			                $email=$_REQUEST["email"];
					$domainurl = "http://".$_SERVER["SERVER_NAME"];
			                
			                ?><center>Processing...</center>
			                <form name="trwave" action="http://www.trafficwave.net/cgi-bin/autoresp/inforeq.cgi" method="post">
							<input type="hidden" name="trwvid" value="<? echo($trwvid); ?>">
							<input type="hidden" name="series" value="<? echo($trwvseries); ?>">
							<input type="hidden" name="subscrLandingURL" value="<? echo($domainurl); ?>/thank_you.php?memberid=<?=$id;?>&emailadd=<?=$email;?>">
							<input type="hidden" name="confirmLandingURL" value="<? echo($domainurl); ?>/verify.php?id=<?=$id;?>&vcode=<?=$vericode;?>">
					                <input type="hidden" id="da_name" name="da_name" value="<?=$fullname;?>">
					                <input type="hidden" id="da_email" name="da_email" value="<?=$email;?>">
					                <input type="hidden" name="awdone" value="1">
			                </form>
			                <script type="text/javascript">
			                document.forms['trwave'].submit();
			                </script>
	            			<?
	            			exit;
				}

			echo("
			<html>
			<center>
			<p><b>Complete Your Signup</b></p>
			".show_button_code($_REQUEST["mtid"])."
			</center>
			</html>
			");
			
			exit;
				
		}
	}	

	/////////////////////////////////////////////////////////////////
	//
	// End of signup form posted section
	//
	/////////////////////////////////////////////////////////////////

    // Query settings table for sitename etc
    $res=@mysql_query("SELECT * FROM ".$prefix."settings",$mconn) or die("897: Unable to find settings!");
    $row=@mysql_fetch_array($res);
    $sitename=$row["sitename"];
	$firstcookie=$row["firstcookie"];
	$pp_curr=$row["pp_curr"];
	
	$promocode="N/A";
	if(isset($_REQUEST["pc"]) && strlen($_REQUEST["pc"]) > 0)
	{
		$promocode=$_REQUEST["pc"];
	}

	if(isset($_GET["pc"]) && strlen($_GET["pc"]) > 0)
	{
		$promocode=$_GET["pc"];
	}

  if ( $_SERVER["REQUEST_URI"] != '/' ) {
    include "inc/theme.php";
    load_template ($theme_dir."/header.php");
  }

// Check if any membership levels are visible
$mvres=@mysql_query("SELECT COUNT(*) as mvisible FROM ".$prefix."membertypes WHERE enabled=1");
$mvrow=@mysql_fetch_object($mvres);
if($mvrow->mvisible == 0 && $promocode == "N/A")
{

   $page_content = '<div align="center"><h3>Sorry - We Are Currently Not Accepting Members</h3><br/></div>';
   load_template ($theme_dir."/content.php");
   load_template ($theme_dir."/footer.php");
   exit;
}

// Get signup 'head' template
$shres=@mysql_query("SELECT template_data FROM ".$prefix."templates WHERE template_name='Signup'",$mconn);
$shrow=@mysql_fetch_array($shres);
if (function_exists('html_entity_decode'))
{
   $page_content = html_entity_decode($shrow["template_data"]);
}
else
{
   $page_content = unhtmlentities($shrow["template_data"]);
}

$page_content .= '<form name="loginfrm"  id="signup-form" method="post" action="signup.php">
<input type="hidden" name="pc" value="'.$promocode.'" />
<input type="hidden" name="rid" value="'.$rid.'" />';

/*
following code posts the campaign id back to signup.php
*/
if(isset($_GET['jhcv'])&&is_numeric($_GET['jhcv'])){
$page_content .= '<input type=hidden name=jhcvp value='.$_GET[jhcv].'>';
}elseif ($_COOKIE['jhcvc'] != "" && is_numeric($_COOKIE['jhcvc'])){
$page_content .= '<input type=hidden name=jhcvp value='.$_COOKIE[jhcvc].'>';
}elseif ($_SESSION['jhcvs'] != ""&& is_numeric($_SESSION['jhcvs'])){
$page_content .= '<input type=hidden name=jhcvp value='.$_SESSION[jhcvs].'>';
}elseif($_POST['jhcvp']!=""&& is_numeric($_POST['jhcvp'])){
$page_content .= '<input type=hidden name=jhcvp value='.$_POST[jhcvp].'>';
}
//end hitsconnect code

$page_content .= '<table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">';

if(strlen($errmsg) >= 1){ 
   $page_content .= '<tr>
    <td colspan="2" align="center"><font color="red" size="2"><strong>'.$errmsg.'</strong></font>&nbsp;</td>
    </tr>';
}
else {
   $page_content .= '<tr><td colspan="2" align="center">&nbsp;</td></tr>';   
}
  
$page_content .= '</table> <div class="form grey-border">
                    <div style="background-color: #99CC66;font-weight: bold; text-align: center; margin-bottom: 2%;width: 71%; padding: 5px;">SIGN UP</div>
                    <div id="reg-success" style="margin-bottom:8px;display:none;"><font style="margin-left:120px;color:#006600;font-family:Open Sans,sans-serif;font-size:0.95em;font-weight:bold;">Registration successfully done</font></div>
                    <div id="loading-reg"></div>';
  

$fee=0;	// Flag to display payment processor or N/A

if($promocode != "N/A" ) {
// More than one membership option (or a promocode has been entered)
   
	// If a valid promo code has been entered, we need to include that membership type and select it
	$pcqry="SELECT * FROM ".$prefix."ipn_products WHERE promocode='".$promocode."'";
	$pcres=@mysql_query($pcqry,$mconn);
	if(mysql_num_rows($pcres) > 0 && $promocode != "N/A")
	{
	
		$page_content .= '<tr>
<td align="left" nowrap="NOWRAP" class="formlabelbold">Membership Type:</td>
<td align="left">
<select name="mtid" onChange="this.form.submit();">
<option value="1">Standard Signup</option>';
	
		// Found one!
		while($pcrow=@mysql_fetch_array($pcres))
		{
			if(strlen($pcrow["promocode"]) > 0)
			{
				$fee=$pcrow["amount"];
				// Check for 2CO
            $page_content .= '<option value="'.$pcrow["id"].'" selected="selected">'.$pcrow["name"].' ($'.$fee.')</option>';
			}
		}
	}

	$page_content .= '</select></td></tr>';
 
}
else
{
   // Only one membership 'visible' .. get details
   $fee=0.00;
   $mtid_single=1;
   $page_content .= '<input type="hidden" name="mtid" value="1">';
}

if($formfields["firstname"]["enable"] == 1)
{
   $page_content .= '<legend><input name="firstname" type="text" size="'.$formfields["firstname"]["fieldsize"].'" id="firstname" value="'.$_REQUEST['firstname'].'" placeholder="Firstname" class="input-field" /></legend><br/>';
} 
if($formfields["lastname"]["enable"] == 1)
{
   $page_content .= '<legend><input name="lastname" type="text" size="'.$formfields["lastname"]["fieldsize"].'" id="lastname" value="'.$_REQUEST['lastname'].'" placeholder="Lastname" class="input-field" /></legend><br/>';
}
$page_content .= '<input name="email" type="text" id="email" value="'.$_REQUEST['email'].'" size="30" placeholder="Email Address" class="input-field" /></legend><br/><br/><br/>
  <legend><input name="email2" type="text" id="email2" value="'.$_REQUEST['email2'].'" size="30" placeholder="Confirm Email Address" class="input-field" /></legend><br/>';

if($formfields["address"]["enable"] == 1)
{
   $page_content .= '<tr>
    <td align="left" nowrap="NOWRAP" class="formlabelbold">Address:</td>
    <td align="left"><input name="address" type="text" size="'.$formfields["address"]["fieldsize"].'" id="address" value="'.$_REQUEST['address'].'" /></td>
  </tr>';
}

if($formfields["city"]["enable"] == 1)
{
   $page_content .= '<tr>
    <td align="left" nowrap="NOWRAP" class="formlabelbold">City:</td>
    <td align="left"><input name="city" type="text" size="'.$formfields["city"]["fieldsize"].'" id="city" value="'.$_REQUEST['city'].'" /></td>
  </tr>';
}

if($formfields["state"]["enable"] == 1)
{
   $page_content .= '<tr>
    <td align="left" nowrap="NOWRAP" class="formlabelbold">State/County:</td>
    <td align="left"><input name="state" type="text" size="'.$formfields["state"]["fieldsize"].'" id="state" value="'.$_REQUEST['state'].'" /></td>
  </tr>';
}

if($formfields["zip"]["enable"] == 1)
{
   $page_content .= '<tr>
    <td align="left" nowrap="NOWRAP" class="formlabelbold">Zip/Postcode:</td>
    <td align="left"><input name="postcode" type="text" size="'.$formfields["zip"]["fieldsize"].'" id="postcode" value="'.$_REQUEST['postcode'].'" /></td>
  </tr>';
}

if($formfields["country"]["enable"] == 1)
{
   $page_content .= '<legend><select name="country" id="country" class="input-field">
                    <OPTION VALUE="">Select Country</OPTION>
                    <OPTION VALUE="AF">AFGHANISTAN</OPTION>
                    <OPTION VALUE="AL">ALBANIA</OPTION>
                    <OPTION VALUE="DZ">ALGERIA</OPTION>
                    <OPTION VALUE="AS">AMERICAN SAMOA</OPTION>
                    <OPTION VALUE="AD">ANDORRA</OPTION>
                    <OPTION VALUE="AO">ANGOLA</OPTION>
                    <OPTION VALUE="AI">ANGUILLA</OPTION>
                    <OPTION VALUE="AQ">ANTARCTICA</OPTION>
                    <OPTION VALUE="AG">ANTIGUA AND BARBUDA</OPTION>
                    <OPTION VALUE="AR">ARGENTINA</OPTION>
                    <OPTION VALUE="AM">ARMENIA</OPTION>
                    <OPTION VALUE="AW">ARUBA</OPTION>
                    <OPTION VALUE="AU">AUSTRALIA</OPTION>
                    <OPTION VALUE="AT">AUSTRIA</OPTION>
                    <OPTION VALUE="AZ">AZERBAIJAN</OPTION>
                    <OPTION VALUE="BS">BAHAMAS</OPTION>
                    <OPTION VALUE="BH">BAHRAIN</OPTION>
                    <OPTION VALUE="BD">BANGLADESH</OPTION>
                    <OPTION VALUE="BB">BARBADOS</OPTION>
                    <OPTION VALUE="BY">BELARUS</OPTION>
                    <OPTION VALUE="BE">BELGIUM</OPTION>
                    <OPTION VALUE="BZ">BELIZE</OPTION>
                    <OPTION VALUE="BJ">BENIN</OPTION>
                    <OPTION VALUE="BM">BERMUDA</OPTION>
                    <OPTION VALUE="BT">BHUTAN</OPTION>
                    <OPTION VALUE="BO">BOLIVIA</OPTION>
                    <OPTION VALUE="BA">BOSNIA AND HERZEGOVINA</OPTION>
                    <OPTION VALUE="BW">BOTSWANA</OPTION>
                    <OPTION VALUE="BV">BOUVET ISLAND</OPTION>
                    <OPTION VALUE="BR">BRAZIL</OPTION>
                    <OPTION VALUE="IO">BRITISH INDIAN OCEAN TERR.</OPTION>
                    <OPTION VALUE="BN">BRUNEI DARUSSALAM</OPTION>
                    <OPTION VALUE="BG">BULGARIA</OPTION>
                    <OPTION VALUE="BF">BURKINA FASO</OPTION>
                    <OPTION VALUE="BI">BURUNDI</OPTION>
                    <OPTION VALUE="KH">CAMBODIA</OPTION>
                    <OPTION VALUE="CM">CAMEROON</OPTION>
                    <OPTION VALUE="CA">CANADA</OPTION>
                    <OPTION VALUE="CV">CAPE VERDE</OPTION>
                    <OPTION VALUE="KY">CAYMAN ISLANDS</OPTION>
                    <OPTION VALUE="CF">CENTRAL AFRICAN REPUBLIC</OPTION>
                    <OPTION VALUE="TD">CHAD</OPTION>
                    <OPTION VALUE="CL">CHILE</OPTION>
                    <OPTION VALUE="CN">CHINA</OPTION>
                    <OPTION VALUE="CX">CHRISTMAS ISLAND</OPTION>
                    <OPTION VALUE="CC">COCOS (KEELING) ISLANDS</OPTION>
                    <OPTION VALUE="CO">COLOMBIA</OPTION>
                    <OPTION VALUE="KM">COMOROS</OPTION>
                    <OPTION VALUE="CG">CONGO</OPTION>
                    <OPTION VALUE="CD">CONGO, DEMOCRATIC REPUBLIC</OPTION>
                    <OPTION VALUE="CK">COOK ISLANDS</OPTION>
                    <OPTION VALUE="CR">COSTA RICA</OPTION>
                    <OPTION VALUE="CI">COTE DIVOIRE</OPTION>
                    <OPTION VALUE="HR">CROATIA</OPTION>
                    <OPTION VALUE="CU">CUBA</OPTION>
                    <OPTION VALUE="CY">CYPRUS</OPTION>
                    <OPTION VALUE="CZ">CZECH REPUBLIC</OPTION>
                    <OPTION VALUE="DK">DENMARK</OPTION>
                    <OPTION VALUE="DJ">DJIBOUTI</OPTION>
                    <OPTION VALUE="DM">DOMINICA</OPTION>
                    <OPTION VALUE="DO">DOMINICAN REPUBLIC</OPTION>
                    <OPTION VALUE="EC">ECUADOR</OPTION>
                    <OPTION VALUE="EG">EGYPT</OPTION>
                    <OPTION VALUE="SV">EL SALVADOR</OPTION>
                    <OPTION VALUE="GQ">EQUATORIAL GUINEA</OPTION>
                    <OPTION VALUE="ER">ERITREA</OPTION>
                    <OPTION VALUE="EE">ESTONIA</OPTION>
                    <OPTION VALUE="ET">ETHIOPIA</OPTION>
                    <OPTION VALUE="FK">FALKLAND ISLANDS (MALVINAS)</OPTION>
                    <OPTION VALUE="FO">FAROE ISLANDS</OPTION>
                    <OPTION VALUE="FJ">FIJI</OPTION>
                    <OPTION VALUE="FI">FINLAND</OPTION>
                    <OPTION VALUE="FR">FRANCE</OPTION>
                    <OPTION VALUE="GF">FRENCH GUIANA</OPTION>
                    <OPTION VALUE="PF">FRENCH POLYNESIA</OPTION>
                    <OPTION VALUE="TF">FRENCH SOUTHERN TERR.</OPTION>
                    <OPTION VALUE="GA">GABON</OPTION>
                    <OPTION VALUE="GM">GAMBIA</OPTION>
                    <OPTION VALUE="GE">GEORGIA</OPTION>
                    <OPTION VALUE="DE">GERMANY</OPTION>
                    <OPTION VALUE="GH">GHANA</OPTION>
                    <OPTION VALUE="GI">GIBRALTAR</OPTION>
                    <OPTION VALUE="GR">GREECE</OPTION>
                    <OPTION VALUE="GL">GREENLAND</OPTION>
                    <OPTION VALUE="GD">GRENADA</OPTION>
                    <OPTION VALUE="GP">GUADELOUPE</OPTION>
                    <OPTION VALUE="GU">GUAM</OPTION>
                    <OPTION VALUE="GT">GUATEMALA</OPTION>
                    <OPTION VALUE="GN">GUINEA</OPTION>
                    <OPTION VALUE="GW">GUINEA-BISSAU</OPTION>
                    <OPTION VALUE="GY">GUYANA</OPTION>
                    <OPTION VALUE="HT">HAITI</OPTION>
                    <OPTION VALUE="HM">HEARD/MCDONALD ISLANDS</OPTION>
                    <OPTION VALUE="VA">HOLY SEE (VATICAN CITY STATE)</OPTION>
                    <OPTION VALUE="HN">HONDURAS</OPTION>
                    <OPTION VALUE="HK">HONG KONG</OPTION>
                    <OPTION VALUE="HU">HUNGARY</OPTION>
                    <OPTION VALUE="IS">ICELAND</OPTION>
                    <OPTION VALUE="IN">INDIA</OPTION>
                    <OPTION VALUE="ID">INDONESIA</OPTION>
                    <OPTION VALUE="IR">IRAN, ISLAMIC REPUBLIC</OPTION>
                    <OPTION VALUE="IQ">IRAQ</OPTION>
                    <OPTION VALUE="IE">IRELAND</OPTION>
                    <OPTION VALUE="IL">ISRAEL</OPTION>
                    <OPTION VALUE="IT">ITALY</OPTION>
                    <OPTION VALUE="JM">JAMAICA</OPTION>
                    <OPTION VALUE="JP">JAPAN</OPTION>
                    <OPTION VALUE="JO">JORDAN</OPTION>
                    <OPTION VALUE="KZ">KAZAKHSTAN</OPTION>
                    <OPTION VALUE="KE">KENYA</OPTION>
                    <OPTION VALUE="KI">KIRIBATI</OPTION>
                    <OPTION VALUE="KP">KOREA - DEMOCRATIC PEOPLES REP.</OPTION>
                    <OPTION VALUE="KR">KOREA - REPUBLIC OF</OPTION>
                    <OPTION VALUE="KW">KUWAIT</OPTION>
                    <OPTION VALUE="KG">KYRGYZSTAN</OPTION>
                    <OPTION VALUE="LA">LAO PEOPLE\'S DEMOCRATIC REP.</OPTION>
                    <OPTION VALUE="LV">LATVIA</OPTION>
                    <OPTION VALUE="LB">LEBANON</OPTION>
                    <OPTION VALUE="LS">LESOTHO</OPTION>
                    <OPTION VALUE="LR">LIBERIA</OPTION>
                    <OPTION VALUE="LY">LIBYAN ARAB JAMAHIRIYA</OPTION>
                    <OPTION VALUE="LI">LIECHTENSTEIN</OPTION>
                    <OPTION VALUE="LT">LITHUANIA</OPTION>
                    <OPTION VALUE="LU">LUXEMBOURG</OPTION>
                    <OPTION VALUE="MO">MACAO</OPTION>
                    <OPTION VALUE="MK">MACEDONIA - FORMER YUGOSLAV REP.</OPTION>
                    <OPTION VALUE="MG">MADAGASCAR</OPTION>
                    <OPTION VALUE="MW">MALAWI</OPTION>
                    <OPTION VALUE="MY">MALAYSIA</OPTION>
                    <OPTION VALUE="MV">MALDIVES</OPTION>
                    <OPTION VALUE="ML">MALI</OPTION>
                    <OPTION VALUE="MT">MALTA</OPTION>
                    <OPTION VALUE="MH">MARSHALL ISLANDS</OPTION>
                    <OPTION VALUE="MQ">MARTINIQUE</OPTION>
                    <OPTION VALUE="MR">MAURITANIA</OPTION>
                    <OPTION VALUE="MU">MAURITIUS</OPTION>
                    <OPTION VALUE="YT">MAYOTTE</OPTION>
                    <OPTION VALUE="MX">MEXICO</OPTION>
                    <OPTION VALUE="FM">MICRONESIA, FEDERATED STATES</OPTION>
                    <OPTION VALUE="MD">MOLDOVA, REPUBLIC OF</OPTION>
                    <OPTION VALUE="MC">MONACO</OPTION>
                    <OPTION VALUE="MN">MONGOLIA</OPTION>
                    <OPTION VALUE="MS">MONTSERRAT</OPTION>
                    <OPTION VALUE="MA">MOROCCO</OPTION>
                    <OPTION VALUE="MZ">MOZAMBIQUE</OPTION>
                    <OPTION VALUE="MM">MYANMAR</OPTION>
                    <OPTION VALUE="NA">NAMIBIA</OPTION>
                    <OPTION VALUE="NR">NAURU</OPTION>
                    <OPTION VALUE="NP">NEPAL</OPTION>
                    <OPTION VALUE="NL">NETHERLANDS</OPTION>
                    <OPTION VALUE="AN">NETHERLANDS ANTILLES</OPTION>
                    <OPTION VALUE="NC">NEW CALEDONIA</OPTION>
                    <OPTION VALUE="NZ">NEW ZEALAND</OPTION>
                    <OPTION VALUE="NI">NICARAGUA</OPTION>
                    <OPTION VALUE="NE">NIGER</OPTION>
                    <OPTION VALUE="NG">NIGERIA</OPTION>
                    <OPTION VALUE="NU">NIUE</OPTION>
                    <OPTION VALUE="NF">NORFOLK ISLAND</OPTION>
                    <OPTION VALUE="MP">NORTHERN MARIANA ISLANDS</OPTION>
                    <OPTION VALUE="NO">NORWAY</OPTION>
                    <OPTION VALUE="OM">OMAN</OPTION>
                    <OPTION VALUE="PK">PAKISTAN</OPTION>
                    <OPTION VALUE="PW">PALAU</OPTION>
                    <OPTION VALUE="PS">PALESTINIAN TERRITORY, OCCUPIED</OPTION>
                    <OPTION VALUE="PA">PANAMA</OPTION>
                    <OPTION VALUE="PG">PAPUA NEW GUINEA</OPTION>
                    <OPTION VALUE="PY">PARAGUAY</OPTION>
                    <OPTION VALUE="PE">PERU</OPTION>
                    <OPTION VALUE="PH">PHILIPPINES</OPTION>
                    <OPTION VALUE="PN">PITCAIRN</OPTION>
                    <OPTION VALUE="PL">POLAND</OPTION>
                    <OPTION VALUE="PT">PORTUGAL</OPTION>
                    <OPTION VALUE="PR">PUERTO RICO</OPTION>
                    <OPTION VALUE="QA">QATAR</OPTION>
                    <OPTION VALUE="RE">REUNION</OPTION>
                    <OPTION VALUE="RO">ROMANIA</OPTION>
                    <OPTION VALUE="RU">RUSSIAN FEDERATION</OPTION>
                    <OPTION VALUE="RW">RWANDA</OPTION>
                    <OPTION VALUE="SH">SAINT HELENA</OPTION>
                    <OPTION VALUE="KN">SAINT KITTS AND NEVIS</OPTION>
                    <OPTION VALUE="LC">SAINT LUCIA</OPTION>
                    <OPTION VALUE="PM">SAINT PIERRE AND MIQUELON</OPTION>
                    <OPTION VALUE="VC">SAINT VINCENT AND THE GRENADINES</OPTION>
                    <OPTION VALUE="WS">SAMOA</OPTION>
                    <OPTION VALUE="SM">SAN MARINO</OPTION>
                    <OPTION VALUE="ST">SAO TOME AND PRINCIPE</OPTION>
                    <OPTION VALUE="SA">SAUDI ARABIA</OPTION>
                    <OPTION VALUE="SN">SENEGAL</OPTION>
                    <OPTION VALUE="CS">SERBIA AND MONTENEGRO</OPTION>
                    <OPTION VALUE="SC">SEYCHELLES</OPTION>
                    <OPTION VALUE="SL">SIERRA LEONE</OPTION>
                    <OPTION VALUE="SG">SINGAPORE</OPTION>
                    <OPTION VALUE="SK">SLOVAKIA</OPTION>
                    <OPTION VALUE="SI">SLOVENIA</OPTION>
                    <OPTION VALUE="SB">SOLOMON ISLANDS</OPTION>
                    <OPTION VALUE="SO">SOMALIA</OPTION>
                    <OPTION VALUE="ZA">SOUTH AFRICA</OPTION>
                    <OPTION VALUE="GS">SOUTH GEORGIA/SOUTH SANDWICH IS.</OPTION>
                    <OPTION VALUE="ES">SPAIN</OPTION>
                    <OPTION VALUE="LK">SRI LANKA</OPTION>
                    <OPTION VALUE="SD">SUDAN</OPTION>
                    <OPTION VALUE="SR">SURINAME</OPTION>
                    <OPTION VALUE="SJ">SVALBARD AND JAN MAYEN</OPTION>
                    <OPTION VALUE="SZ">SWAZILAND</OPTION>
                    <OPTION VALUE="SE">SWEDEN</OPTION>
                    <OPTION VALUE="CH">SWITZERLAND</OPTION>
                    <OPTION VALUE="SY">SYRIAN ARAB REPUBLIC</OPTION>
                    <OPTION VALUE="TW">TAIWAN, PROVINCE OF CHINA</OPTION>
                    <OPTION VALUE="TJ">TAJIKISTAN</OPTION>
                    <OPTION VALUE="TZ">TANZANIA, UNITED REPUBLIC OF</OPTION>
                    <OPTION VALUE="TH">THAILAND</OPTION>
                    <OPTION VALUE="TL">TIMOR-LESTE</OPTION>
                    <OPTION VALUE="TG">TOGO</OPTION>
                    <OPTION VALUE="TK">TOKELAU</OPTION>
                    <OPTION VALUE="TO">TONGA</OPTION>
                    <OPTION VALUE="TT">TRINIDAD AND TOBAGO</OPTION>
                    <OPTION VALUE="TN">TUNISIA</OPTION>
                    <OPTION VALUE="TR">TURKEY</OPTION>
                    <OPTION VALUE="TM">TURKMENISTAN</OPTION>
                    <OPTION VALUE="TC">TURKS AND CAICOS ISLANDS</OPTION>
                    <OPTION VALUE="TV">TUVALU</OPTION>
                    <OPTION VALUE="UG">UGANDA</OPTION>
                    <OPTION VALUE="UA">UKRAINE</OPTION>
                    <OPTION VALUE="AE">UNITED ARAB EMIRATES</OPTION>
                    <OPTION VALUE="GB">UNITED KINGDOM</OPTION>
                    <OPTION VALUE="US" selected>UNITED STATES</OPTION>
                    <OPTION VALUE="UM">UNITED STATES MINOR OUTLYING IS.</OPTION>
                    <OPTION VALUE="UY">URUGUAY</OPTION>
                    <OPTION VALUE="UZ">UZBEKISTAN</OPTION>
                    <OPTION VALUE="VU">VANUATU</OPTION>
                    <OPTION VALUE="VE">VENEZUELA</OPTION>
                    <OPTION VALUE="VN">VIET NAM</OPTION>
                    <OPTION VALUE="VG">VIRGIN ISLANDS, BRITISH</OPTION>
                    <OPTION VALUE="VI">VIRGIN ISLANDS, U.S.</OPTION>
                    <OPTION VALUE="WF">WALLIS AND FUTUNA</OPTION>
                    <OPTION VALUE="EH">WESTERN SAHARA</OPTION>
                    <OPTION VALUE="YE">YEMEN</OPTION>
                    <OPTION VALUE="ZM">ZAMBIA</OPTION>
                    <OPTION VALUE="ZW">ZIMBABWE</OPTION>
                  </select></legend><br/>';
}
if($formfields["telephone"]["enable"] == 1)
{
   $page_content .= '<tr>
    <td align="left" nowrap="NOWRAP" class="formlabelbold">Telephone:</td>
    <td align="left"><input name="telephone" type="text" size="'.$formfields["telephone"]["fieldsize"].'" id="telephone" value="'.$_REQUEST['telephone'].'" /></td>
  </tr>';
}

$page_content .= '<legend><input name="username" type="text" id="username" value="'.$_REQUEST['username'].'" placeholder="Username" class="input-field"  /></legend><br/>
  <legend><input name="password" type="password" id="password" value="'.$_REQUEST['password'].'" placeholder="Password" class="input-field" /></legend><br/>
  <legend><input name="password2" type="password" id="password2" value="'.$_REQUEST['password2'].'" placeholder="Confirm password" class="input-field" /></legend><br/>';

if($formfields["paypal"]["enable"] == 1)
{
   $page_content .= '<tr>
    <td align="left" nowrap="nowrap" class="formlabelbold">PayPal Email <br />
          (we will pay you via PayPal) </td>
    <td align="left" valign="top"><input name="paypal_email" size="'.$formfields["paypal"]["fieldsize"].'" type="text" id="paypal_email" value="'.$_REQUEST['paypal_email'].'" /></td>
  </tr>';
}

if($formfields["clickbank"]["enable"] == 1)
{
$page_content .= '<tr>
    <td align="left" nowrap="nowrap" class="formlabelbold">Clickbank ID</td>
    <td align="left" valign="top"><input name="cb_id" size="'.$formfields["clickbank"]["fieldsize"].'" type="text" id="cb_id" value="'.$_REQUEST['cb_id'].'" /></td>
  </tr>';
} 
 
if($formfields["geo"]["enable"] == 1)
{
   $page_content .= '<tr>
    <td colspan="2" align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Which part of the world do you live in? </font></strong></td>
    </tr>
  <tr valign="middle">
    <td colspan="2" align="left" nowrap="nowrap"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
      <input name="geo" type="radio" value="USA" '.(($_REQUEST["geo"] == "USA") ? 'checked="checked"' : '').' />USA/Canada
      <input name="geo" type="radio" value="UK" '.(($_REQUEST["geo"] == "UK") ? 'checked="checked"' : '').' />UK
      <input name="geo" type="radio" value="ME" '.(($_REQUEST["geo"] == "ME") ? 'checked="checked"' : '').' />Mainland Europe
      <input name="geo" type="radio" value="OT" '.(($_REQUEST["geo"] == "OT") ? 'checked="checked"' : '').' />Other</font></strong></td>
  </tr>';
}

// Custom Fields
$getfields = mysql_query("SELECT id, name, type, options FROM `".$prefix."customfields` WHERE onsignup=1 ORDER BY rank ASC");
if (mysql_num_rows($getfields) > 0) {
	for ($i = 0; $i < mysql_num_rows($getfields); $i++) {
		$fieldid = mysql_result($getfields, $i, "id");
		$fieldname = mysql_result($getfields, $i, "name");
		$fieldtype = mysql_result($getfields, $i, "type");
		$fieldoptions = mysql_result($getfields, $i, "options");
		
		$fieldpostname = "custom_".$fieldid;
		
		$page_content .= '
		<tr>
    			<td align="left" nowrap="nowrap" class="formlabelbold">'.$fieldname.': </td>
    			<td align="left" valign="top">';
    			
    			if ($fieldtype == 1) {
				$page_content .= '<input name="'.$fieldpostname.'" type="text" maxlength="255" id="'.$fieldpostname.'" value="'.$_REQUEST[$fieldpostname].'" />';
			} else {
				$page_content .= '<select name="'.$fieldpostname.'" id="'.$fieldpostname.'">';
				
				$splitoptions = explode(",", $fieldoptions);
				for ($j = 0; $j < count($splitoptions); $j++) {
					if (strlen(trim($splitoptions[$j])) > 0) {
						$splitoptions[$j] = trim($splitoptions[$j]);
						$page_content .= '<OPTION VALUE="'.$splitoptions[$j].'"';
						if ($_REQUEST[$fieldpostname] == $splitoptions[$j]) { $page_content .= ' selected="selected"'; }
						$page_content .= '>'.$splitoptions[$j].'</OPTION>';
					}
				}
				
				$page_content .= '</select>';
			}
			
			$page_content .= '</td>
		</tr>
		';
		
		
	}
}
// End Custom Fields

$page_content .= '<table width="100%"><tr>
    <td align="center" nowrap="nowrap">
	<img width="100px" height="50px" src="captcha.php" alt="captcha image"><br />
	<legend><input type="text" name="captcha" size="3" maxlength="3" placeholder="Security Code" class="input-field" />
	<font size="2" face="Verdana, Arial, Helvetica, sans-serif">(enter 3 black symbols)</font></legend>
    </td>
  </tr><br/>
  
  <tr>
    <td>I Agree To The <a target="_blank" href="terms.php">Terms</a>: </td>
    <td style="padding-right:410px;"><input type="checkbox" name="termsagree"></td>
  </tr></table><br/>
  
  <tr>
    <td colspan="2" align="center"><input type="submit" id="submit" name="Submit" value="Submit">   </td>
  </tr>';
  
    if($formfields["checkbox"]["enable"] == 1) {
		$page_content .= '<tr>
    <td align="center" nowrap="NOWRAP" class="formlabelbold" colspan = "3"><span style="font-size:12px;"><input type="checkbox"> '.$signup_checkbox_text.'</span></td>
  </tr>';
    }

$page_content .= '
<input type="hidden" name="fee" id="fee" value="'.$fee.'">';

if(isset($mtid_single)) {
   $page_content .= '<input type="hidden" name="mtid" id="mtid" value="'.$mtid_single.'">';
}

$page_content .= '</div></form></div></div></div></div></div>';

// Get the referers userid
$r_res=@mysql_query("SELECT firstname,lastname FROM ".$prefix."members WHERE Id=$rid",$mconn);
$r_row=@mysql_fetch_object($r_res);
$refname=$r_row->firstname." ".$r_row->lastname;

if(strlen($refname) > 1)
{
   $page_content .= '<br><center><font color="#999999" size="2" face="Verdana, Arial, Helvetica, sans-serif">[Your referrer is '.$refname.']</font></center><br/>';
}

if ( $_SERVER["REQUEST_URI"] == '/' ) {
  print $page_content;
} else {
  load_template ($theme_dir."/content.php");
  load_template ($theme_dir."/footer.php");
}
?>
