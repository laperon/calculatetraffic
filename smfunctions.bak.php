<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

function creditmember($userid=0, $acctype=0, $creditColor) {
$prefix = $_SESSION["prefix"];
if (!is_numeric($userid) || !is_numeric($acctype) || $acctype < 1 || $userid < 1 || $_SESSION["userid"] != $userid) {
	//Invalid Request
	$_SESSION['sysmess'] = "<span style=\"color: red;\"><b>SURFBAR ERRROR</b></span>";
	$_SESSION["siteviewed"] = 0;
} else {

	//Process session surfing time
	$getsurfingtime = mysql_query("Select lastclick, starttime, recordtime, timetoday, lasttoday from ".$prefix."members where Id=$userid limit 1");
	$lastclick = mysql_result($getsurfingtime, 0, "lastclick");
	$surfingtime = mysql_result($getsurfingtime, 0, "starttime");
	$recordtime = mysql_result($getsurfingtime, 0, "recordtime");
	$timetoday = mysql_result($getsurfingtime, 0, "timetoday");
	$lasttoday = mysql_result($getsurfingtime, 0, "lasttoday");
	$currenttime = time();

	if ($lastclick < ($currenttime-120)) {
		//New surfing session
		@mysql_query("Update ".$prefix."members set starttime=".$currenttime.", lasttoday=0 where Id=$userid limit 1");
		$lasttoday = 0;
	} else {
		if (($currenttime-$surfingtime) > $recordtime) {
			//Surfing record for this member
			$newrecordtime = $currenttime-$surfingtime;
			@mysql_query("Update ".$prefix."members set recordtime=".$newrecordtime." where Id=$userid limit 1");
			}

		$checksurflimit = mysql_query("Select sessiontime from ".$prefix."anticheat where id=1 limit 1");
		$surflimit = mysql_result($checksurflimit, 0, "sessiontime");
		$surfingseconds = $currenttime-$surfingtime;
		$surfinghours = ($surfingseconds/60)/60;

		if ($surfinghours >= $surflimit) {
			    $_SESSION["uid"] = "";
			    $_SESSION["password"] = "";
			    if (isset($_COOKIE[session_name()])) {
	    		    @setcookie(session_name(), '', time()-42000, '/');
			    }
			    if (isset($_COOKIE["lfmpr3s5gy"])) {
    			    @setcookie("lfmpr3s5gy", '', time()-42000, '/');
			    }
		    	@session_destroy();
		    	echo("You have reached the surfing limit for one session.  Please take a break and come back later.");
			exit;
		}

		$timediff = $currenttime-$lasttoday;
		@mysql_query("Update ".$prefix."members set timetoday=timetoday+".$timediff." where Id=$userid limit 1");
		$timetoday = $timetoday+$timediff;

	}
	//End process session surfing time

	//Check daily surfing time
	$checksurflimit = mysql_query("Select maxtime from ".$prefix."anticheat where id=1 limit 1");
	$daylimit = mysql_result($checksurflimit, 0, "maxtime");
	$dayhours = ($timetoday/60)/60;

	if ($dayhours >= $daylimit) {
			    $_SESSION["uid"] = "";
			    $_SESSION["password"] = "";
			    if (isset($_COOKIE[session_name()])) {
	    		    @setcookie(session_name(), '', time()-42000, '/');
			    }
			    if (isset($_COOKIE["lfmpr3s5gy"])) {
    			    @setcookie("lfmpr3s5gy", '', time()-42000, '/');
			    }
		    	@session_destroy();
		    	echo("You have reached the surfing limit for one day.  Please take a break and come back tomorrow.");
			exit;
	}
	//End check daily surfing time


	$getratio = mysql_query("Select surfratio, hitvalue from ".$prefix."membertypes where mtid=$acctype limit 1");
	$surfratio = mysql_result($getratio, 0, "surfratio");
	$hitvalue = mysql_result($getratio, 0, "hitvalue");

	if ($surfratio < 0) {
		//Invalid Surfing Ratio
		$_SESSION['sysmess'] = "<span style=\"color: red;\"><b>Surf Ratio Error</b></span>";
		$_SESSION["siteviewed"] = 0;
	} else {

	//Start Dynamic Surf Ratios

	$getclicks = mysql_query("select clickstoday from ".$prefix."members where Id=$userid");
	if (mysql_num_rows($getclicks) != 0) {
	 $clickstoday = mysql_result($getclicks, 0, "clickstoday");
	} else {
	 $clickstoday = 0;
	}
	$getboost = mysql_query("select boost from ".$prefix."dynamic where accid=$acctype and clicks<=$clickstoday order by clicks desc limit 1");

	if (mysql_num_rows($getboost) > 0) {
		$surfratio = mysql_result($getboost, 0, "boost");
	}

	//End Dynamic Surf Ratios

	//Credit Boost

	$t = time();
	$getboost = mysql_query("SELECT surfboost FROM ".$prefix."cboost WHERE `starttime`<$t AND `endtime`>$t AND surfboost>1 AND (acctype=$acctype OR acctype=0) LIMIT 1;");

	if (mysql_num_rows($getboost) > 0) {
		$surfboost = mysql_result($getboost, 0, "surfboost");
		$surfratio = $surfratio*$surfboost;
	}

	//End Credit Boost

	//Reward Upline
	$upline = mysql_result(mysql_query("Select refid from ".$prefix."members where Id=$userid limit 1"),0);
	if ($upline > 0) {
	$checkupline = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."members where Id=$upline"),0);
	if ($checkupline == 1) {

		$refacctype = mysql_result(mysql_query("Select mtype from ".$prefix."members where Id=$upline limit 1"),0);
		$refcrds = mysql_result(mysql_query("Select refcrds from ".$prefix."membertypes where mtid=$refacctype limit 1"),0);

		if ($refcrds > 0) {
		$givetoref = $surfratio*($refcrds/100);
		@mysql_query("Update ".$prefix."members set credits=credits+$givetoref, creditstoday=creditstoday+$givetoref where Id=$upline limit 1");
		}

		//Reward Level 2 Upline
		$upline2 = mysql_result(mysql_query("Select refid from ".$prefix."members where Id=$upline limit 1"),0);
		if ($upline2 > 0) {
		$checkupline = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."members where Id=$upline2"),0);
		if ($checkupline == 1) {

			$refacctype2 = mysql_result(mysql_query("Select mtype from ".$prefix."members where Id=$upline2 limit 1"),0);
			$refcrds2 = mysql_result(mysql_query("Select refcrds2 from ".$prefix."membertypes where mtid=$refacctype2 limit 1"),0);

			if ($refcrds2 > 0) {
			$givetoref = $surfratio*($refcrds2/100);
			@mysql_query("Update ".$prefix."members set credits=credits+$givetoref, creditstoday=creditstoday+$givetoref where Id=$upline2 limit 1");
			}
		}
		}
		//End Reward Level 2 Upline
	}
	}
	//End Reward Upline


	//Auto Assign
	$toaccount = $surfratio;

	$geturls = mysql_query("Select id, assign from ".$prefix."msites where memid=$userid and assign>0");
	if (mysql_num_rows($geturls) > 0) {
	for ($i = 0; $i < mysql_num_rows($geturls); $i++) {
		$siteid = mysql_result($geturls, $i, "id");
		$siteassign = mysql_result($geturls, $i, "assign");
		$tosite = round($surfratio*($siteassign/100), 9);
		$toaccount = round($toaccount-$tosite, 9);
		$givetosite = round($tosite*$hitvalue, 9);
		@mysql_query("Update ".$prefix."msites set credits=credits+$givetosite where id=$siteid limit 1");
	}
	@mysql_query("Update ".$prefix."members set actsite=1 where Id=$userid limit 1");
	}
	//End Auto Assign

	if (($toaccount >= 0) && ($toaccount <= $surfratio)) {
		@mysql_query("Update ".$prefix."members set lastclick=".$currenttime.", lasttoday=".$currenttime.", goodclicks=goodclicks+1, clickstoday=clickstoday+1, credits=credits+$toaccount, creditstoday=creditstoday+$surfratio where Id=$userid limit 1");
	} else {
		//Invalid credit amount.  Reset and send back to auto assign page.
		@mysql_query("Update ".$prefix."msites set assign=0 where memid=$userid");
		header("Location: autoassign.php?surfreturn=1");
		exit;
	}

	if (is_numeric($_SESSION["siteviewed"]) && $_SESSION["siteviewed"] > 0) {
		@mysql_query("Update ".$prefix."msites set hits=hits+1, hitstoday=hitstoday+1, credits=credits-1 where id=".$_SESSION["siteviewed"]." limit 1");
		$_SESSION["siteviewed"] = 0;
	}

	$_SESSION['sysmess'] = "<span style=\"color:".$creditColor.";\"><b>Credited $surfratio credits</b></span>";

}
}
}

function wrongclick($userid=0) {
$prefix = $_SESSION["prefix"];
if (!is_numeric($userid) || $userid < 1 || $_SESSION["userid"] != $userid) {
	//Invalid Request
	$_SESSION['sysmess'] = "<span style=\"color: red;\"><b>SURFBAR ERRROR</b></span>";
	$_SESSION["siteviewed"] = 0;
} else {
	@mysql_query("Update ".$prefix."members set badclicks=badclicks+1, clickstoday=clickstoday+1 where Id=$userid limit 1");
	$_SESSION["siteviewed"] = 0;
	$_SESSION['sysmess'] = "<span style=\"color: red;\"><b>Incorrect click</b></span>";

	//Auto Suspend
	$getsettings = mysql_query("Select accuracy, gracep from ".$prefix."anticheat where id=1 limit 1");
	$clickgrace = mysql_result($getsettings, 0, "gracep");
	$accrequired = mysql_result($getsettings, 0, "accuracy");

	//If the click grace is too low for accurate detection, uses default of 20
	if ((!is_numeric($clickgrace)) || ($clickgrace <= 4)) {
		$clickgrace = 20;
	}

	//If the click accuracy requirement is too low or too high, uses default of 75%
	if ((!is_numeric($accrequired)) || ($accrequired < 5) || ($accrequired > 95)) {
		$accrequired = 75;
	}

	$accrequired = $accrequired/100;
	$getclicks = mysql_query("Select goodclicks, badclicks from ".$prefix."members where Id=$userid limit 1");
	$goodcount = mysql_result($getclicks, 0, "goodclicks");
	$badcount = mysql_result($getclicks, 0, "badclicks");
	$totalcount = $goodcount+$badcount;
	$accurate = $goodcount/$totalcount;

	if (($accrequired > $accurate) && ($totalcount > $clickgrace)) {
		@mysql_query("Update ".$prefix."members set status='Suspended' where Id=$userid limit 1");
	}
	//End Auto Suspend

}
}

function checkautoassign($userid=0, $acctype=0) {
$prefix = $_SESSION["prefix"];
if (!is_numeric($userid) || !is_numeric($acctype) || $acctype < 1 || $userid < 1 || $_SESSION["userid"] != $userid) {
	//Invalid Request
	header("Location: members.php?mf=lo");
	exit;
} else {

	$getrequired = mysql_query("Select minauto from ".$prefix."membertypes where mtid=$acctype limit 1");
	$minauto = mysql_result($getrequired, 0, "minauto");

	$countsites = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."msites where memid=$userid"),0);
	if ($countsites < 1) {
		//No sites in this member's account.  Send to My Sites page.
		header("Location: mysites.php?surfreturn=1");
		exit;
	}

	$totalassigned = mysql_result(mysql_query("Select SUM(assign) from ".$prefix."msites where memid=$userid"),0);
	if ($totalassigned < $minauto) {
		//Not enough is auto assigned.  Send to auto assign page.
		header("Location: autoassign.php?surfreturn=1");
		exit;
	}

	if (($totalassigned > 100) || ($totalassigned < 0)) {
		//Invalid auto-assign sum.  Reset and send back to auto assign page.
		@mysql_query("Update ".$prefix."msites set assign=0 where memid=$userid");
		header("Location: autoassign.php?surfreturn=1");
		exit;
	}

}
}

?>