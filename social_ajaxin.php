<?php

// LFMTE Social Surfbar
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

$output .= "
		var startsocial = surfdata.indexOf('#STARTSOCIAL#') + 13;
		if (surfdata.indexOf('#STARTSOCIAL#') > -1) {
			var endsocial = surfdata.indexOf('#ENDSOCIAL#');
			var socialdata = surfdata.substring(startsocial, endsocial);
			if (document.getElementById(\"socialsurfbar\") != null) {
				document.getElementById(\"socialsurfbar\").innerHTML = socialdata;
			}
		}
";

?>