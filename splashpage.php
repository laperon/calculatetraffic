<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.24
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if (!isset($_GET['splashid']) || !is_numeric($_GET['splashid'])) {
	echo("Invalid Splash ID");
	exit;
}

require_once "inc/filter.php";
require_once "inc/lfmsql.php";
include "inc/config.php";
lfmsql_connect($dbhost,$dbuser,$dbpass);
lfmsql_select_db($dbname) or die( "Unable to select database");

require_once "inc/funcs.php";

$domainurl = "http://".$_SERVER["SERVER_NAME"];

$rid = $_GET['rid'];

$spres=@lfmsql_query("SELECT * FROM ".$prefix."promotion WHERE type='splashpage' and id=".$_GET['splashid']);

if (lfmsql_num_rows($spres) == 0) {
	echo("Invalid Splash ID");
	exit;
}

$sprow=@lfmsql_fetch_array($spres);
$content=$sprow["content"];

if (isset($rid) && is_numeric($rid)) {
	$content = str_replace("#REFURL#", $domainurl."/index.php?rid=".$rid, $content);
	$content = str_replace("#AFFILIATEID#", $rid, $content);
} else {
	$content = str_replace("#REFURL#", $domainurl."/index.php", $content);
	$content = str_replace("#AFFILIATEID#", "0", $content);
}

// Start Auto Conversion Tracking
if (isset($_GET['srtrkck']) && $_GET['srtrkck'] == "1") {
	echo("<!--srtrkvalid-->");
}
if (isset($_GET['srtrkdm']) && strlen($_GET['srtrkdm'])>3 && isset($_GET['srtrkid']) && is_numeric($_GET['srtrkid']) && $_GET['srtrkid']>0) {
	@lfmsql_query("INSERT INTO `".$prefix."autotrack` (timehit, ipaddress, srtrkdm, srtrkid) VALUES ('".time()."', '".$_SERVER['REMOTE_ADDR']."', '".$_GET['srtrkdm']."', '".$_GET['srtrkid']."')");
}
// End Auto Conversion Tracking

$content = translate_site_tags($content);

// Ref's User Macros
if (isset($rid) && is_numeric($rid)) {
	$checkexists = lfmsql_result(lfmsql_query("SELECT COUNT(*) FROM ".$prefix."members WHERE Id='".$rid."' AND status='Active'"), 0);
	if ($checkexists > 0) {
		$ridsearch = " WHERE Id='".$rid."'";
	} else {
		$ridsearch = "";
	}
} else {
	$ridsearch = "";
}
$getusermail = lfmsql_query("SELECT email FROM ".$prefix."members".$ridsearch." ORDER BY Id LIMIT 1");
if (lfmsql_num_rows($getusermail) > 0) {
	$useremail = lfmsql_result($getusermail, 0, "email");
	$content = translate_user_tags($content, $useremail);
}
// End Ref's User Macros

echo($content);

exit;
?>