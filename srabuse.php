<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

// Make Sure The Campaign Is Valid
if (!isset($_GET['siteid']) || !is_numeric($_GET['siteid'])) {
	echo("Invalid Site ID.");
	exit;
}
if (!isset($_GET['rateid']) || !is_numeric($_GET['rateid'])) {
	echo("Invalid Rating ID.");
	exit;
}
$siteid = $_GET['siteid'];
$rateid = $_GET['rateid'];


####################

//Begin main page

####################

include "inc/theme.php";
load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>

<center><h4><b>Report A SourceRanks Rating</b></h4>

");

$getrating = mysql_query("SELECT abuseflag, comment FROM `tracker_rates` WHERE id='".$rateid."' AND tracker_id='".$siteid."' LIMIT 1") or die(mysql_error());
if (mysql_num_rows($getrating) < 1) {
	echo("<p><b>Rating ID Not Found</b></p>");
	exit;
}

$abuseflag = mysql_result($getrating, 0, "abuseflag");
$comment = mysql_result($getrating, 0, "comment");

if ($abuseflag == 0) {

	if ($_GET['confirm'] == "yes") {
		mysql_query("UPDATE `tracker_rates` SET abuseflag=1 WHERE id='".$rateid."' AND tracker_id='".$siteid."' LIMIT 1") or die(mysql_error());
		
		$notifyabuse = mysql_result(mysql_query("SELECT value FROM `tracker_settings` WHERE field='notifyabuse'"), 0);
		if ($notifyabuse == 1) {
			// Send Admin Notification
			$getadmininfo = mysql_query("SELECT email FROM `".$prefix."admin` ORDER BY id DESC LIMIT 1") or die(mysql_error());
			if (mysql_num_rows($getadmininfo) > 0) {
				$adminemail = mysql_result($getadmininfo, 0, "email");
				if ($adminemail != "") {
					$getsiteinfo = mysql_query("SELECT sitename, affurl FROM `".$prefix."settings` ORDER BY id DESC LIMIT 1") or die(mysql_error());
					if (mysql_num_rows($getsiteinfo) > 0) {
						$sitename = mysql_result($getsiteinfo, 0, "sitename");
						$siteurl = mysql_result($getsiteinfo, 0, "affurl");
						if ($siteurl != "") {
							$spliturl = explode("/", $siteurl);
							$sitedomain = $spliturl[2];
							$sitedomain = str_replace("www.", "", $sitedomain);
							
							$headers='From: "'.$sitename.'" <abusereports@'.$sitedomain.'>';
							
							$adminmessage = "One of your members have reported a SourceRanks comment as abuse.\n\nTo check the abuse report, login to your Admin area at:\n\nhttp://".$sitedomain."/admin\n\nThen click Source Ranks under the Rotator menu.\n\nThese notifications can be disabled under the Main Settings section of your Rotator/Tracker plugin.";
							
							@mail($adminemail, 'SourceRanks Abuse Report', $adminmessage, $headers);
						}
					}
				}
			}
		}
		
		echo("<p><b>Thank you.  The comment will be reviewed by an administrator.</b></p>");
		exit;
	}

	echo("
	
	<p><b>Are you sure you want to report the following comment as abuse?</b></p>
	
	<table border=1 bordercolor=black cellpadding=5 cellspacing=0 width=450>
	
	<tr>
	<td align=\"center\">
	<p align=\"left\"><font size=\"2\">".$comment."</font></p>
	</td>
	</tr>
	
	</table><br /><br />
	
	<p><b><a href=\"srabuse.php?siteid=$siteid&rateid=$rateid&confirm=yes\">Send Abuse Report</a></b></p>
	
	");

} else {
	echo("<p><b>This rating is pending admin review.</b></p>");
}

include $theme_dir."/footer.php";

exit;

?>