<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";

include "inc/config.php";
$dbconnectlink = mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die( "Unable to select database");

// Get The IP Address And Table
$ipaddress = $_SERVER['REMOTE_ADDR'];
if (!preg_match('/^([0-9]{1,3})\.([0-9]{1,3})\.' . '([0-9]{1,3})\.([0-9]{1,3})$/', $ipaddress, $range)) {
	echo("Unable to verify your IP Address.");
	exit;
}

// Get Verification Data
if (!isset($_GET['prizeid']) || !is_numeric($_GET['prizeid'])) {
	echo("Invalid prize access.");
	exit;
}

$prizeid = $_GET['prizeid'];

$verifyprize = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_prizeswon` WHERE prizeid='".$prizeid."' and ipaddress='".$ipaddress."'"), 0);
if ($verifyprize < 1) {
	echo("Could not verify prize download.");
	exit;
}

// Get Prize
$getprize = mysql_query("SELECT filename FROM `tracker_prizes` WHERE id='".$prizeid."' LIMIT 1");
if (mysql_num_rows($getprize) < 1) {
	echo("System Error: Prize ".$prizeid." could not be found in database.  Please contact support.");
	exit;
}

$prizefile = mysql_result($getprize, 0, "filename");
$prizepath = "srprizedir/".$prizefile;

if (!file_exists($prizepath)) {
	echo("System Error: Prize file ".$prizeid." could not be found in directory.  Please contact support.");
	exit;
}

// Send file stream
header('Content-Description: File Transfer');
header("Content-Type: application/zip");
header("Content-Disposition: attachment; filename=$prizefile");
header('Pragma: no-cache');
header('Expires: 0');

readfile($prizepath);

exit;

?>