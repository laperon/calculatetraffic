<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

// Show Source ID
if (isset($_GET["showsourceid"]) && is_numeric($_GET["showsourceid"]) && $_GET["showsourceid"] >= 0) {
            $showsourceid=trim($_GET["showsourceid"]);
} else {
	$showsourceid = -1;
}
// End Show Source ID

// Show Tracker ID
if (isset($_GET["showtrackerid"]) && is_numeric($_GET["showtrackerid"]) && $_GET["showtrackerid"] >= 0) {
            $showtrackerid=trim($_GET["showtrackerid"]);
} else {
	$showtrackerid = -1;
}
// End Show Tracker ID

// Show Rotator ID
if (isset($_GET["showrotatorid"]) && is_numeric($_GET["showrotatorid"]) && $_GET["showrotatorid"] >= 0) {
            $showrotatorid=trim($_GET["showrotatorid"]);
} else {
	$showrotatorid = -1;
}
// End Show Rotator ID

####################

//Begin main page

####################

include "inc/theme.php";
load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>

<center><h4><b>View SourceRanks Ratings</b></h4>
");

echo("<form action=\"srview.php?showtrackerid=".$showtrackerid."&showsourceid=".$showsourceid."&showrotatorid=".$showrotatorid."\" method=\"post\">
<p><b>Show: </b>
<select name=\"ratefilter\">
<option value=\"0\">All Ratings</option>
<option value=\"1\""); if ($_POST['ratefilter'] == 1) { echo(" selected"); } echo(">1 Stars</option>
<option value=\"2\""); if ($_POST['ratefilter'] == 2) { echo(" selected"); } echo(">2 Stars</option>
<option value=\"3\""); if ($_POST['ratefilter'] == 3) { echo(" selected"); } echo(">3 Stars</option>
<option value=\"4\""); if ($_POST['ratefilter'] == 4) { echo(" selected"); } echo(">4 Stars</option>
<option value=\"5\""); if ($_POST['ratefilter'] == 5) { echo(" selected"); } echo(">5 Stars</option>
</select>
<input  type=\"submit\" name=\"submit\" value=\"Go\">
</form><br />
");

$wherefilter = " WHERE a.abuseflag='0'";

if (is_numeric($showsourceid) && $showsourceid >= 0) {
	$wherefilter .= " AND a.source_id='".$showsourceid."'";
}

if (is_numeric($showtrackerid) && $showtrackerid >= 0) {
	$wherefilter .= " AND a.tracker_id='".$showtrackerid."'";
}

if (is_numeric($showrotatorid) && $showrotatorid >= 0) {
	$wherefilter .= " AND a.rotator_id='".$showrotatorid."'";
}

if ($_POST['ratefilter'] == 1) {
	$wherefilter .= " AND a.rate=1 ORDER BY a.id DESC";
} elseif ($_POST['ratefilter'] == 2) {
	$wherefilter .= " AND a.rate=2 ORDER BY a.id DESC";
} elseif ($_POST['ratefilter'] == 3) {
	$wherefilter .= " AND a.rate=3 ORDER BY a.id DESC";
} elseif ($_POST['ratefilter'] == 4) {
	$wherefilter .= " AND a.rate=4 ORDER BY a.id DESC";
} elseif ($_POST['ratefilter'] == 5) {
	$wherefilter .= " AND a.rate=5 ORDER BY a.id DESC";
} else {
	$wherefilter .= " ORDER BY a.id DESC";
}


$getstats = mysql_query("SELECT a.id AS rateid, a.tracker_id AS siteid, a.rate AS rating, a.date AS date, a.comment AS comment, b.name AS trackername, c.url AS sourcename FROM `tracker_rates` a LEFT JOIN `tracker_urls` b ON (a.tracker_id=b.id) LEFT JOIN `tracker_sources` c ON (a.source_id=c.id)".$wherefilter);

if (mysql_num_rows($getstats) > 0) {

for ($i = 0; $i < mysql_num_rows($getstats); $i++) {
	$num = $i+1;
	$rateid = mysql_result($getstats, $i, "rateid");
	$siteid = mysql_result($getstats, $i, "siteid");
	$rating = mysql_result($getstats, $i, "rating");
	$ratedate = mysql_result($getstats, $i, "date");
	$comment = mysql_result($getstats, $i, "comment");
	$trackername = mysql_result($getstats, $i, "trackername");
	$sourcename = mysql_result($getstats, $i, "sourcename");
	
	if ($sourcename == null) { $sourcename = "unknown"; }
	
	if ($rating > 0) {
		$rateimg = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
		for ($ratecount = 0; $ratecount < $rating; $ratecount++) {
			$rateimg .= "<td><img border=\"0\" src=\"srstar.gif\"></td>";
		}
		$rateimg .= "</tr></table>";
	} else {
		$rateimg = "N/A";
	}
	
	if ($comment == "none") {
		$comment = "<i>No Comments</i>";
	} else {
		$comment = $comment."<br /><br /><a target=\"_blank\" href=\"srabuse.php?siteid=$siteid&rateid=$rateid\"><font size=\"2\" color=\"red\">Report Abuse</font></a>";
	}
	
	echo("
<table border=1 bordercolor=black cellpadding=5 cellspacing=0 width=500>

<tr>
<td align=\"left\">
<b>#".$num."</b> ".$rateimg."</td>
<td align=\"left\">
<b>".$ratedate."</b>
</td>
</tr>

<tr>
<td colspan=2 align=\"center\">
<p align=\"left\"><font size=\"2\"><b>Tracker Name:</b> ".$trackername."</font></p>
<p align=\"left\"><font size=\"2\"><b>Source:</b> ".$sourcename."</font></p>

<p align=\"left\"><font size=\"2\">".$comment."</font></p>
</td>
</tr>

</table><br /><br />
");

}

} else {

echo("<p><font size=2><b>No Ratings Found</b></font></p>");

}


echo("
<br><br>");

include $theme_dir."/footer.php";

exit;

?>