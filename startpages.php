<?php
///////////////////////////////////////////////////////////
// AUTOMATED START PAGE SYSTEM v1.21		         //
// (c) 2007-2009 Simon B Kelly. All rights reserved.     //
// http://replytosimon.com                               //
//                                                       //
// Not for resale. Sold and installed exclusively by:    //
// Simon B Kelly and Josh Abbott, http://trafficmods.com //
///////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$countcredits = mysql_query("select credits, mtype, email from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");
$useremail = mysql_result($countcredits, 0, "email");

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

$getuserdata = mysql_query("Select email, mtype from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
include "inc/theme.php";
load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");

####################

//Begin main page

####################

$time = time();
echo '<center>
<font face="Tahoma" size=4><b>My Start Pages</b></font>
<p><font size=2>';

if($_POST[submit]=="Edit") {
	mysql_query("UPDATE ".$prefix."startpage SET url='$_POST[url]' WHERE id='$_POST[id]';");
	echo '<em>URL updated!</em><br><br>';
	}

$r=mysql_query("SELECT * FROM ".$prefix."startpage_settings;");
$row=mysql_fetch_array($r);
extract($row);

$result = mysql_query("SELECT * FROM ".$prefix."startpage WHERE `usrid`='$userid' ORDER BY `to` DESC;");
if(mysql_num_rows($result)) {
   echo 'You have purchased the following startpages:
<p>
<table align="center" border=0 cellpadding=2 cellspacing=2>
<tr bgcolor="#000000" style="font-size:11px; color:white;"><th>Dates Booked</th><th>URL</th><th width=50>Views Today</th><th width=50>Total Views</th><th width=50>Last Viewed</th><th width=50>Last<br>User ID</th></tr>
';
   while($row=mysql_fetch_array($result)) {
	extract($row);
	if($type=="D") { $date=date("j M Y",$from); }
	if($type=="W") { $date=date("j M Y",$from).' -<br>'.date("j M Y",$to); }
	if($type=="M") { $date=date("M Y",$from); }
	if($lastview) {
		if($time<$to) {
			$lastview=$time-$lastview.' secs ago';
			} else {
			$lastview=$date=date("j M Y",$to);
			}
		} else {
		$lastview='n/a';
		$lastusrid='n/a';
		}
	echo '<tr align="center" bgcolor="#F8F8F8"><td>'.$date.'</td>';
	if($time<$from || strlen($url)<12) { 
		echo '<FORM action="startpages.php" method="POST">
<INPUT type="hidden" name="id" value="'.$id.'">
<td nowrap="nowrap"><INPUT type="text" name="url" value="'.$url.'" size=22> <INPUT type="submit" name="submit" value="Edit"></td>
</FORM><td>n/a</td>';
		} else {
		echo '<td><INPUT type="text" name="url" value="'.$url.'" size=30></td><td>'.$viewstoday.'</td>';
		}
	echo '<td>'.$views.'</td><td>'.$lastview.'</td><td>'.$lastusrid.'</td></tr>';
	}
   echo '</table>';
   } else {
   echo '<p>We have no record of any purchased Start Pages.
<br>If you believe this information to be incorrect please contact us.';
   }

echo '<p>You can purchase future Start Pages <a href="buycredits.php">here</a>.</font>
</center><br><br>';

include $theme_dir."/footer.php";
exit;

?>