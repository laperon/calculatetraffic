<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.32
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

//Check for a bonus page
$t = time();
$date = date("Y-m-d H:i:s");
$checkbonus = lfmsql_query("Select url from `".$prefix."bonuspages` where (starttime=0 or (starttime<$t AND endtime>$t)) and (accid=0 or accid=$acctype) and (shownum=$clickstoday or (showtype=1 and ($clickstoday%shownum=0) and ($clickstoday>shownum))) limit 1");

if (lfmsql_num_rows($checkbonus) > 0) {
	//Show Bonus
	$_SESSION['bonuscode'] = rand(100,10000);
	$sbonuscode = md5($_SESSION['bonuscode']);
	$newsite = lfmsql_result($checkbonus, 0, "url");

	if(strpos($newsite, "?") === false) {
		$newsite .= "?securecode=".$sbonuscode;
	} else {
		$newsite .= "&securecode=".$sbonuscode;
	}
}

?>