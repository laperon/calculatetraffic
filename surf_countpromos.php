<?
////////////////////////////////////////////////////////
// LFMTE ACTIVE SURFER REWARDS MOD 
// (c) 2008-2011 Simon B Kelly. All rights reserved.
// http://replytosimon.com  
//    
// Not for resale.
// Sold at: http://thetrafficexchangescript.com
////////////////////////////////////////////////////////

// Cross Promo Addition
// �2011 Josh Abbott, http://trafficmods.com
// Licensed for the LFMTE script

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

// the following variables have already been retrieved in `surfing.php`:
// $userid, $acctype, $clickstoday

$time = time();

// The script only checks for a Word Search letter if their click is correct
if ($_SESSION["checkletter"] == 1) {

$getpromos = mysql_query("Select id, timediff, promokey from `".$prefix."active_surfer_promos` where active>0 AND (starttime+timediff)<$time AND (endtime+timediff)>$time");

if (mysql_num_rows($getpromos) > 0) {
for ($i = 0; $i < mysql_num_rows($getpromos); $i++) {

	$promoid = mysql_result($getpromos, $i, "id");
	$timediff = mysql_result($getpromos, $i, "timediff");
	$promokey = mysql_result($getpromos, $i, "promokey");
	
	$promotime = $time+$timediff;
	$promoday = date("Y-m-d", $promotime);
	
	$entryexists = mysql_result(mysql_query("Select COUNT(*) from `".$prefix."active_surfer_promo_clicks` where userid=$userid and promoid=$promoid and date='".$promoday."'"), 0);
	
	if ($entryexists == 0) {
		mysql_query("Delete from `".$prefix."active_surfer_promo_clicks` where promoid=$promoid and date!='".$promoday."'");
		mysql_query("Insert into `".$prefix."active_surfer_promo_clicks` (userid, promoid, promokey, date, clickstoday) VALUES ($userid, $promoid, '".$promokey."', '".$promoday."', 1)");
	} else {
		mysql_query("Update `".$prefix."active_surfer_promo_clicks` set clickstoday=clickstoday+1 where userid=$userid and promoid=$promoid and date='".$promoday."'");
	}

}
}

}

?>