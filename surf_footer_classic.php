<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.08
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

$output .= "

<style type=\"text/css\">

html, body, div, iframe { margin:0; padding:0; height:100%; }
iframe { display:block; width:100%; border:none; }

#surf_footer {
display:block;
text-align:center;
vertical-align: middle;

margin:0;
padding:0;
position:absolute;
bottom:0;
right:0;
width:100%;
height:".$classic_surf_footer_height."px;
z-index:1000;

background-color: ".$bottombgcolor.";
color: ".$bottomfontColor.";
border-width:0px;
";

if ($surfbarprefs['footer_transparent'] == 1) {
	$output .= "filter: alpha(opacity=70);
	opacity:0.7;";
} else {
	$output .= "filter: alpha(opacity=100);
	opacity:1.0;";
}

$output .= "
}
";

$output .= "
</style>

<script language=\"javascript\">

function footer_fade_in() {
";
if ($surfbarprefs['footer_transparent'] == 1) {
	$output .= "document.getElementById('surf_footer').style.filter = 'alpha(opacity=100)';
	document.getElementById('surf_footer').style.opacity = 1.0;";
}
$output .= "	
}

function footer_fade_out() {
";
if ($surfbarprefs['footer_transparent'] == 1) {
	$output .= "document.getElementById('surf_footer').style.filter = 'alpha(opacity=70)';
	document.getElementById('surf_footer').style.opacity = 0.7;";
}
$output .= "
}

</script>
";
$output .= "
<div id=\"surf_footer\" onMouseOver=\"return footer_fade_in();\" onMouseOut=\"return footer_fade_out()\">
	<iframe name=\"footer_frame\" id=\"footer_frame\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" height=\"100%\" width=\"100%\" src=\"surfbarfooter.php\">Error: Frames disabled</iframe>
</div>

<script language=\"javascript\">
// Moves the footer to the default position
";
if ($surfbarprefs['surfbartop'] == 1) {
	// Bottom Left
	$output .= "document.getElementById('surf_footer').style.left = 0 + 'px';
	document.getElementById('surf_footer').style.top = (document.body.clientHeight - ".$classic_surf_footer_height.") + 'px';";
} else {
	// Top Left
	$output .= "document.getElementById('surf_footer').style.left = 0 + 'px';
	document.getElementById('surf_footer').style.top = 0 + 'px';";
}

$output .= "
</script>

";

?>