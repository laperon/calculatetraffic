<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

$_SESSION['switchframe1'] = 0;
$_SESSION['switchframe2'] = 0;
$_SESSION['showingframe'] = 0;

if ($newsite == "framebreakcatch.php") {
	// Show Frame Breaker Catch Page
	$_SESSION['frame1site'] = "framebreakcatch.php";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];

	if ($surfbarprefs['preloadsites'] == 1) {
		$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);	
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 1;
		$_SESSION['showingframe'] = 1;
	} else {
		$_SESSION['frame2site'] = "about:blank";
	}
	
} elseif ($surfbarprefs['preloadsites'] != 1) {
	// Restart Surfing Session
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['frame2site'] = "about:blank";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	
} else {
	// Restart Surfing Session
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	
	$_SESSION['switchframe1'] = 1;
	$_SESSION['switchframe2'] = 1;
	$_SESSION['showingframe'] = 1;
	
}

$surfcode = md5(rand(100,10000));
$_SESSION['surfingkey'] = $surfcode;

$output = "<html>

<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
	top.window.moveTo(0,0);
	top.window.resizeTo(screen.availWidth,screen.availHeight);

	var legitclick=0;
	
	function userClicked() {
	 legitclick=1;
	 return true;
	}
";

if ($framebreakblock == 1) {

	$output .= "

	function frameCatcher() {
	if (legitclick==0) {
	window.top.location.href = 'surfing.php?framecatch=yes';
	return false;
	}
	}

	function addEvent (elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener (evType, fn, useCapture);
		return true;
	} else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	} else {
		elm['on' + evType] = fn;
	}
	}

	addEvent (window, 'unload', frameCatcher, false);
	";

}

$output .= "
var xmlhttp = false;
var newDate = false
var milliCount = 0;

var frame1site = '';
var frame2site = '';

var opensiteurl = '';
	
var switchframe1 = 1;
var switchframe2 = 1;
var showingframe = 1;

";


// Load The JavaScript For Ajax Icon Type

$output .= "

function surfbar_clicksend(xvalue, keydata) {

	if(window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
		if(xmlhttp.overrideMimeType){
			xmlhttp.overrideMimeType('text/xml');
		}
	} else if(window.ActiveXObject) {
		try{
			xmlhttp=new ActiveXObject(\"Msxml2.XMLHTTP\");
		} catch(e) {
			try {
				xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
			} catch(e) {
			}
		}
	}

	if(!xmlhttp) {
		alert('Your browser is blocking Ajax');
		return false;
	}

	xmlhttp.onreadystatechange = function() {
	if (xmlhttp.readyState == 4) {
		var surfdata = xmlhttp.responseText;

		var startresult = surfdata.indexOf('#STARTRESULT#') + 13;
		var endresult = surfdata.indexOf('#ENDRESULT#');
		var bardata = surfdata.substring(startresult, endresult);
		if (bardata == \"Session Expired\") {
			window.location.href=\"login.php?s=noauth\";
		} else {
			if (document.getElementById(\"clickresultdiv\") != null) {
				document.getElementById(\"clickresultdiv\").innerHTML = bardata;
			}
		}
		
		";
		
		// Run Ajax In Mods
		$getmods = mysql_query("Select filename from `".$prefix."barmods_ajaxin` where enabled=1");
		if (mysql_num_rows($getmods) > 0) {
			while ($modlist = mysql_fetch_array($getmods)) {
				$modfilename = trim($modlist['filename']);
				if (file_exists($modfilename)) {
					include($modfilename);
				}
			}
		}
		// End Run Ajax In Mods
		
		$output .= "
		
		var starticons = surfdata.indexOf('#STARTICONS#') + 12;
		var endicons = surfdata.indexOf('#ENDICONS#');
		var icondata = surfdata.substring(starticons, endicons);
		document.getElementById(\"myform\").innerHTML = icondata + '<div class=\"view_surf_information\"></div>';

		var startbanner = surfdata.indexOf('#STARTBANNER#') + 13;
		var endbanner = surfdata.indexOf('#ENDBANNER#');
		var bannerdata = surfdata.substring(startbanner, endbanner);
		if (document.getElementById(\"bannerdiv\") != null) {
			document.getElementById(\"bannerdiv\").innerHTML = bannerdata;
		}
		
		var starttext = surfdata.indexOf('#STARTTEXT#') + 11;
		var endtext = surfdata.indexOf('#ENDTEXT#');
		var textdata = surfdata.substring(starttext, endtext);
		if (document.getElementById(\"textdiv\") != null) {
			document.getElementById(\"textdiv\").innerHTML = textdata;
		}
		
		
		var startframe1 = surfdata.indexOf('#FRAME1#') + 8;
		var endframe1 = surfdata.indexOf('#ENDFRAME1#');
		frame1site = surfdata.substring(startframe1, endframe1);
		
		var startframe2 = surfdata.indexOf('#FRAME2#') + 8;
		var endframe2 = surfdata.indexOf('#ENDFRAME2#');
		frame2site = surfdata.substring(startframe2, endframe2);
		
		var startswitchframe1 = surfdata.indexOf('#SWITCHFRAME1#') + 14;
		var endswitchframe1 = surfdata.indexOf('#ENDSWITCHFRAME1#');
		switchframe1 = surfdata.substring(startswitchframe1, endswitchframe1);
		
		var startswitchframe2 = surfdata.indexOf('#SWITCHFRAME2#') + 14;
		var endswitchframe2 = surfdata.indexOf('#ENDSWITCHFRAME2#');
		switchframe2 = surfdata.substring(startswitchframe2, endswitchframe2);	
		
		var startshowingframe = surfdata.indexOf('#SHOWINGFRAME#') + 14;
		var endshowingframe = surfdata.indexOf('#ENDSHOWINGFRAME#');
		showingframe = surfdata.substring(startshowingframe, endshowingframe);
		
		var startsiteurl = surfdata.indexOf('#SITEURL#') + 9;
		var endsiteurl = surfdata.indexOf('#ENDSITEURL#');
		opensiteurl = surfdata.substring(startsiteurl, endsiteurl);
		if (document.getElementById(\"surfbarmenu\") != null) {
			document.getElementById(\"surfbarmenu\").innerHTML = '<a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"members.php\" onclick=\"userClicked();\">Home</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" href=\"' + opensiteurl + '\">Open Site</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" href=\"reportsite.php\">Report Site</a> | <a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"surfbar_settings.php\" onclick=\"userClicked();\">Surf Settings</a>';
		}
		
		";
		
		// Update HTML Modules
		$getmodules = mysql_query("SELECT id FROM `".$prefix."barmods_topbar` WHERE state='1' AND filename='None' ORDER BY rank ASC");
		while ($modlist = mysql_fetch_array($getmodules)) {
			if ($modlist['id'] >= 10) {
				$searchlength = 12;
			} else {
				$searchlength = 11;
			}
			$output .= "
			var startmod".$modlist['id']."text = surfdata.indexOf('#STARTMOD".$modlist['id']."#') + ".$searchlength.";
			var endmod".$modlist['id']."text = surfdata.indexOf('#ENDMOD".$modlist['id']."#');
			var mod".$modlist['id']."data = surfdata.substring(startmod".$modlist['id']."text, endmod".$modlist['id']."text);
			document.getElementById(\"modbox".$modlist['id']."\").innerHTML = mod".$modlist['id']."data;
			";
		}
		// End Update HTML Modules
		
		// Update HTML Extensions
		$getmodules = mysql_query("SELECT id FROM `".$prefix."barmods_extensions` WHERE state='1' AND filename='None' ORDER BY rank ASC");
		while ($modlist = mysql_fetch_array($getmodules)) {
			if ($modlist['id'] >= 10) {
				$searchlength = 12;
			} else {
				$searchlength = 11;
			}
			$output .= "
			var startext".$modlist['id']."text = surfdata.indexOf('#STARTEXT".$modlist['id']."#') + ".$searchlength.";
			var endext".$modlist['id']."text = surfdata.indexOf('#ENDEXT".$modlist['id']."#');
			var ext".$modlist['id']."data = surfdata.substring(startext".$modlist['id']."text, endext".$modlist['id']."text);
			document.getElementById(\"extbox".$modlist['id']."\").innerHTML = ext".$modlist['id']."data;
			";
		}
		// End Update HTML Extensions
		
		$output .= "
		
		if (document.getElementById(\"footer_frame\") != null) {
			document.getElementById('footer_frame').src = 'surfbarfooter.php?rand=' + Math.floor(Math.random()*900000);
		}
		
		frame_site();

		";
		
		if ($surfbarprefs['preloadsites'] == 1) {
		
		$output .= "
		if (switchframe1 == 1) {
			document.getElementById('frame1').src = frame1site;
		}

		if (switchframe2 == 1) {
			document.getElementById('frame2').src = frame2site;
		}
		";
		
		} else {
			$output .= "
			document.getElementById('frame1').src = frame1site;
			";
		}
		
	$output .= "
	}
	}

	newDate = new Date();
	milliCount = newDate.getTime();
	xmlhttp.open('GET','surfbar_click.php?m=' + milliCount + '&xval=' + xvalue + '&toclickkey=' + keydata,true);
	xmlhttp.send(null);
}

function surfbar_click(numval, keyval) {

	";
	
	if ($surfbarprefs['preloadsites'] == 1) {
		$output .= "
		if (showingframe == 1) {
			// Display frame 2 and hide frame 1
			document.getElementById('frame2').style.display = 'block';
			document.getElementById('frame1').style.display = 'none';
		} else {
			// Display frame 1 and hide frame 2
			document.getElementById('frame1').style.display = 'block';
			document.getElementById('frame2').style.display = 'none';
		}
		";
	}

	$output .= "
	surfbar_clicksend(numval, keyval);
	reset();	
}

";

$output .= "
function frame_site() {

	document.getElementById('site_frames').style.height = (document.body.clientHeight - document.getElementById('surfbar').offsetHeight) + 'px';
	var site_frames_height = document.getElementById('site_frames').style.height.replace('px', '');

	document.getElementById('surf_topbar').style.height = document.getElementById('surfbar').offsetHeight + 'px';

	site_frames_height = site_frames_height - ".$classic_surf_footer_height.";
	document.getElementById('site_frames').style.height = site_frames_height + 'px';


	document.getElementById('site_frames').style.top = (document.body.clientHeight - site_frames_height - ".$classic_surf_footer_height.") + 'px';
	document.getElementById('site_frames').style.width = (document.body.clientWidth) + 'px';

	";
	
	// Run Frame Site Mods
	$getmods = mysql_query("Select filename from `".$prefix."barmods_framesite` where enabled=1");
	if (mysql_num_rows($getmods) > 0) {
		while ($modlist = mysql_fetch_array($getmods)) {
			$modfilename = trim($modlist['filename']);
			if (file_exists($modfilename)) {
				include($modfilename);
			}
		}
	}
	// End Run Frame Site Mods
	
	$output .= "
}

</script>";


$output .= "

    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'>


<style type=\"text/css\">
html, body, div, iframe { margin:0; padding:0; height:100%; }
iframe { display:block; width:100%; border:none; }
#site_frames { position:absolute; z-index:1; }

#surf_topbar {
display:block;
text-align:center;
vertical-align: middle;

margin:0;
padding:0;
position:absolute;
bottom:0;
right:0;
width:100%;
height:".$classic_surfbar_height."px;
z-index:1000;

background-color: ".$bgcolor.";
color: ".$fontColor.";
border-width:0px;
filter: alpha(opacity=100);
opacity:1.0;
}
.view_surf_information {
	display:none;
}
@media only screen and (min-width: 320px) and (max-width: 768px){
    table.surf_icon {
        position: fixed !important;
        left: 0;
        width: 75px;
        top: 80px;
        background: none;
        z-index:100;
    }
    table.surf_icon img {
        display: block;
    }
   #surf_footer {
        display: none !important;
        height:0 !important;
    }
   .view_surf_information {
		display:block !important;
	}
	.dialog_surf_information {
		display: block !important;
	}
	.ui-dialog .ui-dialog-buttonpane {
		padding: 0!important;
		text-align: center !important;
	}
	.ui-dialog .ui-dialog-titlebar {
		padding: 0 !important;
	}
	.ui-dialog {
		z-index: 9999 !important;
	}
	#site_frames {
		height: 100% !important;
		overflow: auto;
	}
	.dialog_surf_information p {
		border-bottom: 1px solid black;
		margin: 0;
		padding: 10px;
	}
	.dialog_surf_information span {
		padding-top: 5px;
		font-size: 12px;
		font-weight: 500;
		display: block;
	}

	.ui-dialog-titlebar {
		background: #4BAB29 !important;
	}
	.ui-dialog .ui-dialog-content {
		padding: 0 !important;
	}
	span.ui-button-text {
		background: #4BAB29;
	}
	.view_surf_information {
		background: url('../images/Information-icon.png') no-repeat;
		height: 75px;
		width: 75px;
		position: relative;
		z-index: 50;
	}
}


</style>

<div id=\"surf_topbar\">
	";
	include("surf_topbar.php");
	$output .= "
</div>

<script language=\"javascript\">
// Moves the surfbar to the default position
document.getElementById('surf_topbar').style.left = 0 + 'px';
document.getElementById('surf_topbar').style.top = 0 + 'px';
</script>
";


// Run Header Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_header` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Header Mods

include("surf_footer_classic.php");

$output .= "

<body bgcolor=\"white\" leftmargin=\"0px\" topmargin=\"0px\" marginwidth=\"0px\" marginheight=\"0px\">
<div id=\"site_frames\">
<iframe name=\"frame1\" id=\"frame1\" style=\"display: block;\" height=\"100%\" width=\"100%\" src=\"".$_SESSION['frame1site']."\">Error: Frames disabled</iframe>
<iframe name=\"frame2\" id=\"frame2\" style=\"display: none;\" height=\"100%\" width=\"100%\" src=\"".$_SESSION['frame2site']."\">Error: Frames disabled</iframe>

</div>";

// Run Body Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_body` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Body Mods
$output .= "
<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css\">
<script src=\"//code.jquery.com/ui/1.11.4/jquery-ui.js\"></script>
<div class='dialog_surf_information' title='Surf and Word Game Info'></div>
</body>

<script language=\"javascript\">
frame_site();

$(document).ready(function(){
		setTimeout(function() {
 			$('body').on( 'touchstart' , '.view_surf_information', function(){
			var iframe = document.getElementById('footer_frame').contentWindow.surf_information;

 			$('.dialog_surf_information').html(iframe);

				$( '.dialog_surf_information' ).dialog({
					modal: true,
      				buttons: {
        			Ok: function() {
          			$( this ).dialog( \"close\" );
				}
			  }
			});
		} );
		}, 10000)
	});
</script>

</html>";

echo($output);

?>