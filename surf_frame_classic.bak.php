<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.16
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

$output .= "

<style type=\"text/css\">

html, body, div, iframe { margin:0; padding:0; height:100%; }
iframe { display:block; width:100%; border:none; }

#classic_bar {
display:block;
text-align:center;
vertical-align: middle;

margin:0;
padding:0;
position:absolute;
bottom:0;
right:0;
width:100%;
height:".$classic_surfbar_height."px;
z-index:1000;

background-color: ".$bgcolor.";
color: ".$fontColor.";
border-width:0px;
filter: alpha(opacity=100);
opacity:1.0;

}

</style>

<div id=\"classic_bar\">
	<iframe name=\"classicbar_frame\" id=\"classicbar_frame\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" height=\"100%\" width=\"100%\" src=\"surfbar.php?surfcode=".$_SESSION['surfingkey']."\">Error: Frames disabled</iframe>
</div>

<script language=\"javascript\">
// Moves the surfbar to the default position
";

if ($surfbarprefs['surfbartop'] == 1) {
	// Top
	$output .= "document.getElementById('classic_bar').style.left = 0 + 'px';
	document.getElementById('classic_bar').style.top = 0 + 'px';";
} else {
	// Bottom
	$output .= "document.getElementById('classic_bar').style.left = 0 + 'px';
	document.getElementById('classic_bar').style.top = (document.body.clientHeight - ".$classic_surfbar_height.") + 'px';";
}

$output .= "
</script>

";

?>