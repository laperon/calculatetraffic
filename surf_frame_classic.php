<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.19
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}


$_SESSION['switchframe1'] = 0;
$_SESSION['switchframe2'] = 0;
$_SESSION['showingframe'] = 0;

if ($newsite == "framebreakcatch.php") {
	// Show Frame Breaker Catch Page
	$_SESSION['frame1site'] = "framebreakcatch.php";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	$_SESSION['frame2site'] = "about:blank";
	
} else {
	// Restart Surfing Session
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['frame2site'] = "about:blank";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	
	if (is_numeric($_SESSION["newsiteid"])) {
		@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=".$_SESSION["newsiteid"]." where Id=$userid limit 1");
	} else {
		@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=0 where Id=$userid limit 1");
	}
	
}

$surfcode = md5(rand(100,10000));
$_SESSION['surfingkey'] = $surfcode;

$output = "<html>

<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
	top.window.moveTo(0,0);
	top.window.resizeTo(screen.availWidth,screen.availHeight);
	
	var legitclick=0;
	
	function userClicked() {
	 legitclick=1;
	 return true;
	}
";

if ($framebreakblock == 1) {

	$output .= "

	function frameCatcher() {
	if (legitclick==0) {
	window.top.location.href = 'surfing.php?framecatch=yes';
	return false;
	}
	}

	function addEvent (elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener (evType, fn, useCapture);
		return true;
	} else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	} else {
		elm['on' + evType] = fn;
	}
	}

	addEvent (window, 'unload', frameCatcher, false);
	";

}

$output .= "
var xmlhttp = false;
var newDate = false
var milliCount = 0;

var frame1site = '';
var frame2site = '';

var opensiteurl = '';
	
var switchframe1 = 1;
var switchframe2 = 1;
var showingframe = 1;

var surfbardrop = ".$surfbarprefs['surfbardrop'].";
var surfbartop = ".$surfbarprefs['surfbartop'].";

var footertype = ".$surfbarprefs['footertype'].";

";

$modern_surfbarheight = $classic_surfbar_height;

$output .= "
function frame_site() {

	document.getElementById('site_frames').style.height = (document.body.clientHeight - ".$classic_surfbar_height.") + 'px';
	var site_frames_height = document.getElementById('site_frames').style.height.replace('px', '');
	
	if (footertype == 1) {
		site_frames_height = site_frames_height - ".$classic_surf_footer_height.";
		document.getElementById('site_frames').style.height = site_frames_height + 'px';
	}
	
	
	if (surfbartop == 1) {
		if (footertype == 1) {
			document.getElementById('site_frames').style.top = (document.body.clientHeight - site_frames_height - ".$classic_surf_footer_height.") + 'px';
		} else {
			document.getElementById('site_frames').style.top = (document.body.clientHeight - site_frames_height) + 'px';
		}
	} else {
		if (footertype == 1) {
			document.getElementById('site_frames').style.top = ".$classic_surf_footer_height." + 'px';
		} else {
			document.getElementById('site_frames').style.top = 0 + 'px';
		}
	}

	document.getElementById('site_frames').style.width = (document.body.clientWidth) + 'px';
	
	";
	
	// Run Frame Site Mods
	$getmods = mysql_query("Select filename from `".$prefix."barmods_framesite` where enabled=1");
	if (mysql_num_rows($getmods) > 0) {
		while ($modlist = mysql_fetch_array($getmods)) {
			$modfilename = trim($modlist['filename']);
			if (file_exists($modfilename)) {
				include($modfilename);
			}
		}
	}
	// End Run Frame Site Mods
		
	$output .= "
}

function unframe_site() {
	document.getElementById('site_frames').style.height = (document.body.clientHeight) + 'px';
	document.getElementById('site_frames').style.top = 0 + 'px';
	document.getElementById('site_frames').style.width = (document.body.clientWidth) + 'px';
}

</script>";


$output .= "
<style type=\"text/css\">
html, body, div, iframe { margin:0; padding:0; height:100%; }
iframe { display:block; width:100%; border:none; }
#site_frames { position:absolute; z-index:1; }
</style>
";


$output .= "

<style type=\"text/css\">

html, body, div, iframe { margin:0; padding:0; height:100%; }
iframe { display:block; width:100%; border:none; }

#classic_bar {
display:block;
text-align:center;
vertical-align: middle;

margin:0;
padding:0;
position:absolute;
bottom:0;
right:0;
width:100%;
height:".$classic_surfbar_height."px;
z-index:1000;

background-color: ".$bgcolor.";
color: ".$fontColor.";
border-width:0px;
filter: alpha(opacity=100);
opacity:1.0;

}

</style>

<div id=\"classic_bar\">
	<iframe name=\"classicbar_frame\" id=\"classicbar_frame\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\" height=\"100%\" width=\"100%\" src=\"surfbar.php?surfcode=".$_SESSION['surfingkey']."\">Error: Frames disabled</iframe>
</div>

<script language=\"javascript\">
// Moves the surfbar to the default position
";

if ($surfbarprefs['surfbartop'] == 1) {
	// Top
	$output .= "document.getElementById('classic_bar').style.left = 0 + 'px';
	document.getElementById('classic_bar').style.top = 0 + 'px';";
} else {
	// Bottom
	$output .= "document.getElementById('classic_bar').style.left = 0 + 'px';
	document.getElementById('classic_bar').style.top = (document.body.clientHeight - ".$classic_surfbar_height.") + 'px';";
}

$output .= "
</script>

";


// Run Header Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_header` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Header Mods

if ($surfbarprefs['footertype'] == 2) {
	include("surf_footer_modern.php");
} else {
	include("surf_footer_classic.php");
}

$output .= "

<body bgcolor=\"white\" leftmargin=\"0px\" topmargin=\"0px\" marginwidth=\"0px\" marginheight=\"0px\">
<div id=\"site_frames\">
<iframe name=\"frame1\" id=\"frame1\" style=\"display: block;\" height=\"100%\" width=\"100%\" src=\"".$_SESSION['frame1site']."\">Error: Frames disabled</iframe>
<iframe name=\"frame2\" id=\"frame2\" style=\"display: none;\" height=\"100%\" width=\"100%\" src=\"".$_SESSION['frame2site']."\">Error: Frames disabled</iframe>
</div>";

// Run Body Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_body` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Body Mods

$output .= "

</body>

<script language=\"javascript\">";

if ($_SESSION['framesites'] == 0) {
	$framesp = @mysql_result(@mysql_query("Select alwaysframe from ".$prefix."startpage_settings"), 0);
	if ($framesp == 1) {
		$output .= "frame_site();";
	} else {
		$output .= "unframe_site();";
	}
} else {
	$output .= "frame_site();";
}

$output .= "</script>
";

$output .= "
</html>";

echo($output);

?>