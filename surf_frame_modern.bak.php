<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.16
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

$_SESSION['switchframe1'] = 0;
$_SESSION['switchframe2'] = 0;
$_SESSION['showingframe'] = 0;

if ($newsite == "framebreakcatch.php") {
	// Show Frame Breaker Catch Page
	$_SESSION['frame1site'] = "framebreakcatch.php";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];

	if ($surfbarprefs['preloadsites'] == 1) {
		$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);	
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 1;
		$_SESSION['showingframe'] = 1;
	} else {
		$_SESSION['frame2site'] = "about:blank";
	}
	
} elseif ($surfbarprefs['surfbarstyle'] == 1 || $surfbarprefs['preloadsites'] != 1) {
	// Restart Surfing Session
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['frame2site'] = "about:blank";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	
} else {
	// Restart Surfing Session
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	
	$_SESSION['switchframe1'] = 1;
	$_SESSION['switchframe2'] = 1;
	$_SESSION['showingframe'] = 1;
	
}

$surfcode = md5(rand(100,10000));
$_SESSION['surfingkey'] = $surfcode;

if ($surfbarprefs['surfbarstyle'] == 2) {
	include("surfbar_modern.php");
}

$output = "<html>

<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
	top.window.moveTo(0,0);
	top.window.resizeTo(screen.availWidth,screen.availHeight);
	
	var legitclick=0;
	
	function userClicked() {
	 legitclick=1;
	 return true;
	}
";

if ($framebreakblock == 1) {

	$output .= "

	function frameCatcher() {
	if (legitclick==0) {
	window.top.location.href = 'surfing.php?framecatch=yes';
	return false;
	}
	}

	function addEvent (elm, evType, fn, useCapture) {
	if (elm.addEventListener) {
		elm.addEventListener (evType, fn, useCapture);
		return true;
	} else if (elm.attachEvent) {
		var r = elm.attachEvent('on' + evType, fn);
		return r;
	} else {
		elm['on' + evType] = fn;
	}
	}

	addEvent (window, 'unload', frameCatcher, false);
	";

}

$output .= "
var xmlhttp = false;
var newDate = false
var milliCount = 0;

var frame1site = '';
var frame2site = '';

var opensiteurl = '';
	
var switchframe1 = 1;
var switchframe2 = 1;
var showingframe = 1;

var surfbardrop = ".$surfbarprefs['surfbardrop'].";
var surfbartop = ".$surfbarprefs['surfbartop'].";

var footertype = ".$surfbarprefs['footertype'].";

";

if ($surfbarprefs['surfbarstyle'] == 2) {
	
	if ($_SESSION['surficontype'] == 2) {
		// Load The JavaScript For Ajax Icon Type
		
		$output .= "
		
		function surfbar_clicksend(xvalue) {
		
			if(window.XMLHttpRequest){
				xmlhttp=new XMLHttpRequest();
				if(xmlhttp.overrideMimeType){
					xmlhttp.overrideMimeType('text/xml');
				}
			} else if(window.ActiveXObject) {
				try{
					xmlhttp=new ActiveXObject(\"Msxml2.XMLHTTP\");
				} catch(e) {
					try {
						xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
					} catch(e) {
					}
				}
			}
		
			if(!xmlhttp) {
				alert('Your browser is blocking Ajax');
				return false;
			}
		
			xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				var surfdata = xmlhttp.responseText;
				
				";
				
				// Run Ajax In Mods
				$getmods = mysql_query("Select filename from `".$prefix."barmods_ajaxin` where enabled=1");
				if (mysql_num_rows($getmods) > 0) {
					while ($modlist = mysql_fetch_array($getmods)) {
						$modfilename = trim($modlist['filename']);
						if (file_exists($modfilename)) {
							include($modfilename);
						}
					}
				}
				// End Run Ajax In Mods
				
				$output .= "
				var startresult = surfdata.indexOf('#STARTRESULT#') + 13;
				var endresult = surfdata.indexOf('#ENDRESULT#');
				var bardata = surfdata.substring(startresult, endresult);
				document.getElementById(\"clickresultdiv\").innerHTML = bardata;
				
				var starticons = surfdata.indexOf('#STARTICONS#') + 12;
				var endicons = surfdata.indexOf('#ENDICONS#');
				var icondata = surfdata.substring(starticons, endicons);
				document.getElementById(\"myform\").innerHTML = icondata;
				
				var startbanner = surfdata.indexOf('#STARTBANNER#') + 13;
				var endbanner = surfdata.indexOf('#ENDBANNER#');
				var bannerdata = surfdata.substring(startbanner, endbanner);
				document.getElementById(\"bannerdiv\").innerHTML = bannerdata;
				
				var starttext = surfdata.indexOf('#STARTTEXT#') + 11;
				var endtext = surfdata.indexOf('#ENDTEXT#');
				var textdata = surfdata.substring(starttext, endtext);
				document.getElementById(\"textdiv\").innerHTML = textdata;
				
				
				
				var startframe1 = surfdata.indexOf('#FRAME1#') + 8;
				var endframe1 = surfdata.indexOf('#ENDFRAME1#');
				frame1site = surfdata.substring(startframe1, endframe1);
				
				var startframe2 = surfdata.indexOf('#FRAME2#') + 8;
				var endframe2 = surfdata.indexOf('#ENDFRAME2#');
				frame2site = surfdata.substring(startframe2, endframe2);
				
				var startswitchframe1 = surfdata.indexOf('#SWITCHFRAME1#') + 14;
				var endswitchframe1 = surfdata.indexOf('#ENDSWITCHFRAME1#');
				switchframe1 = surfdata.substring(startswitchframe1, endswitchframe1);
				
				var startswitchframe2 = surfdata.indexOf('#SWITCHFRAME2#') + 14;
				var endswitchframe2 = surfdata.indexOf('#ENDSWITCHFRAME2#');
				switchframe2 = surfdata.substring(startswitchframe2, endswitchframe2);	
				
				var startshowingframe = surfdata.indexOf('#SHOWINGFRAME#') + 14;
				var endshowingframe = surfdata.indexOf('#ENDSHOWINGFRAME#');
				showingframe = surfdata.substring(startshowingframe, endshowingframe);
				
				var startframesite = surfdata.indexOf('#FRAMESITE#') + 11;
				var endframesite = surfdata.indexOf('#ENDFRAMESITE#');
				framethissite = surfdata.substring(startframesite, endframesite);
				
				if (framethissite == 1) {
					frame_site();
				} else {
					unframe_site();
				}
				
				var startsiteurl = surfdata.indexOf('#SITEURL#') + 9;
				var endsiteurl = surfdata.indexOf('#ENDSITEURL#');
				opensiteurl = surfdata.substring(startsiteurl, endsiteurl);
				document.getElementById(\"surfbarmenu\").innerHTML = '<a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"members.php\" onclick=\"userClicked();\">Home</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" href=\"' + opensiteurl + '\">Open Site</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" href=\"reportsite.php\">Report Site</a> | <a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"surfbar_settings.php\" onclick=\"userClicked();\">Surf Settings</a>';
		
				if (footertype == 2) {
					var startsurfed = surfdata.indexOf('#STARTSURFED#') + 13;
					var endsurfed = surfdata.indexOf('#ENDSURFED#');
					document.getElementById(\"surfedtoday\").innerHTML = surfdata.substring(startsurfed, endsurfed);
				
					var startlhunt = surfdata.indexOf('#STARTLHUNT#') + 12;
					var endlhunt = surfdata.indexOf('#ENDLHUNT#');
					document.getElementById(\"letterhunt\").innerHTML = surfdata.substring(startlhunt, endlhunt);
				} else {
					document.getElementById('footer_frame').src = 'surfbarfooter.php?rand=' + Math.floor(Math.random()*900000);
				}
		
				";
				
				if ($surfbarprefs['preloadsites'] == 1) {
				
				$output .= "
				if (switchframe1 == 1) {
					document.getElementById('frame1').src = frame1site;
				}
		
				if (switchframe2 == 1) {
					document.getElementById('frame2').src = frame2site;
				}
				";
				
				} else {
					$output .= "
					document.getElementById('frame1').src = frame1site;
					";
				}
				
			$output .= "
			}
			}
		
			newDate = new Date();
			milliCount = newDate.getTime();
			xmlhttp.open('GET','surfbar_click.php?m=' + milliCount + '&xval=' + xvalue,true);
			xmlhttp.send(null);
		}
		
		function surfbar_click(obj, evt) {
		
			";
			
			if ($surfbarprefs['preloadsites'] == 1) {
				$output .= "
				if (showingframe == 1) {
					// Display frame 2 and hide frame 1
					document.getElementById('frame2').style.display = 'block';
					document.getElementById('frame1').style.display = 'none';
				} else {
					// Display frame 1 and hide frame 2
					document.getElementById('frame1').style.display = 'block';
					document.getElementById('frame2').style.display = 'none';
				}
				";
			}
		
			$output .= "
			var x = (window.Event) ? evt.layerX : evt.x;
			x -= obj.offsetLeft;
			var ele = obj.offsetParent;
			while (ele) {
				x -= ele.offsetLeft;
				ele = ele.offsetParent;
			}
			if (navigator.userAgent.indexOf('MSIE')==-1) { 
				x += document.getElementById('surfbarfloater').offsetLeft;
			}
			surfbar_clicksend(x);
			reset();
			
		}
		
		";
		
	}

} else {
	$modern_surfbarheight = $classic_surfbar_height;
}

$output .= "
function frame_site() {

	document.getElementById('site_frames').style.height = (document.body.clientHeight - ".$modern_surfbarheight.") + 'px';
	var site_frames_height = document.getElementById('site_frames').style.height.replace('px', '');
	
	if (footertype == 1) {
		site_frames_height = site_frames_height - ".$classic_surf_footer_height.";
		document.getElementById('site_frames').style.height = site_frames_height + 'px';
	}
	
	
	if (surfbartop == 1) {
		if (footertype == 1) {
			document.getElementById('site_frames').style.top = (document.body.clientHeight - site_frames_height - ".$classic_surf_footer_height.") + 'px';
		} else {
			document.getElementById('site_frames').style.top = (document.body.clientHeight - site_frames_height) + 'px';
		}
	} else {
		if (footertype == 1) {
			document.getElementById('site_frames').style.top = ".$classic_surf_footer_height." + 'px';
		} else {
			document.getElementById('site_frames').style.top = 0 + 'px';
		}
	}

	document.getElementById('site_frames').style.width = (document.body.clientWidth) + 'px';
	
	";
	
	// Run Frame Site Mods
	$getmods = mysql_query("Select filename from `".$prefix."barmods_framesite` where enabled=1");
	if (mysql_num_rows($getmods) > 0) {
		while ($modlist = mysql_fetch_array($getmods)) {
			$modfilename = trim($modlist['filename']);
			if (file_exists($modfilename)) {
				include($modfilename);
			}
		}
	}
	// End Run Frame Site Mods
		
	$output .= "
}

function unframe_site() {
	document.getElementById('site_frames').style.height = (document.body.clientHeight) + 'px';
	document.getElementById('site_frames').style.top = 0 + 'px';
	document.getElementById('site_frames').style.width = (document.body.clientWidth) + 'px';
}

</script>";


$output .= "
<style type=\"text/css\">
html, body, div, iframe { margin:0; padding:0; height:100%; }
iframe { display:block; width:100%; border:none; }
#site_frames { position:absolute; z-index:1; }
</style>
";

if ($surfbarprefs['surfbarstyle'] == 1) {
	include("surf_frame_classic.php");
} else {

	include("surfbar_float.php");
	
	$output .= "
	<script language=\"javascript\">
	  var timer=".$timer.";
	  function run () {
		  if (timer <= 0) {
			  document.getElementById(\"myform\").style.visibility=\"visible\";
			  document.getElementById(\"timer\").innerHTML=\"GO!\";
			  maxSurfbar()
	
		  } else {
			  document.getElementById(\"timer\").innerHTML=timer;
			  timer--;
			  setTimeout(run, 1000);
		  }
	  }
	  
	  function reset () {
		  timer = ".$timer.";
		  document.getElementById(\"myform\").style.visibility=\"hidden\";
		  minSurfbar()
		  run();
	  }
	  
	  document.onLoad=run();
	
	  function setform (\$thecode) {
		  document.getElementById(\"mysubcode\").value=\$thecode;
	  }
	</script>
	";
}

// Run Header Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_header` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Header Mods

if ($surfbarprefs['footertype'] == 2) {
	include("surf_footer_modern.php");
} else {
	include("surf_footer_classic.php");
}

$output .= "

<body bgcolor=\"white\" leftmargin=\"0px\" topmargin=\"0px\" marginwidth=\"0px\" marginheight=\"0px\">
<div id=\"site_frames\">
<iframe name=\"frame1\" id=\"frame1\" style=\"display: block;\" height=\"100%\" width=\"100%\" src=\"".$_SESSION['frame1site']."\">Error: Frames disabled</iframe>
<iframe name=\"frame2\" id=\"frame2\" style=\"display: none;\" height=\"100%\" width=\"100%\" src=\"".$_SESSION['frame2site']."\">Error: Frames disabled</iframe>
</div>";

// Run Body Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_body` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Body Mods

$output .= "
</body>

<script language=\"javascript\">";

if ($_SESSION['framesites'] == 0) {
	$framesp = @mysql_result(@mysql_query("Select alwaysframe from ".$prefix."startpage_settings"), 0);
	if ($framesp == 1) {
		$output .= "frame_site();";
	} else {
		$output .= "unframe_site();";
	}
} else {
	$output .= "frame_site();";
}

$output .= "</script>
";

if ($surfbarprefs['surfbarstyle'] == 2) {
	$output .= "<script language=\"javascript\">
	  //Instantly Moves Surfbar Into Position
	  xMoveTo('surfbarfloater', (xClientWidth()/2)-400, maxTo);
	</script>
	";
}

$output .= "
</html>";

echo($output);

?>