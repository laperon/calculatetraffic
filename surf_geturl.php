<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

function getnewurl($acctype=0, $accstatus="", $lastclicktime=0, $clickstoday=0, $timer=0) {

// Prevent Invalid Access
if (!isset($_SESSION["userid"]) || !is_numeric($_SESSION["userid"])) {
	header("Location: members.php?mf=lo");
	exit;
}

$prefix = $_SESSION["prefix"];
$userid = $_SESSION["userid"];

$getdefaults = mysql_query("Select defurl from `".$prefix."settings` limit 1");
$defaultsite = mysql_result($getdefaults, 0, "defurl");

$newsite = "";

//Startpage

$url = '';
$startpageshow = "no";

if ($_SESSION['startpage']==0) {

  $m = mysql_query("SELECT * FROM ".$prefix."startpage_settings;");
  extract(mysql_fetch_array($m));
  if($enable) {
	 $startpageshow = "yes";
	 $t = time();
	 $m = mysql_query("SELECT id,url,lastview FROM ".$prefix."startpage WHERE `from`<$t AND `to`>$t;");
	 $rows = mysql_num_rows($m);
	 if(mysql_num_rows($m)) {
		// startpage found - get url and update stats
		extract(mysql_fetch_array($m));
		if($url>'') {
			$_SESSION['startpage'] = 2;
			if(date("j",$lastview)<>date("j",$t)) {
			   @mysql_query("Delete from ".$prefix."startpage_stats WHERE sid != $id");
			   @mysql_query("OPTIMIZE TABLE ".$prefix."startpage_stats");
			   mysql_query("UPDATE ".$prefix."startpage SET viewstoday=0, uniquetoday=0 WHERE id=$id;");
			   }
			mysql_query("UPDATE ".$prefix."startpage SET viewstoday=viewstoday+1,views=views+1,lastview=$t,lastusrid=$userid WHERE id=$id;");
			$m = mysql_query("SELECT id FROM ".$prefix."startpage_stats WHERE sid=$id AND usrid=$userid");
			if(mysql_num_rows($m)) {
			   $sid = mysql_result($m,0);
			   mysql_query("UPDATE ".$prefix."startpage_stats SET views=views+1 WHERE id=$sid");
			   } else {
			   mysql_query("INSERT INTO ".$prefix."startpage_stats (sid,usrid,views,lastview) VALUES ($id,$userid,1,$t)");
			   mysql_query("UPDATE ".$prefix."startpage SET totalunique=totalunique+1, uniquetoday=uniquetoday+1 WHERE id=$id");
			   }
            return $url;
			} else {
//Get Default
$getdefault = mysql_query("Select * from ".$prefix."startpage_settings");
$url = mysql_result($getdefault, 0, "default");
$_SESSION['startpage'] = 2;
//End Get Default
}
		} else {
//Get Default
$getdefault = mysql_query("Select * from ".$prefix."startpage_settings");
$url = mysql_result($getdefault, 0, "default");
$_SESSION['startpage'] = 2;

//End Get Default
}
	 }
  }

//End startpage

if ($url != "") {
	//Show startpage
	$newsite = "view_startpage.php";
	$startpageshow = "yes";
} 

if ($newsite == "") {
	// Run Surf Mods
	$getmods = mysql_query("Select filename from `".$prefix."surfmods` where enabled=1 order by rank asc");
	if (mysql_num_rows($getmods) > 0) {
	while (($modlist = mysql_fetch_array($getmods)) && ($newsite == "")) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
	}
}

if ($newsite == "") {
	// Get a new site
	$newsite = getsite($userid, $defaultsite);
	
	$getnewsiteid = mysql_query("Select id from `".$prefix."msites` where url='".$newsite."' limit 1");
	if (mysql_num_rows($getnewsiteid) > 0) {
		$newsiteid = mysql_result($getnewsiteid, 0, "id");
	} else {
		$newsiteid = 0;
	}
}

if (!isset($newsiteid)) {
	$_SESSION["newsiteid"] = 0;
} else {
	$_SESSION["newsiteid"] = $newsiteid;
}

// Surf Logs
$ts = time();
mysql_query("Insert into ".$prefix."sstats (userid,url,websiteid,adate) values ('$userid','$newsite','$newsiteid','$ts')");

return $newsite;

}

?>