<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

//Get Default URLs
$getdefaults = mysql_query("Select defbanimg, defbantar, deftextad, deftexttar from `".$prefix."settings` limit 1");
$defbanimg = mysql_result($getdefaults, 0, "defbanimg");
$defbantar = mysql_result($getdefaults, 0, "defbantar");
$deftextad = mysql_result($getdefaults, 0, "deftextad");
$deftexttar = mysql_result($getdefaults, 0, "deftexttar");

$topbaroutput .= "

<table bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0>
<tr><td align=center>

<div id=\"bannerdiv\">".showbanner($userid, $defbanimg, $defbantar)."</div>
<div id=\"textdiv\" style=\"margin: 0px;\">".showtext($userid, 50, $deftextad, $deftexttar, $textImpColor)."</div>

</td></tr>
</table>";

?>