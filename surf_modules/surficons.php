<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Get The Images
require_once "surfimages.php";

// Create The Buttons

$theimagekey = array_rand($images);
$_SESSION['toclick'] = $toclick = rand(1,4);

$_SESSION['toclickkey'] = md5(rand(1, 999999));

$toclickkeys[1] = md5(rand(1, 999999));
$toclickkeys[2] = md5(rand(1, 999999));
$toclickkeys[3] = md5(rand(1, 999999));
$toclickkeys[4] = md5(rand(1, 999999));

$toclickkeys[$toclick] = $_SESSION['toclickkey'];

$theimage = $images[$theimagekey];
$imagedata = explode("|", $theimage);

$mainimage = $imagedata[0];
$showimage = $imagedata[$toclick];

$imagesize = @getimagesize($image_dir."/".$showimage);
$imagewidth = $imagesize[0];

$_SESSION['surfimage'] = $image_dir."/".$showimage;

$_SESSION['surfclickimage1'] = $image_dir."/cache/".$mainimage."_1";
$_SESSION['surfclickimage2'] = $image_dir."/cache/".$mainimage."_2";
$_SESSION['surfclickimage3'] = $image_dir."/cache/".$mainimage."_3";
$_SESSION['surfclickimage4'] = $image_dir."/cache/".$mainimage."_4";

if (!file_exists($_SESSION['surfclickimage1']) || !file_exists($_SESSION['surfclickimage2']) || !file_exists($_SESSION['surfclickimage3']) || !file_exists($_SESSION['surfclickimage4'])) {
	
	if (!is_dir($image_dir."/cache")) {
		@mkdir($image_dir."/cache");
	}
	
	$clickimageinfo = @getimagesize($image_dir."/".$mainimage);
	
	if ($clickimageinfo[2] == IMAGETYPE_PNG) {
		//Convert PNG To JPG
		$clickimagefile = imagecreatefrompng($image_dir."/".$mainimage);
	} elseif ($clickimageinfo[2] == IMAGETYPE_GIF) {
		//Convert GIF To JPG
		$tempgif = imagecreatefromgif($image_dir."/".$mainimage);
		$clickimagefile = imagecreatetruecolor($imagewidth*4, $imagewidth);
		imagecopy($clickimagefile, $tempgif, 0, 0, 0, 0, $imagewidth, $imagewidth);
	} else {
		$clickimagefile = imagecreatefromjpeg($image_dir."/".$mainimage);
	}
	
	$lcsurfclickimage1 = imagecreatetruecolor($imagewidth, $imagewidth);
	$lcsurfclickimage2 = imagecreatetruecolor($imagewidth, $imagewidth);
	$lcsurfclickimage3 = imagecreatetruecolor($imagewidth, $imagewidth);
	$lcsurfclickimage4 = imagecreatetruecolor($imagewidth, $imagewidth);
	
	imagecopy($lcsurfclickimage1, $clickimagefile, 0, 0, 0, 0, $imagewidth, $imagewidth);
	imagecopy($lcsurfclickimage2, $clickimagefile, 0, 0, $imagewidth, 0, $imagewidth, $imagewidth);
	imagecopy($lcsurfclickimage3, $clickimagefile, 0, 0, $imagewidth*2, 0, $imagewidth, $imagewidth);
	imagecopy($lcsurfclickimage4, $clickimagefile, 0, 0, $imagewidth*3, 0, $imagewidth, $imagewidth);
	
	if (is_dir($image_dir."/cache")) {
		@imagejpeg($lcsurfclickimage1, $_SESSION['surfclickimage1'], 90);
		@imagejpeg($lcsurfclickimage2, $_SESSION['surfclickimage2'], 90);
		@imagejpeg($lcsurfclickimage3, $_SESSION['surfclickimage3'], 90);
		@imagejpeg($lcsurfclickimage4, $_SESSION['surfclickimage4'], 90);
	}
	
	if (!file_exists($_SESSION['surfclickimage1']) || !file_exists($_SESSION['surfclickimage2']) || !file_exists($_SESSION['surfclickimage3']) || !file_exists($_SESSION['surfclickimage4'])) {
		// Cache image creation failed - Store the image data in the session
		ob_start();
		imagejpeg($lcsurfclickimage1);
		$_SESSION['surfclickimage1data'] = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		imagejpeg($lcsurfclickimage2);
		$_SESSION['surfclickimage2data'] = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		imagejpeg($lcsurfclickimage3);
		$_SESSION['surfclickimage3data'] = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		imagejpeg($lcsurfclickimage4);
		$_SESSION['surfclickimage4data'] = ob_get_contents();
		ob_end_clean();
	}
	
}

$icontablewidth = ($imagewidth*5)+30;
$icontableheight = $imagewidth;

$barcode = md5(rand(100,10000));
$_SESSION['surfbarkey'] = $barcode;

$topbaroutput .= "

<table bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0 width=".$icontablewidth." height=".$icontableheight." class='surf_icon'>
<tr><td align=center valign=middle>

<div id=\"myform\" style=\"visibility: hidden;\">
  <img src=\"surficon.php?ts=".time()."\">&nbsp;
  <table style=\"display: inline;\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><img onclick=\"return surfbar_click(1, '".$toclickkeys[1]."');\" src=\"surfclickicon.php?iconid=1&ts=".time()."\"><img onclick=\"return surfbar_click(2, '".$toclickkeys[2]."');\" src=\"surfclickicon.php?iconid=2&ts=".time()."\"><img onclick=\"return surfbar_click(3, '".$toclickkeys[3]."');\" src=\"surfclickicon.php?iconid=3&ts=".time()."\"><img onclick=\"return surfbar_click(4, '".$toclickkeys[4]."');\" src=\"surfclickicon.php?iconid=4&ts=".time()."\">
</td></tr></table>
<div class=\"view_surf_information\"></div>
</div>

</td></tr>
</table>
";

?>