<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Get The Timer Settings
$gettimertype = mysql_query("SELECT value FROM `".$prefix."surf_admin_prefs` WHERE field='timertype'");
if (mysql_num_rows($gettimertype) > 0) {
	$graphicaltimer = mysql_result($gettimertype, 0, "value");
	if (!is_numeric($graphicaltimer) || $graphicaltimer < 0 || $graphicaltimer > 1) {
		$graphicaltimer = 0;
	}
} else {
	$graphicaltimer = 0;
}

$gettimercolor = mysql_query("SELECT value FROM `".$prefix."surf_admin_prefs` WHERE field='timercolor'");
if (mysql_num_rows($gettimercolor) > 0) {
	$timercolor = mysql_result($gettimercolor, 0, "value");
	$timercolor = str_ireplace("#", "", $timercolor);
	if (strlen($timercolor) != 6) {
		$timercolor = "0000FF";
	}
} else {
	$timercolor = "0000FF";
}

// Get The Timer
$gettimer = mysql_query("Select surftimer from `".$prefix."membertypes` where mtid=$acctype limit 1");
$timer = mysql_result($gettimer, 0, "surftimer");

if ($graphicaltimer == 1) {
	
	$topbaroutput .= "
	
	<script language=\"javascript\" type=\"text/javascript\" src=\"jquery-1.10.2.min.js\"></script>
	<script language=\"javascript\" type=\"text/javascript\" src=\"jquery.peity.min.js\"></script>
	
	<style type=\"text/css\">
		#holder1 {
			position:relative;
			top:0px;
			left:0px;
			z-index:1;
		}
		#holder2 {
			position:absolute;
			top:5px;
			left:5px;
			z-index:2;
		}
		#holder3 {
			position:absolute;
			top:17px;
			left:12px;
			width:30px;
			z-index:3;
		}
	</style>
	
	<table bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0 width=60>
	<tr><td align=center>
	
		<div id=\"holder1\">
		<div id=\"pie1\" class=\"pie1\">10/100</div>
		<div id=\"holder2\">
			<div id=\"pie2\" class=\"pie2\">1/1</div>
			<div id=\"holder3\">
				<center><div id=\"timer\" style=\"font-size: 16pt; margin: 0px;\">10</div></center>
			</div>
		</div>
		</div>
	
	</td>
	<td width=10><p>&nbsp;</p></td>
	<td width=10><p>&nbsp;</p></td>
	<td width=10><p>&nbsp;</p></td>
	</tr>
	</table>
	
	<script language=\"javascript\" type=\"text/javascript\">
	$.fn.peity.defaults.pie = {
	  colours: [\"#".$timercolor."\", \"#DDDDDD\"],
	  delimiter: null,
	  diameter: 65,
	  height: null,
	  width: null
	};
	$(\"div.pie1\").peity(\"pie\");
	$.fn.peity.defaults.pie = {
	  colours: [\"#EEEEEE\"],
	  delimiter: null,
	  diameter: 55,
	  height: null,
	  width: null
	};
	$(\"div.pie2\").peity(\"pie\");
	</script>
	
	<script language=\"javascript\">
	  var timer=".$timer.";
	  var pietimer=timer*10;
	  var starttime = pietimer;
	  var startseconds = Math.round(new Date().getTime()/1000);
	  function run () {
		  currentseconds = Math.round(new Date().getTime()/1000);
		  if (pietimer <= 0 || (currentseconds > (startseconds+timer))) {
			  document.getElementById(\"myform\").style.visibility=\"visible\";
			  $(\"div.pie1\").text(\"100/100\").change();
			  document.getElementById(\"timer\").innerHTML=\"GO\";
	
		  } else {
			  
			  if (pietimer%10 == 0) {
			    if (timer+1-Math.ceil(pietimer/10) < currentseconds-startseconds) {
			      pietimer = (timer-(currentseconds-startseconds))*10;
			    }
			  }
			  
			  document.getElementById(\"timer\").innerHTML=Math.ceil(pietimer/10);
			  $(\"div.pie1\").text(Math.ceil(100-(pietimer/starttime)*100) + \"/100\").change();
			  pietimer--;
			  setTimeout(run, 100);
		  }
	  }
	  
	  function reset () {
		  timer = ".$timer.";
		  pietimer=timer*10;
		  startseconds = Math.round(new Date().getTime()/1000);
		  document.getElementById(\"myform\").style.visibility=\"hidden\";
		  document.getElementById(\"myform\").innerHTML=\"Loading...\";
		  run();
	  }
	  
	  document.onLoad=run();

	</script>
	";
	
} else {
	
	$topbaroutput .= "
	
	<table bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0 width=40>
	<tr><td align=center>
	
		<div id=\"timer\" style=\"font-size: 12pt; color: $fontColor; margin: 4px; \"></div>
	
	</td></tr>
	</table>
	
	<script language=\"javascript\">
	  var timer=".$timer.";
	  function run () {
		  if (timer <= 0) {
			  document.getElementById(\"myform\").style.visibility=\"visible\";
			  document.getElementById(\"timer\").innerHTML=\"GO\";
	
		  } else {
			  document.getElementById(\"timer\").innerHTML=timer;
			  timer--;
			  setTimeout(run, 1000);
		  }
	  }
	  
	  function reset () {
		  timer = ".$timer.";
		  document.getElementById(\"myform\").style.visibility=\"hidden\";
		  document.getElementById(\"myform\").innerHTML=\"Loading...\";
		  run();
	  }
	  
	  document.onLoad=run();

	</script>
	";
}

?>