<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.16
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

function get_admin_prefs() {

	include "inc/config.php";
	@mysql_connect($dbhost,$dbuser,$dbpass) or die("user connect: ".mysql_error());
	@mysql_select_db($dbname) or die("user select: ". mysql_error());
	
	$adminprefs = array();
	
	$getadminprefs = mysql_query("Select field, value from ".$prefix."surf_admin_prefs");
	for ($i = 0; $i < mysql_num_rows($getadminprefs); $i++) {
		$fieldval = mysql_result($getadminprefs, $i, "field");
		$valueval = mysql_result($getadminprefs, $i, "value");
		$adminprefs[$fieldval] = $valueval;
	}
	return $adminprefs;
}


function get_user_prefs($userid=0) {
	
	include "inc/config.php";
	@mysql_connect($dbhost,$dbuser,$dbpass) or die("user connect: ".mysql_error());
	@mysql_select_db($dbname) or die("user select: ". mysql_error());
	
	$adminprefs = get_admin_prefs();
	$userprefs = array();
	
	// Set Default Theme
	$countuserpref = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."surf_user_prefs where userid=".$userid), 0);
	if ($countuserpref < 1) {
		if ($adminprefs['surftheme'] > 0) {
			if ($adminprefs['surftheme'] == 1) {
		
				$surfthemedata['surfbarstyle'] = 2;
				$surfthemedata['surfbar_pos'] = 1;
				$surfthemedata['footer_pos'] = 4;
				$surfthemedata['footer_transparent'] = 1;
				$surfthemedata['footertype'] = 2;
				$surfthemedata['framesites'] = -1;
		
			} elseif ($adminprefs['surftheme'] == 2) {
	
				$surfthemedata['surfbarstyle'] = 2;
				$surfthemedata['surfbar_pos'] = 2;
				$surfthemedata['footer_pos'] = 4;
				$surfthemedata['footer_transparent'] = 0;
				$surfthemedata['footertype'] = 1;
				$surfthemedata['framesites'] = 1;
		
			} else {
	
				$surfthemedata['surfbarstyle'] = 1;
				$surfthemedata['surfbar_pos'] = 2;
				$surfthemedata['footer_pos'] = 4;
				$surfthemedata['footer_transparent'] = 0;
				$surfthemedata['footertype'] = 1;
				$surfthemedata['framesites'] = 1;
			}
			
			$surfthemedata['surficontype'] = $adminprefs['surficontype'];
			
			if ($surfthemedata['surfbar_pos'] == 1) {
				$surfbardrop = 1;
				$surfbartop = 1;
			} elseif ($surfthemedata['surfbar_pos'] == 2) {
				$surfbardrop = 0;
				$surfbartop = 1;
			} else {
				$surfbardrop = 0;
				$surfbartop = 0;
			}
	
			// Update settings
			@mysql_query("Delete from ".$prefix."surf_user_prefs where userid=".$userid);
	
		  	@mysql_query("Insert into ".$prefix."surf_user_prefs (field, userid, value) VALUES
		  	('surficontype', ".$userid.", '".$surfthemedata['surficontype']."'),
			('surfbarstyle', ".$userid.", '".$surfthemedata['surfbarstyle']."'),
			('preloadsites', ".$userid.", '1'),
			('surfbardrop', ".$userid.", '".$surfbardrop."'),
			('surfbartop', ".$userid.", '".$surfbartop."'),
			('framesites', ".$userid.", '".$surfthemedata['framesites']."'),
			('footer_pos', ".$userid.", '".$surfthemedata['footer_pos']."'),
			('can_move_footer', ".$userid.", '1'),
			('footertype', ".$userid.", '".$surfthemedata['footertype']."'),
			('footer_transparent', ".$userid.", '".$surfthemedata['footer_transparent']."');");
			
		}
	}
	// End Set Default Theme
	
	foreach ($adminprefs as $key=>$value) {
		if ($value == -1) {
			// Admin Value Is User's Choice
			$getuserpref = mysql_query("Select value from ".$prefix."surf_user_prefs where userid=".$userid." and field='".$key."'");
			if (mysql_num_rows($getuserpref) > 0 && (@mysql_result($getuserpref, 0, "value") >= 0)) {
				// Set Value To User Pref
				$userprefs[$key] = mysql_result($getuserpref, 0, "value");
			} else {
				// Set Value To Default
				switch ($key) {
					case "surficontype":
						$userprefs[$key] = 2;
						break;
					case "surfbarstyle":
						$userprefs[$key] = 2;
						break;
					case "preloadsites":
						$userprefs[$key] = 1;
						break;
					case "surfbardrop":
						$userprefs[$key] = 1;
						break;
					case "surfbartop":
						$userprefs[$key] = 1;
						break;
					case "footer_pos":
						$userprefs[$key] = 3;
						break;
					case "can_move_footer":
						$userprefs[$key] = 1;
						break;
					case "footer_transparent":
						$userprefs[$key] = 1;
						break;
					case "footertype":
						$userprefs[$key] = 2;
						break;
					case "framesites":
						// This choice is for the member promoting
						// the site in rotation, not the surfer
						$userprefs[$key] = -1;
						break;
					default:
						$userprefs[$key] = 1;
				}
			}
		} else {
			$userprefs[$key] = $value;
		}
	}
	return $userprefs;
}

?>