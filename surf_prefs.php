<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

function get_admin_prefs() {

	include "inc/config.php";
	@mysql_connect($dbhost,$dbuser,$dbpass) or die("user connect: ".mysql_error());
	@mysql_select_db($dbname) or die("user select: ". mysql_error());
	
	$adminprefs = array();
	
	$getadminprefs = mysql_query("Select field, value from ".$prefix."surf_admin_prefs");
	for ($i = 0; $i < mysql_num_rows($getadminprefs); $i++) {
		$fieldval = mysql_result($getadminprefs, $i, "field");
		$valueval = mysql_result($getadminprefs, $i, "value");
		$adminprefs[$fieldval] = $valueval;
	}
	return $adminprefs;
}


function get_user_prefs($userid=0) {
	
	include "inc/config.php";
	@mysql_connect($dbhost,$dbuser,$dbpass) or die("user connect: ".mysql_error());
	@mysql_select_db($dbname) or die("user select: ". mysql_error());
	
	$adminprefs = get_admin_prefs();
	$userprefs = array();
	
	foreach ($adminprefs as $key=>$value) {
		if ($value == -1) {
			// Admin Value Is User's Choice
			$getuserpref = mysql_query("Select value from ".$prefix."surf_user_prefs where userid=".$userid." and field='".$key."'");
			if (mysql_num_rows($getuserpref) > 0 && (@mysql_result($getuserpref, 0, "value") >= 0)) {
				// Set Value To User Pref
				$userprefs[$key] = mysql_result($getuserpref, 0, "value");
			} else {
				// Set Value To Default
				switch ($key) {
					case "surfbarstyle":
						$userprefs[$key] = 2;
						break;
					case "preloadsites":
						$userprefs[$key] = 1;
						break;
					default:
						$userprefs[$key] = 1;
				}
			}
		} else {
			$userprefs[$key] = $value;
		}
	}
	
	// These Are No Longer Used But Some Plugins May Still Look For Them
	$userprefs['surfbardrop'] = "0";
	$userprefs['surfbartop'] = "1";
	$userprefs['framesites'] = "1";
	$userprefs['footer_pos'] = "3";
	$userprefs['footertype'] = "1";
	$userprefs['footer_transparent'] = "0";
	
	return $userprefs;
}

?>