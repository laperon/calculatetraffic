<?php

#########################################
##     LFMTE Prize Page Addon v2       ##
##                                     ##
##        �2011 LJ Fix LLC             ##
##           www.LJFix.com             ##
##                                     ##
#########################################

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

// Prize Page Settings
$pPrizePage = "http://".$_SERVER["SERVER_NAME"]."/prizepage.php"; // URL for Prize Page
$pTable = $prefix.'prizepage_prizes';
$pWinnersTable = $prefix.'prizepage_prize_winners';
$pSettingsTable = $prefix.'prizepage_prize_settings';

$date = date("Y-m-d");

$nextPrizeView2=0;
$prizeurl = "";

$rand_limits[0]=mysql_result(mysql_query("select `value` from `$pSettingsTable` where `field`='rnd_a'"),0);
$rand_limits[1]=mysql_result(mysql_query("select `value` from `$pSettingsTable` where `field`='rnd_b'"),0);

$per_mem = mysql_result(mysql_query("select `value` from `$pSettingsTable` where `field`='per_mem'"),0);

$prizeAvail=mysql_result(mysql_query("select count(`id`) from `$pTable` where `available`=1 and (`qty_in` - `qty_out`) > 0"),0);
		
$LimitDaily = 0;
$pMaxWin=@mysql_result(@mysql_query("select `value` from `$pSettingsTable` where `field`='max_win'"),0);
switch ($pMaxWin) {
	case 1:
		$pDays='1 day ago';
		break;
	case 2:
		$pDays='7 days ago';
		break;
	case 3:
		$pDays='1 month ago';
		break;
	case 4:
		$LimitDaily='2';
		$pDays='1 day ago';
		break;
	case 5:
		$LimitDaily='4';
		$pDays='1 day ago';
		break;
	case 6:
		$LimitDaily='8';
		$pDays='1 day ago';
		break;
	case 7:
		$LimitDaily='10';
		$pDays='1 day ago';
		break;
	case 8:
		$LimitDaily='20';
		$pDays='1 day ago';
		break;
	case 9:
		$LimitDaily='50';
		$pDays='1 day ago';
		break;
	default:
		$pDays=0;
		break;
}

if ($LimitDaily == 0) {
	$pDay = strftime("%Y-%m-%d", strtotime("$date + $pDays"));
	$pCount=mysql_result(mysql_query("select count(`user_id`) from `$pWinnersTable` where `user_id`=$userid and `datetime`>'$pDay'"),0);
} else {
	$pCount=mysql_result(mysql_query("select count(`user_id`) from `$pWinnersTable` where `user_id`=$userid and DATE(datetime)='$date'"),0);
	if ($pCount < $LimitDaily) {
		// Hasn't reached limit yet
		$pCount = 0;
	} else {
		// Earned max prizes today
		$pCount = 1;
	}
}

if ($per_mem == 0) {
	// Site-wide Views
	if ($prizeAvail > 0 and 0 != $rand_limits[1] and (0 == $pCount or 0 == $pDays)) {
		$cnx = mysql_query("select sum(`clickstoday`) from `".$prefix."members`");
		$siteWideViewed = mysql_result($cnx,0);
		mysql_query("LOCK TABLES $pSettingsTable,$pWinnersTable WRITE");
		if ($siteWideViewed < 0 or $siteWideViewed == NULL) {
			$siteWideViewed=0;
			$nextPrizeView=0;
		} else {
			$nextPrizeView=mysql_result(mysql_query("select `value` from `$pSettingsTable` where `field`='next_view'"),0);
		}
		
		if ($nextPrizeView <= $siteWideViewed and $siteWideViewed != 0 and $rand_limits[1] != 0){
			$nextPrizeView2=$siteWideViewed + rand($rand_limits[0],$rand_limits[1]) + 1; //added one because the random page counts as one

				$prizeurl = $pPrizePage;
				
		} elseif ($nextPrizeView > $siteWideViewed and ($siteWideViewed+$rand_limits[1]) < $nextPrizeView) {
			$nextPrizeView2=$siteWideViewed + rand($rand_limits[0],$rand_limits[1]);
		}
		if ($nextPrizeView != $nextPrizeView2 and $nextPrizeView2 != 0) {
			mysql_query("update `$pSettingsTable` set `value`=$nextPrizeView2 where `field`='next_view'");
		}
		mysql_query('UNLOCK TABLES');
	}
} else {
	// Per Member Views
	if ($prizeAvail > 0 and 0 != $rand_limits[1] and (0 == $pCount or 0 == $pDays)) {
		$cnx = mysql_query("select clickstoday from `".$prefix."members` where Id=$userid limit 1");
		$siteWideViewed = mysql_result($cnx,0);
		mysql_query("LOCK TABLES $pSettingsTable,$pWinnersTable WRITE");
		if ($siteWideViewed < 0 or $siteWideViewed == NULL) {
			$siteWideViewed=0;
			$nextPrizeView=0;
		} else {
			$nextPrizeView=mysql_result(mysql_query("select `next_prize` from `".$prefix."members` where Id=$userid limit 1"),0);
		}
		
		if ($nextPrizeView <= $siteWideViewed and $siteWideViewed != 0 and $rand_limits[1] != 0){
			$nextPrizeView2=$siteWideViewed + rand($rand_limits[0],$rand_limits[1]) + 1; //added one because the random page counts as one

				$prizeurl = $pPrizePage;
				
		} elseif ($nextPrizeView > $siteWideViewed and ($siteWideViewed+$rand_limits[1]) < $nextPrizeView) {
			$nextPrizeView2=$siteWideViewed + rand($rand_limits[0],$rand_limits[1]);
		}
		if ($nextPrizeView != $nextPrizeView2 and $nextPrizeView2 != 0) {
			mysql_query("update `".$prefix."members` set `next_prize`=$nextPrizeView2 where Id=$userid limit 1");
		}
		mysql_query('UNLOCK TABLES');
	}
}

if ($prizeurl != "") {
	$newsite = $prizeurl;
	$_SESSION['prizepage']['on'] = 'PrizeOn';
	$_SESSION['prizepage']['clickcount'] = 0;
} else {
	if (!isset($_SESSION['prizepage']['clickcount']) || !is_numeric($_SESSION['prizepage']['clickcount'])) {
		$_SESSION['prizepage']['clickcount'] = 0;
	} elseif ($_SESSION['prizepage']['clickcount'] > 2) {
		unset($_SESSION['prizepage']);
	} else {
		$_SESSION['prizepage']['clickcount'] = $_SESSION['prizepage']['clickcount'] + 1;
	}
}

?>