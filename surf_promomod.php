<?php

// LFMTE Surf Promo Code Plugin v3
// �2013 LFM Wealth Systems
// http://thetrafficexchangescript.com

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

$getuserdata = mysql_query("Select email, mtype, lastip from ".$prefix."members where Id=$userid") or die(mysql_error());
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$memip = mysql_result($getuserdata, 0, "lastip");

$prizewon = 0;

// Check for active code
$checkcode = mysql_query("Select codeid from ".$prefix."promo_users where state=0 and userid=$userid");
if (mysql_num_rows($checkcode) > 0) {
	for ($i = 0; $i < mysql_num_rows($checkcode); $i++) {
		$codeid = mysql_result($checkcode, $i, "codeid");
		$getcodeinfo = mysql_query("Select * from ".$prefix."promo_codes where id=$codeid");
		if (mysql_num_rows($getcodeinfo) == 0) {
			// The code does not exist
			mysql_query("Delete from ".$prefix."promo_users where codeid=$codeid and userid=$userid");
		} else {
			$clicksreq = mysql_result($getcodeinfo, 0, "clicks");
			if (($clickstoday+1) >= $clicksreq) { // Include the extra view to match the counter
				// Member has earned that prize
				$prizeid = mysql_result($getcodeinfo, 0, "prizeid");
				$getprizeinfo = mysql_query("Select * from ".$prefix."promo_prizes where id=$prizeid");
				if (mysql_num_rows($getprizeinfo) == 0) {
					// Prize does not exist
					mysql_query("Delete from ".$prefix."promo_users where codeid=$codeid and userid=$userid");
				} else {
					
					$prizewon = $prizeid;
					
					// Add A Download
					$prizedl = mysql_result($getprizeinfo, 0, "download");
					if ($prizedl > 0) {
						$checkexisting = mysql_query("Select * from ".$prefix."purchases where affid=".$userid." and itemid=".$prizedl." limit 1");
						if (mysql_num_rows($checkexisting) == 0) {
							mysql_query("INSERT INTO ".$prefix."purchases(affid,itemid,txn_id) VALUES(".$userid.",'".$prizedl."','0')");
						}
					}
					
					// Add Prizes
					$prizecredits = mysql_result($getprizeinfo, 0, "credits");
					if ($prizecredits > 0) {
						mysql_query("Update `".$prefix."members` set credits=credits+".$prizecredits." where Id=$userid limit 1");
					}
					
					$prizebanners = mysql_result($getprizeinfo, 0, "banners");
					if ($prizebanners > 0) {
						mysql_query("Update `".$prefix."members` set bannerimps=bannerimps+".$prizebanners." where Id=$userid limit 1");
					}
					
					$prizetexts = mysql_result($getprizeinfo, 0, "texts");
					if ($prizetexts > 0) {
						mysql_query("Update `".$prefix."members` set textimps=textimps+".$prizetexts." where Id=$userid limit 1");
					}
					
					$prizecash = mysql_result($getprizeinfo, 0, "cash");
					if ($prizecash > 0) {
						mysql_query("Insert into `".$prefix."sales` (affid, saledate, itemid, itemname, itemamount, commission, txn_id, prize) values ($userid, NOW(), 0, 'Promo Code Prize', '".$prizecash."', '".$prizecash."', '0', 1)");
					}
					
					$prizepeelimps = mysql_result($getprizeinfo, 0, "peelimps");
					if ($prizepeelimps > 0) {
						@mysql_query("Update `".$prefix."members` set peelimps=peelimps+".$prizepeelimps." where Id=$userid limit 1");
					}
					
					$prizesqbanimps = mysql_result($getprizeinfo, 0, "sqbanimps");
					if ($prizesqbanimps > 0) {
						@mysql_query("Update `".$prefix."members` set sqbanimps=sqbanimps+".$prizesqbanimps." where Id=$userid limit 1");
					}
					
					$prizeporkyps = mysql_result($getprizeinfo, 0, "porkyps");
					if ($prizeporkyps > 0) {
						if (file_exists("inc/porkyp_funcs.php")) {
							require_once "inc/porkyp_funcs.php";
							$porkytransfer = porkytransferpoints($useremail, $memip, $prizeporkyps);
						}
					}
					
					$prizejetfuel = mysql_result($getprizeinfo, 0, "jetfuel");
					if ($prizejetfuel > 0) {
						$getjetfuelpack = mysql_query("SELECT * FROM `".$prefix."jetfuel` WHERE id='".$prizejetfuel."'");
						if (mysql_num_rows($getjetfuelpack) > 0) {
							$packname = mysql_result($getjetfuelpack, 0, "name");
							$duration = mysql_result($getjetfuelpack, 0, "duration");
							$multiplier = mysql_result($getjetfuelpack, 0, "multiplier");
							// Check Existing
							$getexisting = mysql_query("SELECT endtime FROM ".$prefix."jetfuel_users WHERE userid='".$userid."' AND active='1' ORDER BY endtime DESC LIMIT 1") or die(mysql_error());
							if (mysql_num_rows($getexisting) > 0) {
								$existingend = mysql_result($getexisting, 0, "endtime");
								$timeleft = $existingend - time();
								if ($timeleft < 1) {
									$timeleft = 0;
								}
							} else {
								$timeleft = 0;
							}
							$starttime = time() + $timeleft + 1;
							$endtime = $starttime + round($duration * 3600);
							$codetext = mysql_result($getcodeinfo, 0, "code");
							mysql_query("INSERT INTO ".$prefix."jetfuel_users (userid, starttime, endtime, multiplier, active, fromtype) VALUES('".$userid."', '".$starttime."', '".$endtime."', '".$multiplier."', '1', 'Completed Promo Code: ".$codetext."')");
						}
					}
					
					// Send The Member An Email
					$sendemail = mysql_result($getprizeinfo, 0, "sendemail");
					if ($sendemail > 0) {
						$emailsubj = mysql_result($getprizeinfo, 0, "emailsubj");
						$emailtext = mysql_result($getprizeinfo, 0, "emailtext");
						$refid = mysql_result($getcodeinfo, 0, "refid");
						$emailtext = str_replace('#REFID#', $refid, $emailtext);
						$emailtext=translate_site_tags($emailtext);
						$emailtext=translate_user_tags($emailtext,$useremail);
						
						$getsiteinfo = mysql_query("Select sitename, affurl from `".$prefix."settings` where id=1 limit 1");
						if (mysql_num_rows($getsiteinfo) > 0) {
							$sitename = mysql_result($getsiteinfo, 0, "sitename");
							$siteurl = mysql_result($getsiteinfo, 0, "affurl");
						} else {
							$sitename = "Traffic Exchange";
							$siteurl = "http://thetrafficexchangescript.com";
						}
						$getadmininfo = mysql_query("Select email from `".$prefix."admin` where id=1 limit 1");
						if (mysql_num_rows($getadmininfo) > 0) {
							$adminemail = mysql_result($getadmininfo, 0, "email");
						} else {
							$adminemail = "lfmteipn@trafficmods.com";
						}
						
						$emailheader = "MIME-Version: 1.0\nContent-type: text/plain; charset=iso-8859-1\nFrom: ".$sitename." <".$adminemail.">\nReply-To: ".$sitename." <".$adminemail.">\nX-Priority: 3\nX-Mailer: PHP 4\n";
						mail($useremail, $emailsubj, $emailtext, $emailheader);
					}
					
					// Send An Admin Email
					$sendadmin = mysql_result($getcodeinfo, 0, "sendadmin");
					if ($sendadmin > 0) {
						$adminto = mysql_result($getcodeinfo, 0, "adminto");
						$emailsubj = mysql_result($getcodeinfo, 0, "emailsubj");
						$mailtext = mysql_result($getcodeinfo, 0, "adminemail");
						$codetext = mysql_result($getcodeinfo, 0, "code");
						
						$mailtext = str_replace('#SURFCODE#', $codetext, $mailtext);
						$mailtext=translate_site_tags($mailtext);
						$mailtext=translate_user_tags($mailtext,$useremail);
						
						$getsiteinfo = mysql_query("Select sitename, affurl from `".$prefix."settings` where id=1 limit 1");
						if (mysql_num_rows($getsiteinfo) > 0) {
							$sitename = mysql_result($getsiteinfo, 0, "sitename");
							$siteurl = mysql_result($getsiteinfo, 0, "affurl");
						} else {
							$sitename = "Traffic Exchange";
							$siteurl = "http://thetrafficexchangescript.com";
						}
						$getadmininfo = mysql_query("Select email from `".$prefix."admin` where id=1 limit 1");
						if (mysql_num_rows($getadmininfo) > 0) {
							$adminemail = mysql_result($getadmininfo, 0, "email");
						} else {
							$adminemail = "lfmteipn@trafficmods.com";
						}
						
						$emailheader = "MIME-Version: 1.0\nContent-type: text/plain; charset=iso-8859-1\nFrom: ".$sitename." <".$adminemail.">\nReply-To: ".$sitename." <".$adminemail.">\nX-Priority: 3\nX-Mailer: PHP 4\n";
						mail($adminto, $emailsubj, $mailtext, $emailheader);
						
					}
					
					mysql_query("Update ".$prefix."promo_users set state=1, date=NOW() where codeid=$codeid and userid=$userid");
				}
			}
		}
	}
}

if ($prizewon > 0) {
    $newsite = "surfpromowon.php?prizeid=".$prizewon;
}

?>