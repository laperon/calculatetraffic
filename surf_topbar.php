<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
require_once "surfbarColors.php";

require_once "sfunctions.php"; //Main functions
require_once "surfoutput.php"; //Output functions

$topbaroutput = "
<style type=\"text/css\">
<!--
.surfbar {
	color: $fontColor;
}
-->
</style>


<center>
<table id=\"surfbar\" class=\"surfbar\" bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0>
<tr>";

$numofmodules = 0;
$getmodules = mysql_query("SELECT id, filename, html FROM `".$prefix."barmods_topbar` WHERE state='1' ORDER BY rank ASC");
while ($modlist = mysql_fetch_array($getmodules)) {
	$topbaroutput .= "<td"; if ($modlist['filename'] == "classiclayout.php") { $topbaroutput .= " width=\"100%\""; } $topbaroutput .= ">";
	
	$modfilename = trim($modlist['filename']);
	if ($modfilename == "None" && strlen($modlist['html']) > 0) {
		$topbaroutput .= "<div id=\"modbox".$modlist['id']."\">";
		$modlist['html'] = translate_site_tags($modlist['html']);
		$modlist['html'] = translate_user_tags($modlist['html'], $useremail);
		$topbaroutput .= $modlist['html'];
		$topbaroutput .= "</div>";
		$numofmodules++;
	} else {
		if (file_exists("surf_modules/".$modfilename)) {
			include("surf_modules/".$modfilename);
			$numofmodules++;
		}
	}
	
	$topbaroutput .= "</td>";
}

$topbaroutput .= "</tr>";

$getextensions = mysql_query("SELECT id, filename, html FROM `".$prefix."barmods_extensions` WHERE state='1' ORDER BY rank ASC");
while ($modlist = mysql_fetch_array($getextensions)) {
	$topbaroutput .= "<tr><td colspan=".$numofmodules.">";
	
	$modfilename = trim($modlist['filename']);
	if ($modfilename == "None" && strlen($modlist['html']) > 0) {
		$topbaroutput .= "<div id=\"extbox".$modlist['id']."\">";
		$modlist['html'] = translate_site_tags($modlist['html']);
		$modlist['html'] = translate_user_tags($modlist['html'], $useremail);
		$topbaroutput .= $modlist['html'];
		$topbaroutput .= "</div>";
	} else {
		if (file_exists("surf_extensions/".$modfilename)) {
			include("surf_extensions/".$modfilename);
		}
	}
	
	$topbaroutput .= "</td></tr>";
}

$topbaroutput .= "</table>";

$topbaroutput = preg_replace("/((\r(?!\n))|((?<!\r)\n)|(\r\n))/", "", $topbaroutput);

$output .= $topbaroutput;
?>

