<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.08
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}

//Check for a word search claim page
$checkclaimpage = mysql_query("Select value from ".$prefix."wssettings where field='claimpage' limit 1");
$claimpage = mysql_result($checkclaimpage, 0, "value");
// Make sure the claim page wasn't already preloaded on the last view
if ($_SESSION["showingclaimpage"] == 1) {
	$_SESSION["showingclaimpage"] = 0;
} else {
	if (($claimpage == 1) && ($_SESSION["checkletter"] == 1) && (checkforclaimpage($userid) == 1)) {
        $_SESSION["showingclaimpage"] = 1;
		$newsite = "letterhuntpage.php";
	}
}

?>