<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.08
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
require_once "surfbarColors.php";

session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];
$_SESSION["prefix"] = $prefix;

if ($_GET['surfcode'] != $_SESSION['surfingkey']) {
	header("Location: surfing.php");
	exit;
}

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

include("sfunctions.php"); //Main functions
include("surfoutput.php"); //Output functions

// Get The Timer
$gettimer = mysql_query("Select surftimer from `".$prefix."membertypes` where mtid=$acctype limit 1");
$timer = mysql_result($gettimer, 0, "surftimer");
// Get The Images
include("surfimages.php");

$sysmess = $_SESSION['sysmess'];

// Create The Buttons

$theimagekey = array_rand($images);
$toclick = rand(1,4);

$theimage = $images[$theimagekey];
$imagedata = explode("|", $theimage);

$mainimage = $imagedata[0];
$showimage = $imagedata[$toclick];

$_SESSION['surfimage'] = $image_dir."/".$showimage;
$_SESSION['surfclickimage'] = $image_dir."/".$mainimage;

$imagesize = @getimagesize($image_dir."/".$showimage);
$imagewidth = $imagesize[0];

$_SESSION['mincorrect'] = $imagewidth*$toclick-$imagewidth;
$_SESSION['maxcorrect'] = $imagewidth*$toclick;

$imgdisp = "<input type=\"image\" src=\"surfclickimage.php\">";

$barcode = md5(rand(100,10000));
$_SESSION['surfbarkey'] = $barcode;

$usercredits = round($usercredits, 2);

//Get Default URLs
$getdefaults = mysql_query("Select defbanimg, defbantar, deftextad, deftexttar from `".$prefix."settings` limit 1");
$defbanimg = mysql_result($getdefaults, 0, "defbanimg");
$defbantar = mysql_result($getdefaults, 0, "defbantar");
$deftextad = mysql_result($getdefaults, 0, "deftextad");
$deftexttar = mysql_result($getdefaults, 0, "deftexttar");

$output = "<html>
<style type=\"text/css\">
<!--
.surfbar {
	color: $fontColor;
}
-->
</style>

<body>

<script language=\"javascript\">
    if (parent.location.href==window.location.href) {
	 window.location.href = \"surfing.php\";
	}

    function userClicked() {
	 top.legitclick=1;
	 return true;
	}

</script>

<table class=\"surfbar\" bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0 width=100%>
<tr><td align=left>

<div>".$sysmess."<br>Account Credits: ".$usercredits."
<div id=\"timer\" style=\"font-size: 12pt; margin: 4px; \"></div>

</td><td align=center>

<div id=\"myform\" style=\"visibility: hidden;\">
<form style=\"margin:2px\" target=\"_top\" onsubmit=\"document.getElementById('myform').style.visibility='hidden'; userClicked();\" action=\"surfing.php\" method=\"post\">
  <font size=2>Click</font><img src=\"surfimage.php\">&nbsp;&nbsp;
  <div style=\"border: 1px #000 solid; display: inline; padding: 3px; \">".$imgdisp."</div>
  <input type=\"hidden\" name=\"barcode\" value=\"$barcode\">
  <input type=\"hidden\" name=\"mysubcode\" id=\"mysubcode\" value=\"\">
</form>
</div>
<a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"members.php\" onclick=\"userClicked();\">Home</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" onclick=\"userClicked();\" href=\"".$_SESSION['currentsite']."\">Open Site</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" onclick=\"userClicked();\" href=\"reportsite.php\">Report Site</a> | <a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"surfbar_settings.php\" onclick=\"userClicked();\">Surf Settings</a>

</td><td width=470 align=right>

".showbanner($userid, $defbanimg, $defbantar)."
<br>
<center>".showtext($userid, 50, $deftextad, $deftexttar, $textImpColor)."</center>

</td></tr>
</table>

<script language=\"javascript\">
  var timer=".$timer.";
  function run () {
	  if (timer <= 0) {
		  document.getElementById('myform').style.visibility='visible';
		  document.getElementById('timer').innerHTML='GO!';

	  } else {
		  document.getElementById('timer').innerHTML=timer;
		  timer--;
		  setTimeout(run, 1000);
	  }
  }
  document.onLoad=run();

  function setform (\$thecode) {
	  document.getElementById('mysubcode').value=\$thecode;
  }
</script>";

$output = str_replace("
", "", $output);

$encryptsurf = mysql_result(mysql_query("Select encrypt from `".$prefix."anticheat` where id=1"), 0);
if ($encryptsurf == 1) {
	surfoutput($output);
} else {
	echo($output);
}

?>