<?
/*
	Below you can change the text and background colors of the top and bottom
	surfbar images.

	Either the name of the color or a hex value can be used for the color.
	"white" is equivalent to "#FFFFFF"
	"red" is equivalent to "#FF0000"
	The hex value will offer a much greater range of color matching.
*/

// Bottom Surf Background and Font colors
$bottombgcolor = "white"; //Bottom surbar color
$bottomfontColor = "black"; // Bottom surfbar font color

// Top Surfbar Background and Font colors

$bgcolor = "white";// Top surfbar background color
$fontColor = "black";// Top surfbar font color main font
$textImpColor = "blue"; //Text impression font color (below banner)
$menuItems = "blue";  // Surfbar menu items (My Account, Open Site, etc)
$creditColor = "blue"; // Credited account text colors

?>