<?
/*
	Below you can change the sizes of various components of the surfbar.
	Each value represents the number of pixels.
*/

$classic_surfbar_height = 80;

$modern_surfbartopheight = 74; // The height of the top part of the surfbar (where the banner and icons are)
$modern_surfbarheight = 114; // The height of the entire surfbar

$modern_surfbarwidth = 800;


$classic_surf_footer_height = 48;

$modern_surf_footer_height = 75;
$modern_surf_footer_width = 315;

###############################################################################################################

/*
	DEFAULT VALUES - INFORMATIONAL PURPOSES ONLY - DO NOT MODIFY THE VALUES BELOW
	
	$classic_surfbar_height = 80;

	$modern_surfbartopheight = 74; // The height of the top part of the surfbar (where the banner and icons are)
	$modern_surfbarheight = 114; // The height of the entire surfbar
	
	$modern_surfbarwidth = 800;
	
	
	$classic_surf_footer_height = 48;
	
	$modern_surf_footer_height = 75;
	$modern_surf_footer_width = 315;
*/

?>