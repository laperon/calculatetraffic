<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.07
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

include "surfbarColors.php";

require_once "lhuntfunctions.php"; //Word Search Game

$getacctype = mysql_query("Select status, credits, mtype, lastclick, clickstoday from `".$prefix."members` where Id=$userid limit 1");
$usercredits = mysql_result($getacctype, 0, "credits");
$acctype = mysql_result($getacctype, 0, "mtype");
$accstatus = mysql_result($getacctype, 0, "status");
$lastclicktime = mysql_result($getacctype, 0, "lastclick");
$clickstoday = mysql_result($getacctype, 0, "clickstoday");

if ($accstatus == "Suspended") {
header("Location: members.php?suspended=yes");
exit;
}

include("sfunctions.php"); //Main functions
include("smfunctions.php"); //Member functions

include("surf_geturl.php"); //Get URL functions

$gettimer = mysql_query("Select surftimer from `".$prefix."membertypes` where mtid=$acctype limit 1");
$timer = mysql_result($gettimer, 0, "surftimer");

$preloadsites = $_SESSION['preload_sess'];


// Get The Images
include("surfimages.php");

$sysmess = $_SESSION['sysmess'];

$usercredits = round($usercredits, 2);

//Get Default URLs
$getdefaults = mysql_query("Select defbanimg, defbantar, deftextad, deftexttar from `".$prefix."settings` limit 1");
$defbanimg = mysql_result($getdefaults, 0, "defbanimg");
$defbantar = mysql_result($getdefaults, 0, "defbantar");
$deftextad = mysql_result($getdefaults, 0, "deftextad");
$deftexttar = mysql_result($getdefaults, 0, "deftexttar");


// Get frames content
if (!isset($_SESSION['frame1site']) || $_SESSION['frame1site'] == "") {
	// Restart Surfing Session
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['frame2site'] = "about:blank";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	
	if ($preloadsites != 1) {
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 0;
		$_SESSION['showingframe'] = 0;
	} else {
		$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 1;
		$_SESSION['showingframe'] = 1;
	}

} elseif ($preloadsites != 1) {

	$_SESSION['showingframe'] = 1;
	$_SESSION['switchframe1'] = 0;
	$_SESSION['switchframe2'] = 0;
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	if (is_numeric($_SESSION["newsiteid"])) {
		@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=".$_SESSION["newsiteid"]." where Id=$userid limit 1");
	} else {
		@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=0 where Id=$userid limit 1");
	}
	
} else {
	// Switch Frames
	
	if ($_SESSION['showingframe'] == 1) {
		// Display frame 2 and switch frame 1
		$_SESSION['showingframe'] = 2;
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 0;
		if (is_numeric($_SESSION["newsiteid"])) {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=".$_SESSION["newsiteid"]." where Id=$userid limit 1");
		} else {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=0 where Id=$userid limit 1");
		}
		$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
		$_SESSION['currentsite'] = $_SESSION['frame2site'];
	} else {
		// Display frame 1 and switch frame 2
		$_SESSION['showingframe'] = 1;
		$_SESSION['switchframe1'] = 0;
		$_SESSION['switchframe2'] = 1;
		if (is_numeric($_SESSION["newsiteid"])) {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=".$_SESSION["newsiteid"]." where Id=$userid limit 1");
		} else {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=0 where Id=$userid limit 1");
		}
		$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
		$_SESSION['currentsite'] = $_SESSION['frame1site'];
	}
}

// Check if the site should be framed
if ($_SESSION['framesites'] == 1) {
	$framethissite = 1;
} elseif ($_SESSION['framesites'] == -1) {
	$checksite = mysql_query("Select framesite from `".$prefix."msites` where url='".$_SESSION['currentsite']."' limit 1");
	if (mysql_num_rows($checksite) > 0) {
		$framethissite = mysql_result($checksite, 0, "framesite");
	} else {
		$framethissite = 0;
	}
} else {
	$framethissite = 0;
}


// Check the last click
	
checkautoassign($userid, $acctype);

if (!isset($_GET['xval']) || !is_numeric($_GET['xval'])) {
	$_SESSION['sysmess'] = "<span style=\"color: blue;\"><b>Starting surf session</b></span>";
	$_SESSION["refreshcount"] = $_SESSION["refreshcount"]+1;
	$_SESSION["siteviewed"] = 0;
	$_SESSION["clickcount"] = 0;
	$_SESSION["checkletter"] = 0;

} elseif ($lastclicktime > (time()-($timer-2))) {
	//Multiple browsers detected
	@mysql_query("Update `".$prefix."members` set lastclick=0 where Id=$userid limit 1");
	header("Location: members.php?mf=lo");
	exit;

} elseif (($_GET['xval'] < $_SESSION['mincorrect']) || ($_GET['xval'] > $_SESSION['maxcorrect'])) {
	$_SESSION["refreshcount"] = 0;
	$_SESSION["checkletter"] = 0;
	wrongclick($userid);

/*
} elseif ($_POST['barcode'] != $_SESSION['surfbarkey']) {
	$_SESSION['sysmess'] = "<span style=\"color: red;\"><b>Verification error</b></span>";
	$_SESSION["refreshcount"] = 0;
	$_SESSION["siteviewed"] = 0;
	$_SESSION["checkletter"] = 0;
*/	
} elseif (($_GET['xval'] >= $_SESSION['mincorrect']) && ($_GET['xval'] <= $_SESSION['maxcorrect'])) {
	$_SESSION["refreshcount"] = 0;
	$_SESSION["clickcount"] = $_SESSION["clickcount"] + 1;
	$_SESSION["checkletter"] = 1;
	creditmember($userid, $acctype,$creditColor);
	
} else {
	$_SESSION['sysmess'] = "<span style=\"color: blue;\"><b>Starting surf session</b></span>";
	$_SESSION["refreshcount"] = $_SESSION["refreshcount"]+1;
	$_SESSION["siteviewed"] = 0;
	$_SESSION["clickcount"] = 0;
	$_SESSION["checkletter"] = 0;
}

// Create The Buttons

$theimagekey = array_rand($images);
$toclick = rand(1,4);

$theimage = $images[$theimagekey];
$imagedata = explode("|", $theimage);

$mainimage = $imagedata[0];
$showimage = $imagedata[$toclick];

$_SESSION['surfimage'] = $image_dir."/".$showimage;
$_SESSION['surfclickimage'] = $image_dir."/".$mainimage;

$imagesize = @getimagesize($image_dir."/".$showimage);
$imagewidth = $imagesize[0];

$_SESSION['mincorrect'] = $imagewidth*$toclick-$imagewidth;
$_SESSION['maxcorrect'] = $imagewidth*$toclick;

$barcode = md5(rand(100,10000));
$_SESSION['surfbarkey'] = $barcode;

$imgdisp = "<img onclick=\"return surfbar_click(this, event);\" src=\"surfclickimage.php?tval=".time()."\">";


// Surfbar Footer
$checkwsenabled = mysql_query("Select value from ".$prefix."wssettings where field='enabled' limit 1");
$wsenabled = mysql_result($checkwsenabled, 0, "value");
if ($wsenabled == 1) {

	$getletterhuntinstr = mysql_query("Select value from ".$prefix."wssettings where field='instructions' limit 1");
	$letterhuntinstr = mysql_result($getletterhuntinstr, 0, "value");

	$checkclaimpage = mysql_query("Select value from ".$prefix."wssettings where field='claimpage' limit 1");
	$claimpage = mysql_result($checkclaimpage, 0, "value");

	if (($_SESSION["checkletter"] == 1) && ($claimpage == 0)) {
		//Check for a letter
		$letterhunttext = checkforletter($userid);
	} else {
		//User made a wrong click, or the claim page is enabled
		$letterhunttext = displayletters($userid);
	}

} else {
	$letterhuntinstr = "";
	$letterhunttext = "Letter Hunt Disabled...";
}
$getclicks = mysql_query("Select clickstoday from `".$prefix."members` where Id=$userid limit 1");
$clickstoday = mysql_result($getclicks, 0, "clickstoday");

$t = time();
$getboost = mysql_query("SELECT surfimage, surftext FROM ".$prefix."cboost WHERE `starttime`<$t AND `endtime`>$t AND surfboost>1 AND surfimage!='' AND (acctype=$acctype OR acctype=0) LIMIT 1;");
if (mysql_num_rows($getboost) > 0) {
	$boostimage = mysql_result($getboost, 0, "surfimage");
	$boosttext = mysql_result($getboost, 0, "surftext");
	$bonushtml = "<img onclick=\"javascript: alert('".$boosttext."');\" src=\"".$boostimage."\" border=\"0\">";
} else {
	$bonushtml = "&nbsp";
}
// End Surfbar Footer


echo("#STARTRESULT#".$_SESSION['sysmess']."#ENDRESULT#");

echo("#STARTICONS#<font size=2>Click</font><img src=\"surfimage.php?tval=".time()."\">&nbsp;&nbsp;
  <div style=\"border: 1px #000 solid; display: inline; padding: 3px; \">".$imgdisp."</div>#ENDICONS#");

echo("#STARTBANNER#".showbanner($userid, $defbanimg, $defbantar)."#ENDBANNER#");

echo("#STARTTEXT#".showtext($userid, 50, $deftextad, $deftexttar, $textImpColor)."&nbsp;#ENDTEXT#");


echo("
#FRAME1#".$_SESSION['frame1site']."#ENDFRAME1#
#FRAME2#".$_SESSION['frame2site']."#ENDFRAME2#

#SWITCHFRAME1#".$_SESSION['switchframe1']."#ENDSWITCHFRAME1#
#SWITCHFRAME2#".$_SESSION['switchframe2']."#ENDSWITCHFRAME2#

#SHOWINGFRAME#".$_SESSION['showingframe']."#ENDSHOWINGFRAME#

#SITEURL#".$_SESSION['currentsite']."#ENDSITEURL#

#FRAMESITE#".$framethissite."#ENDFRAMESITE#

#STARTSURFED#<font size=\"2\" color=\"".$bottomfontColor."\">Surfed Today: <b>".$clickstoday."</b></font><br />".$bonushtml."#ENDSURFED#
#STARTLHUNT#<font size=\"2\" color=\"".$bottomfontColor."\">".$letterhunttext."</font>#ENDLHUNT#
");

// Run Ajax Out Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_ajaxout` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Ajax Out Mods

exit;

?>