<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();

if(!isset($_SESSION["uid"])) {
	echo("#STARTRESULT#Session Expired#ENDRESULT#");
	exit;
}

include "inc/userauth.php";
$userid = $_SESSION["userid"];

include "surfbarColors.php";

require_once "lhuntfunctions.php"; //Word Search Game
require_once "inc/bandj/functions.php";

$getacctype = mysql_query("Select email, status, credits, mtype, lastclick, clickstoday from `".$prefix."members` where Id=$userid limit 1");
$useremail = mysql_result($getacctype, 0, "email");
$usercredits = mysql_result($getacctype, 0, "credits");
$acctype = mysql_result($getacctype, 0, "mtype");
$accstatus = mysql_result($getacctype, 0, "status");
$lastclicktime = mysql_result($getacctype, 0, "lastclick");
//$clickstoday = mysql_result($getacctype, 0, "clickstoday");
$clickstoday = bandjSurfedToday($prefix, $userid);

if ($accstatus == "Suspended") {
header("Location: members.php?suspended=yes");
exit;
}

require_once "sfunctions.php"; //Main functions
require_once "smfunctions.php"; //Member functions

require_once "surf_geturl.php"; //Get URL functions

$gettimer = mysql_query("Select surftimer from `".$prefix."membertypes` where mtid=$acctype limit 1");
$timer = mysql_result($gettimer, 0, "surftimer");

$preloadsites = $_SESSION['preload_sess'];


// Get The Images
include("surfimages.php");

$sysmess = $_SESSION['sysmess'];

$usercredits = round($usercredits, 2);

//Get Default URLs
$getdefaults = mysql_query("Select defbanimg, defbantar, deftextad, deftexttar from `".$prefix."settings` limit 1");
$defbanimg = mysql_result($getdefaults, 0, "defbanimg");
$defbantar = mysql_result($getdefaults, 0, "defbantar");
$deftextad = mysql_result($getdefaults, 0, "deftextad");
$deftexttar = mysql_result($getdefaults, 0, "deftexttar");


// Get frames content
if (!isset($_SESSION['frame1site']) || $_SESSION['frame1site'] == "") {

	// Restart Surfing Session
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['frame2site'] = "about:blank";
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	
	if ($preloadsites != 1) {
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 0;
		$_SESSION['showingframe'] = 0;
	} else {
		$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 1;
		$_SESSION['showingframe'] = 1;
	}

} elseif ($preloadsites != 1) {

	$_SESSION['showingframe'] = 1;
	$_SESSION['switchframe1'] = 0;
	$_SESSION['switchframe2'] = 0;
	$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
	$_SESSION['currentsite'] = $_SESSION['frame1site'];
	if (is_numeric($_SESSION["newsiteid"])) {
		@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=".$_SESSION["newsiteid"]." where Id=$userid limit 1");
	} else {
		@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=0 where Id=$userid limit 1");
	}
	
} else {
	// Switch Frames

	if ($_SESSION['showingframe'] == 1) {
		// Display frame 2 and switch frame 1
		$_SESSION['showingframe'] = 2;
		$_SESSION['switchframe1'] = 1;
		$_SESSION['switchframe2'] = 0;
		if (is_numeric($_SESSION["newsiteid"])) {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=".$_SESSION["newsiteid"]." where Id=$userid limit 1");
		} else {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=0 where Id=$userid limit 1");
		}
		$_SESSION['frame1site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
		$_SESSION['currentsite'] = $_SESSION['frame2site'];
	} else {
		// Display frame 1 and switch frame 2
		$_SESSION['showingframe'] = 1;
		$_SESSION['switchframe1'] = 0;
		$_SESSION['switchframe2'] = 1;
		if (is_numeric($_SESSION["newsiteid"])) {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=".$_SESSION["newsiteid"]." where Id=$userid limit 1");
		} else {
			@mysql_query("Update `".$prefix."members` set lastview=currentview, currentview=0 where Id=$userid limit 1");
		}
		$_SESSION['frame2site'] = getnewurl($acctype, $accstatus, $lastclicktime, $clickstoday, $timer);
		$_SESSION['currentsite'] = $_SESSION['frame1site'];
	}
}


// Check the last click

checkautoassign($userid, $acctype);

if (!isset($_GET['xval']) || !is_numeric($_GET['xval'])) {
	$_SESSION['sysmess'] = "<span style=\"color: blue;\"><b>Starting surf session</b></span>";
	$_SESSION["refreshcount"] = $_SESSION["refreshcount"]+1;
	$_SESSION["siteviewed"] = 0;
	$_SESSION["clickcount"] = 0;
	$_SESSION["checkletter"] = 0;
	
} elseif ($lastclicktime > (time()-($timer-1))) {
	//Multiple browsers detected
	@mysql_query("Update `".$prefix."members` set lastclick=0 where Id=$userid limit 1");
	$_SESSION["uid"] = "";
	$_SESSION["user_password"] = "";
	@session_destroy();
	echo("#STARTRESULT#Session Expired#ENDRESULT#");
	exit;
	
} elseif ($_GET['xval'] != $_SESSION['toclick'] || $_GET['toclickkey'] != $_SESSION['toclickkey']) {
	$_SESSION["refreshcount"] = 0;
	$_SESSION["checkletter"] = 0;
	wrongclick($userid);
	
} elseif ($_GET['xval'] == $_SESSION['toclick'] && $_GET['toclickkey'] == $_SESSION['toclickkey']) {
	$_SESSION["refreshcount"] = 0;
	$_SESSION["clickcount"] = $_SESSION["clickcount"] + 1;
	$_SESSION["checkletter"] = 1;
	creditmember($userid, $acctype,$creditColor);
	
} else {
	$_SESSION['sysmess'] = "<span style=\"color: blue;\"><b>Starting surf session</b></span>";
	$_SESSION["refreshcount"] = $_SESSION["refreshcount"]+1;
	$_SESSION["siteviewed"] = 0;
	$_SESSION["clickcount"] = 0;
	$_SESSION["checkletter"] = 0;
}

// Create The Buttons

$theimagekey = array_rand($images);
$_SESSION['toclick'] = $toclick = rand(1,4);

$_SESSION['toclickkey'] = md5(rand(1, 999999));

$toclickkeys[1] = md5(rand(1, 999999));
$toclickkeys[2] = md5(rand(1, 999999));
$toclickkeys[3] = md5(rand(1, 999999));
$toclickkeys[4] = md5(rand(1, 999999));

$toclickkeys[$toclick] = $_SESSION['toclickkey'];

$theimage = $images[$theimagekey];
$imagedata = explode("|", $theimage);

$mainimage = $imagedata[0];
$showimage = $imagedata[$toclick];

$imagesize = @getimagesize($image_dir."/".$showimage);
$imagewidth = $imagesize[0];

$_SESSION['surfimage'] = $image_dir."/".$showimage;

$_SESSION['surfclickimage1'] = $image_dir."/cache/".$mainimage."_1";
$_SESSION['surfclickimage2'] = $image_dir."/cache/".$mainimage."_2";
$_SESSION['surfclickimage3'] = $image_dir."/cache/".$mainimage."_3";
$_SESSION['surfclickimage4'] = $image_dir."/cache/".$mainimage."_4";

if (!file_exists($_SESSION['surfclickimage1']) || !file_exists($_SESSION['surfclickimage2']) || !file_exists($_SESSION['surfclickimage3']) || !file_exists($_SESSION['surfclickimage4'])) {
	
	if (!is_dir($image_dir."/cache")) {
		@mkdir($image_dir."/cache");
	}
	
	$clickimageinfo = @getimagesize($image_dir."/".$mainimage);
	
	if ($clickimageinfo[2] == IMAGETYPE_PNG) {
		//Convert PNG To JPG
		$clickimagefile = imagecreatefrompng($image_dir."/".$mainimage);
	} elseif ($clickimageinfo[2] == IMAGETYPE_GIF) {
		//Convert GIF To JPG
		$tempgif = imagecreatefromgif($image_dir."/".$mainimage);
		$clickimagefile = imagecreatetruecolor($imagewidth*4, $imagewidth);
		imagecopy($clickimagefile, $tempgif, 0, 0, 0, 0, $imagewidth, $imagewidth);
	} else {
		$clickimagefile = imagecreatefromjpeg($image_dir."/".$mainimage);
	}
	
	$lcsurfclickimage1 = imagecreatetruecolor($imagewidth, $imagewidth);
	$lcsurfclickimage2 = imagecreatetruecolor($imagewidth, $imagewidth);
	$lcsurfclickimage3 = imagecreatetruecolor($imagewidth, $imagewidth);
	$lcsurfclickimage4 = imagecreatetruecolor($imagewidth, $imagewidth);
	
	imagecopy($lcsurfclickimage1, $clickimagefile, 0, 0, 0, 0, $imagewidth, $imagewidth);
	imagecopy($lcsurfclickimage2, $clickimagefile, 0, 0, $imagewidth, 0, $imagewidth, $imagewidth);
	imagecopy($lcsurfclickimage3, $clickimagefile, 0, 0, $imagewidth*2, 0, $imagewidth, $imagewidth);
	imagecopy($lcsurfclickimage4, $clickimagefile, 0, 0, $imagewidth*3, 0, $imagewidth, $imagewidth);
	
	if (is_dir($image_dir."/cache")) {
		@imagejpeg($lcsurfclickimage1, $_SESSION['surfclickimage1'], 90);
		@imagejpeg($lcsurfclickimage2, $_SESSION['surfclickimage2'], 90);
		@imagejpeg($lcsurfclickimage3, $_SESSION['surfclickimage3'], 90);
		@imagejpeg($lcsurfclickimage4, $_SESSION['surfclickimage4'], 90);
	}
	
	if (!file_exists($_SESSION['surfclickimage1']) || !file_exists($_SESSION['surfclickimage2']) || !file_exists($_SESSION['surfclickimage3']) || !file_exists($_SESSION['surfclickimage4'])) {
		// Cache image creation failed - Store the image data in the session
		ob_start();
		imagejpeg($lcsurfclickimage1);
		$_SESSION['surfclickimage1data'] = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		imagejpeg($lcsurfclickimage2);
		$_SESSION['surfclickimage2data'] = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		imagejpeg($lcsurfclickimage3);
		$_SESSION['surfclickimage3data'] = ob_get_contents();
		ob_end_clean();
		
		ob_start();
		imagejpeg($lcsurfclickimage4);
		$_SESSION['surfclickimage4data'] = ob_get_contents();
		ob_end_clean();
	}
	
}

echo("#STARTRESULT#".$_SESSION['sysmess']."#ENDRESULT#");

echo("#STARTICONS#<img src=\"surficon.php?ts=".time()."\">&nbsp;
  <table style=\"border-top: none; border-left: 1px #000 solid; border-right: 1px #000 solid; border-bottom: 1px #000 solid; display: inline;\" cellpadding=\"0\" cellspacing=\"0\"><tr><td><img onclick=\"return surfbar_click(1, '".$toclickkeys[1]."');\" src=\"surfclickicon.php?iconid=1&ts=".time()."\"><img onclick=\"return surfbar_click(2, '".$toclickkeys[2]."');\" src=\"surfclickicon.php?iconid=2&ts=".time()."\"><img onclick=\"return surfbar_click(3, '".$toclickkeys[3]."');\" src=\"surfclickicon.php?iconid=3&ts=".time()."\"><img onclick=\"return surfbar_click(4, '".$toclickkeys[4]."');\" src=\"surfclickicon.php?iconid=4&ts=".time()."\"></td></tr></table>#ENDICONS#");

$bannertextenabled = mysql_result(mysql_query("SELECT COUNT(*) FROM `".$prefix."barmods_topbar` WHERE state='1' AND filename='bannertext.php'"), 0);
if ($bannertextenabled > 0) {
	echo("#STARTBANNER#".__showbanner($userid, $defbanimg, $defbantar)."#ENDBANNER#");
	echo("#STARTTEXT#".__showtext($userid, 50, $deftextad, $deftexttar, $textImpColor)."&nbsp;#ENDTEXT#");
}

echo("
#FRAME1#".$_SESSION['frame1site']."#ENDFRAME1#
#FRAME2#".$_SESSION['frame2site']."#ENDFRAME2#

#SWITCHFRAME1#".$_SESSION['switchframe1']."#ENDSWITCHFRAME1#
#SWITCHFRAME2#".$_SESSION['switchframe2']."#ENDSWITCHFRAME2#

#SHOWINGFRAME#".$_SESSION['showingframe']."#ENDSHOWINGFRAME#

#SITEURL#".$_SESSION['currentsite']."#ENDSITEURL#

");

// Run Ajax Out Mods
$getmods = mysql_query("Select filename from `".$prefix."barmods_ajaxout` where enabled=1");
if (mysql_num_rows($getmods) > 0) {
	while ($modlist = mysql_fetch_array($getmods)) {
		$modfilename = trim($modlist['filename']);
		if (file_exists($modfilename)) {
			include($modfilename);
		}
	}
}
// End Run Ajax Out Mods

// Update HTML Modules
$getmodules = mysql_query("SELECT id, html FROM `".$prefix."barmods_topbar` WHERE state='1' AND filename='None' ORDER BY rank ASC");
while ($modlist = mysql_fetch_array($getmodules)) {
	echo("#STARTMOD".$modlist['id']."#");
	$modlist['html'] = translate_site_tags($modlist['html']);
	$modlist['html'] = translate_user_tags($modlist['html'], $useremail);
	echo($modlist['html']);
	echo("#ENDMOD".$modlist['id']."#");
}
// End Update HTML Modules

// Update HTML Extensions
$getmodules = mysql_query("SELECT id, html FROM `".$prefix."barmods_extensions` WHERE state='1' AND filename='None' ORDER BY rank ASC");
while ($modlist = mysql_fetch_array($getmodules)) {
	echo("#STARTEXT".$modlist['id']."#");
	$modlist['html'] = translate_site_tags($modlist['html']);
	$modlist['html'] = translate_user_tags($modlist['html'], $useremail);
	echo($modlist['html']);
	echo("#ENDEXT".$modlist['id']."#");
}
// End Update HTML Extensions


exit;

?>