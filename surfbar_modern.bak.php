<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.07
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// Prevent Invalid Access
if (!isset($userid) || $userid != $_SESSION["userid"]) {
	header("Location: members.php?mf=lo");
	exit;
}


$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

// Get The Timer
$gettimer = mysql_query("Select surftimer from `".$prefix."membertypes` where mtid=$acctype limit 1");
$timer = mysql_result($gettimer, 0, "surftimer");

// Get The Images
include("surfimages.php");

$sysmess = $_SESSION['sysmess'];

// Create The Buttons

$theimagekey = array_rand($images);
$toclick = rand(1,4);

$theimage = $images[$theimagekey];
$imagedata = explode("|", $theimage);

$mainimage = $imagedata[0];
$showimage = $imagedata[$toclick];

$_SESSION['surfimage'] = $image_dir."/".$showimage;
$_SESSION['surfclickimage'] = $image_dir."/".$mainimage;

$imagesize = @getimagesize($image_dir."/".$showimage);
$imagewidth = $imagesize[0];

$_SESSION['mincorrect'] = $imagewidth*$toclick-$imagewidth;
$_SESSION['maxcorrect'] = $imagewidth*$toclick;

$barcode = md5(rand(100,10000));
$_SESSION['surfbarkey'] = $barcode;

$imgdisp = "<img onclick=\"return surfbar_click(this, event);\" src=\"surfclickimage.php?tval=".time()."\">";

$usercredits = round($usercredits, 2);

//Get Default URLs
$getdefaults = mysql_query("Select defbanimg, defbantar, deftextad, deftexttar from `".$prefix."settings` limit 1");
$defbanimg = mysql_result($getdefaults, 0, "defbanimg");
$defbantar = mysql_result($getdefaults, 0, "defbantar");
$deftextad = mysql_result($getdefaults, 0, "deftextad");
$deftexttar = mysql_result($getdefaults, 0, "deftexttar");

$baroutput = "
<style type=\"text/css\">
<!--
.surfbar {
	color: $fontColor;
}
-->
</style>

<body>

<table class=\"surfbar\" bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0 width=".$modern_surfbarwidth.">

<tr><td align=left valign=top width=332>

<div id=\"myform\" style=\"visibility: hidden;\">

  <font size=2>Click</font><img src=\"surfimage.php?tval=".time()."\">&nbsp;&nbsp;
  <div style=\"border: 1px #000 solid; display: inline; padding: 3px; \">".$imgdisp."</div>

</div>

</td><td align=right valign=top width=468>

<div id=\"bannerdiv\">".showbanner($userid, $defbanimg, $defbantar)."</div>

</td></tr>
</table>

<hr>

<table class=\"surfbar\" bgcolor=\"$bgcolor\" border=0 cellpadding=0 cellspacing=0 width=".$modern_surfbarwidth.">
<tr><td align=center valign=bottom width=50>

<div id=\"timer\" style=\"font-size: 12pt; margin: 0px; \"></div>

</td><td width=165 valign=bottom>

<div id=\"clickresultdiv\" style=\"margin: 0px;\">".$sysmess."</div>

</td><td width=330 valign=bottom>

<div id=\"surfbarmenu\" style=\"margin: 0px;\"><a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"members.php\" onclick=\"userClicked();\">Home</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" href=\"".$_SESSION['currentsite']."\">Open Site</a> | <a target=_blank style=\"text-decoration:none;color:".$menuItems."\" href=\"reportsite.php\">Report Site</a> | <a target=_top style=\"text-decoration:none;color:".$menuItems."\" href=\"surfbar_settings.php\" onclick=\"userClicked();\">Surf Settings</a></div>

</td><td align=right width=255 valign=bottom>

<div id=\"textdiv\" style=\"margin: 0px;\">".showtext($userid, 50, $deftextad, $deftexttar, $textImpColor)."&nbsp;</div>

</td></tr>
</table>

";

$baroutput = preg_replace("/((\r(?!\n))|((?<!\r)\n)|(\r\n))/", "", $baroutput);

?>