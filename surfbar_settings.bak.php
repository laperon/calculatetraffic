<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.16
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$_SESSION['startpage'] = 0;

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";

include("surf_prefs.php");
$adminprefs = get_admin_prefs();
$userprefs = get_user_prefs($_SESSION[userid]);

$optionsavail = 0;

$page_content = '<html>
<body>
<center>';

if ($adminprefs['surftheme'] > 0) {

	// Simple Surfbar Options

	if($_GET["update"] == "go") {
	
	  	if ((!isset($_POST['surficontype'])) || (!is_numeric($_POST['surficontype'])) || ($_POST['surficontype'] < 1) || ($_POST['surficontype'] > 2)) {
			$_POST['surficontype'] = 2;
		}

		if ($_POST['surftype'] == 1) {
		
			$_POST['surficontype'] = 2;
			$_POST['surfbarstyle'] = 2;
			$_POST['surfbar_pos'] = 1;
			$_POST['footer_pos'] = 3;
			$_POST['footer_transparent'] = 1;
			$_POST['footertype'] = 2;
			$_POST['framesites'] = -1;
			
		} elseif ($_POST['surftype'] == 2) {
		
			$_POST['surfbarstyle'] = 2;
			$_POST['surfbar_pos'] = 2;
			$_POST['footer_pos'] = 3;
			$_POST['footer_transparent'] = 0;
			$_POST['footertype'] = 1;
			$_POST['framesites'] = 1;
			
		} else {
		
			$_POST['surficontype'] = 1;
			$_POST['surfbarstyle'] = 1;
			$_POST['surfbar_pos'] = 2;
			$_POST['footer_pos'] = 3;
			$_POST['footer_transparent'] = 0;
			$_POST['footertype'] = 1;
			$_POST['framesites'] = 1;
		}
		
		if ($_POST['surfbar_pos'] == 1) {
			$surfbardrop = 1;
			$surfbartop = 1;
		} elseif ($_POST['surfbar_pos'] == 2) {
			$surfbardrop = 0;
			$surfbartop = 1;
		} else {
			$surfbardrop = 0;
			$surfbartop = 0;
		}
		
		// Update settings
		@mysql_query("Delete from ".$prefix."surf_user_prefs where userid=".$_SESSION[userid]);
		
	  	@mysql_query("Insert into ".$prefix."surf_user_prefs (field, userid, value) VALUES
	  	('surficontype', ".$_SESSION[userid].", '".$_POST['surficontype']."'),
		('surfbarstyle', ".$_SESSION[userid].", '".$_POST['surfbarstyle']."'),
		('preloadsites', ".$_SESSION[userid].", '1'),
		('surfbardrop', ".$_SESSION[userid].", '".$surfbardrop."'),
		('surfbartop', ".$_SESSION[userid].", '".$surfbartop."'),
		('framesites', ".$_SESSION[userid].", '".$_POST['framesites']."'),
		('footer_pos', ".$_SESSION[userid].", '".$_POST['footer_pos']."'),
		('can_move_footer', ".$_SESSION[userid].", '1'),
		('footertype', ".$_SESSION[userid].", '".$_POST['footertype']."'),
		('footer_transparent', ".$_SESSION[userid].", '".$_POST['footer_transparent']."');");
		
		$adminprefs = get_admin_prefs();
		$userprefs = get_user_prefs($_SESSION[userid]);
	
	}
	
	
	if ($adminprefs['choosetheme'] != 0) {
		$page_content .= '
		<p><font size="3"><b>Surfbar Types</b></font></p>
		<table width="400" border="1" bordercolor="black" align="center" cellpadding="4" cellspacing="0">
		<tr><td align="center" bgcolor="#EEEEEE">
		<p align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Dropdown</b>: An advanced surfbar that drops from the top of the screen when the timer counts down, maximizing the amount of space available for a member\'s site.  This surfbar can preload the next site in rotation while the timer is still counting down, giving members an extremely fast surfing experience.  Also features a movable and transparent surfbar footer.</font></p>
		</td></tr>
		<tr><td align="center" bgcolor="#DDDDDD">
		<p align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Basic</b>: Features the fast speed and preloading capabilities of the modern surfbar, but with a classic traffic exchange surfbar layout.  Compatible with both Ajax and Post icon types.</font></p>
		</td></tr>
		<tr><td align="center" bgcolor="#EEEEEE">
		<p align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Classic</b>: The original LFMTE surfbar.  This surfbar is slower, and is only recommended for members who are unable to use the modern surfbar due to an older browser or other technical limitations.</font></p>
		</td></tr>
		</table>
		<p>&nbsp;</p>
		';
	}

	$page_content.='<form name="updfrm" method="post" action="surfbar_settings.php?update=go">
	<table width="250" border="0" align="center" cellpadding="4" cellspacing="0">
	<tr>
	<td colspan="2" align="center"><font size="3"><b>Surfbar Settings</b></font></td>
	</tr>
	<tr>
	<td align="left" nowrap="NOWRAP">&nbsp;</td>
	<td align="left">&nbsp;</td>
	</tr>';
		  
	if ($adminprefs['choosetheme'] != 0) {
	      $page_content.='
	      <tr>
	        <td align="left" nowrap="NOWRAP">Surfbar Type</td>
	        <td align="left">
	        
	        <select name="surftype">
	        <option value="1"'; if($userprefs["surfbarstyle"] == 2 && $userprefs["surfbardrop"] == 1){$page_content.=' selected';}  $page_content.='>Dropdown</option>
	        <option value="2"'; if($userprefs["surfbarstyle"] == 2 && $userprefs["surfbardrop"] == 0){$page_content.=' selected';}  $page_content.='>Basic</option>
	        <option value="3"'; if($userprefs["surfbarstyle"] == 1){$page_content.=' selected';}  $page_content.='>Classic</option>
	        </select>
	        
	        </td>
	      </tr>
	      ';
	      $optionsavail = 1;
	}
	
	if ($userprefs["surfbarstyle"] == 2 && $userprefs["surfbardrop"] == 0 && $adminprefs["surficontype"] == -1) {
	      $page_content.='
	      <tr>
	        <td align="left" nowrap="NOWRAP">Surf Icon Type</td>
	        <td align="left">
	        
	        <select name="surficontype">
	        <option value="2"'; if($userprefs["surficontype"] == 2){$page_content.=' selected';}  $page_content.='>Ajax</option>
	        <option value="1"'; if($userprefs["surficontype"] == 1){$page_content.=' selected';}  $page_content.='>Post</option>
	        </select>
	        
	        </td>
	      </tr>
	      <tr><td align="left" colspan="2"><p><font size="1"><b>Note:</b> If your clicks are being registered as incorrect, try changing the Surf Icon Type from Ajax to Post, or vice-versa.</font></p></td></tr>
	      ';
	      $optionsavail = 1;
	}

	if ($optionsavail == 1) {
	  $page_content.='
	 <tr>
	 <td colspan="2" align="center">
	  <input type="submit" name="Submit" value="Update" /></td>
	   </tr>';
	} else {
	  $page_content.='
	 <tr>
	 <td colspan="2" align="center">
	 <p><font size="2"><b>There are no customizable surfbar options.</b></font></p>
	 </td>
	   </tr>';
	}
	  
	$page_content.='
	</table>
	</form>';

} else {

		  // Advanced Surfbar Options

		  $page_content.='<form name="updfrm" method="post" action="surfbar_settings.php?update=go">
			   <table width="250" border="0" align="center" cellpadding="4" cellspacing="0">
				 <tr>
				   <td colspan="2" align="center"><font size="3"><b>Surfbar Settings</b></font></td>
		   </tr>
		  <tr>
		  <td align="left" nowrap="NOWRAP">&nbsp;</td>
		  <td align="left">&nbsp;</td>
		  </tr>';
		  
		  if($_GET["update"] == "go") {
		  
		  	if ((!isset($_POST['surficontype'])) || (!is_numeric($_POST['surficontype'])) || ($_POST['surficontype'] < 1) || ($_POST['surficontype'] > 2)) {
				$_POST['surficontype'] = 2;
			}
		  
		  	if ((!isset($_POST['surfbarstyle'])) || (!is_numeric($_POST['surfbarstyle'])) || ($_POST['surfbarstyle'] < 1) || ($_POST['surfbarstyle'] > 2)) {
				$_POST['surfbarstyle'] = 2;
			}
		
			if ((!isset($_POST['preloadsites'])) || (!is_numeric($_POST['preloadsites'])) || ($_POST['preloadsites'] < 0) || ($_POST['preloadsites'] > 1)) {
				$_POST['preloadsites'] = 1;
			}
			
			if ((!isset($_POST['surfbar_pos'])) || (!is_numeric($_POST['surfbar_pos'])) || ($_POST['surfbar_pos'] < 1) || ($_POST['surfbar_pos'] > 3)) {
				$_POST['surfbar_pos'] = 1;
			}
			
			if ((!isset($_POST['footer_pos'])) || (!is_numeric($_POST['footer_pos'])) || ($_POST['footer_pos'] < 1) || ($_POST['footer_pos'] > 4)) {
				$_POST['footer_pos'] = 4;
			}
			
			if ((!isset($_POST['can_move_footer'])) || (!is_numeric($_POST['can_move_footer'])) || ($_POST['can_move_footer'] < 0) || ($_POST['can_move_footer'] > 1)) {
				$_POST['can_move_footer'] = 1;
			}
		
			if ((!isset($_POST['footer_transparent'])) || (!is_numeric($_POST['footer_transparent'])) || ($_POST['footer_transparent'] < 0) || ($_POST['footer_transparent'] > 1)) {
				$_POST['footer_transparent'] = 1;
			}
			
			if ((!isset($_POST['footertype'])) || (!is_numeric($_POST['footertype'])) || ($_POST['footertype'] < 0) || ($_POST['footertype'] > 2)) {
				$_POST['footertype'] = 2;
			}
			
			
			if ($_POST['surfbar_pos'] == 1) {
				$surfbardrop = 1;
				$surfbartop = 1;
			} elseif ($_POST['surfbar_pos'] == 2) {
				$surfbardrop = 0;
				$surfbartop = 1;
			} else {
				$surfbardrop = 0;
				$surfbartop = 0;
			}
			
			// Update settings
		  	@mysql_query("Delete from ".$prefix."surf_user_prefs where userid=".$_SESSION[userid]);
		  	
  			@mysql_query("Insert into ".$prefix."surf_user_prefs (field, userid, value) VALUES
  			('surficontype', ".$_SESSION[userid].", '".$_POST['surficontype']."'),
			('surfbarstyle', ".$_SESSION[userid].", '".$_POST['surfbarstyle']."'),
			('preloadsites', ".$_SESSION[userid].", '".$_POST['preloadsites']."'),
			('surfbardrop', ".$_SESSION[userid].", '".$surfbardrop."'),
			('surfbartop', ".$_SESSION[userid].", '".$surfbartop."'),
			('framesites', ".$_SESSION[userid].", '-1'),
			('footer_pos', ".$_SESSION[userid].", '".$_POST['footer_pos']."'),
			('can_move_footer', ".$_SESSION[userid].", '".$_POST['can_move_footer']."'),
			('footertype', ".$_SESSION[userid].", '".$_POST['footertype']."'),
			('footer_transparent', ".$_SESSION[userid].", '".$_POST['footer_transparent']."');");
			
			$adminprefs = get_admin_prefs();
			$userprefs = get_user_prefs($_SESSION[userid]);
		  
		  }
		  
		if ($adminprefs['surfbardrop'] == -1 && $adminprefs['surfbartop'] == -1) {
			$adminprefs["surfbar_pos"] = -1;
		}

		if ($userprefs['surfbardrop'] == 1 && $userprefs['surfbartop'] == 1) {
			$userprefs["surfbar_pos"] = 1;
		} elseif ($userprefs['surfbardrop'] == 0 && $userprefs['surfbartop'] == 1) {
			$userprefs["surfbar_pos"] = 2;
		} elseif ($userprefs['surfbardrop'] == 0 && $userprefs['surfbartop'] == 0) {
			$userprefs["surfbar_pos"] = 3;
		}
		
if ($adminprefs["surfbarstyle"] == -1) {
      $page_content.='
	<tr>
        <td align="left" nowrap="NOWRAP">Surfbar Type</td>
        <td align="left">
        
        <select name="surfbarstyle">
        <option value="2"'; if($userprefs["surfbarstyle"] == 2){$page_content.=' selected';}  $page_content.='>Modern</option>
        <option value="1"'; if($userprefs["surfbarstyle"] == 1){$page_content.=' selected';}  $page_content.='>Classic</option>
        </select>
        
        </td>
      </tr>
      ';
      $optionsavail = 1;
}


if ($userprefs["surfbarstyle"] == 2) {

	if ($adminprefs["preloadsites"] == -1) {
	      $page_content.='
	      <tr>
	        <td align="left" nowrap="NOWRAP">Preload Sites</td>
	        <td align="left">
	        
	        <select name="preloadsites">
	        <option value="1"'; if($userprefs["preloadsites"] == 1){$page_content.=' selected';}  $page_content.='>Yes</option>
	        <option value="0"'; if($userprefs["preloadsites"] == 0){$page_content.=' selected';}  $page_content.='>No</option>
	        </select>
	        
	        </td>
	      </tr>
	      ';
	      $optionsavail = 1;
	}
	
	
	if ($adminprefs["surfbar_pos"] == -1) {
	      $page_content.='
	      <tr>
	        <td align="left" nowrap="NOWRAP">Surfbar Location</td>
	        <td align="left">
	        
	        <select name="surfbar_pos">
	        <option value="1"'; if($userprefs["surfbar_pos"] == 1){$page_content.=' selected';}  $page_content.='>Top - Dropdown</option>
	        <option value="2"'; if($userprefs["surfbar_pos"] == 2){$page_content.=' selected';}  $page_content.='>Top - Stationary</option>
	        <option value="3"'; if($userprefs["surfbar_pos"] == 3){$page_content.=' selected';}  $page_content.='>Bottom</option>
	        </select>
	        
	        </td>
	      </tr>
	      ';
	      $optionsavail = 1;
	}
	
	if ($adminprefs["surficontype"] == -1) {
	      $page_content.='
	      <tr>
	        <td align="left" nowrap="NOWRAP">Surf Icon Type</td>
	        <td align="left">
	        
	        <select name="surficontype">
	        <option value="2"'; if($userprefs["surficontype"] == 2){$page_content.=' selected';}  $page_content.='>Ajax</option>
	        <option value="1"'; if($userprefs["surficontype"] == 1){$page_content.=' selected';}  $page_content.='>Post</option>
	        </select>
	        
	        </td>
	      </tr>
	      <tr><td align="left" colspan="2"><p><font size="2"><b>Note:</b>If your clicks are being registered as incorrect, try changing the Surf Icon Type from Ajax to Post, or vice-versa.</font></p></td></tr>
	      ';
	      $optionsavail = 1;
	}
	
}


if ($adminprefs["footertype"] == -1) {
      $page_content.='
      <tr>
        <td align="left" nowrap="NOWRAP">Footer Type</td>
        <td align="left">
        
        <select name="footertype">
        <option value="2"'; if($userprefs["footertype"] == 2){$page_content.=' selected';}  $page_content.='>Modern</option>
        <option value="1"'; if($userprefs["footertype"] == 1){$page_content.=' selected';}  $page_content.='>Classic</option>
        </select>
        
        </td>
      </tr>
      ';
      $optionsavail = 1;
}

if ($userprefs["footertype"] == 2) {

	if ($adminprefs["footer_pos"] == -1) {
	      $page_content.='
	      <tr>
        	<td align="left" nowrap="NOWRAP">Surf Footer Location</td>
	        <td align="left">
        	
	        <select name="footer_pos">
        	<option value="1"'; if($userprefs["footer_pos"] == 1){$page_content.=' selected';}  $page_content.='>Top Left</option>
	        <option value="2"'; if($userprefs["footer_pos"] == 2){$page_content.=' selected';}  $page_content.='>Top Right</option>
        	<option value="3"'; if($userprefs["footer_pos"] == 3){$page_content.=' selected';}  $page_content.='>Bottom Left</option>
	        <option value="4"'; if($userprefs["footer_pos"] == 4){$page_content.=' selected';}  $page_content.='>Bottom Right</option>
        	</select>
	        
        	</td>
	      </tr>
	      ';
	      $optionsavail = 1;
	}

	if ($adminprefs["can_move_footer"] == -1) {
	      $page_content.='
	      <tr>
        	<td align="left" nowrap="NOWRAP">Drag-And-Move Footer</td>
	        <td align="left">
        	
	        <select name="can_move_footer">
        	<option value="1"'; if($userprefs["can_move_footer"] == 1){$page_content.=' selected';}  $page_content.='>Yes</option>
	        <option value="0"'; if($userprefs["can_move_footer"] == 0){$page_content.=' selected';}  $page_content.='>No</option>
        	</select>
        	
	        </td>
	      </tr>
	      ';
	      $optionsavail = 1;
	}
	
}

if ($adminprefs["footer_transparent"] == -1) {
      $page_content.='
      <tr>
       	<td align="left" nowrap="NOWRAP">Footer Transparent</td>
        <td align="left">
       	
        <select name="footer_transparent">
       	<option value="1"'; if($userprefs["footer_transparent"] == 1){$page_content.=' selected';}  $page_content.='>Yes</option>
        <option value="0"'; if($userprefs["footer_transparent"] == 0){$page_content.=' selected';}  $page_content.='>No</option>
       	</select>
       	
        </td>
      </tr>
      ';
      $optionsavail = 1;
}

		  // End Surfbar Options
		  
		  if ($optionsavail == 1) {
			  $page_content.='
			 <tr>
			 <td colspan="2" align="center">
			  <input type="submit" name="Submit" value="Update" /></td>
			   </tr>';
		  } else {
			  $page_content.='
			 <tr>
			 <td colspan="2" align="center">
			 <p><font size="2"><b>There are no customizable surfbar options.</b></font></p>
			 <p><font size="2"><b><a href="surfing.php">Continue Surfing</b></b></font></p>
			 </td>
			   </tr>';
		  }
		  
		   $page_content.='
		   </table>
		   </form>';
}

		$page_content.='
		</center>
		</body>
		</html>';

load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");
echo($page_content);
include $theme_dir."/footer.php";
exit;

?>