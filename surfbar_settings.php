<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";

session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$_SESSION['startpage'] = 0;

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");
include "inc/theme.php";

include("surf_prefs.php");
$adminprefs = get_admin_prefs();
$userprefs = get_user_prefs($_SESSION['userid']);

$optionsavail = 0;

$page_content = '<html>
<body>
<center>';

	  // Surfbar Options

	  $page_content.='<form name="updfrm" method="post" action="surfbar_settings.php?update=go">
		   <table width="250" border="0" align="center" cellpadding="4" cellspacing="0">
			 <tr>
			   <td colspan="2" align="center"><font size="3"><b>Surfbar Settings</b></font></td>
	   </tr>
	  <tr>
	  <td align="left" nowrap="NOWRAP">&nbsp;</td>
	  <td align="left">&nbsp;</td>
	  </tr>';
	  
if($_GET["update"] == "go") {
	
	if ((!isset($_POST['preloadsites'])) || (!is_numeric($_POST['preloadsites'])) || ($_POST['preloadsites'] < 0) || ($_POST['preloadsites'] > 1)) {
		$_POST['preloadsites'] = 1;
	}
	
	// Update settings
  	@mysql_query("Delete from ".$prefix."surf_user_prefs where userid=".$_SESSION['userid']);
  	
	@mysql_query("Insert into ".$prefix."surf_user_prefs (field, userid, value) VALUES
	('preloadsites', ".$_SESSION['userid'].", '".$_POST['preloadsites']."');");
	
	$adminprefs = get_admin_prefs();
	$userprefs = get_user_prefs($_SESSION['userid']);
}


if ($userprefs["surfbarstyle"] != 1 && $adminprefs["preloadsites"] == -1) {
      $page_content.='
      <tr>
        <td align="left" nowrap="NOWRAP"><font size="2"><b>Preload Sites</b></font></td>
        <td align="left">
        
        <select name="preloadsites">
        <option value="1"'; if($userprefs["preloadsites"] == 1){$page_content.=' selected';}  $page_content.='>Yes</option>
        <option value="0"'; if($userprefs["preloadsites"] == 0){$page_content.=' selected';}  $page_content.='>No</option>
        </select>
        
        </td>
      </tr>
      <tr>
        <td colspan="2" align="left">
        <font size="2">Preloading will load the next site in rotation while the timer is counting down, and provide a faster surfing experience.  However, you may hear audio and video start playing from the next site when this option is enabled.</font>
        </td>
      </tr>
      ';
      $optionsavail = 1;
}

// End Surfbar Options

if ($optionsavail == 1) {
 $page_content.='
<tr>
<td colspan="2" align="center">
 <input type="submit" name="Submit" value="Update" /></td>
  </tr>';
} else {
 $page_content.='
<tr>
<td colspan="2" align="center">
<p><font size="2"><b>There are no customizable surfbar options.</b></font></p>
<p><font size="2"><b><a href="surfing.php">Continue Surfing</b></b></font></p>
</td>
  </tr>';
}

 $page_content.='
 </table>
 </form>';

$page_content.='
</center>
</body>
</html>';

load_template ($theme_dir."/header.php");
load_template ($theme_dir."/mmenu.php");
echo($page_content);
include $theme_dir."/footer.php";
exit;

?>