<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright ©2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
require_once "inc/bandj/functions.php";
include "surfbarColors.php";

session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

require_once "lhuntfunctions.php"; //Word Search Game

$getsurfedtoday = mysql_query("Select clickstoday, mtype from ".$prefix."members where Id=$userid limit 1");
//$surfedtoday = mysql_result($getsurfedtoday, 0, "clickstoday");
$surfedtoday = bandjSurfedToday($prefix, $userid);
$acctype = mysql_result($getsurfedtoday, 0, "mtype");

$checkwsenabled = mysql_query("Select value from ".$prefix."wssettings where field='enabled' limit 1");
$wsenabled = mysql_result($checkwsenabled, 0, "value");

if ($wsenabled == 1) {

	$getletterhuntinstr = mysql_query("Select value from ".$prefix."wssettings where field='instructions' limit 1");
	$letterhuntinstr = mysql_result($getletterhuntinstr, 0, "value");

	$checkclaimpage = mysql_query("Select value from ".$prefix."wssettings where field='claimpage' limit 1");
	$claimpage = mysql_result($checkclaimpage, 0, "value");

	if (($_SESSION["checkletter"] == 1) && ($claimpage == 0)) {
		//Check for a letter
		$letterhunttext = checkforletter($userid);
	} else {
		//User made a wrong click, or the claim page is enabled
		$letterhunttext = displayletters($userid);
	}

} else {
	$letterhuntinstr = "";
	$letterhunttext = "";
}

$t = time();
$getboost = mysql_query("SELECT surfimage, surftext FROM ".$prefix."cboost WHERE `starttime`<$t AND `endtime`>$t AND surfboost>1 AND surfimage!='' AND (acctype=$acctype OR acctype=0) LIMIT 1;");

if (mysql_num_rows($getboost) > 0) {

	$boostimage = mysql_result($getboost, 0, "surfimage");
	$boosttext = mysql_result($getboost, 0, "surftext");

	$bonushtml = "<img onclick=\"javascript: alert('".$boosttext."');\" src=\"".$boostimage."\" border=\"0\">";

} else {
	$bonushtml = "&nbsp";
}
//Mobile version;
$surf_information = "<p>Surfed This Session: ".$_SESSION['clickcount']."</p><p>Surfed Today:".$surfedtoday."</p><span>Find all the letters by surfing and earn credits</span><p>Complete the word to win credits! ".$letterhunttext."</p>";


echo("<html>
<style type=\"text/css\">
<!--
.surfbar {
	color: $bottomfontColor;
}
-->
</style>

<body>
<table class=\"surfbar\" bgcolor=\"$bottombgcolor\" width=\"100%\" height=\"100%\"><tr>
<td valign=\"middle\" align=\"center\" width=\"15%\"><font size=\"2\">Surfed This Session: <b>".$_SESSION["clickcount"]."</b></font><br><font size=\"2\">Surfed Today: <b>".$surfedtoday."</b></font></td>
<td valign=\"middle\" align=\"right\" width=\"5%\">$bonushtml</td>
<td valign=\"middle\" align=\"center\" width=\"30%\"><div><font size=\"2\"><b>".$letterhuntinstr."</b></font></div></td>
<td valign=\"middle\" align=\"right\" width=\"25%\"><font size=\"2\">Complete the word to win credits!</font></td><td valign=\"middle\" align=\"center\" width=\"20%\"><font size=\"2\"><b>".$letterhunttext."</b></font></td>
</tr></table>

<script>
	var surf_information = \"$surf_information\";
</script>
</body>
</html>


");
exit;
?>
