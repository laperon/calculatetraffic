<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright ©2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
include "surfbarColors.php";

session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

require_once "lhuntfunctions.php"; //Word Search Game

$getsurfedtoday = mysql_query("Select clickstoday, mtype from ".$prefix."members where Id=$userid limit 1");
$surfedtoday = mysql_result($getsurfedtoday, 0, "clickstoday");
$acctype = mysql_result($getsurfedtoday, 0, "mtype");

$checkwsenabled = mysql_query("Select value from ".$prefix."wssettings where field='enabled' limit 1");
$wsenabled = mysql_result($checkwsenabled, 0, "value");

if ($wsenabled == 1) {

	$getletterhuntinstr = mysql_query("Select value from ".$prefix."wssettings where field='instructions' limit 1");
	$letterhuntinstr = mysql_result($getletterhuntinstr, 0, "value");

	$checkclaimpage = mysql_query("Select value from ".$prefix."wssettings where field='claimpage' limit 1");
	$claimpage = mysql_result($checkclaimpage, 0, "value");

	if (($_SESSION["checkletter"] == 1) && ($claimpage == 0)) {
		//Check for a letter
		$letterhunttext = checkforletter($userid);
	} else {
		//User made a wrong click, or the claim page is enabled
		$letterhunttext = displayletters($userid);
	}

} else {
	$letterhuntinstr = "";
	$letterhunttext = "";
}

$t = time();
$getboost = mysql_query("SELECT surfimage, surftext FROM ".$prefix."cboost WHERE `starttime`<$t AND `endtime`>$t AND surfboost>1 AND surfimage!='' AND (acctype=$acctype OR acctype=0) LIMIT 1;");

if (mysql_num_rows($getboost) > 0) {

	$boostimage = mysql_result($getboost, 0, "surfimage");
	$boosttext = mysql_result($getboost, 0, "surftext");

	$bonushtml = "<img onclick=\"javascript: alert('".$boosttext."');\" src=\"".$boostimage."\" border=\"0\">";

} else {
	$bonushtml = "&nbsp";
}

// Get Social Surfbar Footer Settings

$enable_digg = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_digg'"), 0);
$enable_stumbleupon = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_stumbleupon'"), 0);
$enable_facebook = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_facebook'"), 0);
$enable_twitter = @mysql_result(@mysql_query("SELECT value FROM `socialsurfbar_footer` WHERE field='enable_twitter'"), 0);

if ($enable_digg == 1) {
	$digg_js = "<script type=\"text/javascript\">(function() {var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];s.type = 'text/javascript';s.async = true;s.src = 'http://widgets.digg.com/buttons.js';s1.parentNode.insertBefore(s, s1);})();</script>";
	$digg_column = "<a class=\"DiggThisButton DiggCompact\" href=\"http://digg.com/submit?url=".urlencode($_SESSION['currentsite'])."\"></a>";
} else {
	$digg_js = "";
	$digg_column = "&nbsp;";
}

if ($enable_stumbleupon == 1) {
	$stumbleupon_column = "<script src=\"http://www.stumbleupon.com/hostedbadge.php?s=1&r=".$_SESSION['currentsite']."\"></script>";
} else {
	$stumbleupon_column = "&nbsp;";
}

if ($enable_facebook == 1) {
	$facebook_column = "<iframe src=\"http://www.facebook.com/plugins/like.php?href=".urlencode($_SESSION['currentsite'])."&amp;layout=button_count&amp;show_faces=true&amp;width=100&amp;action=like&amp;colorscheme=light&amp;height=21\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:100px; height:20px;\" allowTransparency=\"true\"></iframe>";
} else {
	$facebook_column = "&nbsp;";
}

if ($enable_twitter == 1) {
	$twitter_column = "<a href=\"http://twitter.com/share\" class=\"twitter-share-button\" data-url=\"".$_SESSION['currentsite']."\" data-count=\"horizontal\">Tweet</a><script type=\"text/javascript\" src=\"http://platform.twitter.com/widgets.js\"></script>";
} else {
	$twitter_column = "&nbsp;";
}

// End Get Social Surfbar Footer Settings

echo("<html>
<style type=\"text/css\">
<!--
.surfbar {
	color: $bottomfontColor;
}
-->
</style>

<body>
".$digg_js."
<table class=\"surfbar\" bgcolor=\"$bottombgcolor\" width=\"100%\" height=\"100%\" cellspacing=0 cellpadding=0><tr>

<!--Start Social Surfbar Footer -->
<td width=\"220\">
<table border=\"0\" cellpadding=\"0\" cellspacing=\"2\">
<tr>
  <td valign=\"middle\" align=\"center\">".$stumbleupon_column."</td>
  <td valign=\"middle\" align=\"center\">".$facebook_column."</td>
</tr>
<tr>
  <td valign=\"middle\" align=\"center\">".$digg_column."</td>
  <td valign=\"middle\" align=\"center\">".$twitter_column."</td>
</tr>
</table>
</td>
<!--End Social Surfbar Footer-->

<td valign=\"middle\" align=\"center\" width=\"150\"><font size=\"2\">Surfed This Session: <b>".$_SESSION["clickcount"]."</b></font><br><font size=\"2\">Surfed Today: <b>".$surfedtoday."</b></font></td>
<td valign=\"middle\" align=\"right\" >$bonushtml</td>
<td valign=\"middle\" align=\"center\" ><font size=\"2\"><b>".$letterhuntinstr."</b></font></div></td>
<td valign=\"middle\" align=\"center\" ><font size=\"2\"><b>".$letterhunttext."</b></font></td>
</tr></table>
</body>
</html>");

exit;

?>