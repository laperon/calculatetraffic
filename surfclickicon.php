<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";

if (!isset($_GET['iconid']) || !is_numeric($_GET['iconid'])) {
	echo("Invalid Icon ID");
	exit;
}


if ($_GET['iconid'] == 1) {
	
	if (isset($_SESSION['surfclickimage1']) && file_exists($_SESSION['surfclickimage1'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		@readfile($_SESSION['surfclickimage1']);
		exit;
	} elseif (isset($_SESSION['surfclickimage1data'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		echo($_SESSION['surfclickimage1data']);
		exit;
	} else {
		echo("Session Image Error");
		exit;
	}
	
} elseif ($_GET['iconid'] == 2) {
	
	if (isset($_SESSION['surfclickimage2']) && file_exists($_SESSION['surfclickimage2'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		@readfile($_SESSION['surfclickimage2']);
		exit;
	} elseif (isset($_SESSION['surfclickimage2data'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		echo($_SESSION['surfclickimage2data']);
		exit;
	} else {
		echo("Session Image Error");
		exit;
	}
	
} elseif ($_GET['iconid'] == 3) {
	
	if (isset($_SESSION['surfclickimage3']) && file_exists($_SESSION['surfclickimage3'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		@readfile($_SESSION['surfclickimage3']);
		exit;
	} elseif (isset($_SESSION['surfclickimage3data'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		echo($_SESSION['surfclickimage3data']);
		exit;
	} else {
		echo("Session Image Error");
		exit;
	}
	
} elseif ($_GET['iconid'] == 4) {
	
	if (isset($_SESSION['surfclickimage4']) && file_exists($_SESSION['surfclickimage4'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		@readfile($_SESSION['surfclickimage4']);
		exit;
	} elseif (isset($_SESSION['surfclickimage4data'])) {
		header('Cache-Control: no-store, must-revalidate');
		header("Content-Type: image/jpeg");
		echo($_SESSION['surfclickimage4data']);
		exit;
	} else {
		echo("Session Image Error");
		exit;
	}
	
} else {
	echo("Invalid Icon ID");
	exit;
}


?>