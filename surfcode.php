<?php

// LFMTE Surf Promo Code Plugin v3
// �2013 LFM Wealth Systems
// http://thetrafficexchangescript.com

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$countcredits = mysql_query("select credits, mtype, clickstoday from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");
$clickstoday = mysql_result($countcredits, 0, "clickstoday");

$getuserdata = mysql_query("Select email from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = $acctype;
include "inc/theme.php";

$message = "";

if ($_GET['addcode'] == "go") {

	$surfcode = $_POST['surfcode'];
	$getcodeinfo = mysql_query("Select * from ".$prefix."promo_codes where enabled=1 and code='".$surfcode."'");
	if (mysql_num_rows($getcodeinfo) == 0) {
		$message = "Invalid Or Expired Code";
	} else {
		$codeid = mysql_result($getcodeinfo, 0, "id");
		$checkexisting = mysql_result(mysql_query("Select COUNT(*) from ".$prefix."promo_users where userid=$userid and codeid='".$codeid."'"), 0);
		if ($checkexisting > 0) {
			$message = "You already submitted this code.";
		} else {

            $date = date('Y-m-d H:i:s');
			mysql_query("Insert into ".$prefix."promo_users (date, userid, codeid) values ('" . $date . "', $userid, '".$codeid."')");

			$prizeid = mysql_result($getcodeinfo, 0, "prizeid");
			$clicks = mysql_result($getcodeinfo, 0, "clicks");
			$prizename = mysql_result(mysql_query("Select prizename from ".$prefix."promo_prizes where id=$prizeid"), 0);
			$prizeporkyps = mysql_result(mysql_query("Select porkyps from ".$prefix."promo_prizes where id=$prizeid"), 0);
			
			if ($prizeporkyps > 0) {
                    if (file_exists("inc/porkyp_funcs.php")) {
					require_once "inc/porkyp_funcs.php";
					// Pre-authorize the Porky Points transaction
					$checktransfer = porkychecktransfer($prizeporkyps);
					if ($checktransfer !== true) {
						mysql_query("DELETE FROM ".$prefix."promo_users WHERE userid='".$userid."' AND codeid='".$codeid."'") or die(mysql_error());
						load_template ($theme_dir."/header.php");
						load_template ($theme_dir."/mmenu.php");
						echo("<p><font size=\"3\"><b>ERROR: System error or not enough Porky Points to award this promo code.<br><br>Please contact the site's owner or support staff.</b></font></p>");
						include $theme_dir."/footer.php";
						exit;
					}
				}
			}
			$message = "Code Accepted!<br /><br />Surf $clicks pages and receive:<br />".$prizename;
		}
	}
}

####################

//Begin main page

####################

load_template ($theme_dir."/header.php");
echo("<div class=wfull>
    <div class=\"grid w960\">
        <div class=header-banner>&nbsp;</div>
    </div>
</div>");
load_template ($theme_dir."/mmenu.php");

if ($message != "") {
	echo("<p><font size=\"2\"><b>".$message."</b></font></p>");
}

?>

<!--center><h2><b>Enter A Surf Promo Code</b></h2></center>
<form action="surfcode.php?addcode=go" method="post">
<input type="text" name="surfcode">
<input type="submit" value="Add">
</form-->
<div class="table-structure">
    <table cellspacing="0" cellpadding="0" border="0" width="80%">
        <tr>
            <td valign="top" align="left" class="step-title">Enter A Surf Promo Code</td>
        </tr>
        <tr>
            <td valign="top" class="Image">
                <div style="text-align: center">
                    <div id="loading"></div>
                    <form action="surfcode.php?addcode=go" method="post">
                        <div>Surf Promo Code: <input type="text" name="surfcode" value=""></div>
                        <input type="submit" style="width:80px!important;margin:10px;" name="Add" value="Add" class="del-btn">
                    </form>
                </div>
            </td>
        </tr>
    </table>
</div>

<style type="text/css">
	hr {
	border: 0;
	clear:both;
	display:block;
	width: 98%;
	background-color: #BBBBBB;
	height: 1px;
	}
</style>

<center>
<br>
<hr>
<h2><b>Active Promo Codes</b></h2>
<br>

<?php

$currentdate = date("Y-m-d");
$checkdate = strftime("%Y-%m-%d", strtotime("$currentdate + 60 days ago"));

$getcodes = mysql_query("SELECT b.clicks AS clicks, b.code AS code, c.prizename AS prizename FROM ".$prefix."promo_users a LEFT JOIN ".$prefix."promo_codes b ON (a.codeid=b.id) LEFT JOIN ".$prefix."promo_prizes c ON (b.prizeid=c.id) WHERE a.userid='".$userid."' AND a.date >= '".$checkdate."' AND a.state='0' AND b.id > '0' AND c.id > '0' ORDER BY a.state ASC, b.code ASC") or die(mysql_error());

if (mysql_num_rows($getcodes) > 0) {
	
	echo("<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">");
	
	for ($i = 0; $i < mysql_num_rows($getcodes); $i++) {
		$codeclicks = mysql_result($getcodes, $i, "clicks");
		$code = mysql_result($getcodes, $i, "code");
		$prizename = mysql_result($getcodes, $i, "prizename");
		
		$clicksleft = $codeclicks - $clickstoday;
		if ($clicksleft <= 1) {
			$status = "Surf 1 more page today";
		} else {
			$status = "Surf ".$clicksleft." more pages today";
		}

		
		echo("<tr>
			<td align=\"left\"><font size=\"2\">".$code." - ".$prizename." - ".$status."</font></td>
		</tr>");
		
	}
	
	echo("</table>");
	
} else {
	echo("<p><font size=\"2\">You don't have any active promo codes.</font></p>");
}


?>

<br>
<hr>
<h2><b>Completed Promo Codes</b></h2>
<br>

<?php

$currentdate = date("Y-m-d");
$checkdate = strftime("%Y-%m-%d", strtotime("$currentdate + 60 days ago"));

$getcodes = mysql_query("SELECT b.code AS code, c.prizename AS prizename FROM ".$prefix."promo_users a LEFT JOIN ".$prefix."promo_codes b ON (a.codeid=b.id) LEFT JOIN ".$prefix."promo_prizes c ON (b.prizeid=c.id) WHERE a.userid='".$userid."' AND a.date >= '".$checkdate."' AND a.state='1' AND b.id > '0' AND c.id > '0' ORDER BY b.code ASC") or die(mysql_error());

if (mysql_num_rows($getcodes) > 0) {
	
	echo("<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">");
	
	for ($i = 0; $i < mysql_num_rows($getcodes); $i++) {
		$code = mysql_result($getcodes, $i, "code");
		$prizename = mysql_result($getcodes, $i, "prizename");
		
		echo("<tr>
			<td align=\"left\"><font size=\"2\">".$code." - ".$prizename."</font></td>
		</tr>");
		
	}
	
	echo("</table>");
	
} else {
	echo("<p><font size=\"2\">You don't have any recently completed promo codes.</font></p>");
}


?>

</center>

<?php

include $theme_dir."/footer.php";
exit;

?>