<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";

if ($_SESSION['surfimage'] == "") {
echo("Session error");
exit;
}

$imagepath = $_SESSION['surfimage'];

if (!file_exists($imagepath)) {
echo("Image not found");
exit;
}

header('Cache-Control: no-store, must-revalidate');
header("Content-Type: image/jpeg");
@readfile($imagepath);

exit;

?>