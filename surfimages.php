<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

$get_icon_theme = @mysql_query("SELECT surficontheme from ".$prefix."settings LIMIT 1");
$icon_theme = @mysql_result($get_icon_theme, 0, "surficontheme");

$default_theme = "surficon_themes/LFMTE_default";

if (file_exists($icon_theme."/surfimages.php")) {

	// Use Selected Surfbar Icons Theme
	$image_dir = $icon_theme;
	include($icon_theme."/surfimages.php");
	
} elseif (file_exists("surfimages.bak.php")) {

	// Use Pre-Update v2.11 Backup Files
	$image_dir = "simg";
	include("surfimages.bak.php");
	
} elseif (file_exists($default_theme."/surfimages.php")) {

	// Attempt To Use The Default Icons
	$image_dir = $default_theme;
	include($default_theme."/surfimages.php");
	
} else {

	echo("ERROR - Could not find surfing icons: ".$icon_theme);
	exit;

}

?>