<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.27
// Copyright �2013 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
include "surfbarColors.php";
include "surfbarSizes.php";

session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

require_once "lhuntfunctions.php"; //Word Search Game

if ($_SESSION["refreshcount"] > 3) {
	//Auto refresh detected
	header("Location: members.php?mf=lo");
	exit;
}

$getacctype = mysql_query("Select email, status, mtype, lastclick, clickstoday from `".$prefix."members` where Id=$userid limit 1");
$acctype = mysql_result($getacctype, 0, "mtype");
$useremail = mysql_result($getacctype, 0, "email");
$accstatus = mysql_result($getacctype, 0, "status");
$lastclicktime = mysql_result($getacctype, 0, "lastclick");
$clickstoday = mysql_result($getacctype, 0, "clickstoday");

if ($accstatus == "Suspended") {
header("Location: members.php?suspended=yes");
exit;
}

$_SESSION["prefix"] = $prefix;

include("sfunctions.php"); //Main functions
include("smfunctions.php"); //Member functions
include("surfoutput.php"); //Output functions

include("surf_geturl.php"); //Get URL functions

$gettimer = mysql_query("Select surftimer from `".$prefix."membertypes` where mtid=$acctype limit 1");
$timer = mysql_result($gettimer, 0, "surftimer");

$getframebreak = mysql_query("Select framebreakblock from `".$prefix."settings` limit 1");
$framebreakblock = mysql_result($getframebreak, 0, "framebreakblock");

// Get Surfbar Prefs
include("surf_prefs.php");
$surfbarprefs = get_user_prefs($userid);
$_SESSION['preload_sess'] = $surfbarprefs['preloadsites'];
// End Get Surfbar Prefs


$newsite = "";

if ($framebreakblock == 1 && $_GET['framecatch'] == "yes") {
	//Frame Breaker Detected
	$newsite = "framebreakcatch.php";
	$_SESSION["catch1"] = $_SESSION['frame1site'];
	$_SESSION["catch2"] = $_SESSION['frame2site'];
	
} elseif ($surfbarprefs['surfbarstyle'] == 1 && file_exists("surf_frame_classic.php")) {

	// Post Method
	
	checkautoassign($userid, $acctype);
	
	if (!isset($_POST['x']) || !is_numeric($_POST['x'])) {
		$_SESSION['sysmess'] = "<span style=\"color: blue;\"><b>Starting surf session</b></span>";
		$_SESSION["refreshcount"] = $_SESSION["refreshcount"]+1;
		$_SESSION["siteviewed"] = 0;
		$_SESSION["clickcount"] = 0;
		$_SESSION["checkletter"] = 0;
	
	} elseif ($lastclicktime > (time()-$timer)) {
		//Multiple browsers detected
		@mysql_query("Update `".$prefix."members` set lastclick=0 where Id=$userid limit 1");
		header("Location: members.php?mf=lo");
		exit;
	
	} elseif (($_POST['x'] < $_SESSION['mincorrect']) || ($_POST['x'] > $_SESSION['maxcorrect'])) {
		$_SESSION["refreshcount"] = 0;
		$_SESSION["checkletter"] = 0;
		wrongclick($userid);
	
	} elseif ($_POST['barcode'] != $_SESSION['surfbarkey']) {
		$_SESSION['sysmess'] = "<span style=\"color: red;\"><b>Verification error</b></span>";
		$_SESSION["refreshcount"] = 0;
		$_SESSION["siteviewed"] = 0;
		$_SESSION["checkletter"] = 0;
	
	} elseif (($_POST['x'] >= $_SESSION['mincorrect']) && ($_POST['x'] <= $_SESSION['maxcorrect'])) {
		$_SESSION["refreshcount"] = 0;
		$_SESSION["clickcount"] = $_SESSION["clickcount"] + 1;
		$_SESSION["checkletter"] = 1;
		creditmember($userid, $acctype,$creditColor);
	
	} else {
		$_SESSION['sysmess'] = "<span style=\"color: blue;\"><b>Starting surf session</b></span>";
		$_SESSION["refreshcount"] = $_SESSION["refreshcount"]+1;
		$_SESSION["siteviewed"] = 0;
		$_SESSION["clickcount"] = 0;
		$_SESSION["checkletter"] = 0;
	}

} else {

	// Ajax Method - Start surfing session
	
	checkautoassign($userid, $acctype);
	$_SESSION['sysmess'] = "<span style=\"color: blue;\"><b>Starting surf session</b></span>";
	$_SESSION["refreshcount"] = $_SESSION["refreshcount"]+1;
	$_SESSION["siteviewed"] = 0;
	$_SESSION["clickcount"] = 0;
	$_SESSION["checkletter"] = 0;
}

if ($surfbarprefs['surfbarstyle'] == 1 && file_exists("surf_frame_classic.php")) {
	include("surf_frame_classic.php");
} else {
	include("surf_frame.php");
}

exit;

?>