<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.30
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

// These functions are no longer used in LFMTE.
// They will echo the input for backwards-compatibility purposes

function surfoutput($input) {
	echo($input);
}

function surfoutputframe($output, $enclevel) {
	echo($output);
}

function surfoutputbar($output, $enclevel) {
	echo($output);
}

?>