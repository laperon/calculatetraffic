<?php

//ok what're we doing here
$a = (!empty($_GET['a']) && in_array($_GET['a'], array('doauth', 'docollect')))
    ? $_GET['a']
    : false;

if (!$a)
{
    $a = !empty($_POST['a']) && in_array($_POST['a'], array('docollectmulti'))
        ? $_POST['a']
        : false;
}

//if $a is false, invalid parameters
if (!$a)
{
    exit;
}

@set_time_limit(0);
ignore_user_abort(true);

//shared key - do not EDIT, do not SHARE
//this key is tied to your tecommandpost account
$sharedkey = '0f0fdfbc3160b3b86a9364f468b';

//hmac function
function hmac($data, $key, $hash = 'md5', $blocksize = 64)
{
    if (strlen($key) > $blocksize)
    {
        $key = pack('H*', $hash($key));
    } else
    {
        $key = $key;
    }

    $key = str_pad($key, $blocksize, chr(0));
    $ipad = str_repeat(chr(0x36), $blocksize);
    $opad = str_repeat(chr(0x5c), $blocksize);

    return $hash(($key ^ $opad) . pack('H*', $hash(($key ^ $ipad) . $data)));
}

//db connection
require 'inc/config.php';
require 'inc/lfmsql.php';
require 'inc/lfm_password.php';

function escaper($val,$link){
    if ($GLOBALS['lfmsqlext'] == "mysql"){
        return mysql_real_escape_string($val);
    }
    return mysqli_real_escape_string($link,$val);
}

$link=lfmsql_connect($dbhost, $dbuser, $dbpass);


lfmsql_select_db($dbname);


//authorization
if ($a == 'doauth')
{
    //parameter check
    if (empty($_GET['u']) || empty($_GET['p']) || empty($_GET['h']))
    {
        echo 'invalid';
        exit;
    }

    //hmac shared key hash check
    if (hmac($_GET['u'] . $_GET['p'] . 'doauth', $sharedkey) != $_GET['h'])
    {
        echo 'invalid';
        exit;
    }

    //magic quotes checking
    if (get_magic_quotes_gpc())
    {
        $_GET['u'] = stripslashes($_GET['u']);
        $_GET['p'] = stripslashes($_GET['p']);
    }
    $u = escaper($_GET['u'],$link);
    $s_password=escaper($_GET['p'],$link);

    $authquery="SELECT Id, username, email, password, status FROM ".$prefix."members WHERE username='".$u."'";
    $authresult=lfmsql_query($authquery);
    $authrow=lfmsql_fetch_array($authresult);

    // Validate Password
    $loginuserid = $authrow["Id"];
    $passwordhash = $authrow["password"];


    if (strlen($passwordhash) == 32) {
        if($passwordhash == md5($s_password)){
            echo $loginuserid;
        }else{
            echo 'invalid';
            exit;
        }

    } else{
        if (lfmCheckPassword($s_password, $passwordhash)) {
            echo $loginuserid;
        }else {
            echo 'invalid';
            exit;
        }
    }

    //db cleansing
    $u = escaper($_GET['u'],$link);
    $p = crypt($_GET['p']);


}

//collecting
if ($a == 'docollect')
{
    if (!empty($_GET['u']) && !empty($_GET['k']) && !empty($_GET['h']))
    {
        //output container
        $out = array();

        //magic quotes checking
        if (get_magic_quotes_gpc())
        {
            $_GET['u'] = stripslashes($_GET['u']);
            $_GET['k'] = stripslashes($_GET['k']);
            $_GET['h'] = stripslashes($_GET['h']);
        }

        //hmac check
        if (hmac($_GET['u'] . 'docollect', $sharedkey) != $_GET['h'])
        {
            exit;
        }

        //sanitize $u
        $u = escaper($_GET['u'],$link);
        $k = $_GET['k'];

        //get users member id
        $mRes = lfmsql_query("SELECT `id` FROM `" . $prefix . "members` WHERE `id` = '$u' LIMIT 1");

        $memid = @lfmsql_result($mRes);

        //assigned credits
        $acRes = lfmsql_query(
            "SELECT SUM(`credits`) FROM `" . $prefix . "msites` WHERE `memid` = '$memid'"
        );

        if (lfmsql_num_rows($acRes))
        {
            $out['urls']['assigned'] = (string) round(@lfmsql_result($acRes, 0), 2);
        }

        //unassigned credits
        $ucRes = lfmsql_query(
            "SELECT `credits` FROM `" . $prefix . "members` WHERE `id` = '$memid' LIMIT 1"
        );

        if (lfmsql_num_rows($ucRes))
        {
            $out['urls']['unassigned'] = (string) round(@lfmsql_result($ucRes, 0), 2);
        }

        //urls
        $urlRes = lfmsql_query(
            "SELECT `id`, `url`, `credits` FROM `" . $prefix . "msites` WHERE `memid` = '$memid' ORDER BY `id` ASC"
        );

        if (lfmsql_num_rows($urlRes))
        {
            while ($urlArr = lfmsql_fetch_assoc($urlRes))
            {
                $out['urls']['urls'][] = array(
                    'id' => $urlArr['id'],
                    'url' => $urlArr['url'],
                    'assigned' => (string) round($urlArr['credits'], 2)
                );
            }
        }

        //assigned banner impressions
        $abiRes = lfmsql_query(
            "SELECT SUM(`imps`) FROM `" . $prefix . "mbanners` WHERE `memid` = '$memid'"
        );

        if (lfmsql_num_rows($abiRes))
        {
            $out['banners']['assigned'] = (string) round(@lfmsql_result($abiRes, 0), 2);
        }

        //unassigned banner impressions
        $ubiRes = lfmsql_query(
            "SELECT `bannerimps` FROM `" . $prefix . "members` WHERE `id` = '$memid' LIMIT 1"
        );

        if (lfmsql_num_rows($ubiRes))
        {
            $out['banners']['unassigned'] = (string) round(@lfmsql_result($ubiRes, 0), 2);
        }

        //banners
        $bRes = lfmsql_query(
            "SELECT `id`, `imps`, `img`, `target` FROM `" . $prefix . "mbanners` WHERE `memid` = '$memid' ORDER BY `id` ASC"
        );

        if (lfmsql_num_rows($bRes))
        {
            while ($bArr = lfmsql_fetch_assoc($bRes))
            {
                $out['banners']['banners'][] = array(
                    'id' => $bArr['id'],
                    'img' => $bArr['img'],
                    'url' => $bArr['target'],
                    'assigned' => (string) round($bArr['imps'], 2)
                );
            }
        }

        //assigned text link impressions
        $atliRes = lfmsql_query(
            "SELECT SUM(`imps`) FROM `" . $prefix . "mtexts` WHERE `memid` = '$memid'"
        );

        if (lfmsql_num_rows($atliRes))
        {
            $out['textlinks']['assigned'] = (string) round(@lfmsql_result($atliRes, 0), 2);
        }

        //unassigned text link impressions
        $utliRes = lfmsql_query(
            "SELECT `textimps` FROM `" . $prefix . "members` WHERE `id` = '$memid' LIMIT 1"
        );

        if (lfmsql_num_rows($utliRes))
        {
            $out['textlinks']['unassigned'] = (string) round(@lfmsql_result($utliRes, 0), 2);
        }

        //text links
        $tlRes = lfmsql_query(
            "SELECT `id`, `text`, `target`, `imps` FROM `" . $prefix . "mtexts` WHERE `memid` = '$memid' ORDER BY `id` ASC"
        );

        if (lfmsql_num_rows($tlRes))
        {
            while ($tlArr = lfmsql_fetch_assoc($tlRes))
            {
                $out['textlinks']['textlinks'][] = array(
                    'id' => $tlArr['id'],
                    'text' => $tlArr['text'],
                    'link' => $tlArr['target'],
                    'assigned' => (string) round($tlArr['imps'], 2)
                );
            }
        }

        //affiliate comm earned
        $cRes = lfmsql_query(
            "SELECT SUM(`commission`) FROM `" . $prefix . "sales` WHERE `status` IS NULL AND `affid` = '$memid'"
        );

        if (lfmsql_num_rows($cRes))
        {
            $out['cash']['com'] = round(@lfmsql_result($cRes, 0), 2);
        } else
        {
            $out['cash']['com'] = '0.00';
        }

        //minimum cashout requirement
        $out['cash']['min'] = 0.00;

        //text/html header.  no crazy chunked/encoding, plz
        header('Content-type: text/html');
    }

    //serialize $out
    $out = serialize($out);

    //render
    echo $out;
}

if ($a == 'docollectmulti')
{
    $out = array();

    function stripGPC($arr)
    {
        $ret = array();
        if (is_array($arr))
        {
            foreach ($arr AS $k => $v)
            {
                $ret[$k] = stripGPC($v);
            }
        } else
        {
            $ret = stripslashes($arr);
        }

        return $ret;
    }

    if (get_magic_quotes_gpc())
    {
        foreach (array('_GET', '_POST', '_COOKIE') AS $sg)
        {
            if (!empty(${$sg}))
            {
                ${$sg} = stripGPC(${$sg});
            }
        }
    }

    //hmac check
    $postwho = str_replace(array(';', '|'), array('%3B', '%7C'), $_POST['who']);
    $hmac = hmac('a=docollectmulti&who=' . $postwho, $sharedkey);

    if (empty($_POST['h']) || ($hmac != $_POST['h']))
    {
        exit;
    }

    if (!empty($_POST['who']))
    {
        $stringtoarray = explode('|', $_POST['who']);
        array_pop($stringtoarray);

        foreach ($stringtoarray AS $k)
        {
            $pieces = explode(';', $k);
            $kv[$pieces[0]] = strtolower($pieces[1]);
        }

        $who = strtolower(implode(', ', array_values($kv)));

        //assigned credits
        $acRes = lfmsql_query(
            "SELECT `memid`, SUM(`credits`) AS `asscredits` FROM `" . $prefix . "msites` WHERE `memid` IN($who) GROUP BY `memid`"
        );

        if (lfmsql_num_rows($acRes))
        {
            while ($acArr = lfmsql_fetch_assoc($acRes))
            {
                $key = array_keys($kv, $acArr['memid']);
                $key = $key[0];

                $out['urls']['assigned'][$key] = (string) round($acArr['asscredits'], 2);
            }
        }

        unset($acRes, $acArr);

        //unassigned credits
        $ucRes = lfmsql_query(
            "SELECT `id`, `credits` FROM `" . $prefix . "members` WHERE `id` IN($who)"
        );

        if (lfmsql_num_rows($ucRes))
        {
            while ($ucArr = lfmsql_fetch_assoc($ucRes))
            {
                $key = array_keys($kv, $ucArr['id']);
                $key = $key[0];

                $out['urls']['unassigned'][$key] = (string) round($ucArr['credits'], 2);
            }
        }

        unset($ucRes, $ucArr);

        //urls
        $urlRes = lfmsql_query(
            "SELECT `id`, `memid`, `url`, `credits` FROM `" . $prefix . "msites` WHERE `memid` IN($who) ORDER BY `id` ASC"
        );

        if (lfmsql_num_rows($urlRes))
        {
            while ($urlArr = lfmsql_fetch_assoc($urlRes))
            {
                $key = array_keys($kv, $urlArr['memid']);
                $key = $key[0];

                $out['urls']['urls'][$key][] = array(
                    'id' => $urlArr['id'],
                    'url' => $urlArr['url'],
                    'assigned' => (string) round($urlArr['credits'], 2)
                );
            }
        }


        unset($urlRes, $urlArr);

        //assigned banner impressions
        $abiRes = lfmsql_query(
            "SELECT `memid`, SUM(`imps`) AS `aimps` FROM `" . $prefix . "mbanners` WHERE `memid` IN($who) GROUP BY `memid`"
        );

        if (lfmsql_num_rows($abiRes))
        {
            while ($abiArr = lfmsql_fetch_assoc($abiRes))
            {
                $key = array_keys($kv, $abiArr['memid']);
                $key = $key[0];

                $out['banners']['assigned'][$key] = (string) round($abiArr['aimps'], 2);
            }
        }


        unset($abiRes, $abiArr);

        //unassigned banner impressions
        $ubiRes = lfmsql_query(
            "SELECT `id`, `bannerimps` FROM `" . $prefix . "members` WHERE `id` IN($who)"
        );

        if (lfmsql_num_rows($ubiRes))
        {
            while ($ubiArr = lfmsql_fetch_assoc($ubiRes))
            {
                $key = array_keys($kv, $ubiArr['id']);
                $key = $key[0];

                $out['banners']['unassigned'][$key] = (string) round($ubiArr['bannerimps'], 2);
            }
        }


        unset($ubiRes, $ubiArr);

        //banners
        $bRes = lfmsql_query(
            "SELECT `id`, `memid`, `imps`, `img`, `target` FROM `" . $prefix . "mbanners` WHERE `memid` IN($who) ORDER BY `id` ASC"
        );

        if (lfmsql_num_rows($bRes))
        {
            while ($bArr = lfmsql_fetch_assoc($bRes))
            {
                $key = array_keys($kv, $bArr['memid']);
                $key = $key[0];

                $out['banners']['banners'][$key][] = array(
                    'id' => $bArr['id'],
                    'img' => $bArr['img'],
                    'url' => $bArr['target'],
                    'assigned' => (string) round($bArr['imps'], 2)
                );
            }
        }


        unset($bRes, $bArr);

        //assigned text link impressions
        $atliRes = lfmsql_query(
            "SELECT `memid`, SUM(`imps`) AS `aimps` FROM `" . $prefix . "mtexts` WHERE `memid` IN($who) GROUP BY `memid`"
        );

        if (lfmsql_num_rows($atliRes))
        {
            while ($atliArr = lfmsql_fetch_assoc($atliRes))
            {
                $key = array_keys($kv, $atliArr['memid']);
                $key = $key[0];

                $out['textlinks']['assigned'][$key] = (string) round($atliArr['aimps'], 2);
            }
        }


        unset($atliRes, $atliArr);

        //unassigned text link impressions
        $utliRes = lfmsql_query(
            "SELECT `id`, `textimps` FROM `" . $prefix . "members` WHERE `id` IN($who)"
        );

        if (lfmsql_num_rows($utliRes))
        {
            while ($utliArr = lfmsql_fetch_assoc($utliRes))
            {
                $key = array_keys($kv, $utliArr['id']);
                $key = $key[0];

                $out['textlinks']['unassigned'][$key] = (string) round($utliArr['textimps'], 2);
            }
        }


        unset($utliRes, $utliArr);

        //text links
        $tlRes = lfmsql_query(
            "SELECT `id`, `memid`, `text`, `target`, `imps` FROM `" . $prefix . "mtexts` WHERE `memid` IN($who) ORDER BY `id` ASC"
        );

        if (lfmsql_num_rows($tlRes))
        {
            while ($tlArr = lfmsql_fetch_assoc($tlRes))
            {
                $key = array_keys($kv, $tlArr['memid']);
                $key = $key[0];

                $out['textlinks']['textlinks'][$key][] = array(
                    'id' => $tlArr['id'],
                    'text' => $tlArr['text'],
                    'link' => $tlArr['target'],
                    'assigned' => (string) round($tlArr['imps'], 2)
                );
            }
        }

        unset($tlRes, $tlArr);

        //affiliate comm earned
        $cRes = lfmsql_query(
            "SELECT `affid`, SUM(`commission`) AS `comfield` FROM `" . $prefix . "sales` WHERE `status` IS NULL AND `affid` IN($who) GROUP BY `affid`"
        );

        if (lfmsql_num_rows($cRes))
        {
            while ($cArr = lfmsql_fetch_assoc($cRes))
            {
                $key = array_keys($kv, $cArr['affid']);
                $key = $key[0];

                $out['cash']['com'][$key] = round($cArr['comfield'], 2);
            }
        } else
        {
            $out['cash']['com'] = array();
        }

        unset($cRes, $cArr);

        //text/html header.  no crazy chunked/encoding, plz
        header('Content-type: text/html');

        //serialize $out
        $out = serialize($out);

        //render
        echo $out;
    }
}