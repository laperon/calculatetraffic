<?php

// /////////////////////////////////////////////////////////////////////
// LFMTE v2.11
// Copyright �2011 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

include "inc/checkauth.php"; 
include "inc/funcs.php";

if(!isset($_SESSION["adminid"])) { exit; };

require "admin/templates_defaults.php";

if (isset($_GET['template_name']) && $_GET['template_name'] != "" && is_numeric($_GET['restore_version'])) {

	$template_name = urldecode($_GET['template_name']);
	$restore_version = $_GET['restore_version'];
	
	if ($restore_version == 0) {
	
		if (isset($default_templates[$template_name])) {
			$templatehtml = $default_templates[$template_name];
		} else {
			$getrestoretemplate = mysql_query("SELECT template_data from ".$prefix."templates_defaults WHERE template_name='".$template_name."'") or die(mysql_error());
			if (mysql_num_rows($getrestoretemplate) > 0) {
				$templatehtml = mysql_result($getrestoretemplate, 0, "template_data");
			} else {
				echo("<html><body><center><p><font size=\"2\">Could not find template data.</font></p></center></body></html>");
				exit;
			}
		}
			
	} else {
		
		$getrestoretemplate = mysql_query("SELECT template_data from ".$prefix."templates_backups WHERE id='".$restore_version."' AND template_name='".$template_name."'") or die(mysql_error());
		if (mysql_num_rows($getrestoretemplate) > 0) {
			$templatehtml = mysql_result($getrestoretemplate, 0, "template_data");
		} else {
			echo("<html><body><center><p><font size=\"2\">Could not find template data.</font></p></center></body></html>");
			exit;
		}
	}

} elseif (isset($_GET['mtid']) && is_numeric($_GET['mtid']) && is_numeric($_GET['restore_version'])) {

	$template_name = $_GET['mtid'];
	$restore_version = $_GET['restore_version'];
	
	if ($restore_version == 0) {
	
		if (isset($default_templates['Members Area'])) {
			$templatehtml = $default_templates['Members Area'];
		} else {
			echo("<html><body><center><p><font size=\"2\">Could not find template data.</font></p></center></body></html>");
			exit;
		}
			
	} else {
		
		$getrestoretemplate = mysql_query("SELECT template_data from ".$prefix."memtemplates_backups WHERE id='".$restore_version."' AND mtid=".$template_name) or die(mysql_error());
		if (mysql_num_rows($getrestoretemplate) > 0) {
			$templatehtml = mysql_result($getrestoretemplate, 0, "template_data");
		} else {
			echo("<html><body><center><p><font size=\"2\">Could not find template data.</font></p></center></body></html>");
			exit;
		}
	}


} elseif (isset($_GET['pageid']) && is_numeric($_GET['pageid']) && is_numeric($_GET['restore_version'])) {

	$template_name = $_GET['pageid'];
	$restore_version = $_GET['restore_version'];
			
	$getrestoretemplate = mysql_query("SELECT template_data from ".$prefix."pages_backups WHERE id='".$restore_version."' AND pageid=".$template_name) or die(mysql_error());
	if (mysql_num_rows($getrestoretemplate) > 0) {
		$templatehtml = mysql_result($getrestoretemplate, 0, "template_data");
	} else {
		echo("<html><body><center><p><font size=\"2\">Could not find template data.</font></p></center></body></html>");
		exit;
	}

}

if (function_exists('html_entity_decode')) {
	$templatehtml = html_entity_decode($templatehtml);
} else {
	$templatehtml = unhtmlentities($templatehtml);
}

$templatehtml=translate_site_tags($templatehtml);

$getusermail = mysql_query("SELECT email FROM ".$prefix."members ORDER BY Id LIMIT 1");
if (mysql_num_rows($getusermail) > 0) {
	$useremail = mysql_result($getusermail, 0, "email");
	$templatehtml=translate_user_tags($templatehtml,$useremail);
}

echo("$templatehtml");
exit;

?>