<html>
<head>
	<title>Calculating Traffic</title>
	<link rel="stylesheet" type="text/css" href="page.css">
</head>

<body>
<center>
<table width=700 border=1 cellpadding=0 cellspacing=0>
<tr>
       <td valign=top><center><IMG src="http://www.surfingforsuccess.com/images/surfing-for-success.jpg" alt="Calculating Traffic"></center></td>
</tr>
<tr>
       <td valign=top style="background-color: #FFFFFF; padding-top: 20px; padding-bottom: 20px; padding-left: 10px; padding-right: 10px;">

	   <center
style="font-family: helvetica,arial,sans-serif; color: rgb(255, 0, 0);">
<h1>Terms Of Service</h1></center>
<hr style="width: 100%; height: 2px;">
<P><b>Welcome!</b></P>
<center><p><u><b>PLEASE REVIEW THIS DOCUMENT IN ITS ENTIRETY<br>You Must Agree To These Terms of Services Before Joining As A Member of CalculatingTraffic.com.</u></b></p></center>
<p><b><u>Terms of Membership</u></b></p>
<p>1. This Membership Agreement is a binding agreement between you the member and <a href="http://calculatingtraffic.com">CalculatingTraffic.com</a>, a manual traffic exchange.</p>
<p>2. Acceptance of membership and/or maintaining a subscription for services implies consent to this agreement, as well as future updates, alterations or changes to this agreement. CalculatingTraffic.com reserves the right to update and change these terms and conditions of use at any time without prior notice.</p>
<p>3. DO NOT SEND SPAM OR Unsolicited Commercial Email (UCE). SPAM and (UCE) will not be tolerated in any way. Formal complaints of SPAM/UCE are handled as top priority and legal action WILL be pursued by CalculatingTraffic.com for any loss or damages incurred due to SPAM/UCE complaints against you. Damages can be substantial. DO NOT SPAM!</p>
<p>4. Promoting Traffic-Splash using "paid to sign up" sites is NOT allowed.</p>
<p>5. CalculatingTraffic.com is not responsible for the income claims or promises with sites in rotation. You join any program at your own risk and we strongly advise you to research all business opportunities before you invest any money.</p>
<p>6. You agree to receive regular updates, discount offers and promotions from CalculatingTraffic.com and/or Darrell Dean. Your direct sponsor may also mail you for support purposes only. Please report any spam abuse to us immediately.</p>
<p>7. You are solely responsible for the maintenance and payment of any subscription(s) to CalculatingTraffic.com created by you or your representatives. We do not create or maintain member subscriptions to our services. Only those who have access to your online banking account(s) may do so. Please contact us if you need any advice on maintaining your subscription.</p>
<p>8. CalculatingTraffic.com is rotator friendly.  We recommend TE Toolbox rotators.</p>
<p>9. CalculatingTraffic.com reserves the right to refuse any advertisement and/or website that we feel is inappropriate. This includes but is not limited to the following types of programs:</p>
<p>- Sites that contain Extreme Adult Content, Pornography, Racial or Hate Issues, Vulgar Language, Illegal content, and Violence;</p>
<p>- Sites promoting HYIP Programs;</p>
<p>- All Auto Surf Programs;</p>
<p>- All Investment Surfs;</p>
<p>- All Paid To Promote Sites;</p>
<p>- Any site promoted as an 'instant money system' rather than providing real product or information of value (including spam, ponzis, randomizers, pyramid schemes etc.);</p>
<p>- Any other program which CalculatingTraffic.com deems inappropriate.<p>
<p>10. Websites will NOT be accepted into our network of member sites if they contain the following:</p>
<p>- More than one (1) pop-up window.  This pop-up window must not disrupt surfing in any way;.</p>
<p>- The ability to break out of CalculatingTraffic.com frames;</p>
<p>- Any form of Warez, including i-frame PPC sites;</p>
<p>- Overall content based on a language other than English.</p>
<p>11. If any sites which do not conform to items 9 & 10 above are found in the exchange, CalculatingTraffic.com reserves the right to immediately suspend the site, and possibly your account.</p>
<p>12. CalculatingTraffic.com is not responsible for any lost credits, or any damages, loss of data, or other problems associated with using our services. You agree to use this service at your own risk.</p>
<p>13. Only one account per user or IP address is permitted. Only one user per account. YOU must view sites to earn credits with the intention of displaying your own site/s to other members. You cannot have friends, family or employees view sites for you. If you do not want to view you can buy credits (if available at the time).</p>
<p>14. Changing Upline:  Deleting your account only to re-join again in an effort to change your sponsor/referrer/upline is not allowed. Asking members to delete their account and re-join under you is not allowed.</p>
<p>15. You may not sell any Traffic-Splash credits that you have earned in any way. You may not re-sell any Traffic-Splash credits that you have purchased without receiving prior permission from a representative of CalculatingTraffic.com. You may sell your account at CalculatingTraffic.com.</p>
<p>16. Your viewing session must be a full browser window and you may not use any auto-refreshing, or auto-clicking devices. ANY technique other than actually loading your CalculatingTraffic.com start page manually and viewing it yourself is a violation of the TERMS OF USE.</p>
<p>17. Advertising your CalculatingTraffic.com start page in any way that attempts to get credits from other people's viewing is considered theft and your account may be terminated.</p>
<p>18. CalculatingTraffic.com reserves the right to deactivate or remove FREE accounts if you do not view other members sites regularly. Usually within a 90-120 day period. No action will be taken without prior notice. Pro Members have no requirement to view sites to remain active.</p>
<p>19. You agree to maintain a functioning email address in your personal member details. A single autoresponder message from your account is grounds for account deletion and we WILL file a spam report.</p>
<p>20. All content included on this site, such as text, graphics, logos, button icons, images, audio clips, video clips and software, is the property of CalculatingTraffic.com or its content suppliers and protected by U.S. and international copyright laws. No part of this website may be reproduced or transmitted in any form or by any means without written permission from the authors.</p>
<p>21. CalculatingTraffic.com does not warrant that the website and service will be error free or uninterrupted. The website and service is distributed on "as is" basis without warranties of any kind, either expressed or implied.(/p>
<p>22. CalculatingTraffic.com will not be responsible for any losses, damages or costs that you and/or your business may suffer when using the website or service. You agree to indemnify and hold harmless CalculatingTraffic.com from any claims resulting from the website or service.</p>
<br>
<p><b><u>Member Statement:</u></b></p>
I have thoroughly read and understand the CalculatingTraffic.com terms and conditions. I understand that CalculatingTraffic.com will always do it's utmost to provide a professional, quality advertising service. In joining and/or purchasing a CalculatingTraffic.com membership I am automatically agreeing to said terms and conditions and I understand that failure to comply with these terms may result in account termination.</p>
<p>If you have any questions please don't hesitate to Contact Us.</p>
<br>
<p><b><u>Summary of URL Rules:</u></b></p>
<p>Before submitting a URL to CalculatingTraffic.com, please read these rules very carefully and make sure your URL does not violate any of them. If you violate any of these rules, your site will be deleted along with any credits assigned to them. Your CalculatingTraffic.com account may also be deleted.</p>
<p>- No sites with illegal activity:</p>
<p>- No frame-breaking sites:</p>
<p>- No viruses, trojans, or spyware:</p>
<p>- No adult content;</p>
<p>- Rotators may be promoted only if all sites or banners in rotation comply with these terms;</p>
<p>- No HYIPs or "investment autosurfs". These are illegal in most countries, including Canada and the United States;</p>
<p>- CalculatingTraffic.com has the full right to delete any site that we deem inappropriate or questionable at our sole discretion, and may do so without first notifying you.</p>
<br>
<center>
<script language="javascript" type="text/javascript">
<!--

var win=null;

function NewWindow(mypage,myname,w,h,scroll,pos){

if(pos=="random"){
	LeftPosition=(screen.availWidth)?Math.floor(Math.random()*(screen.availWidth-w)):50;
	TopPosition=(screen.availHeight)?Math.floor(Math.random()*((screen.availHeight-h)-75)):50;
}
if(pos=="center"){
	LeftPosition=(screen.availWidth)?(screen.availWidth-w)/2:50;
	TopPosition=(screen.availHeight)?(screen.availHeight-h)/2:50;
}
if(pos=="default"){
	LeftPosition=170;
	TopPosition=100;
}else if((pos!="center" && pos!="random" && pos!="default") || pos==null){
	LeftPosition=0;
	TopPosition=20;
}
settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',location=yes,directories=no,status=yes,menubar=yes,toolbar=yes,resizable=no';

win=window.open(mypage,myname,settings);
if(win.focus){
	win.focus();
}       
}
// -->
</script>
<a href="http://www.e-trust-e.com/verify.php?id=188" target="_blank" onclick="NewWindow(this.href,'','650','550','yes','default');return false" onfocus="this.blur()" class="navlink"><img src="http://www.e-trust-e.com/image.php?id=188" alt="e-trust-e Member Seal. Your guarantee of a good experience online!" border="0"></a><br><img src="http://www.e-trust-e.com/rating_stars.php?id=188">
</center>
<br>
<hr style="width: 100%; height: 2px;">
<div
style="text-align: center; font-family: helvetica,arial,sans-serif;"><font
size="-1"><span style="font-weight: bold;"><a target="_blank"
href="http://www.surfingforsuccess.com/contactus">Contact Us</a> |
Terms of Service
| <a target="_blank"
href="http://www.surfingforsuccess.com/disclaimer.php">Earnings
Disclaimer</a> | <a target="_blank"
href="http://www.surfingforsuccess.com/privacy.php">Privacy Policy</a>
| <a target="_blank"
href="http://www.surfingforsuccess.com/aboutus.php">About Us</a></span></font><font
size="-1"><span style="font-weight: bold;"></span><br>
<br>
<span style="font-weight: bold;">&copy;
Copyright 2012, CalculatingTraffic.com, All Rights Reserved Worldwide</span></font></div> 

	   
	   </td>
</tr>
<tr>
       <td valign=top><center><IMG src="http://www.surfingforsuccess.com/images/footer.jpg" alt="Calculating Traffic"></center></td>
</tr>
</table>
</center>



</body>
</html>