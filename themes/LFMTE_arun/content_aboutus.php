<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">About Us</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <p> Darrell Dean is a dedicated and successful small business owner, proudly offering exceptional products and services to customers and
                partners worldwide.  Customer service is paramount to Darrell.</p>

            <p>&nbsp;</p>

            <p>Formally trained as a professional accountant (Certified General Accountant - CGA) with a university degree in economics, in 2001 Darrell left his successful corporate career to begin a new direction in his own search for personal development, freedom and prosperity.  Darrell's
                mission is to awaken people to the possibilities for experiencing greater abundance.</p>

            <p>&nbsp;</p>


            <p>A dynamic, empowering and entertaining entrepreneur, he awakes each day with a deep commitment to help others achieve their "Plan B."</p>
            <p>&nbsp;</p>

            <p>In 2005 Darrell published Robert Collier's timeless classic self-help book <strong>"Be Rich:  The Science of Getting What You Want."</strong>  This publication came about as a labor of love and desire to share Mr. Collier's important message to others.  This book fits perfectly with Darrell's philosophy of personal development as a key component for personal success.</p>

            <p>&nbsp;</p>

            <p>Other online businesses owned and operated by Darrell include:</p>

            <p>&nbsp;</p>

            <p><strong><a href="http://surfingforsuccess.com" target="_blank">SurfingForSuccess.com</a></strong><br />

            <p>&nbsp;</p>

                <strong><a href="http://onlineorganizingmadeeasy.com" target="_blank">OnlineOrganizingMadeEasy.com</a></strong><br />
            <p>&nbsp;</p>


                <strong><a href="http://berichebook.com" target="_blank">BeRicheBook.com</a></strong></p>

            <p>&nbsp;</p>

            <p>Darrell's other successful offline ventures in the past include:   a bicycling touring company, conducting tours in the southern area of England and the Loire Valley area of France; two Network Marketing companies; a Financial Planning and Consulting company; and, a professional futures/commodity trader practice.</p>

            <p>&nbsp;</p>

            <p>Darrell lives in the Kingston, Ontario, Canada area with his wife Sue and daughter Ashley.  He enjoys fishing, boating, skiing, writing and
                playing the piano.
            </p>
            <br>
            <div class="clear"></div>
        </div>
        <div class="col-md-4">
            <img src="<?php echo $theme_dir ?>/images/ab-right.jpg" alt="About traffic exchange system CalculatingTraffic.com"  title="About us"/>
        </div>
    </div>
</div>
