
        <div class="container">
            <div><h1 style="color:#09405F; text-align: center;">Contact Us</h1></div>

            <div class="row">
                <div class="col-md-8">
                    <div class="contact-map">

                        <p class="contact-map-details" itemscope itemtype="http://schema.org/PostalAddress">
                            <span><img src="<?php echo $theme_dir ?>/images/map-icon.png" alt="" title="" /></span>
                            <span>Darrell Dean</span>
                            <span itemprop="addressRegion">Kingston, Ontario</span>
                            <span itemprop="addressCountry">Canada</span>
                            <span itemprop="streetAddress">K7P 1P2</span>
                        </p>
                        <!--<div id="map-canvas"></div>-->
                        <div><iframe width="90%" height="300" class="gmap" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Kingston,Ontario,Canada,K7P1P2&amp;z=7&amp;iwloc=A&amp;output=embed&amp;iwloc=near"></iframe></div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="category">
                        <ul>
                            <li><center><h2><b>Information Center</b></h2></center></li>
                            <hr/>
                            <li><h3>Frequently Asked Questions</h3></li>
                            <li><a target="_blank" href="http://calculatingtraffic.com/faqs.php"><b>FAQ</b></a></li>
                            <hr/>
                            <li><h3>Our website rules<h3></li>
                            <li><a target="_blank" href="http://www.calculatingtraffic.com/terms.php">Terms of services</b></a></li>
                            <hr/>
                            <li><h3>Contact us</h3></li>
                            <li><a target="_blank" href="http://www.darrellddean.com/support/contact/">Support Center</b></span></a></li>
                        </ul>
                    </div>

                </div>
                </div>
            </div>
        </div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">

$(document).ready(function() {
var map;
function initialize() {

  var mapOptions = {
    zoom: 4,
    center: new google.maps.LatLng(44.25390, -76.58595)
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
}

google.maps.event.addDomListener(window, 'load', initialize);

});

</script>