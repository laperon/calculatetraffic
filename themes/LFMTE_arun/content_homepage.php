<div id="top_content">
  <div class="container">
    <div class="left_block ">
      <div class="left_block_img">
        <img class="img-responsive" src="<?php echo $theme_dir ?>/images/left_block_07.png" alt=""/>
        <h4>Darrell Dean<br/>Owner/Operator</h4>
      </div>
      <div class="left_block_img">
        <img class="img-responsive" src="<?php echo $theme_dir ?>/images/left_block_11.png" alt=""/>
      </div>
      <div class="left_block_img">
        <a data-toggle="modal" id="cash_paid" data-target="#modalJoinNow" href="#modalJoinNow">
          <img class="img-responsive" src="<?php echo $theme_dir ?>/images/left_block_15.png" alt=""/>
        </a>
      </div>
    </div>
    <div class="middle_block ">
      <img class="text-center center-block" src="<?php echo $theme_dir ?>/images/count_07.png" alt=""/>
      <p>Traffic daily</p>
      <h2>Increase Website Traffic For Free!</h2>
      <form class="advertise-now" action="">
        <input data-toggle="modal" data-target="#modalJoinNow" id="join_now" type="button" value="Join Now!"/>
        <div class="clear"></div>
        <input class="first_text_input js-site col-sm-5" type="text" placeholder="http://"/>
        <!--<div class="col-sm-2"></div>-->
        <input class="col-sm-5 js-email" type="text" placeholder="Your e-mail"/>
        <div class="clear"></div>
        <input id="advertise-now" type="button" value="Advertise now"/>
      </form>
      <div class="middle_block_content_text">


        <h3>...from the creator of the “Surfing Calculators” comes:  </h3>
        <p>Synchronized Surfing! A revolutionary new surfing concept designed to
          maximize your surfing results!</p>
        <p>My name is Darrell Dean, owner of CalculatingTraffic.com, and I have been
          using traffic exchanges for over 12 years. During that time testing has proven
          that surfing results increase when I use banners and text ads when
          advertising my web sites or splash pages in traffic exchanges. That`s what
          synchronized surfing is all about...</p>
        <article>Synchronization = your banners and text ads are shown <br/>
          at the same time your web site or splash page is shown! </article>
      </div>





    </div>

    <div class="right_block">
      <h2>Sign Up</h2>

    <?php require $_SERVER['DOCUMENT_ROOT'] .  '/login/social_login.php'; ?>

      <div class="soc_buttons">

        <div class="facebook_icon">
          <a href="/login/social_login.php?login=fb-login">Sign Up</a>
        </div>
        <div class="google_icon">
          <a href="/login/social_login.php?login=gplus-login">Sign Up </a>
        </div>
      </div>

        <!--<div class="google_icon"></div>-->
      <div class="line_or"></div>
      <div class="signup">
        <?php require $_SERVER['DOCUMENT_ROOT'] . '/signup_home.php'; ?>
      </div>
    </div>
  </div>
</div>
<div id="video_all">
  <div class="container">
    <div class="col-sm-6">
      <h2>How A Traffic Exchange Works</h2>
      <iframe width="540" height="298" src="https://www.youtube.com/embed/_8JgckH36JY" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="col-sm-6">
      <h2>The Story Behind Synchronized Surfing</h2>
      <iframe width="540" height="298" src="https://www.youtube.com/embed/yDCX2XsTnCo" frameborder="0" allowfullscreen></iframe>
    </div>
    <div class="clear"></div>
                <div class="social_icons_all col-sm-12 text-center center-block">
                    <div class="social_icons">
                        <div class="soc_icon text-center center-block">
                          <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//calculatingtraffic.com/aboutus.php"></a>
                        </div>
                        <div class="soc_icon text-center center-block">
                          <a href="https://twitter.com/home?status=http%3A//calculatingtraffic.com"></a>
                        </div>
                        <div class="soc_icon text-center center-block">
                          <a href="https://plus.google.com/share?url=http%3A//calculatingtraffic.com"></a>
                        </div>
                        <div class="soc_icon text-center center-block">
                          <a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//calculatingtraffic.com&title=&summary=&source="></a>
                        </div>
                        <div class="soc_icon text-center center-block">
                          <a href="https://pinterest.com/pin/create/button/?url=http%3A//calculatingtraffic.com/themes/LFMTE_arun/images/content_img_14.jpg&media=&description="></a>
                        </div>
                    </div>
                </div>
  </div>

</div>



<div id="calculation_traffic">
  <div class="container">
    <h3 class="text-center">Calculating Traffic</h3>
    <p class="text-center">is a full-feature manual traffic exchange:</p>
    <div class="list_traffic col-sm-12 ">
      <div class="col-sm-4 ">
        <img class="img-responsive " src="<?php echo $theme_dir ?>/images/content_img_12.jpg" alt=""/>
        <ul>
          <li>Website advertising</li>
          <li>Banner ads advertising</li>
          <li>Text ads advertising</li>
        </ul>
      </div>
      <div class="col-sm-4">
        <img class="img-responsive " src="<?php echo $theme_dir ?>/images/content_img_14.jpg" alt=""/>
        <ul>
          <li>Mobile-friendly</li>
          <li>Automatic commission payments</li>
          <li>Professional tracking link service </li>
        </ul>
      </div>
    </div>
  </div>
</div>



<div id="calculation_traffic_categories">
  <div class="container">
    <div class="col-sm-4">
      <div class="groups_item"></div>
      <p class="text-center">Get more sales & sign ups</p>
    </div>
    <div class="col-sm-4">
      <div class="groups_item"></div>
      <p class="text-center">Save time & money</p>
    </div>
    <div class="col-sm-4">
      <div class="groups_item"></div>
      <p class="text-center">Effective advertising solutions</p>
    </div>
  </div>
</div>



<div id="calculation_traffic_groups">
  <div class="container">
    <h3 class="text-center">Offering...</h3>

    <p class="text-center">Free. High Quality Traffic To Your Website</p>

    <p class="text-center">
      Get Started At <a href="http://calculatingtraffic.com/signup.php"> Calculating Traffic!</a>
    </p>

    <div class="col-sm-4">
      <div class="col-sm-12">
        <a href="http://calculatingtraffic.com/signup.php">
          <img class="img-responsive" src="<?php echo $theme_dir ?>/images/categories_12.jpg" alt=""/>

        </a>
      </div>
      <p>Results</p>
    </div>
    <div class="col-sm-4">
      <div class="col-sm-12">
        <a href="http://calculatingtraffic.com/signup.php">
          <img class="img-responsive" src="<?php echo $theme_dir ?>/images/categories_14.jpg" alt=""/>

        </a>
      </div>
      <p>Fairness</p>
    </div>
    <div class="col-sm-4">
      <div class="col-sm-12">
        <a href="http://calculatingtraffic.com/signup.php">
          <img class="img-responsive" src="<?php echo $theme_dir ?>/images/categories_16.jpg" alt=""/>
        </a>
      </div>
      <p>Service</p>
    </div>
  </div>
</div>

<div class="text-center">
  <a href="signup.php"><img src="/images/joinnow.png" border="0" alt="Join Now"></a><br>
</div>