<?php
$url = $_SERVER['REQUEST_URI'];
$url = preg_replace("/[^a-zA-Z]/","", $url);
?>
<!doctype html>
<html lang="en">
<head>
     <title><?php
		if($_SERVER['REQUEST_URI'] == '/'){
		    echo 'Traffic exchange system | traffic | CalculatingTraffic.com';
		}
		elseif($_SERVER['REQUEST_URI'] == '/contactus.php'){
		    echo 'Contact us | CalculatingTraffic.com';
		}
		elseif($_SERVER['REQUEST_URI'] == '/aboutus.php'){
		    echo 'About Us | CalculatingTraffic.com';
		}
        elseif($_SERVER['REQUEST_URI'] == '/faqs.php'){
		    echo 'FAQ | CalculatingTraffic.com';
		}
	    elseif($_SERVER['REQUEST_URI'] == '/login.php?s=noauth'){
		    echo 'Login | CalculatingTraffic.com';
		}
	    elseif($_SERVER['REQUEST_URI'] == '/signup.php'){
		    echo 'Sign Up | CalculatingTraffic.com';
		}
        ?></title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:image" content="http://calculatingtraffic.com/themes/LFMTE_arun/images/logo_03.png" />

  <?php

    	if($_SERVER['REQUEST_URI'] == '/'){
		    echo '<meta name="description" content="The best traffic exchange system CalculatingTraffic" />';
		}
		elseif($_SERVER['REQUEST_URI'] == '/contactus.php'){
		    echo '<meta name="description" content="Contact details of CalculatingTraffic" />';
		}
		elseif($_SERVER['REQUEST_URI'] == '/aboutus.php'){
		    echo '<meta name="description" content="Information about CalculatingTraffic" />';
		}
        elseif($_SERVER['REQUEST_URI'] == '/faqs.php'){
		    echo '<meta name="description" content="FAQ page of the CalculatingTraffic" />';
		}
	    elseif($_SERVER['REQUEST_URI'] == '/login.php?s=noauth'){
		    echo '<meta name="description" content="Login to CalculatingTraffic" />';
		}
	    elseif($_SERVER['REQUEST_URI'] == '/signup.php'){
		    echo '<meta name="description" content="Sign up to CalculatingTraffic" />';
		}

    ?>

    <link rel="stylesheet" href="<?php echo $theme_dir.'/css/bootstrap.min.css'; ?>"/>
    <!--<link rel="stylesheet" href="css/bootstrap-responsive.css"/>-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo $theme_dir.'/css/normalize.css'; ?> "/>
    <link rel="stylesheet" href="<?php echo $theme_dir ?>/css/jquery-ui.css">


    <?php if($_SERVER['REQUEST_URI'] == '/' ||
        $_SERVER['REQUEST_URI'] == '/aboutus.php' ||
        $_SERVER['REQUEST_URI'] == '/contactus.php' ||
        $_SERVER['REQUEST_URI'] == '/faqs.php' ||
        $url == 'rid' ||
        $_SERVER['REQUEST_URI'] == '/login.php?s=noauth' ||
        $_SERVER['REQUEST_URI'] == '/how_it_works.php' ||
        $_SERVER['REQUEST_URI'] == '/signup.php') : ?>
    <link rel="stylesheet" href="<?php echo $theme_dir.'/css/style.css'; ?> "/>

    <?php else: ?>
      <link rel="stylesheet" href="<?php echo $theme_dir.'/css/custom.css'; ?> "/>
      <link rel="stylesheet" href="<?php echo $theme_dir.'/css/gridiculous.css'; ?> "/>
      <link rel="stylesheet" href="<?php echo $theme_dir.'/css/style.css'; ?> "/>


    <?php endif; ?>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="<?php echo $theme_dir.'/js/bootstrap.min.js'; ?>"></script>
    <script src="<?php echo $theme_dir.'/js/main.js'; ?>"></script>
    <script src="<?php echo $theme_dir.'/js/jquery.validate.min.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo $theme_dir ?>/js/jquery-ui.min.js"></script>


    <?php lfm_head(); ?>
</head>

<body>
    <div id="main_wrapper">
        <div id="header_all">
            <div class="container">

                <div class="header_block col-sm-offset-2 col-sm-8">
                    <div class="col-sm-8 col-xs-6">
                        <a href="/members.php"><img style="margin-left: 0;" src="<?php echo $theme_dir ?>/images/logo_03.png" alt="logo"/></a>
                    </div>
                    <div class="header_right col-sm-4 text-right col-xs-6">
                        <span ><a href="http://calculatingtraffic.com/contactus.php">Help</a></span>
                        <span >|</span>
                        <span ><a href="http://calculatingtraffic.com/faqs.php">FAQ</a></span>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="header_bottom menu_all col-sm-8 ">
                    <nav class="navbar  navbar-fixed navigation" role="nav">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div id="bs-example-navbar-collapse-1" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                                <ul class="nav navbar-nav ">
                                 <?php
                          //echo '<pre>'; print_r($_SESSION); 
                          if($_SESSION["uid"] == '')
                          {
		                        ?>
                          <li <?php if($_SERVER['PHP_SELF']=="/index.php") echo 'class="active item"'; ?>><a href="index.php">Home</a></li>
                          <li <?php if($_SERVER['PHP_SELF']=="/how_it_works.php") echo 'class="q item"'; ?>><a href="how_it_works.php"> How It Works </a> </li>
                          <li <?php if($_SERVER['PHP_SELF']=="/login.php") echo 'class="active item"'; ?>><a data-toggle="modal" data-target="#modalJoinNow" href="#modalJoinNow"> Surf Now </a> </li>
                          <li <?php if($_SERVER['PHP_SELF']=="/aboutus.php") echo 'class="active item"'; ?>><a href="aboutus.php"> About us </a> </li>
                          <li <?php if($_SERVER['PHP_SELF']=="/contactus.php") echo 'class="active item"';?>><a href="contactus.php">Contact us</a></li>
                          <li <?php if($_SERVER['PHP_SELF']=="/login.php") echo 'class="active item"'; ?> ><a id="login" data-toggle="modal" data-target="#modalJoinNow" href="#modalJoinNow">Login</a></li>
                          <li <?php if($_SERVER['PHP_SELF']=="/signup.php") echo 'class="active item"'; ?> ><a href="signup.php">Sign Up</a></li>
		                     <?php }  ?>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                </div>
            </div>

        <?php if($_SERVER['REQUEST_URI'] == '/' ||
          $_SERVER['REQUEST_URI'] == '/aboutus.php' ||
          $_SERVER['REQUEST_URI'] == '/contactus.php' ||
          $_SERVER['REQUEST_URI'] == '/faqs.php' ||
          $_SERVER['REQUEST_URI'] == '/login.php?s=noauth' ||
          $url == 'rid' ||
          $_SERVER['REQUEST_URI'] == '/how_it_works.php' ||
          $_SERVER['REQUEST_URI'] == '/signup.php') : ?>
            <div id="banner_all">
              <?php
              include $theme_dir . '/slider.php';
              ?>
<!--                <div class="banner">-->
<!--                    <h1>CalculatingTraffic.com: The manual traffic exchange that treats-->
<!--                        everyone with respect, honesty and integrity. And-->
<!--                        we love free members, too!</h1>-->
<!--                </div>-->

                <!--<div class="container">-->

                <!--</div>-->
            </div>
        <?php endif; ?>






