$(document).ready(function () {

    $('.signup #signup-form').validate({
    rules : {
        firstname : {
            required: true,
            minlength: 2
        },
        lastname : {
            required: true,
            minlength: 2
        },
        email:{
            required: true,
        },
        email2:{
            required: true,
        },
        username: {
            required: true,
            minlength: 4
        },
        password: {
            required: true,
            minlength: 4
        },
        password2: {
            required: true,
            minlength: 4
        },
        captcha: {
            required: true,
            remote: "/captcha/process.php"
        },
        termsagree: {
            required: true,
        },
        mtid: 1

    },
    messages: {
        required: "Please enter a username",
        minlength: "Your username must consist of at least 2 characters"
    }
    });

    $("body").on("click", "#refreshimg", function(){
        $.post("/captcha/newsession.php");
        $("#captchaimage").load("/captcha/image_req.php");
        return false;
    });

    /*Advertise now*/
    $('form #advertise-now').on('click', function(){

        var ads_site = $('form.advertise-now .js-site').val();
        var ads_email = $('form.advertise-now .js-email').val();
        var error = '';

        if(!ads_email || !ads_site) {
            $('form.advertise-now .js-site').css('border' , 'solid 1px red');
            $('form.advertise-now .js-email').css('border' , 'solid 1px red');
        } else {
            $('form.advertise-now .js-site').css('border' , 'solid 1px #78ac00');
            $('form.advertise-now .js-email').css('border' , 'solid 1px #78ac00');

            $.post('/advertising.php' , { 'ads_site': ads_site , 'ads_email':ads_email } , function(success){
                console.log(success);
                var data = $.parseJSON(JSON.stringify(success));
                if(!data.error.length) {
                  $('#modalJoinNow').modal('show')
                } else {
                  alert(data.error);
                }
            } , "json");
        }
    });

    /*Cash paid*/
    $('#main_wrapper #cash_paid').on('click' , function(){
        $('form#loginfrm #option').html('<input type="hidden" name="cash_paid" value="1">');
    });
    $('.navbar #login').on('click' , function(){
        $('form#loginfrm #option').html('');
    })

})