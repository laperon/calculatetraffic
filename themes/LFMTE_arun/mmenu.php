<style type="text/css">

/*- Menu Tabs J--------------------------- */



    #lfm_menu {

      float:left;

      font-size:100%;

      line-height:normal;  

      margin-left:10px;

      margin-right:10px;

      }

    #lfm_menu ul {

        margin:0;

        padding:5px 10px 0 2px;

        list-style:none;

      }

    #lfm_menu li {

      display:inline;

      margin:0;

      padding:0;

      }

    #lfm_menu a {

      float:left;

      background:url("<?= $theme_dir ?>/images/tableftJ.gif") no-repeat left top;

      margin:0;

      padding:0 0 0 5px;

      text-decoration:none;

      }

    #lfm_menu a span {

      float:left;

      display:block;

      background:url("<?= $theme_dir ?>/images/tabrightJ.gif") no-repeat right top;

      padding:5px 15px 4px 6px;

      color:#24618E;

      }

    /* Commented Backslash Hack hides rule from IE5-Mac \*/

    #lfm_menu a span {float:none;}

    /* End IE5-Mac hack */

    #lfm_menu a:hover span {

      color:#FFF;

      }

    #lfm_menu a:hover {

      background-position:0% -42px;

      }

    #lfm_menu a:hover span {

      background-position:100% -42px;

      }



        #lfm_menu #current a {

                background-position:0% -42px;

        }

        #lfm_menu #current a span {

                background-position:100% -42px;

                color:#FFF;

        }

li.active a {
    background: none repeat scroll 0 0 #00d8ff;
    color: #fff;
}
-->

.showhide {
    display: none !important;
}

</style>

<div class="wfull">
<div class="grid w960">
    <div class="menu-button">Menu</div>
<div class="c3 members_menu">
    <div class="tabs">
        <ul>
             <li class="heading">Start</li>
            <? if(get_setting("enablemenu_home") == "1") { ?><li><a href="members.php"><span>Home</span></a></li><? } ?>

<li <?php if($_SERVER['REQUEST_URI']=="/autoassign.php?surfreturn=1") echo 'class="active"';?>><a href="surfing.php"><span style="color:#FF0000;"><b>Surf</b></span></a></li>

<li <?php if($_SERVER['PHP_SELF']=="/surfcode.php") echo 'class="active"';?>><a href="surfcode.php">Enter Surf Code</a></li>

<li <?php if($_SERVER['PHP_SELF']=="/claim.php") echo 'class="active"';?>><a href="claim.php">Surfer Rewards</a></li>

<li class="heading">Your Advertising</li>

<li <?php if($_SERVER['PHP_SELF']=="/mysites.php") echo 'class="active"';?>><a href="mysites.php"><span>Sites</span></a></li>

<li <?php if($_SERVER['PHP_SELF']=="/mybanners.php") echo 'class="active"';?>><a href="mybanners.php"><span>Banners</span></a></li>

<li <?php if($_SERVER['PHP_SELF']=="/mytexts.php") echo 'class="active"';?>><a href="mytexts.php"><span>Text Ads</span></a></li>

<li <?php if($_SERVER['PHP_SELF']=="/autoassign.php" && $_SERVER['REQUEST_URI'] != "/autoassign.php?surfreturn=1") echo 'class="active"';?>><a href="autoassign.php"><span>Auto Assign</span></a></li>

<li <?php if($_SERVER['PHP_SELF']=="/myrotators.php") echo 'class="active"';?>><a href="myrotators.php"><span>Trackers/Rotators</span></a></li>

<li class="heading">Manage Accounts</li>

<? if(get_setting("enablemenu_upgrade") == "1") { ?><li <?php if($_SERVER['REQUEST_URI']=="/members.php?mf=ug") echo 'class="active"';?>><a href="members.php?mf=ug"><span>Upgrade Account</span></a></li><? } ?>

<li <?php if($_SERVER['PHP_SELF']=="/buycredits.php") echo 'class="active"';?>><a href="buycredits.php"><span>Buy Credits</span></a></li>

<? if(get_setting("enablemenu_profile") == "1") { ?><li <?php if($_SERVER['REQUEST_URI']=="/members.php?mf=pf") echo 'class="active"';?>><a href="members.php?mf=pf"><span>Profile</span></a></li><? } ?>

<? if(get_setting("enablemenu_makemoney") == "1") { ?><li <?php if($_SERVER['REQUEST_URI']=="/members.php?mf=mc") echo 'class="active"';?>><a href="members.php?mf=mc"><span>Commissions</span></a></li><? } ?>

<li class="heading">Affiliate Tools</li>

<? if(get_setting("enablemenu_promote") == "1") { ?><li <?php if($_SERVER['REQUEST_URI']=="/members.php?mf=li") echo 'class="active"';?>><a href="members.php?mf=li"><span>Affiliate Toolbox</span></a></li><? } ?>

<? if(get_setting("enablemenu_dlb") == "1") { ?><li <?php if($_SERVER['REQUEST_URI']=="/members.php?mf=dlb") echo 'class="active"';?>><a href="members.php?mf=dlb"><span>Downline Builder</span></a></li><? } ?>

<? if(get_setting("enablemenu_downloads") == "1") { ?><li <?php if($_SERVER['REQUEST_URI']=="/members.php?mf=md") echo 'class="active"';?>><a href="members.php?mf=md"><span>Downloads</span></a></li><? } ?>

          <li <?php if($_SERVER['PHP_SELF']=="/social_branding.php") echo 'class="active"';?>><a href="faqs.php"><span>Social Branding</span></a></li>


<? if(get_setting("enablemenu_ref") == "1") { ?><li <?php if($_SERVER['REQUEST_URI']=="/members.php?mf=mr") echo 'class="active"';?>><a href="members.php?mf=mr"><span>Referrals</span></a></li><? } ?>



<li class="heading">Miscellanous</li>

<li <?php if($_SERVER['PHP_SELF']=="/faqs.php") echo 'class="active"';?>><a href="faqs.php"><span>FAQs</span></a></li>

<? if(get_setting("enablemenu_forum") == "1") { ?><li><a href="/forum/"><span>Forum</span></a></li><? } ?>

<?php lfm_show_user_menu(); ?>

<? if(get_setting("enablemenu_logout") == "1") { ?><li><a href="members.php?mf=lo"><span>Logout</span></a></li><? } ?>
        </ul>
    </div>
</div>
 