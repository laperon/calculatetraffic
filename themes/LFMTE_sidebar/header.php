<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php lfm_sitename(); ?></title>
	<style type="text/css" media="screen">
		@import url( <?php echo $theme_dir.'/style.css'; ?> );
	</style>
	
<style type="text/css">

.rollover a {
display:block;
width: 150px;
padding:10px 0px 11px 10px;
font: bold 14px sans-serif;
color:#000000;
background: url("themes/LFMTE_sidebar/images/nav_normal.jpg") 0 0 no-repeat;
text-decoration: none;
}

.rollover a:hover {
width: 150px;
padding:10px 0px 11px 10px;
color:#FFFFFF;
background: url("themes/LFMTE_sidebar/images/nav_roll.jpg") 0 0 no-repeat;
}

.rollover ul {
margin:0;
list-style:none;
}

</style>
	
<?php lfm_head(); ?>
</head>

<body>
<div id="wrapper">
<div id="header">
	<h1 style="padding-left:220px;"></h1>
	<span id="description" style="padding-left:240px;"></span>
</div>

<table border="0" cellpadding="0" cellspacing="0" width="791">
<tr>
<td width="210" align="left" valign="top">

<!-- menu column -->

<div class="rollover">
<br /><br />
<ul>

<? if(isset($_SESSION["uid"]) && $_SESSION["uid"] != "") { ?>

<? if(get_setting("enablemenu_home") == "1") { ?><li><a href="members.php"><span>Home</span></a></li><? } ?>

<li><a href="surfing.php"><span>Surf</span></a></li><li><a href="claim.php"><span>Surfer Rewards</span></a></li>

<li><a href="mysites.php"><span>Sites</span></a></li>

<li><a href="mybanners.php"><span>Banners</span></a></li>

<li><a href="mytexts.php"><span>Text Ads</span></a></li>

<li><a href="autoassign.php"><span>Auto Assign</span></a></li>

<li><a href="buycredits.php"><span>Buy Credits</span></a></li><li><a href="surfcode.php"><span>Enter Surf Code</span></a></li>

<? if(get_setting("enablemenu_promote") == "1") { ?><li><a href="members.php?mf=li"><span>Affiliate Toolbox</span></a></li><? } ?>

<? if(get_setting("enablemenu_makemoney") == "1") { ?><li><a href="members.php?mf=mc"><span>Commissions</span></a></li><? } ?>

<? if(get_setting("enablemenu_ref") == "1") { ?><li><a href="members.php?mf=mr"><span>Referrals</span></a></li><? } ?>

<? if(get_setting("enablemenu_profile") == "1") { ?><li><a href="members.php?mf=pf"><span>Profile</span></a></li><? } ?>

<? if(get_setting("enablemenu_downloads") == "1") { ?><li><a href="members.php?mf=md"><span>Downloads</span></a></li><? } ?>

<? if(get_setting("enablemenu_dlb") == "1") { ?><li><a href="members.php?mf=dlb"><span>Downline Builder</span></a></li><? } ?>

<? if(get_setting("enablemenu_upgrade") == "1") { ?><li><a href="members.php?mf=ug"><span>Upgrade</span></a></li><? } ?>

<li><a href="faqs.php"><span>FAQs</span></a></li>

<? if(get_setting("enablemenu_forum") == "1") { ?><li><a href="/forum/"><span>Forum</span></a></li><? } ?>

<?php lfm_show_user_menu(); ?>

<? if(get_setting("enablemenu_logout") == "1") { ?><li><a href="members.php?mf=lo"><span>Logout</span></a></li><? } ?>

<? } else { ?>

<li><a href="index.php"><span>Home</span></a></li>

<li><a href="login.php"><span>Login</span></a></li>

<li><a href="signup.php"><span>Signup</span></a></li>

<li><a href="faqs.php"><span>FAQs</span></a></li>

<? } ?>

</ul>

</div>

<br /><br />

<p align="center"><a target="_blank" href="http://surfingguard.com"><img border="0" src="http://surfingguard.com/hcsgshield125.png" width="125" height="125"></a></p>

<!-- end menu column -->

</td><td width="581" align="center" valign="top">

<!-- end header -->
