<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

// VisuGraph JS 1.0
// �2012 Josh Abbott, http://visugraph.com
// Licensed for the LFMTE script

if (!isset($userid) || !is_numeric($userid)) {
	echo("Invalid Link");
	exit;
}

$livestats = @mysql_result(@mysql_query("SELECT value FROM `tracker_settings` WHERE field='livestats'"), 0);
if ($livestats == "1") {
	// Live Stats Enabled
}

// Check If Conversion Tracking Is Enabled
$getmtype = mysql_query("select mtype from ".$prefix."members where Id=$userid");
$acctype = mysql_result($getmtype, 0, "mtype");
$cnx = mysql_query("select rot_convtrk from ".$prefix."membertypes where mtid=$acctype");
$convtrk = mysql_result($cnx,0,'rot_convtrk');

if (isset($_GET['dailystats']) && is_numeric($_GET['dailystats'])) {
	$siteid = $_GET['dailystats'];	
} else {
	echo("Invalid Site ID");
	exit;
}

if ($_GET['rotpage'] == "trackers") {
	// Daily Tracker Stats
	$logtype = "tracker";
} elseif ($_GET['rotpage'] == "rotators") {
	// Daily Rotator Stats
	$logtype = "rotator";
} else {
	echo("Invalid Page");
	exit;
}

$currentdate = date("Y-m-d");
$startdate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));
$enddate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));
$starttime = mktime(0,0,0,substr($startdate,5,2),substr($startdate,8),substr($startdate,0,4));
$endtime = mktime(0,0,0,substr($enddate,5,2),substr($enddate,8),substr($enddate,0,4))+86399;

?>

<table width="560" border="0" cellpadding="0" cellspacing="5">

<tr><td align="left" valign="top">
	
	<table border=1 cellpadding=0 cellspacing=0 width=315 id="graphimg">
	<tr><td align="center" bgcolor="#EEEEEE"><span style="display: block; margin-bottom: 10px; margin-top: 10px;"><font size=4><b>Daily Results Graph</b></font></span></td></tr>
	<tr><td align="center">
		<br>
		<select name="datatype" id="datatype">
		<option value=1 selected>Hits Received</option>
		<option value=2>Unique Hits</option>
		<option value=3>Conversions</option>
		</select>
		<input type="hidden" name="siteid" id="siteid" value="<? echo($siteid); ?>">
		<input type="hidden" name="tabletype" id="tabletype" value="<? echo($logtype); ?>">
		<input type="hidden" name="sdate" id="DPC_sdate_YYYY-MM-DD" value="<? echo($startdate); ?>">
		<input type="hidden" name="edate" type id="DPC_edate_YYYY-MM-DD" value="<? echo($enddate); ?>">
		<button onclick="newGraph(document.getElementById('siteid').value, document.getElementById('datatype').value, document.getElementById('tabletype').value, (stimestamp+86400), (etimestamp+86400))">View</button>
		<br>
		
		<div id="graphdiv" style="width:300px;height:300px"></div>
		
		<table border=0 cellpadding=0 cellspacing=0>
			<tr>
				<td>&nbsp;</td>
				<td><input id="zoominbtn" type="button" OnClick="changeZoom(0, document.getElementById('siteid').value, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value="+"></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><input id="panleftbtn" type="button" OnClick="changePan(0, document.getElementById('siteid').value, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value="<"></td>
				<td><img id="loadingimg" src="graphblankimg.jpg"></td>
				<td><input id="panrightbtn" type="button" OnClick="changePan(1, document.getElementById('siteid').value, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value=">"></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input id="zoomoutbtn" type="button" OnClick="changeZoom(1, document.getElementById('siteid').value, document.getElementById('datatype').value, document.getElementById('tabletype').value, stimestamp, etimestamp)" value="-"></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<br>
	</td></tr></table>
	
</td>

<td align="left" valign="top">
	
	<table border=1 cellpadding=0 cellspacing=0 width="240">
	<tr><td align="center" bgcolor="#EEEEEE"><span style="display: block; margin-bottom: 10px; margin-top: 10px;" id="charttitle"><font size=4><b>Top Sources Today</b></font></span></td></tr>
	<tr><td align="left">
	
		<table width="240" border="1" bordercolor="gray" cellpadding="2" cellspacing="0" id="statstable">
		<thead>
			<tr>
				<td bgcolor="#EEEEEE"><font size=1>Source</font></td>
				<td bgcolor="#EEEEEE"><font size=1>Hits</font></td>
			</tr>
		</thead>
		<tbody>
		</tbody>
		</table>
	
	</td></tr></table>
	
	<br>
	<p id="chartdetails"><font size="1"><b>Hits</b> - The total number of hits your link received from that source.</font><br><br>
	<font size="1"><b>Uniq. Hits</b> - The number of individual people who viewed your link from that source.</font><br><br>
	<font size="1"><b>Max Hits</b> - The maximum number of times that the same person viewed your link from that source.</font><br><br>
	<font size="1"><b>Convs.</b> - The number of conversions for your link from that source.</font></p>
	
</td></tr>

</table>

<link href="trackerflotlayout.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript" src="flotjquery.js"></script>
<script language="javascript" type="text/javascript" src="jquery.flot.js"></script>
<script language="JavaScript" src="startxmlhttp.js"></script>

<script language="javascript">

// Connection Function
function ajax_connect(url) {

	graph_xmlhttp = start_xml_http();

	if(!graph_xmlhttp) {
		alert('Your browser is blocking or does not support Ajax');
		return false;
	}

	graph_xmlhttp.onreadystatechange = function() {
	if (graph_xmlhttp.readyState == 4) {
	
		graphresponse = graph_xmlhttp.responseText;
		
		// Pull The Start Date
		var startsdate = graphresponse.indexOf('#STARTSTARTDATE#') + 16;
		if (graphresponse.indexOf('#STARTSTARTDATE#') > -1) {
			var endsdate = graphresponse.indexOf('#ENDSTARTDATE#');
			var graphsdate = graphresponse.substring(startsdate, endsdate);	
			document.getElementById('DPC_sdate_YYYY-MM-DD').value = graphsdate;
			tempdateobj = new Date(graphsdate);
			stimestamp = (tempdateobj.getTime()) / 1000;
		}
		
		// Pull The End Date
		var startedate = graphresponse.indexOf('#STARTENDDATE#') + 14;
		if (graphresponse.indexOf('#STARTENDDATE#') > -1) {
			var endedate = graphresponse.indexOf('#ENDENDDATE#');
			var graphedate = graphresponse.substring(startedate, endedate);	
			document.getElementById('DPC_edate_YYYY-MM-DD').value = graphedate;
			tempdateobj = new Date(graphedate);
			etimestamp = (tempdateobj.getTime()) / 1000;	
		}
		
		// Pull The Graph Type
		var startgraphtype = graphresponse.indexOf('#STARTGRAPHTYPE#') + 16;
		if (graphresponse.indexOf('#STARTGRAPHTYPE#') > -1) {
			var endgraphtype = graphresponse.indexOf('#ENDGRAPHTYPE#');
			var graphgraphtype = graphresponse.substring(startgraphtype, endgraphtype);	
			if (graphgraphtype == 'monthly') {
				MonthlyGraph = true;
			} else {
				MonthlyGraph = false;
			}
		}
		
		// Pull The Graph Data Label
		var startdatalabel = graphresponse.indexOf('#STARTDATALABEL#') + 16;
		if (graphresponse.indexOf('#STARTDATALABEL#') > -1) {
			var enddatalabel = graphresponse.indexOf('#ENDDATALABEL#');
			var graphdatalabel = graphresponse.substring(startdatalabel, enddatalabel);	
		}
		
		// Pull The Graph Data
		var startdata = graphresponse.indexOf('#STARTDATA#') + 11;
		if (graphresponse.indexOf('#STARTDATA#') > -1) {
			var enddata = graphresponse.indexOf('#ENDDATA#');
			var graphdata = graphresponse.substring(startdata, enddata);
			
			// Break up the results and put them back in usable arrays
			var arraygraphdata = graphdata.split('|');
			var arrlength = arraygraphdata.length;
			var temparray = [];
			for (var i = 0; i < arrlength; i++) {
				temparray = arraygraphdata[i].split(',');
				arraygraphdata[i] = [temparray[0], temparray[1]];
			}
		} else {
			var arraygraphdata = [];
		}
		
		// Pull The Ticks Data
		var startticks = graphresponse.indexOf('#STARTTICKS#') + 12;
		if (graphresponse.indexOf('#STARTTICKS#') > -1) {
			var endticks = graphresponse.indexOf('#ENDTICKS#');
			var graphticks = graphresponse.substring(startticks, endticks);
			
			// Break up the results and put them back in usable arrays
			var arraygraphticks = graphticks.split('|');
			var arrlength = arraygraphticks.length;
			var temparray = [];
			for (var i = 0; i < arrlength; i++) {
				temparray = arraygraphticks[i].split(',');
				arraygraphticks[i] = [temparray[0], temparray[1]];
			}
			
		} else {
			var arraygraphticks = [];
		}
		
		// Pull The Max Stat (For Top Y Axis)
		var startmax = graphresponse.indexOf('#STARTMAX#') + 10;
		if (graphresponse.indexOf('#STARTMAX#') > -1) {
			var endmax = graphresponse.indexOf('#ENDMAX#');
			var graphmax = graphresponse.substring(startmax, endmax);		
		}
		
		// Pull The Zoom In Data
		var startzoomin = graphresponse.indexOf('#STARTZOOMIN#') + 13;
		if (graphresponse.indexOf('#STARTZOOMIN#') > -1) {
			var endzoomin = graphresponse.indexOf('#ENDZOOMIN#');
			var canzoomin = graphresponse.substring(startzoomin, endzoomin);
			if (canzoomin == "yes") {
				document.getElementById("zoominbtn").disabled = false;
			} else {
				document.getElementById("zoominbtn").disabled = true;
			}
		} else {
			document.getElementById("zoominbtn").disabled = true;
		}
		
		// Pull The Zoom Out Data
		var startzoomout = graphresponse.indexOf('#STARTZOOMOUT#') + 14;
		if (graphresponse.indexOf('#STARTZOOMOUT#') > -1) {
			var endzoomout = graphresponse.indexOf('#ENDZOOMOUT#');
			var canzoomout = graphresponse.substring(startzoomout, endzoomout);
			if (canzoomout == "yes") {
				document.getElementById("zoomoutbtn").disabled = false;
			} else {
				document.getElementById("zoomoutbtn").disabled = true;
			}	
		} else {
			document.getElementById("zoomoutbtn").disabled = true;
		}
		
		// Pull The Pan Left Data
		var startpanleft = graphresponse.indexOf('#STARTPANLEFT#') + 14;
		if (graphresponse.indexOf('#STARTPANLEFT#') > -1) {
			var endpanleft = graphresponse.indexOf('#ENDPANLEFT#');
			var canpanleft = graphresponse.substring(startpanleft, endpanleft);
			if (canpanleft == "yes") {
				document.getElementById("panleftbtn").disabled = false;
			} else {
				document.getElementById("panleftbtn").disabled = true;
			}		
		} else {
			document.getElementById("panleftbtn").disabled = true;
		}
		
		// Pull The Pan Right Data
		var startpanright = graphresponse.indexOf('#STARTPANRIGHT#') + 15;
		if (graphresponse.indexOf('#STARTPANRIGHT#') > -1) {
			var endpanright = graphresponse.indexOf('#ENDPANRIGHT#');
			var canpanright = graphresponse.substring(startpanright, endpanright);
			if (canpanright == "yes") {
				document.getElementById("panrightbtn").disabled = false;
			} else {
				document.getElementById("panrightbtn").disabled = true;
			}	
		} else {
			document.getElementById("panrightbtn").disabled = true;
		}
		
		document.getElementById("loadingimg").src = 'graphblankimg.jpg';
		
		var maxy = ceilpow10(graphmax);
		drawgraph(graphdatalabel, arraygraphdata, maxy, arraygraphticks);
	}
	}
	
	// Disable Buttons While The Graph Is Loading
	document.getElementById("zoominbtn").disabled = true;
	document.getElementById("zoomoutbtn").disabled = true;
	document.getElementById("panleftbtn").disabled = true;
	document.getElementById("panrightbtn").disabled = true;
	
	document.getElementById("loadingimg").src = 'graphloading.gif';

	graph_xmlhttp.open('GET',url,true);
	graph_xmlhttp.send(null);
}

function newGraph(siteid, datatype, tabletype, starttime, endtime) {

	// Create Dates From Time Stamps
	startdateobj = new Date(starttime*1000);
	startyear = startdateobj.getFullYear();
	startmonth = (startdateobj.getMonth())+1;
	startday = startdateobj.getDate();
	startdate = startyear + '-' + startmonth + '-' + startday;
	enddateobj = new Date(endtime*1000);
	endyear = enddateobj.getFullYear();
	endmonth = (enddateobj.getMonth())+1;
	endday = enddateobj.getDate();
	enddate = endyear + '-' + endmonth + '-' + endday;
	
	if (startdate==enddate) {
		return 0;
	}
	
	if (endtime < starttime) {
		return 0;
	}
	
	numdays = countDays(startdate, enddate);
	
	if (numdays <= 0) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "trackergraphs_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&charttype=" + datatype + "&tabletype=" + tabletype + "&siteid=" + siteid;
	ajax_connect(graphurl);

}

function clickZoomIn(siteid, datatype, tabletype, starttime, endtime, clickx) {
	
	timedistance = endtime - starttime;
	distancefourths = Math.round(timedistance/4);
	
	// Last Center Time
	centertime = (starttime+endtime)/2;
	
	// Unix Timestamp Of Clicked Date
	clickx = clickx/1000;
	
	if (timedistance < 604800) {
		// Change table to clicked date
		dateobj = new Date((clickx+40000)*1000);
		clickxyear = dateobj.getFullYear();
		clickxmonth = dateobj.getMonth() + 1;
		clickxday = dateobj.getDate();
		
		if (clickxmonth < 10) {
			clickxmonth = "0" + clickxmonth;
		}
		
		if (clickxday < 10) {
			clickxday = "0" + clickxday;
		}
		
		clickxfulldate = clickxyear + "-" + clickxmonth + "-" + clickxday;
		
		detailed_stats(clickxfulldate, ((clickx+40000)*1000));
		return 1;
	}
	
	// Number Of Seconds From The Center Date
	clickx = Math.round(clickx - centertime);
	
	// Center To The Clicked Point
	starttime = starttime + clickx;
	endtime = starttime + timedistance;
	
	// Zoom Into Clicked Point
	if (document.getElementById("zoominbtn").disabled == false) {
		starttime = starttime + distancefourths;
		endtime = starttime + (distancefourths*2);
	}
	
	if ((endtime - starttime) < 86400) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "trackergraphs_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&charttype=" + datatype + "&tabletype=" + tabletype + "&siteid=" + siteid;
	ajax_connect(graphurl);

}

function changeZoom(zoom, siteid, datatype, tabletype, starttime, endtime) {
	
	timedistance = endtime - starttime;
	distancefourths = Math.round(timedistance/4);
	
	if (zoom == 0 && document.getElementById("zoominbtn").disabled == false) {
		// Zoom In
		starttime = starttime + distancefourths + 86400;
		endtime = starttime + (distancefourths*2);
	} else if (document.getElementById("zoomoutbtn").disabled == false) {
		// Zoom Out
		starttime = starttime - (distancefourths*2) + 86400;
		endtime = starttime + (distancefourths*8);
	} else {
		// Zoom Disabled
		return 0;
	}
	
	if ((endtime - starttime) < 86400) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "trackergraphs_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&charttype=" + datatype + "&tabletype=" + tabletype + "&siteid=" + siteid;
	ajax_connect(graphurl);
	
}

function changePan(direction, siteid, datatype, tabletype, starttime, endtime) {
	
	timedistance = endtime - starttime;
	
	if (timedistance < 1209600) {
		pandistance = 86400/2;
		rightpandistance = 86400*2;
	} else if (timedistance < 3456000) {
		pandistance = Math.round(timedistance/8);
		rightpandistance = pandistance*1.5;
	} else {
		pandistance = Math.round(timedistance/3);
		rightpandistance = pandistance;
	}
	
	if (direction == 0 && document.getElementById("panleftbtn").disabled == false) {
		// Pan Left
		starttime = starttime - pandistance;
		endtime = starttime + timedistance;
	} else if (document.getElementById("panrightbtn").disabled == false) {
		// Pan Right
		starttime = starttime + rightpandistance;
		endtime = starttime + timedistance;
	} else {
		// Pan Disabled
		return 0;
	}
	
	if ((endtime - starttime) < 86400) {
		return 0;
	}
	
	// Connect To Graph
	graphurl = "trackergraphs_ajax.php?starttime=" + starttime + "&endtime=" + endtime + "&charttype=" + datatype + "&tabletype=" + tabletype + "&siteid=" + siteid;
	ajax_connect(graphurl);
	
}

function countDays(firstdate, lastdate) {

	daycount = 0;
	
	d = firstdate.match(/\d+/g);
	firstdatetime = new Date(d[0], d[1] - 1, d[2]).getTime();

	d = lastdate.match(/\d+/g);
	lastdatetime = new Date(d[0], d[1] - 1, d[2]).getTime();

	currdate = firstdatetime;
	
	while (currdate <= lastdatetime) {
		daycount++;
		currdate = currdate + 86400000;
	}
	return daycount;
}

function ceilpow10(val) {
	return Math.ceil(val/10)*10;
}

</script>


<script language="JavaScript" src="stattablefuncs.js"></script>

<script type="text/javascript">
	
	// Stats Functions
	
	// Pull Data Vars
	var xmlhttplive = false;
	var newDate = false
	var milliCount = 0;
	var statsURL = "trackstatsupdate.php?siteid=<? echo($siteid); ?>&type=<? echo($logtype); ?>";
	var buildURL = "trackstatsbuild.php?siteid=<? echo($siteid); ?>&type=<? echo($logtype); ?>";
	var detailsURL = "trackstatsdetailed.php?siteid=<? echo($siteid); ?>&type=<? echo($logtype); ?>";
	
	// Live Counter Vars
	var delaymilli = 5000;
	var countdiff = 0;
	var lastconnecttime = 0;
	var livestatsactive = 0;
	var livestatssetting = <? echo($livestats); ?>;
	
	var sourcesarray = [];
	var statsarray = [];
	
	var updatedsources = [];
	var updatedstatsarray = [];
	
	// Build Initial Table Function
	function build_stats(url) {
		
		lastconnecttime = 0;
		sourcesarray = [];
		statsarray = [];
		updatedsources = [];
		updatedstatsarray = [];
		
		xmlhttplive = start_xml_http();
		
		if(!xmlhttplive) {
			alert('Your browser is blocking or does not support Ajax');
			return false;
		}
		
		xmlhttplive.onreadystatechange = function() {
			if (xmlhttplive.readyState == 4) {
				var buildresponse = xmlhttplive.responseText;
				
				var arraydata = buildresponse.split('|');
				var arrlength = arraydata.length;
				
				// The First Element Will Be The Server Time
				lastconnecttime = parseInt(arraydata[0]);
				
				// Loop Through Each Source
				if (arrlength > 1) {
					for (var i = 1; i < arrlength-1; i++) {
						var tempsourcedata = String(arraydata[i]);
						var splitsourcedata = tempsourcedata.split(',');
						var newsourcename = String(splitsourcedata[0]);
						
						if (newsourcename.length > 18) {
							newsourcename = newsourcename.substring(0,15) + '...';
						}
						
						var newsourcestat = parseInt(splitsourcedata[1]);
						sourcesarray.push(newsourcename);
						statsarray.push(newsourcestat);
						updatedstatsarray.push(newsourcestat);
						addSource(newsourcename, newsourcestat);
					}
				}
				
				if (livestatssetting == 1) {
					// Start The Regular Updates
					newDate = new Date();
					milliCount = newDate.getTime();
					setTimeout("pull_stream('" + statsURL + "&lc=" + lastconnecttime + "&m=" + milliCount + "')", delaymilli);
				}
				
			}
		}
		
		xmlhttplive.open('GET',url,true);
		xmlhttplive.send(null);
	}
	
	// Update Connection Function
	function pull_stream(url) {
		
		xmlhttplive = start_xml_http();
		
		if(!xmlhttplive) {
			alert('Your browser is blocking or does not support Ajax');
			return false;
		}
		
		xmlhttplive.onreadystatechange = function() {
		if (xmlhttplive.readyState == 4) {
			if (livestatsactive == 1) {
				update_stats(xmlhttplive.responseText);
			}
			}
		}
		
		xmlhttplive.open('GET',url,true);
		xmlhttplive.send(null);
	}
	
	// Update Stats Function
	function update_stats(data) {
		
		updatedsources = [];
		
		// Split Each Update
		var arraydata = data.split('|');
		var arrlength = arraydata.length;
		
		// The First Element Will Be The Server Time
		lastconnecttime = parseInt(arraydata[0]);
		
		// Loop Through Each Update
		if (arrlength > 1) {
			var tempsourcename = 0;
			var tempsourcepos = 0;
			for (var i = 1; i < arrlength-1; i++) {
				tempsourcename = String(arraydata[i]);
				
				if (tempsourcename.length > 18) {
					tempsourcename = tempsourcename.substring(0,15) + '...';
				}
				
				tempsourcepos = sourcesarray.indexOf(tempsourcename);
				if (tempsourcepos != -1) {
					// This source is already in the array and needs to be updated
					if (updatedsources.indexOf(tempsourcepos) == -1) { updatedsources.push(tempsourcepos); }
					updatedstatsarray[tempsourcepos] = updatedstatsarray[tempsourcepos] + 1;
				} else {
					// Add source to arrays and table
					sourcesarray.push(tempsourcename);
					statsarray.push(1);
					updatedstatsarray.push(1);
					addSource(tempsourcename, "1");
				}
			}
		}

		// Increment The Counter For Each Source That Has Just Been Updated
		arrlength = updatedsources.length;
		if (arrlength > 0) {
			tempsourcename = 0;
			tempsourcepos = 0;
			var tempoldstat = 0;
			var tempnewstat = 0;
			for (var i = 0; i < arrlength; i++) {
				tempsourcepos = updatedsources[i];
				tempsourcename = sourcesarray[tempsourcepos];
				tempoldstat = statsarray[tempsourcepos];
				tempnewstat = updatedstatsarray[tempsourcepos];
				
				// Start The Increment Process
				countdiff = tempnewstat-tempoldstat;
				if (countdiff > 0) {
					speedval = delaymilli/countdiff;
					increase_counter(tempsourcename, tempoldstat, tempnewstat, speedval);
					statsarray[tempsourcepos] = updatedstatsarray[tempsourcepos];
				}
			}
			sortTable(1, "desc");
		}
		
		if (livestatsactive == 1) {
			// Start The Timer To Pull In The Next Update
			newDate = new Date();
			milliCount = newDate.getTime();
			setTimeout("pull_stream('" + statsURL + "&lc=" + lastconnecttime + "&m=" + milliCount + "')", delaymilli);
		}
		
	}

	// Increment Counter Function
	function increase_counter(incsourcename, currentval, endval, speedval) {
		if (currentval < endval) {
			currentval = currentval+1;
			setStat("stat" + incsourcename, currentval);
			setTimeout("increase_counter('" + incsourcename + "', " + currentval + ", " + endval + ", " + speedval + ")", speedval);
		}
	}
	
	// Start Live Stats Function
	function start_live_stats() {
		document.getElementById("charttitle").innerHTML = '<font size=4><b>Top Sources Today</b></font>';
		document.getElementById('chartdetails').style.visibility = "hidden";
		createLiveTable();
		livestatsactive = 1;
		newDate = new Date();
		milliCount = newDate.getTime();
		build_stats(buildURL + "&m=" + milliCount);
	}
	
	// Pull Detailed Stats Function
	function detailed_stats(date, datetimestamp) {
		
                    var titlemonths = new Array();
                    titlemonths[0] = "Jan";
                    titlemonths[1] = "Feb";
                    titlemonths[2] = "Mar";
                    titlemonths[3] = "Apr";
                    titlemonths[4] = "May";
                    titlemonths[5] = "Jun";
                    titlemonths[6] = "Jul";
                    titlemonths[7] = "Aug";
                    titlemonths[8] = "Sep";
                    titlemonths[9] = "Oct";
                    titlemonths[10] = "Nov";
                    titlemonths[11] = "Dec";
                    
                    var titledateobj = new Date(datetimestamp);
                    var titleyear = titledateobj.getFullYear();
                    var titlemonth = titlemonths[titledateobj.getMonth()];
                    var titleday = titledateobj.getDate();
                    var titletext = titlemonth + " " + titleday + " " + titleyear;
		
		document.getElementById("charttitle").innerHTML = '<font size=4><b>Stats For ' + titletext + '</b></font>';
		document.getElementById('chartdetails').style.visibility = "visible";
		livestatsactive = 0;
		createDetailsTable();
		
		xmlhttpdetails = start_xml_http();
		
		if(!xmlhttpdetails) {
			alert('Your browser is blocking or does not support Ajax');
			return false;
		}
		
		xmlhttpdetails.onreadystatechange = function() {
			if (xmlhttpdetails.readyState == 4) {
				var buildresponse = xmlhttpdetails.responseText;
				
				if (buildresponse.indexOf('|') > -1) {
					
					var arraydata = buildresponse.split('|');
					var arrlength = arraydata.length;
					
					// Loop Through Each Source
					if (arrlength > 0) {
						for (var i = 0; i+1 < arrlength; i++) {
							var tempsourcedata = String(arraydata[i]);
							var splitsourcedata = tempsourcedata.split(',');
							
							var newsourcename = String(splitsourcedata[0]);
							var newsourcenhits = parseInt(splitsourcedata[1]);
							var newsourceuhits = parseInt(splitsourcedata[2]);
							var newsourceconvs = parseInt(splitsourcedata[3]);
							var newsourcemhits = parseInt(splitsourcedata[4]);
							
							if (newsourcename.length > 18) {
								newsourcename = newsourcename.substring(0,15) + '...';
							}
							
							addSourceDetailed(newsourcename, newsourcenhits, newsourceuhits, newsourceconvs, newsourcemhits);
						}
					}
				
				} else {
					addDetailedText(buildresponse);
				}
			}
		}
		
		newDate = new Date();
		milliCount = newDate.getTime();
		xmlhttpdetails.open('GET',detailsURL + "&date=" + date + "&m=" + milliCount,true);
		xmlhttpdetails.send(null);
		
	}

</script>

<script type="text/javascript">
	
	// Set Initial VisuGraph Values
	var ShowTooltips = true;
	var MonthlyGraph = false;
	var stimestamp = <? echo($starttime); ?>;
	var etimestamp = <? echo($endtime); ?>;
	
</script>

<script language="javascript" type="text/javascript" src="trackerflotfunctions.js"></script>

<script type="text/javascript">
	
	// Draw The Initial Graph
	<? echo("newGraph(".$siteid.", 1, '".$logtype."', ".$starttime.", ".$endtime.");"); ?>
	
	// Start Live Stats
	start_live_stats();


</script>


<?

include $theme_dir."/footer.php";
exit;

?>