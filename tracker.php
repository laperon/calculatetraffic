<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";

include "inc/config.php";
$dbconnectlink = mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die("Unable to select database");

// Make Sure The Tracker Is Valid
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
	$trackerid = $_GET['id'];
} elseif (isset($_GET['trkname']) && strlen($_GET['trkname']) > 0) {
	$_GET['trkname'] = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $_GET['trkname']));
	$gettrkid = mysql_query("SELECT id FROM `tracker_urls` WHERE name='".$_GET['trkname']."' LIMIT 1") or die(mysql_error());
	if (mysql_num_rows($gettrkid) < 1) {
		echo("Tracker Name Not Found");
		exit;
	}
	$trackerid = mysql_result($gettrkid, 0, "id");
} else {
	echo("Invalid Tracker ID.");
	exit;
}

// Get The Rotator ID If Used
if (isset($_GET['rotatorid']) && is_numeric($_GET['rotatorid']) && $_GET['rotatorid'] > 0) {
	$rotatorid = $_GET['rotatorid'];
} else {
	$rotatorid = 0;
}

$getsite = mysql_query("SELECT user_id, name, url, sourceranks, autoconv FROM `tracker_urls` WHERE id='".$trackerid."' AND state=0 LIMIT 1");
if (mysql_num_rows($getsite) < 1) {
	echo("Tracker ID Not Found.");
	exit;
}

$ownerid = mysql_result($getsite, 0, "user_id");
$trackername = mysql_result($getsite, 0, "name");
$siteurl = mysql_result($getsite, 0, "url");
$sourceranks = mysql_result($getsite, 0, "sourceranks");
$autoconv = mysql_result($getsite, 0, "autoconv");

if ($autoconv == "1") {
	// Automatically append the domain and tracker ID for sites that support auto conversion tracking
	if (strpos($siteurl, '?') === false) {
		$siteurl = $siteurl."?srtrkdm=".$_SERVER["SERVER_NAME"]."&srtrkid=".$trackerid;
	} else {
		$siteurl = $siteurl."&srtrkdm=".$_SERVER["SERVER_NAME"]."&srtrkid=".$trackerid;
	}
}

// Get The IP Address And Table
$ipaddress = $_SERVER['REMOTE_ADDR'];
if (!preg_match('/^([0-9]{1,3})\.([0-9]{1,3})\.' . '([0-9]{1,3})\.([0-9]{1,3})$/', $ipaddress, $range)) {
	echo("Unable to verify your IP Address.");
	exit;
}

// Get The Source URL
$sourceid = 0;
$source = "";
if (isset($_GET['srcid']) && is_numeric($_GET['srcid'])) {
	$getsourceinfo = mysql_query("SELECT url FROM `tracker_sources` WHERE id='".$_GET['srcid']."' LIMIT 1");
	if (mysql_num_rows($getsourceinfo) > 0) {
		$source = mysql_result($getsourceinfo, 0, "url");
		$sourceid = $_GET['srcid'];
	}
} elseif (isset($_GET['srcname']) && strlen($_GET['srcname']) > 0) {
	$_GET['srcname'] = preg_replace('/[^A-Za-z0-9]/', '', $_GET['srcname']);
	$source = $_GET['srcname'];
} else {
	$sourceurl = $_SERVER['HTTP_REFERER'];
	$splitsource = explode("/", $sourceurl);
	$source = $splitsource[2];
	$source = str_ireplace("www.", "", $source);
}

if ($source != "") {
	
	if ($sourceid == 0) {
		$getsourceid = mysql_query("SELECT id FROM `tracker_sources` WHERE url='".$source."' LIMIT 1");
		if (mysql_num_rows($getsourceid) > 0) {
			$sourceid = mysql_result($getsourceid, 0, "id");
		} else {
			// Add New Source
			mysql_query("INSERT INTO `tracker_sources` (url) VALUES ('".$source."')") or die(mysql_error());
			$newsource = mysql_insert_id($dbconnectlink);
			$sourceid = $newsource;
		}
	}
	
	$livestats = @mysql_result(@mysql_query("SELECT value FROM `tracker_settings` WHERE field='livestats'"), 0);
	if ($livestats == "1" && strlen($source) > 0) {
		@mysql_query("INSERT INTO `tracker_livestats` (stattime, user_id, tracker_id, statdata) VALUES ('".time()."', '".$ownerid."', '".$trackerid."', '".$source."')");
	}
	
}

$checklink = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_trackdata` WHERE user_id='".$ownerid."' AND rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."'"), 0);
if ($checklink > 0) {
	mysql_query("UPDATE `tracker_trackdata` SET lasttime='".time()."', nhits=nhits+1 WHERE user_id='".$ownerid."' AND rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."' LIMIT 1") or die(mysql_error());
} else {
	mysql_query("INSERT INTO `tracker_trackdata` (lasttime, user_id, rotator_id, tracker_id, source_id, nhits) VALUES ('".time()."', '".$ownerid."', '".$rotatorid."', '".$trackerid."', '".$sourceid."', '1')") or die(mysql_error());
}

// Add/Update IP Log
$currentdate = date("Y-m-d");

$checklog = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_iplog` WHERE date='".$currentdate."' AND ipaddress='".$ipaddress."' AND rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."'"), 0);
if ($checklog > 0) {
	mysql_query("UPDATE `tracker_iplog` SET lasttime='".time()."', nhits=nhits+1 WHERE date='".$currentdate."' AND ipaddress='".$ipaddress."' AND rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."' LIMIT 1") or die(mysql_error());
} else {
	mysql_query("INSERT INTO `tracker_iplog` (date, lasttime, ipaddress, rotator_id, tracker_id, source_id, nhits) VALUES ('".$currentdate."', '".time()."', '".$ipaddress."', '".$rotatorid."', '".$trackerid."', '".$sourceid."', '1')") or die(mysql_error());
}

if ($sourceranks == 1) {
	$showrate = "yes";
} else {
	$showrate = "no";
}

// Check If IP Is Banned From Source Ranks
if ($showrate == "yes") {
	$checkbanlist = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_banip` WHERE ipaddress='".$ipaddress."'"), 0);
	if ($checkbanlist > 0) {
		//$banreason = mysql_result(mysql_query("SELECT reason FROM `tracker_banip` WHERE ipaddress='".$ipaddress."'"), 0);
		//echo("This IP address is banned. ".$banreason);
		$showrate = "no";
	}
}

// Check If The User Already Rated This Site
if ($showrate == "yes") {
	$checkuser = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_rates` WHERE ipaddress='".$ipaddress."' AND tracker_id='".$trackerid."'"), 0);
	if ($checkuser > 0) {
		$showrate = "no";
	}
}

// Check If The Owner Has Exceeded Prize Limit
if ($showrate == "yes") {
	$getowner = mysql_query("SELECT a.rankstoday AS rankstoday, b.rot_sr_enable AS rot_sr_enable, b.rot_sr_dailyrates AS rot_sr_dailyrates FROM `".$prefix."members` a LEFT JOIN ".$prefix."membertypes b ON (a.mtype=b.mtid) where a.Id='".$ownerid."'");
	$ownerrankstoday = mysql_result($getowner, 0, "rankstoday");
	$rot_sr_enable = mysql_result($getowner, 0, "rot_sr_enable");
	$rot_sr_dailyrates = mysql_result($getowner, 0, "rot_sr_dailyrates");
	if ($rot_sr_enable != "1" || $ownerrankstoday >= $rot_sr_dailyrates) {
		$showrate = "no";
	}
}

// Check If The User Already Downloaded All Prizes
if ($showrate == "yes") {
	$highestprize = mysql_result(mysql_query("SELECT id FROM `tracker_prizes` ORDER BY id DESC LIMIT 1"), 0);
	$getuserprize = mysql_query("SELECT prizeid FROM `tracker_prizeswon` WHERE ipaddress='".$ipaddress."' ORDER BY prizeid DESC LIMIT 1");
	if (mysql_num_rows($getuserprize) < 1) {
		$lastuserprize = 0;
	} else {
		$lastuserprize = mysql_result($getuserprize, 0, "prizeid");
	}
	
	if ($lastuserprize >= $highestprize) {
		$showrate = "no";
	}
}


if ($showrate == "yes") {
	
	// Update Source Hits
	@mysql_query("UPDATE `tracker_trackdata` SET ratehits=ratehits+1 WHERE user_id='".$ownerid."' AND rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."' LIMIT 1");
	
	// Create Verification Data
	$verifynum = rand(1, 9999);
	$verifycode = md5(md5($verifynum));
	$currentdate = date("Y-m-d");
	@mysql_query("INSERT INTO `tracker_verify` (date, user_id, rotator_id, tracker_id, source_id, ipaddress, verifycode) values ('".$currentdate."', '".$ownerid."', '".$rotatorid."', '".$trackerid."', '".$sourceid."', '".$ipaddress."', '".$verifycode."')");

	// Start HTML Output
?>
<html>
<head>

<title><? echo($trackername); ?></title>

<style type='text/css'>
#floater {
  position:absolute; visibility:hidden;
  width:205px; height:auto;
  color:#330;
  margin:0; padding:0px;
  border:2px solid #330;
  border-top:2px solid #330;
  background:#FFFFFF;
}
a.m {
  display:block; width:100%;
  margin:.5em 0;
}
</style>
<script type='text/javascript' src='/xlib/x.js'></script>
<script type='text/javascript' src='/xlib/xslideto.js'></script>
<script type='text/javascript' src='/xlib/xvisibility.js'></script>
<script type='text/javascript' src='/srfloat.js'></script>

<script type='text/javascript'>

var maxhtml = '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="left" bgcolor="blue"><font face="arial" color="white" size="2"><b>SourceRanks</b></font></td><td align="right" bgcolor="blue"><a onclick="minFloat();"><img height="20" width="20" border="0" src="/srmin.jpg"></a><a onclick="closeFloat();"><img height="20" width="20" border="0" src="/srclose.jpg"></a></td></tr><tr><td align="center" colspan="2"><p><font size="5"><b>Rate This Page</b></font><br /><font size="2"><b>Get An Instant Download Prize</b></font></p><form target="_blank" style="margin:0px" action="/ratesend.php" method="post"><table border="0" cellpadding="0" cellspacing="5" width="100%"><tr><td align="right">Rate: </td><td align="left"><select name="rate"><option value="0">Click to Select</option><option value="1">1 - Poor</option><option value="2">2 - Fair</option><option value="3">3 - Good</option><option value="4">4 - Great</option><option value="5">5 - Excellent</option></select></td></tr></table><br />Comments:<br /><textarea onkeypress="return imposeMaxLength(event, this, 80);" name="comments" cols="20" rows="5"></textarea><br /><br /><input type="hidden" name="verifycode" value="<? echo($verifycode); ?>"><input border="0" type="image" src="/srgetprize.jpg"></form></td></tr></td></tr></table>';

var minhtml = '<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr><td align="left" bgcolor="blue"><font face="arial" color="white" size="2"><b>SourceRanks</b></font></td><td align="right" bgcolor="blue"><a onclick="maxFloat();"><img height="20" width="20" border="0" src="/srmax.jpg"></a><a onclick="closeFloat();"><img height="20" width="20" border="0" src="/srclose.jpg"></a></td></tr></table>';

</script>

<script language="javascript" type="text/javascript">
<!--
function imposeMaxLength(Event, Object, MaxLen)
{
  return ((Object.value.length <= MaxLen) ||(Event.keyCode == 8));
}
-->
</script>

</head>

<body bgcolor="black" leftmargin="0px" topmargin="0px" marginwidth="0px" marginheight="0px">

<div id='floater'>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td align="left" bgcolor="blue"><font face="arial" color="white" size="2"><b>SourceRanks</b></font></td>
<td align="right" bgcolor="blue"><a onclick="minFloat();"><img height="20" width="20" border="0" src="/srmin.jpg"></a><a onclick="closeFloat();"><img height="20" width="20" border="0" src="/srclose.jpg"></a></td></tr>

<tr><td align="center" colspan="2">

<p><font size="5"><b>Rate This Page</b></font><br />
<font size="2"><b>Get An Instant Download Prize</b></font></p>

<form target="_blank" style="margin:0px" action="/ratesend.php" method="post">

<table border="0" cellpadding="0" cellspacing="5" width="100%">
<tr><td align="right">Rate: </td>
<td align="left"><select name="rate">
<option value="0">Click to Select</option>
<option value="1">1 - Poor</option>
<option value="2">2 - Fair</option>
<option value="3">3 - Good</option>
<option value="4">4 - Great</option>
<option value="5">5 - Excellent</option>
</select>
</td></tr>
</table>

<br />

Comments:<br />
<textarea onkeypress="return imposeMaxLength(event, this, 80);" name="comments" cols="20" rows="5"></textarea>

<br /><br />

<input type="hidden" name="verifycode" value="<? echo($verifycode); ?>">

<input border="0" type="image" src="/srgetprize.jpg">

</form>

</td></tr>

</td></tr>
</table>

</div>

<iframe height="100%" width="100%" src="<? echo($siteurl); ?>">

</body>

</html>

<?php

} else {
	header("Location:".$siteurl);
}

exit;

?>