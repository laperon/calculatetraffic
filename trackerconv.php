<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";

include "inc/config.php";
$dbconnectlink = mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die("Unable to select database");

// Converts the hex color into the rgb that the GD Library understands
function imghexrgb($hexstr) {
	$hexstr = str_replace("#", "", $hexstr);
	$int = hexdec($hexstr);
	return array("red" => 0xFF & ($int >> 0x10), "green" => 0xFF & ($int >> 0x8), "blue" => 0xFF & $int);
}

// Creates the output image for the conversion tracker
function output_img($color, $text="") {
	$bgrgb = imghexrgb($color);
	$imgbase = imagecreatetruecolor(200, 200);
	$bg_color = imagecolorallocate($imgbase, $bgrgb['red'], $bgrgb['green'], $bgrgb['blue']);
	imagefill($imgbase, 0, 0, $bg_color);
	
	if (strlen($text) > 0) {
		// Text will always be black regardless of image color, but the text is only intended for debugging
		$text_color = imagecolorallocate($imgbase, 0, 0, 0);
		imagestring($imgbase, 3, 5, 50, $text, $text_color);
	}
	
	// Output The Image
	header("Content-Type: image/jpeg");
	imagejpeg($imgbase,NULL,85);
	imagedestroy($imgbase);
}

// Support custom colors for the blank image
if (isset($_GET['imgcolor']) && strlen($_GET['imgcolor']) > 0) {
	$_GET['imgcolor'] = str_replace("#", "", $_GET['imgcolor']);
	$_GET['imgcolor'] = strtoupper($_GET['imgcolor']);
	$_GET['imgcolor'] = preg_replace('/[^A-Za-z0-9]/', '', $_GET['imgcolor']);
	if (strlen($_GET['imgcolor']) == 6) {
		$imgcolor = $_GET['imgcolor'];
	} else {
		$imgcolor = "FFFFFF";
	}
} else {
	$imgcolor = "FFFFFF";
}

// Make Sure The Tracker Is Valid
if (isset($_GET['id']) && is_numeric($_GET['id'])) {
	$trackerid = $_GET['id'];
} else {
	output_img($imgcolor, "Invalid Tracker ID.");
	exit;
}

$getsite = mysql_query("SELECT a.user_id AS user_id, c.rot_convtrk AS convtrk FROM `tracker_urls` a LEFT JOIN ".$prefix."members b ON (a.user_id=b.Id) LEFT JOIN ".$prefix."membertypes c ON (b.mtype=c.mtid) WHERE a.id='".$trackerid."'") or die(mysql_error());
if (mysql_num_rows($getsite) < 1) {
	output_img($imgcolor, "Tracker ID Or User Not Found.");
	exit;
}

$ownerid = mysql_result($getsite, 0, "user_id");
$convtrk = mysql_result($getsite, 0, "convtrk");

if ($convtrk != "1") {
	output_img($imgcolor, "Conversion Tracking Disabled For This Account Level");
	exit;
}

// Get The IP Address And Table
$ipaddress = $_SERVER['REMOTE_ADDR'];
if (!preg_match('/^([0-9]{1,3})\.([0-9]{1,3})\.' . '([0-9]{1,3})\.([0-9]{1,3})$/', $ipaddress, $range)) {
	output_img($imgcolor, "Unable to verify user IP Address.");
	exit;
}

// Check The Last Hit Within The Past 10 Minutes
$tenminago = time() - 600;
$checklog = mysql_query("SELECT date, rotator_id, source_id FROM `tracker_iplog` WHERE ipaddress='".$ipaddress."' AND tracker_id='".$trackerid."' AND lasttime >= '".$tenminago."' ORDER BY lasttime DESC LIMIT 1") or die(mysql_error());
if (mysql_num_rows($checklog) > 0) {
	// Hit Found
	$hitdate = mysql_result($checklog, 0, "date");
	$rotatorid = mysql_result($checklog, 0, "rotator_id");
	$sourceid = mysql_result($checklog, 0, "source_id");
	$checkexists = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_conversions` WHERE ipaddress='".$ipaddress."' AND tracker_id='".$trackerid."'"), 0);
	if ($checkexists == 0) {
		mysql_query("INSERT INTO `tracker_conversions` (date, ipaddress, rotator_id, tracker_id, source_id) VALUES ('".$hitdate."', '".$ipaddress."', '".$rotatorid."', '".$trackerid."', '".$sourceid."')") or die(mysql_error());
		mysql_query("UPDATE `tracker_trackdata` SET nconvs=nconvs+1 WHERE user_id='".$ownerid."' AND rotator_id='".$rotatorid."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."' LIMIT 1") or die(mysql_error());
	}
}

// Output A Blank Image
output_img($imgcolor);

exit;

?>