<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$countcredits = mysql_query("select credits, mtype from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");

$getuserdata = mysql_query("Select email, mtype, joindate from ".$prefix."members where Id=$userid");
$useremail = mysql_result($getuserdata, 0, "email");
$mtype = mysql_result($getuserdata, 0, "mtype");
$joindate = mysql_result($getuserdata, 0, "joindate");

if (!isset($_GET['trackerid']) || !is_numeric($_GET['trackerid'])) {
	echo("Invalid Tracker ID");
	exit;
} else {
	$trackerid = $_GET['trackerid'];
}

// Validate Tracker Belongs To User
$checktracker = mysql_query("SELECT url FROM `tracker_urls` WHERE user_id='".$userid."' AND id='".$trackerid."'") or die(mysql_error());
if (mysql_num_rows($checktracker) < 1) {
	echo("Invalid Tracker");
	exit;
}
$trackerurl = mysql_result($checktracker, 0, "url");

// Check If AutoTrack Is Enabled

$urlautotrack = 0;

if (strpos($trackerurl, '?') === false) {
	$checkurl = $trackerurl."?srtrkck=1";
} else {
	$checkurl = $trackerurl."&srtrkck=1";
}

$urlinfo = parse_url($checkurl);
$hostname = $urlinfo['host'];
if ($hostname == "" || gethostbyname($hostname) == $hostname) {
	// Could Not Connect
	$urlautotrack = 0;
} else {
	$port = 80;
	$timeout = 5;
	$headers = "GET ".$checkurl." HTTP/1.1\r\n";
	$headers .= "Host: ".$hostname."\r\n";
	$headers .= "Referer: http://".$_SERVER["SERVER_NAME"]."\r\n";
	$headers .= "User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\r\n";
	$headers .= "Connection: close\r\n";
	$headers .= "Accept: */*\r\n";
	$headers .= "\r\n";
	$checkreturn = "";
	$attempts = 0;
	while((strpos($checkreturn, "200 OK") === false) && (strpos($checkreturn, "302 Found") === false) && (strpos($checkreturn, "srtrkvalid") === false) && ($attempts<2)) {
		$connection = fsockopen($hostname, $port, $errno, $errstr, $timeout);
		if ($connection) {
			stream_set_timeout($connection, $timeout);
			fwrite($connection,$headers);
			//Read the reply
			$checkreturn = "";
			$readruns = 0;
			while (!feof($connection) && !$checktimeout['timed_out'] && $readruns < 50) {
				$checkreturn .= fread($connection, 10000);
				$checktimeout = stream_get_meta_data($connection);
				$readruns++;
			}
			fclose($connection);
		}
		$attempts = $attempts+1;
	}
	
	if (strpos($checkreturn, "srtrkvalid") === false) {
		// Auto Tracking Not Found
		$urlautotrack = 0;
	} else {
		// Auto Tracking Is Enabled
		$urlautotrack = 1;
	}
}

// End Check If AutoTrack Is Enabled

// Update AutoTrack Setting
if ($_GET['checkautotrack'] == 1 && $urlautotrack == 1) {
	@mysql_query("UPDATE `tracker_urls` SET autoconv='1' WHERE user_id='".$userid."' AND id='".$trackerid."'");
} elseif ((isset($_GET['checkautotrack']) && $_GET['checkautotrack'] == 0) || $urlautotrack == 0) {
	@mysql_query("UPDATE `tracker_urls` SET autoconv='0' WHERE user_id='".$userid."' AND id='".$trackerid."'");
}



####################

//Begin main page

####################

?>
<html>
<body>
<center>

	<h4><b>Conversions Tracker</b></h4>
	<p align="left">The conversion tracker counts how many actions (such as signups or sales) you're getting from promoting your tracker.</p>

	<?php
	
if ($urlautotrack == 1) {

	$autosetting = mysql_result(mysql_query("SELECT autoconv FROM `tracker_urls` WHERE user_id='".$userid."' AND id='".$trackerid."'"), 0);
	
	if ($autosetting == 1) {
		$autobox = "<a href=\"trackerconversions.php?checkautotrack=0&trackerid=$trackerid\"><img border=\"0\" src=\"framesite_enable.jpg\"></a>";
	} else {
		$autobox = "<a href=\"trackerconversions.php?checkautotrack=1&trackerid=$trackerid\"><img border=\"0\" src=\"framesite_disable.jpg\"></a>";
	}

	echo("<table width=500 border=1 bordercolor=gray cellpadding=5 cellspacing=0>
	<tr><td align=center bgcolor=#EEEEEE><font size=4><b>Auto Conversion Tracking</b></font></td></tr>
	<tr><td align=left>
	<p><font size=\"2\">The site you're promoting in this tracker reports that it supports Auto Conversion Tracking.  This allows you to track your conversions without adding any extra code to the site.  To enable Auto Conversion Tracking for this tracker, just click the checkbox below.</font></p>
	<center>
	<table border=0 cellpadding=2 cellspacing=0><tr><td align=right valign=middle>".$autobox."</td><td align=left valign=middle><font size=\"2\"><b>Enable Auto Conversion Tracking</b></font></td></tr></table>
	</center>
	</td></tr>
	</table>
	<br><br>");
	
	if (!isset($_GET['showmanual']) || $_GET['showmanual'] != "1") {
		echo("<p><font size=\"1\">Having problems with Auto Conversion Tracking?<br><a href=\"trackerconversions.php?trackerid=".$trackerid."&showmanual=1\">Click Here To View The Code For Manual Conversion Tracking</a></font></p>");
		exit;
	}
}

?>

<table width=500 border=1 bordercolor=gray cellpadding=5 cellspacing=0>
	<tr><td align=center bgcolor=#EEEEEE><font size=4><b>Conversion Tracking Code</b></font></td></tr>
	<tr><td align=left>
	<p><font size="2">To track the conversions for your tracker, just add the code below to your Thank You page.  For example, if you're tracking the number of signups you're getting from a squeeze page, you would add this code to the confirmation page users see after they enter their e-mail address into the form.</font></p>
	<center>
	<textarea rows="3" cols="60"><img border="0" width="1" height="1" src="<? echo("http://".$_SERVER["SERVER_NAME"]."/trackerconv.php?id=".$trackerid); ?>"></textarea>
	</center>
	</td></tr>
</table>

<?

echo("
<br><br>

</center>
</body>
</html>");

exit;

?>