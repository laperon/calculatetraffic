<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";

include "inc/config.php";
$dbconnectlink = mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die("Unable to select database");

$currentdate = date("Y-m-d");
$yesterdate = strftime("%Y-%m-%d", strtotime("$currentdate + 1 days ago"));

$getlastcron = mysql_query("SELECT value FROM `tracker_settings` WHERE field='lastcron'") or die(mysql_error());

if (mysql_num_rows($getlastcron) < 1) {
	echo("Last Cron Setting Is Missing");
	exit;
}

$lastcron = mysql_result($getlastcron, 0, "value");

if ($lastcron == $currentdate) {
	echo("This cron has already been run today.");
	exit;
}

mysql_query("UPDATE `tracker_settings` SET value='".$currentdate."' WHERE field='lastcron'") or die(mysql_error());

// Reset SourceRanks Daily Limits
mysql_query("UPDATE `".$prefix."members` SET rankstoday='0'");

// Clear Old Data
$minago = time() - 60;
$twodaysago = time() - 172800;

// These are streaming live every few seconds, so delete any older than a minute
mysql_query("DELETE FROM `rotator_livestats` WHERE stattime < '".$minago."'");
mysql_query("DELETE FROM `tracker_livestats` WHERE stattime < '".$minago."'");

// Delete SourceRanks verifications that were never used
mysql_query("DELETE FROM `tracker_verify` WHERE date < '".$yesterdate."'");

// This is to prevent a backlog just in case the cron job isn't finishing correctly
mysql_query("DELETE FROM `tracker_iplog` WHERE lasttime < '".$twodaysago."'");

// Delete the daily tracking logs
$deldailydays = @mysql_result(@mysql_query("SELECT value FROM `tracker_settings` WHERE field='keepdailylogs'"), 0);
if ($deldailydays > 0) {
	$deletedate = strftime("%Y-%m-%d", strtotime("$currentdate + ".$deldailydays." days ago"));
	mysql_query("DELETE FROM `rotator_datelog` WHERE date < '".$deletedate."'");
	mysql_query("DELETE FROM `tracker_datelog` WHERE date < '".$deletedate."'");
}

// Delete the individual source stats of the daily tracking logs
$deldailysrcdays = @mysql_result(@mysql_query("SELECT value FROM `tracker_settings` WHERE field='keepsourcelogs'"), 0);
if ($deldailysrcdays > 0) {
	$deletedate = strftime("%Y-%m-%d", strtotime("$currentdate + ".$deldailysrcdays." days ago"));
	mysql_query("DELETE FROM `rotator_datesourcelog` WHERE date < '".$deletedate."'");
	mysql_query("DELETE FROM `tracker_datesourcelog` WHERE date < '".$deletedate."'");
}

// End Clear Old Data

// Get The Trackers To Update
$gettrackers = mysql_query("SELECT DISTINCT(tracker_id) AS trackerid FROM `tracker_iplog` WHERE date='".$yesterdate."'") or die(mysql_error());
if (mysql_num_rows($gettrackers) > 0) {
	for ($i = 0; $i < mysql_num_rows($gettrackers); $i++) {
		
		$trackerid = mysql_result($gettrackers, $i, "trackerid");
		
		// Update The Daily Tracker Log
		$gethitcounts = mysql_query("SELECT SUM(nhits) AS totalhits, COUNT(DISTINCT ipaddress) AS uniquehits FROM `tracker_iplog` WHERE date='".$yesterdate."' AND tracker_id='".$trackerid."'") or die(mysql_error());
		$totalhits = mysql_result($gethitcounts, 0, "totalhits");
		$uniquehits = mysql_result($gethitcounts, 0, "uniquehits");
		$conversions = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_conversions` WHERE date='".$yesterdate."' AND tracker_id='".$trackerid."'"), 0);
		mysql_query("INSERT INTO `tracker_datelog` (date, tracker_id, nhits, uhits, convs) VALUES ('".$yesterdate."', '".$trackerid."', '".$totalhits."', '".$uniquehits."', '".$conversions."')") or die(mysql_error());
		$datelogid = mysql_insert_id();
		
		// Update The Daily Tracker Log For Each Source
		$getsources = mysql_query("SELECT DISTINCT(a.source_id) AS sourceid, b.url AS sourcename FROM `tracker_iplog` a LEFT JOIN `tracker_sources` b ON (a.source_id=b.id) WHERE a.date='".$yesterdate."' AND a.tracker_id='".$trackerid."' AND a.source_id>0 ORDER BY sourcename ASC") or die(mysql_error());
		if (mysql_num_rows($getsources) > 0) {
			$logdata = "";
			for ($j = 0; $j < mysql_num_rows($getsources); $j++) {
				
				$sourceid = mysql_result($getsources, $j, "sourceid");
				$sourcename = mysql_result($getsources, $j, "sourcename");
				
				$gethitcounts = mysql_query("SELECT SUM(nhits) AS totalhits, COUNT(DISTINCT ipaddress) AS uniquehits FROM `tracker_iplog` WHERE date='".$yesterdate."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."'") or die(mysql_error());
				$totalhits = mysql_result($gethitcounts, 0, "totalhits");
				$uniquehits = mysql_result($gethitcounts, 0, "uniquehits");
				$maxhits = mysql_result(mysql_query("SELECT SUM(nhits) AS maxhits FROM `tracker_iplog` WHERE date='".$yesterdate."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."' GROUP BY ipaddress ORDER BY maxhits DESC LIMIT 1"), 0, "maxhits");
				
				$conversions = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_conversions` WHERE date='".$yesterdate."' AND tracker_id='".$trackerid."' AND source_id='".$sourceid."'"), 0);
				
				$logdata .= $sourcename.",".$totalhits.",".$uniquehits.",".$conversions.",".$maxhits."|";
				
			}
			mysql_query("INSERT INTO `tracker_datesourcelog` (date, datelog_id, logdata) VALUES ('".$yesterdate."', '".$datelogid."', '".$logdata."')") or die(mysql_error());
		}
		
	}
}

// Get The Rotators To Update
$getrotators = mysql_query("SELECT DISTINCT(rotator_id) AS rotatorid FROM `tracker_iplog` WHERE rotator_id>0 AND date='".$yesterdate."'") or die(mysql_error());
if (mysql_num_rows($getrotators) > 0) {
	for ($i = 0; $i < mysql_num_rows($getrotators); $i++) {
		
		$rotatorid = mysql_result($getrotators, $i, "rotatorid");
		
		// Update The Daily Rotator Log
		$gethitcounts = mysql_query("SELECT SUM(nhits) AS totalhits, COUNT(DISTINCT ipaddress) AS uniquehits FROM `tracker_iplog` WHERE date='".$yesterdate."' AND rotator_id='".$rotatorid."'") or die(mysql_error());
		$totalhits = mysql_result($gethitcounts, 0, "totalhits");
		$uniquehits = mysql_result($gethitcounts, 0, "uniquehits");
		$conversions = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_conversions` WHERE date='".$yesterdate."' AND rotator_id='".$rotatorid."'"), 0);
		mysql_query("INSERT INTO `rotator_datelog` (date, rotator_id, nhits, uhits, convs) VALUES ('".$yesterdate."', '".$rotatorid."', '".$totalhits."', '".$uniquehits."', '".$conversions."')") or die(mysql_error());
		$datelogid = mysql_insert_id();
		
		// Update The Daily Rotator Log For Each Source
		$getsources = mysql_query("SELECT DISTINCT(a.source_id) AS sourceid, b.url AS sourcename FROM `tracker_iplog` a LEFT JOIN `tracker_sources` b ON (a.source_id=b.id) WHERE a.date='".$yesterdate."' AND a.rotator_id='".$rotatorid."' AND a.source_id>0 ORDER BY sourcename ASC") or die(mysql_error());
		if (mysql_num_rows($getsources) > 0) {
			$logdata = "";
			for ($j = 0; $j < mysql_num_rows($getsources); $j++) {
				
				$sourceid = mysql_result($getsources, $j, "sourceid");
				$sourcename = mysql_result($getsources, $j, "sourcename");
				
				$gethitcounts = mysql_query("SELECT SUM(nhits) AS totalhits, COUNT(DISTINCT ipaddress) AS uniquehits FROM `tracker_iplog` WHERE date='".$yesterdate."' AND rotator_id='".$rotatorid."' AND source_id='".$sourceid."'") or die(mysql_error());
				$totalhits = mysql_result($gethitcounts, 0, "totalhits");
				$uniquehits = mysql_result($gethitcounts, 0, "uniquehits");
				$maxhits = mysql_result(mysql_query("SELECT SUM(nhits) AS maxhits FROM `tracker_iplog` WHERE date='".$yesterdate."' AND rotator_id='".$rotatorid."' AND source_id='".$sourceid."' GROUP BY ipaddress ORDER BY maxhits DESC LIMIT 1"), 0, "maxhits");
				
				$conversions = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_conversions` WHERE date='".$yesterdate."' AND rotator_id='".$rotatorid."' AND source_id='".$sourceid."'"), 0);
				
				$logdata .= $sourcename.",".$totalhits.",".$uniquehits.",".$conversions.",".$maxhits."|";
				
			}
			mysql_query("INSERT INTO `rotator_datesourcelog` (date, datelog_id, logdata) VALUES ('".$yesterdate."', '".$datelogid."', '".$logdata."')") or die(mysql_error());
		}
		
	}
}

// Delete Old Entries
$tenminago = time() - 600;
mysql_query("DELETE FROM `tracker_iplog` WHERE date != '".$currentdate."' AND lasttime < '".$tenminago."'") or die(mysql_error());

echo("Rotator and Tracker cron completed successfully on: ".$currentdate.".  Server timestamp: ".time());
exit;

?>