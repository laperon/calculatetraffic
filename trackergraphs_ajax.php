<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

// VisuGraph JS 1.0
// �2012 Josh Abbott, http://visugraph.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

function countdays($firstdate, $lastdate) {
	$currdate = $firstdate;
	$daycount = 0;
	while ($currdate <= $lastdate) {
		$daycount++;
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
	}
	return $daycount;
}

// Prepare Graph

$starttime = $_GET['starttime'];
$endtime = $_GET['endtime'];
$charttype = $_GET['charttype'];
$tabletype = $_GET['tabletype'];
$siteid = $_GET['siteid'];

if (!is_numeric($siteid) || $siteid < 1) { exit; }

//Chart Type 1 = Total Hits
//Chart Type 2 = Unique Hits
//Chart Type 3 = Conversions
if ($charttype == "" || !is_numeric($charttype) || $charttype < 1 || $charttype > 3) {
	// Set to default
	$charttype = 1;
}

if ($charttype == 1) {
	$searchvalue = "nhits";
	$datalabel = "Hits";
} elseif ($charttype == 2) {
	$searchvalue = "uhits";
	$datalabel = "Hits";
} else {
	$searchvalue = "convs";
	$datalabel = "Convs";
}

if ($tabletype == "rotator") {
	
	// Validate User
	$uservalid = mysql_result(mysql_query("SELECT COUNT(*) FROM `rotators` WHERE id='".$siteid."' AND user_id='".$userid."'"), 0);
	if ($uservalid != 1) {
		echo("Invalid Rotator ID");
		exit;
	}
	
	$tablename = "rotator_datelog";
	$idcolumn = "rotator_id";
} else {
	
	// Validate User
	$uservalid = mysql_result(mysql_query("SELECT COUNT(*) FROM `tracker_urls` WHERE id='".$siteid."' AND user_id='".$userid."'"), 0);
	if ($uservalid != 1) {
		echo("Invalid Tracker ID");
		exit;
	}
	
	$tablename = "tracker_datelog";
	$idcolumn = "tracker_id";
}

if (!is_numeric($starttime) || $starttime < 1) { exit; }
if (!is_numeric($endtime) || $endtime < 1) { exit; }

$startdate = date("Y-m-d",$starttime);
$enddate = date("Y-m-d",$endtime);

$numdays = countdays($startdate, $enddate);
$numelems = 0;

if ($numdays > 40) {
	//Switch to monthly view
	
	$graphtype = "monthly";

	$currdate = $startdate;
	
	while ($currdate <= $enddate) {
	
	$selecteddate = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
	
	$selectedmonth = date("m", $selecteddate);
	$selectedyear = date("Y", $selecteddate);
	$daya = $selectedyear."-".$selectedmonth."-"."01";
	$dayb = $selectedyear."-".$selectedmonth."-"."31";
	$monthname = date("M", $selecteddate);
		
	if ($monthname == "Jan") {
		$monthname = "Jan<br>".$selectedyear;
	} else {
		$monthname = $monthname."<br><br>";
	}
	
	$getmonth = mysql_query("Select SUM(".$searchvalue.") from ".$tablename." where date>='".$daya."' and date<='".$dayb."' and ".$idcolumn."=".$siteid);
	
	$currentsum = mysql_result($getmonth, 0);
	if ($currentsum == NULL) {
		$currentsum = 0;
	}

	$statsum[$numelems] = $currentsum;
	$xname[$numelems] = $monthname;
	$xvals[$numelems] = $selecteddate * 1000;
	$numelems = $numelems+1;
	
	$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 month"));
	
	}

} else {

	$graphtype = "daily";

	$numelems = $numdays;

	$getdays = mysql_query("Select ".$searchvalue.", date from ".$tablename." where date>='".$startdate."' and date<='".$enddate."' and ".$idcolumn."=".$siteid." order by date asc");
	
	if (mysql_num_rows($getdays) > 0) {
		for ($i = 0; $i < mysql_num_rows($getdays); $i++) {
			$thedate = mysql_result($getdays, $i, "date");
			$datelist[$i] = $thedate;
			$sumlist[$i] = mysql_result($getdays, $i, $searchvalue);
		}
	} else {
		// Set Empty Date Array
		$datelist[0] = 0;
	}
	
	$currdate = $startdate;
	$i = 0;
	while ($currdate <= $enddate) {
	
		$dateposition = array_search($currdate, $datelist);
		if ($dateposition === false) {
			//No Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = 0;
			$xname[$i] = date("d", $thetime);
			if ($xname[$i] == 1) { $xname[$i] = $xname[$i]."<br>".date("M", $thetime); } else { $xname[$i] = $xname[$i]."<br><br>"; }
			$xvals[$i] = $thetime * 1000;
		} else {
			//Enter Stats For Date
			$thetime = mktime(0,0,0,substr($currdate,5,2),substr($currdate,8),substr($currdate,0,4));
			$statsum[$i] = $sumlist[$dateposition];
			$xname[$i] = date("d", $thetime);
			if ($xname[$i] == 1) { $xname[$i] = $xname[$i]."<br>".date("M", $thetime); } else { $xname[$i] = $xname[$i]."<br><br>"; }
			$xvals[$i] = $thetime * 1000;
		}
	
		$currdate = strftime("%Y-%m-%d", strtotime("$currdate + 1 day"));
		$i++;
	}
}

// Check The Date Range Available
$currentdate = date("Y-m-d");

$getfirstdate = mysql_query("Select date from ".$tablename." where ".$idcolumn."=".$siteid." order by date asc limit 1");
if (mysql_num_rows($getfirstdate) > 0) {
	$firstavaildate = mysql_result($getfirstdate, 0, "date");
} else {
	$firstavaildate = strftime("%Y-%m-%d", strtotime("$currentdate + 7 days ago"));
}

$getlastdate = mysql_query("Select date from ".$tablename." where ".$idcolumn."=".$siteid." order by date desc limit 1");
if (mysql_num_rows($getlastdate) > 0) {
	$lastavaildate = mysql_result($getlastdate, 0, "date");
} else {
	$lastavaildate = $currentdate;
}

// Check Zoom In
if ($numdays > 7) {
	$canzoomin = "yes";
} else {
	$canzoomin = "no";
}

// Check Zoom Out
if ($numdays >= 365) {
	// Show A Max Of 1 Year
	$canzoomout = "no";
} elseif ($firstavaildate < $startdate || $lastavaildate > $enddate) {
	$canzoomout = "yes";
} else {
	$canzoomout = "no";
}

// Check Pan Left
if ($firstavaildate < $startdate) {
	$canpanleft = "yes";
} else {
	$canpanleft = "no";
}

// Check Pan Right
if ($lastavaildate > $enddate) {
	$canpanright = "yes";
} else {
	$canpanright = "no";
}


// End Prepare Graph

if ($numelems < 1) {
	// No Data To Display
	exit;
}

// Make Data Array And Ticks (Axis Labels)
$datatext = $xvals[0].",".$statsum[0];
$tickstext = $xvals[0].",".$xname[0];
for ($i = 1; $i < $numelems; $i++) {
	$datatext .= "|".$xvals[$i].",".$statsum[$i];
	$tickstext .= "|".$xvals[$i].",".$xname[$i];			
}

// Set The Top Line On The Y Axis By Finding The Highest Stat
if ($numdays <= 40) {
	$getavg = @mysql_query("SELECT MAX(".$searchvalue.") AS maxval, AVG(".$searchvalue.") AS averageval from ".$tablename." where ".$idcolumn."=".$siteid);
	$maxval =  @mysql_result($getavg, 0, "maxval");
	$avgval = @mysql_result($getavg, 0, "averageval");
	
	if (isset($maxval) && is_numeric($maxval) && isset($avgval) && is_numeric($avgval)) {
		if (($avgval*3) >= $maxval) {
			// Keeps A Consistent Axis Unless Max Value Is An Outlier
			$maxstat = $maxval;
		} else {
			$maxstat = max($statsum);
		}
	} else {
		$maxstat = max($statsum);
	}
} else {
	$monthlysums = mysql_query("SELECT SUM(".$searchvalue.") AS monthsum FROM `".$tablename."` where ".$idcolumn."=".$siteid." group by YEAR(date), MONTH(date)");
	if (mysql_num_rows($monthlysums) > 1) {
		while ($row = mysql_fetch_array($monthlysums)) {
			$sumsarray[] = $row['monthsum'];
		}
		$maxval = max($sumsarray);
		$avgval = array_sum($sumsarray) / count($sumsarray);
		if (($avgval*3) >= $maxval) {
			// Keeps A Consistent Axis Unless Max Value Is An Outlier
			$maxstat = $maxval;
		} else {
			$maxstat = max($statsum);
		}
	} else {
		$maxstat = max($statsum);
	}
}

// Output The Data
echo("#STARTSTARTDATE#".$startdate."#ENDSTARTDATE#");
echo("#STARTENDDATE#".$enddate."#ENDENDDATE#");

echo("#STARTGRAPHTYPE#".$graphtype."#ENDGRAPHTYPE#");
echo("#STARTDATALABEL#".$datalabel."#ENDDATALABEL#");

echo("#STARTDATA#".$datatext."#ENDDATA#");
echo("#STARTTICKS#".$tickstext."#ENDTICKS#");
echo("#STARTMAX#".$maxstat."#ENDMAX#");

echo("#STARTZOOMIN#".$canzoomin."#ENDZOOMIN#");
echo("#STARTZOOMOUT#".$canzoomout."#ENDZOOMOUT#");
echo("#STARTPANLEFT#".$canpanleft."#ENDPANLEFT#");
echo("#STARTPANRIGHT#".$canpanright."#ENDPANRIGHT#");

exit;

?>