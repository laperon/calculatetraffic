<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

if (!isset($userid) || !is_numeric($userid)) {
	echo("Invalid Link");
	exit;
}

$getmtype = mysql_query("select mtype from ".$prefix."members where Id=$userid");
$acctype = mysql_result($getmtype, 0, "mtype");

$cnx = mysql_query("select rot_convtrk, rot_sr_enable from ".$prefix."membertypes where mtid=$acctype");
$convtrk = mysql_result($cnx,0,'rot_convtrk');
$accsourceranks = mysql_result($cnx,0,'rot_sr_enable');

$timequery = "";

// Group By Type
// Type 0: Show All Data
// Type 1: Group By Rotator
// Type 2: Group By Tracker
// Type 3: Group By Source
if (!isset($_GET['show']) || !is_numeric($_GET['show']) || $_GET['show'] < 0 || $_GET['show'] > 3) {
	$showtype = 2;
} else {
	$showtype = $_GET['show'];
}
// End Group By Type

// Support For Custom Search
if ($_POST["searchtext"] != "") {
            $searchfield=trim($_POST["searchfield"]);
            $searchtext=trim($_POST["searchtext"]);
            
} elseif (($_GET['searchfield'] != "") && ($_GET['searchtext'] != "")) {
            $searchfield=trim($_GET["searchfield"]);
            $searchtext=trim($_GET["searchtext"]);
}
// End Support For Custom Search

// Source Search
if (isset($_POST['sourcesearch']) && strlen($_POST['sourcesearch']) > 0) {
	$_GET["showsourceid"] = -1;
	$sourcesearch = trim(urlencode($_POST['sourcesearch']));
} elseif (isset($_GET['sourcesearch']) && strlen($_GET['sourcesearch']) > 0) {
	$sourcesearch = $_GET['sourcesearch'];
} else {
	$sourcesearch = "";
}
// End Source Search

// Tracker Search
if (isset($_POST['trackersearch']) && strlen($_POST['trackersearch']) > 0) {
	$_GET["showtrackerid"] = -1;
	$trackersearch = trim(urlencode($_POST['trackersearch']));
} elseif (isset($_GET['trackersearch']) && strlen($_GET['trackersearch']) > 0) {
	$trackersearch = $_GET['trackersearch'];
} else {
	$trackersearch = "";
}
// End Tracker Search

// Show Rotator ID
if (isset($_POST["showrotatorid"]) && is_numeric($_POST["showrotatorid"]) && $_POST["showrotatorid"] >= -1) {
            $showrotatorid=trim($_POST["showrotatorid"]);
} elseif (isset($_GET['showrotatorid']) && is_numeric($_GET["showrotatorid"]) && $_GET['showrotatorid'] >= -1) {
            $showrotatorid=trim($_GET["showrotatorid"]);
} else {
            $showrotatorid = -1;
}
// End Show Rotator ID

// Show Source ID
if (isset($_GET["showsourceid"]) && is_numeric($_GET["showsourceid"]) && $_GET["showsourceid"] >= 0) {
            $showsourceid=trim($_GET["showsourceid"]);
} else {
	$showsourceid = -1;
}
// End Show Source ID

// Show Tracker ID
if (isset($_GET["showtrackerid"]) && is_numeric($_GET["showtrackerid"]) && $_GET["showtrackerid"] >= 0) {
            $showtrackerid=trim($_GET["showtrackerid"]);
} else {
	$showtrackerid = -1;
}
// End Show Tracker ID

// Min Hits Filter
if (isset($_POST["minnhits"]) && is_numeric($_POST["minnhits"]) && $_POST["minnhits"] >= 0) {
            $minnhits=trim($_POST["minnhits"]);
} elseif (isset($_GET["minnhits"]) && is_numeric($_GET["minnhits"]) && $_GET["minnhits"] > 0) {
            $minnhits=trim($_GET["minnhits"]);
} else {
	$minnhits=0;
}
// End Min Hits Filter



// Table Sorting

if (($_GET["sortby"] != "") && ($_GET["sortorder"] != "")) {
$sortby = $_GET["sortby"];
$sortorder = $_GET["sortorder"];

//Set Orders
if ($sortby == "a.id" && $sortorder == "ASC") {
$idorder = "DESC";
} else {
$idorder = "ASC";
}
if ($sortby == "d.url" && $sortorder == "ASC") {
$sourceidorder = "DESC";
} else {
$sourceidorder = "ASC";
}

if ($sortby == "c.name" && $sortorder == "ASC") {
$trackeridorder = "DESC";
} else {
$trackeridorder = "ASC";
}

if ($sortby == "b.name" && $sortorder == "ASC") {
$rotatoridorder = "DESC";
} else {
$rotatoridorder = "ASC";
}

if ($sortby == "nhits" && $sortorder == "DESC") {
$nhitsorder = "ASC";
} else {
$nhitsorder = "DESC";
}
if ($sortby == "nconvs" && $sortorder == "DESC") {
$nconvsorder = "ASC";
} else {
$nconvsorder = "DESC";
}

if ($sortby == "convrate" && $sortorder == "DESC") {
$convrateorder = "ASC";
} else {
$convrateorder = "DESC";
}

if ($sortby == "ratehits" && $sortorder == "DESC") {
$ratehitsorder = "ASC";
} else {
$ratehitsorder = "DESC";
}

if ($sortby == "nrates" && $sortorder == "DESC") {
$nratesorder = "ASC";
} else {
$nratesorder = "DESC";
}

if ($sortby == "ratesconv" && $sortorder == "DESC") {
$ratesconvorder = "ASC";
} else {
$ratesconvorder = "DESC";
}

if ($sortby == "avgrate" && $sortorder == "DESC") {
$avgrateorder = "ASC";
} else {
$avgrateorder = "DESC";
}

} else {
$sortby = "a.id";
$sortorder = "ASC";

$idorder = "ASC";
$sourceidorder = "ASC";

$trackeridorder = "ASC";
$rotatoridorder = "ASC";

$nhitsorder = "DESC";
$nconvsorder = "DESC";

$convrateorder = "DESC";
$ratehitsorder = "DESC";
$nratesorder = "DESC";
$ratesconvorder = "DESC";
$avgrateorder = "DESC";

}

// End Table Sorting

####################

//Begin main page

####################

include "inc/theme.php";
load_template ($theme_dir."/header.php");
?>
<div class="wfull">
    <div class="grid w960">
        <div class="header-banner">&nbsp;</div>
    </div>
</div>
<?php
load_template ($theme_dir."/mmenu.php");

echo(getrotmenu($rotpage));

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>

<style type=\"text/css\">
 .membertd {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11pt;
	background-color:#B0CBFD;
}

.membertdbold {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 11pt;
	font-weight: bold;
	background-color:#B0CBFD;
}
.admintd {
	font-family: Arial, Helvetica, sans-serif; 
	font-size: 14px;
	background-color:#B0CBFD;
	border: solid thin; border-width: 1px; border-color: #000000;
}
</style>

<center>
<br>
");



?>

<center>

<hr>

<p><font size=3><b>Show:</b>

<a href="myrotators.php?rotpage=trackstats&show=2&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>"><? if ($showtype == 2) { echo("<b>Trackers</b>"); } else { echo("Trackers"); } ?></a> | 

<a href="myrotators.php?rotpage=trackstats&show=1&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>"><? if ($showtype == 1) { echo("<b>Rotators</b>"); } else { echo("Rotators"); } ?></a> | 

<a href="myrotators.php?rotpage=trackstats&show=3&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>"><? if ($showtype == 3) { echo("<b>Sources</b>"); } else { echo("Sources"); } ?></a> | 

<a href="myrotators.php?rotpage=trackstats&show=0&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>"><? if ($showtype == 0) { echo("<b>Combo Stats</b>"); } else { echo("Combo Stats"); } ?></a> | 

</p>

</center>

<?

    // Get The Data
    $mqry = "";
    
    $selqry = "SELECT SQL_CALC_FOUND_ROWS a.rotator_id AS rotatorid, a.tracker_id AS trackerid, a.source_id AS sourceid, SUM(a.nhits) AS nhits, SUM(a.nconvs) AS nconvs, round(((SUM(a.nconvs)/SUM(a.nhits))*100),5) AS convrate, SUM(a.ratehits) AS ratehits, SUM(a.nrates) AS nrates, round(((SUM(a.nrates)/SUM(a.ratehits))*100),5) AS ratesconv, AVG(a.avgrate) AS avgrate, b.name AS rotatorname, c.name AS trackername, d.url AS sourcename FROM `tracker_trackdata` a LEFT JOIN `rotators` b ON (a.rotator_id=b.id) LEFT JOIN `tracker_urls` c ON (a.tracker_id=c.id) LEFT JOIN `tracker_sources` d ON (a.source_id=d.id) WHERE a.user_id='".$_SESSION["userid"]."'";

    // Add search criteria if applicable
    if($searchtext != "") {
    
    		if ($timequery != "") {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."' AND ".$timequery;
		} else {
			$mqry.=" AND ".$searchfield." LIKE '".$searchtext."'";
		}
		
		$searchtext = str_replace("%", "", $searchtext);

    }
    	
    	
	if (is_numeric($showrotatorid) && $showrotatorid >= 0) {
		$mqry.=" AND a.rotator_id='".$showrotatorid."'";
	}
	
	if (is_numeric($showsourceid) && $showsourceid >= 0) {
		$mqry.=" AND a.source_id='".$showsourceid."'";
	}
	
	if (is_numeric($showtrackerid) && $showtrackerid >= 0) {
		$mqry.=" AND a.tracker_id='".$showtrackerid."'";
	}
	
	if (isset($sourcesearch) && strlen($sourcesearch) > 0) {
		$sourcesearch=trim(urldecode($sourcesearch));
		$sourcesearch = str_replace("www.", "", $sourcesearch);
		$mqry.=" AND d.url LIKE '%".$sourcesearch."%'";
		$sourcesearch = urlencode($sourcesearch);
	}
	
	if (isset($trackersearch) && strlen($trackersearch) > 0) {
		$trackersearch=trim(urldecode($trackersearch));
		$trackersearch = str_replace("www.", "", $trackersearch);
		$mqry.=" AND (c.url LIKE '%".$trackersearch."%' OR c.name LIKE '%".$trackersearch."%')";
		$trackersearch = urlencode($trackersearch);
	}
	
	if ($showtype == 1) {
		$mqry.=" AND a.rotator_id != '0' GROUP BY a.rotator_id";
	} elseif ($showtype == 2) {
		$mqry.=" AND a.tracker_id != '0' GROUP BY a.tracker_id";
	} elseif ($showtype == 3) {
		$mqry.=" AND a.source_id != '0' GROUP BY a.source_id";
	} else {
		// Only take the sums if the entries are in a group
		$selqry = "SELECT SQL_CALC_FOUND_ROWS a.rotator_id AS rotatorid, a.tracker_id AS trackerid, a.source_id AS sourceid, a.nhits AS nhits, a.nconvs AS nconvs, round(((a.nconvs/a.nhits)*100),5) AS convrate, a.ratehits AS ratehits, a.nrates AS nrates, round(((a.nrates/a.ratehits)*100),5) AS ratesconv, a.avgrate AS avgrate, b.name AS rotatorname, c.name AS trackername, d.url AS sourcename FROM `tracker_trackdata` a LEFT JOIN `rotators` b ON (a.rotator_id=b.id) LEFT JOIN `tracker_urls` c ON (a.tracker_id=c.id) LEFT JOIN `tracker_sources` d ON (a.source_id=d.id) WHERE a.user_id='".$_SESSION["userid"]."'";
	}
	
    $mqry.=" ORDER BY ".$sortby." ".$sortorder;
    
	if (!is_numeric($minnhits) || $minnhits <= 0) {
		// Limit the number of entries per page
		$entriesperpage = "30";
		if (!isset($_GET['pageoffset']) || !is_numeric($_GET['pageoffset'])) {
			$pageoffset = "0";
		} else {
			$pageoffset = $_GET['pageoffset'];
		}
		
		$mqry.=" LIMIT ".$pageoffset.", ".$entriesperpage;
	}

    $mres = mysql_query($selqry.$mqry) or die($selqry.$mqry."<br><br>".mysql_error());
    
    $entrycount = mysql_result(mysql_query("SELECT FOUND_ROWS();"), 0);


?>

<table width=500 border=1 bordercolor=gray cellpadding=5 cellspacing=0>
<tr><td align=center bgcolor=#EEEEEE><font size=4><b>Search and Filters</b></font></td></tr>
<tr><td align=center>
<form action="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>" method="post">
	<?
	// Rotator Filter
	if ($showtype != 1) {
		echo("<font size=\"2\"><b>Show Stats From Rotator: </b></font>
			<select name=\"showrotatorid\">
				<option value=\"-1\">All Rotators</option>");
		
		$getrotators = mysql_query("SELECT id, name FROM rotators WHERE user_id='".$userid."' ORDER BY name ASC");
		for ($i = 0; $i < mysql_num_rows($getrotators); $i++) {
			$rotlinkid = mysql_result($getrotators, $i, "id");
			$rotlinkname = mysql_result($getrotators, $i, "name");
			echo("<option value=\"".$rotlinkid."\""); if ($rotlinkid == $showrotatorid) { echo(" selected=\"selected\""); } echo(">".$rotlinkname."</option>");
		}
		
		echo("</select>
			<br /><br />");
	} else {
		echo("<input type=\"hidden\" name=\"showrotatorid\" value=\"-1\">");
	}
	
	if ($showtype == 0 || $showtype == 2) {
		// Tracker Filter
		echo("<font size=\"2\"><b>Search For Tracker: </b></font>
			<input type=\"text\" name=\"trackersearch\" size=\"15\" value=\"".$trackersearch."\">
			<br /><br />");
	}
	
	if ($showtype == 0 || $showtype == 3) {
		// Source Filter
		echo("<font size=\"2\"><b>Search For Source: </b></font>
			<input type=\"text\" name=\"sourcesearch\" size=\"15\" value=\"".$sourcesearch."\">
			<br /><br />");
	}
	
	?>
<font size="2"><b>Exclude entries with less than </b></font> <input type="text" name="minnhits" size="3" value="<?=$minnhits?>"> <font size="2"><b> hits.</b></font>
<br /><br />
<input type="submit" value="Search">
</form>
</td></tr>
</table>
<br />

<table width="100%" border="0" cellspacing="0" cellpadding="4">
  <tr>
    <td><table width="95%" border="0" cellpadding="1" cellspacing="0" class="lfmtable">
      
      <tr class="admintd">
      
      <?
      	if ($accsourceranks == "1") {
      		$rowspan = "2";
      	} else {
      		$rowspan = "1";
      	}
      ?>
      
      <? if ($showtype == 0 || $showtype == 1) { ?>
	<td rowspan="<?=$rowspan?>" width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=b.name&sortorder=<?=$rotatoridorder?>">Rotator</a></font></strong></td>
      <? } ?>
	
      <? if ($showtype == 0 || $showtype == 2) { ?>
	<td rowspan="<?=$rowspan?>" width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=c.name&sortorder=<?=$trackeridorder?>">Tracker</a></font></strong></td>
      <? } ?>

      <? if ($showtype == 0 || $showtype == 3) { ?>
	<td rowspan="<?=$rowspan?>" width="50" align="left" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=d.url&sortorder=<?=$sourceidorder?>">Source</a></font></strong></td>
      <? } ?>

        <td rowspan="<?=$rowspan?>" align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=nhits&sortorder=<?=$nhitsorder?>">Hits</a></font></strong></td>
	
	<? if ($convtrk == "1") { ?>
	
        <td rowspan="<?=$rowspan?>" align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong>
		<font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=nconvs&sortorder=<?=$nconvsorder?>">Convs.</a></font></strong></td>
		
        <td rowspan="<?=$rowspan?>" align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=convrate&sortorder=<?=$convrateorder?>">Conv. <br /> Rate</a></font></strong> </td>
        
        <? } ?>
        
        
        <? if ($accsourceranks == "1") { ?>
        
	<td colspan="4"  nowrap="NOWRAP" bgcolor="navy"><strong><font size="2" color="white" face="Verdana, Arial, Helvetica, sans-serif">Source Ranks Stats</font></stong></td>
	
	</tr>
	
	<tr class="admintd">
        
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=ratehits&sortorder=<?=$ratehitsorder?>">Times <br /> Shown</a></font></strong> </td>
        
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=nrates&sortorder=<?=$nratesorder?>">Times <br /> Rated</a></font></strong> </td>
        
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=ratesconv&sortorder=<?=$ratesconvorder?>">Response <br /> %</a></font></strong> </td>
        
        <td align="center" nowrap="NOWRAP" background="images/lfmtablethbg.jpg"><strong><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><a href="myrotators.php?rotpage=trackstats&show=<?=$showtype?>&searchfield=<?=$searchfield?>&searchtext=<?=$searchtext?>&showrotatorid=<?=$showrotatorid?>&showsourceid=<?=$showsourceid?>&showtrackerid=<?=$showtrackerid?>&minnhits=<?=$minnhits?>&sourcesearch=<?=$sourcesearch?>&trackersearch=<?=$trackersearch?>&sortby=avgrate&sortorder=<?=$avgrateorder?>">Average <br /> Rating</a></font></strong> </td>
        
        <? } ?>
        
      </tr>
<?
	while($mrow=@mysql_fetch_array($mres))
	{
	
		if (!is_numeric($minnhits) || $minnhits <= 0 || $minnhits <= $mrow["nhits"]) {
			
			
			if($bgcolor == "#FFFFFF")
			{
				$bgcolor="#DDDDDD";
			}
			else
			{
				$bgcolor="#FFFFFF";
			}
			
			if ($mrow["rotatorname"] == null) { $mrow["rotatorname"] = "none"; }
			
			if ($mrow["sourcename"] == null) { $mrow["sourcename"] = "unknown"; }
			
			if (!isset($mrow["convrate"]) || !is_numeric($mrow["convrate"]) || $mrow["convrate"] <= 0) {
				$mrow["convrate"] = "0";
			}
			
			if (!isset($mrow["ratesconv"]) || !is_numeric($mrow["ratesconv"]) || $mrow["ratesconv"] <= 0) {
				$mrow["ratesconv"] = "0";
			}
			
			if (!isset($mrow["avgrate"]) || !is_numeric($mrow["avgrate"]) || $mrow["avgrate"] <= 0) {
				$mrow["avgrate"] = "0";
			}
			
			if ($mrow["convrate"] >=1 ) {
				$mrow["convrate"] = round($mrow["convrate"], 2);
			}
			
			if ($mrow["ratesconv"] >=1 ) {
				$mrow["ratesconv"] = round($mrow["ratesconv"], 2);
			}
			
			if ($mrow["avgrate"] > 0 && $accsourceranks == "1") {
				if ($showtype == 0) {
					$viewlink = "srview.php?showsourceid=".$mrow["sourceid"]."&showtrackerid=".$mrow["trackerid"];
				}
				if ($showtype == 1) {
					$mrow["avgrate"] = mysql_result(mysql_query("SELECT AVG(avgrate) AS avgrating FROM `tracker_trackdata` WHERE `rotator_id`='".$mrow["rotatorid"]."' AND avgrate > '0'"), 0);
					$viewlink = "srview.php?showrotatorid=".$mrow["rotatorid"];
				}
				if ($showtype == 2) {
					$mrow["avgrate"] = mysql_result(mysql_query("SELECT AVG(avgrate) AS avgrating FROM `tracker_trackdata` WHERE `tracker_id`='".$mrow["trackerid"]."' AND avgrate > '0'"), 0);
					$viewlink = "srview.php?showtrackerid=".$mrow["trackerid"];
				}
				if ($showtype == 3) {
					$mrow["avgrate"] = mysql_result(mysql_query("SELECT AVG(avgrate) AS avgrating FROM `tracker_trackdata` WHERE `source_id`='".$mrow["sourceid"]."' AND avgrate > '0'"), 0);
					$viewlink = "srview.php?showsourceid=".$mrow["sourceid"];
				}
				$mrow["avgrate"] = round($mrow["avgrate"]);
				$rateimg = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr>";
				for ($ratecount = 0; $ratecount < $mrow["avgrate"]; $ratecount++) {
					$rateimg .= "<td><img border=\"0\" src=\"srstar.gif\"></td>";
				}
				$rateimg .= "</tr></table><a target=\"_blank\" href=\"".$viewlink."\"><font size=\"1\" face=\"Verdana, Arial, Helvetica, sans-serif\">View Comments</font></a>";
			} else {
				$rateimg = "N/A";
			}
			
			if (strlen($mrow["sourcename"]) > 20) {
				$mrow["sourcename"] = substr($mrow["sourcename"], 0, 17)."...";
			}
			
	?>
	      <tr bgcolor="<?=$bgcolor;?>" onMouseOver="this.bgColor='#99bb99';" onMouseOut="this.bgColor='<?=$bgcolor;?>';">
	        
	        <? if ($showtype == 0 || $showtype == 1) { ?>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
	          <a href="myrotators.php?rotpage=trackstats&show=2&showrotatorid=<?=$mrow["rotatorid"];?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>"><?=$mrow["rotatorname"];?></a>
	        </font></td>
	        <? } ?>
	        
	        <? if ($showtype == 0 || $showtype == 2) { ?>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
	          <a href="myrotators.php?rotpage=trackstats&show=3&showtrackerid=<?=$mrow["trackerid"];?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>"><?=$mrow["trackername"];?></a>
	        </font></td>
	        <? } ?>
	        
	        <? if ($showtype == 0 || $showtype == 3) { ?>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif">
	          <a href="myrotators.php?rotpage=trackstats&show=2&showsourceid=<?=$mrow["sourceid"];?>&sortby=<?=$sortby?>&sortorder=<?=$sortorder?>"><?=$mrow["sourcename"];?></a>
	        </font></td>
	        <? } ?>
	        
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
	          <?=$mrow["nhits"];?>
	        </b></font></td>
	        
	        <? if ($convtrk == "1") { ?>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
	          <?=$mrow["nconvs"];?>
	        </b></font></td>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
	          <?=$mrow["convrate"];?>
	        </b></font></td>
	        <? } ?>
	        
	        <? if ($accsourceranks == "1") { ?>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
	          <?=$mrow["ratehits"];?>
	        </b></font></td>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
	          <?=$mrow["nrates"];?>
	        </b></font></td>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
	          <?=$mrow["ratesconv"];?>
	        </b></font></td>
	        <td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><b>
	          <?=$rateimg;?>
	        </b></font></td>
	        <? } ?>
	
	      </tr>
	<?
	}
}
?>
    </table>
    
<?
  	if (!is_numeric($minnhits) || $minnhits <= 0) {
	  	// Output Page Numbers
	  	$pagenumberhtml = "";
		$pages = intval($entrycount/$entriesperpage);
		if ($pages >= 1 && $entrycount > $entriesperpage) {
			$pagenumberhtml .= "<font size=\"2\"><b>Page: </b>";
			for ($i=1; $i<$pages+2; $i++) {
				$newoffset = $entriesperpage*($i-1);
				if ((($i-1)*$entriesperpage) < $entrycount) {
				if($newoffset == $pageoffset) {
					$pagenumberhtml .= " <b>$i</b> ";
				} else {
					$pagenumberhtml .= " <a href=\"myrotators.php?rotpage=trackstats&show=".$showtype."&searchfield=".$searchfield."&searchtext=".$searchtext."&showrotatorid=".$showrotatorid."&showsourceid=".$showsourceid."&showtrackerid=".$showtrackerid."&minnhits=".$minnhits."&sourcesearch=".$sourcesearch."&trackersearch=".$trackersearch."&sortby=".$sortby."&sortorder=".$sortorder."&pageoffset=".$newoffset."\">$i</a> ";
				}
				}
			}
			$pagenumberhtml .= "</font>";
		}
		echo($pagenumberhtml);
		// End Output Page Numbers
	}
?>
    
    	</td>
  </tr>
  <tr>
    <td>
	</td>
  </tr>
</table>

<?

include $theme_dir."/footer.php";
exit;

?>