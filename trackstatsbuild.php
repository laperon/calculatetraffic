<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

$currentdate = date("Y-m-d");

if (isset($_GET['siteid']) && is_numeric($_GET['siteid'])) {
	$siteid = $_GET['siteid'];	
} else {
	echo("Invalid Site ID");
	exit;
}

if ($_GET['type'] == "tracker") {
	// Tracker Stats
	$columnname = "a.tracker_id";
} elseif ($_GET['type'] == "rotator") {
	//Rotator Stats
	$columnname = "a.rotator_id";
} else {
	echo("Invalid Type");
	exit;
}

if (!isset($_GET['lc']) || !is_numeric($_GET['lc']) || $_GET['lc'] < 1) {
	$_GET['lc'] = 0;
}

$getstats = mysql_query("SELECT SUM(a.nhits) AS statsnum, b.url AS statdata FROM `tracker_iplog` a LEFT JOIN `tracker_sources` b ON (a.source_id=b.id) WHERE a.date='".$currentdate."' AND ".$columnname."='".$siteid."' AND a.source_id>0 GROUP BY a.source_id ORDER BY statsnum DESC") or die(mysql_error());

echo(time()."|");

if (mysql_num_rows($getstats) > 0) {
	for ($i = 0; $i < mysql_num_rows($getstats); $i++) {
		echo(mysql_result($getstats, $i, "statdata").",".mysql_result($getstats, $i, "statsnum")."|");
	}
}

exit;

?>