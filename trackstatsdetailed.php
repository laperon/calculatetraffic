<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

if (isset($_GET['siteid']) && is_numeric($_GET['siteid'])) {
	$siteid = $_GET['siteid'];	
} else {
	echo("Invalid Site ID");
	exit;
}

if (isset($_GET['date']) && strlen($_GET['date']) == 10) {
	$date = $_GET['date'];	
} else {
	echo("Invalid Date");
	exit;
}

if ($_GET['type'] == "tracker") {
	// Tracker Stats
	$tablename1 = "tracker_datelog";
	$tablename2 = "tracker_datesourcelog";
	$columnname = "tracker_id";
} elseif ($_GET['type'] == "rotator") {
	//Rotator Stats
	$tablename1 = "rotator_datelog";
	$tablename2 = "rotator_datesourcelog";
	$columnname = "rotator_id";
} else {
	echo("Invalid Type");
	exit;
}

$getstats = mysql_query("SELECT b.logdata AS statdata FROM `".$tablename1."` a LEFT JOIN `".$tablename2."` b ON (a.id=b.datelog_id) where a.date='".$date."' AND a.".$columnname."='".$siteid."' AND b.date='".$date."'") or die(mysql_error());

if (mysql_num_rows($getstats) > 0) {
	echo(mysql_result($getstats, 0, "statdata"));
} else {
	echo("<center><b>No Stats Available For This Day</b></center>");
}

exit;

?>