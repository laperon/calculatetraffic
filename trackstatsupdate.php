<?php

// Rotator and Tracker Plugin
// �2013 LFM Wealth Systems, http://thetrafficexchangescript.com
// Licensed for the LFMTE script

require_once "inc/filter.php";
session_start();
include "inc/userauth.php";
$userid = $_SESSION["userid"];

if (isset($_GET['siteid']) && is_numeric($_GET['siteid'])) {
	$siteid = $_GET['siteid'];	
} else {
	echo("Invalid Site ID");
	exit;
}

if ($_GET['type'] == "tracker") {
	// Tracker Stats
	$tablename = "tracker_livestats";
	$columnname = "tracker_id";
} elseif ($_GET['type'] == "rotator") {
	//Rotator Stats
	$tablename = "rotator_livestats";
	$columnname = "rotator_id";
} else {
	echo("Invalid Type");
	exit;
}

if (!isset($_GET['lc']) || !is_numeric($_GET['lc']) || $_GET['lc'] < 1) {
	$_GET['lc'] = 0;
}

$getstats = mysql_query("SELECT statdata from `".$tablename."` where stattime > '".$_GET['lc']."' AND user_id='".$userid."' AND ".$columnname."='".$siteid."'") or die(mysql_error());

echo(time()."|");

if (mysql_num_rows($getstats) > 0) {
	for ($i = 0; $i < mysql_num_rows($getstats); $i++) {
		echo(mysql_result($getstats, $i, "statdata")."|");
	}
}

exit;

?>