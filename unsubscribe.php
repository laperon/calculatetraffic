<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.29
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";


	include "inc/config.php";
	@mysql_connect($dbhost,$dbuser,$dbpass);
	@mysql_select_db($dbname) or die( "Unable to select database");

	// Query settings table for sitename
	$res=@mysql_query("SELECT * FROM ".$prefix."settings") or die("Unable to find settings!");
	$row=@mysql_fetch_array($res);
	$sitename=$row["sitename"];
	$replyaddress=$row["replyaddress"];

	if(isset($_REQUEST["email"]) && isset($_REQUEST["user"]) && is_numeric($_REQUEST["user"]))
	{
		$user=$_REQUEST["user"];
		$email=$_REQUEST["email"];
		$lres=@mysql_query("SELECT Id, email FROM ".$prefix."members WHERE email='".$email."' AND Id=$user");
		$lrow=@mysql_fetch_object($lres);

		if($lrow->Id == $user && $lrow->email=$email)
		{
			@mysql_query("UPDATE ".$prefix."members SET newsletter=0 WHERE Id=$user");
			$msg="has been removed from our mailing list.";
		}
		else
		{
			$msg="was not found in our member database.";
		}
	} else {
		echo("Invalid Link");
		exit;
	}

include "inc/theme.php";

load_template ($theme_dir."/header.php");  

$page_content = '<center><p><font face="Verdana, Arial, Helvetica, sans-serif">Your email address: '.$_REQUEST["email"].'</font></p>
   <p><font face="Verdana, Arial, Helvetica, sans-serif">'.$msg.'</font></p></center>';


load_template ($theme_dir."/content.php");
load_template ($theme_dir."/footer.php");

?>