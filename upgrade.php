<?
/////////////////////////////////////////////////////////////////////////
// ULTIMATE IPN & PAYMENT MANAGEMENT SYSTEM v1.3                       //
// (c) 2004-2009 Simon B Kelly. All rights reserved.                   //
// http://replytosimon.com                                             //
//                                                                     //
// TrafficMods.com Ultimate IPN.                                       //
// Not for resale.  Version included with the LFMTE script only.       //
/////////////////////////////////////////////////////////////////////////

require_once "inc/filter.php";
require_once "inc/userauth.php";
$userid = $_SESSION["userid"];

$countcredits = mysql_query("select credits, mtype, email, upgend from ".$prefix."members where Id=$userid");
$usercredits = mysql_result($countcredits, 0, "credits");
$acctype = mysql_result($countcredits, 0, "mtype");
$useremail = mysql_result($countcredits, 0, "email");
$upgend = mysql_result($countcredits, 0, "upgend");

if (!is_numeric($usercredits)) {
	$usercredits = 0;
}

####################

//Begin main page

####################

echo("<script language=\"javascript\">
	if (top.location != self.location) {
		top.location.href=location.href;
	}
</script>

<div class=wfull>
    <div class=\"grid w960\">
        <div class=header-banner>&nbsp;</div>
    </div>
</div>");

/*Load Leftinfobox Menu*/
load_template ($theme_dir."/mmenu.php");

echo("<div class=c9>
<div class=banner-content>
<table cellspacing=0 cellpadding=0 border=0 width=100%>
    <tbody><tr>
    <td align=center valign=top><h4><b><u>Upgrade Account</u></b></h4>
<br>");

$sre = mysql_query("SELECT * FROM ".$prefix."membertypes WHERE mtid=$acctype");
$accname = mysql_result($sre, 0, "accname");

echo '<p align=center>
<font size=4 face='.$fontface.'>Your account type is <b>'.$accname.'</b>';
if ($acctype != 1) { echo ', expires on <b>'.$upgend.'</b>'; } 
echo '</font><br><br><font size=3 face='.$fontface.'><a href="http://www.calculatingtraffic.com/members.php?page=compare" target="_blank">Click Here To Compare Member Levels</a>
<br><br>';

echo '<table width=100% align=center border=0><tr align=center><td>';

if($acctype == 1 || $_GET['action'] == "extend") {

$res = mysql_query("select * from ".$prefix."membertypes where mtid!=1 and enabled=1 order by rank desc");
   for ($i = 0; $i < mysql_num_rows($res); $i++) {
	$a_type = mysql_result($res, $i, "mtid");
    $a_name = mysql_result($res, $i, "accname");
    $a_details = mysql_result($res, $i, "description");
	$r = mysql_query("SELECT * FROM ".$prefix."ipn_products WHERE subscription=1 AND upgrade=$a_type AND disable=0 AND period=1 AND type='M';");
	$row=mysql_fetch_array($r);
	$code = "<font size=3><b>$".$row[amount]."/Month</b></font>".show_button_code($row[id],1);
	$r = mysql_query("SELECT * FROM ".$prefix."ipn_products WHERE subscription=1 AND upgrade=$a_type AND disable=0 AND (period=12 OR type='Y');");
	$row=mysql_fetch_array($r);
        if (mysql_num_rows($r) != 0) { $code=$code.'<p>';
	$code .= "<font size=3><b>$".$row[amount]."/Year</b></font>".show_button_code($row[id],1);
	}
	echo '<table border=1 width=100%>
<tr align=center><td colspan=2 style="padding:30px 270px 15px 0;"><b><font face="'.$fontface.'" size=4>'.$a_name.'</font></b></td><td>'.$code.'</td><td><font face="'.$fontface.'" size=2>'.$a_details.'</font></td></tr>
<!--<tr align="center">
<td><font face="'.$fontface.'" size=2>'.$a_details.'</font></td>
</tr>-->
</table>
<br>';
	}

   } else {

   echo '<a href="members.php?mf=ug&action=extend">Click here to extend your upgraded membership</a>';

   }

echo '</td></tr>
</table></td></tr></table></div></div><br><br>
';

include $theme_dir."/footer.php";
exit;

?>