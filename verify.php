<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.28
// Copyright �2014 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if(!isset($_REQUEST["id"]) || !isset($_REQUEST["vcode"]) || !is_numeric($_REQUEST["id"]))
{
	echo("Invalid User ID or code");
	exit;
}

require_once "inc/filter.php";
include "inc/config.php";
@mysql_connect($dbhost,$dbuser,$dbpass);
@mysql_select_db($dbname) or die( "Unable to select database");

$memberid=$_REQUEST["id"];
$vcode=$_REQUEST["vcode"];

$res=@mysql_query("SELECT COUNT(*) AS mcount FROM ".$prefix."members WHERE Id='$memberid' AND vericode='$vcode' AND status='Unverified'");
$verifyok=@mysql_result($res,0);
	
include "inc/theme.php";

load_template ($theme_dir."/header.php");  

if($verifyok == 1)
{
   @mysql_query("UPDATE ".$prefix."members SET status='Active', newsletter=1 WHERE Id='$memberid' AND status='Unverified'");
   $page_content = '<center><p>&nbsp;</p>
Your address has been verified<br />
Click <a href="'.lfm_login_url().'">HERE</a> to log in</center><p>&nbsp;</p>';

}
else
{
   $page_content = '<center><p>&nbsp;</p>
We were unable to locate your account.<br />
Your account may already be verified.<br><br>
Please Click <a href="'.lfm_login_url().'">HERE</a> to log in </center><p>&nbsp;</p>';
}

load_template ($theme_dir."/content.php");
load_template ($theme_dir."/footer.php");
?>
