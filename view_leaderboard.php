<?php
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.17
// Copyright �2012 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

if (!isset($_GET['boardid']) || !is_numeric($_GET['boardid'])) {
	echo("Invalid Board ID");
	exit;
}

?>
<html>
<body>

<center>

<script language="JavaScript" src="http://<? echo($_SERVER["SERVER_NAME"]); ?>/leaderboard.php?boardid=<? echo($_GET['boardid']); ?>"></script>

</center>

</body>
</html>