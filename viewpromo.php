<?
// /////////////////////////////////////////////////////////////////////
// LFMTE v2.01
// Copyright �2010 AKH Media Group and Josh Abbott. All Rights Reserved
// http://thetrafficexchangescript.com
//
// The sale, duplication or transfer of the script to any 
// person other than the original purchaser is a violation
// of the purchase agreement and is strictly prohibited.
// Any alteration of the script source code or accompanying 
// materials will void any responsibility that AKH Media Group 
// has regarding the proper functioning of the script.
// By using this script you agree to the terms and conditions 
// of use of the script. The terms and conditions of use are 
// included with the script in the file titled terms.html.
// /////////////////////////////////////////////////////////////////////

session_start();
include "inc/userauth.php";
include "inc/config.php";

if(!isset($_SESSION["userid"])) { exit; };

	// Get the promo ID
	if(isset($_GET["promo"]))
	{ $promoid=$_GET["promo"]; }
	 else if(isset($_POST["promo"]))
	{ $promoid=$_POST["promo"]; }
	 else
	{ 
		echo "Error: Parameter incorrect!";
		exit; 
	}

	// Get current group
	$qry="SELECT * FROM ".$prefix."promotion WHERE id=".$promoid;
	$mres=@mysql_query($qry);
	$mrow=@mysql_fetch_array($mres);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>View HTML</title>
<body>
<?= $mrow['content'] ?>
<br><br><br>
<center><input name="Button" type="button" id="Submit" value="Close" onClick="javascript:self.close();" /></center>
</body>
</html>
